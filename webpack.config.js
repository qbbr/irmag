const Encore = require('@symfony/webpack-encore');

Encore
    .setOutputPath('web/build/')
    .setPublicPath('/build')
    .disableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()

    .addEntry('irmag-site', './assets/irmag.site.bundle.js')
    .addEntry('irmag-profile', './assets/irmag.profile.bundle.js')
    .addEntry('irmag-blog', './assets/irmag.blog.bundle.js')
    .addEntry('irmag-forum', './assets/irmag.forum.bundle.js')
    .addEntry('irmag-admin', './assets/irmag.admin.bundle.js')
    .createSharedEntry('vendor', './assets/irmag.core.bundle.js')

    // loaders
    .enableSassLoader(function (options) {
        options.precision = 10;
    })
    .enablePostCssLoader()
    .enableHandlebarsLoader()
    .addLoader({ test: /\.html$/, loader: 'html-loader' })
    .addLoader({ test: /\.mp3$/, loader: 'file-loader' })

    .copyFiles([
        {
            from: './assets/site/images/',
            pattern: /irmag-logo\.png/,
            to: 'images/irmag-logo.png',
        },
        {
            from: './node_modules/ckeditor-codemirror-plugin/',
            pattern: '/codemirror/',
            to: '[path][name].[ext]',
        },
    ])

    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning()
    .enableBuildNotifications()
;

// export the final configuration
module.exports = Encore.getWebpackConfig();
