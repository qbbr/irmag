/**
 * Обёртка над bootstrap-markdown.
 *
 * @namespace IRMAG.Editor
 *
 * @requires jquery
 * @requires bootstrap-markdown
 * @requires bootbox
 * @requires IRMAG.Emoji
 * @requires Routing
 */

import Config from './modules/irmag-config';
import Routing from './modules/routing';

IRMAG.Editor = {
    defaults: {
        iconlibrary: 'fa',
        language: Config.language,
        resize: 'vertical',
        needToEmojifyPreview: false,
        ajaxConfig: function (e) {
            return {
                url: Routing.generate('irmag_markdown_to_html'),
                data: {
                    md: e.getContent(),
                },
            };
        },
        onPreview: function (e) {
            let html = '';
            // show loading
            const $previewIcon = e.$editor.find('.fa-search')
                .removeClass('fa-search').addClass('fa-heart fa-pulse');

            if (e.isDirty()) {
                const ajaxConfig = this.ajaxConfig(e);
                ajaxConfig.async = false;
                const response = $.ajax(ajaxConfig);

                if (200 === response.status) {
                    html = response.responseText;
                } else {
                    bootbox.alert('Во время предпросмотра произошла ошибка, попробуйте ещё раз.');
                }
            }

            // hide loading
            $previewIcon.removeClass('fa-heart fa-pulse').addClass('fa-search');

            return this.needToEmojifyPreview ? IRMAG.Emoji.emojifyString(html) : html;
        },
        hiddenButtons: ['Code'],
        additionalButtons: [
            [{
                name: 'groupFont',
                data: [{
                    name: 'cmdStrike',
                    toggle: true,
                    title: 'Перечёркнутый',
                    icon: 'fa fa-strikethrough',
                    callback: function (e) {
                        let cursor;
                        const selected = e.getSelection();
                        const content = e.getContent();
                        const chunk = (selected.length === 0) ? 'Перечёркнутый текст' : selected.text;

                        // transform selection and set the cursor into chunked text
                        if (content.substr(selected.start - 2, 2) === '~~' && content.substr(selected.end, 2) === '~~') {
                            e.setSelection(selected.start - 2, selected.end + 2);
                            e.replaceSelection(chunk);
                            cursor = selected.start - 2;
                        } else {
                            e.replaceSelection('~~' + chunk + '~~');
                            cursor = selected.start + 2;
                        }

                        // Set the cursor
                        e.setSelection(cursor, cursor + chunk.length);
                    },
                }],
            },
            {
                name: 'groupLink',
                data: [{
                    name: 'cmdVideo',
                    toggle: true,
                    title: 'Видео (Youtube)',
                    icon: 'fa fa-youtube-play',
                    callback: function (e) {
                        const selected = e.getSelection();
                        const link = prompt('Вставьте ссылку на YouTube-ролик', 'http://');
                        const urlRegex = new RegExp('^((http|https)://)[w.]*youtube.|youtu.be', 'i');

                        if (link !== null && link !== '' && link !== 'http://' && urlRegex.test(link)) {
                            const sanitizedLink = $('<div>' + link + '</div>').text();
                            e.replaceSelection('[YOUTUBE](' + sanitizedLink + ')');

                            // курсор в конец, после ссылки
                            const cursor = selected.start + sanitizedLink.length + 11;
                            e.setSelection(cursor, cursor);
                        }
                    },
                }],
            }],
        ],
    },

    /**
     * @param {array} config
     */
    addAdditionalButtons: function (config) {
        this.defaults.additionalButtons[0] = this.defaults.additionalButtons[0].concat(config);
    },

    /**
     * @param {Object} $elm    Textarea element
     * @param {Object} options Extra options
     *
     * @return {Object} bootstrap-markdown
     */
    init: function ($elm, options) {
        options = $.extend(this.defaults, options);

        return $elm.markdown(options).data('markdown');
    },
};
