/**
 * Опросы.
 *
 * @namespace IRMAG.Poll
 *
 * @requires jquery
 */

IRMAG.Poll = {
    init: function () {
        this.bindCheckVariant();
        this.bindBlockFormOnSubmit();
    },

    /**
     * Отметить вариант в голосовании.
     */
    bindCheckVariant: function () {
        const self = this;

        $(document).on('click', 'label.poll-variant', function (e) {
            e.preventDefault();

            const isChecked = $(this).hasClass('checked');
            const isMultiple = ($(this).find('input[type="checkbox"]').length > 0);

            if (!isChecked) {
                if (!isMultiple) {
                    $('label.poll-variant').each(function () {
                        self.clearSelected(this);
                    });
                }

                $(this).addClass('checked');
                $(this).find('input').prop('checked', true);
            } else {
                self.clearSelected(this);
            }

            self.enabledSubmitIfChecked();
        });
    },

    /**
     * Разблокировать кнопку голосования, если выбран вариант.
     */
    enabledSubmitIfChecked: function () {
        const checkedTotal = $('.poll-variant-content input:checked').length;
        const $submitBtn = $('#poll_submit');

        if (!checkedTotal) {
            $submitBtn.prop('disabled', true);
        } else {
            $submitBtn.prop('disabled', false);
        }
    },

    /**
     * Заблокировать форму на время отправки.
     */
    bindBlockFormOnSubmit: function () {
        $(document).on('submit', 'form[name="poll"]', function () {
            $('.poll-content').addClass('block-loading');
        });
    },

    /**
     * Снять выбор с группы элементов по селектору.
     *
     * @param selector
     */
    clearSelected: function (selector) {
        $(selector).removeClass('checked');
        $(selector).find('input').prop('checked', false);
    },
};
