/**
 * @requires jquery
 */

import log from './modules/logger';

(function ($) {
    $.fn.multiTelephoneField = function (options) {
        /**
         * @private
         *
         * @param val
         */
        function normalizeTelephoneNumber(val) {
            // trim
            val = val.trim();

            // not mobile
            if (val.length < 10) {
                return val;
            }

            // remove "8"
            if ('8' === val.charAt(0)) {
                val = val.slice(1);
            }

            return val;
        }

        /**
         * @private
         *
         * @param $input
         */
        function maskTelephone($input) {
            $input.val(normalizeTelephoneNumber($input.val()));
            $input.mask('+7 (Z00) 000-00-00', {
                translation: {
                    Z: {
                        pattern: /9/,
                    },
                },
            });
        }

        /**
         * @private
         *
         * @param $input
         */
        function umaskTelephone($input) {
            $input.unmask();
        }

        return this.each(function () {
            const $btnIcon = $(this).find('.dropdown-toggle i');
            const $dropdown = $(this).find('.dropdown-menu');
            const $telephoneInput = $(this).find('input[type="text"]');
            const $checkbox = $(this).find('input[type="checkbox"]');
            const checked = $checkbox.prop('checked');

            $dropdown.on('click', 'li', function () {
                const $li = $(this);
                const phoneType = $li.data('phone-type');
                log('[multiTelephoneField]', 'select phone-type:', phoneType);

                if ('mobile' === phoneType) {
                    maskTelephone($telephoneInput);
                    $telephoneInput.attr({ minlength: 18, maxlength: 18, placeholder: '+7' });
                    $checkbox.prop('checked', true);
                    $btnIcon.addClass($btnIcon.data('icon-mobile')).removeClass($btnIcon.data('icon-default'));
                } else {
                    umaskTelephone($telephoneInput);
                    $telephoneInput.removeAttr('placeholder minlength maxlength required');
                    $checkbox.prop('checked', false);
                    $btnIcon.addClass($btnIcon.data('icon-default')).removeClass($btnIcon.data('icon-mobile'));
                }

                // active
                $li.addClass('active').siblings().not($li).removeClass('active');

                // callback
                if ($.isFunction(options.onCheckboxChanged)) {
                    options.onCheckboxChanged($checkbox, $telephoneInput.val());
                }
            });

            if ($.isFunction(options.onValueChanged)) {
                $telephoneInput.on('change keyup', function () {
                    options.onValueChanged($telephoneInput.val());
                });
            }

            // state triggers
            if (true === checked) {
                $dropdown.find('li[data-phone-type="mobile"]').trigger('click');
            } else {
                $dropdown.find('li[data-phone-type="default"]').trigger('click');
            }
        });
    };
}(jQuery));
