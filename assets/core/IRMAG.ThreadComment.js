/**
 * Древовидные комментарии.
 *
 * @namespace IRMAG.ThreadComment
 *
 * @requires jquery
 * @requires Routing
 */

import Routing from './modules/routing';
import { ajax } from './modules/irmag-utils';
import { bind, go } from './modules/jumper';
import log from './modules/logger';

IRMAG.ThreadComment = {
    $threadComments: null,

    init: function () {
        log('[ThreadComment]', 'init');

        this.$threadComments = $('#thread-comments');

        if (0 === this.$threadComments.length) {
            return;
        }

        this.initTextarea();
        this.initEmojify();
        this.bindSubmit();
        this.bindJmpToComposeLink();
        this.bindReply();
        this.bindAnchor();
        this.bindRating();
        this.bindLikeDislike();
        this.bindBlockFormOnSubmit();
    },

    bindJmpToComments: function () {
        bind('comment-');
    },

    initTextarea: function () {
        const $textarea = $('#irmag_thread_comment_text');

        if (0 === $textarea.length) {
            return;
        }

        const tmpText = $textarea.val().trim();
        const isAdvancedCommentator = $('#comment-compose').data('advanced');

        $textarea.val('');

        IRMAG.Editor.defaults.ajaxConfig = function (e) {
            return {
                url: Routing.generate('irmag_markdown_to_html'),
                data: {
                    md: e.getContent(),
                    mode: 'thread_comment',
                },
            };
        };

        IRMAG.Editor.defaults.needToEmojifyPreview = true;

        // прикреплять видео и картинки по ссылками могут только избранные
        if (!isAdvancedCommentator) {
            IRMAG.Editor.defaults.hiddenButtons.push('cmdVideo', 'Image');
        }

        IRMAG.Editor.init($textarea).setContent(tmpText);
    },

    initEmojify: function () {
        IRMAG.Emoji.emojify($('#thread-comments .comment-text p'));
        IRMAG.Emoji.bindInput($('#irmag_thread_comment_text'));
    },

    bindJmpToComposeLink: function () {
        this.$threadComments.find('.jmp-to-compose').on('click', function () {
            go('comment-compose', function ($elm) {
                $elm.find('textarea').focus();
            });
        });
    },

    bindReply: function () {
        const self = this;
        // Форма "Оставить отзыв"
        const $commentCompose = $('#comment-compose');
        // input[type="hidden"] с родительским Ид или с пустым значение, если это не ответ
        const $commentParent = $('#irmag_thread_comment_parent');

        // кнопка "Ответить"
        self.$threadComments.find('.media .reply').on('click', function () {
            const commentId = $(this).parents('.media:first').data('comment-id');

            $commentParent.val(commentId);
            $commentCompose.insertAfter($(this).parents('.media-body:first').children('.comment-text'));
            $commentCompose.find('textarea').focus();
        });

        // кнопка "Отмена"
        $commentCompose.find('.comment-reply-cancel')
            .removeClass('hidden')
            .on('click', function () {
                $commentCompose.appendTo(self.$threadComments);
                $commentParent.val('');
            });
    },

    bindSubmit: function () {
        $(document).on('submit', 'form[name="irmag_thread_comment"]', function (e) {
            e.preventDefault();

            $.ajax({
                url: Routing.generate('irmag_thread_comment_save'),
                data: {
                    formData: $(this).serialize(),
                    threadId: $('#thread-comments').data('id'),
                },
                success: function (data) {
                    if (data.success) {
                        window.location.hash = 'jmp-to-comment-' + data.id;
                        window.location.reload();
                    }
                },
            });
        });
    },

    bindAnchor: function () {
        this.$threadComments.find('.media a.anchor').on('click', function () {
            go($(this).data('jumper'));
        });
    },

    bindRating: function () {
        this.$threadComments.find('.block-rating .rating i')
            .on('mouseenter', function () {
                $(this)
                    .addClass('hover')
                    .prevAll().addClass('hover');
            })
            .on('mouseleave', function () {
                $(this)
                    .removeClass('hover')
                    .prevAll().removeClass('hover');
            })
            .on('click', function () {
                // remove old selected
                $(this).parent().children().removeClass('selected');
                // add new selected
                $(this)
                    .addClass('selected')
                    .prevAll().addClass('selected');

                const rating = $(this).index() + 1;

                $(this).parents('.block-rating').find('.rating-text small').text(rating + ' из 5');
                $('#irmag_thread_comment_rating').val(rating);
            });
    },

    bindLikeDislike: function () {
        const self = this;

        self.$threadComments.find('.comment-vote').on('click', function () {
            const $btn = $(this);
            const $btnIcon = $btn.find('i');

            self._voteScore($btn.data('comment-id'), $btn.data('score'), function (data) {
                if (data.success) {
                    if ($btn.hasClass('like')) {
                        // like
                        if ($btnIcon.hasClass('fa-thumbs-o-up')) {
                            $btnIcon.removeClass('fa-thumbs-o-up').addClass('fa-thumbs-up');
                        }
                    } else if ($btn.hasClass('dislike')) {
                        // dislike
                        if ($btnIcon.hasClass('fa-thumbs-o-down')) {
                            $btnIcon.removeClass('fa-thumbs-o-down').addClass('fa-thumbs-down');
                        }
                    }

                    const $scoreCounter = $btn.parent().find('.score-counter').text(data.score);

                    if (data.score > 0) {
                        $scoreCounter.addClass('text-success');
                    } else if (data.score < 0) {
                        $scoreCounter.addClass('text-danger');
                    } else {
                        $scoreCounter.addClass('text-muted');
                    }
                }
            });
        });
    },

    /**
     * Like/Dislike
     *
     * @private
     *
     * @param {number}   commentId Ид комментария
     * @param {number}   score     Очки Like/Dislike
     * @param {function} callback  [optional]
     */
    _voteScore: function (commentId, score, callback) {
        ajax('irmag_thread_comment_vote', {
            commentId: commentId,
            score: score,
        }, callback);
    },

    bindBlockFormOnSubmit: function () {
        $(document).on('submit', '#thread-comments form:first', function () {
            $('#comment-compose').addClass('block-loading');
            $('.comment-spinner').show();
        });
    },
};
