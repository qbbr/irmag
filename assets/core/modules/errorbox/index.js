import './index.scss';

/**
 * Выводит сообщение об ошибке.
 *
 * @param {string} message
 * @param {object} options
 */
export default function error(message, options = {}) {
    bootbox.alert($.extend({
        title: '😿 У-упс, ошибочка вышла',
        message: message,
        className: 'irmag-modal-error',
        buttons: {
            ok: {
                label: 'Понятненько',
                className: 'btn-danger',
            },
        },
    }, options));
}
