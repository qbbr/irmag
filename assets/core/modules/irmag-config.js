const Config = {
    assetsDir: './assets',
    imagesDir: './assets/site/images',
    emojisDir: 'https://cdnjs.cloudflare.com/ajax/libs/emojify.js/1.1.0/images/basic',
    language: 'ru',
    dateFormat: 'dd.mm.yyyy',
    daDataToken: 'f91f6fcbbf4607bb43253f359903b6a471d72fbe',
    debug: process.env.NODE_ENV !== 'production',
};

export default Config;
