import log from './logger';

export const suffix = '#jmp-to-';
const selectedCls = 'jumper-selected';
const offsetTop = -200;

/**
 * Скролит страницу к элемент и добавляет к нему класс {selectedCls}.
 *
 * @param {string}   elementSel CSS selector
 * @param {function} callback   Callback
 */
export function go(elementSel, callback = null) {
    log('[Jumper]', 'go to #' + elementSel);

    const $anchor = $('#' + elementSel);

    if (0 === $anchor.length) {
        log('[Jumper]', 'elm not found');

        return false;
    }

    // rm old selected elms
    $('body').find('.' + selectedCls).removeClass(selectedCls);
    // add selected cls
    $anchor.addClass(selectedCls);

    // scroll to elm
    $('html, body').animate({
        scrollTop: $anchor.offset().top + offsetTop,
    }, 'slow');

    if ($.isFunction(callback)) {
        callback($anchor);
    }
}

/**
 * @param {string} elementSuffix Hash suffix
 *
 * @return {boolean}
 */
export function bind(elementSuffix) {
    if (window.location.hash) {
        if (0 === window.location.hash.indexOf(suffix)) {
            const elementSel = window.location.hash.substring(suffix.length);

            if (-1 !== elementSel.indexOf(elementSuffix)) {
                go(elementSel);

                return true;
            }

            return false;
        }
    }
}
