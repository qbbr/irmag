import Routing from '../../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min';

const routes = require('./../../../var/routes.json'); // eslint-disable-line import/no-unresolved

Routing.setRoutingData(routes);

export default Routing;
