/**
 * @namespace IRMAG.Login
 *
 * @requires jquery
 */

import log from '../core/modules/logger';

IRMAG.Login = {
    init: function () {
        log('[Login]', 'init');

        this.bindPasswordEye();
    },

    bindPasswordEye: function () {
        $('[data-toggle="password"]').on('click', function () {
            const $input = $('#password');

            if ('password' === $input.attr('type')) {
                $input.attr('type', 'text');
            } else {
                $input.attr('type', 'password');
            }

            $(this).find('i').toggleClass('fa-eye fa-eye-slash');
        });
    },
};
