/**
 * Написание поста в блоге.
 *
 * @namespace IRMAG.Blog.Post
 *
 * @requires jquery
 * @requires select2
 * @requires bootbox
 * @requires Routing
 */

import Routing from '../core/modules/routing';
import log from '../core/modules/logger';
import rateVariantsTmpl from './templates/rate-variants.html';

IRMAG.Blog.Post = {
    isValid: false,

    init: function () {
        this.initElementSelector();
        this.initTagSelector();
        this.initTextArea();
        this.initPhotoSliderOnPreview();

        this.bindSelectDropDownMenuItem();
        this.bindChangeControlsOnResize();
        this.bindAddElementToReview();
        this.bindRemoveFromReview();
        this.bindClickToSaveDraft();
        this.bindFormChange();
        this.bindFormSubmit();
    },

    /**
     * Инициализация текстового редактора.
     */
    initTextArea: function () {
        const $textarea = $('#blog_post_text');
        const tmpText = $textarea.val().trim();

        $textarea.val('');
        IRMAG.Editor.addAdditionalButtons([
            {
                name: 'groupCustom',
                data: [{
                    name: 'cmdRules',
                    toggle: true,
                    title: 'Правила публикации',
                    icon: 'fa fa-info-circle',
                    callback: function () {
                        const url = Routing.generate('irmag_blog_rules');
                        window.open(url, '_blank');
                    },
                }],
            },
        ]);

        IRMAG.Editor.defaults.ajaxConfig = function (e) {
            return {
                url: Routing.generate('irmag_markdown_to_html'),
                data: {
                    md: e.getContent(),
                    mode: 'blog_post',
                    object: (typeof $textarea.data('object') !== 'undefined') ? $textarea.data('object') : '',
                    need_wrapping: true,
                },
            };
        };

        IRMAG.Editor.init($textarea).setContent(tmpText);
    },

    /**
     * Инициализация слайдера при предварительном просмотре поста.
     */
    initPhotoSliderOnPreview: function () {
        $(document).on('click', function () {
            IRMAG.Blog.Page.initSlider(false);
        });
    },

    /**
     * Инициализация селектора товаров.
     */
    initElementSelector: function () {
        const self = this;
        const $reviewSelectors = $('input.review-selector');
        let i = 1;

        $(document).ready(function () {
            $reviewSelectors.each(function () {
                const $selector = $(this);
                self.activateElementSelector($selector, $selector.data('element-id'));
                const elementName = $selector.data('element-name');

                if ('undefined' !== elementName) {
                    $selector.parent().find('.select2-container .select2-choice .select2-chosen').replaceWith([
                        '<span id="select2-chosen-', i, '" class="select2-chosen">', elementName, '</span>',
                    ].join(''));
                }

                i++;
            });
        });
    },

    /**
     * Инициализация селектора тэгов.
     */
    initTagSelector: function () {
        const $tagsBlock = $('#blog_post_blog_post_tags');
        let tagsSelected = $('.new-post-tags-block').data('tags');

        tagsSelected = (typeof tagsSelected !== 'undefined') ? tagsSelected : null;

        $(document).ready(function () {
            $tagsBlock.select2({
                placeholder: 'Вводите теги через запятую',
                maximumSelectionLength: 5,
                allowClear: true,
                tags: $tagsBlock.data('available-tags'),
                tokenSeparators: [','],
            })
             .val(tagsSelected)
             .trigger('change');
        });
    },

    /**
     * Поменять элементы формы при ресайзе окна.
     */
    bindChangeControlsOnResize: function () {
        $(window).resize(function () {
            const $rateButtons = $('.review-good-rate button');
            const rates = ['1 — ужасно', '2 — плохо', '3 — удовлетворительно', '4 — хорошо', '5 — отлично'];

            // sm max = 992px
            $rateButtons.each(function () {
                const id = $(this).data('id');
                let text;

                if ($(window).width() > 992) {
                    text = (id === '') ? 'Моя оценка' : rates[parseInt(id, 10) - 1];
                } else {
                    text = (id === '') ? 'Оценка' : parseInt(id, 10).toString();
                }

                $(this)
                    .text(text)
                    .parent('.review-good-rate').find('ul.dropdown-menu').html(rateVariantsTmpl);
            });
        });
    },

    /**
     * Установка data-атрибутов при выборе значения выпадашки.
     */
    bindSelectDropDownMenuItem: function () {
        const self = this;

        $(document).on('click', 'ul.dropdown-menu li', function (e) {
            e.preventDefault();

            const $parentButton = $(this).parent().siblings('button');
            const buttonClass = $parentButton.attr('class');
            let text;

            if (buttonClass.indexOf('elm-rate') !== -1) {
                text = ($(window).width() > 992)
                    ? $(this).find('.elm-rate-value').text() + $(this).find('span.hidden-xs').text()
                    : $(this).find('.elm-rate-value').text();
            } else if (buttonClass.indexOf('section-item') !== -1) {
                text = $(this).find('a:first').text();
            }

            $parentButton.text(text);
            $parentButton.data('id', $(this).attr('id').split('-')[1]);
            self.triggerAutoSave();
        });
    },

    /**
     * Добавить форму товара для обзора.
     */
    bindAddElementToReview: function () {
        const self = this;

        $(document).on('click', 'a.add-to-review', function (e) {
            e.preventDefault();

            const $blogPostElements = $('#blog_post_post_elements');
            const goodsCount = $blogPostElements.children('.form-group').length;
            let dummyHtml = $blogPostElements.attr('data-prototype');

            dummyHtml = dummyHtml.replace(/__name__/g, goodsCount);
            $('div.goods-for-review').append(dummyHtml);

            const $elementSelector = $('.review-selector:last');
            self.activateElementSelector($elementSelector, null);
        });
    },

    /**
     * Удалить товар из обзора.
     */
    bindRemoveFromReview: function () {
        const self = this;

        $(document).on('click', 'span.delete-related-element', function () {
            const $parentWrapper = $(this).parents('div[id^="blog_post_post_elements_"]:first').parent('.form-group');
            const id = $parentWrapper.find('input[id^="blog_post_post_elements_"]').val();

            if (id !== null) {
                $.ajax({
                    url: Routing.generate('irmag_blog_remove_post_element'),
                    dataType: 'json',
                    data: {
                        id: id,
                        post: $('#blog_post_id').val(),
                    },
                    success: function (data) {
                        if (data.success) {
                            $parentWrapper.remove();
                            self.triggerAutoSave();
                        }
                    },
                    fail: function () {
                        bootbox.alert('Не удаётся удалить товар из обзора.');
                    },
                });
            } else {
                $parentWrapper.remove();
            }
        });
    },

    /**
     * Сохранения черновика по клике на кнопку.
     */
    bindClickToSaveDraft: function () {
        const self = this;

        $(document).on('click', '.btn-post-to-draft', function () {
            $.ajax({
                url: Routing.generate('irmag_blog_save_draft'),
                dataType: 'json',
                data: self.getCurrentData(),
                success: function (data) {
                    if (data.success) {
                        const currentId = $('#irmag_blog_id').val();

                        if (currentId === '') {
                            $('#blog_post_id').val(data.id);
                            bootbox.alert('Ваша запись сохранена как черновик.');
                        } else {
                            bootbox.alert('Черновик успешно обновлён.');
                        }
                    }
                },
                fail: function () {
                    bootbox.alert('Ошибка при сохранении черновика!');
                },
            });
        });
    },

    /**
     * По изменению формы вызывать автосохранение.
     */
    bindFormChange: function () {
        const self = this;

        $(document).on('change', 'form', function () {
            self.triggerAutoSave();
        });
    },

    /**
     * Вызвать валидацию перед сабмитом.
     */
    bindFormSubmit: function () {
        const self = this;

        $(document).on('submit', function (e) {
            if (self.isValid === false) {
                e.preventDefault();
                self.isValid = self.checkBeforeSend();
            }
        });
    },

    /**
     * Активация селектора товаров.
     *
     * @param $selector
     * @param value
     */
    activateElementSelector: function ($selector, value) {
        $selector.select2({
            allowClear: true,
            minimumInputLength: 2,
            placeholder: 'Введите название товара',
            ajax: {
                url: Routing.generate('irmag_blog_get_elements'),
                cache: true,
                dataType: 'JSON',
                method: 'GET',
                delay: 250,
                data: function (term) {
                    return {
                        term: term,
                    };
                },
                results: function (data) {
                    return {
                        results: $.map(data.results, function (item) {
                            return {
                                text: item.text,
                                id: item.id,
                            };
                        }),
                    };
                },
            },
        })
            .val(value);
    },

    /**
     * Автосохранение черновика.
     */
    triggerAutoSave: function () {
        const self = this;

        $.ajax({
            url: Routing.generate('irmag_blog_save_draft'),
            dataType: 'json',
            data: self.getCurrentData(),
            fail: function () {
                log('[ERROR]: unable to save draft');
            },
        });
    },

    /**
     * Дополнительная валидация формы перед отправкой.
     *
     * @return bool
     */
    checkBeforeSend: function () {
        const $items = $('input[id^="blog_post_post_elements_"]');
        const $rates = $('button.elm-rate');
        const section = $('#blog_post_blog_section_name').data('id');
        const $attachments = $('.uploaded-pic-container');
        let needToContinue = true;
        let flag = false;

        if (section === '') {
            bootbox.alert('Пожалуйста, выберите раздел для вашего обзора');
        } else if ($items.length !== 0 && $rates.length !== 0) {
            $items.each(function () {
                if ($(this).data('id') === '') {
                    bootbox.alert('Пожалуйста, выберите товары, которые упоминаются в вашем обзоре.');
                    needToContinue = false;

                    return false;
                }
            });

            if (needToContinue === true) {
                $rates.each(function () {
                    if ($(this).data('id') === '') {
                        bootbox.alert('Пожалуйста, оцените все добавленные вами товары.');
                        needToContinue = false;

                        return false;
                    }
                });
            }

            if (needToContinue === true) {
                if ($attachments.length === 0) {
                    bootbox.alert('Пожалуйста, добавьте фотографии к вашему обзору.');
                } else {
                    $attachments.each(function () {
                        const currentAttId = $(this).data('id');

                        if ($('#blog_post_text').data('markdown').getContent().indexOf('](' + currentAttId + ')') === -1) {
                            $(this).addClass('unattached');
                            bootbox.alert('Пожалуйста, вставьте все прикрепленные фотографии в текст обзора, либо удалите неиспользуемые прикреплённые фотографии.');
                            needToContinue = false;

                            return false;
                        }

                        if ($(this).hasClass('unattached')) {
                            $(this).removeClass('unattached');
                        }
                    });
                }
            }

            flag = needToContinue;
        } else {
            bootbox.alert('Пожалуйста, добавьте товары, которые вы обозреваете.');
        }

        return flag;
    },

    /**
     * Собрать текущие данные формы.
     *
     * @return Object
     */
    getCurrentData: function () {
        const selectorPrefix = '#blog_post_';
        const section = $(selectorPrefix + 'blog_section_name').data('id') ? $(selectorPrefix + 'blog_section_name').data('id') : '';

        return {
            id: $(selectorPrefix + 'id').val(),
            md: $(selectorPrefix + 'text').data('markdown').getContent(),
            title: $(selectorPrefix + 'title').val(),
            subtitle: $(selectorPrefix + 'subtitle').val(),
            tags: $(selectorPrefix + 'blog_post_tags').val(),
            section: section,
            attachments: this.getCurrentAttachments(),
            elements: this.getElements(),
        };
    },

    /**
     * Получить список всех прикрепленных для обзора товаров с их оценками.
     *
     * @return string
     */
    getElements: function () {
        const $elements = $('input[id^="blog_post_post_elements_"]');
        const result = [];

        $elements.each(function () {
            if ($(this).val() !== null) {
                const rate = $(this)
                    .parents('.element-name:first')
                    .parents('div[id^="blog_post_post_elements_"]:first')
                    .find('button:first')
                    .data('id');

                if (typeof rate !== 'undefined' && rate !== '') {
                    result.push({ element: $(this).val(), rate: rate });
                }
            }
        });

        return JSON.stringify(result);
    },

    /**
     * Получить список аттачей.
     *
     * @return string
     */
    getCurrentAttachments: function () {
        const attachments = [];

        $('.uploaded-pic-container').each(function () {
            attachments.push($(this).data('id'));
        });

        return JSON.stringify(attachments);
    },
};
