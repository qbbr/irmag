/**
 * Написание поста в блоге.
 *
 * @namespace IRMAG.Blog.Page
 *
 * @requires jquery
 * @requires bootbox
 * @requires Routing
 */

import Routing from '../core/modules/routing';
import { ajax } from '../core/modules/irmag-utils';

IRMAG.Blog.Page = {
    init: function () {
        this.initSlider(true);
        this.bindToggleBottomTabs();
        this.bindRemovePost();
        this.bindVote();
        this.bindAddToCart();
    },

    /**
     * Инициализация слайдера для постов.
     */
    initSlider: function (adaptiveHeight) {
        $('.slider-container').slick({
            dots: true,
            adaptiveHeight: adaptiveHeight,
            infinite: true,
            speed: 500,
            fade: true,
            cssEase: 'linear',
        });
    },

    /**
     * Переключение между вкладками "Комментарии" и "Другие посты".
     */
    bindToggleBottomTabs: function () {
        $(document).on('click', 'ul.post-bottom-tabs li', function () {
            $('ul.post-bottom-tabs li').addClass('post-bottom-tab-inactive');
            $('ul.post-bottom-tabs li span').addClass('post-bottom-link-inactive');
            $(this).removeClass('post-bottom-tab-inactive');
            $(this).children('span').removeClass('post-bottom-link-inactive');
        });

        $(document).on('click', '#post-show-comments', function () {
            $('.post-bottom-others-content').hide();
            $('.post-bottom-comments-content').show();
        });

        $(document).on('click', '#post-show-others', function () {
            $('.post-bottom-comments-content').hide();
            $('.post-bottom-others-content').show();
        });
    },

    /**
     * Если товар в наличии - положить в корзину, иначе перейти на страницу с товаром.
     */
    bindAddToCart: function () {
        $(document).on('click', '.blog-to-cart', function (e) {
            if ($(this).data('active') && $(this).hasClass('blog-button-red')) {
                e.preventDefault();

                const elementId = $(this).data('element-id');
                const self = $(this);

                ajax('irmag_blog_basket_ajax_add_element', {
                    elementId: elementId,
                    amount: 1,
                }, function () {
                    IRMAG.API.Basket.notifyAdd();
                    self.removeClass('blog-button-red').addClass('blog-button-green').text('В корзине');
                });
            }
        });
    },

    /**
     * Удаление черновиков и отклоненных постов.
     */
    bindRemovePost: function () {
        $(document).on('click', '.blog-post-remove', function () {
            const chunks = $(this).parents('tr').attr('id').split('-');
            const id = chunks[1];
            bootbox.confirm({
                buttons: {
                    confirm: {
                        label: 'Да',
                        className: 'btn-primary',
                    },
                    cancel: {
                        label: 'Нет',
                        className: 'btn-danger',
                    },
                },
                message: 'Вы действительно хотите удалить этот пост?',
                callback: function (res) {
                    if (res === true) {
                        $.ajax({
                            url: Routing.generate('irmag_blog_remove_post'),
                            data: {
                                id: id,
                            },
                            dataType: 'json',
                            success: function (data) {
                                if (data.success) {
                                    bootbox.alert('Пост удалён.', function () { window.location.reload(true); });
                                } else {
                                    bootbox.alert('Не удаётся удалить пост. Обратитесь к администратору.');
                                }
                            },
                            fail: function () {
                                bootbox.alert('Не удаётся удалить пост. Обратитесь к администратору.');
                            },
                        });
                    }
                },
                title: 'Подтверждение',
            });
        });
    },

    /**
     * Голосование за посты.
     */
    bindVote: function () {
        $(document).on('click', '.post-rating i', function () {
            let score = 0;
            const currentClass = $(this).attr('class');
            const currentRating = parseInt($('.post-rating').text().trim(), 10);

            if (currentClass.indexOf('green-thumb') !== -1) {
                score = 1;
            } else if (currentClass.indexOf('red-thumb') !== -1) {
                score = -1;
            }

            $.ajax({
                url: Routing.generate('irmag_blog_vote_for_post'),
                dataType: 'json',
                data: {
                    score: score,
                    post: parseInt($('input[name="viewed-post-id"]').val(), 10),
                },
                beforeSend: function () {
                    $('.post-rating i').hide();
                },
                success: function (data) {
                    if (data.status === true) {
                        $('.post-rating').text('Рейтинг: ' + (currentRating + score));
                    }
                },
                fail: function () {
                    bootbox.alert('Ошибка при попытке голосования.');
                },
            });
        });
    },
};
