/**
 * Посещение форума.
 *
 * @namespace IRMAG.Forum.Visitor
 *
 * @requires jquery
 * @requires Routing
 */

import Routing from '../core/modules/routing';

IRMAG.Forum.Visitor = {
    /* Зарегистрировать просмотр темы */
    log: function () {
        const topic = $('.forum-topic-posts').data('id');
        const user = $('.forum-container').data('user');

        if (user !== '') {
            $.ajax({
                url: Routing.generate('irmag_forum_log_visitor'),
                dataType: 'json',
                data: {
                    topic: topic,
                },
            });
        }
    },
};
