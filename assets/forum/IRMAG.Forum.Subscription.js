/**
 * Управление подписками на форуме.
 *
 * @namespace IRMAG.Forum.Subscription
 *
 * @requires jquery
 * @requires Routing
 * @requires bootbox
 */

import Routing from '../core/modules/routing';

IRMAG.Forum.Subscription = {
    init: function () {
        this.bindClickToSubscribeForTopic();
    },

    /* Подписаться на обновление темы */
    bindClickToSubscribeForTopic: function () {
        const _ = this;

        $(document).on('click', '.subscribe-to-topic', function (e) {
            e.preventDefault();
            _.subscribe($(this), $(this).data('topic'), 'irmag_forum_subscribe_to_topic');
        });
    },

    /* AJAX-запрос на подписку */
    subscribe: function (self, id, route) {
        const status = self.data('status');

        $.ajax({
            url: Routing.generate(route),
            dataType: 'json',
            data: {
                status: status,
                id: id,
            },
            success: function () {
                if (status === false) {
                    self.attr('data-original-title', 'Отменить подписку на обновления');
                    self.find('i').replaceWith('<i class="fa fa-undo forum-attention"></i>');
                    self.data('status', true);
                } else if (status === true) {
                    self.attr('data-original-title', 'Подписаться на обновления');
                    self.find('i').replaceWith('<i class="fa fa-paper-plane-o forum-available"></i>');
                    self.data('status', false);
                }
            },
            fail: function () {
                bootbox.alert('Не удалось подписаться. Попробуйте ещё раз или обратитесь к администратору форума.');
            },
        });
    },
};
