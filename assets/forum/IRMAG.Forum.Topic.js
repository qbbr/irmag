/**
 * Тема на форуме.
 *
 * @namespace IRMAG.Forum.Topic
 *
 * @requires jquery
 * @requires bootbox
 * @requires select2
 */

import Routing from '../core/modules/routing';
import log from '../core/modules/logger';

IRMAG.Forum.Topic = {
    init: function () {
        this.initEditor();
        this.initTagsSelector();
        this.bindClickToMarkAllRead();
        this.bindClickToAddTopic();
        this.bindSubmitTopic();
    },

    /* Добавить новую тему на форум */
    bindClickToAddTopic: function () {
        $(document).on('click', '.forum-add-topic', function () {
            const forumId = $('.forums-table').data('forum');
            window.location = Routing.generate('irmag_forum_new_topic', { id: forumId });
        });
    },

    /* Пометить все темы форума как прочитанные */
    bindClickToMarkAllRead: function () {
        $(document).on('click', '.forum-mark-all-topics-read', function (e) {
            e.preventDefault();

            const forum = $(this).find('i:first').data('forum');

            bootbox.confirm({
                buttons: {
                    confirm: {
                        label: 'Да',
                        className: 'btn-primary',
                    },
                    cancel: {
                        label: 'Нет',
                        className: 'btn-danger',
                    },
                },
                message: 'Вы действительно хотите отметить все темы как прочитанные?',
                callback: function (res) {
                    if (res === true) {
                        $.ajax({
                            url: Routing.generate('irmag_forum_read_topics'),
                            dataType: 'json',
                            data: {
                                forum: forum,
                            },
                            fail: function () {
                                log('[ERROR]: could not mark all topics as read.');
                            },
                            success: function () {
                                window.location.reload();
                            },
                        });
                    }
                },
            });
        });
    },

    /* Инициализация селектора тегов */
    initTagsSelector: function () {
        const $input = $('#forum-topic-tag-selector');
        const availableTags = $input.data('available-tags');

        $(document).ready(function () {
            $input.select2({
                placeholder: 'Введите тег',
                maximumSelectionLength: 5,
                tags: availableTags,
                tokenSeparators: [','],
            });
        });
    },

    /* Инициализация редактора */
    initEditor: function () {
        IRMAG.Editor.defaults.hiddenButtons.push('Heading');
        IRMAG.Editor.defaults.needToEmojifyPreview = true;
        IRMAG.Editor.defaults.ajaxConfig = function (e) {
            return {
                url: Routing.generate('irmag_markdown_to_html'),
                data: {
                    md: e.getContent(),
                    mode: 'forum_post',
                },
            };
        };

        const $textarea = $('#forum_topic_post_text');

        if ($textarea.length > 0) {
            const tmpText = $textarea.val().trim();

            IRMAG.Editor.init($textarea).setContent(tmpText);
            IRMAG.Emoji.bindInput($textarea);
        }
    },

    /* Сабмит формы */
    bindSubmitTopic: function () {
        $(document).on('click', '#forum_topic_submit', function () {
            $('.new-topic-container').addClass('block-loading');
            $('.forum-spinner').show();
        });
    },
};
