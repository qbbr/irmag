/**
 * Список персональных сообщений форума.
 *
 * @namespace IRMAG.Forum.Messagelist
 *
 * @requires jquery
 * @requires bootbox
 * @requires Routing
 */

import Routing from '../core/modules/routing';

IRMAG.Forum.Messagelist = {
    init: function () {
        this.bindRemoveMessage();
        this.bindClearMessage();
        this.bindMarkUnread();
        this.bindMarkAsRead();
        this.bindRestoreMessage();
        this.bindClearAllMessages();
    },

    /* Очистить всю папку (если корзина - всё удалить) */
    bindClearAllMessages: function () {
        $(document).on('click', 'a.forum-messages-clear-all', function (e) {
            e.preventDefault();
            const type = $(this).parents('tr:first').data('type');

            bootbox.confirm({
                buttons: {
                    confirm: {
                        label: 'Да',
                        className: 'btn-primary',
                    },
                    cancel: {
                        label: 'Нет',
                        className: 'btn-danger',
                    },
                },
                message: (type === 'trash')
                        ? 'Вы действительно хотите удалить всё из корзины?'
                        : 'Вы действительно хотите переместить все сообщения из папки в корзину?',
                callback: function (res) {
                    if (res === true) {
                        $.ajax({
                            url: Routing.generate('irmag_forum_clear_messages_folder'),
                            dataType: 'json',
                            data: {
                                type: type,
                            },
                            success: function (data) {
                                if (data.success) {
                                    window.location.reload();
                                }
                            },
                            fail: function () {
                                bootbox.alert('Что-то пошло не так. Пожалуйста, повторите ваше действие.');
                            },
                        });
                    }
                },
            });
        });
    },

    /* Восстановить сообщение из корзины */
    bindRestoreMessage: function () {
        $(document).on('click', 'a.forum-message-restore', function (e) {
            e.preventDefault();
            IRMAG.Forum.Messagelist.doActionWithMessage($(this), 'irmag_forum_message_restore');
        });
    },

    /* Удалить в корзину */
    bindRemoveMessage: function () {
        $(document).on('click', 'a.forum-message-remove', function (e) {
            e.preventDefault();
            IRMAG.Forum.Messagelist.removeMessageForUser($(this));
        });
    },

    /* Удалить сообщение из корзины */
    bindClearMessage: function () {
        $(document).on('click', 'a.forum-message-clear', function (e) {
            e.preventDefault();
            IRMAG.Forum.Messagelist.removeMessageForUser($(this));
        });
    },

    /* Пометить как непрочитанное */
    bindMarkUnread: function () {
        $(document).on('click', 'a.forum-message-mark-unread', function (e) {
            e.preventDefault();
            IRMAG.Forum.Messagelist.doActionWithMessage($(this), 'irmag_forum_mark_unread');
        });
    },

    /* Пометить как непрочитанное */
    bindMarkAsRead: function () {
        $(document).on('click', 'a.forum-message-mark-as-read', function (e) {
            e.preventDefault();
            IRMAG.Forum.Messagelist.doActionWithMessage($(this), 'irmag_forum_mark_as_read');
        });
    },

    /**
     * Отметить письмо как прочитанное/непрочитанное
     *
     * @param eventSender Создатель события
     * @param route       Название роута
     */
    doActionWithMessage: function (eventSender, route) {
        $.ajax({
            url: Routing.generate(route),
            dataType: 'json',
            data: {
                id: eventSender.parents('tr:first').data('id'),
            },
            success: function (data) {
                if (data.success) {
                    window.location.reload();
                }
            },
            fail: function () {
                bootbox.alert('Что-то пошло не так. Пожалуйста, повторите ваше действие.');
            },
        });
    },

    /**
     * Удалить сообщение для текущего пользователя (если корзина - стереть)
     *
     * @param eventSender Создатель события
     */
    removeMessageForUser: function (eventSender) {
        const id = parseInt(eventSender.parents('tr:first').data('id'), 10);
        const type = eventSender.parents('tr:first').data('type');

        bootbox.confirm({
            buttons: {
                confirm: {
                    label: 'Да',
                    className: 'btn-primary',
                },
                cancel: {
                    label: 'Нет',
                    className: 'btn-danger',
                },
            },
            message: 'Вы действительно хотите удалить сообщение?',
            callback: function (res) {
                if (res === true) {
                    $.ajax({
                        url: Routing.generate('irmag_forum_message_remove'),
                        data: {
                            id: id,
                            type: type,
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data.success) {
                                window.location.reload();
                            }
                        },
                        fail: function () {
                            bootbox.alert('Не удаётся удалить сообщение. Попробуйте ещё раз или обратитесь к администратору.');
                        },
                    });
                }
            },
        });
    },
};
