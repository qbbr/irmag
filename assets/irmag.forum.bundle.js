import './forum/stylesheets/main.scss';

import './forum/IRMAG.Forum';
import './forum/IRMAG.Forum.Message';
import './forum/IRMAG.Forum.Messagelist';
import './forum/IRMAG.Forum.Post';
import './forum/IRMAG.Forum.Profile';
import './forum/IRMAG.Forum.Subscription';
import './forum/IRMAG.Forum.Topic';
import './forum/IRMAG.Forum.Visitor';
