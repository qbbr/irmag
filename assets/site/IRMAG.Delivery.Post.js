/**
 * Расчёт доставки Почтой России.
 *
 * @namespace IRMAG.Delivery
 *
 * @requires jquery
 * @requires bootbox
 * @requires Routing
 */

import Routing from '../core/modules/routing';

IRMAG.Delivery.Post = {

    calculatorRoute: 'irmag_get_post_delivery_form',

    init: function () {
        this.bindOpenDeliveryCalculatorWindow();
        this.bindSanitizeIntegerInput();
        this.bindCalculateDeliveryCost();
        this.bindResetCalculations();
        this.bindResetCalculationsFromStatusBar();
        this.bindSelect();
    },

    /**
     * Загружает форму калькулятора.
     */
    bindOpenDeliveryCalculatorWindow: function () {
        const self = this;

        $(document).off('click', '.select-post');
        $(document).on('click', '.select-post', function () {
            const weight = $(this).data('weight');

            $.fn.modal.Constructor.prototype.enforceFocus = function () {};
            IRMAG.Delivery.ajaxLoadCalculator(self.calculatorRoute, { weight: weight });
        });
    },

    /**
     * Санитизация ввода данных индекса (только целые числа, не более 6 символов).
     */
    bindSanitizeIntegerInput: function () {
        const self = this;

        $(document).on('keydown', '#post_calculator_indexTo', function (e) {
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 110, 190]) !== -1
                || (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true))
                || (e.keyCode >= 35 && e.keyCode <= 40)) {
                return;
            }

            if (e.keyCode === 13) {
                e.preventDefault();
                self.calculate();
            }

            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });

        $(document).on('keyup change input paste', '#post_calculator_indexTo', function () {
            if ($(this).val().length > 6) {
                $(this).val($(this).val().substring(0, 6));
            }
        });
    },

    /**
     * Запрос на стоимость отправки.
     */
    bindCalculateDeliveryCost: function () {
        const self = this;

        $(document).off('click', '.post-delivery-calculator-submit');
        $(document).on('click', '.post-delivery-calculator-submit', function () {
            self.calculate();
        });
    },

    /**
     * Сбрасывает результаты расчёта.
     */
    bindResetCalculations: function () {
        $(document).off('click', '.post-delivery-reset');
        $(document).on('click', '.post-delivery-reset', function (e) {
            e.preventDefault();
            $.ajax({
                url: Routing.generate('irmag_reset_post_delivery_data'),
                success: function (data) {
                    if (data.success) {
                        $('#post_calculator_indexTo').val('');
                        $('.post-delivery-results').hide(400);
                    }
                },
            });
        });
    },

    /**
     * Сбрасывает результаты расчёта с подтверждением.
     */
    bindResetCalculationsFromStatusBar: function () {
        $(document).off('click', '.post-delivery-reset-statusbar');
        $(document).on('click', '.post-delivery-reset-statusbar', function (e) {
            e.preventDefault();
            bootbox.confirm({
                buttons: {
                    confirm: {
                        label: 'Да',
                        className: 'btn-primary',
                    },
                    cancel: {
                        label: 'Нет',
                        className: 'btn-danger',
                    },
                },
                message: 'Сбросить все рассчитанные данные?',
                callback: function (res) {
                    if (res === true) {
                        $.ajax({
                            dataType: 'json',
                            url: Routing.generate('irmag_reset_post_delivery_data'),
                            success: function () {
                                window.location.reload(true);
                            },
                            error: function () {
                                bootbox.alert('Что-то пошло не так. Пожалуйста, попробуйте ещё раз.');
                            },
                        });
                    }
                },
            });
        });
    },

    /**
     * Выбирает предложенный вариант доставки.
     */
    bindSelect: function () {
        $(document).off('click', '.post-delivery-select');
        $(document).on('click', '.post-delivery-select', function (e) {
            e.preventDefault();
            $.ajax({
                url: Routing.generate('irmag_select_post_delivery'),
                success: function (data) {
                    if (data.success) {
                        window.location.reload(true);
                    }
                },
            });
        });
    },

    /**
     * Включает/выключает спиннер загрузки.
     *
     * @param {boolean} flag
     */
    toggleSpinner: function (flag) {
        const $deliverySpinner = $('#delivery-spinner-simple');

        if (true === flag) {
            $deliverySpinner.siblings('.panel:first').addClass('block-loading');
            $deliverySpinner.show();
        } else {
            $deliverySpinner.siblings('.panel:first').removeClass('block-loading');
            $deliverySpinner.hide();
        }
    },

    /**
     * Рассчитать стоимость доставки.
     */
    calculate: function () {
        const self = this;
        const weight = $('#post_calculator_weight').val();

        $.ajax({
            url: Routing.generate('irmag_get_post_delivery_cost'),
            data: {
                weight: weight,
                indexTo: $('#post_calculator_indexTo').val(),
            },
            beforeSend: function () {
                self.toggleSpinner(true);
            },
            success: function (response) {
                self.toggleSpinner(false);

                if (!response.success) {
                    if (response.message) {
                        bootbox.alert(response.message);
                    } else {
                        bootbox.alert('Не удалось рассчитать стоимость доставки. Пожалуйста, попробуйте ещё раз.');
                    }
                } else {
                    bootbox.hideAll();
                    $.fn.modal.Constructor.prototype.enforceFocus = function () {};
                    IRMAG.Delivery.ajaxLoadCalculator(self.calculatorRoute, { weight: weight });
                }
            },
            error: function () {
                self.toggleSpinner(false);
                bootbox.alert('Во время расчёта стоимости произошла ошибка. Пожалуйста, попробуйте ещё раз.');
            },
        });
    },
};
