/**
 * Фильтр каталога.
 *
 * @namespace IRMAG.Catalog.Filter
 *
 * @requires jquery
 * @requires Cookies
 * @requires bootstrap-slider
 */

import log from '../core/modules/logger';

const LC_TEXT_PICK = 'Выбрать';
const LC_TEXT_LOADING = 'Загрузка';

IRMAG.Catalog.Filter = {
    VISIBLE_COOKIE_STATE_NAME: 'catalog_filter_visible',

    xhr: null,

    init: function () {
        log('[Catalog] [Filter] init');

        this.$wrapper = $('#catalog-filter-wrapper');

        if (0 === this.$wrapper.length) {
            log('[Catalog] [Filter] [Error] filter wrapper not found, init exit 0');
            return;
        }

        this.$filter = this.$wrapper.find('.catalog-filter');
        this.$fakeFocus = this.$filter.find('.filter-fake-focus');
        this.$toggleBtn = $('.filter-toggle-btn');
        this.$form = this.$filter.find('form[name="filter"]');

        this.bindToggleBtn();
        this.bindSelect();
        this.bindSlider();
        this.bindOnChange();
    },

    /**
     * Показать/скрыть фильтр
     */
    bindToggleBtn: function () {
        const self = this;

        self.$toggleBtn.on('click', function () {
            if ($(this).hasClass('active')) {
                self.close();
            } else {
                self.open();
            }
        });
    },

    /**
     * Открыть фильтр
     */
    open: function () {
        const self = this;

        Cookies.set(this.VISIBLE_COOKIE_STATE_NAME, '1');

        self.$wrapper.stop(true).slideDown(function () {
            self.$toggleBtn
                .addClass('active')
                .html(self.$toggleBtn.html().replace('Открыть', 'Закрыть'))
            ;
        });
    },

    /**
     * Скрыть фильтр
     */
    close: function () {
        const self = this;

        Cookies.remove(this.VISIBLE_COOKIE_STATE_NAME);

        self.$wrapper.stop(true).slideUp(function () {
            self.$toggleBtn
                .removeClass('active')
                .html(self.$toggleBtn.html().replace('Закрыть', 'Открыть'))
            ;
        });
    },

    /**
     * Бренд / страна (irmag-filter-select)
     */
    bindSelect: function () {
        const self = this;
        const $filterSelects = self.$filter.find('.irmag-filter-select');

        if (0 === $filterSelects.length) {
            return;
        }

        log('[Catalog] [Filter] [select] init');

        $filterSelects.each(function () {
            const $filterSelect = $(this);
            const $resetBtn = $filterSelect.parent().find('.reset-selected');
            const elementsToEmpty = $filterSelect.data('elements-to-empty');

            // clear div wrapper
            function clearElms() {
                for (const i in elementsToEmpty) {
                    $('#filter_' + elementsToEmpty[i]).empty();
                }
            }

            // click a
            $filterSelect.on('click', 'ul li a', function () {
                const id = $(this).data('id');
                const mappedTo = $(this).data('mapped-to');

                const $li = $(this).parent();
                $li.parent().find('.active').removeClass('active');
                $li.addClass('active');

                $filterSelect.find('.selected-text').text($(this).text());

                clearElms();

                // add input (collections)
                $('#filter_' + mappedTo)
                    .append('<input type="hidden" name="filter[' + mappedTo + '][0]" value="' + id + '">');

                $filterSelect.find('.dropdown-toggle').removeClass('btn-default').addClass('btn-success');
                $resetBtn.removeClass('hidden');
                self.$form.trigger('change');
                $filterSelect.find('.dropdown').removeClass('open');

                self.fakeFocus();

                // clear fast-search
                $filterSelect.find('.fast-search input').val('').trigger('change');

                return false;
            });

            $filterSelect.find('ul.dropdown-menu li.active a').trigger('click');

            // fast-search
            $filterSelect.find('.fast-search input').on('change keyup', function () {
                const searchQuery = $(this).val();
                const $liList = $filterSelect.find('ul.inner li');

                if (!searchQuery) {
                    $liList.show();

                    return;
                }

                const $liListShow = $liList.has('a:Contains("' + searchQuery + '")').show();
                $liList.not($liListShow).hide();
            });

            // reset btn
            $resetBtn.on('click', function () {
                clearElms();
                $filterSelect.find('.selected-text').text(LC_TEXT_PICK);
                $filterSelect.find('li.active').removeClass('active');
                $filterSelect.find('.dropdown-toggle').removeClass('btn-success').addClass('btn-default');
                $(this).addClass('hidden');
                self.$form.trigger('change');
                self.fakeFocus();
            });

            // scroll to selected
            $filterSelect.find('.dropdown').on('shown.bs.dropdown', function () {
                let $elm = $(this).find('.dropdown-menu li.active a');

                if (0 === $elm.length) {
                    $elm = $(this).find('.dropdown-menu .fast-search input').focus();
                }

                $elm.focus();
            });
        });
    },

    /**
     * Слайдер цен
     */
    bindSlider: function () {
        const self = this;
        const slider = $('#price-slider').slider();

        const inputStart = $(slider.data('input-start-id'))
            // bind input start price on change
            .on('change', function () {
                slider.slider('setValue', [
                    parseInt($(this).val(), 10),
                    slider.slider('getValue')[1],
                ]);
            });

        const inputEnd = $(slider.data('input-end-id'))
            // bind input end price on change
            .on('change', function () {
                slider.slider('setValue', [
                    slider.slider('getValue')[0],
                    parseInt($(this).val(), 10),
                ]);
            });

        // bind slider on change
        slider.on('change', function (ev) {
            inputStart.val(ev.value.newValue[0]);
            inputEnd.val(ev.value.newValue[1]);
            self.fakeFocus();
        });
    },

    /**
     * Хак для отправки формы по нажатию на Enter
     */
    fakeFocus: function () {
        this.$fakeFocus.focus();
    },

    /**
     * При изменение формы, показываем кол-во
     */
    bindOnChange: function () {
        const self = this;
        const formDataInit = self.$form.serialize();
        const $btnSubmit = self.$form.find('button.do-filter');
        const btnDefaultHtml = $btnSubmit.html();

        let filterTimer;
        const filterTimerDelay = 300;

        self.$form.on('change', function () {
            const formData = self.$form.serialize();

            if (formData !== formDataInit) {
                // загрузка
                $btnSubmit
                    .html('<i class="fa fa-fw fa-refresh fa-pulse"></i> ' + LC_TEXT_LOADING)
                    .prop('disabled', true);

                // очищаем timer
                if (null !== filterTimer) {
                    clearTimeout(filterTimer);
                }

                // timer
                filterTimer = setTimeout(function () {
                    self.getFilterResultsCount(function (data) {
                        const count = parseInt(data.count, 10);

                        $btnSubmit.html(btnDefaultHtml + ' (' + count + ')');

                        if (count > 0) {
                            // есть товар у такой фильтрации
                            $btnSubmit.prop('disabled', false);
                        } else {
                            // товара нет
                            $btnSubmit.prop('disabled', true);
                        }
                    });
                }, filterTimerDelay);
            } else {
                // форма не изменилась,
                // отменяем xhr
                self.xhrAbort();
                // и устанавливаем значения по-умолчанию у кнопки
                $btnSubmit
                    .html(btnDefaultHtml);
            }
        });
    },

    /**
     * Получить кол-во фильтрованных товаров
     *
     * @param {function} callback [optional]
     */
    getFilterResultsCount: function (callback) {
        const url = '?' + this.$form.serialize();

        // abort current xhr
        this.xhrAbort();

        // new xhr
        this.xhr = $.ajax({
            url: url,
            data: {
                'filter-results-count': true,
            },
            success: function (data) {
                if ($.isFunction(callback)) {
                    callback(data);
                }
            },
        });
    },

    /**
     * Отменить текущий ajax запрос
     */
    xhrAbort: function () {
        if (this.xhr && this.xhr.readyState !== 4) {
            this.xhr.abort();
        }
    },
};
