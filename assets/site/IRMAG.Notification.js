/**
 * @namespace IRMAG.Notification
 */

import { ajax } from '../core/modules/irmag-utils';

IRMAG.Notification = {
    init: function () {
        this.bindClick();
        this.bindMaskAsReadBtn();
        this.bindMarkAllAsReadBtn();
    },

    bindClick: function () {
        $('.notification-table a').on('click', function () {
            const $a = $(this);
            const id = $a.parents('tr').data('id');

            ajax('irmag_profile_notification_mark_as_read', { id: id }, function () {
                window.location.href = $a.attr('href');
            });
        });
    },

    bindMaskAsReadBtn: function () {
        $('.notification-table .mark-as-read-btn').on('click', function () {
            const $btn = $(this);
            const $tr = $btn.parents('tr');
            const id = $tr.data('id');

            ajax('irmag_profile_notification_mark_as_read', { id: id }, function (data) {
                if (data.success) {
                    $tr
                        .addClass('text-muted')
                        .find('.fa-info-circle').remove();
                    $btn.remove();
                    $('.tooltip').remove();
                    const $counter = $('.notification-bell .badge');
                    $counter.html(Number($counter.html()) - 1);
                }
            });
        });
    },

    bindMarkAllAsReadBtn: function () {
        $('.mark-all-as-read-btn').on('click', function () {
            ajax('irmag_profile_notification_mark_all_as_read', {}, function (data) {
                if (data.success) {
                    window.location.reload(true);
                }
            });
        });
    },
};
