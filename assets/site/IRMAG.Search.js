/**
 * IRMAG.Search - Поиск с автодополнением.
 *
 * @namespace IRMAG.Search
 *
 * @requires jquery
 * @requires typeahead
 * @requires Bloodhound
 * @requires Handlebars
 * @requires Routing
 * @requires Quagga
 * @requires bootbox
 * @requires Cookies
 */

import Config from '../core/modules/irmag-config';
import Routing from '../core/modules/routing';
import log from '../core/modules/logger';
import { ajax } from '../core/modules/irmag-utils';

import elementTmpl from './templates/search_element.handlebars';
import sectionTmpl from './templates/search_section.handlebars';
import brandTmpl from './templates/search_brand.handlebars';
import countryTmpl from './templates/search_country.handlebars';
import actionTmpl from './templates/search_action.handlebars';

/** timeouts */
const rateLimitWaitElement = 600;
const rateLimitWaitOther = 800;

IRMAG.Search = {
    BARCODE_SCANNER_COOKIE_STATE_NAME: 'barcode_scanner_disabled',

    $input: null,
    $loading: null,

    init: function () {
        this.$input = $('#irmag-search');
        this.$loading = $('#irmag-search-loading');

        if (0 === this.$input.length) {
            return;
        }

        this.initTypeahead();
    },

    initBarcodeScanner: function () {
        navigator.getUserMedia = navigator.getUserMedia || navigator.mozGetUserMedia || navigator.webkitGetUserMedia;

        if (navigator.getUserMedia
            && typeof navigator.mediaDevices.getUserMedia === 'function'
            && '1' !== Cookies.get(this.BARCODE_SCANNER_COOKIE_STATE_NAME)
        ) {
            this.enableBarcodeScanner();
        }
    },

    initTypeahead: function () {
        const self = this;

        self.$input.typeahead({
            hint: true,
            highlight: true,
            minLength: 3,
        },
        {
            name: 'element',
            source: this.getElementSource(),
            display: 'name',
            limit: 100, // max results, do not change. control it from backend.
            templates: {
                notFound: '<div class="alert text-danger p-5">Товары не найдены &#9785;</div>',
                suggestion: elementTmpl,
            },
        },
        {
            name: 'section',
            source: this.getSectionSource(),
            display: 'name',
            async: true,
            templates: {
                header: '<hr>',
                suggestion: sectionTmpl,
            },
        },
        {
            name: 'brand',
            source: this.getBrandSource(),
            display: 'name',
            async: true,
            templates: {
                header: '<hr>',
                suggestion: brandTmpl,
            },
        },
        {
            name: 'country',
            source: this.getCountrySource(),
            display: 'name',
            async: true,
            templates: {
                header: '<hr>',
                suggestion: countryTmpl,
            },
        },
        {
            name: 'action',
            source: this.getActionSource(),
            display: 'title',
            async: true,
            templates: {
                header: '<hr>',
                suggestion: actionTmpl,
            },
        })
        .on('typeahead:select', function (ev, suggestion) {
            if (suggestion.url) {
                window.location.href = suggestion.url;
                ev.stopPropagation();
            }
        })
        .on('typeahead:asyncrequest', function () {
            self.$loading.removeClass('hidden');
        })
        .on('typeahead:asynccancel typeahead:asyncreceive', function () {
            self.$loading.addClass('hidden');
        });
    },

    getElementSource: function () {
        return new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: Routing.generate('irmag_catalog_search_element', {
                    searchQuery: '__QUERY__',
                }, true),
                wildcard: '__QUERY__',
                cache: !Config.debug,
                rateLimitWait: rateLimitWaitElement,
            },
        });
    },

    getSectionSource: function () {
        return new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: Routing.generate('irmag_catalog_search_section', {
                    searchQuery: '__QUERY__',
                }, true),
                wildcard: '__QUERY__',
                cache: !Config.debug,
                rateLimitWait: rateLimitWaitOther,
            },
        });
    },

    getBrandSource: function () {
        return new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: Routing.generate('irmag_catalog_search_brand', {
                    searchQuery: '__QUERY__',
                }, true),
                wildcard: '__QUERY__',
                cache: !Config.debug,
                rateLimitWait: rateLimitWaitOther,
            },
        });
    },

    getCountrySource: function () {
        return new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: Routing.generate('irmag_catalog_search_country', {
                    searchQuery: '__QUERY__',
                }, true),
                wildcard: '__QUERY__',
                cache: !Config.debug,
                rateLimitWait: rateLimitWaitOther,
            },
        });
    },

    getActionSource: function () {
        return new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('title'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: Routing.generate('irmag_catalog_search_action', {
                    searchQuery: '__QUERY__',
                }, true),
                wildcard: '__QUERY__',
                cache: !Config.debug,
                rateLimitWait: rateLimitWaitOther,
            },
        });
    },

    enableBarcodeScanner: function () {
        log('[barcode-scanner] init');

        const self = this;
        const btn = $('#barcode-scanner-btn').removeClass('hidden');
        let lastCode = 0;

        btn.on('click', function () {
            const dialog = bootbox.alert({
                title: 'IRMAG Rainbow Barcode Scanner (beta)',
                message: '<div id="camera-target" class="camera-target"></div>',
                show: false,
                className: 'modal-camera-wrapper',
            });

            dialog
                .on('shown.bs.modal', function () {
                    log('[barcode-scanner]', 'modal show');

                    Quagga.init({
                        inputStream: {
                            name: 'Live',
                            type: 'LiveStream',
                            target: document.querySelector('#camera-target'),
                            size: 640,
                            constraints: {
                                width: { min: 640 },
                                height: { min: 480 },
                                aspectRatio: { min: 1, max: 100 },
                                facingMode: 'environment', // or 'user' for the front camera
                            },
                        },
                        locator: {
                            patchSize: 'small',
                            halfSample: false,
                        },
                        numOfWorkers: (navigator.hardwareConcurrency ? navigator.hardwareConcurrency : 4),
                        decoder: {
                            readers: [{
                                format: 'ean_reader',
                                config: {},
                            }],
                        },
                        frequency: 10,
                        locate: true,
                    }, function (err) {
                        if (err) {
                            dialog.modal('hide');
                            btn.hide();
                            Cookies.set(self.BARCODE_SCANNER_COOKIE_STATE_NAME, 1);
                            bootbox.alert(err);

                            return;
                        }

                        log('[barcode-scanner]', 'start');
                        self._normaliseBootbox(dialog);
                        Quagga.start();
                    });

                    Quagga.onProcessed(function (result) {
                        const drawingCtx = Quagga.canvas.ctx.overlay;
                        const drawingCanvas = Quagga.canvas.dom.overlay;

                        if (result) {
                            if (result.boxes) {
                                drawingCtx.clearRect(0, 0, parseInt(drawingCanvas.getAttribute('width'), 10), parseInt(drawingCanvas.getAttribute('height'), 10));
                                result.boxes.filter(function (box) {
                                    return box !== result.box;
                                }).forEach(function (box) {
                                    Quagga.ImageDebug.drawPath(box, { x: 0, y: 1 }, drawingCtx, { color: 'green', lineWidth: 2 });
                                });
                            }

                            if (result.box) {
                                Quagga.ImageDebug.drawPath(result.box, { x: 0, y: 1 }, drawingCtx, { color: 'blue', lineWidth: 2 });
                            }

                            if (result.codeResult && result.codeResult.code) {
                                Quagga.ImageDebug.drawPath(result.line, { x: 'x', y: 'y' }, drawingCtx, { color: 'red', lineWidth: 3 });
                            }
                        }
                    });

                    Quagga.onDetected(function (result) {
                        const code = String(result.codeResult.code);

                        log('[barcode-scanner][code]:', code);

                        if (lastCode === code
                            && (8 === code.length || 13 === code.length)
                        ) {
                            log('[barcode-scanner][code][ok]:', code);
                            dialog.modal('hide');
                            ajax('irmag_get_element_id_by_barcode', { barcode: code }, function (data) {
                                if (data.id) {
                                    window.location.href = Routing.generate('irmag_catalog_element', { id: data.id }) + '#jmp-to-buy-me';
                                } else {
                                    self.$input.typeahead('val', code).typeahead('open');
                                }
                            });
                            lastCode = 0;
                        } else {
                            lastCode = code;
                        }
                    });
                })
                .on('hidden.bs.modal', function () {
                    log('[barcode-scanner]', 'stop');
                    Quagga.stop();
                    log('[barcode-scanner]', 'modal remove');
                    dialog.remove();
                })
            ;

            dialog.modal('show');
        });
    },

    /**
     * @private
     *
     * @param $bootbox
     */
    _normaliseBootbox($bootbox) {
        const $bootboxBody = $bootbox.find('.bootbox-body');
        const $cameraTarget = $bootboxBody.find('.camera-target');
        const $video = $cameraTarget.find('video');

        const scale = $bootboxBody.width() / $video.width();

        $cameraTarget.css('transform', 'scale(' + scale + ')');
        $bootboxBody.css('height', $video.height() * scale);

        // fix center and show video
        $cameraTarget.css({
            'margin-left': ($bootboxBody.width() - $video.width() * scale) / 2,
            visibility: 'visible',
        });
    },
};
