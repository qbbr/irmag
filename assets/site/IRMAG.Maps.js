/**
 * Google Maps JS wrapper.
 *
 * @namespace IRMAG.Maps
 *
 * @requires jquery
 * @requires Google Maps JavaScript API v3+
 * @requires bootbox
 */

import error from '../core/modules/errorbox';
import log from '../core/modules/logger';
import irmagMarketImage from './images/irmag-market.png';
import irmagLogoImage from './images/irmag-logo.svg';

IRMAG.Maps = {
    IRMAG_PLACE_ID: 'ChIJqweWVsclqF0RfQSuqdLchnw',
    CONTAINER_ID: 'google-maps-container',

    map: null,
    infowindowIrmag: null,

    init: function (containerId) {
        containerId = containerId || this.CONTAINER_ID;
        log('[Maps]', 'init', containerId);
        const container = document.getElementById(this.CONTAINER_ID);

        if (!container) {
            log('[Maps]', 'container not found, skipped.');
            return;
        }

        this.map = new google.maps.Map(container, {
            center: new google.maps.LatLng(52.3336096, 104.210494),
            zoom: 16,
            panControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            styles: [{
                stylers: [{
                    saturation: -60,
                }],
            }],
        });

        this.drawIrmagMarket(this.map, this.IRMAG_PLACE_ID);

        const centerControlDiv = document.createElement('div');
        this.drawNavigateBtn(this.map, centerControlDiv);
        centerControlDiv.index = 1;
        this.map.controls[google.maps.ControlPosition.TOP_CENTER].push(centerControlDiv);
    },

    drawNavigateBtn: function (map, div) {
        const controlUI = document.createElement('div');
        controlUI.className = 'irmag-map-navigate-btn';
        div.appendChild(controlUI);

        const controlText = document.createElement('div');
        controlText.className = 'btn-text';
        controlText.innerHTML = '<i class="fa fa-car"></i> Проложить маршрут';
        controlUI.appendChild(controlText);

        const self = this;

        controlUI.addEventListener('click', function () {
            self.confirmNavigation();
        });
    },

    confirmNavigation: function () {
        if ('undefined' === typeof navigator.geolocation) {
            error('Ваше устройство не поддерживает геолокацию!');

            return;
        }

        const self = this;

        bootbox.confirm('<i class="fa fa-car"></i> Проложить маршрут до <b>IRMAG</b>?', function () {
            self.navigateToIrmag();
        });
    },

    navigateToIrmag: function () {
        const self = this;
        const infowindowIm = new google.maps.InfoWindow();
        infowindowIm.setMap(this.map);
        const directionsDisplay = new google.maps.DirectionsRenderer();
        directionsDisplay.setMap(this.map);
        const directionsService = new google.maps.DirectionsService();

        // geo
        navigator.geolocation.getCurrentPosition(function (position) {
            const pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            };
            infowindowIm.setPosition(pos);
            infowindowIm.setContent('Это вы ;)');
            // map.setCenter(pos);

            directionsService.route({
                origin: pos,
                destination: self.place.geometry.location,
                travelMode: google.maps.DirectionsTravelMode.DRIVING,
            }, function (response, status) {
                if (status === google.maps.DirectionsStatus.OK) { // hardcoded status
                    directionsDisplay.setDirections(response);
                    infowindowIm.setContent([
                        '<h4>Это вы</h4>',
                        '<div>',
                            '<i class="fa fa-car"></i>: ',
                            response.routes[0].legs[0].distance.text,
                            '~',
                            response.routes[0].legs[0].duration.text,
                        '</div>',
                    ].join('\n'));

                    self.infowindowIrmag.close();
                } else {
                    error('Directions request failed due to ' + status);
                }
            });
        }, function () {
            error('Не удалось получить Ваше местоположение, что-то пошло не так.');
        });
    },

    drawIrmagMarket: function (map, placeId) {
        const self = this;
        self.infowindowIrmag = new google.maps.InfoWindow();
        const service = new google.maps.places.PlacesService(map);

        service.getDetails({ placeId: placeId }, function (place, status) {
            self.place = place; // global

            if (status !== google.maps.places.PlacesServiceStatus.OK) {
                return;
            }

            const marker = new google.maps.Marker({
                map: map,
                position: place.geometry.location,
                icon: {
                    url: irmagMarketImage,
                    // size: new google.maps.Size(61, 50),
                    // origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(28, 50),
                },
            });

            self.infowindowIrmag.setContent([
                '<div class="text-center text-sm">',
                    '<img src="' + irmagLogoImage + '" class="mb-5">',
                    '<div class="text-muted">',
                        'С понедельника по пятницу: <b>09.00 – 18.00</b>',
                        '<br>',
                        'Суббота: <b>09.00 – 12.00</b>',
                        '<br>',
                        'Воскресенье: <b>Выходной</b>',
                    '</div>',
                    '<div class="mt-5">', place.formatted_address, '</div>',
                '</div>',
            ].join('\n'));

            self.infowindowIrmag.open(map, marker);

            google.maps.event.addListener(marker, 'click', function () {
                self.infowindowIrmag.open(map, this);
            });
        });
    },
};
