/**
 * @namespace IRMAG.Site
 *
 * @requires jquery
 * @requires clipboard
 * @requires ResponsiveBootstrapToolkit
 */

import log from '../core/modules/logger';
import './components/scroller';
import './images/irmag-heart-colored.png';

IRMAG.Site = {
    _isDesktop: null,

    init: function () {
        if (this.isDesktop()) { // md, lg
            // IRMAG.Catalog.Menu.init();
            this.affixHeaderBlueLine();
        } else { // xs, sm
            this.affixHeaderBlueLine(76);
        }

        this.bindCollapseGroup();
        this.bindTelephones();
        this.bindTooltips();
        this.bindScrollTo();
        this.bindMiniBasketFavorite();

        IRMAG.Promocode.bindActivatePromocode();
        IRMAG.Promocode.bindDeactivatePromocode();

        IRMAG.Search.init();
        IRMAG.Catalog.bindExpandableNames();
        IRMAG.Basket.bindAddElementActionBtn();
        IRMAG.Favorite.bindAddElementActionBtn();

        this.bindScrollToError(); // must as EOL;
    },

    isDesktop: function () {
        if (null === this._isDesktop) {
            this._isDesktop = ResponsiveBootstrapToolkit.is('>sm');
        }

        return this._isDesktop;
    },

    bindMiniBasketFavorite: function () {
        const $links = $('a.basket, a.favorite');

        if (this.isDesktop()) { // md, lg
            $links
                .on('mouseenter', function () {
                    if ($(this).hasClass('non-empty')) {
                        const offset = $('#header-blue-line > .container').offset().left + 30;

                        $($(this).data('target'))
                            .css('right', offset)
                            .addClass('active');
                    }
                })
                .on('mouseleave', function () {
                    $($(this).data('target')).removeClass('active');
                })
                .on('click', function (e) {
                    e.stopPropagation();
                })
            ;
        } else {
            $links
                .attr('href', 'javascript:;')
                .on('click', function () {
                    $($(this).data('target')).collapse('toggle');
                })
            ;

            // catalog-btn
            $('a.catalog-btn').on('click', function (e) {
                // close opened mini favorite/basket
                $('[data-collapse-group="header-basket-favorite-info"]:not(.collapsed)').each(function () {
                    // disable animation
                    $($(this).addClass('collapsed').data('target')).removeClass('in').collapse('hide');
                });

                const sectionList = $('.section-list:first');

                if (0 !== sectionList.length) {
                    $('html, body').animate({
                        scrollTop: sectionList.offset().top - 74, // filter btn + 20px space
                    }, 'normal');

                    $(this).blur();
                    e.preventDefault();
                }
            });
        }
    },

    // fixed nav
    affixHeaderBlueLine: function (plusTop = 0) {
        $('#header-blue-line')
            .on('affix.bs.affix', function () {
                $('#header').css('height', $('#header').outerHeight(true));

                $(this).css('top', 0);
            })
            .affix({
                offset: {
                    top: function () {
                        return $('#header > .navbar-irmag').outerHeight(true) + plusTop;
                    },
                },
            });

        // fix min-height on resize
        $(window).on('resize', function () {
            $('#header').css('height', '');
        });
    },

    // collapse group
    bindCollapseGroup: function () {
        $(document).on('click', '[data-collapse-group]', function () {
            $('[data-collapse-group="' + $(this).data('collapse-group') + '"]').not($(this)).each(function () {
                $($(this).data('target')).collapse('hide');
            });
        });
    },

    // bind telephone change on click
    bindTelephones: function () {
        $('.telephones span').on('click', function () {
            $(this).parent().children('span').toggleClass('hidden');
        });
    },

    // bootstrap tooltip
    bindTooltips: function () {
        $(document).tooltip({
            selector: '[data-toggle="tooltip"]',
            container: 'body',
        });
    },

    // bootstrap popover
    bindBootstrapPopover: function () {
        $('[data-toggle="popover"]').popover({
            trigger: 'hover',
            html: true,
            container: 'body',
        });
    },

    // scroll to
    bindScrollTo: function () {
        $(document).on('click', 'a[data-toggle="scroll-to"][href^="#"]', function (e) {
            e.preventDefault();
            const hash = this.hash;
            const offsetTop = parseInt($(this).data('offset-top'), 10) || 0;
            const addHash = !(offsetTop);

            setTimeout(function () {
                $('html, body').animate(
                    {
                        scrollTop: $(hash).offset().top - offsetTop,
                    },
                    300,
                    function () {
                        if (true === addHash) {
                            window.location.hash = hash;
                        }
                    },
                );
            }, 100);
        });
    },

    bindHeaderWarningLineCloseBtn: function () {
        $(document).on('click', '#header-warning-line-close-btn', function () {
            $(this).parents('.header-warning-line').fadeOut('fast', function () {
                $(this).remove();

                Cookies.set('header_warning_line_hidden', '1', {
                    expires: 0.5, // 12h
                    domain: '.' + document.domain.substring(document.domain.lastIndexOf('.', document.domain.lastIndexOf('.') - 1) + 1),
                });
            });

            $('.tooltip').remove();
            $('#header').css('height', '');
        });
    },

    /**
     * Если есть сообщения об ошибках, то "проскроливает" до первого сообщения.
     */
    bindScrollToError: function () {
        const $firstElm = $('form .alert-danger, form .help-block').first();

        if (0 === $firstElm.length) {
            return;
        }

        const $tab = $firstElm.parents('.tab-pane');

        if (0 !== $tab.length) {
            $('a[href="#' + $tab.attr('id') + '"]').tab('show');
        }

        log('[Form]', 'scroll to error message, elm:', $firstElm);

        $('html, body').animate({
            scrollTop: $firstElm.offset().top - 200,
        }, 'normal');
    },

    /**
     * @param btnIdSelector
     */
    bindClipboard: function (btnIdSelector = '#clipboard-btn') {
        new ClipboardJS(btnIdSelector).on('success', function (e) {
            const $owner = $(e.trigger).removeClass('btn-default').addClass('btn-success'); // btn
            $owner.parents('.input-group').addClass('has-success'); // .input-group
            const originalTitle = $owner.data('original-title');

            $owner
                .attr('title', 'Скопировано.').tooltip('fixTitle').tooltip('show')
                .on('hidden.bs.tooltip', function () {
                    $(this).attr('title', originalTitle).tooltip('fixTitle');
                });

            e.clearSelection();
        });
    },

};
