/**
 * Расчёт доставки транспортной компанией.
 *
 * @namespace IRMAG.Delivery.TransportCompany
 *
 * @requires jquery
 * @requires Routing
 * @requires select2
 * @requires bootbox
 */

import Routing from '../core/modules/routing';
import optionTmpl from './templates/city-option.handlebars';

IRMAG.Delivery.TransportCompany = {

    calculatorRoute: 'irmag_get_delivery_service_form',

    init: function () {
        this.bindDeliveryCalculatorWindow();
        this.initCalculatorModal();
        this.initTerminalSelectorModal();
    },

    /**
     * Выбор пунктов доставки.
     */
    initTerminalSelectorModal: function () {
        this.bindClickDeliveryTerminal();
        this.bindSelectDeliveryTerminal();
    },

    /**
     * Калькулятор доставки.
     */
    initCalculatorModal: function () {
        this.bindCalculateDeliveryCost();
        this.bindClickDeliveryVariant();
        this.bindSelectDeliveryVariant();
        this.bindReset();
    },

    /**
     * Инициализация калькулятора по клику на кнопку.
     */
    bindDeliveryCalculatorWindow: function () {
        const self = this;

        $(document).off('click', '.select-transport-company');
        $(document).on('click', '.select-transport-company', function () {
            const weight = $(this).data('weight');
            const volume = $(this).data('volume');

            IRMAG.Delivery.ajaxLoadCalculator(self.calculatorRoute, { weight: weight, volume: volume }, function () { self.initDefaultDeliveryServiceData(); });
            IRMAG.Site.bindTooltips();
        });
    },

    /**
     * Сброс расчёта.
     */
    bindReset: function () {
        // fix for ajax-updated order form
        $(document).off('click', '.delivery-service-reset');
        $(document).on('click', '.delivery-service-reset', function () {
            bootbox.confirm({
                buttons: {
                    confirm: {
                        label: 'Да',
                        className: 'btn-primary',
                    },
                    cancel: {
                        label: 'Нет',
                        className: 'btn-danger',
                    },
                },
                message: 'Сбросить все рассчитанные данные?',
                callback: function (res) {
                    if (res === true) {
                        $.ajax({
                            dataType: 'json',
                            url: Routing.generate('irmag_clear_delivery_data'),
                            success: function () {
                                window.location.reload(true);
                            },
                            error: function () {
                                bootbox.alert('Что-то пошло не так. Пожалуйста, попробуйте ещё раз.');
                            },
                        });
                    }
                },
            });
        });
    },

    /**
     * Выбор значения по умолчанию.
     */
    initDefaultDeliveryServiceData: function () {
        $('.delivery-service-service').find('option:first').attr('selected', true);
        this.initDeliveryPointSelector();
    },

    /**
     * Инициализация выбора пунктов доставки.
     */
    initDeliveryPointSelector: function () {
        const $selector = $('.delivery-service-point');
        const self = this;

        $selector
            .select2({
                placeholder: 'Введите название вашего населённого пункта',
                minimumInputLength: 2,
                allowClear: true,
                // width: '100%',
                ajax: {
                    url: Routing.generate('irmag_get_delivery_city'),
                    cache: true,
                    dataType: 'JSON',
                    method: 'GET',
                    delay: 250,
                    data: function (term) {
                        return {
                            term: term,
                            service: $('.delivery-service-service').val(),
                        };
                    },
                    results: function (data) {
                        if (data.success) {
                            return {
                                results: $.map(data.results, function (item) {
                                    return {
                                        text: item.name,
                                        id: item.id,
                                    };
                                }),
                            };
                        }
                    },
                },
            })
            .on('select2-selecting', function (event) {
                const chosenCity = event.choice;

                $('#calculator_selectedCity').val(JSON.stringify({
                    cityId: chosenCity.id,
                    cityName: chosenCity.text,
                }));

                $.ajax({
                    url: Routing.generate('irmag_save_delivery_city'),
                    data: {
                        service: $('.delivery-service-service').val(),
                        cityId: chosenCity.id,
                    },
                    success: function (data) {
                        if (!data.success) {
                            bootbox.alert('Что-то пошло не так. Пожалуйста, попробуйте выбрать город ещё раз');
                        }
                    },
                    error: function () {
                        bootbox.alert('Что-то пошло не так. Пожалуйста, попробуйте выбрать город ещё раз');
                    },
                });
            })
            .on('select2-removed', function () {
                self.ajaxHideCalculations();
            });

        const selectedCity = self.getSelectedCity();

        if (selectedCity.cityId !== '') {
            $selector
                .parent()
                .find('.select2-container .select2-choice')
                .removeClass('select2-default')
                .find('.select2-chosen')
                .replaceWith(optionTmpl(selectedCity));
        }
    },

    /**
     * Активирует кнопку выбора при выборе варианта.
     */
    bindClickDeliveryVariant: function () {
        $(document).on('click', '.delivery-service-radio', function () {
            $('.delivery-service-radio').prop('checked', false);
            $(this).prop('checked', true);
            $('button.btn-delivery-service-select').prop('disabled', false);
        });
    },

    /**
     * Выбирает вариант доставки.
     */
    bindSelectDeliveryVariant: function () {
        $(document).off('click', 'button.btn-delivery-service-select');
        $(document).on('click', 'button.btn-delivery-service-select', function () {
            const $selectedRadio = $('.delivery-service-radio:checked');

            $.ajax({
                url: Routing.generate('irmag_select_delivery_variant'),
                dataType: 'json',
                data: {
                    service: $('.delivery-service-dropdown').data('value'),
                    city: $('.delivery-service-point').val(),
                    type: $selectedRadio.data('type'),
                    variant: $selectedRadio.data('variant'),
                    variantFull: $selectedRadio.data('variant-full'),
                    cost: $selectedRadio.data('cost'),
                    days: $selectedRadio.data('days'),
                },
                success: function (data) {
                    if (data.success) {
                        // XXX: hardcode, подумать
                        if ('toSelfDelivery' === $selectedRadio.data('type')) {
                            const $calculator = $('.embed-delivery-calculator');

                            $.ajax({
                                url: Routing.generate('irmag_get_available_delivery_points'),
                                data: {
                                    weight: $calculator.data('weight'),
                                },
                                beforeSend: function () {
                                    $calculator.addClass('block-loading');
                                    $('#delivery-spinner-simple').show();
                                },
                                success: function (content) {
                                    if (content.empty) {
                                        window.location.reload(true);
                                    } else {
                                        $calculator.removeClass('block-loading');
                                        $('#delivery-spinner-simple').hide();

                                        bootbox.hideAll();
                                        bootbox.dialog({
                                            message: content,
                                            closeButton: true,
                                            className: 'delivery-modal',
                                        });

                                        IRMAG.Site.bindBootstrapPopover();
                                    }
                                },
                            });
                        } else {
                            window.location.reload(true);
                        }
                    } else {
                        bootbox.alert('Что-то пошло не так. Пожалуйста, попробуйте ещё раз.');
                    }
                },
                error: function () {
                    bootbox.alert('Что-то пошло не так. Пожалуйста, попробуйте ещё раз.');
                },
            });
        });
    },

    /**
     * Подсчитывает стоимость доставки.
     */
    bindCalculateDeliveryCost: function () {
        const self = this;
        $(document).off('click', '.btn-delivery-calculate');
        $(document).on('click', '.btn-delivery-calculate', function () {
            const $self = $(this);
            const selectedCity = self.getSelectedCity().cityId;
            const $calculator = $('.embed-delivery-calculator');

            $.ajax({
                url: Routing.generate('irmag_get_delivery_cost'),
                data: {
                    service: $('.delivery-service-service').val(),
                    city: selectedCity,
                    weight: $calculator.data('weight'),
                    volume: $calculator.data('volume'),
                },
                beforeSend: function () {
                    if (!selectedCity) {
                        bootbox.alert('Пожалуйста, укажите пункт доставки');

                        return false;
                    }

                    $self.parents('.panel:first').addClass('block-loading');
                    $('#delivery-spinner-extended').show();
                },
                success: function (data) {
                    if (data.success) {
                        $self.parents('.panel:first').removeClass('block-loading');
                        $('#delivery-spinner-extended').hide();

                        const $embedCalculator = $('.embed-delivery-calculator');
                        const weight = $embedCalculator.data('weight');
                        const volume = $embedCalculator.data('volume');

                        bootbox.hideAll();

                        $.fn.modal.Constructor.prototype.enforceFocus = function () {};
                        IRMAG.Delivery.ajaxLoadCalculator(self.calculatorRoute, { weight: weight, volume: volume }, function () { self.initDefaultDeliveryServiceData(); });
                    }
                },
                error: function () {
                    bootbox.alert('Что-то пошло не так. Пожалуйста, попробуйте подсчитать стоимость снова.');
                    $self.parents('.panel:first').removeClass('block-loading');
                    $('#delivery-spinner-extended').hide();
                },
            });
        });
    },

    /**
     * Активирует кнопку выбора при выборе пункта доставки.
     */
    bindClickDeliveryTerminal: function () {
        $(document).on('click', 'input[type="radio"]', function () {
            $('#terminal_select').prop('disabled', false);
        });
    },

    /**
     * Выбирает пункт доставки.
     */
    bindSelectDeliveryTerminal: function () {
        $(document).off('click', '#terminal_select');
        $(document).on('click', '#terminal_select', function () {
            const $selectedRadio = $('input[name^="terminal"][type="radio"]:checked');

            $.ajax({
                url: Routing.generate('irmag_set_delivery_terminal'),
                dataType: 'json',
                data: {
                    terminal: $selectedRadio.val(),
                    address: $selectedRadio.data('address'),
                },
                success: function (data) {
                    if (data.success) {
                        window.location.reload(true);
                    }
                },
                error: function () {
                    bootbox.alert('Что-то пошло не так. Пожалуйста, попробуйте ещё раз.');
                },
            });
        });
    },

    /**
     * Скрывает результаты расчёта и чистит сессию.
     */
    ajaxHideCalculations: function () {
        $.ajax({
            url: Routing.generate('irmag_clear_delivery_data'),
            success: function (data) {
                if (data.success) {
                    $('#delivery-results').hide(400);
                    $('.btn-delivery-service-select').remove();
                    $('.delivery-service-reset').remove();
                }
            },
        });
    },
    /**
     * Возвращает объект с данными о выбранном городе доставки.
     *
     * @return {{cityId: string, cityName: string}|jQuery}
     */
    getSelectedCity: function () {
        let selectedCity = $('#calculator_selectedCity').val();
        selectedCity = ('' !== selectedCity) ? JSON.parse(selectedCity) : { cityId: '', cityName: '' };

        return selectedCity;
    },
};
