/**
 * Баннеры на главной.
 *
 * @namespace IRMAG.IndexBanner
 *
 * @requires jquery
 * @requires slick
 */

import log from '../core/modules/logger';

IRMAG.IndexBanner = {
    banners: [],
    $container: null,
    $slick: null,

    init: function (banners) {
        log('[IndexBanner] init', banners);

        this.banners = banners;
        this.$container = $('#index-banners-container');
        this.$slick = this.$container.find('.slick-container');

        if (false === IRMAG.Site.isDesktop()) { // xs, sm
            this.adaptHeight();

            $(window).on('resize', function () {
                IRMAG.IndexBanner.adaptHeight();
            });
        }

        if (0 !== this.$slick.length) {
            this.initSlick();
        }
    },

    adaptHeight: function () {
        const defaultWidth = 1000;
        const defaultHeight = 450;

        let height = defaultHeight * $(window).width() / defaultWidth;

        if (height > defaultHeight) {
            height = defaultHeight;
        }

        this.$container.css('height', height);
    },

    initSlick: function () {
        const self = this;

        self.$slick
            .on('beforeChange', function (event, slick, currentSlide, nextSlide) {
                self.changeBannerBg(nextSlide);
            })
            .on('init', function () {
                self.$slick.css('visibility', 'visible');
                self.$container.slideDown();
            })
            .slick({
                infinite: false,
                dots: true,
                autoplay: true,
                autoplaySpeed: 20000,
                adaptiveHeight: true,
            })
        ;
    },

    /**
     * Меняет фон у баннера
     *
     * @param {integer} i
     */
    changeBannerBg: function (i) {
        const $bannerNow = this.$container.find('.banner-bg');
        const $bannerNew = $bannerNow.clone();
        $bannerNew.css('background-image', 'url(' + this.banners[i].banner_bg + ')');
        $bannerNow.parent().prepend($bannerNew);
        $bannerNow.fadeOut('slow', function () {
            $(this).remove();
        });
    },
};
