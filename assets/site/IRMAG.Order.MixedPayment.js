/**
 * Смешанный платёж.
 *
 * @namespace IRMAG.Order.MixedPayment
 *
 * @requires jquery
 */

IRMAG.Order.MixedPayment = {
    cashElm: '#order_mixedPayment_sumCash',
    cardElm: '#order_mixedPayment_sumCard',
    certificateElm: '#order_mixedPayment_sumCertificate',
    total: '#order_mixedPayment_total',

    init: function () {
        this.bindMixedPaymentOnChange();
        this.bindKeyPressFiltering();
    },

    /**
     * @param $input
     */
    handleInput: function ($input) {
        let inputValue = parseFloat($input.val());
        let valueToSet;

        if (!isNaN(inputValue) && inputValue >= 0) {
            const tag = $input.data('tag');

            if ('cash' === tag) {
                valueToSet = this.recalculateFields(inputValue, $input, $(this.cardElm), $(this.certificateElm), false);
                $(this.cashElm).val(valueToSet);
            } else if ('card' === tag) {
                valueToSet = this.recalculateFields(inputValue, $input, $(this.cashElm), $(this.certificateElm), false);
                $(this.cardElm).val(valueToSet);
            } else if ('cert' === tag) {
                inputValue = this.roundToNearest(inputValue, 500); // XXX: 500
                valueToSet = this.recalculateFields(inputValue, $input, $(this.cashElm), $(this.cardElm), true);
                $(this.certificateElm).val(valueToSet);
            }
        } else {
            $input.val(0);
        }
    },

    bindMixedPaymentOnChange: function () {
        const self = this;

        $(document).on('change', 'input[name^="order[mixedPayment]"]', function () {
            self.handleInput($(this));
        });
    },

    bindKeyPressFiltering: function () {
        if ($('input[name^="order[mixedPayment]"]').length > 0) {
            $('input[name^="order[mixedPayment]"]').keypress(function (event) {
                // не сабмиттим по энтеру
                if (event.which === 13) {
                    event.preventDefault();
                }
            });
        }
    },

    initMixedPayment: function () {
        const total = parseFloat($('#total-tip').text());

        $(this.total).val(total); // XXX: dd
        $(this.cashElm).val(total);
        $(this.cardElm).val(0);
        $(this.certificateElm).val(0);
    },

    /**
     * XXX: Wat? $eventElm - not used
     *
     * @param inputValue
     * @param $eventElm
     * @param $mainElm
     * @param $secondaryElm
     * @param forceInputValue
     *
     * @return {string}
     */
    recalculateFields: function (inputValue, $eventElm, $mainElm, $secondaryElm, forceInputValue) {
        const total = parseFloat($(this.total).val());
        const mainElementValue = parseFloat($mainElm.val());
        const secondaryElementValue = parseFloat($secondaryElm.val());
        const oldValue = total - mainElementValue - secondaryElementValue;
        forceInputValue = (typeof forceInputValue === 'undefined') ? false : forceInputValue;

        if (inputValue <= oldValue) {
            $mainElm.val((mainElementValue + (oldValue - inputValue)).toFixed(2));
        } else if (inputValue > oldValue && inputValue <= total) {
            if (true === forceInputValue) {
                if (inputValue > mainElementValue) {
                    $mainElm.val(0);
                    $secondaryElm.val((secondaryElementValue - inputValue + mainElementValue).toFixed(2));
                } else {
                    $mainElm.val((mainElementValue - inputValue).toFixed(2));
                }
            } else {
                const diff = inputValue - oldValue;

                if (diff >= mainElementValue) {
                    $mainElm.val(0);

                    return (oldValue + mainElementValue).toFixed(2);
                } if (mainElementValue > 0) {
                    $mainElm.val((mainElementValue - diff).toFixed(2));
                } else {
                    return oldValue.toFixed(2);
                }
            }
        } else {
            $secondaryElm.val(0);
            $mainElm.val(0);
        }

        const valueToSet = (inputValue > total) ? total : inputValue;

        return valueToSet.toFixed(2);
    },

    /**
     * XXX: use native `input[type="number"]` step, dd?
     *
     * @param number
     * @param multipleOf
     *
     * @return {number}
     */
    roundToNearest: function (number, multipleOf) {
        if (number > 0) {
            return Math.ceil(number / parseFloat(multipleOf)) * multipleOf;
        } if (number < 0) {
            return Math.floor(number / parseFloat(multipleOf)) * multipleOf;
        }
            return 0;
    },
};
