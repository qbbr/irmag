/**
 * Меню каталога.
 *
 * @namespace IRMAG.Catalog.Menu
 *
 * @requires jquery
 * @requires menuAim
 */

import log from '../core/modules/logger';

IRMAG.Catalog.Menu = {
    timerOpen: null,
    timerClose: null,
    timeoutOpen: 350,
    timeoutClose: 1000,

    init: function () {
        this.$menu = $('#catalog-menu');
        this.$menuBtn = $('#catalog-btn');

        this.bindMenu();
    },

    bindMenu: function () {
        log('[Catalog] [Menu]', 'init');
        const self = this;

        // кнопка каталога
        self.$menuBtn
            .on('mouseenter', function () {
                self.clearTimerClose();

                self.timerOpen = setTimeout(function () {
                    self.show();
                }, self.timeoutOpen);
            })
            .on('mouseleave', function () {
                self.clearTimerOpen();
                self.hide();
            });

        // меню
        self.$menu
            .on('mouseenter', function () {
                self.clearTimerClose();
            })
            .on('mouseleave', function () {
                self.hide();
            });

        // submenu
        self.$menu.find('> ul').menuAim({
            activate: function (row) {
                const $row = $(row).addClass('active');
                const $submenu = $row.find('.submenu');
                const styles = {
                    left: self.$menu.outerWidth() + 15,
                    'min-height': self.$menu.height(),
                    width: $('.container:first').outerWidth() - self.$menu.outerWidth() - 30,
                };

                if ($row.hasClass('brand-zones')) {
                    styles['max-height'] = styles['min-height'];
                }

                $submenu.css(styles);
                self.$menu.addClass('active-lvl-2');
            },
            deactivate: function (row) {
                $(row).removeClass('active');
                self.$menu.removeClass('active-lvl-2');
            },
        });
    },

    /**
     * Показывает меню
     */
    show: function () {
        this.$menu.addClass('active');
        this.$menuBtn.addClass('active');
        this.clearTimerClose();
    },

    /**
     * Скрывает меню
     */
    hide: function () {
        const self = this;

        this.timerClose = setTimeout(function () {
            self.$menu
                .removeClass('active active-lvl-2')
                .find('li.active').removeClass('active');

            self.$menuBtn.removeClass('active');
        }, this.timeoutClose);
    },

    /**
     * Очищает таймер открытия
     */
    clearTimerOpen: function () {
        clearTimeout(this.timerOpen);
    },

    /**
     * Очищает таймер скрытия
     */
    clearTimerClose: function () {
        clearTimeout(this.timerClose);
    },
};
