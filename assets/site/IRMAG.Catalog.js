/**
 * Каталог.
 *
 * @namespace IRMAG.Catalog
 *
 * @requires jquery
 * @requires Cookies
 */

import { bind, go } from '../core/modules/jumper';
import './images/not-found-image.jpg';

IRMAG.Catalog = {
    init: function () {
        if (false === IRMAG.Site.isDesktop()) { // xs, sm
            this.adaptBrandZone();

            $(window).on('resize', function () {
                IRMAG.Catalog.adaptBrandZone();
            });
        }

        this.Filter.init();
        this.bindPerpage();
        this.bindViewToggler();
        this.bindNextPageBtn();
        this.bindPaginationJumper();

        bind('catalog-content'); // for catalog-view-toggler
        IRMAG.Action.initCountdown(); // for oneRandomOfferAction
    },

    /**
     * Адаптирует бренд-зону под мобильное разрешение.
     */
    adaptBrandZone: function () {
        const _width = 1200;
        const _height = 430;
        const scale = $(window).outerWidth() / _width;

        $('#brand-zone-container-adaptive')
            .stop(true)
            .animate({
                height: _height * scale,
                opacity: 1,
            }, 600);
        $('#brand-zone-container').css({ width: _width, height: _height, transform: 'scale(' + scale + ')' });
    },

    /**
     * Длинные названия товаров сползают вниз при наведение
     */
    bindExpandableNames: function () {
        $(document).on('mouseenter', '.catalog:not(.catalog-not-expandable) .catalog-element .image', function () {
            const $name = $(this).parent().find('.name');
            const nameHeight = $name.height();

            if (nameHeight > 40) {
                if (!$name.data('original-height')) {
                    $name.data('original-height', $name.height());
                }
                $name.stop().animate({ height: '36px' }, 250);
            }
        }).on('mouseleave', '.catalog:not(.catalog-not-expandable) .catalog-element .image', function () {
            const $name = $(this).parent().find('.name');

            if ($name.data('original-height')) {
                $name.stop().animate({ height: $name.data('original-height') }, 150);
            }
        });
    },

    /**
     * Эл-ов на странице
     */
    bindPerpage: function () {
        $(document).on('change', '.perpage-block form', function (e) {
            e.preventDefault();

            let url = $(this).attr('action') || '';

            url += (0 === url.length) ? '?' : '&';
            url += $(this).serialize();

            window.location = url;
        });
    },

    /**
     * Вид
     */
    bindViewToggler: function () {
        $(document).on('click', '#catalog-view-toggler [data-catalog-view]', function () {
            const $a = $(this);

            if ($a.hasClass('active')) {
                return;
            }

            const view = $a.data('catalog-view');
            Cookies.set('_catalog_view_type', view);
            window.location.hash = '#jmp-to-catalog-content';
            window.location.reload(true);
        });
    },

    /**
     * Следующая страница
     */
    bindNextPageBtn: function () {
        $(document).on('click', 'a.btn-ajax-next-page', function (e) {
            e.preventDefault();

            const url = $(this).attr('href');

            $(this)
                .css('cursor', 'wait')
                .find('i').removeClass('fa-arrow-down').addClass('fa-refresh fa-pulse');

            $.ajax({
                url: url,
                data: {
                    'show-page-separator': true,
                },
                success: function (data) {
                    const $catalogContent = $('#catalog-content');
                    // удаляем старый пагинатор
                    $catalogContent.find('.navigation').remove();
                    // добавляем новую страницу
                    $catalogContent.append(data);
                    // запоминаем новый url
                    if ('undefined' !== typeof window.history.pushState) {
                        window.history.replaceState(null, '', url);
                    }
                    // обновляем scroller @see `IRMAG.bindScroller`
                    $(window).trigger('scroll.scroller');
                    // lazyload
                    new LazyLoad();
                },
            });
        });
    },

    /**
     * Скролит до страницы, которая была подгружена через AJAX (bindNextPageBtn)
     */
    bindPaginationJumper: function () {
        $(document).on('click', '.navigation [data-jmp-to-page]', function (e) {
            const pageId = $(this).data('jmp-to-page');
            go('page-' + pageId);
            e.stopPropagation();

            return false;
        });
    },
};
