/**
 * @namespace IRMAG.Promocode
 *
 * @requires jquery
 * @requires bootbox
 * @requires Routing
 */

import Routing from '../core/modules/routing';

IRMAG.Promocode = {
    init: function () {
        this.bindActivatePromocode();
        this.bindDeactivatePromocode();
    },

    bindActivatePromocode: function () {
        const self = this;

        $(document).on('click', '.activate-promocode-btn:not([disabled])', function () {
            self.activatePromocodeDialog();
        });
    },

    /**
     * Активирует промокод
     */
    activatePromocodeDialog: function () {
        bootbox.prompt('Введите код промокода для скидки', function (result) {
            if (result) {
                let success = false;
                let message = '';

                $.ajax({
                    url: Routing.generate('irmag_basket_ajax_activate_promocode'),
                    data: {
                        code: result,
                    },
                    async: false,
                    success: function (data) {
                        success = data.success;
                        message = data.msg;
                    },
                });

                if (success) {
                    bootbox.alert(message, function () {
                        window.location.reload(true);
                    });
                } else {
                    // error msg
                    const $form = $(this).find('.bootbox-form');

                    // удаляем прошлое сообщение об ошибке
                    if ($form.hasClass('has-error')) {
                        $form
                            .removeClass('has-error')
                            .find('.help-block').remove();
                    }

                    // выводим ошибку
                    $form
                        .addClass('has-error')
                        .append($('<div class="help-block m-0-bottom">' + message + '</div>'));

                    return false;
                }
            }
        });
    },

    bindDeactivatePromocode: function () {
        const self = this;

        $(document).on('click', '.deactivate-promocode-btn', function () {
            self.deactivatePromocodeDialog();
        });
    },

    /**
     * Деактивирует промокод
     */
    deactivatePromocodeDialog: function () {
        bootbox.confirm('Деактивировать промокод?', function (result) {
            if (true === result) {
                $.ajax({
                    url: Routing.generate('irmag_basket_ajax_deactivate_promocode'),
                    data: {
                        code: result,
                    },
                    success: function (data) {
                        if (data.success) {
                            window.location.reload(true);
                        }
                    },
                });
            }
        });
    },
};
