/**
 * API для избранного.
 *
 * @namespace IRMAG.API.Favorite
 */

import { ajax } from '../core/modules/irmag-utils';

IRMAG.API.Favorite = {
    /**
     * Создаёт избранное по имени
     *
     * @param {string}   favoriteName Название избранного
     * @param {function} callback     [optional]
     */
    create: function (favoriteName, callback) {
        ajax('irmag_favorite_ajax_create', {
            favoriteName: favoriteName,
        }, callback);
    },

    /**
     * Очищает избранное
     *
     * @param {number}   favoriteId Ид избранного
     * @param {function} callback   [optional]
     */
    clear: function (favoriteId, callback) {
        ajax('irmag_favorite_ajax_clear', {
            favoriteId: favoriteId,
        }, callback);
    },

    /**
     * Переименовывает избранное
     *
     * @param {number}   favoriteId   Ид избранного
     * @param {string}   favoriteName Новое название избранного
     * @param {function} callback     [optional]
     */
    rename: function (favoriteId, favoriteName, callback) {
        ajax('irmag_favorite_ajax_rename', {
            favoriteId: favoriteId,
            favoriteName: favoriteName,
        }, callback);
    },

    /**
     * Удаляет избранное
     *
     * @param {number}   favoriteId Ид избранного
     * @param {function} callback   [optional]
     */
    remove: function (favoriteId, callback) {
        ajax('irmag_favorite_ajax_remove', {
            favoriteId: favoriteId,
        }, callback);
    },

    /**
     * Добавляет товар в избранное
     *
     * @param {number}   elementId    Ид элемента
     * @param {string}   favoriteName Название избранного
     * @param {function} callback     [optional]
     */
    addElement: function (elementId, favoriteName, callback) {
        ajax('irmag_favorite_ajax_add_element', {
            elementId: elementId,
            favoriteName: favoriteName,
        }, callback);
    },

    /**
     * Удаляет товар из избранного
     *
     * @param {number}   elementId    Ид элемента
     * @param {string}   favoriteName Название избранного
     * @param {function} callback     [optional]
     */
    removeElement: function (elementId, favoriteName, callback) {
        ajax('irmag_favorite_ajax_remove_element', {
            elementId: elementId,
            favoriteName: favoriteName,
        }, callback);
    },

    /**
     * Перемещает товар из одного избранного в другое
     *
     * @param {number}   favoriteFromId Старое избранное
     * @param {number}   favoriteToId   Новое избранное
     * @param {number}   elementId      Ид элемента
     * @param {function} callback       [optional]
     */
    moveElementToFavoriteFromFavorite: function (favoriteFromId, favoriteToId, elementId, callback) {
        ajax('irmag_favorite_ajax_move_element_to_favorite_from_favorite', {
            favoriteFromId: favoriteFromId,
            favoriteToId: favoriteToId,
            elementId: elementId,
        }, callback);
    },

    /**
     * Копирует все товары избранного в корзину
     *
     * @param {string}   favoriteId Ид избранного [optional]
     * @param {function} callback   [optional]
     */
    copyAllElementsToBasket: function (favoriteId, callback) {
        ajax('irmag_favorite_ajax_copy_all_elements_to_basket', {
            favoriteId: favoriteId,
        }, callback);
    },

    /**
     * Помещает все элемененты из списка по акции в указанное избранное
     *
     * @param {string}   actionId     Ид акции
     * @param {string}   favoriteName Название избранного
     * @param {function} callback     [optional]
     */
    copyAllPromocodeElementsToFavorite: function (actionId, favoriteName, callback) {
        ajax('irmag_favorite_ajax_copy_all_promocode_elements_to_favorite', {
            actionId: actionId,
            favoriteName: favoriteName,
        }, callback);
    },

    /**
     * Запоминает свёрнуто ли избранное
     *
     * @param {string}   favoriteId Ид избранного
     * @param {boolean}  collapsed  Свёрнуто
     * @param {function} callback   [optional]
     */
    setCollapsed: function (favoriteId, collapsed, callback) {
        ajax('irmag_favorite_ajax_set_collapsed', {
            favoriteId: favoriteId,
            collapsed: collapsed,
        }, callback);
    },

    /**
     * Получает контент избранного
     *
     * @param {array}    data     Ajax request data
     * @param {function} callback [optional]
     */
    getContent: function (data, callback) {
        ajax('irmag_favorite_ajax_content', data, callback);
    },

    /**
     * Получает данные о кол-во товаров и мини избранное
     *
     * @param {function} callback [optional]
     */
    getData: function (callback) {
        ajax('irmag_favorite_ajax_data', null, callback);
    },

    /**
     * Получает ссылку для избранного
     *
     * @param {string}   favoriteId Ид избранного
     * @param {function} callback   [optional]
     */
    getShareLink: function (favoriteId, callback) {
        ajax('irmag_favorite_ajax_get_share_link', {
            favoriteId: favoriteId,
        }, callback);
    },

    /**
     * Копирует все товары из share избранного в своё.
     *
     * @param {number}   favoriteShareToken Токен share избранного
     * @param {string}   favoriteName       Название избранного
     * @param {function} callback           [optional]
     */
    copyAllFromFavoriteShareToFavorite: function (favoriteShareToken, favoriteName, callback) {
        ajax('irmag_favorite_ajax_copy_all_from_favorite_share_to_favorite', {
            favoriteShareToken: favoriteShareToken,
            favoriteName: favoriteName,
        }, callback);
    },

    /**
     * Копирует все товары из share избранного в корзину.
     */
    copyAllFromFavoriteShareToBasket: function (favoriteShareToken, callback) {
        ajax('irmag_favorite_ajax_copy_all_from_favorite_share_to_basket', {
            favoriteShareToken: favoriteShareToken,
        }, callback);
    },

    /**
     * Уведомляет, что товар добавлен в избранное
     */
    notifyAdd: function () {
        IRMAG.API.Basket._notify('favorite add');
    },

    /**
     * Уведомляет, что товар удалён из избранного
     */
    notifyRemove: function () {
        IRMAG.API.Basket._notify('favorite remove');
    },
};
