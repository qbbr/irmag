/**
 * @namespace IRMAG.Action
 *
 * @requires jquery
 * @requires countdown
 */

import { plural, isElementInView } from '../core/modules/irmag-utils';
import { bind, go } from '../core/modules/jumper';

IRMAG.Action = {
    init: function () {
        this.initCountdown();
        this.bindJmpToBody();
        this.bindJmpToComments();
        this.bindPromocodeElementsListCollapse();
        this.bindPromocodeElementsListPagination();
        IRMAG.Catalog.bindExpandableNames();
        IRMAG.Catalog.bindNextPageBtn();
        IRMAG.Catalog.bindViewToggler();
        IRMAG.Favorite.bindCopyAllPromocodeElementsToNewFavorite();
        bind('catalog-content');
        bind('action-body');
        bind('thread-comments');
        bind('promocode-elements-list');
    },

    /**
     * @see https://github.com/hilios/jQuery.countdown
     */
    initCountdown: function () {
        $('[data-end-date-time]').each(function () {
            const $elm = $(this);

            $elm.countdown($elm.data('end-date-time'))
                .on('update.countdown', function (event) {
                    $(this).html([
                        '<div class="days">',
                            '<span>', event.offset.totalDays, '</span> ',
                            plural(event.offset.totalDays, ['день', 'дня', 'дней']),
                        '</div>',
                        '<div class="hours">',
                            '<span>', event.offset.hours, '</span>',
                            plural(event.offset.hours, ['час', 'часа', 'часов']),
                        '</div>',
                        '<div class="minutes">',
                            '<span>', event.offset.minutes, '</span>',
                            plural(event.offset.minutes, ['минута', 'минуты', 'минут']),
                        '</div>',
                        '<div class="seconds">',
                            '<span>', event.offset.seconds, '</span>',
                            plural(event.offset.seconds, ['секунда', 'секунды', 'секунд']),
                        '</div>',
                    ].join('\n'));
                })
                .on('finish.countdown', function () {
                    $(this).countdown('stop')
                        .parent().html('<br>Акция завершена');
                });
        });
    },

    bindJmpToBody: function () {
        $('a[href="#jmp-to-action-body"]').on('click', function () {
            go('action-body');
        });
    },

    bindJmpToComments: function () {
        $('a[href="#jmp-to-thread-comments"]').on('click', function () {
            go('thread-comments');
        });
    },

    bindPromocodeElementsListPagination: function () {
        const $navWarapper = $('.block-element-list-nav');

        if (0 === $navWarapper.length) {
            return;
        }

        $navWarapper.find('a').each(function () {
            $(this).attr('href', $(this).attr('href') + '#jmp-to-promocode-elements-list');
        });
    },

    bindPromocodeElementsListCollapse: function () {
        const $toggler = $('#toggle-elements-list');

        if (0 === $toggler.length) {
            return;
        }

        $toggler.on('click', function () {
            const isCollapsed = $(this).attr('aria-expanded');

            if ('false' === isCollapsed) {
                $(this).find('span.fold').addClass('hidden');
                $(this).find('span.unfold').removeClass('hidden');
            } else {
                $(this).find('span.unfold').addClass('hidden');
                $(this).find('span.fold').removeClass('hidden');
            }
        });
    },

    bindMobileScrollBgHover: function () {
        $(window).on('scroll', function () {
            $('.action-list > article').each(function () {
                const $bannerBg = $(this).find('.banner-bg');

                if (isElementInView($bannerBg, true)) {
                    $(this).addClass('hover');
                } else {
                    $(this).removeClass('hover');
                }
            });
        });
    },
};
