/**
 * Оформление заказа.
 *
 * @namespace IRMAG.Order
 *
 * @requires jquery
 * @requires bootstrap-datepicker
 * @requires select2
 * @requires fancybox
 */

import Config from '../core/modules/irmag-config';
import log from '../core/modules/logger';
import { bindTrimmer } from '../core/modules/irmag-utils';

IRMAG.Order = {
    onChangeElms: '[name="order[isPayerLegal]"], [name="order[deliveryAddress][deliveryCity]"], [name="order[deliveryMethod]"], [name="order[paymentMethod]"], [name="order[payerIndividual][payerTelephone]"], select[name="order[deliveryTime]"], [name="order[selfserviceMethod]"]',
    datepickerOptions: {
        language: Config.language,
        format: Config.dateFormat,
        todayHighlight: true,
        autoclose: true,
        startDate: '0d',
        endDate: '+60d',
    },

    init: function () {
        this.$wrapper = $('#order-ajax-wrapper');

        this.bindHubs();
        this.bindOnChange();
        this.bindDatepicker();
        this.blockSubmitIfAgreementIsNotConfirmed();
        this.blockMultipleSubmit();
        this.initSelect2();

        this.showApplePayPaymentHubIfSupported();
        this.showAndroidPayHubIfSupported();
        this.bindSelfserviceMapBtn();

        this.bindCheckboxLegalAddressIsEq();
        this.bindConsigneeAddressIsEq();
        this.bindDaDataParty();

        IRMAG.Order.MixedPayment.init();
        IRMAG.Site.bindBootstrapPopover();
        IRMAG.Delivery.init();

        // date/time focus and click trigger
        $(document).on('click', '.input-group-addon', function () {
            $(this).parent().find('input, select').focus().click();
        });

        bindTrimmer('form[name="order"] input, form[name="order"] textarea');
    },

    /**
     * @see hub_widget in `src/Irmag/SiteBundle/Resources/views/Default/fields.html.twig`
     */
    bindHubs: function () {
        $(document).on('change', 'input[type="radio"]', function () {
            const $currentHub = $(this).parents('.hub').addClass('checked');
            $currentHub.parent().parent().find('.hub').not($currentHub).removeClass('checked');
        });
    },

    bindOnChange: function () {
        const self = this;

        $(document).on('change', this.onChangeElms, function () {
            self.updateForm();
        });
    },

    updateForm: function () {
        const self = this;
        const $form = self.$wrapper.find('form[name="order"]');

        self.$wrapper.addClass('block-loading');

        IRMAG.API.Order.submitForm($form.serialize(), function (data) {
            self.$wrapper
                .html(data)
                .removeClass('block-loading');

            // rebind fancybox, popover and IRMAG.Delivery form elements
            // IRMAG.Site.bindFancybox();
            IRMAG.Site.bindBootstrapPopover();
            IRMAG.Delivery.init();
            IRMAG.Delivery.reloadDeliveryBlock('irmag_get_order_delivery');

            self.bindDaDataParty();

            // wat?
            IRMAG.Order.MixedPayment.init();
            IRMAG.Order.MixedPayment.initMixedPayment();

            self.fixTimeOutOfRangeError();
            self.initSelect2();
            // self.autopickDeliveryTime(); // nn, using native sf form
            self.showApplePayPaymentHubIfSupported();
            self.showAndroidPayHubIfSupported();
            // fix twbs tooltips freeze
            $('.tooltip').remove();
        });
    },

    bindDatepicker: function () {
        const self = this;
        const monthPlus = 2;
        const inputSelector = 'input.datepicker';

        /**
         * @private
         */
        function pickFstAvailableDate($daysSheet, i = 0) {
            const $fstAvailableDay = $daysSheet.find('td:not(.disabled):first');

            if (1 === $fstAvailableDay.length) {
                $fstAvailableDay.trigger('click');
                log('[autopick-date]', new Date($fstAvailableDay.data('date')));

                return;
            }

            if (i > monthPlus) {
                return;
            }

            ++i;

            // next month
            $daysSheet.find('.next:first').trigger('click').promise().done(function () {
                pickFstAvailableDate($daysSheet, i);
            });
        }

        $(document)
            .on('click', inputSelector, function () {
                $(this)
                    .datepicker(self.datepickerOptions)
                    .datepicker('show')
                    .on('changeDate', function () {
                        self.updateForm();
                    })
                ;
            })
            .on('click', '#autopick-date', function () {
                $(inputSelector)
                    .datepicker(self.datepickerOptions)
                    .datepicker('show')
                    .on('changeDate', function () {
                        self.updateForm();
                    })
                    .promise().done(function () {
                        pickFstAvailableDate($('.datepicker-days'));
                    })
                ;
            });
    },

    blockSubmitIfAgreementIsNotConfirmed: function () {
        $(document).on('change', 'input[name="order[isAgreementAccepted]"]', function () {
            $('button.send-order').prop('disabled', !$(this).prop('checked'));
        });
    },

    blockMultipleSubmit: function () {
        $(document).on('click', 'button.send-order', function (e) {
            e.preventDefault();
            $(this).prop('disabled', true);
            $(this).parents('form').submit();
        });
    },

    showApplePayPaymentHubIfSupported: function () {
        if (window.ApplePaySession && window.ApplePaySession.canMakePayments()) {
            $('.hub-option-apple_pay').removeClass('hidden');
        }
    },

    showAndroidPayHubIfSupported: function () {
        if (window.PaymentRequest && typeof window.ApplePaySession === 'undefined') {
            $('.hub-option-android_pay').removeClass('hidden');
        }
    },

    bindSelfserviceMapBtn: function () {
        $(document).on('click', '#order_delivery_selfservice_map_btn', function () {
            const $option = $(this).parents('.input-group').find('option:selected');
            const address = $option.data('address');
            const key = $(this).data('google-key');

            $.fancybox.open({
                src: 'https://www.google.com/maps/embed/v1/place?q=' + address + '&key=' + key,
                type: 'iframe',
            });
        });
    },

    initSelect2: function () {
        $('select[name="order[deliveryAddress][deliveryCity]"]').select2({ width: '100%' }); // width 100% - fix resize bug
    },

    /**
     * XXX: rewrite to php
     */
    fixTimeOutOfRangeError: function () {
        const $crrOpt = $('[name="order[deliveryTime]"]:not(:disabled) option:selected');

        if (0 === $crrOpt.length) {
            return;
        }

        const $formGroupWithError = $crrOpt.parents('.form-group.has-error');

        if (0 === $formGroupWithError.length) {
            return;
        }

        $formGroupWithError.removeClass('has-error')
            .find('.help-block li:first').text('');
    },

    bindCheckboxLegalAddressIsEq: function () {
        $(document).on('change', '#legal-address-is-eq', function () {
            const $actualAdress = $('#order_payerLegal_payerLegalActualAddress');
            const $legalAdress = $('#order_payerLegal_payerLegalAddress');

            if ($(this).is(':checked')) {
                $(document).on('change.legal-address keyup.legal-address', $legalAdress, function () {
                    $actualAdress.val($legalAdress.val());
                });

                $actualAdress
                    .attr('readonly', true)
                    .trigger('change.legal-address')
                ;
            } else {
                $actualAdress.removeAttr('readonly');
                $(document).off('.legal-address');
            }
        });
    },

    bindConsigneeAddressIsEq: function () {
        $(document).on('change', '#consignee-address-is-eq', function () {
            const $legalName = $('#order_payerLegal_payerLegalName');
            const $consigneeAdress = $('#order_payerLegal_payerLegalConsigneeAddress');
            const $legalAdress = $('#order_payerLegal_payerLegalAddress');

            if ($(this).is(':checked')) {
                $(document).on('change.consignee-address keyup.consignee-address', [$legalAdress, $legalName], function () {
                    $consigneeAdress.val(($legalName.val() ? $legalName.val() + '. ' : '') + $legalAdress.val());
                });

                $consigneeAdress
                    .attr('readonly', true)
                    .trigger('change.consignee-address')
                ;
            } else {
                $consigneeAdress.removeAttr('readonly');
                $(document).off('.consignee-address');
            }
        });
    },

    bindDaDataParty: function () {
        const self = this;

        $('#party').suggestions({
            token: Config.daDataToken,
            type: 'PARTY',
            deferRequestBy: 300,
            count: 10,
            addon: 'clear',
            onSelect: function (suggestion) {
                self.daDataPartyPicked(suggestion);
            },
        });
    },

    /**
     * @param {object} suggestion
     *
     * @see https://dadata.ru/api/suggest/#about-party
     */
    daDataPartyPicked: function (suggestion) {
        log('[DaData] party suggestion:', suggestion);

        $('#order_payerLegal_payerLegalName').val(suggestion.value);

        if (suggestion.data.inn) {
            $('#order_payerLegal_payerLegalInn').val(suggestion.data.inn);
        }

        if (suggestion.data.kpp) {
            $('#order_payerLegal_payerLegalKpp').val(suggestion.data.kpp);
        }

        if (suggestion.data.ogrn) {
            $('#order_payerLegal_payerLegalOgrn').val(suggestion.data.ogrn);
        }

        if (suggestion.data.phones) {
            $('#order_payerLegal_payerLegalTelephone').val(suggestion.data.phones);
        }

        if (suggestion.data.address) {
            $('#order_payerLegal_payerLegalAddress').val(suggestion.data.address.data && suggestion.data.address.data.source ? suggestion.data.address.data.source : suggestion.data.address.value);
            $('#order_payerLegal_payerLegalActualAddress').val('').trigger('change.legal-address change.consignee-address');
        }
    },
};
