/**
 * Избранное.
 *
 * @namespace IRMAG.Favorite
 *
 * @requires jquery
 * @requires bootbox
 * @requires clipboard
 */

import log from '../core/modules/logger';

IRMAG.Favorite = {
    init: function () {
        this.bindCreateFavoriteActionBtn();
        this.bindClearFavoriteActionBtn();
        this.bindRenameFavoriteActionBtn();
        this.bindRemoveElementActionBtn();
        this.bindMoveElementToFavoriteFromFavoriteActionBtn();
        this.bindCopyAllElementsToBasketActionBtn();
        this.bindRemoveFavoriteActionBtn();
        this.bindCollapseFavoriteBtn();
        this.bindShare();
        this.bindSearch();
    },

    /**
     * @param {function} callback            [optional]
     * @param {string}   defaultFavoriteName [optional]
     */
    bindCreateFavoriteActionBtn: function (callback, defaultFavoriteName) {
        log('[Favorite]', 'bind create favorite btn');

        const self = this;

        $(document).on('click', '.create-favorite', function () {
            bootbox.prompt({
                title: 'Название избранного',
                value: defaultFavoriteName || 'Мои покупки',
                callback: function (favoriteName) {
                    if (favoriteName !== null) {
                        IRMAG.API.Favorite.create(favoriteName, function (data) {
                            if ($.isFunction(callback)) {
                                callback(data);
                            } else if (data.success) {
                                self.updateContent();
                            }
                        });
                    }
                },
            });
        });
    },

    bindCopyAllPromocodeElementsToNewFavorite: function () {
        log('[Favorite]', 'bind move promocode elements to a new favorite');

        const self = this;

        $(document).on('click', '.move-to-new-favorite', function () {
            const defaultFavoriteName = $(this).data('action-name');
            const actionId = $(this).data('action-id');

            bootbox.prompt({
                title: 'Название избранного',
                value: defaultFavoriteName || 'Товары по акции',
                callback: function (favoriteName) {
                    IRMAG.API.Favorite.copyAllPromocodeElementsToFavorite(actionId, favoriteName, function () {
                        self.updateData();
                    });
                },
            });
        });
    },

    bindClearFavoriteActionBtn: function () {
        log('[Favorite]', 'bind clear favorite btn');

        const self = this;

        $(document).on('click', '.clear-favorite', function () {
            const favoriteId = $(this).data('favorite-id');

            bootbox.dialog({
                message: 'Очистить избранное?',
                buttons: {
                    Отмена: {
                        className: 'btn-default',
                    },
                    Очистить: {
                        className: 'btn-danger',
                        callback: function () {
                            IRMAG.API.Favorite.clear(favoriteId, function (data) {
                                if (data.success) {
                                    IRMAG.API.Favorite.notifyRemove();
                                    self.updateContent();
                                }
                            });
                        },
                    },
                },
            });
        });
    },

    bindRenameFavoriteActionBtn: function () {
        log('[Favorite]', 'bind rename favorite btn');

        const self = this;

        $(document).on('click', '.rename-favorite', function () {
            const favoriteId = $(this).data('favorite-id');
            const favoriteOldName = $(this).data('favorite-name');

            bootbox.prompt({
                title: 'Новое название избранного',
                value: favoriteOldName,
                callback: function (favoriteName) {
                    if (favoriteName !== null && favoriteOldName !== favoriteName) {
                        IRMAG.API.Favorite.rename(favoriteId, favoriteName, function (data) {
                            if (data.success) {
                                self.updateContent();
                            }
                        });
                    }
                },
            });
        });
    },

    bindRemoveFavoriteActionBtn: function () {
        log('[Favorite]', 'bind remove favorite btn');

        const self = this;

        $(document).on('click', '.remove-favorite', function () {
            const favoriteId = $(this).data('favorite-id');

            bootbox.dialog({
                message: 'Удалить избранное?',
                buttons: {
                    Отмена: {
                        className: 'btn-default',
                    },
                    Удалить: {
                        className: 'btn-danger',
                        callback: function () {
                            IRMAG.API.Favorite.remove(favoriteId, function (data) {
                                if (data.success) {
                                    IRMAG.API.Favorite.notifyRemove();
                                    self.updateContent();
                                }
                            });
                        },
                    },
                },
            });
        });
    },

    bindAddElementActionBtn: function () {
        log('[Favorite]', 'bind add element btn');

        $(document).on('click', '.add-element-to-favorite[data-element-id], .add-element-to-favorite ~ ul.favorite-dropdown li:not(.disabled) a[data-element-id]', function () {
            const $addBtn = $(this);
            const elementId = $addBtn.data('element-id');
            const favoriteName = $addBtn.data('favorite-name') || null;

            IRMAG.API.Favorite.addElement(elementId, favoriteName, function (data) {
                if (data.success) {
                    IRMAG.API.Favorite.notifyAdd();
                    IRMAG.Favorite.updateData();

                    if (favoriteName) {
                        // multiple, dropdown menu
                        $addBtn
                            .parent().addClass('disabled') // li
                            .parents('.btn-group').find('.dropdown-toggle i.fa-star-o') // a i.fa-star-o
                            .removeClass('fa-star-o').addClass('fa-star');
                    } else {
                        // single
                        $addBtn.find('i.fa-star-o').removeClass('fa-star-o').addClass('fa-star');
                    }
                }
            });
        });
    },

    bindRemoveElementActionBtn: function () {
        log('[Favorite]', 'bind remove element btn');

        const self = this;

        $(document).on('click', '.remove-element-from-favorite', function () {
            const elementId = $(this).data('element-id');
            const favoriteName = $(this).data('favorite-name') || null;

            IRMAG.API.Favorite.removeElement(elementId, favoriteName, function (data) {
                if (data.success) {
                    IRMAG.API.Favorite.notifyRemove();
                    self.updateContent();
                }
            });
        });
    },

    bindMoveElementToFavoriteFromFavoriteActionBtn: function () {
        log('[Favorite]', 'bind move element to favorite from favorite btn');

        const self = this;

        $(document).on('click', '.move-element-from-favorite-to-favorite ~ ul.favorite-dropdown li:not(.disabled) a[data-element-id]', function () {
            const elementId = $(this).data('element-id');
            const favoriteFromId = $(this).data('favorite-from-id');
            const favoriteToId = $(this).data('favorite-to-id');

            IRMAG.API.Favorite.moveElementToFavoriteFromFavorite(favoriteFromId, favoriteToId, elementId, function (data) {
                if (data.success) {
                    IRMAG.API.Favorite.notifyAdd();
                    self.updateContent();
                }
            });
        });
    },

    bindCopyAllElementsToBasketActionBtn: function () {
        log('[Favorite]', 'bind copy all elements to basket btn');

        $(document).on('click', '.copy-all-elements-to-basket', function () {
            const favoriteId = $(this).data('favorite-id');

            bootbox.confirm('Скопировать все товары в корзину?', function (result) {
                if (true === result) {
                    IRMAG.API.Favorite.copyAllElementsToBasket(favoriteId, function (data) {
                        if (data.success) {
                            IRMAG.API.Basket.notifyAdd();
                            IRMAG.Basket.updateData();
                        }
                    });
                }
            });
        });
    },

    bindCollapseFavoriteBtn: function () {
        log('[Favorite]', 'bind collapse favorite btn');

        function setCollapse($link, isCollapsed) {
            const $icon = $link.find('i');
            const favoriteId = $link.data('favorite-id');

            if (isCollapsed) {
                $icon
                    .addClass('fa-plus-square')
                    .removeClass('fa-minus-square')
                    .attr('title', 'Развернуть')
                ;
            } else {
                $icon
                    .addClass('fa-minus-square')
                    .removeClass('fa-plus-square')
                    .attr('title', 'Свернуть')
                ;
            }

            $icon.tooltip('fixTitle').tooltip('hide');

            IRMAG.API.Favorite.setCollapsed(favoriteId, isCollapsed);
        }

        $(document)
            .on('shown.bs.collapse', '.panel-body.collapse', function (e) {
                setCollapse($(e.target).data('bs.collapse').$trigger, false);
            })
            .on('hidden.bs.collapse', '.panel-body.collapse', function (e) {
                setCollapse($(e.target).data('bs.collapse').$trigger, true);
            })
        ;
    },

    bindShare: function () {
        log('[Favorite]', 'bind share btn');

        $(document).on('click', '#favorite-share-btn', function () {
            const favoriteId = $(this).data('favorite-id');

            bootbox.dialog({
                message: 'Поделиться избранным?',
                buttons: {
                    Отмена: {
                        className: 'btn-default',
                    },
                    Поделиться: {
                        className: 'btn-success',
                        callback: function () {
                            IRMAG.API.Favorite.getShareLink(favoriteId, function (data) {
                                if (data.success) {
                                    const html = [
                                        '<h4>Ссылка на избранное:</h4>',
                                        '<div class="input-group">',
                                            '<input class="form-control" id="clipboard-input" value="' + data.url + '" readonly>',
                                            '<span class="input-group-btn">',
                                                '<button type="button" class="btn btn-default" id="clipboard-btn" data-toggle="tooltip" data-clipboard-target="#clipboard-input" title="Нажмите, чтобы скопировать.">',
                                                    '<i class="fa fa-clipboard"></i></span>',
                                                '</button>',
                                            '</span>',
                                        '</div>',
                                    ].join('\n');

                                    bootbox.alert(html);

                                    new ClipboardJS('#clipboard-btn').on('success', function (e) {
                                        const $owner = $(e.trigger).removeClass('btn-default').addClass('btn-success'); // #clipboard-btn
                                        $owner.parents('.input-group').addClass('has-success'); // .input-group
                                        const originalTitle = $owner.data('original-title');

                                        $owner
                                            .attr('title', 'Скопировано.').tooltip('fixTitle').tooltip('show')
                                            .on('hidden.bs.tooltip', function () {
                                                $(this).attr('title', originalTitle).tooltip('fixTitle');
                                            });

                                        e.clearSelection();
                                    });
                                }
                            });
                        },
                    },
                },
            });
        });
    },

    bindCopyAllFromFavoriteShareToFavoriteBtn: function () {
        log('[Favorite]', 'bind copy all elements from favorite share to favorite btn');

        $(document).on('click', 'button.copy-all-from-favorite-share-to-favorite, .copy-all-from-favorite-share-to-favorite ul li:not(.disabled) a[data-favorite-name]', function () {
            const favoriteShareToken = $('[data-favorite-share-token]').data('favorite-share-token');
            const favoriteName = $(this).data('favorite-name') || null;

            bootbox.confirm('Скопировать все товары в избранное?', function (result) {
                if (true === result) {
                    IRMAG.API.Favorite.copyAllFromFavoriteShareToFavorite(favoriteShareToken, favoriteName, function (data) {
                        if (data.success) {
                            IRMAG.API.Favorite.notifyAdd();
                            IRMAG.Favorite.updateData();
                        }
                    });
                }
            });
        });
    },

    bindCopyAllFromFavoriteShareToBasketBtn: function () {
        log('[Favorite]', 'bind copy all elements from favorite share to basket btn');

        $(document).on('click', '.copy-all-from-favorite-share-to-basket', function () {
            const favoriteShareToken = $('[data-favorite-share-token]').data('favorite-share-token');

            bootbox.confirm('Скопировать все товары в корзину?', function (result) {
                if (true === result) {
                    IRMAG.API.Favorite.copyAllFromFavoriteShareToBasket(favoriteShareToken, function (data) {
                        if (data.success) {
                            IRMAG.API.Basket.notifyAdd();
                            IRMAG.Basket.updateData();
                        }
                    });
                }
            });
        });
    },

    bindSearch: function () {
        const $input = $('#favorite-search');

        if (0 === $input.length) {
            return;
        }

        const $clearBtn = $input.next('a');

        $input.on('keyup', function (e) {
            const val = $(this).val();
            const $rows = $('.favorite .panel-body > .row');
            const $rowsHr = $rows.next('hr');

            // Esc
            if (27 === e.keyCode) {
                $clearBtn.trigger('click');
                $rows.show();
                $rowsHr.show();

                return;
            }

            if (!val) {
                $rows.show();
                $rowsHr.show();
                $clearBtn.stop(true).fadeOut('fast');

                return;
            }

            $clearBtn.stop(true).fadeIn('fast');
            const $rowsForShow = $rows.has('.name > a:Contains("' + val + '")').show();
            const $rowsHrForShow = $rowsForShow.next('hr').show();
            $rows.not($rowsForShow).hide();
            $rowsHr.not($rowsHrForShow).hide();

            // скрываем последний hr
            $('.favorite .panel-body').each(function () {
                $(this).children('.row:visible:last').next('hr').hide();
            });
        });

        $clearBtn.on('click', function () {
            $(this).fadeOut('fast');
            $input.val('').trigger('keyup');
        });
    },

    updateContent: function () {
        const self = this;
        const $wrapper = $('#favorite-ajax-wrapper').addClass('block-loading');
        const sortData = $('[data-sort]').data('sort');

        IRMAG.API.Favorite.getContent(sortData, function (data) {
            $wrapper
                .html(data)
                .removeClass('block-loading');

            self.updateData();
            self.bindSearch();
        });
    },

    /**
     * Обновляет информацию о кол-ве эл-ов (header-blue-line) и мини избранное
     */
    updateData: function () {
        IRMAG.API.Favorite.getData(function (data) {
            $('#total-count-favorite-wrapper').html(data['total-count']);
            $('#mini-favorite-wrapper').html(data['mini-favorite']);
            IRMAG.Site.bindMiniBasketFavorite();
        });
    },
};
