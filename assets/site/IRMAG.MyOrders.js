/**
 * @namespace IRMAG.MyOrders
 *
 * @requires jquery
 * @requires clipboard
 */

IRMAG.MyOrders = {
    init: function () {
        this.bindClipboard();
    },

    /**
     * Копирует трек-код.
     */
    bindClipboard: function () {
        new ClipboardJS('.my-track-code').on('success', function (e) {
           const $owner = $(e.trigger);
           const originalTitle = $owner.data('hint');

           $owner
               .attr('title', 'Трек-код скопирован.').tooltip('fixTitle').tooltip('show')
               .on('hidden.bs.tooltip', function () {
                   $(this).attr('title', originalTitle).tooltip('fixTitle');
               })
            ;
        });
    },
};
