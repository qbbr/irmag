/**
 * @namespace IRMAG.Cat404
 *
 * @requires jquery
 */

IRMAG.Cat404 = {
    init: function () {
        const $el1 = $('#eye-left').show();
        const $eyeBall1 = $el1.find('div');
        const $el2 = $('#eye-right').show();
        const $eyeBall2 = $el2.find('div');

        const x1 = $el1.offset().left + 165;
        const y1 = $el1.offset().top + 25;
        const r = 38;

        $('body').mousemove(function (e) {
            const x2 = e.pageX;
            const y2 = e.pageY;
            const y = ((r * (y2 - y1)) / Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))) + y1;
            const x = (((y - y1) * (x2 - x1)) / (y2 - y1)) + x1;

            $.merge($eyeBall1, $eyeBall2).css({
                'margin-top': y - y1 + 1,
                'margin-left': x - x1,
            });
        });
    },
};
