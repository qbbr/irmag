/**
 * Онлайн платежи.
 *
 * @namespace IRMAG.Payment
 *
 * @requires jquery
 * @requires Routing
 */

import Routing from '../core/modules/routing';
import log from '../core/modules/logger';

IRMAG.Payment = {
    merchantCapabilities: ['supports3DS', 'supportsCredit', 'supportsDebit'],
    supportedNetworks: ['masterCard', 'visa'],
    label: 'ООО "ИРМАГ"',
    btnApplePaySelector: '#apple-pay-button',
    applePayBtnContainerSelector: '#apple-pay-block',

    init: function () {
        this.hideSpinnerOnLoadAndShowApplePayButtonIfAvailable();
        this.bindDoApplePaymentRequest();
    },

    /**
     * Скрыть анимацию загрузки после загрузки страницы.
     */
    hideSpinnerOnLoadAndShowApplePayButtonIfAvailable: function () {
        const self = this;

        $(document).ready(function () {
            $('#payment-frame').removeClass('block-loading');
            $('#payment-frame-spinner').addClass('hidden');

            if (window.ApplePaySession) {
                const merchantIdentifier = $(self.applePayBtnContainerSelector).data('merchant');
                const promise = window.ApplePaySession.canMakePaymentsWithActiveCard(merchantIdentifier);

                promise.then(function (canMakePayments) {
                    if (canMakePayments) {
                        $('.apple-pay-button-with-text').removeClass('hidden');
                    }
                });
            }
        });
    },

    /**
     * Обработка платежа ApplePay.
     */
    bindDoApplePaymentRequest: function () {
        const self = this;

        $(self.btnApplePaySelector).on('click', function () {
            if (window.PaymentRequest) {
                self.doStandartizedPaymentRequest();
            } else {
                self.doNativeApplePayPaymentAction();
            }
        });
    },

    /**
     * Нативный ApplePay платеж в Safari (Apple Pay JS API).
     */
    doNativeApplePayPaymentAction: function () {
        const self = this;
        const paymentRequest = self.createApplePayPaymentRequest();
        const session = new window.ApplePaySession(1, paymentRequest);

        session.onvalidatemerchant = function (event) {
            const promise = self.performValidation(event.validationURL);

            promise.then(function (merchantSession) {
                session.completeMerchantValidation(merchantSession);
            });
        };

        session.onshippingcontactselected = function () {
            const shippingMethods = [{
 label: 'Standart Shipping', amount: 0, detail: '1 day', identifier: 'domestic',
}];
            const newTotal = { type: 'final', label: self.label, amount: self._getTotalAmount() };
            session.completeShippingContactSelection(window.ApplePaySession.STATUS_SUCCESS, shippingMethods, newTotal, self._getLineItems());
        };

        session.onshippingmethodselected = function () {
            const newTotal = { type: 'final', label: self.label, amount: self._getTotalAmount() };
            session.completeShippingMethodSelection(window.ApplePaySession.STATUS_SUCCESS, newTotal, self._getLineItems());
        };

        session.onpaymentmethodselected = function () {
            const newTotal = { type: 'final', label: self.label, amount: self._getTotalAmount() };
            session.completePaymentMethodSelection(newTotal, self._getLineItems());
        };

        session.onpaymentauthorized = function (event) {
            const promise = self.sendApplePayPaymentToken(event.payment.token);

            promise.then(function (data) {
                if (data.success) {
                    session.completePayment(window.ApplePaySession.STATUS_SUCCESS);
                    window.location.href = Routing.generate('irmag_order_finish_online_payment', { orderId: data.tkn });
                } else {
                    session.completePayment(window.ApplePaySession.STATUS_FAILURE);
                    window.location.href = Routing.generate('irmag_order_cancel_online_payment', { orderId: data.tkn });
                }
            });
        };

        session.oncancel = function () {};
        session.begin();
    },

    /**
     * Платеж через Payment Request API.
     */
    doStandartizedPaymentRequest: function () {
        const self = this;
        const applePayMethods = [{
            supportedMethods: ['https://apple.com/apple-pay'],
            data: {
                version: 3,
                supportedNetworks: self.supportedNetworks,
                merchantCapabilities: self.merchantCapabilities,
                countryCode: 'RU',
                merchantIdentifier: $(self.applePayBtnContainerSelector).data('merchant'),
            },
        }];

        const paymentDetails = {
            total: { label: 'Заказ ИРМАГ', amount: { value: self._getTotalAmount(), currency: 'RUB' } },
            displayItems: [{ label: 'Заказ ИРМАГ', amount: { value: self._getTotalAmount(), currency: 'RUB' } }],
            shippingOptions: [{ id: 'standard', label: 'Standard Shipping', amount: { value: 0.00, currency: 'RUB' } }],
        };

        const paymentOptions = {
            requestPayerName: true,
            requestPayerEmail: true,
            requestPayerPhone: false,
            requestShipping: false,
            shippingType: 'shipping',
        };

        try {
            const paymentRequest = new PaymentRequest(applePayMethods, paymentDetails, paymentOptions);
            paymentRequest.onmerchantvalidation = function (event) {
                const sessionPromise = self.performValidation(event.validationURL);
                event.complete(sessionPromise);
            };

            paymentRequest.show().then(function (result) {
                const response = result;
                const promise = self.sendApplePayPaymentToken(result.details.token);

                promise.then(function (data) {
                    if (data.success) {
                        response.complete('success');
                        window.location.href = Routing.generate('irmag_order_finish_online_payment', { orderId: data.tkn });
                    } else {
                        response.complete('fail');
                        window.location.href = Routing.generate('irmag_order_cancel_online_payment', { orderId: data.tkn });
                    }
                });
            });
        } catch (err) {
            log('[Error]: ', err);
        }
    },

    /**
     * Создать объект PaymentRequest.
     *
     * @returns object
     */
    createApplePayPaymentRequest: function () {
        const self = this;

        return {
            currencyCode: 'RUB',
            countryCode: 'RU',
            merchantCapabilities: self.merchantCapabilities,
            supportedNetworks: self.supportedNetworks,
            requiredShippingContactFields: ['postalAddress'],
            lineItems: [{ label: 'Заказ ИРМАГ ', amount: self._getTotalAmount() }],
            total: {
                label: self.label,
                type: 'final',
                amount: self._getTotalAmount(),
            },
        };
    },

    /** @private */
    _getTotalAmount: function () {
        return parseFloat($(this.applePayBtnContainerSelector).data('total') / 100.00);
    },

    /** @private */
    _getLineItems: function () {
        return [{ type: 'final', label: 'Заказ ИРМАГ ', amount: this._getTotalAmount() }];
    },

    /**
     * Выполнить валидацию merchant у Apple.
     *
     * @param validationUrl
     */
    performValidation: function (validationUrl) {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: Routing.generate('irmag_order_validate_apple_pay_merchant'),
                method: 'GET',
                data: {
                    validationUrl: validationUrl,
                },
                success: function (data) {
                    resolve(data.data);
                },
                error: function () {
                    reject();
                },
            });
        });
    },

    /**
     * Переслать PaymentToken от Apple в Сбербанк.
     *
     * @param paymentToken
     *
     * @returns {Promise<any>}
     */
    sendApplePayPaymentToken: function (paymentToken) {
        const self = this;

        return new Promise(function (resolve, reject) {
            $.ajax({
                url: Routing.generate('irmag_order_process_apple_pay'),
                data: {
                    paymentToken: JSON.stringify(paymentToken.paymentData),
                    paymentId: $(self.applePayBtnContainerSelector).data('payment'),
                },
                success: function (response) {
                    resolve(response);
                },
                error: function () {
                    reject();
                },
            });
        });
    },
};
