import './index.scss';
import html from './index.html';

$('body').append(html);

const $scroller = $('#scroller');

if (0 !== $scroller.length) {
    const $scrollUp = $scroller.find('.scroll-up');
    const $scrollDown = $scroller.find('.scroll-down');
    const offset = 200;

    $(window).on('scroll.scroller', function () {
        const windowHeight = $(window).height();
        const windowScrollTop = $(window).scrollTop();
        const documentHeight = $(document).height();

        if (windowScrollTop > offset) {
            $scrollUp.show();

            if (documentHeight - (windowScrollTop + windowHeight) < offset) {
                $scrollDown.hide();
            } else if (documentHeight > 1200) {
                $scrollDown.show();
            }
        } else {
            $scrollUp.hide();
            $scrollDown.show();
        }
    }).trigger('scroll.scroller');

    $scrollUp.on('click', function () {
        $('body, html').animate({ scrollTop: 0 }, 1000);

        return false;
    });

    $scrollDown.on('click', function () {
        $('body, html').animate({ scrollTop: $(document).height() }, 1000);

        return false;
    });

    setTimeout(function () {
        $scroller.fadeIn();
    }, 500);
}
