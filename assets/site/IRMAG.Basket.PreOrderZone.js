/**
 * Предкассовая зона.
 *
 * @namespace IRMAG.Basket.PreOrderZone
 *
 * @requires jquery
 * @requires slick-carousel
 */

import log from '../core/modules/logger';

IRMAG.Basket.PreOrderZone = {
    $container: null,
    $slides: null,

    init: function () {
        this.$container = $('#pre-order-zone-carousel');
        this.$slides = this.$container.find('.carousel-slide');

        if (0 === this.$slides.length) {
            return;
        }

        log('[PreOrderZone]', 'init');

        this.initElementCarousel();
        this.bindUpdateBasketOnElementAdded();
    },

    /**
     * Инициализация карусельки с товарами предкассовой зоны.
     */
    initElementCarousel: function () {
        this.$container.slick({
            dots: true,
            adaptiveHeight: true,
        });
    },

    /**
     * Обновить содержимое корзины при добавлении товара из карусели.
     */
    bindUpdateBasketOnElementAdded: function () {
        this.$container.on('click', '.add-element-to-basket', function () {
            IRMAG.Basket.updateContent();
        });
    },
};
