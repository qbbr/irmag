/**
 * API для оформления заказа.
 *
 * @namespace IRMAG.API.Order
 */

import { ajax } from '../core/modules/irmag-utils';

IRMAG.API.Order = {
    /**
     * @param {string}   formSerialize $form.serialize()
     * @param {function} callback      [optional]
     */
    submitForm: function (formSerialize, callback) {
        ajax('irmag_order_ajax_content', formSerialize, callback);
    },
};
