module.exports = {
    "root": true,
    "env": {
        "es6": true,
        "browser": true,
        "jquery": true
    },
    "extends": ["airbnb"],
    "rules": {
        "func-names": ["error", "never"],
        "quotes": ["error", "single"],
        "indent": 0,
        "prefer-arrow-callback": 0,
        "object-shorthand": 0,
        "no-script-url": 0,
        "semi-style": 0,
        "no-plusplus": 0,
        "no-template-curly-in-string": 0,
        "max-len": 0,
        "no-underscore-dangle": 0,
        "no-bitwise": 0,
        "no-param-reassign": 0,
        "no-new": 0,
        "yoda": 0,
        "consistent-return": 0,
        "no-alert": 0,
        "no-console": 0,
        "prefer-template": 0,
        "newline-per-chained-call": 0,
        "no-cond-assign": 0,
        "no-useless-concat": 0,
        "guard-for-in": 0
    },
    "globals": {
        "IRMAG": true,
        "bootbox": true,
        "Bloodhound": true,
        "Quagga": true,
        "Cookies": true,
        "ClipboardJS": true,
        "ApplePaySession": true,
        "google": true,
        "ResponsiveBootstrapToolkit": true,
        "LazyLoad": true,
        "emojify": true
    }
};
