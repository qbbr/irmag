# IRMAG on Symfony framework

[![build status](https://irmag.gitlab/php/irmag/badges/master/build.svg)](https://irmag.gitlab/php/irmag/commits/master)

## Build

```bash
git clone git@irmag.gitlab:php/irmag.git && cd $_
cp .env.dev .env
make build
make load-default-data
```

 * [Makefile](Makefile)

### Fake data generator (optional)

Install [irmag-extra/generator-bundle](git@irmag.gitlab:php/irmag-extra-generator-bundle.git)

```bash
make load-generator-data
```

## Configuration

 * [CONFIGURATION](CONFIGURATION.md)

## Requirements and Dependencies

 * [php 7.2](https://php.net/)
 * [symfony 3.4](https://symfony.com/)
 * [postgresql 9.6](https://www.postgresql.org/)
 * [redis](https://redis.io/)
 * [nodejs](https://nodejs.org/)
 * [yarn](https://yarnpkg.com/)
 * [webpack](https://webpack.js.org/)
 * [jpegoptim](https://github.com/tjko/jpegoptim) for `LiipImagineBundle` post_processors.

## Contributors

 * [CONTRIBUTORS](CONTRIBUTORS.md)
