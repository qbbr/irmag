<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180725015545 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        // set default selfservice_method_id
        $this->addSql("
            UPDATE orders
            SET selfservice_method_id = (
                SELECT id
                FROM orders_delivery_selfservice_methods
                WHERE shortname = 'irmag'
            )
            WHERE delivery_method_id = (
                SELECT id
                FROM orders_delivery_methods
                WHERE shortname = 'selfservice'
            )
            AND selfservice_method_id IS NULL
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
