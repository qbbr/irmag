<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180316022309 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE forum_posts DROP old_id');
        $this->addSql('ALTER TABLE forum_forums DROP old_id');
        $this->addSql('ALTER TABLE forum_topics DROP old_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE forum_posts ADD old_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE forum_topics ADD old_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE forum_forums ADD old_id INT DEFAULT NULL');
    }
}
