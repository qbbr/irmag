<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181022042422 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("DELETE FROM roles_childrens WHERE children_id IN (SELECT id FROM roles WHERE name LIKE 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_PUSH_SENDER_%')");
        $this->addSql("DELETE FROM roles WHERE name LIKE 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_PUSH_SENDER_%'");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_READER', '[ADMIN] Администрирование - Рассыльщик уведомлений - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_VIEW', '[ADMIN] Администрирование - Рассыльщик уведомлений - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_LIST', '[ADMIN] Администрирование - Рассыльщик уведомлений - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_VIEW')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_EDITOR', '[ADMIN] Администрирование - Рассыльщик уведомлений - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_EDIT', '[ADMIN] Администрирование - Рассыльщик уведомлений - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_CREATE', '[ADMIN] Администрирование - Рассыльщик уведомлений - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_ADMIN', '[ADMIN] Администрирование - Рассыльщик уведомлений - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_DELETE', '[ADMIN] Администрирование - Рассыльщик уведомлений - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_EXPORT', '[ADMIN] Администрирование - Рассыльщик уведомлений - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_ADMIN')
            );
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
