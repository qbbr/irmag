<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180309051249 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("INSERT INTO roles (id, name, description)
                        VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_READER', '[ADMIN] Администрирование - Очередь обмена с 1С - Читатель')"
        );

        $this->addSql("INSERT INTO roles (id, name, description)
                        VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_VIEW', '[ADMIN] Администрирование - Очередь обмена с 1С - Просмотр')"
        );

        $this->addSql("INSERT INTO roles (id, name, description)
                        VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_LIST', '[ADMIN] Администрирование - Очередь обмена с 1С - Список')"
        );

        $this->addSql("INSERT INTO roles_childrens (role_id, children_id)
                        VALUES (
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_READER'),
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_VIEW')
                        )"
        );

        $this->addSql("INSERT INTO roles_childrens (role_id, children_id)
                        VALUES (
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_READER'),
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_LIST')
                        )"
        );

        $this->addSql("INSERT INTO roles (id, name, description)
                        VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_EDITOR', '[ADMIN] Администрирование - Очередь обмена с 1С - Редактор')"
        );
        $this->addSql("INSERT INTO roles (id, name, description)
                        VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_EDIT', '[ADMIN] Администрирование - Очередь обмена с 1С - Редактировать')"
        );
        $this->addSql("INSERT INTO roles (id, name, description)
                        VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_CREATE', '[ADMIN] Администрирование - Очередь обмена с 1С - Создавать')"
        );
        $this->addSql("INSERT INTO roles_childrens (role_id, children_id)
                        VALUES (
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_EDITOR'),
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_EDIT')
                        )"
        );
        $this->addSql("INSERT INTO roles_childrens (role_id, children_id)
                        VALUES (
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_EDITOR'),
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_CREATE')
                        )"
        );
        $this->addSql("INSERT INTO roles_childrens (role_id, children_id)
                        VALUES (
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_EDITOR'),
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_READER')
                        )"
        );
        $this->addSql("INSERT INTO roles (id, name, description)
                        VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_ADMIN', '[ADMIN] Администрирование - Очередь обмена с 1С - Администратор')"
        );
        $this->addSql("INSERT INTO roles (id, name, description)
                        VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_DELETE', '[ADMIN] Администрирование - Очередь обмена с 1С - Удалять')"
        );
        $this->addSql("INSERT INTO roles (id, name, description)
                        VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_EXPORT', '[ADMIN] Администрирование - Очередь обмена с 1С - Экспорт')"
        );
        $this->addSql("INSERT INTO roles_childrens (role_id, children_id)
                        VALUES (
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_ADMIN'),
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_DELETE')
                        )"
        );
        $this->addSql("INSERT INTO roles_childrens (role_id, children_id)
                        VALUES (
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_ADMIN'),
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_EXPORT')
                        )"
        );
        $this->addSql("INSERT INTO roles_childrens (role_id, children_id)
                        VALUES (
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_ADMIN'),
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_EDITOR')
                        )"
        );

        $this->addSql("INSERT INTO groups_roles (role_id, group_id)
                        VALUES (
                            (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE_ADMIN'),
                            (SELECT id FROM groups WHERE shortname = 'administrators')
                        )"
        );
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DELETE FROM roles WHERE name LIKE `ROLE_IRMAG_ADMIN_ADMINISTRATION_EXCHANGE_1_C_QUEUE%`');
    }
}
