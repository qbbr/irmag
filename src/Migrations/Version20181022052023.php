<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181022052023 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DELETE FROM users_notification_queues');
        $this->addSql('ALTER TABLE users_notification_queues ADD event_id INT NOT NULL');
        $this->addSql('ALTER TABLE users_notification_queues ADD CONSTRAINT FK_69F32E5171F7E88B FOREIGN KEY (event_id) REFERENCES users_notification_events (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_69F32E5171F7E88B ON users_notification_queues (event_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notification_queues DROP CONSTRAINT FK_69F32E5171F7E88B');
        $this->addSql('DROP INDEX IDX_69F32E5171F7E88B');
        $this->addSql('ALTER TABLE users_notification_queues DROP event_id');
    }
}
