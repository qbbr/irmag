<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180419043543 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE coupon_elements_lists (id INT NOT NULL, uuid1c UUID NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_219E7F63C009BE4B ON coupon_elements_lists (uuid1c)');
        $this->addSql('CREATE TABLE coupon_elements_lists_elements (coupon_elements_list_id INT NOT NULL, element_id INT NOT NULL, PRIMARY KEY(coupon_elements_list_id, element_id))');
        $this->addSql('CREATE INDEX IDX_913A7924AD83 ON coupon_elements_lists_elements (coupon_elements_list_id)');
        $this->addSql('CREATE INDEX IDX_913A7921F1F2A24 ON coupon_elements_lists_elements (element_id)');
        $this->addSql('ALTER TABLE coupon_elements_lists_elements ADD CONSTRAINT FK_913A7924AD83 FOREIGN KEY (coupon_elements_list_id) REFERENCES coupon_elements_lists (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE coupon_elements_lists_elements ADD CONSTRAINT FK_913A7921F1F2A24 FOREIGN KEY (element_id) REFERENCES elements (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE coupons ADD element_list_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE coupons ADD CONSTRAINT FK_F5641118CA4556DF FOREIGN KEY (element_list_id) REFERENCES coupon_elements_lists (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_F5641118CA4556DF ON coupons (element_list_id)');
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE coupons DROP CONSTRAINT FK_F5641118CA4556DF');
        $this->addSql('ALTER TABLE coupon_elements_lists_elements DROP CONSTRAINT FK_913A7924AD83');
        $this->addSql('DROP TABLE coupon_elements_lists');
        $this->addSql('DROP TABLE coupon_elements_lists_elements');
        $this->addSql('DROP INDEX IDX_F5641118CA4556DF');
        $this->addSql('ALTER TABLE coupons DROP element_list_id');
    }
}
