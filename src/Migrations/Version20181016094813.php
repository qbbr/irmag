<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181016094813 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("    INSERT INTO roles (id, name, description)
    VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_READER', '[ADMIN] Администрирование - Подписки пользователей на события - Читатель');");

        $this->addSql("    INSERT INTO roles (id, name, description)
    VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_VIEW', '[ADMIN] Администрирование - Подписки пользователей на события - Просмотр');");

        $this->addSql("    INSERT INTO roles (id, name, description)
    VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_LIST', '[ADMIN] Администрирование - Подписки пользователей на события - Список');");

        $this->addSql("
    INSERT INTO roles_childrens (role_id, children_id)
    VALUES (
        (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_READER'),
        (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_VIEW')
    );");

        $this->addSql("
    INSERT INTO roles_childrens (role_id, children_id)
    VALUES (
        (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_READER'),
        (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_LIST')
    );");

        $this->addSql("    INSERT INTO roles (id, name, description)
    VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EDITOR', '[ADMIN] Администрирование - Подписки пользователей на события - Редактор');");

        $this->addSql("    INSERT INTO roles (id, name, description)
    VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EDIT', '[ADMIN] Администрирование - Подписки пользователей на события - Редактировать');");

        $this->addSql("    INSERT INTO roles (id, name, description)
    VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_CREATE', '[ADMIN] Администрирование - Подписки пользователей на события - Создавать');");

        $this->addSql("
    INSERT INTO roles_childrens (role_id, children_id)
    VALUES (
        (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EDITOR'),
        (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EDIT')
    );");

        $this->addSql("
    INSERT INTO roles_childrens (role_id, children_id)
    VALUES (
        (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EDITOR'),
        (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_CREATE')
    );");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_ADMIN', '[ADMIN] Администрирование - Подписки пользователей на события - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_DELETE', '[ADMIN] Администрирование - Подписки пользователей на события - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EXPORT', '[ADMIN] Администрирование - Подписки пользователей на события - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_ADMIN')
            );
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("DELETE FROM roles WHERE name LIKE 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_%'");
    }
}
