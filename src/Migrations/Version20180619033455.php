<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180619033455 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_order_profiles ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE users_order_profiles ALTER delivery_city_id SET NOT NULL');
        $this->addSql('ALTER TABLE users_block_history DROP CONSTRAINT FK_547C8CB8A76ED395');
        $this->addSql('ALTER TABLE users_block_history ALTER user_id SET NOT NULL');
        $this->addSql('ALTER TABLE users_block_history ADD CONSTRAINT FK_547C8CB8A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders ALTER delivery_city_id SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders ALTER delivery_city_id DROP NOT NULL');
        $this->addSql('ALTER TABLE users_block_history DROP CONSTRAINT fk_547c8cb8a76ed395');
        $this->addSql('ALTER TABLE users_block_history ALTER user_id DROP NOT NULL');
        $this->addSql('ALTER TABLE users_block_history ADD CONSTRAINT fk_547c8cb8a76ed395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_order_profiles ALTER user_id DROP NOT NULL');
        $this->addSql('ALTER TABLE users_order_profiles ALTER delivery_city_id DROP NOT NULL');
    }
}
