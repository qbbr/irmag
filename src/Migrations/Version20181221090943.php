<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181221090943 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE forum_messages DROP CONSTRAINT fk_fcf19371dd92dab8');
        $this->addSql('ALTER TABLE forum_message_attachments DROP CONSTRAINT fk_668604aa232d562b');
        $this->addSql('DROP SEQUENCE forum_message_attachments_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE forum_messages_id_seq CASCADE');
        $this->addSql('DROP TABLE forum_messages');
        $this->addSql('DROP TABLE forum_message_attachments');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE forum_message_attachments_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE forum_messages_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE forum_messages (id INT NOT NULL, user_sender_id INT DEFAULT NULL, user_recipient_id INT DEFAULT NULL, in_reply_to_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, text TEXT NOT NULL, text_html TEXT DEFAULT NULL, is_active BOOLEAN DEFAULT \'true\' NOT NULL, is_read BOOLEAN DEFAULT \'false\' NOT NULL, is_need_confirm_read BOOLEAN DEFAULT \'false\' NOT NULL, read_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, is_show_for_sender BOOLEAN DEFAULT \'true\' NOT NULL, is_show_for_recipient BOOLEAN DEFAULT \'true\' NOT NULL, is_available_for_sender BOOLEAN DEFAULT \'true\' NOT NULL, is_available_for_recipient BOOLEAN DEFAULT \'true\' NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_fcf1937169e3f37a ON forum_messages (user_recipient_id)');
        $this->addSql('CREATE INDEX idx_fcf19371f6c43e79 ON forum_messages (user_sender_id)');
        $this->addSql('CREATE INDEX idx_fcf19371dd92dab8 ON forum_messages (in_reply_to_id)');
        $this->addSql('CREATE TABLE forum_message_attachments (id INT NOT NULL, object_id INT DEFAULT NULL, file_id BIGINT DEFAULT NULL, user_attached_by_id INT DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, is_active BOOLEAN DEFAULT \'true\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_668604aa232d562b ON forum_message_attachments (object_id)');
        $this->addSql('CREATE INDEX idx_668604aacc331a7d ON forum_message_attachments (user_attached_by_id)');
        $this->addSql('CREATE UNIQUE INDEX uniq_668604aa93cb796c ON forum_message_attachments (file_id)');
        $this->addSql('ALTER TABLE forum_messages ADD CONSTRAINT fk_fcf1937169e3f37a FOREIGN KEY (user_recipient_id) REFERENCES users (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE forum_messages ADD CONSTRAINT fk_fcf19371dd92dab8 FOREIGN KEY (in_reply_to_id) REFERENCES forum_messages (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE forum_messages ADD CONSTRAINT fk_fcf19371f6c43e79 FOREIGN KEY (user_sender_id) REFERENCES users (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE forum_message_attachments ADD CONSTRAINT fk_668604aa232d562b FOREIGN KEY (object_id) REFERENCES forum_messages (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE forum_message_attachments ADD CONSTRAINT fk_668604aa93cb796c FOREIGN KEY (file_id) REFERENCES files (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE forum_message_attachments ADD CONSTRAINT fk_668604aacc331a7d FOREIGN KEY (user_attached_by_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
