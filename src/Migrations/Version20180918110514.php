<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180918110514 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_push_queue ADD ttl TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
        $this->addSql('ALTER TABLE users_notifications ADD ttl TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');

        $this->addSql("UPDATE users_push_queue SET ttl = created_at + INTERVAL '1 month'");
        $this->addSql("UPDATE users_notifications SET ttl = created_at + INTERVAL '1 month'");

        $this->addSql('ALTER TABLE users_push_queue ALTER ttl SET NOT NULL');
        $this->addSql('ALTER TABLE users_notifications ALTER ttl SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_push_queue DROP ttl');
        $this->addSql('ALTER TABLE users_notifications DROP ttl');
    }
}
