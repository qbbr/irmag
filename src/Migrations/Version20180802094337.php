<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180802094337 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE actions_elements DROP CONSTRAINT FK_9BEDB27E1F1F2A24');
        $this->addSql('ALTER TABLE actions_elements ADD CONSTRAINT FK_9BEDB27E1F1F2A24 FOREIGN KEY (element_id) REFERENCES elements (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE actions_elements DROP CONSTRAINT fk_9bedb27e1f1f2a24');
        $this->addSql('ALTER TABLE actions_elements ADD CONSTRAINT fk_9bedb27e1f1f2a24 FOREIGN KEY (element_id) REFERENCES elements (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
