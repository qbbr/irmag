<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181204023045 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("UPDATE roles SET name = replace(name, 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER', 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_NOTIFICATION_QUEUE') WHERE name LIKE 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER_%'");
        $this->addSql("UPDATE roles SET description = replace(description, 'Администрирование - Рассыльщик уведомлений', 'Уведомления - Очередь') WHERE description LIKE '%Администрирование - Рассыльщик уведомлений%'");

        $this->addSql("UPDATE roles SET name = replace(name, 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT', 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_NOTIFICATION_EVENT') WHERE name LIKE 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_%'");
        $this->addSql("UPDATE roles SET description = replace(description, 'Администрирование', 'Уведомления') WHERE description LIKE '%Администрирование - События уведомлений%'");

        $this->addSql("UPDATE roles SET name = replace(name, 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION', 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_NOTIFICATION_SUBSCRIPTION') WHERE name LIKE 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SUBSCRIPTION_EDIT_%'");
        $this->addSql("UPDATE roles SET description = replace(description, 'Администрирование', 'Уведомления') WHERE description LIKE '%Администрирование - Подписки пользователей на события%'");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_READER', '[ADMIN] Идентификаторы PUSH уведомлений - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_VIEW', '[ADMIN] Идентификаторы PUSH уведомлений - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_LIST', '[ADMIN] Идентификаторы PUSH уведомлений - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_VIEW')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EDITOR', '[ADMIN] Идентификаторы PUSH уведомлений - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EDIT', '[ADMIN] Идентификаторы PUSH уведомлений - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_CREATE', '[ADMIN] Идентификаторы PUSH уведомлений - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_ADMIN', '[ADMIN] Идентификаторы PUSH уведомлений - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_DELETE', '[ADMIN] Идентификаторы PUSH уведомлений - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EXPORT', '[ADMIN] Идентификаторы PUSH уведомлений - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_PUSH_ID_ADMIN')
            );
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        //$this->addSql("UPDATE roles SET name = replace(name, 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_NOTIFICATION_QUEUE', 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_SENDER') WHERE name LIKE 'ROLE_IRMAG_ADMIN_NOTIFICATION_USER_NOTIFICATION_QUEUE_%'");
        //$this->addSql("UPDATE roles SET description = replace(description, 'Уведомления - Очередь', 'Администрирование - Рассыльщик уведомлений') WHERE description LIKE '%Уведомления - Очередь%'");
    }
}
