<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171207084411 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE elements_brands ADD tsv TSVECTOR DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN elements_brands.tsv IS \'(DC2Type:tsvector)\'');
        $this->addSql('ALTER TABLE elements_manufacturers ADD tsv TSVECTOR DEFAULT NULL');
        $this->addSql('COMMENT ON COLUMN elements_manufacturers.tsv IS \'(DC2Type:tsvector)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE elements_brands DROP tsv');
        $this->addSql('ALTER TABLE elements_manufacturers DROP tsv');
    }
}
