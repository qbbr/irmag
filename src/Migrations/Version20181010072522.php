<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181010072522 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('UPDATE orders SET waybill_id = NULL');
        $this->addSql('DELETE FROM waybill');
        $this->addSql('ALTER TABLE waybill DROP driver_fullname');
        $this->addSql('ALTER TABLE waybill ADD CONSTRAINT FK_D8ACA965C3423909 FOREIGN KEY (driver_id) REFERENCES users (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D8ACA965C3423909 ON waybill (driver_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE waybill DROP CONSTRAINT FK_D8ACA965C3423909');
        $this->addSql('DROP INDEX IDX_D8ACA965C3423909');
        $this->addSql('ALTER TABLE waybill ADD driver_fullname VARCHAR(255) NOT NULL');
    }
}
