<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180426071122 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE coupon_elements_lists_elements DROP CONSTRAINT fk_913a7924ad83');
        $this->addSql('DROP INDEX idx_913a7924ad83');
        $this->addSql('ALTER INDEX coupon_elements_lists_elements_pkey RENAME TO promocode_elements_lists_elements_pkey');
        $this->addSql('ALTER TABLE coupon_elements_lists_elements RENAME COLUMN coupon_elements_list_id TO promocode_elements_list_id');
        $this->addSql('ALTER TABLE coupon_elements_lists_elements ADD CONSTRAINT FK_913A7925FAB753F FOREIGN KEY (promocode_elements_list_id) REFERENCES coupon_elements_lists (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders RENAME COLUMN coupon_discount TO promocode_discount');
        $this->addSql('ALTER TABLE orders RENAME COLUMN coupon_code TO promocode');

        $this->addSql('ALTER TABLE coupons RENAME TO promocodes');
        $this->addSql('ALTER SEQUENCE coupons_id_seq RENAME TO promocodes_id_seq');
        $this->addSql('ALTER INDEX coupons_pkey RENAME TO promocodes_pkey');

        $this->addSql('ALTER TABLE coupon_elements_lists RENAME TO promocode_elements_lists');
        $this->addSql('ALTER SEQUENCE coupon_elements_lists_id_seq RENAME TO promocode_elements_lists_id_seq');
        $this->addSql('ALTER INDEX coupon_elements_lists_pkey RENAME TO promocode_elements_lists_pkey');

        $this->addSql('ALTER TABLE coupons_history RENAME TO promocodes_history');
        $this->addSql('ALTER INDEX coupons_history_pkey RENAME TO promocodes_history_pkey');

        $this->addSql('ALTER TABLE secret_coupon_page RENAME TO secret_promocode_page');
        $this->addSql('ALTER SEQUENCE secret_coupon_page_id_seq RENAME TO secret_promocode_page_id_seq');
        $this->addSql('ALTER INDEX secret_coupon_page_pkey RENAME TO secret_promocode_page_pkey');

        $this->addSql('ALTER TABLE coupon_elements_lists_elements RENAME TO promocode_elements_lists_elements');

        // i dont know why doctrine wants to rename indexes
        $this->addSql('ALTER INDEX idx_2eb4beb3a76ed395 RENAME TO IDX_CEB3D14A76ED395');
        $this->addSql('ALTER INDEX uniq_f564111877153098 RENAME TO UNIQ_F211125077153098');
        $this->addSql('ALTER INDEX idx_f5641118ca4556df RENAME TO IDX_F2111250CA4556DF');
        $this->addSql('ALTER INDEX uniq_7e828648989d9b62 RENAME TO UNIQ_5177583C989D9B62');
        $this->addSql('ALTER INDEX uniq_219e7f63c009be4b RENAME TO UNIQ_7B66190DC009BE4B');
        $this->addSql('ALTER INDEX uniq_219e7f639d32f035 RENAME TO UNIQ_7B66190D9D32F035');
        $this->addSql('ALTER INDEX idx_913a7921f1f2a24 RENAME TO IDX_71B20BB71F1F2A24');

        // rename roles
        $this->addSql("UPDATE roles SET description = replace(description, 'Купоны', 'Промокоды') WHERE name ILIKE '%COUPON%'");
        $this->addSql("UPDATE roles SET description = replace(description, 'Секретная страница купон', 'Секретная страница с промокодом') WHERE name ILIKE '%COUPON%'");
        $this->addSql("UPDATE roles SET description = replace(description, 'купон', 'промокод') WHERE name ILIKE '%COUPON%'");
        $this->addSql("UPDATE roles SET description = replace(description, 'Списки товаров', 'Товары по промокодам') WHERE name ILIKE '%COUPON%'");
        $this->addSql("UPDATE roles SET name = replace(name, 'COUPON', 'PROMOCODE') WHERE name ILIKE '%COUPON%'");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE promocodes RENAME TO coupons');
        $this->addSql('ALTER SEQUENCE promocodes_id_seq RENAME TO coupons_id_seq');
        $this->addSql('ALTER INDEX promocodes_pkey RENAME TO coupons_pkey');

        $this->addSql('ALTER TABLE promocode_elements_lists RENAME TO coupon_elements_lists ');
        $this->addSql('ALTER SEQUENCE promocode_elements_lists_id_seq RENAME TO coupon_elements_lists_id_seq');
        $this->addSql('ALTER INDEX promocode_elements_lists_pkey RENAME TO coupon_elements_lists_pkey');

        $this->addSql('ALTER TABLE promocodes_history RENAME TO coupons_history');
        $this->addSql('ALTER INDEX promocodes_history_pkey RENAME TO coupons_history_pkey');

        $this->addSql('ALTER TABLE secret_promocode_page RENAME TO secret_coupon_page');
        $this->addSql('ALTER SEQUENCE secret_promocode_page_id_seq RENAME TO secret_coupon_page_id_seq');
        $this->addSql('ALTER INDEX secret_promocode_page_pkey RENAME TO secret_coupon_page_pkey');

        $this->addSql('ALTER TABLE promocode_elements_lists_elements RENAME TO coupon_elements_lists_elements ');
        $this->addSql('ALTER TABLE coupon_elements_lists_elements DROP CONSTRAINT FK_913A7925FAB753F');
        $this->addSql('ALTER INDEX promocode_elements_lists_elements_pkey RENAME TO coupon_elements_lists_elements_pkey');
        $this->addSql('ALTER TABLE coupon_elements_lists_elements RENAME COLUMN promocode_elements_list_id TO coupon_elements_list_id');
        $this->addSql('ALTER TABLE coupon_elements_lists_elements ADD CONSTRAINT fk_913a7924ad83 FOREIGN KEY (coupon_elements_list_id) REFERENCES coupon_elements_lists (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_913a7924ad83 ON coupon_elements_lists_elements (coupon_elements_list_id)');
        $this->addSql('ALTER TABLE orders RENAME COLUMN promocode_discount TO coupon_discount');
        $this->addSql('ALTER TABLE orders RENAME COLUMN promocode TO coupon_code');

        // rename indexes
        $this->addSql('ALTER INDEX IDX_CEB3D14A76ED395 RENAME TO idx_2eb4beb3a76ed395');
        $this->addSql('ALTER INDEX UNIQ_F211125077153098 RENAME TO uniq_f564111877153098');
        $this->addSql('ALTER INDEX IDX_F2111250CA4556DF RENAME TO idx_f5641118ca4556df');
        $this->addSql('ALTER INDEX UNIQ_5177583C989D9B62 RENAME TO uniq_7e828648989d9b62');
        $this->addSql('ALTER INDEX UNIQ_7B66190DC009BE4B RENAME TO uniq_219e7f63c009be4b');
        $this->addSql('ALTER INDEX UNIQ_7B66190D9D32F035 RENAME TO uniq_219e7f639d32f035');
        $this->addSql('ALTER INDEX IDX_71B20BB71F1F2A24 RENAME TO idx_913a7921f1f2a24');

        // rename roles
        $this->addSql("UPDATE roles SET description = replace(description, 'Промокоды', 'Купоны') WHERE name ILIKE '%PROMOCODE%'");
        $this->addSql("UPDATE roles SET description = replace(description, 'Секретная страница с промокодом', 'Секретная страница купон') WHERE name ILIKE '%PROMOCODE%'");
        $this->addSql("UPDATE roles SET description = replace(description, 'промокод', 'купон') WHERE name ILIKE '%PROMOCODE%'");
        $this->addSql("UPDATE roles SET description = replace(description, 'Товары по промокодам', 'Списки товаров') WHERE name ILIKE '%PROMOCODE%'");
        $this->addSql("UPDATE roles SET name = replace(name, 'PROMOCODE', 'COUPON') WHERE name ILIKE '%PROMOCODE%'");
    }
}
