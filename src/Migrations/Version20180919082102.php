<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180919082102 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE orders_delivery_selfservice_methods_orders_delivery_times (order_delivery_selfservice_method_id INT NOT NULL, order_delivery_time_id INT NOT NULL, PRIMARY KEY(order_delivery_selfservice_method_id, order_delivery_time_id))');
        $this->addSql('CREATE INDEX IDX_BACC99F0C73F1DCD ON orders_delivery_selfservice_methods_orders_delivery_times (order_delivery_selfservice_method_id)');
        $this->addSql('CREATE INDEX IDX_BACC99F036E276F9 ON orders_delivery_selfservice_methods_orders_delivery_times (order_delivery_time_id)');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods_orders_delivery_times ADD CONSTRAINT FK_BACC99F0C73F1DCD FOREIGN KEY (order_delivery_selfservice_method_id) REFERENCES orders_delivery_selfservice_methods (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods_orders_delivery_times ADD CONSTRAINT FK_BACC99F036E276F9 FOREIGN KEY (order_delivery_time_id) REFERENCES orders_delivery_times (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE orders_delivery_selfservice_methods_orders_delivery_times');
    }
}
