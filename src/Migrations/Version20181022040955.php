<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181022040955 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE users_notification_queues_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE users_notification_queues (id INT NOT NULL, is_done BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, title VARCHAR(50) NOT NULL, body VARCHAR(150) NOT NULL, url VARCHAR(255) DEFAULT NULL, ttl TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE TABLE user_notification_queue_user (user_notification_queue_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(user_notification_queue_id, user_id))');
        $this->addSql('CREATE INDEX IDX_6A6092CCD26F2E75 ON user_notification_queue_user (user_notification_queue_id)');
        $this->addSql('CREATE INDEX IDX_6A6092CCA76ED395 ON user_notification_queue_user (user_id)');
        $this->addSql('ALTER TABLE user_notification_queue_user ADD CONSTRAINT FK_6A6092CCD26F2E75 FOREIGN KEY (user_notification_queue_id) REFERENCES users_notification_queues (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_notification_queue_user ADD CONSTRAINT FK_6A6092CCA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE user_notification_queue_user DROP CONSTRAINT FK_6A6092CCD26F2E75');
        $this->addSql('DROP SEQUENCE users_notification_queues_id_seq CASCADE');
        $this->addSql('DROP TABLE users_notification_queues');
        $this->addSql('DROP TABLE user_notification_queue_user');
    }
}
