<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180301074847 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_order_profiles ALTER payer_legal_ogrn TYPE VARCHAR(30)');
        $this->addSql('ALTER TABLE orders ALTER payer_legal_ogrn TYPE VARCHAR(30)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_order_profiles ALTER payer_legal_ogrn TYPE VARCHAR(20)');
        $this->addSql('ALTER TABLE orders ALTER payer_legal_ogrn TYPE VARCHAR(20)');
    }
}
