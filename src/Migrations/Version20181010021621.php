<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181010021621 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_WAYBILL_READER', '[ADMIN] Drive - Путёвки - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_WAYBILL_VIEW', '[ADMIN] Drive - Путёвки - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_WAYBILL_LIST', '[ADMIN] Drive - Путёвки - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_VIEW')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_WAYBILL_EDITOR', '[ADMIN] Drive - Путёвки - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_WAYBILL_EDIT', '[ADMIN] Drive - Путёвки - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_WAYBILL_CREATE', '[ADMIN] Drive - Путёвки - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_WAYBILL_ADMIN', '[ADMIN] Drive - Путёвки - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_WAYBILL_DELETE', '[ADMIN] Drive - Путёвки - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_WAYBILL_EXPORT', '[ADMIN] Drive - Путёвки - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_WAYBILL_ADMIN')
            );
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
