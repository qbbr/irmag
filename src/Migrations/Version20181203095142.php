<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181203095142 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE actions RENAME COLUMN min_condition_sum TO condition_min_sum');
        $this->addSql('ALTER TABLE actions RENAME COLUMN min_condition_count TO condition_min_count');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE actions RENAME COLUMN condition_min_sum TO min_condition_sum');
        $this->addSql('ALTER TABLE actions RENAME COLUMN condition_min_count TO min_condition_count');
    }
}
