<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181022062735 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DELETE FROM users_notifications');
        $this->addSql('ALTER TABLE users_notifications ADD event_id INT NOT NULL');
        $this->addSql('ALTER TABLE users_notifications ADD CONSTRAINT FK_69E5B8DE71F7E88B FOREIGN KEY (event_id) REFERENCES users_notification_events (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_69E5B8DE71F7E88B ON users_notifications (event_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notifications DROP CONSTRAINT FK_69E5B8DE71F7E88B');
        $this->addSql('DROP INDEX IDX_69E5B8DE71F7E88B');
        $this->addSql('ALTER TABLE users_notifications DROP event_id');
    }
}
