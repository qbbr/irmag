<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181225102850 extends AbstractMigration
{
    private const MESSAGE_NEW = 'message_new';

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE users_messages');
        $this->addSql('ALTER TABLE users DROP IF EXISTS is_notify_email_when_new_message_on_forum');

        $this->addSql(sprintf("
            DELETE FROM users_notification_subscriptions
            WHERE event_id = (SELECT id FROM users_notification_events WHERE name = '%s')
        ", self::MESSAGE_NEW));

        $this->addSql(sprintf("
            DELETE FROM users_notification_events
            WHERE name = '%s'
        ", self::MESSAGE_NEW));
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE users_messages_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE users_messages (id INT NOT NULL, from_id INT NOT NULL, to_id INT NOT NULL, message TEXT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_a043354c30354a65 ON users_messages (to_id)');
        $this->addSql('CREATE INDEX idx_a043354c78ced90b ON users_messages (from_id)');
        $this->addSql('ALTER TABLE users_messages ADD CONSTRAINT fk_a043354c30354a65 FOREIGN KEY (to_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_messages ADD CONSTRAINT fk_a043354c78ced90b FOREIGN KEY (from_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users ADD is_notify_email_when_new_message_on_forum BOOLEAN DEFAULT \'false\' NOT NULL');
    }
}
