<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181206072033 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('TRUNCATE users_notification_queues');
        $this->addSql('TRUNCATE users_notifications');

        $this->addSql('ALTER TABLE users_notification_queues ALTER body SET NOT NULL');
        $this->addSql('ALTER TABLE users_notifications ALTER body SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notifications ALTER body DROP NOT NULL');
        $this->addSql('ALTER TABLE users_notification_queues ALTER body DROP NOT NULL');
    }
}
