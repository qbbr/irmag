<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181018055610 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_push_ids ADD device VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE users_push_ids ADD ip VARCHAR(15) DEFAULT NULL');
        $this->addSql('ALTER TABLE users_push_ids ADD ua VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_push_ids DROP device');
        $this->addSql('ALTER TABLE users_push_ids DROP ip');
        $this->addSql('ALTER TABLE users_push_ids DROP ua');
    }
}
