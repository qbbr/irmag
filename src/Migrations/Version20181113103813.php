<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181113103813 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notification_queues ALTER body TYPE TEXT');
        $this->addSql('ALTER TABLE users_notification_queues ALTER body DROP DEFAULT');
        $this->addSql('ALTER TABLE users_notification_queues ALTER body TYPE TEXT');
        $this->addSql('ALTER TABLE users DROP is_notify_email_when_reply_on_forum');
        $this->addSql('ALTER TABLE users_notifications ALTER body TYPE TEXT');
        $this->addSql('ALTER TABLE users_notifications ALTER body DROP DEFAULT');
        $this->addSql('ALTER TABLE users_notifications ALTER body TYPE TEXT');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notifications ALTER body TYPE VARCHAR(150)');
        $this->addSql('ALTER TABLE users_notifications ALTER body DROP DEFAULT');
        $this->addSql('ALTER TABLE users_notification_queues ALTER body TYPE VARCHAR(150)');
        $this->addSql('ALTER TABLE users_notification_queues ALTER body DROP DEFAULT');
        $this->addSql('ALTER TABLE users ADD is_notify_email_when_reply_on_forum BOOLEAN DEFAULT \'true\' NOT NULL');
    }
}
