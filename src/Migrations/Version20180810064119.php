<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180810064119 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_READER', '[ADMIN] [ADMIN] Оформление заказа - Города доставки <=> Район - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_VIEW', '[ADMIN] [ADMIN] Оформление заказа - Города доставки <=> Район - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_LIST', '[ADMIN] [ADMIN] Оформление заказа - Города доставки <=> Район - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_VIEW')
        );");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_EDITOR', '[ADMIN] [ADMIN] Оформление заказа - Города доставки <=> Район - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_EDIT', '[ADMIN] [ADMIN] Оформление заказа - Города доставки <=> Район - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_CREATE', '[ADMIN] [ADMIN] Оформление заказа - Города доставки <=> Район - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_ADMIN', '[ADMIN] [ADMIN] Оформление заказа - Города доставки <=> Район - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_DELETE', '[ADMIN] [ADMIN] Оформление заказа - Города доставки <=> Район - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_EXPORT', '[ADMIN] [ADMIN] Оформление заказа - Города доставки <=> Район - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_DISTRICT_ADMIN')
            );
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
