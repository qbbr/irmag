<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181210094954 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE favorites_watching_diff (favorite_id INT NOT NULL, element_id INT NOT NULL, is_active BOOLEAN DEFAULT \'false\' NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(favorite_id, element_id))');
        $this->addSql('CREATE INDEX IDX_5934FCE6AA17481D ON favorites_watching_diff (favorite_id)');
        $this->addSql('CREATE INDEX IDX_5934FCE61F1F2A24 ON favorites_watching_diff (element_id)');
        $this->addSql('ALTER TABLE favorites_watching_diff ADD CONSTRAINT FK_5934FCE6AA17481D FOREIGN KEY (favorite_id) REFERENCES favorites (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE favorites_watching_diff ADD CONSTRAINT FK_5934FCE61F1F2A24 FOREIGN KEY (element_id) REFERENCES elements (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP TABLE favorites_watching_diff');
    }
}
