<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181204032406 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("UPDATE users_notification_events SET is_active = FALSE WHERE name = 'newsletter' OR name = 'favorite_element_price' OR name = 'favorite_element_is_active'");
        $this->addSql("UPDATE users_notification_events SET is_sms_disabled = TRUE WHERE name = 'favorite_element_is_active' OR name = 'favorite_element_price' OR name = 'action_new' OR name = 'order_status' OR name = 'news_new' OR name = 'forum_post_reply' OR name = 'blog_comment_reply' OR name = 'site_comment_reply' OR name = 'blog_post_new'");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
