<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180320144903 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE threads_comments DROP CONSTRAINT FK_AB551FA8727ACA70');
        $this->addSql('ALTER TABLE threads_comments ADD CONSTRAINT FK_AB551FA8727ACA70 FOREIGN KEY (parent_id) REFERENCES threads_comments (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE threads_comments DROP CONSTRAINT fk_ab551fa8727aca70');
        $this->addSql('ALTER TABLE threads_comments ADD CONSTRAINT fk_ab551fa8727aca70 FOREIGN KEY (parent_id) REFERENCES threads_comments (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
