<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180716061656 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        // backup old table data
        $this->addSql('CREATE TABLE users_referral_links_old AS SELECT * FROM users_referral_links');
        $this->addSql('DROP TABLE users_referral_links');
        // create new
        $this->addSql('CREATE TABLE users_referral_links (user_referral_id INT NOT NULL, user_owner_id INT NOT NULL, PRIMARY KEY(user_referral_id, user_owner_id))');
        $this->addSql('CREATE INDEX IDX_C7F4D92095D16D22 ON users_referral_links (user_referral_id)');
        $this->addSql('CREATE INDEX IDX_C7F4D9209EB185F9 ON users_referral_links (user_owner_id)');
        $this->addSql('ALTER TABLE users_referral_links ADD CONSTRAINT FK_C7F4D92095D16D22 FOREIGN KEY (user_referral_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_referral_links ADD CONSTRAINT FK_C7F4D9209EB185F9 FOREIGN KEY (user_owner_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        // fill cols from old
        $this->addSql('INSERT INTO users_referral_links (user_referral_id, user_owner_id) (SELECT user_id, user_referral_id FROM users_referral_links_old)');
        // dd old
        $this->addSql('DROP TABLE users_referral_links_old');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
