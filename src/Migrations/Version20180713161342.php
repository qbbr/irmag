<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180713161342 extends AbstractMigration
{
    const SHORTNAME = 'fast';

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_delivery_times ADD shortname VARCHAR(32) DEFAULT NULL');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_AAB13E7864082763 ON orders_delivery_times (shortname)');

        $this->addSql(sprintf("
            INSERT INTO orders_delivery_times
                (id, name, shortname, time_limit, is_active)
                VALUES(nextval('orders_delivery_times_id_seq'), 'Срочно', '%s', '18:00:00', true)
        ", self::SHORTNAME));
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(sprintf("DELETE FROM orders_delivery_times WHERE shortname = '%s'", self::SHORTNAME));
        $this->addSql('ALTER TABLE orders_delivery_times DROP shortname');
        $this->addSql('DROP INDEX UNIQ_AAB13E7864082763');
    }
}
