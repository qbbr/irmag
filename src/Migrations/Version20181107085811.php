<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181107085811 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DELETE FROM users_notification_queues');
        $this->addSql('DROP TABLE user_notification_queue_user');
        $this->addSql('ALTER TABLE users_notification_queues ADD user_id INT NOT NULL');
        $this->addSql('ALTER TABLE users_notification_queues ADD CONSTRAINT FK_69F32E51A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_69F32E51A76ED395 ON users_notification_queues (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE TABLE user_notification_queue_user (user_notification_queue_id INT NOT NULL, user_id INT NOT NULL, PRIMARY KEY(user_notification_queue_id, user_id))');
        $this->addSql('CREATE INDEX idx_6a6092cca76ed395 ON user_notification_queue_user (user_id)');
        $this->addSql('CREATE INDEX idx_6a6092ccd26f2e75 ON user_notification_queue_user (user_notification_queue_id)');
        $this->addSql('ALTER TABLE user_notification_queue_user ADD CONSTRAINT fk_6a6092ccd26f2e75 FOREIGN KEY (user_notification_queue_id) REFERENCES users_notification_queues (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE user_notification_queue_user ADD CONSTRAINT fk_6a6092cca76ed395 FOREIGN KEY (user_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_notification_queues DROP CONSTRAINT FK_69F32E51A76ED395');
        $this->addSql('DROP INDEX IDX_69F32E51A76ED395');
        $this->addSql('ALTER TABLE users_notification_queues DROP user_id');
    }
}
