<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181111070803 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE orders_elements_changes_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE orders_changes (id INT NOT NULL, delivery_time_id INT DEFAULT NULL, payment_method_id INT DEFAULT NULL, status_id INT DEFAULT NULL, mixed_payment_id INT DEFAULT NULL, delivery_date DATE NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D155CA4654F462E5 ON orders_changes (delivery_time_id)');
        $this->addSql('CREATE INDEX IDX_D155CA465AA1164F ON orders_changes (payment_method_id)');
        $this->addSql('CREATE INDEX IDX_D155CA466BF700BD ON orders_changes (status_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_D155CA4654DA761 ON orders_changes (mixed_payment_id)');
        $this->addSql('CREATE TABLE orders_elements_changes (id INT NOT NULL, order_id INT NOT NULL, element_id INT NOT NULL, price NUMERIC(10, 2) NOT NULL, amount SMALLINT DEFAULT 1 NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A041A88F8D9F6D38 ON orders_elements_changes (order_id)');
        $this->addSql('CREATE INDEX IDX_A041A88F1F1F2A24 ON orders_elements_changes (element_id)');
        $this->addSql('CREATE UNIQUE INDEX order_element_changes_unique_indx ON orders_elements_changes (order_id, element_id)');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT FK_D155CA4654F462E5 FOREIGN KEY (delivery_time_id) REFERENCES orders_delivery_times (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT FK_D155CA465AA1164F FOREIGN KEY (payment_method_id) REFERENCES orders_payment_methods (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT FK_D155CA466BF700BD FOREIGN KEY (status_id) REFERENCES orders_statuses (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT FK_D155CA4654DA761 FOREIGN KEY (mixed_payment_id) REFERENCES orders_mixed_payments (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_elements_changes ADD CONSTRAINT FK_A041A88F8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders_changes (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_elements_changes ADD CONSTRAINT FK_A041A88F1F1F2A24 FOREIGN KEY (element_id) REFERENCES elements (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_elements_changes DROP CONSTRAINT FK_A041A88F8D9F6D38');
        $this->addSql('DROP SEQUENCE orders_elements_changes_id_seq CASCADE');
        $this->addSql('DROP TABLE orders_changes');
        $this->addSql('DROP TABLE orders_elements_changes');
    }
}
