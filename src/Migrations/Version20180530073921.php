<?php

namespace Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180530073921 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE users_referral_order_payouts_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE users_referral_order_payouts (id INT NOT NULL, user_referral_id INT DEFAULT NULL, user_owner_id INT DEFAULT NULL, order_id INT NOT NULL, user_bonus_history_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B84073BF95D16D22 ON users_referral_order_payouts (user_referral_id)');
        $this->addSql('CREATE INDEX IDX_B84073BF9EB185F9 ON users_referral_order_payouts (user_owner_id)');
        $this->addSql('CREATE INDEX IDX_B84073BF8D9F6D38 ON users_referral_order_payouts (order_id)');
        $this->addSql('CREATE INDEX IDX_B84073BF9C79A4EE ON users_referral_order_payouts (user_bonus_history_id)');
        $this->addSql('ALTER TABLE users_referral_order_payouts ADD CONSTRAINT FK_B84073BF95D16D22 FOREIGN KEY (user_referral_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_referral_order_payouts ADD CONSTRAINT FK_B84073BF9EB185F9 FOREIGN KEY (user_owner_id) REFERENCES users (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_referral_order_payouts ADD CONSTRAINT FK_B84073BF8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE users_referral_order_payouts ADD CONSTRAINT FK_B84073BF9C79A4EE FOREIGN KEY (user_bonus_history_id) REFERENCES users_bonus_history (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE users_referral_order_payouts_id_seq CASCADE');
        $this->addSql('DROP TABLE users_referral_order_payouts');
    }
}
