<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180302084049 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_READER', '[ADMIN] [ADMIN] Оформление заказа - Города доставки (бан дат) - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_VIEW', '[ADMIN] [ADMIN] Оформление заказа - Города доставки (бан дат) - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_LIST', '[ADMIN] [ADMIN] Оформление заказа - Города доставки (бан дат) - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_VIEW')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_EDITOR', '[ADMIN] [ADMIN] Оформление заказа - Города доставки (бан дат) - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_EDIT', '[ADMIN] [ADMIN] Оформление заказа - Города доставки (бан дат) - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_CREATE', '[ADMIN] [ADMIN] Оформление заказа - Города доставки (бан дат) - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_ADMIN', '[ADMIN] [ADMIN] Оформление заказа - Города доставки (бан дат) - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_DELETE', '[ADMIN] [ADMIN] Оформление заказа - Города доставки (бан дат) - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_EXPORT', '[ADMIN] [ADMIN] Оформление заказа - Города доставки (бан дат) - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_ADMIN')
            );
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("DELETE FROM roles_childrens WHERE children_id IN (SELECT id FROM roles WHERE name LIKE 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_%')");
        $this->addSql("DELETE FROM roles WHERE name LIKE 'ROLE_IRMAG_ADMIN_ORDER_DELIVERY_CITY_BAN_DAY_%'");
    }
}
