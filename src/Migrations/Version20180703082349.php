<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180703082349 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE orders_delivery_cities_selfservice_methods_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE orders_delivery_cities_selfservice_methods (id INT NOT NULL, delivery_city_id INT NOT NULL, selfservice_id INT NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_53160CE581B2DD71 ON orders_delivery_cities_selfservice_methods (delivery_city_id)');
        $this->addSql('CREATE INDEX IDX_53160CE5CF863A76 ON orders_delivery_cities_selfservice_methods (selfservice_id)');
        $this->addSql('CREATE TABLE order_delivery_city_selfservice_method_order_payment_method (order_delivery_city_selfservice_method_id INT NOT NULL, order_payment_method_id INT NOT NULL, PRIMARY KEY(order_delivery_city_selfservice_method_id, order_payment_method_id))');
        $this->addSql('CREATE INDEX IDX_1D76FB66C4A86A83 ON order_delivery_city_selfservice_method_order_payment_method (order_delivery_city_selfservice_method_id)');
        $this->addSql('CREATE INDEX IDX_1D76FB664EC25C14 ON order_delivery_city_selfservice_method_order_payment_method (order_payment_method_id)');
        $this->addSql('ALTER TABLE orders_delivery_cities_selfservice_methods ADD CONSTRAINT FK_53160CE581B2DD71 FOREIGN KEY (delivery_city_id) REFERENCES orders_delivery_cities (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_delivery_cities_selfservice_methods ADD CONSTRAINT FK_53160CE5CF863A76 FOREIGN KEY (selfservice_id) REFERENCES orders_delivery_selfservice_methods (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_delivery_city_selfservice_method_order_payment_method ADD CONSTRAINT FK_1D76FB66C4A86A83 FOREIGN KEY (order_delivery_city_selfservice_method_id) REFERENCES orders_delivery_cities_selfservice_methods (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE order_delivery_city_selfservice_method_order_payment_method ADD CONSTRAINT FK_1D76FB664EC25C14 FOREIGN KEY (order_payment_method_id) REFERENCES orders_payment_methods (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_53160CE581B2DD71CF863A76 ON orders_delivery_cities_selfservice_methods (delivery_city_id, selfservice_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE order_delivery_city_selfservice_method_order_payment_method DROP CONSTRAINT FK_1D76FB66C4A86A83');
        $this->addSql('DROP SEQUENCE orders_delivery_cities_selfservice_methods_id_seq CASCADE');
        $this->addSql('DROP TABLE orders_delivery_cities_selfservice_methods');
        $this->addSql('DROP TABLE order_delivery_city_selfservice_method_order_payment_method');
    }
}
