<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180418070430 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_READER', '[ADMIN] Интернет-магазин - Списки товаров - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_VIEW', '[ADMIN] Интернет-магазин - Списки товаров - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_LIST', '[ADMIN] Интернет-магазин - Списки товаров - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_VIEW')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_EDITOR', '[ADMIN] Интернет-магазин - Списки товаров - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_EDIT', '[ADMIN] Интернет-магазин - Списки товаров - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_CREATE', '[ADMIN] Интернет-магазин - Списки товаров - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_ADMIN', '[ADMIN] Интернет-магазин - Списки товаров - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_DELETE', '[ADMIN] Интернет-магазин - Списки товаров - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_EXPORT', '[ADMIN] Интернет-магазин - Списки товаров - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_ADMIN')
            );
        ");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql("DELETE FROM roles WHERE name LIKE 'ROLE_IRMAG_ADMIN_ECOMMERCE_COUPON_ELEMENTS_LIST_%'");
    }
}
