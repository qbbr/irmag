<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180803052726 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE sections ADD seo_keywords VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE sections ADD seo_description VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE sections ADD elements_count INT DEFAULT 0 NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE sections DROP seo_keywords');
        $this->addSql('ALTER TABLE sections DROP seo_description');
        $this->addSql('ALTER TABLE sections DROP elements_count');
    }
}
