<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181016091437 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_READER', '[ADMIN] Администрирование - События уведомлений - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_VIEW', '[ADMIN] Администрирование - События уведомлений - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_LIST', '[ADMIN] Администрирование - События уведомлений - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_VIEW')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_EDITOR', '[ADMIN] Администрирование - События уведомлений - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_EDIT', '[ADMIN] Администрирование - События уведомлений - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_CREATE', '[ADMIN] Администрирование - События уведомлений - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_ADMIN', '[ADMIN] Администрирование - События уведомлений - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_DELETE', '[ADMIN] Администрирование - События уведомлений - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_EXPORT', '[ADMIN] Администрирование - События уведомлений - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_EVENT_ADMIN')
            );
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("DELETE FROM roles WHERE name LIKE 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_NOTIFICATION_%'");
    }
}
