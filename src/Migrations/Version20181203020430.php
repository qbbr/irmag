<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181203020430 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_elements_changes ALTER price DROP NOT NULL');
        $this->addSql('ALTER TABLE orders_elements_changes ALTER amount DROP DEFAULT');
        $this->addSql('ALTER TABLE orders_elements_changes ALTER amount DROP NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_elements_changes ALTER price SET NOT NULL');
        $this->addSql('ALTER TABLE orders_elements_changes ALTER amount SET DEFAULT 1');
        $this->addSql('ALTER TABLE orders_elements_changes ALTER amount SET NOT NULL');
    }
}
