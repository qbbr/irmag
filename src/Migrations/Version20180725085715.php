<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180725085715 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            INSERT INTO orders_statuses
                (id, name, shortname)
            VALUES
                (nextval('orders_statuses_id_seq'), 'Доставлен в пункт самовывоза', 'E'),
                (nextval('orders_statuses_id_seq'), 'Выдан из пункта самовывоза', 'V')
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            DELETE FROM orders_statuses
            WHERE shortname IN ('E', 'V')
        ");
    }
}
