<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181016083837 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notification_events ADD is_push_disabled BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE users_notification_events ADD is_email_disabled BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE users_notification_events ADD is_site_disabled BOOLEAN DEFAULT \'false\' NOT NULL');
        $this->addSql('ALTER TABLE users_notification_events ADD is_sms_disabled BOOLEAN DEFAULT \'false\' NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE users_notification_events DROP is_push_disabled');
        $this->addSql('ALTER TABLE users_notification_events DROP is_email_disabled');
        $this->addSql('ALTER TABLE users_notification_events DROP is_site_disabled');
        $this->addSql('ALTER TABLE users_notification_events DROP is_sms_disabled');
    }
}
