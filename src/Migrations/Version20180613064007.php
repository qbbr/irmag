<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180613064007 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("UPDATE orders_online_payments SET need_to_sync = false WHERE need_to_sync = true AND provider_id = 
            (SELECT id FROM orders_online_payments_providers WHERE shortname = 'openbank')
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql(sprintf("UPDATE orders_online_payments SET need_to_sync = true WHERE provider_id = 
            (SELECT id FROM orders_online_payments_providers WHERE shortname = 'openbank')
            AND created_at > '%s' 
        ", '2018-01-01 00:00:00'));
    }
}
