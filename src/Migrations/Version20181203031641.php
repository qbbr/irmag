<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181203031641 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE orders_changes_mixed_payments_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE orders_changes_mixed_payments (id INT NOT NULL, order_id INT NOT NULL, sum_cash NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, sum_card NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, sum_certificate NUMERIC(10, 2) DEFAULT \'0\' NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_E21138EC8D9F6D38 ON orders_changes_mixed_payments (order_id)');
        $this->addSql('ALTER TABLE orders_changes_mixed_payments ADD CONSTRAINT FK_E21138EC8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders_changes (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders_changes DROP CONSTRAINT FK_D155CA4654DA761');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT FK_D155CA4654DA761 FOREIGN KEY (mixed_payment_id) REFERENCES orders_changes_mixed_payments (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_changes DROP CONSTRAINT FK_D155CA4654DA761');
        $this->addSql('DROP SEQUENCE orders_changes_mixed_payments_id_seq CASCADE');
        $this->addSql('DROP TABLE orders_changes_mixed_payments');
        $this->addSql('ALTER TABLE orders_changes DROP CONSTRAINT fk_d155ca4654da761');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT fk_d155ca4654da761 FOREIGN KEY (mixed_payment_id) REFERENCES orders_mixed_payments (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
