<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181204083505 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE orders_changes_statuses_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE orders_changes_statuses (id INT NOT NULL, name VARCHAR(255) NOT NULL, shortname VARCHAR(32) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8B51739964082763 ON orders_changes_statuses (shortname)');
        $this->addSql('ALTER TABLE orders_changes DROP CONSTRAINT FK_D155CA466BF700BD');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT FK_D155CA466BF700BD FOREIGN KEY (status_id) REFERENCES orders_changes_statuses (id) NOT DEFERRABLE INITIALLY IMMEDIATE');

        $this->addSql("
            INSERT INTO orders_changes_statuses (id, name, shortname)
            VALUES
                (nextval('orders_changes_statuses_id_seq'), 'Оплачен', 'P'),
                (nextval('orders_changes_statuses_id_seq'), 'Перенесён', 'M'),
                (nextval('orders_changes_statuses_id_seq'), 'Отменён', 'C')
        ");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders_changes DROP CONSTRAINT FK_D155CA466BF700BD');
        $this->addSql('DROP SEQUENCE orders_changes_statuses_id_seq CASCADE');
        $this->addSql('DROP TABLE orders_changes_statuses');
        $this->addSql('ALTER TABLE orders_changes DROP CONSTRAINT fk_d155ca466bf700bd');
        $this->addSql('ALTER TABLE orders_changes ADD CONSTRAINT fk_d155ca466bf700bd FOREIGN KEY (status_id) REFERENCES orders_statuses (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }
}
