<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180704054019 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders ADD selfservice_method_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEFC5D11B1 FOREIGN KEY (selfservice_method_id) REFERENCES orders_delivery_selfservice_methods (id) ON DELETE SET NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E52FFDEEFC5D11B1 ON orders (selfservice_method_id)');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods ADD shortname VARCHAR(32) DEFAULT NULL');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods RENAME COLUMN delivery_street TO address');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods ADD description TEXT DEFAULT NULL');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods ADD is_active BOOLEAN DEFAULT \'true\' NOT NULL');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods DROP delivery_house');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A8B6595B64082763 ON orders_delivery_selfservice_methods (shortname)');

        $this->addSql("UPDATE orders_delivery_selfservice_methods SET shortname = 'irmag', address = 'Восточный пер., 8/2' WHERE name = 'ИРМАГ'");
        $this->addSql("UPDATE orders_delivery_selfservice_methods SET shortname = 'klass_market', address = 'Юрия Тена, 21' WHERE name = 'Класс-Маркет'");
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods ALTER COLUMN shortname SET NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders DROP CONSTRAINT FK_E52FFDEEFC5D11B1');
        $this->addSql('DROP INDEX IDX_E52FFDEEFC5D11B1');
        $this->addSql('ALTER TABLE orders DROP selfservice_method_id');
        $this->addSql('DROP INDEX UNIQ_A8B6595B64082763');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods ADD delivery_house VARCHAR(50) DEFAULT NULL');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods DROP shortname');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods DROP description');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods DROP is_active');
        $this->addSql('ALTER TABLE orders_delivery_selfservice_methods RENAME COLUMN address TO delivery_street');
    }
}
