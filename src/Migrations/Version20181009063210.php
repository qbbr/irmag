<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20181009063210 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE waybill ADD total_summ NUMERIC(10, 2) DEFAULT NULL');
        $this->addSql('ALTER TABLE waybill ADD sum_cash NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE waybill ADD sum_card NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE waybill ADD sum_certificate NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE waybill ADD sum_rs NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE waybill ADD sum_online NUMERIC(10, 2) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE waybill ADD driver_id INT NOT NULL');
        $this->addSql('ALTER TABLE waybill ADD driver_fullname VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE waybill ADD date DATE NOT NULL');
        $this->addSql('ALTER TABLE waybill ADD comment TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE waybill DROP total_summ');
        $this->addSql('ALTER TABLE waybill DROP sum_cash');
        $this->addSql('ALTER TABLE waybill DROP sum_card');
        $this->addSql('ALTER TABLE waybill DROP sum_certificate');
        $this->addSql('ALTER TABLE waybill DROP sum_rs');
        $this->addSql('ALTER TABLE waybill DROP sum_online');
        $this->addSql('ALTER TABLE waybill DROP driver_id');
        $this->addSql('ALTER TABLE waybill DROP driver_fullname');
        $this->addSql('ALTER TABLE waybill DROP date');
        $this->addSql('ALTER TABLE waybill DROP comment');
    }
}
