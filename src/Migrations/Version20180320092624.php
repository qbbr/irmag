<?php

namespace Migrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180320092624 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_READER', '[ADMIN] Администрирование - История операций по бонусному счёту - Читатель');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_VIEW', '[ADMIN] Администрирование - История операций по бонусному счёту - Просмотр');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_LIST', '[ADMIN] Администрирование - История операций по бонусному счёту - Список');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_VIEW')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_READER'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_LIST')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_EDITOR', '[ADMIN] Администрирование - История операций по бонусному счёту - Редактор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_EDIT', '[ADMIN] Администрирование - История операций по бонусному счёту - Редактировать');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_CREATE', '[ADMIN] Администрирование - История операций по бонусному счёту - Создавать');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_EDIT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_CREATE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_EDITOR'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_READER')
            );
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_ADMIN', '[ADMIN] Администрирование - История операций по бонусному счёту - Администратор');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_DELETE', '[ADMIN] Администрирование - История операций по бонусному счёту - Удалять');
        ");

        $this->addSql("
            INSERT INTO roles (id, name, description)
            VALUES (NEXTVAL('roles_id_seq'), 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_EXPORT', '[ADMIN] Администрирование - История операций по бонусному счёту - Экспорт');
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_DELETE')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_EXPORT')
            );
        ");

        $this->addSql("
            INSERT INTO roles_childrens (role_id, children_id)
            VALUES (
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_ADMIN'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_EDITOR')
            );
        ");

        $this->addSql("
            INSERT INTO groups_roles (group_id, role_id)
            VALUES (
                (SELECT id FROM groups WHERE name = 'Администраторы'),
                (SELECT id FROM roles WHERE name = 'ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_BONUS_HISTORY_ADMIN')
            );
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
    }
}
