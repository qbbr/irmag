<?php

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20180710081125 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE blog_posts_attachments ALTER is_active SET DEFAULT \'true\'');
        $this->addSql('ALTER TABLE forum_post_attachments ALTER is_active SET DEFAULT \'true\'');
        $this->addSql('ALTER TABLE forum_message_attachments ALTER is_active SET DEFAULT \'true\'');
        $this->addSql('ALTER TABLE threads_comments_attachments ALTER is_active SET DEFAULT \'true\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE blog_posts_attachments ALTER is_active DROP DEFAULT');
        $this->addSql('ALTER TABLE forum_message_attachments ALTER is_active DROP DEFAULT');
        $this->addSql('ALTER TABLE threads_comments_attachments ALTER is_active DROP DEFAULT');
        $this->addSql('ALTER TABLE forum_post_attachments ALTER is_active DROP DEFAULT');
    }
}
