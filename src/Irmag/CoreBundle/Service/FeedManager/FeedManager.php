<?php

namespace Irmag\CoreBundle\Service\FeedManager;

use Symfony\Component\HttpFoundation\Response;
use Irmag\CoreBundle\Model\Feed;

class FeedManager
{
    /**
     * @var string
     */
    const TEMPLATE = '@IrmagCore/Feed/feed.atom.twig';

    /**
     * @var \Twig_Environment
     */
    private $twig;

    /**
     * @var Feed
     */
    private $feed;

    /**
     * @param \Twig_Environment $twig Twig engine
     */
    public function __construct(
        \Twig_Environment $twig
    ) {
        $this->twig = $twig;
    }

    /**
     * @param Feed $feed
     *
     * @return FeedManager
     */
    public function setFeed(Feed $feed): self
    {
        $this->feed = $feed;

        return $this;
    }

    /**
     * @throws FeedManagerException If feed model is not set
     *
     * @return Response
     */
    public function getResponse(): Response
    {
        if (null === $this->feed) {
            throw new FeedManagerException('Feed model is not set (use setFeed method).');
        }

        $content = $this->twig->render(self::TEMPLATE, ['feed' => $this->feed]);

        return new Response($content, 200, ['Content-Type' => 'application/atom+xml']);
    }
}
