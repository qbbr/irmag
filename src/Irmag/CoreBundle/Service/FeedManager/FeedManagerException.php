<?php

namespace Irmag\CoreBundle\Service\FeedManager;

use Irmag\CoreBundle\Exception\IrmagException;

class FeedManagerException extends IrmagException
{
}
