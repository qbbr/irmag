<?php

namespace Irmag\CoreBundle\Twig;

use Symfony\Bridge\Twig\Extension\HttpKernelExtension;
use Symfony\Bridge\Twig\Extension\HttpKernelRuntime;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\HttpKernel\Controller\ControllerReference;
use Twig\TwigFunction;

/**
 * XXX: Unused.
 */
class RenderCachedExtension extends HttpKernelExtension
{
    /**
     * @var HttpKernelRuntime
     */
    private $httpKernelRuntime;

    /**
     * @var AdapterInterface
     */
    private $cache;

    /**
     * @var string
     */
    private $environment;

    /**
     * @param HttpKernelRuntime $httpKernelRuntime
     * @param AdapterInterface  $cache
     * @param string            $environment
     */
    public function __construct(
        HttpKernelRuntime $httpKernelRuntime,
        AdapterInterface $cache,
        string $environment
    ) {
        $this->httpKernelRuntime = $httpKernelRuntime;
        $this->cache = $cache;
        $this->environment = $environment;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('render_cached', [$this, 'renderCached'], [
                'is_safe' => ['html'],
            ]),
        ];
    }

    /**
     * @param ControllerReference $controllerReference
     * @param array               $options
     * @param string              $cacheId             The cache id
     *
     * @return string
     */
    public function renderCached(ControllerReference $controllerReference, $options = [], $cacheId = null)
    {
        if ('dev' === $this->environment) {
            return $this->httpKernelRuntime->renderFragment($controllerReference, $options);
        }

        if (null === $cacheId) {
            $cacheId = $controllerReference->controller;

            if (!empty($controllerReference->attributes)) {
                $cacheId .= json_encode($controllerReference->attributes);
            }

            if (!empty($controllerReference->query)) {
                $cacheId .= json_encode($controllerReference->query);
            }
        }

        $cachedRenderedContent = $this->cache->getItem($cacheId);

        if (!$cachedRenderedContent->isHit()) {
            $renderedContent = $this->httpKernelRuntime->renderFragment($controllerReference, $options);
            $cachedRenderedContent->set($renderedContent);
            $this->cache->save($cachedRenderedContent);
        } else {
            $renderedContent = $cachedRenderedContent->get();
        }

        return $renderedContent;
    }

    /**
     * @deprecated since 1.26 (to be removed in 2.0), not used anymore internally, but n redefine HttpKernelExtension.
     *
     * @return string
     */
    public function getName()
    {
        return 'irmag_render_cached';
    }
}
