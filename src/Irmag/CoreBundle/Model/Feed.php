<?php

namespace Irmag\CoreBundle\Model;

class Feed
{
    private $id;
    private $title;
    private $subtitle;
    private $linkSelf;
    private $link;
    private $updated;
    private $author;
    private $entries = [];

    public function setId($id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setTitle($title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setSubtitle($subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getSubtitle()
    {
        return $this->subtitle;
    }

    public function setLinkSelf($linkSelf): self
    {
        $this->linkSelf = $linkSelf;

        return $this;
    }

    public function getLinkSelf()
    {
        return $this->linkSelf;
    }

    public function setLink($link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getLink()
    {
        return $this->link;
    }

    public function setUpdated(\DateTime $updated): self
    {
        $this->updated = $updated;

        return $this;
    }

    public function getUpdated()
    {
        return $this->updated;
    }

    public function setAuthor($author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getAuthor()
    {
        return $this->author;
    }

    public function addEntry(FeedEntry $entity): self
    {
        $this->entries[] = $entity;

        return $this;
    }

    public function getEntries(): array
    {
        return $this->entries;
    }
}
