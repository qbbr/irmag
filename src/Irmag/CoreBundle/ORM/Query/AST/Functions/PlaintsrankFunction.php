<?php

namespace Irmag\CoreBundle\ORM\Query\AST\Functions;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

/**
 * PlainssrankFunction ::= "PLAINTSRANK" "(" StringPrimary "," StringPrimary ")".
 */
class PlaintsrankFunction extends FunctionNode
{
    public $fieldName = null;
    public $queryString = null;
    public $configuration = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->fieldName = $parser->StringPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->queryString = $parser->StringPrimary();

        $lexer = $parser->getLexer();
        if ($lexer->isNextToken(Lexer::T_COMMA)) {
            $parser->match(Lexer::T_COMMA);
            $this->configuration = $parser->StringPrimary();
        }

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        if ($this->configuration) {
            return sprintf("ts_rank(%s, plainto_tsquery(%s, translate(%s, 'ё', 'е')))",
                $this->fieldName->dispatch($sqlWalker),
                $this->configuration->dispatch($sqlWalker),
                $this->queryString->dispatch($sqlWalker));
        }

        return sprintf("ts_rank(%s, plainto_tsquery(translate(%s, 'ё', 'е')))",
                $this->fieldName->dispatch($sqlWalker),
                $this->queryString->dispatch($sqlWalker));
    }
}
