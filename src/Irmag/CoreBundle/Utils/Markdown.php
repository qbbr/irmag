<?php

namespace Irmag\CoreBundle\Utils;

class Markdown
{
    /**
     * @var \Parsedown
     */
    private $parser;

    public function __construct()
    {
        $this->parser = new \Parsedown();
    }

    /**
     * Конвертирует Markdown в HTML.
     *
     * @param string $md Markdown text
     *
     * @return string HTML
     */
    public function toHtml(string $md): string
    {
        return $this->parser->text($md);
    }
}
