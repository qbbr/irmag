<?php

namespace Irmag\CoreBundle\Utils;

class TokenGenerator
{
    /**
     * Возвращает случайный token.
     *
     * @return string
     */
    public static function generateToken(): string
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}
