<?php

namespace Irmag\CoreBundle\Utils;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;

class PropertyFiller
{
    /**
     * @var PropertyAccessorInterface
     */
    private $accessor;

    public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * Заполняет значения свойств из одного класса в другой.
     *
     * @param \object $fromClass  От кого
     * @param \object $toClass    Кому
     * @param array   $properties Свойства
     */
    public function fill(object $fromClass, object $toClass, array $properties): void
    {
        foreach ($properties as $property) {
            $value = $this->accessor->getValue($fromClass, $property);

            if ($value) {
                $this->accessor->setValue($toClass, $property, $value);
            }
        }
    }

    /**
     * Заполняет значения свойств.
     *
     * @param object $class      Класс
     * @param array  $properties Свойства
     */
    public function set(object $class, array $properties): void
    {
        foreach ($properties as $name => $value) {
            $this->accessor->setValue($class, $name, $value);
        }
    }
}
