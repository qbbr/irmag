<?php

namespace Irmag\CoreBundle\Utils;

class Formatter
{
    /**
     * Formats bytes into a human readable string.
     *
     * @param int $bytes
     *
     * @return string Formatted string
     */
    public static function formatBytes(int $bytes): string
    {
        if ($bytes > 1024 * 1024 * 1024) {
            return round($bytes / 1024 / 1024 / 1024, 2).' GB';
        } elseif ($bytes > 1024 * 1024) {
            return round($bytes / 1024 / 1024, 2).' MB';
        } elseif ($bytes > 1024) {
            return round($bytes / 1024, 2).' KB';
        }

        return $bytes.' B';
    }

    /**
     * Formats grams into a human readable string.
     *
     * @param int $grams
     *
     * @return string
     */
    public static function formatGrams(int $grams): string
    {
        if ($grams > 1000) {
            return round($grams / 1000, 2).' кг.';
        }

        return $grams.' г.';
    }

    /**
     * SpecialPrice -> special-price.
     *
     * @param string $string
     *
     * @return string
     */
    public static function camelCaseToDashLowerCase(string $string): string
    {
        return ltrim(mb_strtolower(preg_replace('/([A-Z])/', '-$1', $string)), '-');
    }
}
