<?php

namespace Irmag\CoreBundle\Monolog\Logger;

use Doctrine\DBAL\Exception\ConnectionException;
use Monolog\Handler\FingersCrossed\ErrorLevelActivationStrategy;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Irmag\SiteBundle\Service\SearchQueryNormalizerException;

class ActivationStrategy extends ErrorLevelActivationStrategy
{
    public function __construct()
    {
        parent::__construct('error');
    }

    /**
     * {@inheritdoc}
     */
    public function isHandlerActivated(array $record)
    {
        $isActivated = parent::isHandlerActivated($record);

        if ($isActivated && isset($record['context']['exception'])) {
            /** @var \Exception $exception */
            $exception = $record['context']['exception'];

            if ($exception instanceof HttpException) {
                return !\in_array($exception->getStatusCode(), [403, 404, 405], true);
            }

            if ($exception instanceof SearchQueryNormalizerException) {
                return false;
            }

            if ($exception instanceof ConnectionException) {
                return false;
            }
        }

        return $isActivated;
    }
}
