<?php

namespace Irmag\CoreBundle\Monolog\Logger;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\RestApiBundle\EventListener\ApiRequestListener;

class ExtraProcessor
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param RequestStack          $requestStack
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        RequestStack $requestStack,
        TokenStorageInterface $tokenStorage
    ) {
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param array $record
     *
     * @return array
     */
    public function processRecord(array $record): array
    {
        $masterRequest = $this->requestStack->getMasterRequest();

        if ($token = $this->tokenStorage->getToken()) {
            $record['extra']['username'] = $token->getUsername();
        }

        if (!$masterRequest) {
            return $record;
        }

        if ($method = $masterRequest->getMethod()) {
            $record['extra']['method'] = $method;
        }

        if ($content = $masterRequest->getContent()) {
            $record['extra']['content'] = $content;
        }

        if ($uri = $masterRequest->getUri()) {
            $record['extra']['uri'] = $uri;
        }

        if ($ips = $masterRequest->getClientIps()) {
            $record['extra']['ip'] = $ips;
        }

        if ($headers = $masterRequest->headers->all()) {
            // hide api token if exists
            if (isset($headers[ApiRequestListener::TOKEN_PARAM])) {
                $headers[ApiRequestListener::TOKEN_PARAM] = ['%IRMAG_API_TOKEN%'];
            }

            $record['extra']['headers'] = $headers;
        }

        if ($session = $masterRequest->getSession()) {
            $session = $session->all();

            if (isset($session['_security_main'])) {
                unset($session['_security_main']);
            }

            $record['extra']['session'] = $session;
        }

        return $record;
    }
}
