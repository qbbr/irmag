<?php

namespace Irmag\OrderPostDeliveryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\Order;

/**
 * @ORM\Table(name="orders_post_delivery_data")
 * @ORM\Entity
 * @Serializer\ExclusionPolicy("all")
 */
class OrderPostDeliveryData
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_post_delivery_data", "api_order"})
     */
    private $id;

    /**
     * Заказ.
     *
     * @var Order
     *
     * @ORM\OneToOne(targetEntity="\Irmag\SiteBundle\Entity\Order", inversedBy="postDeliveryData")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_post_delivery_data"})
     */
    private $order;

    /**
     * Вес (в граммах).
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $weight;

    /**
     * Индекс получателя.
     *
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_post_delivery_data"})
     */
    private $deliveryIndex;

    /**
     * Город и регион получателя.
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_post_delivery_data"})
     */
    private $deliveryCity;

    /**
     * Стоимость пересылки (руб).
     *
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $cost;

    /**
     * Минимальное кол-во дней доставки.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minDays;

    /**
     * Максимальное кол-во дней доставки.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_post_delivery_data"})
     */
    private $maxDays;

    /**
     * Номер отслеживания посылки.
     *
     * @var int
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_post_delivery_data"})
     */
    private $trackCode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWeight(): ?float
    {
        return $this->weight;
    }

    public function setWeight(float $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getDeliveryIndex(): ?int
    {
        return $this->deliveryIndex;
    }

    public function setDeliveryIndex(int $deliveryIndex): self
    {
        $this->deliveryIndex = $deliveryIndex;

        return $this;
    }

    public function getDeliveryCity(): ?string
    {
        return $this->deliveryCity;
    }

    public function setDeliveryCity(?string $deliveryCity): self
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getMinDays(): ?int
    {
        return $this->minDays;
    }

    public function setMinDays(?int $minDays): self
    {
        $this->minDays = $minDays;

        return $this;
    }

    public function getMaxDays(): ?int
    {
        return $this->maxDays;
    }

    public function setMaxDays(?int $maxDays): self
    {
        $this->maxDays = $maxDays;

        return $this;
    }

    public function getTrackCode(): ?string
    {
        return $this->trackCode;
    }

    public function setTrackCode(?string $trackCode): self
    {
        $this->trackCode = $trackCode;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }
}
