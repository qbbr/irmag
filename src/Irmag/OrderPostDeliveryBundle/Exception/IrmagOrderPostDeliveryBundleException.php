<?php

namespace Irmag\OrderPostDeliveryBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

class IrmagOrderPostDeliveryBundleException extends IrmagException
{
}
