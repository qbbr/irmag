<?php

namespace Irmag\OrderPostDeliveryBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\SiteBundle\Event\OrderEvent;
use Irmag\OrderPostDeliveryBundle\Service\OrderPostDeliveryManager;

class OrderPreFinishPostDeliveryDataListener implements EventSubscriberInterface
{
    /**
     * @var OrderPostDeliveryManager
     */
    private $manager;

    /**
     * @param OrderPostDeliveryManager $manager
     */
    public function __construct(OrderPostDeliveryManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::ORDER_PRE_FINISH => 'onOrderPreFinish',
        ];
    }

    /**
     * @param OrderEvent $event
     */
    public function onOrderPreFinish(OrderEvent $event): void
    {
        $order = $event->getOrder();

        if ('russian_post' !== $order->getDeliveryMethod()->getShortname()) {
            return;
        }

        $this->manager->setOrderDeliveryData($order);
    }
}
