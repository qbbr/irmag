<?php

namespace Irmag\OrderPostDeliveryBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\OrderPostDeliveryBundle\Service\OrderPostDeliveryManager;
use Irmag\SiteBundle\IrmagSiteEvents;

class OrderDeliverySelectTransportCompanyListener implements EventSubscriberInterface
{
    /**
     * @var OrderPostDeliveryManager
     */
    private $manager;

    /**
     * @param OrderPostDeliveryManager $manager
     */
    public function __construct(OrderPostDeliveryManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::DELIVERY_SELECT_TRANSPORT_COMPANY => 'onSelectTransportCompany',
        ];
    }

    /**
     * Стирает данные расчета доставки почтой.
     */
    public function onSelectTransportCompany(): void
    {
        $this->manager->resetResult();
    }
}
