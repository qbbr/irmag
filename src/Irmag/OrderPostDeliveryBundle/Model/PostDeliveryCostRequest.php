<?php

namespace Irmag\OrderPostDeliveryBundle\Model;

class PostDeliveryCostRequest
{
    /**
     * @var int
     */
    private $indexTo;

    /**
     * @var int
     */
    private $weight;

    /**
     * @return int
     */
    public function getIndexTo(): int
    {
        return $this->getIndexTo();
    }

    /**
     * @param int $indexTo
     *
     * @return $this
     */
    public function setIndexTo(int $indexTo): self
    {
        $this->indexTo = $indexTo;

        return $this;
    }

    /**
     * @return int
     */
    public function getWeight(): int
    {
        return $this->weight;
    }

    /**
     * @param int $weight
     *
     * @return $this
     */
    public function setWeight(int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'index-to' => $this->indexTo,
            'mass' => $this->weight,
        ];
    }
}
