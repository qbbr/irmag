<?php

namespace Irmag\OrderPostDeliveryBundle\Service;

use Buzz\Browser;
use Irmag\OrderPostDeliveryBundle\Exception\IrmagOrderPostDeliveryBundleException;
use Irmag\OrderPostDeliveryBundle\Model\PostDeliveryCostRequest;

class OrderPostDeliveryService
{
    /**
     * URL хоста для запросов к API.
     */
    const API_HOST = 'https://otpravka-api.pochta.ru';

    /**
     * Индекс отправки.
     */
    const DEFAULT_INDEX_FROM = 664025;

    /**
     * Метод оплаты по умолчанию.
     */
    const DEFAULT_PAYMENT_METHOD = 'CASHLESS';

    /**
     * Тип отправки по умолчанию.
     */
    const DEFAULT_MAIL_TYPE = 'POSTAL_PARCEL';

    /**
     * Категория отправки по умолчанию.
     */
    const DEFAULT_MAIL_CATEGORY = 'ORDINARY';

    /**
     * @var Browser
     */
    private $buzzBrowser;

    /**
     * @var array
     */
    private $headers;

    /**
     * @param string $key
     * @param string $userKey
     */
    public function __construct(
        string $key,
        string $userKey
    ) {
        $this->buzzBrowser = new Browser();
        $this->headers = [
            'Authorization' => sprintf('AccessToken %s', $key),
            'X-User-Authorization' => sprintf('Basic %s', $userKey),
            'Content-Type' => 'application/json;charset=UTF-8',
        ];
    }

    /**
     * Рассчитывает стоимость пересылки.
     *
     * @param PostDeliveryCostRequest $request
     *
     * @throws IrmagOrderPostDeliveryBundleException
     *
     * @return array
     */
    public function calculateShipping(PostDeliveryCostRequest $request): array
    {
        $defaultOptions = [
            'index-from' => self::DEFAULT_INDEX_FROM,
            'mail-type' => self::DEFAULT_MAIL_TYPE,
            'mail-category' => self::DEFAULT_MAIL_CATEGORY,
            'payment-method' => self::DEFAULT_PAYMENT_METHOD,
        ];

        $requestJson = json_encode(array_merge($defaultOptions, $request->toArray()));

        try {
            $response = $this->buzzBrowser->post(
                self::API_HOST.'/1.0/tariff',
                $this->headers,
                $requestJson
            );
        } catch (\Exception $ex) {
            throw new IrmagOrderPostDeliveryBundleException(
                sprintf('[OrderPostDeliveryBundle] Could not do a request to %s, message: "%s"', self::API_HOST, $ex->getMessage()),
                $ex->getCode()
            );
        }

        $result = json_decode($response->getBody(), true);

        return $this->parseResponse($result);
    }

    /**
     * Парсит ответ с API почты.
     *
     * @param array $response
     *
     * @throws IrmagOrderPostDeliveryBundleException
     *
     * @return array
     */
    private function parseResponse(array $response): array
    {
        if ((empty($response['total-rate']) || 0 === $response['total-rate']) && !empty($response['code'])) {
            throw new IrmagOrderPostDeliveryBundleException(
                sprintf('Указан некорректный или несуществующий индекс')
            );
        }

        return [
            'minDays' => (!empty($response['delivery-time']) && !empty($response['delivery-time']['min-days'])) ? $response['delivery-time']['min-days'] : '',
            'maxDays' => (!empty($response['delivery-time']) && !empty($response['delivery-time']['max-days'])) ? $response['delivery-time']['max-days'] : '',
            'price' => round(($response['total-rate'] + $response['total-vat']) / 100.00),
        ];
    }
}
