<?php

namespace Irmag\OrderPostDeliveryBundle\Service;

use Buzz\Browser;
use Buzz\Client\Curl;

class OrderPostDeliveryKladrService
{
    /**
     * URL хоста для запросов к API.
     */
    const API_HOST = 'https://kladr-api.ru';

    /**
     * @var Browser
     */
    private $buzzBrowser;

    public function __construct()
    {
        $client = new Curl([
            'curl' => [
                CURLOPT_TIMEOUT => 15,
            ],
        ]);
        $this->buzzBrowser = new Browser($client);
    }

    /**
     * Возвращает полное название населённого пункта (название + регион) по почтовому индексу.
     * Формат ответа: <аббревиатура. Название_пункта (название_региона)>.
     * Исключения: федеральные города (без указания региона).
     *
     * @param int $index Почтовый индекс
     *
     * @return string
     */
    public function getFullCityNameFromIndex(int $index): string
    {
        $rawResult = $this
            ->buzzBrowser
            ->get(
                sprintf(
                    '%s/api.php?zip=%d&contentType=building&withParent=1&limit=1',
                    self::API_HOST,
                    $index
                )
            );

        $result = json_decode($rawResult->getBody(), true);
        $cityName = '';
        $federalCities = ['Москва', 'Санкт-Петербург', 'Севастополь'];

        if (!empty($result['result'])) {
            foreach ($result['result'][0]['parents'] as $chunk) {
                if ('region' === $chunk['contentType']) {
                    $cityName = sprintf(
                        '%s (%s%s)',
                        $cityName,
                        $chunk['name'],
                        (!\in_array($chunk['name'], $federalCities, true) && 'Республика' !== $chunk['type']) ? ' '.$chunk['typeShort'] : ''
                    );
                }

                if ('city' === $chunk['contentType']) {
                    $cityName = sprintf('%s.%s %s', $chunk['typeShort'], $chunk['name'], trim($cityName));
                }
            }
        }

        return $cityName;
    }

    /**
     * Возвращает основной индекс населенного пункта по его названию и коду ОКАТО.
     * Либо null, если не удалось отыскать такой индекс.
     *
     * @param string $cityName Название населенного пункта
     * @param string $okato    Код ОКАТО
     *
     * @return int|null
     */
    public function getMainIndexByCityNameAndOkato(string $cityName, string $okato): ?int
    {
        $rawResult = $this
            ->buzzBrowser
            ->get(
                sprintf(
                    '%s/api.php?query=%s&contentType=city&withParent=0&typeCode=3&limit=10',
                    self::API_HOST,
                    $cityName
                )
            );

        $result = json_decode($rawResult->getBody(), true);
        $city = [];

        if (!empty($result['result'])) {
            foreach ($result['result'] as $item) {
                if ($cityName !== $item['name']) {
                    continue;
                }

                $pattern = sprintf('/^%s/', $okato);

                if (preg_match($pattern, $item['okato'])) {
                    $city = $item;
                    break;
                }
            }
        }

        return $city['zip'] ?? null;
    }
}
