<?php

namespace Irmag\OrderPostDeliveryBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Irmag\BasketBundle\Traits\BasketContentTrait;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\EventDispatcher\DeliveryEventDispatcher;
use Irmag\OrderPostDeliveryBundle\Entity\OrderPostDeliveryData;
use Irmag\OrderPostDeliveryBundle\Exception\IrmagOrderPostDeliveryBundleException;
use Irmag\OrderPostDeliveryBundle\Model\PostDeliveryCostRequest;

class OrderPostDeliveryManager
{
    use BasketContentTrait;

    /**
     * Рассчитанные данные по доставке почтой.
     */
    const POST_DELIVERY_RESULT_LITERAL = 'post_delivery_result';

    /**
     * Выбран ли предложенный вариант.
     */
    const POST_DELIVERY_IS_SELECTED_FLAG = 'post_delivery_is_selected';

    /**
     * Ключ для flash-сообщения.
     */
    const POST_DELIVERY_FLASH_LITERAL = 'post_delivery_flash';

    /**
     * Поправочный коэффициент для стоимости доставки (чтобы покрыть издержки компании).
     */
    const POST_DELIVERY_COST_MULTIPLIER = 1.07;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var BasketManager
     */
    private $basketManager;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var OrderPostDeliveryService
     */
    private $apiService;

    /**
     * @var OrderPostDeliveryKladrService
     */
    private $kladrService;

    /**
     * @var DeliveryEventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param SessionInterface              $session
     * @param EntityManagerInterface        $em
     * @param BasketManager                 $basketManager
     * @param OrderPostDeliveryService      $service
     * @param OrderPostDeliveryKladrService $kladrService
     * @param DeliveryEventDispatcher       $eventDispatcher
     */
    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $em,
        BasketManager $basketManager,
        OrderPostDeliveryService $service,
        OrderPostDeliveryKladrService $kladrService,
        DeliveryEventDispatcher $eventDispatcher
    ) {
        $this->session = $session;
        $this->em = $em;
        $this->basketManager = $basketManager;
        $this->apiService = $service;
        $this->kladrService = $kladrService;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Рассчитывает стоимость по входным данным и запоминает результат в сессию.
     *
     * @param array $options Массив данных для расчета
     *
     * @throws IrmagOrderPostDeliveryBundleException
     */
    public function saveCalculatedDeliveryCost(array $options): void
    {
        $indexTo = $options['indexTo'];

        $apiRequest = new PostDeliveryCostRequest();
        $apiRequest
            ->setWeight($options['weight'])
            ->setIndexTo($indexTo);

        $result = $this->apiService->calculateShipping($apiRequest);
        $cityName = $this->kladrService->getFullCityNameFromIndex((int) $indexTo);

        if (!empty($result['code'])) {
            $errorMsg = (!empty($result['desc'])) ? $result['desc'] : '';
            throw new IrmagOrderPostDeliveryBundleException($errorMsg);
        }

        $result['price'] = round($result['price'] * self::POST_DELIVERY_COST_MULTIPLIER);
        $result['indexTo'] = $indexTo;
        $result['cityName'] = $cityName;
        $result['weight'] = $options['weight'];

        $this->session->set(self::POST_DELIVERY_RESULT_LITERAL, $result);
    }

    /**
     * Возвращает из сессии расчёт доставки.
     *
     * @return array
     */
    public function getResult(): array
    {
        return $this->session->get(self::POST_DELIVERY_RESULT_LITERAL, []);
    }

    /**
     * Возвращает из сессии признак выбора варианта доставки.
     *
     * @return bool
     */
    public function getIsSelected(): bool
    {
        return $this->session->get(self::POST_DELIVERY_IS_SELECTED_FLAG, false);
    }

    /**
     * Добавляет flash-сообщение, что содержимое корзины изменилось.
     */
    public function addFlashMessage(): void
    {
        $this->addFlashMessageAboutBasketContentChanging(self::POST_DELIVERY_FLASH_LITERAL, 'Содержимое вашей корзины было изменено. Необходимо пересчитать стоимость доставки.');
    }

    /**
     * Стирает данные расчёта.
     */
    public function resetResult(): void
    {
        if ($this->session->has(self::POST_DELIVERY_RESULT_LITERAL)) {
            $this->session->remove(self::POST_DELIVERY_RESULT_LITERAL);
        }

        if ($this->session->has(self::POST_DELIVERY_IS_SELECTED_FLAG)) {
            $this->session->remove(self::POST_DELIVERY_IS_SELECTED_FLAG);
        }
    }

    /**
     * Устанавливает признак выбора варианта доставки.
     *
     * @param bool $isSelected
     */
    public function setIsSelected($isSelected = true): void
    {
        $this->session->set(self::POST_DELIVERY_IS_SELECTED_FLAG, $isSelected);
        $this->eventDispatcher->dispatchSelectPost();
    }

    /**
     * Связывает данные из сессии с объектом Order.
     *
     * @param Order $order
     */
    public function setOrderDeliveryData(Order $order): void
    {
        $result = $this->getResult();

        if (!empty($result)) {
            $data = new OrderPostDeliveryData();
            $data
                ->setOrder($order)
                ->setDeliveryIndex((int) $result['indexTo'])
                ->setWeight((float) $result['weight'])
                ->setCost(round($result['price']));

            if (!empty($result['maxDays'])) {
                $data->setMaxDays($result['maxDays']);
            }

            if (!empty($result['minDays'])) {
                $data->setMinDays($result['minDays']);
            }

            if (!empty($result['cityName'])) {
                $data->setDeliveryCity($result['cityName']);
            }

            $this->em->persist($data);
            $this->em->flush();
            $this->em->refresh($order);

            $this->resetResult();
        }
    }
}
