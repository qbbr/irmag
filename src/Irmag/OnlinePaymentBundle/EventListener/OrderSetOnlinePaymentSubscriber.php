<?php

namespace Irmag\OnlinePaymentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\SiteBundle\Event\OrderEvent;
use Irmag\ProfileBundle\Service\UserStateManager\UserStateManager;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;

class OrderSetOnlinePaymentSubscriber implements EventSubscriberInterface
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var UserStateManager
     */
    private $userState;

    /**
     * @param SessionInterface $session
     * @param UserStateManager $userState
     */
    public function __construct(
        SessionInterface $session,
        UserStateManager $userState
    ) {
        $this->session = $session;
        $this->userState = $userState;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::ORDER_SET_ONLINE_PAYMENT => 'onSetOnlinePaymentInitializeUserState',
        ];
    }

    /**
     * @param OrderEvent $event
     */
    public function onSetOnlinePaymentInitializeUserState(OrderEvent $event): void
    {
        $order = $event->getOrder();

        $this->userState->set(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY, $order->getId());
        $this->session->set(OnlinePaymentManager::SESSION_PAYMENT_ORDER_ID, $order->getId());
    }
}
