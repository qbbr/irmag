<?php

namespace Irmag\OnlinePaymentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\ProfileBundle\Service\UserStateManager\UserStateManager;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;

class OrderPostFinishClearSessionSubscriber implements EventSubscriberInterface
{
    /**
     * @var OnlinePaymentManager
     */
    private $onlinePaymentManager;

    /**
     * @var UserStateManager
     */
    private $userState;

    /**
     * @param OnlinePaymentManager $paymentManager
     * @param UserStateManager     $userState
     */
    public function __construct(
        OnlinePaymentManager $paymentManager,
        UserStateManager $userState
    ) {
        $this->onlinePaymentManager = $paymentManager;
        $this->userState = $userState;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::ORDER_POST_FINISH => 'onOrderPostFinishClearSession',
        ];
    }

    /**
     * Чистит сессию после завершения заказа.
     */
    public function onOrderPostFinishClearSession(): void
    {
        $this->onlinePaymentManager->clearOnlinePaymentSession();

        $this->userState->remove(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY);
        $this->userState->remove(OnlinePaymentManager::USER_STATE_ORDER_TO_FINISH);
    }
}
