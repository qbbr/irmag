<?php

namespace Irmag\OnlinePaymentBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('irmag_online_payment');

        $rootNode
            ->children()
                ->arrayNode('configs')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('provider')->isRequired()->cannotBeEmpty()->end()
                            ->scalarNode('service')->isRequired()->cannotBeEmpty()->end()
                            ->booleanNode('is_embed')->defaultFalse()->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
