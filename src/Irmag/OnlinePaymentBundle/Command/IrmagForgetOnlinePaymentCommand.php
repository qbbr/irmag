<?php

namespace Irmag\OnlinePaymentBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;

class IrmagForgetOnlinePaymentCommand extends Command
{
    protected static $defaultName = 'irmag:online_payments:forget';

    /**
     * @var OnlinePaymentManager
     */
    private $onlinePaymentManager;

    /**
     * @param OnlinePaymentManager $onlinePaymentManager
     */
    public function __construct(
        OnlinePaymentManager $onlinePaymentManager
    ) {
        $this->onlinePaymentManager = $onlinePaymentManager;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Disable synchronizing for old payments.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->write('Starting ... ');
        $this->onlinePaymentManager->disableSyncingForOldPayments();
        $output->writeln(sprintf('<info>OK</info>'));
    }
}
