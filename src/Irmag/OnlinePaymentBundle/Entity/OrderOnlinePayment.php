<?php

namespace Irmag\OnlinePaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\SiteBundle\Entity\Order;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="orders_online_payments")
 * @ORM\Entity(repositoryClass="\Irmag\OnlinePaymentBundle\Repository\OrderOnlinePaymentRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderOnlinePayment
{
    use TimestampableEntity;

    /**
     * Отложенные методы оплаты: сначала подтверждение с клиента, потом регистрация платежа.
     */
    const IRMAG_DEFERRED_PAYMENT_METHODS = [
        'apple_pay',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment", "api_order"})
     */
    private $id;

    /**
     * Платежный шлюз.
     *
     * @var OrderOnlinePaymentProvider
     *
     * @ORM\ManyToOne(targetEntity="OrderOnlinePaymentProvider")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $provider;

    /**
     * Заказ, который оплачивается онлайн.
     *
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\SiteBundle\Entity\Order", inversedBy="onlinePayments")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $order;

    /**
     * Сгенерированный номер заказа для регистрации в системе эквайринга.
     *
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $generatedId;

    /**
     * Уникальный номер заказа в системе эквайринга.
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $acquiringId;

    /**
     * Статус онлайн платежа.
     *
     * @var OrderOnlinePaymentStatus
     *
     * @ORM\ManyToOne(targetEntity="OrderOnlinePaymentStatus")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment", "api_order"})
     */
    private $status;

    /**
     * Сумма, которая захолдирована на карте клиента (в копейках).
     *
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $sumHeld;

    /**
     * Сумма, которая была списана с карты клиента (в копейках).
     *
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $sumDeposited;

    /**
     * Сумма, которая была возвращена клиенту на карту (в копейках).
     *
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $sumRefund;

    /**
     * IP-адрес, с которого совершалась оплата.
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $ipAddress;

    /**
     * Имя держателя карты (доступен только после оплаты).
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cardHolderName;

    /**
     * Замаскированный номер карты (доступен только после оплаты).
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cardPAN;

    /**
     * Месяц и год окончания действия карты (доступен только после оплаты).
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cardExpire;

    /**
     * Дата и время регистрации оплаты.
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $registeredAt;

    /**
     * Дата и время подтверждения оплаты.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $approvedAt;

    /**
     * Дата и время отмены оплаты.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $revertedAt;

    /**
     * Дата и время завершения оплаты.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $depositedAt;

    /**
     * Дата и время возврата средств по оплате.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $refundedAt;

    /**
     * Причина отклонения платежа.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $declineReason;

    /**
     * Название банка-эмитента, выпустившего карту.
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cardBankName;

    /**
     * Страна банка-эмитента, выпустившего карту.
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $cardBankCountry;

    /**
     * Нужно ли синхронизировать платеж?
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $needToSync;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $paymentUrl;

    public function __construct()
    {
        $this->sumHeld = 0;
        $this->sumDeposited = 0;
        $this->sumRefund = 0;
        $this->needToSync = true;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGeneratedId(): ?string
    {
        return $this->generatedId;
    }

    public function setGeneratedId(string $generatedId): self
    {
        $this->generatedId = $generatedId;

        return $this;
    }

    public function getAcquiringId(): ?string
    {
        return $this->acquiringId;
    }

    public function setAcquiringId(?string $acquiringId): self
    {
        $this->acquiringId = $acquiringId;

        return $this;
    }

    public function getSumHeld(): ?int
    {
        return $this->sumHeld;
    }

    public function setSumHeld(int $sumHeld): self
    {
        $this->sumHeld = $sumHeld;

        return $this;
    }

    public function getSumDeposited(): ?int
    {
        return $this->sumDeposited;
    }

    public function setSumDeposited(int $sumDeposited): self
    {
        $this->sumDeposited = $sumDeposited;

        return $this;
    }

    public function getSumRefund(): ?int
    {
        return $this->sumRefund;
    }

    public function setSumRefund(int $sumRefund): self
    {
        $this->sumRefund = $sumRefund;

        return $this;
    }

    public function getIpAddress(): ?string
    {
        return $this->ipAddress;
    }

    public function setIpAddress(?string $ipAddress): self
    {
        $this->ipAddress = $ipAddress;

        return $this;
    }

    public function getCardHolderName(): ?string
    {
        return $this->cardHolderName;
    }

    public function setCardHolderName(?string $cardHolderName): self
    {
        $this->cardHolderName = $cardHolderName;

        return $this;
    }

    public function getCardPAN(): ?string
    {
        return $this->cardPAN;
    }

    public function setCardPAN(?string $cardPAN): self
    {
        $this->cardPAN = $cardPAN;

        return $this;
    }

    public function getCardExpire(): ?string
    {
        return $this->cardExpire;
    }

    public function setCardExpire(?string $cardExpire): self
    {
        $this->cardExpire = $cardExpire;

        return $this;
    }

    public function getRegisteredAt(): ?\DateTimeInterface
    {
        return $this->registeredAt;
    }

    public function setRegisteredAt(?\DateTimeInterface $registeredAt): self
    {
        $this->registeredAt = $registeredAt;

        return $this;
    }

    public function getApprovedAt(): ?\DateTimeInterface
    {
        return $this->approvedAt;
    }

    public function setApprovedAt(?\DateTimeInterface $approvedAt): self
    {
        $this->approvedAt = $approvedAt;

        return $this;
    }

    public function getRevertedAt(): ?\DateTimeInterface
    {
        return $this->revertedAt;
    }

    public function setRevertedAt(?\DateTimeInterface $revertedAt): self
    {
        $this->revertedAt = $revertedAt;

        return $this;
    }

    public function getDepositedAt(): ?\DateTimeInterface
    {
        return $this->depositedAt;
    }

    public function setDepositedAt(?\DateTimeInterface $depositedAt): self
    {
        $this->depositedAt = $depositedAt;

        return $this;
    }

    public function getRefundedAt(): ?\DateTimeInterface
    {
        return $this->refundedAt;
    }

    public function setRefundedAt(?\DateTimeInterface $refundedAt): self
    {
        $this->refundedAt = $refundedAt;

        return $this;
    }

    public function getDeclineReason(): ?string
    {
        return $this->declineReason;
    }

    public function setDeclineReason(?string $declineReason): self
    {
        $this->declineReason = $declineReason;

        return $this;
    }

    public function getCardBankName(): ?string
    {
        return $this->cardBankName;
    }

    public function setCardBankName(?string $cardBankName): self
    {
        $this->cardBankName = $cardBankName;

        return $this;
    }

    public function getCardBankCountry(): ?string
    {
        return $this->cardBankCountry;
    }

    public function setCardBankCountry(?string $cardBankCountry): self
    {
        $this->cardBankCountry = $cardBankCountry;

        return $this;
    }

    public function getNeedToSync(): ?bool
    {
        return $this->needToSync;
    }

    public function setNeedToSync(bool $needToSync): self
    {
        $this->needToSync = $needToSync;

        return $this;
    }

    public function getPaymentUrl(): ?string
    {
        return $this->paymentUrl;
    }

    public function setPaymentUrl(?string $paymentUrl): self
    {
        $this->paymentUrl = $paymentUrl;

        return $this;
    }

    public function getProvider(): ?OrderOnlinePaymentProvider
    {
        return $this->provider;
    }

    public function setProvider(?OrderOnlinePaymentProvider $provider): self
    {
        $this->provider = $provider;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getStatus(): ?OrderOnlinePaymentStatus
    {
        return $this->status;
    }

    public function setStatus(?OrderOnlinePaymentStatus $status): self
    {
        $this->status = $status;

        return $this;
    }
}
