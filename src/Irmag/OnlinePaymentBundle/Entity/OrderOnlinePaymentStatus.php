<?php

namespace Irmag\OnlinePaymentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="orders_online_payments_statuses")
 * @ORM\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderOnlinePaymentStatus
{
    /**
     * Статус "Зарегистрирован".
     */
    const PAYMENT_STATUS_REGISTERED = 0;
    const PAYMENT_STATUS_REGISTERED_SHORTNAME = 'Registered';

    /**
     * Статус "Подтверджён".
     */
    const PAYMENT_STATUS_APPROVED = 1;
    const PAYMENT_STATUS_APPROVED_SHORTNAME = 'Approved';

    /**
     * Статус "Завершён".
     */
    const PAYMENT_STATUS_DEPOSITED = 2;
    const PAYMENT_STATUS_DEPOSITED_SHORTNAME = 'Deposited';

    /**
     * Статус "Отменён".
     */
    const PAYMENT_STATUS_REVERTED = 3;
    const PAYMENT_STATUS_REVERTED_SHORTNAME = 'Reverted';

    /**
     * Статус "Возврат".
     */
    const PAYMENT_STATUS_REFUND = 4;
    const PAYMENT_STATUS_REFUND_SHORTNAME = 'Refund';

    /**
     * Статус "Ожидает подтверждения".
     */
    const PAYMENT_STATUS_ACS_AUTHORIZED = 5;
    const PAYMENT_STATUS_ACS_AUTHORIZED_SHORTNAME = 'ACS Authorized';

    /**
     * Статус "Отклонён".
     */
    const PAYMENT_STATUS_DECLINED = 6;
    const PAYMENT_STATUS_DECLINED_SHORTNAME = 'Declined';

    /**
     * Ид статуса.
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment", "api_order"})
     */
    private $id;

    /**
     * Название статуса.
     *
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment", "api_order"})
     */
    private $name;

    /**
     * Название статуса по-русски.
     *
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_online_payment"})
     */
    private $nameTranslated;

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->nameTranslated;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getNameTranslated(): ?string
    {
        return $this->nameTranslated;
    }

    public function setNameTranslated(string $nameTranslated): self
    {
        $this->nameTranslated = $nameTranslated;

        return $this;
    }
}
