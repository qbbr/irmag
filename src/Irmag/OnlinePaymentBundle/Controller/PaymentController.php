<?php

namespace Irmag\OnlinePaymentBundle\Controller;

use Buzz\Browser;
use Buzz\Client\Curl;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePaymentStatus;
use Irmag\OnlinePaymentBundle\Exception\IrmagOnlinePaymentException;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;
use Irmag\OnlinePaymentBundle\ProviderContract\PaymentProviderInterface;
use Irmag\ProfileBundle\Service\UserStateManager\UserStateManager;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Controller\OrderController;
use Irmag\SiteBundle\Service\OrderManager\OrderManager;

/**
 * @Route("/payment")
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 */
class PaymentController extends Controller
{
    /**
     * @Route("/", name="irmag_order_payment_form")
     *
     * @param Request $request
     * @param Order   $order
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function indexAction(Request $request, Order $order): Response
    {
        $session = $request->getSession();

        if (empty($order->getPaymentMethod()) || empty($order->getPaymentMethod()->getShortname())) {
            throw new IrmagOnlinePaymentException(sprintf('No payment method selected for order №%d!', $order->getId()));
        }

        $paymentMethodShortname = $order->getPaymentMethod()->getShortname();
        $pm = $this->getPreparedOnlinePaymentManager($order);

        $currentProviderConfig = $this->getCurrentProviderConfig($paymentMethodShortname);
        $needNewPayment = $session->get(OnlinePaymentManager::SESSION_FORCE_NEW_PAYMENT, false);
        $payment = $pm->getOnlinePaymentByOrder($order, $needNewPayment);
        $provider = $pm->getCurrentPaymentProvider($currentProviderConfig['provider']);
        $payment->setProvider($provider);

        $onlinePaymentTotal = $this->getOnlinePaymentTotal($order);

        if (false === \in_array($paymentMethodShortname, OrderOnlinePayment::IRMAG_DEFERRED_PAYMENT_METHODS, true)) {
            if (empty($payment->getAcquiringId()) || true === $needNewPayment) {
                $pm->registerOnlinePayment($payment, $onlinePaymentTotal, $request->getClientIp());
                $this->get(UserStateManager::class)->set(OnlinePaymentManager::USER_STATE_BANK_RESPONSE_KEY, $this->getReturnToken($payment));
            }
        } else {
            $isDeferred = true;
            $pm->registerDeferredPayment($payment);
        }

        $session->remove(OnlinePaymentManager::SESSION_FORCE_NEW_PAYMENT);
        $paymentPage = $pm->getPaymentFormUrl($payment, $isDeferred ?? false);

        return $this->render('@IrmagOnlinePayment/Payment/payment.html.twig', [
            'payment_page' => $paymentPage,
            'payment_method' => $paymentMethodShortname,
            'total' => $onlinePaymentTotal,
            'is_embed' => $currentProviderConfig['is_embed'],
            'payment_id' => $payment->getId(),
            'order_id' => $order->getId(),
            'apple_merchant' => $this->getParameter('irmag_sberbank_apple_pay_merchant') ?? null,
        ]);
    }

    /**
     * @Route("/online/cancel/", name="irmag_order_cancel_online_payment", options={"expose": true})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function failedOnlinePaymentAction(Request $request): Response
    {
        $this->throwExceptionIfWrongReturnToken($request->query);
        $orderId = $this->get(UserStateManager::class)->get(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY);

        if ($orderId) {
            $orderManager = $this->get(OrderManager::class);
            $order = $orderManager->getOrder($orderId);

            $paymentManager = $this->get(OnlinePaymentManager::class);
            $paymentProviderService = $this->getPaymentProviderService($order);
            $payment = $paymentManager->getOnlinePaymentByOrder($order);

            $state = $paymentProviderService->getState($payment);
            $paymentManager->updateOrderOnlinePayment($payment, $state);

            $message = $payment->getDeclineReason();
            $id = $order->getId();
        }

        return $this->render('@IrmagOnlinePayment/Payment/fail.html.twig', [
            'id' => $id ?? null,
            'message' => $message ?? '',
        ]);
    }

    /**
     * @Route("/online/finish/", name="irmag_order_finish_online_payment", options={"expose": true})
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function finishOnlineAction(Request $request)
    {
        $this->throwExceptionIfWrongReturnToken($request->query);

        $session = $request->getSession();
        $userStateManager = $this->get(UserStateManager::class);
        $orderId = $userStateManager->get(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY);

        if (!$orderId) {
            return $this->redirectToRoute('irmag_catalog');
        }

        $paymentManager = $this->get(OnlinePaymentManager::class);
        $orderManager = $this->get(OrderManager::class);

        $order = $orderManager->getOrder($orderId);
        $payment = $paymentManager->getOnlinePaymentByOrder($order);
        $total = $this->getOnlinePaymentTotal($order);
        $state = $this->getPaymentProviderService($order)->getState($payment);

        if (OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME !== $state->getStatus()) {
            return $this->redirectToRoute('irmag_order_cancel_online_payment', ['tkn' => $request->query->get('tkn')]);
        }

        $paymentManager->approveOnlinePayment($payment, (int) round($total * 100.00, 2));

        if (!empty($state->getExtraData())) {
            $paymentManager->fillExtendedOnlinePaymentOptions($payment, $state->getExtraData());
            $paymentManager->setBankInfo($payment, $state->getExtraData());
        }

        $session->set(OrderController::SESSION_SUCCESS_ORDER_ID, $order->getId());

        $userStateManager->remove(OnlinePaymentManager::USER_STATE_BANK_RESPONSE_KEY);
        $userStateManager->remove(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY);
        $userStateManager->set(OnlinePaymentManager::USER_STATE_ORDER_TO_FINISH, $order->getId());

        return $this->render('@IrmagOnlinePayment/Payment/success.html.twig');
    }

    /**
     * "Насильно" инициализировать новый платёж (установить опцию в сессии и сделать редирект).
     *
     * @Route("/new/", name="irmag_order_new_payment", options={"expose": true})
     *
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function forceNewPaymentAction(Request $request): RedirectResponse
    {
        $orderId = $this->get(UserStateManager::class)->get(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY);

        if (false === $orderId) {
            return $this->redirectToRoute('irmag_catalog');
        }

        $session = $request->getSession();
        $session->set(OnlinePaymentManager::SESSION_FORCE_NEW_PAYMENT, true);

        return $this->redirectToRoute('irmag_order_payment');
    }

    /**
     * Запрос на валидацию мерчанта в платежной системе ApplePay.
     *
     * @Route("/validate_apple/",
     *     name="irmag_order_validate_apple_pay_merchant",
     *     options={"expose": true},
     *     condition="request.isXmlHttpRequest()",
     *     methods={"GET"}
     * )
     *
     * @param Request $request
     *
     * @throws IrmagOnlinePaymentException
     *
     * @return JsonResponse
     */
    public function validateApplePayMerchantAction(Request $request): JsonResponse
    {
        $validationUrl = $request->get('validationUrl');

        if ('https' !== parse_url($validationUrl, \PHP_URL_SCHEME) || '.apple.com' !== mb_substr(parse_url($validationUrl, \PHP_URL_HOST), -10)) {
            throw new IrmagOnlinePaymentException('ValidationUrl is not valid.');
        }

        $params = json_encode([
            'merchantIdentifier' => $this->getParameter('irmag_sberbank_apple_pay_merchant'),
            'domainName' => $this->getParameter('irmag_site_domain'),
            'displayName' => 'ООО "ИРМАГ"',
        ]);

        $client = new Curl([
            'curl' => [
                \CURLOPT_SSLCERT => $this->getParameter('irmag_sberbank_apple_pay_certificate'),
                \CURLOPT_SSLKEY => $this->getParameter('irmag_sberbank_apple_pay_certificate_key'),
                \CURLOPT_SSLKEYPASSWD => $this->getParameter('irmag_sberbank_apple_pay_certificate_password'),
            ],
        ]);

        $buzzBrowser = new Browser($client);
        $response = $buzzBrowser->post($validationUrl, ['Content-Type' => 'application/json;charset=UTF-8'], $params);

        return new JsonResponse(['data' => json_decode($response->getBody())]);
    }

    /**
     * @Route("/process/apple/",
     *     name="irmag_order_process_apple_pay",
     *     options={"expose": true},
     *     condition="request.isXmlHttpRequest()",
     *     methods={"POST"}
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function registerApplePayAction(Request $request): JsonResponse
    {
        $paymentToken = $request->get('paymentToken');
        $paymentId = $request->request->getInt('paymentId');
        $acquiringId = $this->get(OnlinePaymentManager::class)->registerApplePayPayment($paymentId, $paymentToken);
        $payment = $this->get('doctrine.orm.default_entity_manager')->getRepository(OrderOnlinePayment::class)->find($paymentId);

        if (!$payment) {
            $this->createNotFoundException(sprintf('OnlinePayment %d not found', $paymentId));
        }

        $this->get(UserStateManager::class)->set(OnlinePaymentManager::USER_STATE_BANK_RESPONSE_KEY, $this->getReturnToken($payment));

        return new JsonResponse(['success' => true, 'tkn' => $acquiringId]);
    }

    /**
     * Бросает исключение, если запросы на подтверждение или отклонение оплаты приходят не от Платёжного шлюза.
     *
     * @param ParameterBag $bag
     *
     * @throws NotFoundHttpException
     */
    private function throwExceptionIfWrongReturnToken(ParameterBag $bag): void
    {
        $userStateManager = $this->get(UserStateManager::class);

        if ($userStateManager->get(OnlinePaymentManager::USER_STATE_BANK_RESPONSE_KEY) !== md5($bag->get('orderId'))) {
            throw $this->createNotFoundException('Wrong payment token for payment confirmation.');
        }
    }

    /**
     * Бросает исключение, если не настроены опции платежных шлюзов для выбранного способа платежа (online или Apple Pay).
     *
     * @param array  $configs Массив с настройками
     * @param string $option  Shortname способа платежа, выступает в качестве литерала для массива с настройками
     *
     * @throws \Exception
     */
    private function throwExceptionIfPaymentOptionNotConfigured(array $configs, string $option): void
    {
        if (empty($configs[$option])) {
            throw new IrmagOnlinePaymentException(sprintf('Providers for option "%s" not configured. Check your configuration.', $option));
        }
    }

    /**
     * Настроить OnlinePaymentManager согласно выбранному способу оплаты.
     *
     * @param Order $order Текущий заказ
     *
     * @return OnlinePaymentManager
     */
    private function getPreparedOnlinePaymentManager(Order $order): OnlinePaymentManager
    {
        $pm = $this->get(OnlinePaymentManager::class);
        $pm->setPaymentProviderService($this->getPaymentProviderService($order));

        return $pm;
    }

    /**
     * Получить текущий конфиг по короткому имени способа оплаты.
     *
     * @param string $paymentMethodShortname
     *
     * @return array
     */
    private function getCurrentProviderConfig(string $paymentMethodShortname): array
    {
        $providerConfigs = $this->getParameter('irmag_online_payment.configs');
        $this->throwExceptionIfPaymentOptionNotConfigured($providerConfigs, $paymentMethodShortname);

        return $providerConfigs[$paymentMethodShortname];
    }

    /**
     * Получить сервис, обслуживающий платежный шлюз для конкретного заказа.
     *
     * @param Order $order Заказ
     *
     * @return PaymentProviderInterface
     */
    private function getPaymentProviderService(Order $order): PaymentProviderInterface
    {
        $providerConfigs = $this->getParameter('irmag_online_payment.configs');
        $providerServiceName = $providerConfigs[$order->getPaymentMethod()->getShortname()]['service'];

        /** @var PaymentProviderInterface $providerService */
        $providerService = $this->get($providerServiceName);

        return $providerService;
    }

    /**
     * Получить token для идентификации возвратных url от платёжного шлюза.
     *
     * @param OrderOnlinePayment $payment Платёж
     *
     * @return string
     */
    private function getReturnToken(OrderOnlinePayment $payment): string
    {
        return md5($payment->getAcquiringId());
    }

    /**
     * Определить сумму онлайн платежа.
     *
     * @param Order $order
     *
     * @return int
     */
    private function getOnlinePaymentTotal(Order $order): int
    {
        $total = (float) $order->getTotalPrice();

        if (false === \in_array($order->getDeliveryMethod()->getShortname(), ['selfservice'], true) && !empty($order->getDeliveryPrice())) {
            $total += (float) $order->getDeliveryPrice();
        }

        return (int) round($total * 100.00, 2);
    }
}
