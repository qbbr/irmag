<?php

namespace Irmag\OnlinePaymentBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Irmag\SiteBundle\Mailer\SiteMailer;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SberbankAcquiringBundle\Service\SberbankService;
use Irmag\ProfileBundle\Service\UserStateManager\UserStateManager;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePaymentStatus;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePaymentProvider;
use Irmag\OnlinePaymentBundle\Exception\IrmagOnlinePaymentException;
use Irmag\OnlinePaymentBundle\ProviderContract\PaymentStateInterface;
use Irmag\OnlinePaymentBundle\ProviderContract\PaymentProviderInterface;

class OnlinePaymentManager
{
    /**
     * Максимальное время доступности формы (минуты).
     */
    const MAX_PAY_TIME = 19;

    /**
     * Ключ значения в сессии.
     */
    const SESSION_PAYMENT_ORDER_ID = 'payment_order_id';

    /**
     * Пересоздать новый платеж (литерал для сессии).
     */
    const SESSION_FORCE_NEW_PAYMENT = 'need_extra_payment';

    /**
     * Номер неоплаченного заказа для UserState.
     */
    const USER_STATE_UNPAYED_ORDER_KEY = 'unpayed_order_id';

    /**
     * Токен для проверки неоплаченного заказа.
     */
    const USER_STATE_BANK_RESPONSE_KEY = 'unpayed_order_token';

    /**
     * Оплаченный, но незавершенный заказ.
     */
    const USER_STATE_ORDER_TO_FINISH = 'order_payed_and_not_finished';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var SiteMailer
     */
    private $mailer;

    /**
     * @var PaymentProviderInterface
     */
    private $providerService;

    /**
     * @var UserStateManager
     */
    private $userState;

    /**
     * @var array
     */
    private $paymentConfigs;

    /**
     * @param EntityManagerInterface $em
     * @param SessionInterface       $session
     * @param SiteMailer             $mailer
     * @param UserStateManager       $userState
     * @param SberbankService        $paymentService
     * @param array                  $paymentConfigs
     */
    public function __construct(
        EntityManagerInterface $em,
        SessionInterface $session,
        SiteMailer $mailer,
        UserStateManager $userState,
        SberbankService $paymentService,
        array $paymentConfigs
    ) {
        $this->em = $em;
        $this->session = $session;
        $this->mailer = $mailer;
        $this->userState = $userState;
        $this->providerService = $paymentService;
        $this->paymentConfigs = $paymentConfigs;
    }

    /**
     * Возвращает провайдера по шортнейму.
     *
     * @param string $providerShortname
     *
     * @throws IrmagOnlinePaymentException
     *
     * @return OrderOnlinePaymentProvider
     */
    public function getCurrentPaymentProvider(string $providerShortname): OrderOnlinePaymentProvider
    {
        $provider = $this->em
            ->getRepository(OrderOnlinePaymentProvider::class)
            ->findOneBy(['shortname' => $providerShortname]);

        if (!$provider) {
            throw new IrmagOnlinePaymentException(sprintf('No provider with shortname "%s" was found.', $providerShortname));
        }

        return $provider;
    }

    /**
     * @param PaymentProviderInterface $providerService
     */
    public function setPaymentProviderService(PaymentProviderInterface $providerService): void
    {
        $this->providerService = $providerService;
    }

    /**
     * @return int|null
     */
    public function getOrderPayedAndNotFinished(): ?int
    {
        return $this->userState->get(self::USER_STATE_ORDER_TO_FINISH);
    }

    /**
     * Очистить оплаченный, но незавершенный заказ.
     */
    public function clearPayedAndNotFinishedOrder(): void
    {
        $this->userState->remove(self::USER_STATE_ORDER_TO_FINISH);
    }

    /**
     * @return string|null
     */
    public function getUnpayedOrderId(): ?int
    {
        return $this->userState->get(self::USER_STATE_UNPAYED_ORDER_KEY);
    }

    /**
     * Получить онлайн-платёж по заказу.
     *
     * @param Order $order    Заказ
     * @param bool  $forceNew Зарегистрировать заново
     *
     * @return OrderOnlinePayment
     */
    public function getOnlinePaymentByOrder(Order $order, bool $forceNew = false): OrderOnlinePayment
    {
        $orderPayments = $order->getOnlinePayments()->toArray();
        $repo = $this->em->getRepository(OrderOnlinePayment::class);

        if (empty($orderPayments) || true === $forceNew) {
            $payment = new OrderOnlinePayment();
            $payment->setOrder($order);

            $generatedId = (empty($orderPayments)) ?
                $order->getId() :
                uniqid(sprintf('%s-', $payment->getOrder()->getId()));

            $payment->setGeneratedId($generatedId);

            $this->em->persist($payment);
            $this->em->flush();
        } else {
            $payment = ($repo->getActualOrderPayment($order)) ?: $repo->getLastPayment($order);
        }

        return $payment;
    }

    /**
     * Возвращает статус оплаты по его названию или бросает исключение.
     *
     * @param string $statusName Название статуса
     *
     * @throws NotFoundHttpException
     *
     * @return OrderOnlinePaymentStatus
     */
    public function getOnlinePaymentStatus($statusName): OrderOnlinePaymentStatus
    {
        $status = $this->em->getRepository(OrderOnlinePaymentStatus::class)->findOneBy(['name' => $statusName]);

        if (!$status) {
            throw new NotFoundHttpException(sprintf('Order online payment status with name "%s" not found.', $statusName));
        }

        return $status;
    }

    /**
     * Возвращает URL для платёжной формы.
     *
     * @param OrderOnlinePayment $payment    Оплата
     * @param bool               $isDeferred
     *
     * @throws IrmagOnlinePaymentException
     *
     * @return string|null
     */
    public function getPaymentFormUrl(OrderOnlinePayment $payment, bool $isDeferred = false): ?string
    {
        $this->throwExceptionIfPaymentProviderNotSet();

        if (empty($payment->getAcquiringId()) && false === $isDeferred) {
            throw new IrmagOnlinePaymentException('Cannot get form URL for the payment, because it has not been registered.');
        }

        return $this->providerService->getPaymentFormUrl($payment, $isDeferred);
    }

    /**
     * Зарегистрировать платёж.
     *
     * @param OrderOnlinePayment $payment Платёж
     * @param int                $sum     Сумма операции (в копейках)
     * @param string             $ip      IP-адрес плательщика
     */
    public function registerOnlinePayment(OrderOnlinePayment $payment, int $sum, string $ip = null): void
    {
        $this->throwExceptionIfPaymentProviderNotSet();

        $options = [
            'sum' => $sum,
            'orderId' => $payment->getGeneratedId(),
            'ip' => $ip,
        ];

        $paymentMethodShortname = $payment->getOrder()->getPaymentMethod()->getShortname();

        if (false === $this->paymentConfigs[$paymentMethodShortname]['is_embed']) {
            $options['cancelToken'] = md5(sprintf('%s%s', $payment->getOrder()->getUser()->getId(), $payment->getOrder()->getId()));
        }

        $registerData = $this->providerService->register($options);
        $acquiringId = $registerData['orderId'];

        if (!empty($registerData['formUrl'])) {
            $payment->setPaymentUrl($registerData['formUrl']);
        }

        $status = $this->getOnlinePaymentStatus(OrderOnlinePaymentStatus::PAYMENT_STATUS_REGISTERED_SHORTNAME);
        $payment->setAcquiringId($acquiringId);
        $payment->setStatus($status);
        $payment->setRegisteredAt(new \DateTime());

        $this->em->persist($payment);
        $this->em->flush();
    }

    /**
     * Зарегистрировать отложенный платёж.
     *
     * @param OrderOnlinePayment $payment
     */
    public function registerDeferredPayment(OrderOnlinePayment $payment): void
    {
        $status = $this->getOnlinePaymentStatus(OrderOnlinePaymentStatus::PAYMENT_STATUS_REGISTERED_SHORTNAME);
        $payment->setStatus($status);
        $payment->setRegisteredAt(new \DateTime());

        $this->em->persist($payment);
        $this->em->flush();
    }

    /**
     * Зарегистрировать платёж ApplePay.
     *
     * @param int    $paymentId
     * @param string $paymentToken
     *
     * @throws NotFoundHttpException
     *
     * @return string
     */
    public function registerApplePayPayment(int $paymentId, string $paymentToken): string
    {
        $payment = $this->em->getRepository(OrderOnlinePayment::class)->find($paymentId);

        if (!$payment) {
            throw new NotFoundHttpException(sprintf('OnlinePayment %d not found', $paymentId));
        }

        $options = [
            'orderId' => $payment->getOrder()->getId(),
            'paymentToken' => $paymentToken,
        ];

        $registerData = $this->providerService->registerApplePay($options);

        $acquiringId = $registerData['orderId'];
        $payment->setAcquiringId($acquiringId);

        $this->em->persist($payment);
        $this->em->flush();

        return $acquiringId;
    }

    /**
     * Подтверждает платёж.
     *
     * @param OrderOnlinePayment $payment Платёж
     * @param int                $sum     Сумма холдирования (в копейках)
     */
    public function approveOnlinePayment(OrderOnlinePayment $payment, int $sum): void
    {
        $status = $this->getOnlinePaymentStatus(OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME);
        $payment->setStatus($status);
        $payment->setApprovedAt(new \DateTime());
        $payment->setSumHeld((int) $sum);

        $this->em->persist($payment);
        $this->em->flush();
    }

    /**
     * Отменяет платёж.
     *
     * @param OrderOnlinePayment $payment Платёж
     *
     * @throws IrmagOnlinePaymentException
     */
    public function revertOnlinePayment(OrderOnlinePayment $payment): void
    {
        $currentPaymentStatus = $payment->getStatus()->getName();

        if (OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME !== $currentPaymentStatus) {
            throw new IrmagOnlinePaymentException(
                sprintf(
                    'Cannot revert payment with status "%s", only approved payments can be reverted.',
                    $currentPaymentStatus
                )
            );
        }

        $status = $this->getOnlinePaymentStatus(OrderOnlinePaymentStatus::PAYMENT_STATUS_REVERTED_SHORTNAME);
        $payment->setStatus($status);
        $payment->setRevertedAt(new \DateTime());
        $payment->setNeedToSync(false);

        $this->em->persist($payment);
        $this->em->flush();
    }

    /**
     * Отклоняет платёж.
     *
     * @param OrderOnlinePayment $payment Платёж
     * @param string             $reason  Причина отклонения
     */
    public function declineOnlinePayment(OrderOnlinePayment $payment, $reason = null): void
    {
        $status = $this->getOnlinePaymentStatus(OrderOnlinePaymentStatus::PAYMENT_STATUS_DECLINED_SHORTNAME);
        $payment->setStatus($status);
        $payment->setNeedToSync(false);

        if (!empty($reason)) {
            $payment->setDeclineReason($reason);
        }

        $this->em->persist($payment);
        $this->em->flush();
    }

    /**
     * Завершает платёж.
     *
     * @param OrderOnlinePayment $payment     Платеж
     * @param int                $sum         Сумма списания (в копейках)
     * @param bool               $needApiCall Сделать соответствующий вызов в PaymentService API?
     *
     * @throws IrmagOnlinePaymentException
     */
    public function depositOnlinePayment(OrderOnlinePayment $payment, int $sum, bool $needApiCall = false): void
    {
        if (true === $needApiCall) {
            $this->throwExceptionIfPaymentProviderNotSet();
        }

        $currentPaymentStatus = $payment->getStatus()->getName();

        if (OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME !== $currentPaymentStatus) {
            throw new IrmagOnlinePaymentException(
                sprintf(
                    'Cannot deposit payment with status "%s", only approved payments can be deposited.',
                    $currentPaymentStatus
                )
            );
        }

        $sumHeld = $payment->getSumHeld();

        if ($sum > $sumHeld) {
            throw new IrmagOnlinePaymentException(
                sprintf(
                    'Deposited sum %d is bigger than approved sum %d.',
                    $sum,
                    $sumHeld
                )
            );
        }

        if (true === $needApiCall) {
            $this->providerService->deposit($payment, $sum);
        }

        $status = $this->getOnlinePaymentStatus(OrderOnlinePaymentStatus::PAYMENT_STATUS_DEPOSITED_SHORTNAME);
        $payment->setStatus($status);
        $payment->setSumDeposited($sum);
        $payment->setDepositedAt(new \DateTime());

        $this->em->persist($payment);
        $this->em->flush();

        $payer = $payment->getOrder()->getUser();

        if (!empty($payer->getEmail()) && !$payer->getIsEmailInvalid()) {
            $this->mailer->sendOrderOnlinePaymentFinishedMessage($payer->getEmail(), [
                'payment' => $payment,
            ]);
        }
    }

    /**
     * Возвращает деньги с платёжа.
     *
     * @param OrderOnlinePayment $payment     Платеж
     * @param int                $sum         Сумма возврата (в копейках)
     * @param bool               $needApiCall Сделать соответствующий вызов в PaymentService API?
     *
     * @throws IrmagOnlinePaymentException
     */
    public function refundOnlinePayment(OrderOnlinePayment $payment, int $sum, bool $needApiCall = false): void
    {
        if (true === $needApiCall) {
            $this->throwExceptionIfPaymentProviderNotSet();
        }

        $currentPaymentStatus = $payment->getStatus()->getName();

        if (OrderOnlinePaymentStatus::PAYMENT_STATUS_DEPOSITED_SHORTNAME !== $currentPaymentStatus) {
            throw new IrmagOnlinePaymentException(
                sprintf(
                    'Cannot refund payment with status "%s", only deposited payments can be refund.',
                    $currentPaymentStatus
                )
            );
        }

        $depositedSum = $payment->getSumDeposited();

        if ($sum > $depositedSum) {
            throw new IrmagOnlinePaymentException(
                sprintf(
                    'Refund sum %d is greater than deposited %d.',
                    $sum,
                    $depositedSum
                )
            );
        }

        if (true === $needApiCall) {
            $this->providerService->refund($payment, $sum);
        }

        $status = $this->getOnlinePaymentStatus(OrderOnlinePaymentStatus::PAYMENT_STATUS_REFUND_SHORTNAME);
        $payment->setStatus($status);
        $payment->setSumRefund((int) $sum);
        $payment->setRefundedAt(new \DateTime());
        $payment->setNeedToSync(false);

        $this->em->persist($payment);
        $this->em->flush();
    }

    /**
     * Отменяет платёж или возвращает деньги (в зависимости от того, сколько прошло времени).
     *
     * @param OrderOnlinePayment $payment     Платеж
     * @param int                $sum         Сумма операции
     * @param bool               $needApiCall
     *
     * @throws IrmagOnlinePaymentException
     */
    public function cancelOnlinePayment(OrderOnlinePayment $payment, int $sum, bool $needApiCall = false): void
    {
        if (true === $needApiCall) {
            $this->throwExceptionIfPaymentProviderNotSet();
        }

        $status = $payment->getStatus()->getName();

        switch ($status) {
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME:

                if (true === $needApiCall) {
                    $this->providerService->revert($payment, $sum);
                }

                $this->revertOnlinePayment($payment);

                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_DEPOSITED_SHORTNAME:

                if (true === $needApiCall) {
                    $this->providerService->refund($payment, $sum);
                }

                $this->refundOnlinePayment($payment, $sum);

                break;
            default:
                throw new IrmagOnlinePaymentException(
                    sprintf('Cannot cancel online payment %d with status %s', $payment->getOrder()->getId(), $status)
                );

                break;
        }
    }

    /**
     * Устанавливает дополнительные данные по платежу.
     *
     * @param OrderOnlinePayment $payment Платеж
     * @param array              $options Данные платежа
     */
    public function fillExtendedOnlinePaymentOptions(OrderOnlinePayment $payment, $options): void
    {
        if (isset($options['ip'])) {
            $payment->setIpAddress($options['ip']);
        }

        if (isset($options['cardAuthInfo']['cardholderName'])) {
            $payment->setCardHolderName($options['cardAuthInfo']['cardholderName']);
        }

        if (isset($options['cardAuthInfo']['pan'])) {
            $payment->setCardPAN($options['cardAuthInfo']['pan']);
        }

        if (isset($options['cardAuthInfo']['expiration'])) {
            $payment->setCardExpire($options['cardAuthInfo']['expiration']);
        }

        if (isset($options['actionCodeDescription']) && !empty($options['actionCodeDescription'])) {
            $payment->setDeclineReason($options['actionCodeDescription']);
        }

        $this->em->persist($payment);
        $this->em->flush();
    }

    /**
     * Получить все платежи, которые нужно синхронизировать.
     */
    public function getAllPaymentsToSync(): array
    {
        return $this->em->getRepository(OrderOnlinePayment::class)->getAllPaymentsToSync() ?? [];
    }

    /**
     * Обновить данные платежа.
     *
     * @param OrderOnlinePayment    $payment Платёж
     * @param PaymentStateInterface $data    Данные о платеже
     */
    public function updateOrderOnlinePayment(OrderOnlinePayment $payment, PaymentStateInterface $data): void
    {
        $status = $this->em->getRepository(OrderOnlinePaymentStatus::class)->findOneBy(['name' => $data->getStatus()]);

        switch ($status->getName()) {
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_REGISTERED_SHORTNAME:
                $this->setPaymentStatus($payment, $status);
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME:
                $extraData = $data->getExtraData();
                $sum = (!empty($extraData['paymentAmountInfo'])) ? $extraData['paymentAmountInfo']['approvedAmount'] : $data->getAmount();

                $this->approveOnlinePayment($payment, $sum);
                $this->setBankInfo($payment, $extraData);
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_ACS_AUTHORIZED_SHORTNAME:
                $this->setPaymentStatus($payment, $status);
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_DEPOSITED_SHORTNAME:
                $extraData = $data->getExtraData();
                $sum = (!empty($extraData['paymentAmountInfo'])) ? $extraData['paymentAmountInfo']['depositedAmount'] : $data->getAmount();

                if (OrderOnlinePaymentStatus::PAYMENT_STATUS_DEPOSITED_SHORTNAME !== $payment->getStatus()->getName()) {
                    $this->depositOnlinePayment($payment, $sum);
                }

                $this->setBankInfo($payment, $extraData);
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_DECLINED_SHORTNAME:
                $this->declineOnlinePayment($payment);
                $this->setBankInfo($payment, $data->getExtraData());
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_REVERTED_SHORTNAME:
                if (OrderOnlinePaymentStatus::PAYMENT_STATUS_DEPOSITED_SHORTNAME !== $payment->getStatus()->getName()) {
                    $this->revertOnlinePayment($payment);
                }

                $this->setBankInfo($payment, $data->getExtraData());
                break;
            case OrderOnlinePaymentStatus::PAYMENT_STATUS_REFUND_SHORTNAME:
                $extraData = $data->getExtraData();
                $sum = (!empty($extraData['paymentAmountInfo'])) ? $extraData['paymentAmountInfo']['refundedAmount'] : $data->getAmount();

                if (OrderOnlinePaymentStatus::PAYMENT_STATUS_REFUND_SHORTNAME !== $payment->getStatus()->getName()) {
                    $this->refundOnlinePayment($payment, $sum);
                }

                $this->setBankInfo($payment, $extraData);
                break;
        }

        $this->fillExtendedOnlinePaymentOptions($payment, $data->getExtraData());
    }

    /**
     * Устанавливает информацию о банке-эмитенте карты.
     *
     * @param OrderOnlinePayment $payment Платёж
     * @param array              $data    Данные
     */
    public function setBankInfo(OrderOnlinePayment $payment, array $data): void
    {
        if (!empty($data)) {
            if (isset($data['bankInfo']['bankName'])) {
                $payment->setCardBankName($data['bankInfo']['bankName']);
            }

            if (isset($data['bankInfo']['bankCountryName'])) {
                $payment->setCardBankCountry($data['bankInfo']['bankCountryName']);
            }

            $this->em->persist($payment);
            $this->em->flush();
        }
    }

    /**
     * Чистит сессию от значений, нужных для онлайн платежей.
     */
    public function clearOnlinePaymentSession(): void
    {
        if ($this->session->has(self::SESSION_FORCE_NEW_PAYMENT)) {
            $this->session->remove(self::SESSION_FORCE_NEW_PAYMENT);
        }

        if ($this->session->has(self::SESSION_PAYMENT_ORDER_ID)) {
            $this->session->remove(self::SESSION_PAYMENT_ORDER_ID);
        }
    }

    /**
     * Не синхронизировать старые платежи.
     */
    public function disableSyncingForOldPayments(): void
    {
        $this->em->getRepository(OrderOnlinePayment::class)->disableSyncingForOldPayments();
    }

    /**
     * Проверяет платежи перед отменой заказа и, в случае холда, отменяет транзакцию.
     *
     * @param Order $order
     */
    public function cancelHoldTransaction(Order $order): void
    {
        $this->throwExceptionIfPaymentProviderNotSet();

        $payments = $order->getOnlinePayments();

        foreach ($payments as $payment) {
            try {
                $data = $this->providerService->getState($payment);
                $this->updateOrderOnlinePayment($payment, $data);

                if (OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME === $data->getStatus()) {
                    $this->cancelOnlinePayment($payment, $payment->getSumHeld(), true);
                }
            } catch (\Exception $ex) {
                return;
            }
        }
    }

    /**
     * Устанавливает статус оплаты.
     *
     * @param OrderOnlinePayment       $payment Платёж
     * @param OrderOnlinePaymentStatus $status  Статус платежа
     */
    private function setPaymentStatus(OrderOnlinePayment $payment, OrderOnlinePaymentStatus $status): void
    {
        $payment->setStatus($status);
        $this->em->persist($payment);
        $this->em->flush();
    }

    /**
     * Бросает исключение, если не установлен сервис платёжного шлюза.
     *
     * @throws IrmagOnlinePaymentException
     */
    private function throwExceptionIfPaymentProviderNotSet(): void
    {
        if (empty($this->providerService)) {
            throw new IrmagOnlinePaymentException('Payment provider did not set for OnlinePaymentManager!');
        }
    }
}
