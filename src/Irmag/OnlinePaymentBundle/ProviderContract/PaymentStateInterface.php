<?php

namespace Irmag\OnlinePaymentBundle\ProviderContract;

interface PaymentStateInterface
{
    /**
     * Установить статус платежа.
     *
     * @param string $status
     */
    public function setStatus(string $status);

    /**
     * Вернуть статус платежа.
     *
     * @return string
     */
    public function getStatus(): string;

    /**
     * Установить сумму платежа (в копейках).
     *
     * @param int $amount
     */
    public function setAmount(int $amount);

    /**
     * Вернуть сумму платежа.
     *
     * @return int
     */
    public function getAmount(): int;

    /**
     * Установить номер заказа.
     *
     * @param string $orderId
     */
    public function setOrderId(string $orderId);

    /**
     * Вернуть номер заказа.
     *
     * @return string
     */
    public function getOrderId(): string;

    /**
     * Установить дополнительные данные по платежу.
     *
     * @param array $extraData
     */
    public function setExtraData(array $extraData);

    /**
     * Вернуть дополнительные данные по платежу.
     *
     * @return array
     */
    public function getExtraData(): array;
}
