<?php

namespace Irmag\BlogBundle\Mailer;

use Irmag\CoreBundle\Mailer\Mailer;

class BlogMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(
        Mailer $mailer
    ) {
        $this->mailer = $mailer;
    }

    /**
     * @param string|array $to
     * @param array        $context
     *
     * @return int
     */
    public function sendNewPostOnModerationMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagBlog/Emails/new-post-on-moderation-message.html.twig', $context);
    }

    /**
     * @param string|array $to
     * @param array        $context
     *
     * @return int
     */
    public function sendNewFeedbackMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagBlog/Emails/new-feedback-message.html.twig', $context);
    }

    /**
     * @param string|array $to
     * @param array        $context
     *
     * @return int
     */
    public function sendPostWasDeclinedMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagBlog/Emails/post-declined-message.html.twig', $context);
    }

    /**
     * @param string|array $to
     * @param array        $context
     *
     * @return int
     */
    public function sendPostWasPublishedMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagBlog/Emails/post-published-message.html.twig', $context);
    }

    /**
     * @param string|array $to
     * @param array        $context
     *
     * @return int
     */
    public function sendPostAchievedRewardMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagBlog/Emails/post-achieved-reward-message.html.twig', $context);
    }
}
