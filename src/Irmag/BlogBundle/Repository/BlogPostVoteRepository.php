<?php

namespace Irmag\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Irmag\BlogBundle\Entity\BlogPost;

class BlogPostVoteRepository extends EntityRepository
{
    /**
     * Получить рейтинг поста.
     *
     * @param BlogPost $post Пост в блоге
     *
     * @return int
     */
    public function getPostRating(BlogPost $post): int
    {
        return $this->createQueryBuilder('bv')
            ->select('COALESCE(SUM(bv.score), 0) as rating')
            ->where('bv.post = :post')
            ->setParameter('post', $post)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Получить ID пользователей, голосовавших за пост.
     *
     * @param BlogPost $post Пост в блоге
     *
     * @return array
     */
    public function getPostVoters(BlogPost $post): array
    {
        $output = [];
        $votes = $this->createQueryBuilder('bv')
            ->select('u.id')
            ->join('bv.user', 'u')
            ->where('bv.post = :post')
            ->setParameter('post', $post)
            ->getQuery()
            ->getScalarResult();

        foreach ($votes as $vote) {
            $output[] = $vote['id'];
        }

        return $output;
    }

    /**
     * Получить все токены, которые голосовали за этот пост.
     *
     * @param BlogPost $post Пост в блоге
     *
     * @return array
     */
    public function getPostVoterTokens(BlogPost $post): array
    {
        $tokens = [];
        $result = $this->createQueryBuilder('bv')
            ->select('bv.token')
            ->where('bv.post = :post')
            ->andWhere('bv.token IS NOT NULL')
            ->setParameter('post', $post)
            ->distinct()
            ->getQuery()
            ->getArrayResult();

        foreach ($result as $item) {
            $tokens[] = $item['token'];
        }

        return $tokens;
    }
}
