<?php

namespace Irmag\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Irmag\BlogBundle\Entity\BlogPost;

class BlogPostElementRepository extends EntityRepository
{
    /**
     * Получить список обозреваемых товаров и подсчитать рейтинг и кол-во постов.
     *
     * @param bool $isActive Фильтровать связь пост-товар по активности?
     *
     * @return array
     */
    public function getAllItemsWithRates($isActive = true): array
    {
        return $this->createQueryBuilder('be')
            ->select('e.id', 'e.name', 'e.price', 'e.description', 'AVG(be.rate) as rate', 'COUNT(be.id) as cnt')
            ->join('be.element', 'e')
            ->join('be.post', 'p')
            ->where('be.isActive = :isActive')
            ->andWhere('p.status = :status')
            ->groupBy('e.id')
            ->orderBy('cnt', 'DESC')
            ->setParameter('isActive', $isActive)
            ->setParameter('status', BlogPost::BLOG_POST_STATUS_PUBLISHED)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить название товара, обозреваемого в посте, либо первого с самым высоким рейтингом, если их несколько.
     *
     * @param BlogPost $post
     *
     * @return string
     */
    public function getMainBlogPostElementName(BlogPost $post): string
    {
        return $this->createQueryBuilder('be')
            ->select("coalesce(concat(e.name, ' - ', e.toneName), e.name) as name")
            ->join('be.element', 'e')
            ->where('be.post = :post')
            ->andWhere('be.isActive = true')
            ->orderBy('be.rate', 'DESC')
            ->setParameter('post', $post)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
