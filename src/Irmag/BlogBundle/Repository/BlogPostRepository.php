<?php

namespace Irmag\BlogBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Entity\Element;
use Irmag\BlogBundle\Entity\BlogSection;
use Irmag\BlogBundle\Entity\BlogPost;

class BlogPostRepository extends EntityRepository
{
    /**
     * Получить все активные посты.
     *
     * @param int $status Код статуса
     *
     * @return QueryBuilder
     */
    public function getAllPosts($status = BlogPost::BLOG_POST_STATUS_PUBLISHED): QueryBuilder
    {
        return $this->createQueryBuilder('bp')
            ->addSelect('bp.rating AS HIDDEN rating')
            ->addSelect('bp.createdAt AS HIDDEN createdAt')
            ->addSelect('bp.views AS HIDDEN views')
            ->where('bp.status = :status')
            ->andWhere('bp.isActive = true')
            ->groupBy('bp.id')
            ->setParameter('status', $status);
    }

    /**
     * Получить все посты раздела.
     *
     * @param BlogSection $section Раздел блога
     * @param int         $status  Код статуса
     *
     * @return QueryBuilder
     */
    public function getAllPostsBySections(BlogSection $section, $status = BlogPost::BLOG_POST_STATUS_PUBLISHED): QueryBuilder
    {
        return $this->createQueryBuilder('bp')
            ->addSelect('bp.rating AS HIDDEN rating')
            ->addSelect('bp.createdAt AS HIDDEN createdAt')
            ->addSelect('bp.views AS HIDDEN views')
            ->where('bp.status = :status')
            ->andWhere('bp.blogSection = :section')
            ->andWhere('bp.isActive = true')
            ->groupBy('bp.id')
            ->setParameter('status', $status)
            ->setParameter('section', $section);
    }

    /**
     * Получить все посты пользователя.
     *
     * @param User $user          Пользователь
     * @param bool $onlyPublished Показывать только опубликованные
     *
     * @return array
     */
    public function getAllPostsByUser(User $user, $onlyPublished = true): array
    {
        $qb = $this->createQueryBuilder('bp')
            ->select('bp', 'ba', 'f')
            ->leftJoin('bp.attachments', 'ba')
            ->leftJoin('ba.file', 'f')
            ->where('bp.isActive = true')
            ->andWhere('bp.user = :user')
            ->andWhere('ba.isPrimary = true or ba.isPrimary IS NULL');

        if ($onlyPublished) {
            $qb
               ->andWhere('bp.status = :status')
               ->orderBy('bp.createdAt', 'DESC')
               ->setParameter('status', BlogPost::BLOG_POST_STATUS_PUBLISHED);
        } else {
            $qb->orderBy('bp.createdAt', 'DESC');
        }

        return $qb->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить пост по ID с проверкой раздела.
     *
     * @param int    $id   ID поста
     * @param string $slug Slug раздела
     *
     * @return \object|null
     */
    public function getPostBySectionAndId($id, $slug): ?object
    {
        return $this->createQueryBuilder('bp')
            ->join('bp.blogSection', 'sec')
            ->leftJoin('bp.postElements', 'e')
            ->where('bp.isActive = true')
            ->andWhere('bp.id = :id')
            ->andWhere('sec.shortlink = :section')
            ->setParameter('id', $id)
            ->setParameter('section', $slug)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * "Новые обзоры в блоге" на главной странице сайта.
     *
     * @param int $maxResults Ограничитель результатов
     *
     * @return array
     */
    public function getLastPosts($maxResults = 9): array
    {
        return $this->createQueryBuilder('bp')
            ->select('partial bp.{id, title, subtitle, createdAt}, partial bs.{id, shortlink}, bu')
            ->leftJoin('bp.blogSection', 'bs')
            ->leftJoin('bp.user', 'bu')
            ->where('bp.isActive = true')
            ->andWhere('bp.status = :status')
            ->setParameter('status', BlogPost::BLOG_POST_STATUS_PUBLISHED)
            ->orderBy('bp.id', 'desc')
            ->setMaxResults($maxResults)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить все посты, в которых обозревается товар.
     *
     * @param Element $element  Товар
     * @param bool    $isActive Фильтровать посты по активности?
     *
     * @return array
     */
    public function getAllPostsByElement(Element $element, $isActive = true): array
    {
        return $this
            ->createQueryBuilder('bp')
            ->join('bp.postElements', 'be')
            ->join('bp.user', 'u')
            ->where('be.isActive = :isActive')
            ->andWhere('bp.status = :status')
            ->andWhere('be.element = :element')
            ->setParameter('element', $element)
            ->setParameter('isActive', $isActive)
            ->setParameter('status', BlogPost::BLOG_POST_STATUS_PUBLISHED)
            ->getQuery()
            ->getResult();
    }

    /**
     * Возвращает кол-во постов в блоге, в которых обозревается указанный товар.
     *
     * @param Element $element
     *
     * @return int
     */
    public function getBlogPostsCountForElement(Element $element): int
    {
        return $this
            ->createQueryBuilder('bp')
            ->select('COUNT(bp.id)')
            ->join('bp.postElements', 'be')
            ->where('be.isActive = true')
            ->andWhere('bp.status = :status')
            ->andWhere('be.element = :element')
            ->setParameter('status', BlogPost::BLOG_POST_STATUS_PUBLISHED)
            ->setParameter('element', $element)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Получить кол-во всех опубликованных постов, сгруппированных по пользователям.
     *
     * @return array
     */
    public function getAllPublishedPostsGroupedByUsers(): array
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('u as user', 'f', 'COUNT(p.id) as cnt', 'SUM(p.views) as views_cnt', 'SUM(p.rating) as rating_cnt')
            ->from(User::class, 'u')
            ->join('u.posts', 'p')
            ->leftJoin('u.avatar', 'f')
            ->where('p.status = :status')
            ->andWhere('p.isActive = true')
            ->groupBy('u.id', 'f.id')
            ->orderBy('cnt', 'DESC')
            ->setParameter('status', BlogPost::BLOG_POST_STATUS_PUBLISHED)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить все посты-пустышки.
     *
     * @return array
     */
    public function getEmptyPosts(): array
    {
        return $this
            ->createQueryBuilder('bp')
            ->where('bp.status = :status')
            ->andWhere('bp.blogSection IS NULL')
            ->andWhere('bp.title IS NULL')
            ->andWhere('bp.subtitle IS NULL')
            ->andWhere('bp.text IS NULL OR bp.text = :emptyTextHolder')
            ->setParameter('status', BlogPost::BLOG_POST_STATUS_DRAFT)
            ->setParameter('emptyTextHolder', "''")
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить "заброшенные" черновики.
     *
     * @param \DateTime $cutOffDateTime Дата-время отсечки
     *
     * @return array
     */
    public function getAbandonedDrafts(\DateTime $cutOffDateTime): array
    {
        return $this
            ->createQueryBuilder('bp')
            ->where('bp.status = :status')
            ->andWhere('bp.updatedAt < :cutOffDateTime')
            ->setParameter('status', BlogPost::BLOG_POST_STATUS_DRAFT)
            ->setParameter('cutOffDateTime', $cutOffDateTime)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить кол-во черновиков пользователя.
     *
     * @param User $user
     *
     * @return int
     */
    public function getDraftsAmountForUser(User $user): int
    {
        $result = $this
            ->createQueryBuilder('bp')
            ->where('bp.status = :status')
            ->andWhere('bp.user = :user')
            ->setParameter('status', BlogPost::BLOG_POST_STATUS_DRAFT)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();

        return \count($result);
    }
}
