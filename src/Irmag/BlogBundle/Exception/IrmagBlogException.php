<?php

namespace Irmag\BlogBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

class IrmagBlogException extends IrmagException
{
}
