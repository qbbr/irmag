<?php

namespace Irmag\BlogBundle;

final class IrmagBlogEvents
{
    /**
     * Это событие вызывается при отправке нового поста на модерацию.
     */
    const BLOG_NEW_POST_CREATED = 'irmag.blog.post.created';

    /**
     * Это событие вызывается при изменении статуса публикации поста.
     */
    const BLOG_POST_STATUS_CHANGED = 'irmag.blog.post.status_changed';

    /**
     * Это событие вызывается, когда пользователю начисляется вознаграждение за пост.
     */
    const BLOG_POST_GIVE_REWARD_TO_USER = 'irmag.blog.post.reward';

    /**
     * Это событие вызывается при отправке сообщения по каналу обратной связи.
     */
    const BLOG_NEW_FEEDBACK_MESSAGE_CREATED = 'irmag.blog.feedback.created';
}
