<?php

namespace Irmag\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\ProfileBundle\Entity\Group;
use Irmag\ProfileBundle\Entity\User;
use Irmag\BlogBundle\BlogConfig;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Entity\BlogSection;
use Irmag\BlogBundle\Entity\BlogPostVote;
use Irmag\BlogBundle\Entity\BlogPostAttachment;
use Irmag\BlogBundle\Service\SocialPreview;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="irmag_blog_homepage")
     *
     * @param Request $request Объект Request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $qb = $em->getRepository(BlogPost::class)->getAllPosts();
        $posts = $qb->getQuery()->getResult();

        $previews = [];
        foreach ($posts as $post) {
            $postId = $post->getId();
            $previewText = $post->getPreviewTextHtml();
            $previews[$postId] = $previewText;
        }

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $qb,
            $request->query->getInt('page', 1),
            BlogConfig::PAGINATION_POSTS_PER_PAGE_MAIN,
            [
                'sortFieldWhitelist' => ['createdAt', 'views', 'rating'],
                'defaultSortFieldName' => 'createdAt',
                'defaultSortDirection' => 'desc',
            ]
        );

        return $this->render('@IrmagBlog/Default/index.html.twig', [
            'pagination' => $pagination,
            'previews' => $previews,
        ]);
    }

    /**
     * Просмотр по категориям
     *
     * @param Request $request      Объект Request
     * @param string  $categoryslug Slug категории
     *
     * @return Response
     */
    public function categoryAction(Request $request, $categoryslug): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $section = $em->getRepository(BlogSection::class)
            ->findOneBy(['shortlink' => $categoryslug]);

        if (!$section) {
            throw $this->createNotFoundException('Section not found!');
        }

        $qb = $em->getRepository(BlogPost::class)
           ->getAllPostsBySections($section);

        $previews = [];

        foreach ($qb->getQuery()->getResult() as $post) {
            $postId = $post->getId();
            $previewText = $post->getPreviewTextHtml();
            $previews[$postId] = $previewText;
        }

        $pagination = $this->get('knp_paginator')->paginate(
            $qb,
            $request->query->getInt('page', 1),
            BlogConfig::PAGINATION_POSTS_PER_PAGE_SECTION,
            [
                'sortFieldWhitelist' => ['createdAt', 'views', 'rating'],
                'defaultSortFieldName' => 'createdAt',
                'defaultSortDirection' => 'desc',
            ]
        );

        return $this->render('@IrmagBlog/Default/index.html.twig', [
            'pagination' => $pagination,
            'previews' => $previews,
            'category' => $categoryslug,
        ]);
    }

    /**
     * Просмотр поста.
     *
     * @param Request $request
     * @param int     $id           ID поста
     * @param string  $categoryslug Slug категории
     *
     * @return Response
     */
    public function postViewAction(Request $request, int $id, $categoryslug): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $votesRepo = $em->getRepository(BlogPostVote::class);
        $postRepo = $em->getRepository(BlogPost::class);
        $post = $postRepo->getPostBySectionAndId($id, $categoryslug);
        $isAnonymousAlreadyVoted = false;

        if (!$post) {
            // ищем в других категориях
            $post = $postRepo->findOneBy([
                'id' => $id,
                'isActive' => true,
            ]);

            if (!$post) {
                throw $this->createNotFoundException(sprintf('Post with id "%d" was not found!', $id));
            }

            return $this->redirectToRoute(
                'irmag_blog_category_post',
                [
                    'id' => $post->getId(),
                    'categoryslug' => $post->getBlogSection()->getShortlink(),
                ],
                301
            );
        }

        $user = $post->getUser();

        if (BlogPost::BLOG_POST_STATUS_PUBLISHED !== $post->getStatus() && false === $this->isGrantedPostViewing($user)) {
            throw new AccessDeniedHttpException('You have no rights to see this page!');
        }

        if (!$this->getUser()) {
            $registeredVoterTokens = $votesRepo->getPostVoterTokens($post);
            $token = md5($request->getClientIp().$request->headers->get('User-Agent'));

            if (\in_array($token, $registeredVoterTokens, true)) {
                $isAnonymousAlreadyVoted = true;
            }
        }

        $text = $post->getTextHtml();
        $posts = $postRepo->getAllPostsByUser($user);
        $pics = $this->getAllUserPostsPrimaryPictures($posts);

        $items = $this->getAllRelatedElementsData($post);
        $rating = $votesRepo->getPostRating($post);
        $voters = $votesRepo->getPostVoters($post);
        $firstPic = $em->getRepository(BlogPostAttachment::class)->findOneBy(['object' => $post, 'isPrimary' => true]);
        $pic = ($firstPic) ? $firstPic->getFile()->getAbsolutePath() : null;
        $socialPreviewUrl = $pic && file_exists($pic) ? $this->getSocialPreviewPicUrl($post, $pic) : null;

        return $this->render('@IrmagBlog/Post/singlepost.html.twig', [
            'category' => $post->getBlogSection()->getShortlink(),
            'post' => $post,
            'pics' => $pics,
            'voters' => $voters,
            'related_posts' => $posts,
            'text_to_show' => $text,
            'items' => $items,
            'rating' => $rating,
            'social_preview_img' => $socialPreviewUrl,
            'is_anonymous_already_voted' => $isAnonymousAlreadyVoted,
        ]);
    }

    /**
     * Получить первые прикрепленные картинки всех постов.
     *
     * @param array $posts Массив с постами
     *
     * @return array
     */
    protected function getAllUserPostsPrimaryPictures($posts): array
    {
        $pics = [];

        foreach ($posts as $key => $post) {
            $attachments = $post->getAttachments()->toArray();
            $pics[$key] = (!empty($attachments)) ? array_shift($attachments)->getFile()->getWebPath() : null;
        }

        return $pics;
    }

    /**
     * Получить данные для всех товаров обзора (для нужд microdata).
     *
     * @param BlogPost $post Пост в блоге
     *
     * @return array $items
     */
    protected function getAllRelatedElementsData(BlogPost $post): array
    {
        $elements = $post->getPostElements();
        $items = [];

        foreach ($elements as $key => $element) {
            $item = $element->getElement();
            $items[$key]['element'] = $item;
            $items[$key]['rate'] = $element->getRate() ?: '';
        }

        return $items;
    }

    /**
     * Сгенерировать картинку для соцсетей и вернуть её URL.
     *
     * @param BlogPost $post  Пост в блоге
     * @param string   $bgPic Путь к картинке
     *
     * @return string $url
     */
    protected function getSocialPreviewPicUrl(BlogPost $post, $bgPic): string
    {
        $filename = sprintf('blogpost%d.jpg', $post->getId());
        $url = sprintf('%s/%s', $this->getParameter('irmag_social_previews_url'), $filename);
        $socialPicPath = sprintf('%s/%s', realpath($this->getParameter('irmag_social_previews_dir')), $filename);

        if (false === is_readable($socialPicPath)) {
            $socialPreview = $this->get(SocialPreview::class);
            $fontsDir = $this->getParameter('irmag_blog_outers_fonts_dir');
            $outersDir = $this->getParameter('irmag_blog_outers_dir');
            $nick = $post->getUser()->getName();
            $outersMap = [
                'bio-products' => 'outer-bio-products.png',
                'cosmetics-and-perfumery' => 'outer-cosmetics-and-perfumery.png',
                'homeware-goods' => 'outer-homeware-goods.png',
                'professional-tools' => 'outer-professional-tools.png',
            ];

            $socialPreview
                ->setBgImage($bgPic)
                ->setOuterImage(sprintf('%s/%s', $outersDir, $outersMap[$post->getBlogSection()->getShortlink()]))
                ->setTitle($post->getTitle())
                ->setTitleFont(sprintf('%s/%s', $fontsDir, 'pfbeausanspro-sebold-webfont.ttf'))
                ->setSubtitle($post->getSubtitle())
                ->setSubtitleFont(sprintf('%s/%s', $fontsDir, 'pfbeausanspro-thin-webfont.ttf'))
                ->setAuthor($nick)
                ->setAuthorFont(sprintf('%s/%s', $fontsDir, 'pfbeausanspro-thin-webfont.ttf'))
                ->setMaxWordWidth(50)
                ->saveTo($socialPicPath, 95);
        }

        return $url;
    }

    /**
     * Разрешен ли просмотр поста данному пользователю?
     *
     * @param User $user
     *
     * @return bool
     */
    private function isGrantedPostViewing(User $user): bool
    {
        $blogModeratorGroup = $this->get('doctrine.orm.default_entity_manager')->getRepository(Group::class)
            ->findOneBy(['shortname' => 'blog_moderators']);

        if ($user === $this->getUser() || $blogModeratorGroup && $blogModeratorGroup->getUsers()->contains($this->getUser())) {
            return true;
        }

        return false;
    }
}
