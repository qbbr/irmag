<?php

namespace Irmag\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\CoreBundle\Utils\Markdown;
use Irmag\SiteBundle\Entity\Page;
use Irmag\BlogBundle\Form\Type\BlogFeedbackType;
use Irmag\BlogBundle\EventDispatcher\BlogFeedbackEventDispatcher;

class AboutController extends Controller
{
    /**
     * @Route("/about", name="irmag_blog_about")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        return $this->render('@IrmagBlog/About/about.html.twig');
    }

    /**
     * @Route("/about/rules", name="irmag_blog_rules", options={"expose": true})
     *
     * @return Response
     */
    public function rulesAction(): Response
    {
        $page = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Page::class)
            ->findOneBy(['slug' => 'rules', 'isActive' => true]);

        if (!$page) {
            throw $this->createNotFoundException('Page "rules" not found!');
        }

        return $this->render('@IrmagBlog/About/rules.html.twig', ['page' => $page]);
    }

    /**
     * @Route("/about/discount-conditions", name="irmag_blog_conditions")
     *
     * @return Response
     */
    public function discountConditionsAction(): Response
    {
        return $this->render('@IrmagBlog/About/conditions.html.twig');
    }

    /**
     * @Route("/feedback", name="irmag_blog_feedback")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function feedbackAction(Request $request): Response
    {
        $form = $this->createForm(BlogFeedbackType::class, null);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $message = $form->getData();
            $message->setUser($this->getUser());
            $message->setTextHtml($this->get(Markdown::class)->toHtml($message->getText()));

            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($message);
            $em->flush();

            $this->get(BlogFeedbackEventDispatcher::class)->dispatchNewFeedbackMessageCreated($message);

            return $this->render('@IrmagBlog/Default/feedback-finish.html.twig');
        }

        return $this->render('@IrmagBlog/Default/feedback.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
