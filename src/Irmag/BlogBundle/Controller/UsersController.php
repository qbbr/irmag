<?php

namespace Irmag\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\ProfileBundle\Entity\User;
use Irmag\BlogBundle\Entity\BlogPost;

class UsersController extends Controller
{
    /**
     * @Route("/user/", name="irmag_blog_users")
     *
     * @return Response
     */
    public function usersAction(): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');

        return $this->render('@IrmagBlog/Lists/users.html.twig', [
            'list' => $em->getRepository(BlogPost::class)->getAllPublishedPostsGroupedByUsers(),
            'slug' => 'users',
        ]);
    }

    /**
     * @Route("/user/{id}/", name="irmag_blog_user", requirements={"id": "\d+"})
     *
     * @param int $id ID пользователя
     *
     * @return Response
     */
    public function userAction($id): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException('User not found!');
        }

        $posts = ($this->getUser() === $user) ?
            $em->getRepository(BlogPost::class)->getAllPostsByUser($user, false) :
            $em->getRepository(BlogPost::class)->getAllPostsByUser($user);

        $pics = [];
        foreach ($posts as $key => $post) {
            $attachments = $post->getAttachments()->toArray();
            $pics[$key] = (!empty($attachments)) ? array_shift($attachments)->getFile()->getWebPath() : null;
        }

        return $this->render('@IrmagBlog/User/index.html.twig', [
            'related_posts' => $posts,
            'user' => $user,
            'slug' => 'users',
            'pics' => $pics,
        ]);
    }
}
