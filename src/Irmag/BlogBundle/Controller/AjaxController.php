<?php

namespace Irmag\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\SiteBundle\Entity\Element;
use Irmag\AttachmentBundle\Service\AttachmentService;
use Irmag\BlogBundle\Entity\BlogPostAttachment;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Entity\BlogPostElement;
use Irmag\BlogBundle\Service\PostService;

/**
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 * @Route("/ajax", options={"expose": true},
 *     condition="request.isXmlHttpRequest()",
 *     methods={"POST"},
 * host="%irmag_blog_domain%", )
 */
class AjaxController extends Controller
{
    /**
     * @Route("/upload/",
     *     name="irmag_blog_post_upload_pics",
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadPicAction(Request $request): JsonResponse
    {
        $files = $request->files->all();

        if (empty($files['blog_post']['uploader']['uploader'][0])) {
            return new JsonResponse(['success' => false]);
        }

        $file = $files['blog_post']['uploader']['uploader'][0];
        $attach = new BlogPostAttachment();
        $response = $this->get(AttachmentService::class)->upload($file, $attach);

        return new JsonResponse(['success' => true, 'response' => $response]);
    }

    /**
     * @Route("/remove/attachment/",
     *     name="irmag_blog_post_remove_attachment",
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     * )
     *
     * @param Request $request
     *
     * @throws NotFoundHttpException
     *
     * @return JsonResponse
     */
    public function removePicAction(Request $request): JsonResponse
    {
        $id = $request->request->getInt('id');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $attachment = $em->getRepository(BlogPostAttachment::class)->find($id);

        if (!$attachment) {
            throw $this->createNotFoundException('Attachment did not found');
        }

        $this->get(AttachmentService::class)->remove($attachment);

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/remove/post/",
     *     name="irmag_blog_remove_post",
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     * )
     *
     * @param Request $request
     *
     * @throws NotFoundHttpException
     *
     * @return JsonResponse
     */
    public function removePostAction(Request $request): JsonResponse
    {
        $id = $request->request->getInt('id');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $post = $em->getRepository(BlogPost::class)->findOneBy([
            'id' => $id,
            'status' => [BlogPost::BLOG_POST_STATUS_DRAFT, BlogPost::BLOG_POST_STATUS_DECLINED],
            'user' => $this->getUser(),
        ]);

        if (!$post) {
            throw $this->createNotFoundException('Post not found!');
        }

        $em->remove($post);
        $em->flush();

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/remove/element/",
     *     name="irmag_blog_remove_post_element",
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removePostElementAction(Request $request): JsonResponse
    {
        $id = $request->request->getInt('id');
        $postId = $request->request->getInt('post');
        $em = $this->get('doctrine.orm.default_entity_manager');

        if (0 !== $id && 0 !== $postId) {
            $element = $em->getRepository(Element::class)->find($id);

            if (!$element) {
                throw $this->createNotFoundException('Element not found!');
            }

            $postElement = $em->getRepository(BlogPostElement::class)->findOneBy(['element' => $element]);

            if (!$postElement) {
                throw $this->createNotFoundException('PostElement not found!');
            }

            $em->remove($postElement);
            $em->flush();
        }

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/save/",
     *     name="irmag_blog_save_draft",
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function saveDraftAction(Request $request): JsonResponse
    {
        $parameters = $request->request->all();
        $em = $this->get('doctrine.orm.default_entity_manager');
        $options = [
            'id' => (int) $parameters['id'],
            'section' => (int) $parameters['section'],
            'text' => $parameters['md'],
            'title' => $parameters['title'],
            'subtitle' => $parameters['subtitle'],
            'tags' => $parameters['tags'] ?? [],
            'elements' => json_decode($parameters['elements'], true),
            'attachments' => $em->getRepository(BlogPostAttachment::class)->findBy([
                'id' => json_decode($parameters['attachments']),
            ]),
        ];

        $post = $this->get(PostService::class)->save($options);

        return new JsonResponse([
            'success' => true,
            'id' => $post->getId(),
        ]);
    }

    /**
     * @Route("/add_element/", name="irmag_blog_basket_ajax_add_element")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function addToCartAction(Request $request): Response
    {
        return $this->forward('Irmag\\BasketBundle\\Controller\\BasketAjaxController::addElementAction', ['request' => $request]);
    }
}
