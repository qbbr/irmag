<?php

namespace Irmag\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\SiteBundle\Service\ViewManager\ViewManager;

/**
 * @Route("/log",
 *     options={"expose": true},
 *     condition="request.isXmlHttpRequest()",
 *     methods={"POST"},
 *     host="%irmag_blog_domain%"
 * )
 */
class ViewController extends Controller
{
    /**
     * @Route("/view/", name="irmag_blog_log_post_view")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function logViewAction(Request $request): JsonResponse
    {
        $postId = $request->request->getInt('objectId');
        $token = $request->request->get('fingerprint');
        /** @var BlogPost|null $post */
        $post = $this->get('doctrine.orm.default_entity_manager')->getRepository(BlogPost::class)->find($postId);

        if (null === $post) {
            throw new NotFoundHttpException(sprintf('BlogPost with id "%d" not found.', $postId));
        }

        $this->get(ViewManager::class)
            ->setObject($post)
            ->logVisitor($token);

        return new JsonResponse(['success' => true]);
    }
}
