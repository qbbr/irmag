<?php

namespace Irmag\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\AttachmentBundle\Service\TextTools;
use Irmag\BlogBundle\BlogConfig;
use Irmag\BlogBundle\Form\Type\BlogPostType;
use Irmag\BlogBundle\Exception\IrmagBlogException;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Entity\BlogPostAttachment;
use Irmag\BlogBundle\Entity\BlogPostElement;
use Irmag\BlogBundle\EventDispatcher\BlogPostEventDispatcher;
use Irmag\BlogBundle\Service\PostService;

class PostController extends Controller
{
    /**
     * @Route("/post/", name="irmag_blog_new_post")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @return RedirectResponse|Response
     */
    public function newPostAction()
    {
        try {
            $this->checkCurrentNumberOfDrafts();
        } catch (\Exception $ex) {
            return $this->render('@IrmagBlog/Default/drafts-error.html.twig');
        }

        $post = $this->get(PostService::class)->save();

        return $this->redirectToRoute('irmag_blog_update_post', ['id' => $post->getId()]);
    }

    /**
     * @Route("/post/{id}", name="irmag_blog_update_post", requirements={"id": "\d+"}, options={"expose": true})
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     * @param int     $id      ID поста
     *
     * @return Response
     */
    public function updatePostAction(Request $request, int $id): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $postService = $this->get(PostService::class);
        $tagList = $postService->getAllUsedTags();
        $availableTags = [];

        foreach ($tagList as $value) {
            $availableTags[$value] = $value;
        }

        $post = $em->getRepository(BlogPost::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException(sprintf('Post with id "%d" not found!', $id));
        }

        // TODO: сделать админам возможность редактирования
        $allowedStatusesToEdit = [BlogPost::BLOG_POST_STATUS_DRAFT, BlogPost::BLOG_POST_STATUS_DECLINED];
        if ($this->getUser() !== $post->getUser() || false === \in_array($post->getStatus(), $allowedStatusesToEdit, true)) {
            throw new AccessDeniedHttpException('You have no right to edit this post.');
        }

        $attachments = $em->getRepository(BlogPostAttachment::class)->getAllowedAttachments($this->getUser(), $post);
        $form = $this->createForm(BlogPostType::class, $post, ['tags' => $availableTags]);

        if ($request->isMethod(Request::METHOD_POST)) {
            $mainElement = $em->getRepository(BlogPostElement::class)->getMainBlogPostElementName($post);
            $updatedText = $this->get(TextTools::class)->changeImageAltInMarkdown($post->getText(), $mainElement);
            $options = [
                'id' => $post->getId(),
                'status' => BlogPost::BLOG_POST_STATUS_ON_MODERATION,
                'attachments' => $this->get(TextTools::class)->getAttachedToText($post->getTextHtml()),
                'text' => $updatedText,
            ];

            $post = $postService->save($options);
            $this->get(BlogPostEventDispatcher::class)->dispatchNewBlogPostCreated($post);

            return $this->redirectToRoute('irmag_blog_success', []);
        }

        $usedTags = [];

        foreach ($post->getBlogPostTags()->toArray() as $postTag) {
            $usedTags[] = $postTag->getName();
        }

        return $this->render('@IrmagBlog/Post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
            'tagstr' => json_encode($usedTags, JSON_UNESCAPED_UNICODE),
            'attachments' => $attachments,
        ]);
    }

    /**
     * @Route("/success", name="irmag_blog_success")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @return Response
     */
    public function savingSuccessAction(): Response
    {
        return $this->render('@IrmagBlog/Post/success.html.twig');
    }

    /**
     * @throws IrmagBlogException
     */
    private function checkCurrentNumberOfDrafts(): void
    {
        $user = $this->getUser();
        $amount = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(BlogPost::class)
            ->getDraftsAmountForUser($user);

        if ($amount > BlogConfig::MAX_DRAFTS_PER_PERSON) {
            throw new IrmagBlogException(sprintf('User %d exceed limited amount of drafts.', $user->getId()));
        }
    }
}
