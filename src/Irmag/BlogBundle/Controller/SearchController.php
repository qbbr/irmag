<?php

namespace Irmag\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\BlogBundle\Exception\IrmagBlogException;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Entity\BlogPostTag;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Service\SearchQueryNormalizer;

class SearchController extends Controller
{
    /**
     * Ограничение на кол-во найденных постов.
     */
    const LIMIT_ELEMENTS = 10;

    /**
     * @Route("/search/", name="irmag_blog_search", methods={"GET"})
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function searchAction(Request $request): Response
    {
        $query = trim($request->query->get('q'));

        if (!empty($query) && mb_strlen($query) > 2) {
            $blogPostsRepo = $this->get('doctrine.orm.default_entity_manager')->getRepository(BlogPost::class);

            $resultTitles = $blogPostsRepo
                ->createQueryBuilder('bp')
                ->where('bp.isActive = true')
                ->andWhere('bp.status = :statusPublished')
                ->andWhere('LOWER(bp.title) LIKE :term OR LOWER(bp.subtitle) LIKE :term')
                ->setParameter('term', '%'.mb_strtolower($query).'%')
                ->setParameter('statusPublished', BlogPost::BLOG_POST_STATUS_PUBLISHED)
                ->setMaxResults(20)
                ->getQuery()
                ->getResult();

            $resultText = $blogPostsRepo
                ->createQueryBuilder('bp')
                ->where('bp.isActive = true')
                ->andWhere('bp.status = :statusPublished')
                ->andWhere('LOWER(bp.text) LIKE :term')
                ->setParameter('term', '%'.mb_strtolower($query).'%')
                ->setParameter('statusPublished', BlogPost::BLOG_POST_STATUS_PUBLISHED)
                ->setMaxResults(20)
                ->getQuery()
                ->getResult();

            $resultTags = $this->searchPostsByTagName($query);
        }

        return $this->render('@IrmagBlog/Search/index.html.twig', [
            'searchphrase' => $query,
            'result_titles' => $resultTitles ?? [],
            'result_text' => $resultText ?? [],
            'result_tags' => $resultTags ?? [],
        ]);
    }

    /**
     * @Route("/get_elements/",
     *     name="irmag_blog_get_elements",
     *     options={"expose": true},
     *     condition="request.isXmlHttpRequest()",
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     *     methods={"GET", "POST"},
     *     host="%irmag_blog_domain%",
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function searchElementAction(Request $request): JsonResponse
    {
        $data = [];
        $searchQuery = $request->get('term');

        if (empty($searchQuery)) {
            return new JsonResponse($data);
        }

        $searchQuery = SearchQueryNormalizer::normilize($searchQuery);
        $elements = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Element::class)
            ->getSearchQb($searchQuery)
            ->getQuery()
            ->setMaxResults(self::LIMIT_ELEMENTS)
            ->getResult();

        if (!$elements) {
            return new JsonResponse($data);
        }

        foreach ($elements as $element) {
            $data['results'][] = [
                'id' => $element->getId(),
                'text' => empty($element->getToneName())
                    ? $element->getName()
                    : sprintf('%s - %s', $element->getName(), $element->getToneName()),
            ];
        }

        return new JsonResponse($data);
    }

    /**
     * @Route("/tag/", name="irmag_blog_tag_search", methods={"GET"})
     *
     * @param Request $request
     *
     * @throws IrmagBlogException
     *
     * @return Response
     */
    public function tagAction(Request $request): Response
    {
        $query = trim($request->query->get('t'));

        if (!empty($query) && mb_strlen($query) > 2) {
            if (mb_strlen($query) > 255) {
                throw new IrmagBlogException('Query is too long.');
            }

            $result = $this->searchPostsByTagName($query);
        }

        return $this->render('@IrmagBlog/Search/tags.html.twig', [
            'searchphrase' => $query,
            'result' => $result ?? [],
        ]);
    }

    /**
     * Возвращает посты блога по тегам.
     *
     * @param string $tagName Название тега
     *
     * @return array
     */
    private function searchPostsByTagName($tagName): array
    {
        $tag = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(BlogPostTag::class)
            ->findOneBy(['name' => $tagName]);

        if (!$tag) {
            return [];
        }

        $posts = $tag->getBlogPosts();

        if (!$posts) {
            return [];
        }

        $filteredPosts = [];

        foreach ($posts as $post) {
            if (BlogPost::BLOG_POST_STATUS_PUBLISHED === $post->getStatus()) {
                $filteredPosts[] = $post;
            }
        }

        return $filteredPosts;
    }
}
