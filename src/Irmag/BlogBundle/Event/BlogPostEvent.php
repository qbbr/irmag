<?php

namespace Irmag\BlogBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Irmag\BlogBundle\Entity\BlogPost;

class BlogPostEvent extends Event
{
    /**
     * @var BlogPostEvent
     */
    private $post;

    /**
     * @param BlogPost $post
     */
    public function __construct(BlogPost $post)
    {
        $this->post = $post;
    }

    /**
     * @return BlogPost
     */
    public function getBlogPost(): BlogPost
    {
        return $this->post;
    }
}
