<?php

namespace Irmag\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\SiteBundle\Service\ViewManager\IrmagViewableInterface;
use Irmag\SiteBundle\Service\ViewManager\IrmagViewInterface;

/**
 * @ORM\Table(name="blog_post_views", indexes={
 *     @ORM\Index(name="blog_view_search_idx", columns={"post_id", "token"})
 * })
 * @ORM\Entity
 */
class BlogPostView implements IrmagViewInterface
{
    use TimestampableEntity;

    /**
     * Время жизни просмотра, в часах.
     */
    const BLOG_POST_VIEW_LIFETIME = 3;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $token;

    /**
     * @var BlogPost
     *
     * @ORM\ManyToOne(targetEntity="BlogPost")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $object;

    /**
     * XXX: Custom.
     *
     * @return int
     */
    public function getViewLifetime(): int
    {
        return self::BLOG_POST_VIEW_LIFETIME;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getObject(): ?IrmagViewableInterface
    {
        return $this->object;
    }

    public function setObject(?IrmagViewableInterface $object): self
    {
        $this->object = $object;

        return $this;
    }
}
