<?php

namespace Irmag\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachable;
use Symfony\Component\Validator\Constraints as Assert;
use Irmag\ThreadCommentBundle\Entity\Traits\ThreadTrait;
use Irmag\ThreadCommentBundle\Entity\ThreadOwnerInterface;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachmentPoint;
use Irmag\SiteBundle\Service\ViewManager\IrmagViewableInterface;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Table(name="blog_posts")
 * @ORM\Entity(repositoryClass="Irmag\BlogBundle\Repository\BlogPostRepository")
 */
class BlogPost implements ThreadOwnerInterface, IrmagAttachmentPoint, IrmagViewableInterface
{
    use TimestampableEntity;
    use ThreadTrait;

    const ATTACHMENT_CLASS_NAME = BlogPostAttachment::class;
    const VIEW_CLASS_NAME = BlogPostView::class;

    /**
     * Статусы постов.
     */
    const BLOG_POST_STATUS_DRAFT = 1;
    const BLOG_POST_STATUS_ON_MODERATION = 2;
    const BLOG_POST_STATUS_DECLINED = 4;
    const BLOG_POST_STATUS_PUBLISHED = 8;

    public $statusNames = [
        self::BLOG_POST_STATUS_DRAFT => 'Черновик',
        self::BLOG_POST_STATUS_ON_MODERATION => 'На модерации',
        self::BLOG_POST_STATUS_DECLINED => 'Отклонён',
        self::BLOG_POST_STATUS_PUBLISHED => 'Опубликован',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User", inversedBy="posts")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @var string
     *
     * @Assert\Length(
     *     min=0,
     *     max=255,
     *     maxMessage="Длина заголовка превышает допустимое ограничение в {{ limit }} символов."
     * )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @Assert\Length(
     *     min=0,
     *     max=255,
     *     maxMessage="Длина подзаголовка превышает допустимое ограничение в {{ limit }} символов."
     * )
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $subtitle;

    /**
     * @var int
     *
     * @Assert\Choice({1, 2, 4, 8})
     *
     * @ORM\Column(type="integer", options={"default": \Irmag\BlogBundle\Entity\BlogPost::BLOG_POST_STATUS_DRAFT})
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $textHtml;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $previewTextHtml;

    /**
     * @var BlogSection
     *
     * @ORM\ManyToOne(targetEntity="BlogSection")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $blogSection;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $userModifiedBy;

    /**
     * @var Collection|BlogPostTag[]
     *
     * @ORM\ManyToMany(targetEntity="BlogPostTag", inversedBy="blogPosts")
     * @ORM\JoinTable(name="blog_posts_tags")
     */
    private $blogPostTags;

    /**
     * @var Collection|BlogPostElement[]
     *
     * @ORM\OneToMany(targetEntity="BlogPostElement", mappedBy="post", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $postElements;

    /**
     * @var Collection|BlogPostVote[]
     *
     * @ORM\OneToMany(targetEntity="BlogPostVote", mappedBy="post")
     */
    private $postVotes;

    /**
     * @var Collection|BlogPostAttachment[]
     *
     * @ORM\OneToMany(targetEntity="BlogPostAttachment", mappedBy="object", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $attachments;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $views;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $declineReason;

    /**
     * Текущий рейтинг поста.
     *
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $rating;

    /**
     * Начислен ли бонус?
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isBonusPayed;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    public function __construct()
    {
        $this->blogPostTags = new ArrayCollection();
        $this->postElements = new ArrayCollection();
        $this->postVotes = new ArrayCollection();
        $this->attachments = new ArrayCollection();
        $this->status = self::BLOG_POST_STATUS_DRAFT;
        $this->isActive = true;
        $this->views = 0;
        $this->isBonusPayed = false;
        $this->rating = 0;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->getTitle();
    }

    /**
     * XXX: Custom for ThreadCommentBundle.
     */
    public function setThreadOwnerRoute(): void
    {
        $this->getThread()->setOwnerRoute([
            'name' => 'irmag_blog_category_post',
            'parameters' => [
                'categoryslug' => $this->getBlogSection()->getShortlink(),
                'id' => $this->getId(),
            ],
        ]);
    }

    /**
     * Get attachment class name.
     * XXX: custom.
     *
     * @return string
     */
    public function getAttachmentClassName(): string
    {
        return self::ATTACHMENT_CLASS_NAME;
    }

    /**
     * Get object class name.
     * Use it to get class, because standard "get_class()" returns classname with proxy namespaces.
     * XXX: custom.
     *
     * @return string
     */
    public function getClassName(): string
    {
        return __CLASS__;
    }

    /**
     * XXX: custom.
     * Get all post tags as comma-separated string.
     *
     * @return string
     */
    public function getPostTagsAsString(): string
    {
        $tags = $this->getBlogPostTags()->toArray();
        $tagStr = '';

        /** @var BlogPostTag $tag */
        foreach ($tags as $key => $tag) {
            $tagStr .= (0 === $key || $key === \count($tags) - 1) ? $tag->getName() : ', '.$tag->getName();
        }

        return $tagStr;
    }

    /**
     * XXX: custom.
     * Get view object class.
     *
     * @return string
     */
    public function getViewClass(): string
    {
        return self::VIEW_CLASS_NAME;
    }

    /**
     * XXX: Custom for Sonata Admin.
     *
     * @param Collection $postElements
     *
     * @return $this
     */
    public function setPostElements(Collection $postElements): self
    {
        if (\count($postElements) > 0) {
            foreach ($postElements as $postElement) {
                $postElement->setPost($this);
                $this->addPostElement($postElement);
            }
        }

        return $this;
    }

    /**
     * XXX: Custom for Sonata Admin.
     *
     * @param Collection $postAttachments
     *
     * @return $this
     */
    public function setPostAttachments(Collection $postAttachments): self
    {
        if (\count($postAttachments) > 0) {
            foreach ($postAttachments as $postAttachment) {
                $postAttachment->setFile($postAttachment->getFile());
                $postAttachment->setObject($this);
                $this->addAttachment($postAttachment);
            }
        }

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(?string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getTextHtml(): ?string
    {
        return $this->textHtml;
    }

    public function setTextHtml(?string $textHtml): self
    {
        $this->textHtml = $textHtml;

        return $this;
    }

    public function getPreviewTextHtml(): ?string
    {
        return $this->previewTextHtml;
    }

    public function setPreviewTextHtml(?string $previewTextHtml): self
    {
        $this->previewTextHtml = $previewTextHtml;

        return $this;
    }

    public function getViews(): int
    {
        return $this->views;
    }

    public function setViews(int $views): self
    {
        $this->views = $views;

        return $this;
    }

    public function getDeclineReason(): ?string
    {
        return $this->declineReason;
    }

    public function setDeclineReason(?string $declineReason): self
    {
        $this->declineReason = $declineReason;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getIsBonusPayed(): ?bool
    {
        return $this->isBonusPayed;
    }

    public function setIsBonusPayed(bool $isBonusPayed): self
    {
        $this->isBonusPayed = $isBonusPayed;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getBlogSection(): ?BlogSection
    {
        return $this->blogSection;
    }

    public function setBlogSection(?BlogSection $blogSection): self
    {
        $this->blogSection = $blogSection;

        return $this;
    }

    public function getUserModifiedBy(): ?User
    {
        return $this->userModifiedBy;
    }

    public function setUserModifiedBy(?User $userModifiedBy): self
    {
        $this->userModifiedBy = $userModifiedBy;

        return $this;
    }

    /**
     * @return Collection|BlogPostTag[]
     */
    public function getBlogPostTags(): Collection
    {
        return $this->blogPostTags;
    }

    public function addBlogPostTag(BlogPostTag $blogPostTag): self
    {
        if (!$this->blogPostTags->contains($blogPostTag)) {
            $this->blogPostTags[] = $blogPostTag;
        }

        return $this;
    }

    public function removeBlogPostTag(BlogPostTag $blogPostTag): self
    {
        if ($this->blogPostTags->contains($blogPostTag)) {
            $this->blogPostTags->removeElement($blogPostTag);
        }

        return $this;
    }

    /**
     * @return Collection|BlogPostElement[]
     */
    public function getPostElements(): Collection
    {
        return $this->postElements;
    }

    public function addPostElement(BlogPostElement $postElement): self
    {
        if (!$this->postElements->contains($postElement)) {
            $this->postElements[] = $postElement;
            $postElement->setPost($this);
        }

        return $this;
    }

    public function removePostElement(BlogPostElement $postElement): self
    {
        if ($this->postElements->contains($postElement)) {
            $this->postElements->removeElement($postElement);
            // set the owning side to null (unless already changed)
            if ($postElement->getPost() === $this) {
                $postElement->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BlogPostVote[]
     */
    public function getPostVotes(): Collection
    {
        return $this->postVotes;
    }

    public function addPostVote(BlogPostVote $postVote): self
    {
        if (!$this->postVotes->contains($postVote)) {
            $this->postVotes[] = $postVote;
            $postVote->setPost($this);
        }

        return $this;
    }

    public function removePostVote(BlogPostVote $postVote): self
    {
        if ($this->postVotes->contains($postVote)) {
            $this->postVotes->removeElement($postVote);
            // set the owning side to null (unless already changed)
            if ($postVote->getPost() === $this) {
                $postVote->setPost(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BlogPostAttachment[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(IrmagAttachable $attachment): self
    {
        if (!$this->attachments->contains($attachment)) {
            $this->attachments[] = $attachment;
            $attachment->setObject($this);
        }

        return $this;
    }

    public function removeAttachment(IrmagAttachable $attachment): self
    {
        if ($this->attachments->contains($attachment)) {
            $this->attachments->removeElement($attachment);
            // set the owning side to null (unless already changed)
            if ($attachment->getObject() === $this) {
                $attachment->setObject(null);
            }
        }

        return $this;
    }
}
