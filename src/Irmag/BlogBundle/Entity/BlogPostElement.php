<?php

namespace Irmag\BlogBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Irmag\SiteBundle\Entity\Element;

/**
 * @ORM\Table(name="blog_posts_elements")
 * @ORM\Entity(repositoryClass="Irmag\BlogBundle\Repository\BlogPostElementRepository")
 */
class BlogPostElement
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var BlogPost
     *
     * @ORM\ManyToOne(targetEntity="BlogPost", inversedBy="postElements", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $post;

    /**
     * @var Element
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\SiteBundle\Entity\Element")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $element;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $rate;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    public function __construct()
    {
        $this->isActive = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRate(): ?int
    {
        return $this->rate;
    }

    public function setRate(?int $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getPost(): ?BlogPost
    {
        return $this->post;
    }

    public function setPost(?BlogPost $post): self
    {
        $this->post = $post;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }
}
