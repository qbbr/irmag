<?php

namespace Irmag\BlogBundle;

class BlogConfig
{
    /** Постов на главной */
    const PAGINATION_POSTS_PER_PAGE_MAIN = 5;
    /** Постов в разделе */
    const PAGINATION_POSTS_PER_PAGE_SECTION = 5;

    /**
     * Начисление бонусов.
     */

    /** Кол-во лайков для получения бонуса */
    const BLOG_BONUS_REWARD_LIKES_AMOUNT = 100;
    /** Сумма вознаграждения в фантиках */
    const BLOG_BONUS_REWARD_SUM = 100;

    /** Ограничение по кол-ву черновиков */
    const MAX_DRAFTS_PER_PERSON = 10;
}
