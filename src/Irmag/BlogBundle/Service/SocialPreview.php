<?php

namespace Irmag\BlogBundle\Service;

use Irmag\BlogBundle\Exception\IrmagBlogException;

/**
 * Сервис для генерации preview баннера для соц.сетей.
 *
 * @example
 *
 * $socialPreview
 *     ->setBgImage('item.jpg')
 *     ->setOuterImage('blog_share_color_purple.png')
 *     ->setTitle('Обзор на подкручивающую тушь Wild Curl от Lumene')
 *     ->setTitleFont('/var/www/irmag.symfony/web/assets/fonts/pfbeausanspro-sebold-webfont.ttf')
 *     ->setSubtitle('Формула для необузданного изгиба против упрямых ресниц: кто кого?')
 *     ->setSubtitleFont('/var/www/irmag.symfony/web/assets/fonts/pfbeausanspro-thin-webfont.ttf')
 *     ->setAuthor('Мария Крепель')
 *     ->setAuthorFont('/var/www/irmag.symfony/web/assets/fonts/pfbeausanspro-thin-webfont.ttf')
 *     ->setMaxWordWidth(50)
 *     ->saveTo('output.jpg', 95);
 */
class SocialPreview
{
    private $bgImage;
    private $bgImageWidth;
    private $bgImageHeight;
    private $outerImage;
    private $outerImageWidth;
    private $outerImageHeight;
    private $title;
    private $titleFont;
    private $subtitle;
    private $subtitleFont;
    private $author;
    private $authorFont;
    private $maxWordWidth = 60;
    private $textOffsetTop = 100;
    private $image;
    private $cropOffsetX = 0;
    private $cropOffsetY = 0;

    /**
     * Задаёт изображение для фона (jpeg).
     *
     * @param string $path Путь до изображения
     *
     * @throws IrmagBlogException
     *
     * @return SocialPreview $this
     */
    public function setBgImage($path): self
    {
        if (!empty($path) && file_exists($path)) {
            $imageType = exif_imagetype($path);
            switch ($imageType) {
                case IMAGETYPE_JPEG === $imageType:
                    $this->bgImage = imagecreatefromjpeg($path);
                    break;
                case IMAGETYPE_PNG === $imageType:
                    $this->bgImage = imagecreatefrompng($path);
                    break;
                case IMAGETYPE_GIF === $imageType:
                    $this->bgImage = imagecreatefromgif($path);
                    break;
                default:
                    throw new IrmagBlogException('Unsupported image type!');
                    break;
            }
        } else {
            $this->bgImage = imagecreatetruecolor(1000, 420);
            imagecolorallocate($this->bgImage, 77, 77, 77);
        }

        imagealphablending($this->bgImage, true);

        $this->bgImageWidth = imagesx($this->bgImage);
        $this->bgImageHeight = imagesy($this->bgImage);

        return $this;
    }

    /**
     * Задаёт изображение, которое будет накладываться сверху (png).
     *
     * @param string $path Путь до изображения
     *
     * @return SocialPreview $this
     */
    public function setOuterImage($path): self
    {
        $this->outerImage = imagecreatefrompng($path);
        imagealphablending($this->outerImage, false);
        imagesavealpha($this->outerImage, true);

        $this->outerImageWidth = imagesx($this->outerImage);
        $this->outerImageHeight = imagesy($this->outerImage);

        return $this;
    }

    /**
     * Задаёт текст заголовка.
     *
     * @param string $text Заголовок
     *
     * @return SocialPreview $this
     */
    public function setTitle($text): self
    {
        $this->title = $text;

        return $this;
    }

    /**
     * Задаёт шрифт для заголовка.
     *
     * @param string $path Путь до TTF шрифта
     *
     * @return SocialPreview $this
     */
    public function setTitleFont($path): self
    {
        $this->titleFont = $path;

        return $this;
    }

    /**
     * Задаёт текст подзаголовока.
     *
     * @param string $text Подзаголовок
     *
     * @return SocialPreview $this
     */
    public function setSubtitle($text): self
    {
        $this->subtitle = $text;

        return $this;
    }

    /**
     * Задаёт шрифт для подзаголовока.
     *
     * @param string $path Путь до TTF шрифта
     *
     * @return SocialPreview $this
     */
    public function setSubtitleFont($path): self
    {
        $this->subtitleFont = $path;

        return $this;
    }

    /**
     * Задаёт автора.
     *
     * @param string $text Автор
     *
     * @return SocialPreview $this
     */
    public function setAuthor($text): self
    {
        $this->author = $text;

        return $this;
    }

    /**
     * Задаёт шрифт для автора.
     *
     * @param string $path Путь до TTF шрифта
     *
     * @return SocialPreview $this
     */
    public function setAuthorFont($path): self
    {
        $this->authorFont = $path;

        return $this;
    }

    /**
     * Задаёт максимальное количество символов в одной строке (для переноса).
     *
     * @param int $width Количество символов
     *
     * @return SocialPreview $this
     */
    public function setMaxWordWidth($width): self
    {
        $this->maxWordWidth = $width;

        return $this;
    }

    /**
     * Сохраняет изображение в файл.
     *
     * @param string $path    Путь до файла сохранения
     * @param int    $quality Качество JPEG изображения
     *
     * @return bool
     */
    public function saveTo($path, $quality = 90): bool
    {
        if (null === $this->image) {
            $this->draw();
        }

        return imagejpeg($this->image, $path, $quality);
    }

    /**
     * Отрисовывает.
     */
    protected function draw(): void
    {
        // если ширина bgImage !== outerImage, то центруем crop по оси X
        // и вычисляем новый height с учётом пропорции картинки
        if ($this->bgImageWidth !== $this->outerImageWidth && $this->bgImageHeight >= $this->outerImageHeight) {
            $ratio = $this->bgImageWidth / $this->bgImageHeight;
            $newWidth = $this->outerImageWidth;
            $newHeight = $newWidth / $ratio;

            if ($this->bgImageWidth > $this->outerImageWidth) {
                imagecopyresized($this->bgImage, $this->bgImage, 0, 0, 0, 0, $newWidth, $newHeight, $this->bgImageWidth, $this->bgImageHeight);
            } else {
                $image = imagecreatetruecolor($newWidth, $newHeight);
                imagecopyresampled($image, $this->bgImage, 0, 0, 0, 0, $newWidth, $newHeight, $this->bgImageWidth, $this->bgImageHeight);
                $this->bgImage = $image;
            }

            $this->cropOffsetX = ($newWidth - $this->outerImageWidth) / 2;

            $this->bgImageWidth = $newWidth;
            $this->bgImageHeight = $newHeight;
        }

        // если высота bgImage больше outerImage, то центруем crop по оси Y
        if ($this->bgImageHeight > $this->outerImageHeight) {
            $this->cropOffsetY = ($this->bgImageHeight - $this->outerImageHeight) / 2;
        } elseif ($this->bgImageHeight < $this->outerImageWidth) {
            // высота bgImage < outerImage
            $ratio = $this->bgImageWidth / $this->bgImageHeight;
            $newHeight = $this->outerImageHeight;
            $newWidth = $newHeight * $ratio;

            $image = imagecreatetruecolor($newWidth, $newHeight);
            imagecopyresampled($image, $this->bgImage, 0, 0, 0, 0, $newWidth, $newHeight, $this->bgImageWidth, $this->bgImageHeight);
            $this->bgImage = $image;
        }

        // crop bgImage
        //$this->bgImage = imagecrop($this->bgImage, ['x' => $this->cropOffsetX, 'y' => $this->cropOffsetY, 'width' => $this->outerImageWidth, 'height' => $this->outerImageHeight]);

        $this->bgImage = $this->crop($this->bgImage, ['x' => $this->cropOffsetX, 'y' => $this->cropOffsetY, 'width' => $this->outerImageWidth, 'height' => $this->outerImageHeight]);

        // накладываем outerImage
        imagecopy($this->bgImage, $this->outerImage, 0, 0, 0, 0, $this->outerImageWidth, $this->outerImageHeight);

        $this->image = $this->bgImage;

        if (null !== $this->title) {
            $offsetTop = $this->drawText($this->title, $this->textOffsetTop, $this->titleFont, 32);

            if (null !== $this->subtitle) {
                $offsetTop = $this->drawText($this->subtitle, $offsetTop + 50, $this->subtitleFont, 28);
            }

            if (null !== $this->author) {
                $this->drawText($this->author, $offsetTop + 50, $this->authorFont, 22);
            }
        }
    }

    /**
     * Обрезка изображения.
     *
     * @param resource $image
     * @param array    $rect
     *
     * @return resource
     */
    protected function crop($image, array $rect)
    {
        $dest = imagecreatetruecolor($rect['width'], $rect['height']);
        imagecopy(
            $dest,
            $image,
            0,
            0,
            $rect['x'],
            $rect['y'],
            $rect['width'],
            $rect['height']
        );

        return $dest;
    }

    /**
     * Рисует текст.
     *
     * @param string $text      Текст
     * @param int    $offsetTop Отступ сверху
     * @param string $font      Путь до шрифта TTF
     * @param int    $fontSize  Размер шрифта
     *
     * @return int $offsetTop Отступ сверху
     */
    protected function drawText($text, $offsetTop, $font, $fontSize): int
    {
        // устанавливаем переносы строк, если длинное название
        $text = wordwrap($text, $this->maxWordWidth);
        // массив
        $textArr = explode("\n", $text);
        // начальный отступ сверху
        // рисуем построчно с выравниваем
        foreach ($textArr as $text) {
            $offsetTop += $this->drawTextLine($text, $offsetTop, $font, $fontSize);
        }

        return $offsetTop;
    }

    /**
     * Рисует строку текста.
     *
     * @param string    $text
     * @param int       $offsetTop
     * @param string    $font
     * @param float|int $fontSize
     *
     * @return int $textHeight Высота строки
     */
    protected function drawTextLine($text, $offsetTop, $font, $fontSize): int
    {
        $color = imagecolorallocate($this->image, 255, 255, 255);

        $textBox = imagettfbbox($fontSize, 0, $font, $text);
        $textWidth = abs($textBox[4] - $textBox[0]);
        $textHeight = abs($textBox[5] - $textBox[1]);

        $xsize = (imagesx($this->image) / 2) - ($textWidth / 2);

        imagettftext($this->image, $fontSize, 0, $xsize, $offsetTop, $color, $font, $text);

        return $textHeight;
    }

    public function __destruct()
    {
        if (null !== $this->image) {
            @imagedestroy($this->bgImage);
            @imagedestroy($this->outerImage);
        }
    }
}
