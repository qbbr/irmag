<?php

namespace Irmag\BlogBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Entity\Element;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Entity\BlogSection;
use Irmag\BlogBundle\Entity\BlogPostElement;
use Irmag\BlogBundle\Entity\BlogPostTag;
use Irmag\BlogBundle\Exception\IrmagBlogException;

/**
 * Класс для работы с постами блога.
 */
class PostService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Создание / редактирование поста.
     *
     * @param array $options Массив значений
     *
     * @return BlogPost $post
     */
    public function save(array $options = null): BlogPost
    {
        $currentUser = $this->getUserOrThrowException();
        $defaultStatus = BlogPost::BLOG_POST_STATUS_DRAFT;

        if (null === $options) {
            $post = new BlogPost();
            $post->setUser($currentUser);
            $post->setStatus($defaultStatus);
            $post->setText('');
        } else {
            /** @var BlogPost $post */
            $post = $this->getEntityByIdOrThrowException($this->getOption($options, 'id', true), BlogPost::class);
            $section = $this->getEntityByIdOrThrowException($this->getOption($options, 'section'), BlogSection::class);
            $status = $this->getOption($options, 'status') ?: $defaultStatus;

            $title = $this->getOption($options, 'title');
            $subtitle = $this->getOption($options, 'subtitle');
            $text = $this->getOption($options, 'text');
            $user = $this->getOption($options, 'user') ?: $currentUser;
            $userModifiedBy = $this->getOption($options, 'userModifiedBy') ?: $currentUser;
            $elements = $this->getOption($options, 'elements');

            $this->setOptionIfExists($status, $post, 'setStatus');
            $this->setOptionIfExists($section, $post, 'setBlogSection');
            $this->setOptionIfExists($title, $post, 'setTitle');
            $this->setOptionIfExists($subtitle, $post, 'setSubtitle');
            $this->setOptionIfExists($text, $post, 'setText');
            $this->setOptionIfExists($user, $post, 'setUser');
            $this->setOptionIfExists($userModifiedBy, $post, 'setUserModifiedBy');
            $this->setPostTags($options, $post);

            $postElements = $this->setPostElements($elements, $post);

            foreach ($postElements as $postElement) {
                $post->addPostElement($postElement);
            }
        }

        $this->em->persist($post);
        $this->em->flush();

        return $post;
    }

    /**
     * Получить список всех использовавшихся в постах тегов в алфавитном порядке.
     *
     * @return array
     */
    public function getAllUsedTags(): array
    {
        $tagNames = [];
        $tags = $this->em->getRepository(BlogPostTag::class)->findAll();

        foreach ($tags as $tag) {
            $tagNames[] = $tag->getName();
        }

        sort($tagNames, SORT_LOCALE_STRING);

        return $tagNames;
    }

    /**
     * Установить теги для поста.
     *
     * @param array    $options Массив опций
     * @param BlogPost $post    Пост блога
     */
    protected function setPostTags($options, BlogPost $post): void
    {
        if (!empty($options['tags'])) {
            $postTags = $post->getBlogPostTags()->toArray();

            foreach ($postTags as $tag) {
                $post->removeBlogPostTag($tag);
                $this->em->persist($post);
            }

            $tags = explode(',', $options['tags']);

            foreach ($tags as $tag) {
                $postTag = $this->em->getRepository(BlogPostTag::class)->findOneBy(['name' => $tag]);

                if (!$postTag) {
                    $postTag = new BlogPostTag();
                    $postTag->setName($tag);
                    $postTag->addBlogPost($post);
                    $this->em->persist($postTag);
                    $post->addBlogPostTag($postTag);
                    $this->em->persist($post);
                } else {
                    $post->addBlogPostTag($postTag);
                    $this->em->persist($post);
                }
            }
        }
    }

    /**
     * Прикрепить элементы к посту.
     *
     * @param mixed    $elements Массив с Element IDs или null
     * @param BlogPost $post     Пост блога
     *
     * @return array
     */
    protected function setPostElements($elements, BlogPost $post): array
    {
        $postElements = [];

        if (null !== $elements) {
            foreach ($post->getPostElements()->toArray() as $exElement) {
                $this->em->remove($exElement);
            }

            $this->em->flush();

            foreach ($elements as $item) {
                /** @var Element $element */
                $element = $this->getEntityByIdOrThrowException((int) $item['element'], Element::class);
                $postElement = new BlogPostElement();
                $postElement->setElement($element);
                $postElement->setRate((int) $item['rate']);
                $postElement->setPost($post);
                $this->em->persist($postElement);
                $postElements[] = $postElement;
            }
        }

        return $postElements;
    }

    /**
     * Возвращает значение элемента формы,  если оно установлено и не пусто.
     * Если isRequired = true, то в бросает исключение, иначе вернёт null.
     *
     * @param array  $options    Массив значений
     * @param string $option     Индекс нужного значения
     * @param bool   $isRequired Обязательное ли поле
     *
     * @throws IrmagBlogException
     *
     * @return mixed|null
     */
    protected function getOption(array $options, $option, $isRequired = false)
    {
        if (false === isset($options[$option]) || !$options[$option]) {
            if ($isRequired) {
                throw new IrmagBlogException(sprintf('Required option "%s" did not set!', $option));
            }

            return null;
        }

        return $options[$option];
    }

    /**
     * Вызывает сеттер-метод объекта BlogPost, если значение не null.
     *
     * @param mixed    $option   Значение для установки
     * @param BlogPost $post     Объект BlogPost
     * @param string   $callback Название сеттер-метода
     */
    protected function setOptionIfExists($option, BlogPost $post, $callback): void
    {
        if (null !== $option) {
            $post->$callback($option);
        }
    }

    /**
     * Возвращает экземпляр сущности по id, бросает исключение если сущности с такой ID нет.
     * Не делает ничего, если id === null.
     *
     * @param int    $id              ID сущности
     * @param string $repositoryClass Класс Entity
     *
     * @throws NotFoundHttpException
     *
     * @return \object|null
     */
    protected function getEntityByIdOrThrowException(?int $id, string $repositoryClass): ?object
    {
        if (null !== $id) {
            $entity = $this->em->getRepository($repositoryClass)->find($id);

            if (!$entity) {
                throw new NotFoundHttpException(sprintf(
                    'Could not find entity of class "%s" with id = %d!',
                    $repositoryClass,
                    $id
                ));
            }

            return $entity;
        }

        return null;
    }

    /**
     * Проверяет авторизацию пользователя.
     *
     * @throws IrmagBlogException
     *
     * @return User
     */
    protected function getUserOrThrowException(): User
    {
        $token = $this->tokenStorage->getToken();

        if (null !== $token) {
            $user = $token->getUser();

            if ($user instanceof User) {
                return $user;
            }
        }

        throw new IrmagBlogException('Authentication not found!');
    }
}
