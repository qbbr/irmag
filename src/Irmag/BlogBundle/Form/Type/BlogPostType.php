<?php

namespace Irmag\BlogBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Qbbr\EntityHiddenTypeBundle\Form\Type\EntityHiddenType;
use Irmag\AttachmentBundle\Form\Type\AttachmentUploaderType;
use Irmag\BlogBundle\Entity\BlogPostAttachment;
use Irmag\BlogBundle\Entity\BlogPost;

class BlogPostType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('blog_section', BlogSectionType::class)
            ->add('post_elements', CollectionType::class, [
                'entry_type' => BlogElementsType::class,
                'entry_options' => [
                    'label' => false,
                ],
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'attr' => [
                    'class' => 'goods-for-review',
                ],
            ])
            ->add('id', HiddenType::class)
            ->add('title', TextType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add('subtitle', TextType::class, [
                'label' => false,
                'required' => true,
            ])
            ->add('text', TextareaType::class, [
                'required' => true,
            ])
            ->add('uploader', AttachmentUploaderType::class)
            ->add('attachments', CollectionType::class, [
                'entry_type' => EntityHiddenType::class,
                'entry_options' => [
                    'label' => false,
                    'required' => false,
                    'class' => BlogPostAttachment::class,
                ],
                'label' => false,
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'attr' => [
                    'class' => 'attachment-items',
                ],
            ])
            ->add('blog_post_tags', TextType::class, [
                'label' => 'Теги:',
                'mapped' => false,
                'required' => true,
                'attr' => [
                    'data-available-tags' => json_encode(array_values($options['tags'])),
                ],
            ])
            ->add('save_draft', ButtonType::class, [
                'label' => 'Сохранить черновик',
            ])
            ->add('save_post', SubmitType::class, [
                'label' => 'Отправить на проверку',
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => BlogPost::class,
            'translation_domain' => false,
        ]);

        $resolver->setRequired('tags');
        $resolver->setAllowedTypes('tags', 'array');
    }
}
