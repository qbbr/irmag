<?php

namespace Irmag\BlogBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Irmag\BlogBundle\Entity\BlogPost;

class BlogSitemapSubscriber implements EventSubscriberInterface
{
    /**
     * Сколько всего постов грузить в sitemap.
     */
    const TOTAL_POSTS_AT_SITEMAP = 50;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param UrlGeneratorInterface  $urlGenerator
     * @param EntityManagerInterface $em
     */
    public function __construct(
        UrlGeneratorInterface $urlGenerator,
        EntityManagerInterface $em
    ) {
        $this->urlGenerator = $urlGenerator;
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            SitemapPopulateEvent::ON_SITEMAP_POPULATE => 'registerPages',
        ];
    }

    /**
     * @param SitemapPopulateEvent $event
     */
    public function registerPages(SitemapPopulateEvent $event): void
    {
        if (!empty($event->getSection()) && 'blog' === $event->getSection()) {
            $event->getUrlContainer()->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'irmag_blog_homepage',
                        [],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    new \DateTime(),
                    UrlConcrete::CHANGEFREQ_DAILY,
                    1
                ),
                'blog'
            );

            $event->getUrlContainer()->addUrl(
                new UrlConcrete(
                    $this->urlGenerator->generate(
                        'irmag_blog_items',
                        [],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ),
                    new \DateTime(),
                    UrlConcrete::CHANGEFREQ_DAILY,
                    1
                ),
                'blog'
            );

            $posts = $this->em->getRepository(BlogPost::class)
                ->createQueryBuilder('bp')
                ->where('bp.isActive = true')
                ->andWhere('bp.status = :status')
                ->setParameter('status', BlogPost::BLOG_POST_STATUS_PUBLISHED)
                ->addOrderBy('bp.createdAt', 'DESC')
                ->setMaxResults(self::TOTAL_POSTS_AT_SITEMAP)
                ->getQuery()
                ->getResult();

            foreach ($posts as $post) {
                $event->getUrlContainer()->addUrl(
                    new UrlConcrete(
                        $this->urlGenerator->generate(
                            'irmag_blog_category_post',
                            [
                                'categoryslug' => $post->getBlogSection()->getShortlink(),
                                'id' => $post->getId(),
                            ],
                            UrlGeneratorInterface::ABSOLUTE_URL
                        ),
                        new \DateTime()
                    ),
                    'blog'
                );
            }
        }
    }
}
