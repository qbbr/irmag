<?php

namespace Irmag\BlogBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Irmag\BlogBundle\Entity\BlogPost;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Irmag\ProfileBundle\Service\UserNotificationEventManager\UserNotificationEventManager;
use Irmag\ProfileBundle\Service\UserNotificationQueueManager\UserNotificationQueueManager;
use Irmag\ProfileBundle\NotificationEvents;

class NotificationBlogPostSubscriber implements EventSubscriber
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var UserNotificationEventManager
     */
    private $userNotificationEventManager;

    /**
     * @var UserNotificationQueueManager
     */
    private $userNotificationQueueManager;

    /**
     * @param EntityManagerInterface       $em
     * @param UrlGeneratorInterface        $router
     * @param UserNotificationEventManager $userNotificationEventManager
     * @param UserNotificationQueueManager $userNotificationQueueManager
     */
    public function __construct(
        EntityManagerInterface $em,
        UrlGeneratorInterface $router,
        UserNotificationEventManager $userNotificationEventManager,
        UserNotificationQueueManager $userNotificationQueueManager
    ) {
        $this->em = $em;
        $this->router = $router;
        $this->userNotificationEventManager = $userNotificationEventManager;
        $this->userNotificationQueueManager = $userNotificationQueueManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof BlogPost) {
            $uow = $this->em->getUnitOfWork();
            $changes = $uow->getEntityChangeSet($entity);

            if (isset($changes['status'])) {
                $newStatus = $changes['status'][1];

                if (BlogPost::BLOG_POST_STATUS_PUBLISHED === $newStatus) {
                    $event = $this->userNotificationEventManager->getEventByName(NotificationEvents::BLOG_POST_NEW);
                    $url = $this->router->generate('irmag_blog_category_post', ['id' => $entity->getId(), 'categoryslug' => $entity->getBlogSection()->getShortlink()], UrlGeneratorInterface::ABSOLUTE_URL);
                    $this->userNotificationQueueManager->create($event, null, $entity->getTitle(), $url, true, true);
                }
            }
        }
    }
}
