<?php

namespace Irmag\BlogBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Irmag\SiteBundle\Config;
use Irmag\BlogBundle\Entity\BlogPost;

class IrmagBlogGarbageCleanCommand extends Command
{
    /**
     * Сколько месяцев назад от текущей даты учитываем.
     */
    const DEFAULT_MONTHS = 6;

    protected static $defaultName = 'irmag:blog:clean';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Clean abandoned & unused post drafts')
            ->addOption('months', null, InputOption::VALUE_OPTIONAL, 'How many months ago will start', self::DEFAULT_MONTHS)
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'This will simulate cleaning and show you what would happen')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $startedAt = new \DateTime();
        $output->writeln(sprintf('<info>Started at %s</info>', $startedAt->format(Config::DATETIME_FORMAT)));

        $emptyPosts = $this->em->getRepository(BlogPost::class)->getEmptyPosts();
        $output->write('<comment>Cleaning empty posts ... </comment>');
        $progressBar = new ProgressBar($output, \count($emptyPosts));

        foreach ($emptyPosts as $post) {
            $this->remove($input, $post);
            $this->step($progressBar, $output);
        }

        $this->stop($progressBar, $output);
        $output->writeln(sprintf('<info>OK (%d posts were cleaned)</info>', \count($emptyPosts)));

        if ($input->getOption('months')) {
            $cutOffDatetime = $startedAt->modify(sprintf('- %d month', $input->getOption('months')));
            $abandonedDrafts = $this->em->getRepository(BlogPost::class)->getAbandonedDrafts($cutOffDatetime);

            $output->write('<comment>Cleaning abandoned drafts ... </comment>');
            $progressBar = new ProgressBar($output, \count($emptyPosts));

            foreach ($abandonedDrafts as $draft) {
                $this->remove($input, $draft);
                $this->step($progressBar, $output);
            }

            $this->stop($progressBar, $output);
            $output->writeln(sprintf('<info>OK (%d posts were cleaned)</info>', \count($abandonedDrafts)));
        }

        $finishedAt = new \DateTime();
        $output->writeln(sprintf('<info>Finished at %s</info>', $finishedAt->format(Config::DATETIME_FORMAT)));
        $output->writeln('');
    }

    /**
     * @param InputInterface $input
     * @param BlogPost       $post
     */
    private function remove(InputInterface $input, BlogPost $post): void
    {
        if (!$input->getOption('dry-run')) {
            $this->em->remove($post);
            $this->em->flush();
        }
    }

    /**
     * @param ProgressBar     $progress
     * @param OutputInterface $output
     */
    private function step(ProgressBar $progress, OutputInterface $output): void
    {
        if ($output->isVerbose()) {
            $progress->advance();
        }
    }

    /**
     * @param ProgressBar     $progress
     * @param OutputInterface $output
     */
    private function stop(ProgressBar $progress, OutputInterface $output): void
    {
        if ($output->isVerbose()) {
            $progress->finish();
        }
    }
}
