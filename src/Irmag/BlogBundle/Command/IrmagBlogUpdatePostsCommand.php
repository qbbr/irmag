<?php

namespace Irmag\BlogBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\SiteBundle\Config;
use Irmag\BlogBundle\Entity\BlogPost;

class IrmagBlogUpdatePostsCommand extends Command
{
    protected static $defaultName = 'irmag:blog:update_posts';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Update posts HTML views & previews')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $startedAt = new \DateTime();
        $output->writeln(sprintf('<info>Started at %s</info>', $startedAt->format(Config::DATETIME_FORMAT)));
        $posts = $this->em->getRepository(BlogPost::class)->findAll();
        $output->writeln(sprintf('<comment>Total found posts: %d</comment>', \count($posts)));

        if ($output->isVerbose()) {
            $progress = new ProgressBar($output, \count($posts));
            $progress->start();
        }

        /* Обновляем частично, остальное сделает EventSubscriber */
        foreach ($posts as $post) {
            if ($output->isVerbose()) {
                $progress->advance();
            }

            $now = new \DateTime();

            try {
                $post->setTextHtml(null);
                $post->setPreviewTextHtml(null);
                $post->setUpdatedAt($now);
                $this->em->persist($post);
                $this->em->flush();
            } catch (\Exception $ex) {
                $output->writeln(sprintf('An error occured during persist BlogPost, id = %d', $post->getId()));
                throw new IrmagException($ex->getMessage(), $ex->getCode());
            }
        }

        if ($output->isVerbose()) {
            $progress->finish();
        }

        $finishedAt = new \DateTime();
        $output->writeln(sprintf('<info>Finished at %s</info>', $finishedAt->format(Config::DATETIME_FORMAT)));
        $output->writeln('');
    }
}
