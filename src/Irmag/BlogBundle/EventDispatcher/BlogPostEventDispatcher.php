<?php

namespace Irmag\BlogBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Irmag\BlogBundle\IrmagBlogEvents;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Event\BlogPostEvent;

class BlogPostEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param BlogPost $post
     */
    public function dispatchNewBlogPostCreated(BlogPost $post): void
    {
        $this->eventDispatcher->dispatch(IrmagBlogEvents::BLOG_NEW_POST_CREATED, $this->getBlogPostEvent($post));
    }

    /**
     * @param BlogPost $post
     */
    public function dispatchBlogPostStatusChanged(BlogPost $post): void
    {
        $this->eventDispatcher->dispatch(IrmagBlogEvents::BLOG_POST_STATUS_CHANGED, $this->getBlogPostEvent($post));
    }

    /**
     * @param BlogPost $post
     */
    public function dispatchBlogPostGetReward(BlogPost $post): void
    {
        $this->eventDispatcher->dispatch(IrmagBlogEvents::BLOG_POST_GIVE_REWARD_TO_USER, $this->getBlogPostEvent($post));
    }

    /**
     * @param BlogPost $post
     *
     * @return BlogPostEvent
     */
    private function getBlogPostEvent(BlogPost $post): BlogPostEvent
    {
        return new BlogPostEvent($post);
    }
}
