<?php

namespace Irmag\BlogBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Irmag\BlogBundle\Event\BlogFeedbackMessageEvent;
use Irmag\BlogBundle\Entity\BlogFeedbackMessage;
use Irmag\BlogBundle\IrmagBlogEvents;

class BlogFeedbackEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param BlogFeedbackMessage $message
     */
    public function dispatchNewFeedbackMessageCreated(BlogFeedbackMessage $message): void
    {
        $this->eventDispatcher->dispatch(IrmagBlogEvents::BLOG_NEW_FEEDBACK_MESSAGE_CREATED, $this->getBlogFeedbackMessageEvent($message));
    }

    /**
     * @param BlogFeedbackMessage $message
     *
     * @return BlogFeedbackMessageEvent
     */
    private function getBlogFeedbackMessageEvent(BlogFeedbackMessage $message): BlogFeedbackMessageEvent
    {
        return new BlogFeedbackMessageEvent($message);
    }
}
