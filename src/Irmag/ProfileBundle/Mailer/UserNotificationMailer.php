<?php

namespace Irmag\ProfileBundle\Mailer;

use Irmag\CoreBundle\Mailer\Mailer;

class UserNotificationMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(
        Mailer $mailer
    ) {
        $this->mailer = $mailer;
    }

    public function sendMessage($to, $context): int
    {
        return $this->mailer->sendMessage($to, '@IrmagProfile/Emails/user-notification-email.html.twig', $context);
    }
}
