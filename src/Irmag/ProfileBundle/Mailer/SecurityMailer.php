<?php

namespace Irmag\ProfileBundle\Mailer;

use Irmag\CoreBundle\Mailer\Mailer;

class SecurityMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(
        Mailer $mailer
    ) {
        $this->mailer = $mailer;
    }

    public function sendRegistrationMessage($to, $context)
    {
        return $this->mailer->sendMessage($to, '@IrmagProfile/Emails/registration.html.twig', $context);
    }

    public function sendConfirmationRestorePasswordMessage($to, $context)
    {
        return $this->mailer->sendMessage($to, '@IrmagProfile/Emails/confirmation-restore-password.html.twig', $context);
    }

    public function sendConfirmationEmailMessage($to, $context)
    {
        return $this->mailer->sendMessage($to, '@IrmagProfile/Emails/confirmation-email.html.twig', $context);
    }
}
