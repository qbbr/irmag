<?php

namespace Irmag\ProfileBundle;

final class NotificationEvents
{
    public const FAVORITE_ELEMENT_IS_ACTIVE = 'favorite_element_is_active';
    public const FAVORITE_ELEMENT_PRICE = 'favorite_element_price';
    public const ACTION_NEW = 'action_new';
    public const ORDER_STATUS = 'order_status';
    public const BONUS_SEND = 'bonus_send';
    public const NEWS_NEW = 'news_new';
    public const FORUM_POST_REPLY = 'forum_post_reply';
    public const BLOG_COMMENT_REPLY = 'blog_comment_reply';
    public const SITE_COMMENT_REPLY = 'site_comment_reply';
    public const BLOG_POST_NEW = 'blog_post_new';
    public const NEWSLETTER = 'newsletter';
}
