<?php

namespace Irmag\ProfileBundle\Form\Helper;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Irmag\CoreBundle\Utils\PropertyFiller;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserNotificationEvent;
use Irmag\ProfileBundle\Entity\UserNotificationSubscription;
use Irmag\ProfileBundle\Service\UserNotificationEventManager\UserNotificationEventManager;
use Irmag\ProfileBundle\Service\UserNotificationSubscriptionManager\UserNotificationSubscriptionManager;

class ProfileEventSubscriptionHelper
{
    public const IS_EMAIL_SUBSCRIBED = 'isEmailSubscribed';
    public const IS_SITE_SUBSCRIBED = 'isSiteSubscribed';
    public const IS_SMS_SUBSCRIBED = 'isSmsSubscribed';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Security
     */
    private $security;

    /**
     * @var PropertyFiller
     */
    private $propertyFiller;

    /**
     * @var UserNotificationEventManager
     */
    private $userNotificationEventManager;

    /**
     * @var UserNotificationSubscriptionManager
     */
    private $userNotificationSubscriptionManager;

    /**
     * @param EntityManagerInterface              $em
     * @param Security                            $security
     * @param PropertyFiller                      $propertyFiller
     * @param UserNotificationEventManager        $userNotificationEventManager
     * @param UserNotificationSubscriptionManager $userNotificationSubscriptionManager
     */
    public function __construct(
        EntityManagerInterface $em,
        Security $security,
        PropertyFiller $propertyFiller,
        UserNotificationEventManager $userNotificationEventManager,
        UserNotificationSubscriptionManager $userNotificationSubscriptionManager
    ) {
        $this->em = $em;
        $this->security = $security;
        $this->propertyFiller = $propertyFiller;
        $this->userNotificationEventManager = $userNotificationEventManager;
        $this->userNotificationSubscriptionManager = $userNotificationSubscriptionManager;
    }

    /**
     * @return UserNotificationEvent[]
     */
    public function getNotificationEvents(): array
    {
        return $this->em->getRepository(UserNotificationEvent::class)->findBy(['isActive' => true], ['description' => 'asc']);
    }

    public function getUserSubscriptionsData(): array
    {
        $subscriptions = $this->getNotificationSubscriptions();
        $subscriptionsData = [];

        /** @var UserNotificationSubscription $subscription */
        foreach ($subscriptions as $subscription) {
            $event = $subscription->getEvent();
            $data = [];

            if (true === $subscription->getIsPushEnabled() && false === $event->getIsPushDisabled()) {
                $data[] = UserNotificationSubscription::IS_PUSH_ENABLED;
            }

            if (true === $subscription->getIsEmailEnabled() && false === $event->getIsEmailDisabled()) {
                $data[] = UserNotificationSubscription::IS_EMAIL_ENABLED;
            }

            if (true === $subscription->getIsSiteEnabled() && false === $event->getIsSiteDisabled()) {
                $data[] = UserNotificationSubscription::IS_SITE_ENABLED;
            }

            if (true === $subscription->getIsSmsEnabled() && false === $event->getIsSmsDisabled()) {
                $data[] = UserNotificationSubscription::IS_SMS_ENABLED;
            }

            $eventName = $subscription->getEvent()->getName();
            $subscriptionsData[$eventName] = $data;
        }

        return $subscriptionsData;
    }

    /**
     * @param string $name Event name
     * @param array  $data Data from form
     */
    public function upsert(string $name, array $data, $globalSubscribedFlags): void
    {
        $notificationEvent = $this->userNotificationEventManager->getEventByName($name);
        $notificationSubscription = $this->userNotificationSubscriptionManager->getUserSubscription($notificationEvent, $this->getUser());

        // delete
        if (empty($data)) {
            if (null === $notificationSubscription) {
                return;
            }

            $this->em->remove($notificationSubscription);

            return;
        }

        if (null === $notificationSubscription) {
            $notificationSubscription = new UserNotificationSubscription();
            $notificationSubscription->setUser($this->getUser());
            $notificationSubscription->setEvent($notificationEvent);
        }

        $this->propertyFiller->set($notificationSubscription, $this->prepareData($data, $globalSubscribedFlags));

        $this->em->persist($notificationSubscription);
    }

    public function flush(): void
    {
        $this->em->flush();
    }

    private function getNotificationSubscriptions(): array
    {
        return $this->em->getRepository(UserNotificationSubscription::class)->findBy(['user' => $this->getUser()]);
    }

    private function prepareData(array $data, array $globalSubscribedFlags): array
    {
        /** @var User $user */
        $user = $this->getUser();
        $isPushSubscribed = !$user->getPushIds()->isEmpty();
        $isEmailSubscribed = $globalSubscribedFlags[self::IS_EMAIL_SUBSCRIBED] ?? $user->getIsEmailSubscribed();
        $isSiteSubscribed = $globalSubscribedFlags[self::IS_SITE_SUBSCRIBED] ?? $user->getIsSiteSubscribed();
        $isSmsSubscribed = $globalSubscribedFlags[self::IS_SMS_SUBSCRIBED] ?? $user->getIsSmsSubscribed();
        $preparedData = $this->getDefaultData();

        foreach ($data as $flag) {
            $allow = false;

            if (UserNotificationSubscription::IS_PUSH_ENABLED === $flag && true === $isPushSubscribed) {
                $allow = true;
            } elseif (UserNotificationSubscription::IS_EMAIL_ENABLED === $flag && true === $isEmailSubscribed) {
                $allow = true;
            } elseif (UserNotificationSubscription::IS_SITE_ENABLED === $flag && true === $isSiteSubscribed) {
                $allow = true;
            } elseif (UserNotificationSubscription::IS_SMS_ENABLED === $flag && true === $isSmsSubscribed) {
                $allow = true;
            }

            if (true === $allow) {
                $preparedData[$flag] = true;
            }
        }

        return $preparedData;
    }

    private function getDefaultData(): array
    {
        return [
            UserNotificationSubscription::IS_PUSH_ENABLED => false,
            UserNotificationSubscription::IS_EMAIL_ENABLED => false,
            UserNotificationSubscription::IS_SITE_ENABLED => false,
            UserNotificationSubscription::IS_SMS_ENABLED => false,
        ];
    }

    /**
     * @return User|UserInterface
     */
    private function getUser(): User
    {
        return $this->security->getUser();
    }
}
