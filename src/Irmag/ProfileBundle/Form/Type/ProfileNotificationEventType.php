<?php

namespace Irmag\ProfileBundle\Form\Type;

use Irmag\ProfileBundle\Entity\UserNotificationEvent;
use Irmag\ProfileBundle\Entity\UserNotificationSubscription;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\ProfileBundle\Form\Helper\ProfileEventSubscriptionHelper;

class ProfileNotificationEventType extends AbstractType
{
    /**
     * @var ProfileEventSubscriptionHelper
     */
    private $notificationEventSubscriptionHelper;

    /**
     * @param ProfileEventSubscriptionHelper $notificationEventSubscriptionHelper
     */
    public function __construct(
        ProfileEventSubscriptionHelper $notificationEventSubscriptionHelper
    ) {
        $this->notificationEventSubscriptionHelper = $notificationEventSubscriptionHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $events = $this->notificationEventSubscriptionHelper->getNotificationEvents();
        $subscriptionsData = $this->notificationEventSubscriptionHelper->getUserSubscriptionsData();

        foreach ($events as $event) {
            $eventName = $event->getName();

            $builder->add($event->getName(), ChoiceType::class, [
                'choices' => [
                    'Push' => UserNotificationSubscription::IS_PUSH_ENABLED,
                    'Email' => UserNotificationSubscription::IS_EMAIL_ENABLED,
                    'Сайт' => UserNotificationSubscription::IS_SITE_ENABLED,
                    'SMS' => UserNotificationSubscription::IS_SMS_ENABLED,
                ],
                'choice_attr' => function ($choiceValue, $key, $value) use ($event) {
                    $disabled = $this->isDisabled($choiceValue, $event);

                    return true === $disabled ? ['disabled' => 'disabled'] : [];
                },
                'data' => $subscriptionsData[$eventName] ?? [],
                'attr' => [
                    'description' => $event->getDescription(),
                ],
                'multiple' => true,
                'expanded' => true,
            ]);
        }

        $builder->addEventListener(FormEvents::SUBMIT, function (FormEvent $event) {
            $isEmailSubscribed = $event->getForm()->getParent()->get(ProfileEventSubscriptionHelper::IS_EMAIL_SUBSCRIBED)->getData();
            $isSiteSubscribed = $event->getForm()->getParent()->get(ProfileEventSubscriptionHelper::IS_SITE_SUBSCRIBED)->getData();
            $isSmsSubscribed = $event->getForm()->getParent()->get(ProfileEventSubscriptionHelper::IS_SMS_SUBSCRIBED)->getData();

            foreach ($event->getData() as $name => $data) {
                $this->notificationEventSubscriptionHelper->upsert($name, $data, [
                    ProfileEventSubscriptionHelper::IS_EMAIL_SUBSCRIBED => $isEmailSubscribed,
                    ProfileEventSubscriptionHelper::IS_SITE_SUBSCRIBED => $isSiteSubscribed,
                    ProfileEventSubscriptionHelper::IS_SMS_SUBSCRIBED => $isSmsSubscribed,
                ]);
            }

            $this->notificationEventSubscriptionHelper->flush();
        });
    }

    private function isDisabled($choiceValue, UserNotificationEvent $event): bool
    {
        $disabled = false;

        if (UserNotificationSubscription::IS_PUSH_ENABLED === $choiceValue && true === $event->getIsPushDisabled()) {
            $disabled = true;
        } elseif (UserNotificationSubscription::IS_EMAIL_ENABLED === $choiceValue && true === $event->getIsEmailDisabled()) {
            $disabled = true;
        } elseif (UserNotificationSubscription::IS_SITE_ENABLED === $choiceValue && true === $event->getIsSiteDisabled()) {
            $disabled = true;
        } elseif (UserNotificationSubscription::IS_SMS_ENABLED === $choiceValue && true === $event->getIsSmsDisabled()) {
            $disabled = true;
        }

        return $disabled;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'mapped' => false,
            'required' => false,
            'translation_domain' => false,
            'label' => false,
        ]);
    }
}
