<?php

namespace Irmag\ProfileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Irmag\ProfileBundle\Form\Model\ProfileChangeLogin;

class ProfileChangeLoginType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('newUsername', null, [
                'label' => 'Новый логин',
                'required' => true,
            ])
            ->add('currentPassword', PasswordType::class, [
                'label' => 'Текущий пароль',
                'required' => true,
            ])
            ->add('save', SubmitType::class, ['label' => 'Изменить логин'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProfileChangeLogin::class,
            'translation_domain' => false,
        ]);
    }
}
