<?php

namespace Irmag\ProfileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\SiteBundle\Form\Type\PayerLegalType;
use Irmag\SiteBundle\Form\Type\DeliveryAddressType;
use Irmag\ProfileBundle\Entity\UserOrderProfile;

class ProfileOrderProfileType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var UserOrderProfile $data */
        $data = $builder->getData();
        $deliveryCity = $data->getDeliveryCity();

        $builder
            ->add('name', null, ['label' => 'Название'])
            ->add('payerUsername', null, ['label' => 'ФИО'])
            ->add('payerTelephone', null, ['label' => 'Телефон'])
            // Адрес доставки
            ->add('deliveryAddress', DeliveryAddressType::class, ['delivery_city' => $deliveryCity])
            // Юридическое лицо
            ->add('payerLegal', PayerLegalType::class, ['required' => false])
        ;

        // $builder->get('deliveryAddress')->get('deliveryDistrict')->setDisabled(true);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => UserOrderProfile::class,
        ]);
    }
}
