<?php

namespace Irmag\ProfileBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class GenderType extends AbstractType
{
    const MALE = 'm';
    const FEMALE = 'f';

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                'profile.gender.male' => self::MALE,
                'profile.gender.female' => self::FEMALE,
            ],
            'required' => false,
            'placeholder' => 'placeholder.select',
            'label' => 'profile.label.gender',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
