<?php

namespace Irmag\ProfileBundle\Form\Model;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;

/**
 * Для подтверждение текущего пароля.
 */
class ProfileCurrentPassword
{
    /**
     * @SecurityAssert\UserPassword(
     *     message="Не правильный пароль"
     * )
     */
    protected $currentPassword;

    public function getCurrentPassword()
    {
        return $this->currentPassword;
    }

    public function setCurrentPassword($currentPassword)
    {
        $this->currentPassword = $currentPassword;
    }
}
