<?php

namespace Irmag\ProfileBundle\Form\Model;

use Symfony\Component\Validator\Constraints as Assert;

class ProfileChangeEmail extends ProfileCurrentPassword
{
    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Email(checkHost=true)
     */
    protected $newEmail;

    /**
     * @return string
     */
    public function getNewEmail()
    {
        return $this->newEmail;
    }

    /**
     * @param string $newEmail
     */
    public function setNewEmail($newEmail)
    {
        $this->newEmail = $newEmail;
    }
}
