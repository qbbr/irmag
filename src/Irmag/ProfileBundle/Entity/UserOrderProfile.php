<?php

namespace Irmag\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\Traits\PayerLegalTrait;
use Irmag\SiteBundle\Entity\Traits\DeliveryAddressTrait;

/**
 * @ORM\Table(name="users_order_profiles")
 * @ORM\Entity(repositoryClass="Irmag\ProfileBundle\Repository\UserOrderProfileRepository")
 *
 * @Serializer\ExclusionPolicy("none")
 */
class UserOrderProfile
{
    use PayerLegalTrait;
    use DeliveryAddressTrait;
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * ФИО плательщика.
     *
     * @var string
     *
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $payerUsername;

    /**
     * Телефон.
     *
     * @var string
     *
     * @Assert\Length(max=32)
     * @Assert\Regex(
     *     pattern="/^[0-9\+\-\(\)\s]+$/",
     *     htmlPattern="^[0-9\+\-\(\)\s]+$"
     * )
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $payerTelephone;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isPrimary;

    public function __construct()
    {
        $this->isPrimary = false;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return sprintf(
            '%s (%s) %s',
            $this->name,
            $this->getDeliveryFullAddress(),
            ($this->payerLegalAddress) ? '[Юр. данные]' : ''
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPayerUsername(): ?string
    {
        return $this->payerUsername;
    }

    public function setPayerUsername(?string $payerUsername): self
    {
        $this->payerUsername = $payerUsername;

        return $this;
    }

    public function getPayerTelephone(): ?string
    {
        return $this->payerTelephone;
    }

    public function setPayerTelephone(?string $payerTelephone): self
    {
        $this->payerTelephone = $payerTelephone;

        return $this;
    }

    public function getIsPrimary(): ?bool
    {
        return $this->isPrimary;
    }

    public function setIsPrimary(bool $isPrimary): self
    {
        $this->isPrimary = $isPrimary;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
