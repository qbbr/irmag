<?php

namespace Irmag\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\ProfileBundle\Entity\Traits\NotificationDataTrait;

/**
 * @ORM\Table(name="users_notifications")
 * @ORM\Entity
 */
class UserNotification
{
    use TimestampableEntity;
    use NotificationDataTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isAsRead;

    public function __construct()
    {
        $this->isAsRead = false;
        $this->calculateExpiredAt();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsAsRead(): ?bool
    {
        return $this->isAsRead;
    }

    public function setIsAsRead(bool $isAsRead): self
    {
        $this->isAsRead = $isAsRead;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
