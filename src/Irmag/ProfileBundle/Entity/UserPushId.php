<?php

namespace Irmag\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="users_push_ids")
 * @ORM\Entity
 */
class UserPushId
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="pushIds")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Uuid
     *
     * @ORM\Column(type="guid")
     */
    private $uuid;

    /**
     * @var string
     *
     * @Assert\Length(max="64")
     *
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $device;

    /**
     * @var string
     *
     * @Assert\Length(max="15")
     *
     * @ORM\Column(type="string", length=15, nullable=true)
     */
    private $ip;

    /**
     * @var string
     *
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ua;

    public function __toString()
    {
        return $this->user->__toString();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUuid(): ?string
    {
        return $this->uuid;
    }

    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;

        return $this;
    }

    public function getDevice(): ?string
    {
        return $this->device;
    }

    public function setDevice(?string $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(?string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getUa(): ?string
    {
        return $this->ua;
    }

    public function setUa(?string $ua): self
    {
        $this->ua = $ua;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
