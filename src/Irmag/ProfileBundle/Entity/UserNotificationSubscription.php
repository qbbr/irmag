<?php

namespace Irmag\ProfileBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="users_notification_subscriptions")
 * @ORM\Entity
 */
class UserNotificationSubscription
{
    public const IS_PUSH_ENABLED = 'isPushEnabled';
    public const IS_EMAIL_ENABLED = 'isEmailEnabled';
    public const IS_SITE_ENABLED = 'isSiteEnabled';
    public const IS_SMS_ENABLED = 'isSmsEnabled';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $user;

    /**
     * @var UserNotificationEvent
     *
     * @ORM\ManyToOne(targetEntity="UserNotificationEvent")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $event;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isPushEnabled;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isEmailEnabled;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isSiteEnabled;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isSmsEnabled;

    public function __construct()
    {
        $this->isPushEnabled = false;
        $this->isEmailEnabled = false;
        $this->isSiteEnabled = false;
        $this->isSmsEnabled = false;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        if ($this->event) {
            return (string) $this->id.' - '.$this->event->getName();
        }

        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsPushEnabled(): ?bool
    {
        return $this->isPushEnabled;
    }

    public function setIsPushEnabled(bool $isPushEnabled): self
    {
        $this->isPushEnabled = $isPushEnabled;

        return $this;
    }

    public function getIsEmailEnabled(): ?bool
    {
        return $this->isEmailEnabled;
    }

    public function setIsEmailEnabled(bool $isEmailEnabled): self
    {
        $this->isEmailEnabled = $isEmailEnabled;

        return $this;
    }

    public function getIsSiteEnabled(): ?bool
    {
        return $this->isSiteEnabled;
    }

    public function setIsSiteEnabled(bool $isSiteEnabled): self
    {
        $this->isSiteEnabled = $isSiteEnabled;

        return $this;
    }

    public function getIsSmsEnabled(): ?bool
    {
        return $this->isSmsEnabled;
    }

    public function setIsSmsEnabled(bool $isSmsEnabled): self
    {
        $this->isSmsEnabled = $isSmsEnabled;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getEvent(): ?UserNotificationEvent
    {
        return $this->event;
    }

    public function setEvent(?UserNotificationEvent $event): self
    {
        $this->event = $event;

        return $this;
    }
}
