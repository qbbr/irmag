<?php

namespace Irmag\ProfileBundle;

final class Events
{
    /**
     * Это событие вызывается при подтверждении email после регистрации пользователя.
     *
     * @var string
     */
    const USER_REGISTRATION_EMAIL_CONFIRMED = 'irmag.user.registration_email_confirmed';
}
