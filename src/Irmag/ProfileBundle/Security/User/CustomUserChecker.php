<?php

namespace Irmag\ProfileBundle\Security\User;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserChecker;
use Irmag\ProfileBundle\Entity\UserLoginAttempts;

class CustomUserChecker extends UserChecker
{
    const BAN_TIME_IN_MINUTES = 10;
    const FAILTURE_ATTEMPTS = 5;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * {@inheritdoc}
     */
    public function checkPreAuth(UserInterface $user)
    {
        $username = mb_strtolower($user->getUsername());
        $userLoginAttempts = $this->em->getRepository(UserLoginAttempts::class)->findOneBy(['username' => $username]);

        if ($userLoginAttempts && $userLoginAttempts->getAttempts() > self::FAILTURE_ATTEMPTS - 1) {
            if ((new \DateTime()) < $userLoginAttempts->getUpdatedAt()->modify(sprintf('+%d minutes', self::BAN_TIME_IN_MINUTES))) {
                throw new CustomUserMessageAuthenticationException(
                    sprintf(
                        'Учетная запись заблокирована на %d минут. Неудачных попыток входа: %d.',
                        self::BAN_TIME_IN_MINUTES,
                        self::FAILTURE_ATTEMPTS
                    )
                );
            }
        }

        parent::checkPreAuth($user);
    }
}
