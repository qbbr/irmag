<?php

namespace Irmag\ProfileBundle\Security;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchy;
use Symfony\Component\Security\Core\User\UserInterface;

class DynamicRoleHierarchy extends RoleHierarchy
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param array                  $hierarchy
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     */
    public function __construct(
        array $hierarchy,
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        parent::__construct($this->buildRolesTree());
    }

    /**
     * @return array
     */
    private function buildRolesTree(): array
    {
        $hierarchy = [];
        $token = $this->tokenStorage->getToken();

        if (null === $token || !$token->getUser() instanceof UserInterface) {
            return $hierarchy;
        }

        $roles = $this->em->createQuery('select r, c from IrmagProfileBundle:Role r JOIN r.childrens c')->execute();

        foreach ($roles as $role) {
            /** @var $role \Irmag\ProfileBundle\Entity\Role */
            if (\count($role->getChildrens()) > 0) {
                $roleChildren = [];

                foreach ($role->getChildrens() as $child) {
                    /* @var $child \Irmag\ProfileBundle\Entity\Role */
                    $roleChildren[] = $child->getRole();
                }

                $hierarchy[$role->getRole()] = $roleChildren;
            }
        }

        return $hierarchy;
    }
}
