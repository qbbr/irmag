<?php

namespace Irmag\ProfileBundle\Service\UserNotificationSubscriptionManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserNotificationEvent;
use Irmag\ProfileBundle\Entity\UserNotificationSubscription;

class UserNotificationSubscriptionManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @param EntityManagerInterface $em
     * @param UrlGeneratorInterface  $router
     */
    public function __construct(
        EntityManagerInterface $em,
        UrlGeneratorInterface $router
    ) {
        $this->em = $em;
        $this->router = $router;
    }

    /**
     * @param UserNotificationEvent $event
     *
     * @return UserNotificationSubscription[]
     */
    public function getSubscribtionsByEvent(UserNotificationEvent $event)
    {
        return $this->em->getRepository(UserNotificationSubscription::class)->findBy(['event' => $event]);
    }

    /**
     * @param UserNotificationEvent $event
     * @param User                  $user
     *
     * @return UserNotificationSubscription|null
     */
    public function getUserSubscription(UserNotificationEvent $event, User $user): ?UserNotificationSubscription
    {
        return $this->em->getRepository(UserNotificationSubscription::class)->findOneBy(['user' => $user, 'event' => $event]);
    }

    /**
     * @return string
     */
    public function getDefaultUrl(): string
    {
        return $this->router->generate('irmag_profile_notifications', [], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * @param UserNotificationEvent $event
     * @param User                  $user
     * @param bool|null             $push
     * @param bool|null             $email
     * @param bool|null             $site
     * @param bool|null             $sms
     */
    public function subscribe(UserNotificationEvent $event, User $user, ?bool $push = null, ?bool $email = null, ?bool $site = null, ?bool $sms = null): void
    {
        $subscription = $this->getUserSubscription($event, $user);

        if (null === $subscription) {
            $subscription = new UserNotificationSubscription();
            $subscription->setUser($user);
            $subscription->setEvent($event);
        }

        if (null !== $push) {
            $subscription->setIsPushEnabled($push);
        }

        if (null !== $email) {
            $subscription->setIsEmailEnabled($email);
        }

        if (null !== $site) {
            $subscription->setIsSiteEnabled($site);
        }

        if (null !== $sms) {
            $subscription->setIsSmsEnabled($sms);
        }

        $this->em->persist($subscription);
        $this->em->flush();
    }
}
