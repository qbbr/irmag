<?php

namespace Irmag\ProfileBundle\Service\UserNotificationQueueManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserNotificationEvent;
use Irmag\ProfileBundle\Entity\UserNotificationQueue;
use Irmag\ProfileBundle\Service\UserNotificationSubscriptionManager\UserNotificationSubscriptionManager;

class UserNotificationQueueManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var UserNotificationSubscriptionManager
     */
    private $userNotificationSubscriptionManager;

    /**
     * @param EntityManagerInterface              $em
     * @param UrlGeneratorInterface               $router
     * @param UserNotificationSubscriptionManager $notificationSubscriptionManager
     */
    public function __construct(
        EntityManagerInterface $em,
        UrlGeneratorInterface $router,
        UserNotificationSubscriptionManager $notificationSubscriptionManager
    ) {
        $this->em = $em;
        $this->router = $router;
        $this->userNotificationSubscriptionManager = $notificationSubscriptionManager;
    }

    /**
     * @param UserNotificationEvent $event
     * @param User|null             $user
     * @param string                $text
     * @param string|null           $url
     * @param bool                  $uowUsed   Set true if using Unit of Works
     * @param bool                  $uowCommit Set true if using Unit of Works and n commit
     */
    public function create(UserNotificationEvent $event, User $user = null, string $text, string $url = null, bool $uowUsed = false, bool $uowCommit = false): void
    {
        if (false === $event->getIsActive()) {
            return;
        }

        $subscriptions = null === $user
            ? $this->userNotificationSubscriptionManager->getSubscribtionsByEvent($event)
            : [$this->userNotificationSubscriptionManager->getUserSubscription($event, $user)];

        foreach ($subscriptions as $subscription) {
            if (null === $subscription) {
                continue;
            }

            $queue = new UserNotificationQueue();
            $queue->setEvent($event);
            $queue->setUser($subscription->getUser());
            $queue->setTitle($event->getDescription());
            $queue->setBody($text);
            $queue->setUrl($url ?? $this->userNotificationSubscriptionManager->getDefaultUrl());

            $this->em->persist($queue);

            if (true === $uowUsed) {
                $classMetadata = $this->em->getClassMetadata(UserNotificationQueue::class);
                $this->em->getUnitOfWork()->computeChangeSet($classMetadata, $queue);

                if (true === $uowCommit) {
                    $this->em->getUnitOfWork()->commit($queue);
                }
            }
        }

        if (false === $uowUsed) {
            $this->em->flush();
        }
    }
}
