<?php

namespace Irmag\ProfileBundle\Service\UserNotificationManager;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\CoreBundle\Utils\PropertyFiller;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserNotification;
use Irmag\ProfileBundle\Entity\UserNotificationEvent;
use Irmag\ProfileBundle\Entity\UserNotificationQueue;
use Irmag\ProfileBundle\Entity\UserNotificationSubscription;
use Irmag\ProfileBundle\Mailer\UserNotificationMailer;
use Irmag\ProfileBundle\Service\UserNotificationSubscriptionManager\UserNotificationSubscriptionManager;
use Irmag\ProfileBundle\Service\UserPushManager\UserPushManager;
use Irmag\SiteBundle\Service\SmsSender\SmsSender;
use Irmag\SiteBundle\Service\UrlShortener\UrlShortener;

class UserNotificationManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var PropertyFiller
     */
    private $filler;

    /**
     * @var SmsSender
     */
    private $smsSender;

    /**
     * @var UserNotificationMailer
     */
    private $notificationMailer;

    /**
     * @var UserPushManager
     */
    private $pushManager;

    /**
     * @var UserNotificationSubscriptionManager
     */
    private $userNotificationSubscriptionManager;

    /**
     * @var UrlShortener
     */
    private $urlShortener;

    /**
     * @param EntityManagerInterface              $em
     * @param PropertyFiller                      $filler
     * @param SmsSender                           $smsSender
     * @param UserNotificationMailer              $notificationMailer
     * @param UserPushManager                     $pushManager
     * @param UserNotificationSubscriptionManager $userNotificationSubscriptionManager
     * @param UrlShortener                        $urlShortener
     */
    public function __construct(
        EntityManagerInterface $em,
        PropertyFiller $filler,
        SmsSender $smsSender,
        UserNotificationMailer $notificationMailer,
        UserPushManager $pushManager,
        UserNotificationSubscriptionManager $userNotificationSubscriptionManager,
        UrlShortener $urlShortener
    ) {
        $this->em = $em;
        $this->filler = $filler;
        $this->smsSender = $smsSender;
        $this->notificationMailer = $notificationMailer;
        $this->pushManager = $pushManager;
        $this->userNotificationSubscriptionManager = $userNotificationSubscriptionManager;
        $this->urlShortener = $urlShortener;
    }

    public function getAvailableNotificationTypes(User $user, UserNotificationEvent $event): array
    {
        $subscription = $this->userNotificationSubscriptionManager->getUserSubscription($event, $user);

        $types = [];

        if (!$subscription instanceof UserNotificationSubscription) {
            return $types;
        }

        if (
            true === $subscription->getIsPushEnabled()
            &&
            false === $event->getIsPushDisabled()
        ) {
            $types[] = UserNotificationSubscription::IS_PUSH_ENABLED;
        }

        if (
            true === $subscription->getIsEmailEnabled()
            &&
            false === $event->getIsEmailDisabled()
        ) {
            $types[] = UserNotificationSubscription::IS_EMAIL_ENABLED;
        }

        if (
            true === $subscription->getIsSiteEnabled()
            &&
            false === $event->getIsSiteDisabled()
        ) {
            $types[] = UserNotificationSubscription::IS_SITE_ENABLED;
        }

        if (
            true === $subscription->getIsSmsEnabled()
            &&
            false === $event->getIsSmsDisabled()
        ) {
            $types[] = UserNotificationSubscription::IS_SMS_ENABLED;
        }

        return $types;
    }

    public function doPushNotification(UserNotificationQueue $queue, User $user, array $data = []): bool
    {
        return $this->pushManager->send($queue, $user, $data);
    }

    public function doEmailNotification(UserNotificationQueue $queue, User $user): bool
    {
        $email = $user->getEmail();

        if (empty($email) || false === filter_var($email, \FILTER_VALIDATE_EMAIL)) {
            return true;
        }

        $output = $this->notificationMailer->sendMessage($email, ['notification_queue' => $queue, 'user' => $user]);

        if (0 !== $output) {
            return true;
        }

        return false;
    }

    public function doSiteNotification(UserNotificationQueue $queue, User $user): UserNotification
    {
        $notify = new UserNotification();

        $this->filler->fill($queue, $notify, ['title', 'body', 'url', 'createdAt', 'expiredAt']);
        $notify->setUser($user);
        $notify->setEvent($queue->getEvent());

        $this->em->persist($notify);
        $this->em->flush();

        return $notify;
    }

    public function doSmsNotification(UserNotificationQueue $queue, User $user): bool
    {
        $telephone = $this->prepareTelephone($user->getTelephone());

        if (null === $telephone) {
            return true;
        }

        $url = $this->urlShortener->shorten($queue->getUrl());
        $output = $this->smsSender->send($telephone, $queue->getTitle().' '.$url);

        if (is_numeric($output)) {
            return true;
        }

        return false;
    }

    private function prepareTelephone($telephone): ?string
    {
        $telephone = preg_replace('/\D/', '', $telephone);

        if (11 === mb_strlen($telephone)) {
            return $telephone;
        }

        return null;
    }
}
