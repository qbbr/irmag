<?php

namespace Irmag\ProfileBundle\Service\UserStateManager;

interface UserStateManagerInterface
{
    /**
     * Назначает.
     * Если у текущего ключа уже есть значение, то оно будет перезаписано.
     *
     * @param string $key   Ключ
     * @param string $value Значение
     */
    public function set($key, $value);

    /**
     * Получает.
     *
     * @param string $key          Ключ
     * @param string $defaultValue Значение по-умолчанию
     *
     * @return string|null
     */
    public function get($key, $defaultValue = null);

    /**
     * Удаляет.
     *
     * @param string $key Ключ
     */
    public function remove($key);
}
