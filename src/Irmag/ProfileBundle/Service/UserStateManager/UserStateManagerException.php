<?php

namespace Irmag\ProfileBundle\Service\UserStateManager;

use Irmag\CoreBundle\Exception\IrmagException;

class UserStateManagerException extends IrmagException
{
}
