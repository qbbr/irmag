<?php

namespace Irmag\ProfileBundle\Service\UserPushManager;

use Buzz\Browser;
use Buzz\Client\Curl;
use Doctrine\ORM\EntityManagerInterface;
use Irmag\ProfileBundle\Service\UserNotificationSubscriptionManager\UserNotificationSubscriptionManager;
use Irmag\ProfileBundle\Entity\UserNotificationQueue;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserPushId;

class UserPushManager
{
    private const API_URL = 'https://onesignal.com/api/v1/notifications';
    // @see https://documentation.onesignal.com/docs/appearance#section-character-limits
    private const MAX_TITLE_LENGTH = 50;
    private const MAX_BODY_LENGTH = 150;
    //private const CHROME_WEB_BADGE = 'https://i.imgur.com/kXvCkkv.png';
    private const CHROME_WEB_BADGE = 'https://irmag.ru/android-chrome-72x72.png';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $apiKey;

    /**
     * @var string
     */
    private $appId;

    /**
     * @var UserNotificationSubscriptionManager
     */
    private $userNotificationSubscriptionManager;

    /**
     * @param EntityManagerInterface              $em
     * @param UserNotificationSubscriptionManager $userNotificationSubscriptionManager
     * @param string                              $apiKey
     * @param string                              $appId
     */
    public function __construct(
        EntityManagerInterface $em,
        UserNotificationSubscriptionManager $userNotificationSubscriptionManager,
        string $apiKey,
        string $appId
    ) {
        $this->em = $em;
        $this->userNotificationSubscriptionManager = $userNotificationSubscriptionManager;
        $this->apiKey = $apiKey;
        $this->appId = $appId;
    }

    /**
     * @param User   $user   Пользователь
     * @param string $uuid   UUID
     * @param string $device Устройство
     * @param string $ip     IP адрес
     * @param string $ua     User agent
     */
    public function register(User $user, string $uuid, string $device, string $ip, string $ua): void
    {
        $userPushId = $this->get($user, $uuid);

        if (null === $userPushId) {
            $userPushId = new UserPushId();
            $userPushId->setUser($user);
            $userPushId->setUuid($uuid);
            $userPushId->setDevice($device);
            $userPushId->setIp($ip);
            $userPushId->setUa($ua);

            $this->em->persist($userPushId); // updated_at
            $this->em->flush();
        }
    }

    /**
     * @param User   $user
     * @param string $uuid
     */
    public function unregister(User $user, string $uuid): void
    {
        if ($userPushId = $this->get($user, $uuid)) {
            $this->em->remove($userPushId);
            $this->em->flush();
        }
    }

    /**
     * Отправить PUSH пользователю.
     *
     * @param UserNotificationQueue $notificationQueue
     * @param User                  $user
     * @param array                 $data              a custom map of data that is passed back to your app
     *
     * @return bool
     */
    public function send(UserNotificationQueue $notificationQueue, User $user, array $data = []): bool
    {
        $params = $this->buildParams($notificationQueue, $user);

        if (!empty($data)) {
            $params['data'] = $data;
        }

        $buzzBrowser = new Browser(new Curl());
        $response = $buzzBrowser->post(self::API_URL, [
            'Content-Type' => 'application/json;charset=utf-8',
            'Authorization' => sprintf('Basic %s', $this->apiKey),
        ], json_encode($params));

        return 200 === $response->getStatusCode();
    }

    private function buildParams(UserNotificationQueue $notificationQueue, User $user): array
    {
        $params = [
            'app_id' => $this->appId,
            'url' => $notificationQueue->getUrl() ?? $this->userNotificationSubscriptionManager->getDefaultUrl(),
            'chrome_web_badge' => self::CHROME_WEB_BADGE,
            'headings' => [
                'en' => mb_substr($notificationQueue->getTitle(), 0, self::MAX_TITLE_LENGTH),
            ],
            'contents' => [
                'en' => mb_substr($notificationQueue->getBody(), 0, self::MAX_BODY_LENGTH),
            ],
        ];

        $uuidList = [];

        foreach ($user->getPushIds() as $pushId) {
            $uuidList[] = $pushId->getUuid();
        }

        $params['include_player_ids'] = $uuidList;

        return $params;
    }

    /**
     * @param User   $user
     * @param string $uuid
     *
     * @return UserPushId|null
     */
    private function get(User $user, string $uuid): ?UserPushId
    {
        return $this->em->getRepository(UserPushId::class)->findOneBy(['user' => $user, 'uuid' => $uuid]);
    }
}
