<?php

namespace Irmag\ProfileBundle\Service;

use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\ProfileBundle\Entity\User;

trait UserTrait
{
    /**
     * @var User
     */
    protected $user;

    /**
     * Назначает пользователя.
     *
     * @param User $user Пользователь
     *
     * @return $this
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Возвращает пользователя.
     * Если не использовали setUser, то будет возвращён текущий пользователь.
     *
     * @param bool $throwExceptionWhenUserNotAuthenticated
     *
     * @throws IrmagException When user is not authenticated
     *
     * @return User|null Пользователь
     */
    protected function getUser(bool $throwExceptionWhenUserNotAuthenticated = false): ?User
    {
        if (null === $this->user) {
            $token = $this->tokenStorage->getToken();

            if (null !== $token && $token->getUser() instanceof User) {
                $this->user = $token->getUser();
            } elseif (true === $throwExceptionWhenUserNotAuthenticated) {
                throw new IrmagException('User is not authenticated.');
            }
        }

        return $this->user;
    }
}
