<?php

namespace Irmag\ProfileBundle\Service\UserBlockHistoryManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\ProfileBundle\Service\UserTrait;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserBlockHistory;
use Irmag\ProfileBundle\Entity\Role;

class UserBlockHistoryManager
{
    use UserTrait;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Заблокировать / разблокировать пользователя.
     *
     * @param User $user
     * @param bool $needToChangeUserStatus Менять is_active пользователя?
     * @param bool $forcedBlockStatus      Явно заданное действие блокировки (true - блокировать, false - разблокировать)
     */
    public function blockUser(User $user, bool $needToChangeUserStatus = false, bool $forcedBlockStatus = null): void
    {
        $isActive = null !== $forcedBlockStatus ? $forcedBlockStatus : $user->getIsActive();
        $blockAction = $isActive ? UserBlockHistory::USER_ACTION_BLOCK : UserBlockHistory::USER_ACTION_UNBLOCK;
        $this->logUserBlockingState($user, $blockAction);

        if (true === $needToChangeUserStatus) {
            $user->setIsActive(!$user->getIsActive());

            $this->em->persist($user);
            $this->em->flush();
        }
    }

    /**
     * @param User   $user
     * @param string $disallowMode
     * @param array  $disallowRoles
     *
     * @throws IrmagException, NotFoundHttpException
     */
    public function addDisallowRolesForUser(User $user, string $disallowMode, array $disallowRoles): void
    {
        switch ($disallowMode) {
            case 'element':
                $blockingState = UserBlockHistory::USER_ACTION_ELEMENT_COMMENT_BLOCK;
                $unblockingState = UserBlockHistory::USER_ACTION_ELEMENT_COMMENT_UNBLOCK;
                break;
            case 'news' === $disallowMode || 'news_in_archive' === $disallowMode:
                $blockingState = UserBlockHistory::USER_ACTION_NEWS_COMMENT_BLOCK;
                $unblockingState = UserBlockHistory::USER_ACTION_NEWS_COMMENT_UNBLOCK;
                break;
            case 'blog':
                $blockingState = UserBlockHistory::USER_ACTION_BLOG_COMMENT_BLOCK;
                $unblockingState = UserBlockHistory::USER_ACTION_BLOG_COMMENT_UNBLOCK;
                break;
            case 'action' === $disallowMode || 'action_in_archive' === $disallowMode:
                $blockingState = UserBlockHistory::USER_ACTION_ACTION_COMMENT_BLOCK;
                $unblockingState = UserBlockHistory::USER_ACTION_ACTION_COMMENT_UNBLOCK;
                break;
            case 'forum':
                $blockingState = UserBlockHistory::USER_ACTION_FORUM_BLOCK;
                $unblockingState = UserBlockHistory::USER_ACTION_FORUM_UNBLOCK;
                break;
            default:
                throw new IrmagException(sprintf('Unknown comments mode "%s"', $disallowMode));
        }

        $roles = $this->em->getRepository(Role::class)
            ->findBy(['name' => $disallowRoles]);

        if (!$roles) {
            throw new NotFoundHttpException('Required roles did not found');
        }

        $isBlockAction = true;

        foreach ($roles as $role) {
            if (false === \in_array($role->getName(), $user->getRoles(), true)) {
                $user->addPositiveRole($role);
            } else {
                $isBlockAction = false;
                $user->removePositiveRole($role);
            }

            $this->em->persist($user);
        }

        $this->em->flush();
        $finalState = ($isBlockAction) ? $blockingState : $unblockingState;

        $this->logUserBlockingState($user, $finalState);
    }

    /**
     * Записывает в журнал действия по блокировке/разблокировке пользователей.
     *
     * @param User $user
     * @param int  $blockingStatus Статус блокировки
     */
    private function logUserBlockingState(User $user, int $blockingStatus): void
    {
        $userBlockHistory = new UserBlockHistory();
        $userBlockHistory->setUser($user);
        $userBlockHistory->setAdmin($this->getUser(true));
        $userBlockHistory->setStatus($blockingStatus);

        $this->em->persist($userBlockHistory);
        $this->em->flush();
    }
}
