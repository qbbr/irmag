<?php

namespace Irmag\ProfileBundle\Repository;

class RoleRepository extends \Doctrine\ORM\EntityRepository
{
    public function getSonataAdminRoles()
    {
        return $this->createQueryBuilder('r')
            ->where('r.name LIKE :pattern')
            ->setParameter('pattern', 'ROLE_IRMAG_ADMIN_%_ADMIN')
            ->getQuery()
            ->getResult();
    }
}
