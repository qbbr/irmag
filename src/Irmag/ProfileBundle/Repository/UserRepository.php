<?php

namespace Irmag\ProfileBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Irmag\CoreBundle\Repository\Traits\GetTotalTrait;
use Irmag\ProfileBundle\Entity\User;

class UserRepository extends \Doctrine\ORM\EntityRepository
{
    use GetTotalTrait;

    /**
     * @param string $usernameOrEmail
     *
     * @return User|null
     */
    public function findByUsernameOrEmail(string $usernameOrEmail): ?User
    {
        $user = $this->createQueryBuilder('u')
            ->select('u, g, gr')
            ->leftJoin('u.groups', 'g')
            ->leftJoin('g.roles', 'gr')
            ->where('LOWER(u.username) = LOWER(:username) OR LOWER(u.email) = LOWER(:email)')
            ->setParameter('username', $usernameOrEmail)
            ->setParameter('email', $usernameOrEmail)
            ->addOrderBy('u.id', 'asc')
            //->setMaxResults(1) <-- XXX: role/group BUG blad
            ->getQuery()
            ->getResult();

        if ($user) {
            return array_shift($user);
        }

        return null;
    }

    /**
     * Проверяет существование пользователя по username или email.
     *
     * @param string $usernameOrEmail
     *
     * @return bool
     */
    public function exists(string $usernameOrEmail): bool
    {
        if (empty($usernameOrEmail)) {
            return false;
        }

        return $this->findByUsernameOrEmail($usernameOrEmail) instanceof User;
    }

    /**
     * @return array
     */
    public function findByNotNullAndValidEmail()
    {
        return $this->createQueryBuilder('u')
            ->where('u.email IS NOT NULL')
            ->andWhere('u.isEmailInvalid IS NULL OR u.isEmailInvalid = false')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int    $socialMoneyBoxUserId
     * @param string $condition
     *
     * @return QueryBuilder
     */
    public function getUsersForDonate(int $socialMoneyBoxUserId, string $condition): QueryBuilder
    {
        return $this->createQueryBuilder('u')
            ->where('u.id != :social_money_box_user_id')
            ->andWhere('u.bonus > 0')
            ->andWhere($condition)
            ->setParameter('social_money_box_user_id', $socialMoneyBoxUserId);
    }
}
