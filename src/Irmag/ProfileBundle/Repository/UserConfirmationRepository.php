<?php

namespace Irmag\ProfileBundle\Repository;

use Symfony\Component\Security\Core\User\UserInterface;
use Irmag\ProfileBundle\Entity\UserConfirmation;

class UserConfirmationRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Удаляет все подтверждения (токены) у пользователя.
     *
     * @param UserInterface $user
     *
     * @return mixed
     */
    public function removeAllByUser(UserInterface $user)
    {
        return $this->createQueryBuilder('uc')
            ->delete(UserConfirmation::class, 'uc')
            ->where('uc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->execute();
    }
}
