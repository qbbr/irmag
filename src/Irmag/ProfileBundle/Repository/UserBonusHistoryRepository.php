<?php

namespace Irmag\ProfileBundle\Repository;

use Symfony\Component\Security\Core\User\UserInterface;

class UserBonusHistoryRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param UserInterface $user
     *
     * @return array
     */
    public function getAllByUser(UserInterface $user)
    {
        return $this->createQueryBuilder('ubh')
            ->where('ubh.user = :user')
            ->setParameter('user', $user)
            ->orderBy('ubh.createdAt', 'desc')
            ->getQuery()
            ->getResult();
    }
}
