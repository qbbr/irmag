<?php

namespace Irmag\ProfileBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\AuthenticationEvents;
use Symfony\Component\Security\Core\Event\AuthenticationEvent;
use Symfony\Component\Security\Core\Event\AuthenticationFailureEvent;
use Irmag\ProfileBundle\Entity\UserLoginAttempts;

class AuthenticationFailureSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @param EntityManagerInterface $em
     * @param RequestStack           $requestStack
     */
    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack
    ) {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            AuthenticationEvents::AUTHENTICATION_FAILURE => 'onAuthenticationFailure',
            AuthenticationEvents::AUTHENTICATION_SUCCESS => 'onAuthenticationSuccess',
        ];
    }

    /**
     * @param AuthenticationFailureEvent $event
     */
    public function onAuthenticationFailure(AuthenticationFailureEvent $event): void
    {
        $username = mb_strtolower($event->getAuthenticationToken()->getUsername());

        if ('anon.' === $username) {
            return;
        }

        $userLoginAttempts = $this->em->getRepository(UserLoginAttempts::class)->findOneBy(['username' => $username]);

        if (!$userLoginAttempts) {
            $userLoginAttempts = new UserLoginAttempts();
            $userLoginAttempts->setUsername($username);
        }

        $userLoginAttempts->setAttempts($userLoginAttempts->getAttempts() + 1);
        $userLoginAttempts->setIp($this->requestStack->getMasterRequest()->getClientIp());
        $this->em->persist($userLoginAttempts);
        $this->em->flush();
    }

    /**
     * @param AuthenticationEvent $event
     */
    public function onAuthenticationSuccess(AuthenticationEvent $event): void
    {
        $username = mb_strtolower($event->getAuthenticationToken()->getUsername());

        if ('anon.' === $username) {
            return;
        }

        $userLoginAttempts = $this->em->getRepository(UserLoginAttempts::class)->findOneBy(['username' => $username]);

        if ($userLoginAttempts) {
            $this->em->remove($userLoginAttempts);
            $this->em->flush();
        }
    }
}
