<?php

namespace Irmag\ProfileBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Irmag\ProfileBundle\Entity\UserConfirmation;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\SiteBundle\Service\BasketFavoriteManager\FavoriteManager;

class SecurityListener
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BasketManager
     */
    private $basketManager;

    /**
     * @var FavoriteManager
     */
    private $favoriteManager;

    public function __construct(
        EntityManagerInterface $em,
        BasketManager $basketManager,
        FavoriteManager $favoriteManager
    ) {
        $this->em = $em;
        $this->basketManager = $basketManager;
        $this->favoriteManager = $favoriteManager;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($user instanceof UserInterface) {
            // записываем время последней аутентификации пользователя
            $user->setLastLoginAt(new \DateTime());
            // удаляем токены ожидающие подтверждения (восстановление пароля)
            $this->em->getRepository(UserConfirmation::class)->removeAllByUser($user);
            // сливаем корзины
            $this->basketManager->merge();
            // сливаем избранные
            $this->favoriteManager->merge();
            // сохраняем
            $this->em->flush();
        }
    }
}
