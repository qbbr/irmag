<?php

namespace Irmag\ProfileBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Irmag\ProfileBundle\Service\UserStateManager\UserStateManager;

/**
 * XXX: unused.
 */
class UserStateExtension extends AbstractExtension
{
    /**
     * @var UserStateManager
     */
    private $userStateManager;

    /**
     * @param UserStateManager $userStateManager
     */
    public function __construct(UserStateManager $userStateManager)
    {
        $this->userStateManager = $userStateManager;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('user_state_get', [$this, 'get']),
        ];
    }

    public function get($key, $defaultValue = null)
    {
        return $this->userStateManager->get($key, $defaultValue);
    }
}
