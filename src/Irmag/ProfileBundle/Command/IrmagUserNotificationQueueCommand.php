<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\ProfileBundle\Entity\UserNotification;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Irmag\ProfileBundle\Service\UserNotificationManager\UserNotificationManager;
use Irmag\ProfileBundle\Service\UserPushManager\UserPushManager;
use Irmag\ProfileBundle\Entity\UserNotificationQueue;
use Irmag\ProfileBundle\Entity\UserNotificationSubscription;

class IrmagUserNotificationQueueCommand extends Command
{
    private const MAX_ITEMS_PER_PROCESS = 100;

    protected static $defaultName = 'irmag:queue:user_notification';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserNotificationManager
     */
    private $notificationManager;

    /**
     * @var UserPushManager
     */
    private $pushManager;

    /**
     * @param EntityManagerInterface  $em
     * @param UserNotificationManager $notificationManager
     * @param UserPushManager         $pushManager
     */
    public function __construct(
        EntityManagerInterface $em,
        UserNotificationManager $notificationManager,
        UserPushManager $pushManager
    ) {
        $this->em = $em;
        $this->notificationManager = $notificationManager;
        $this->pushManager = $pushManager;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Do notification queue')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queues = $this->em->getRepository(UserNotificationQueue::class)->findBy(['isDone' => false], ['id' => 'desc'], self::MAX_ITEMS_PER_PROCESS);

        foreach ($queues as $queue) {
            $this->em->persist($queue);

            // check ttl
            if ((new \DateTime()) > $queue->getExpiredAt()) {
                $queue->setIsDone(true);
                $this->em->flush();
                continue;
            }

            // process
            $user = $queue->getUser();
            $availableTypes = $this->notificationManager->getAvailableNotificationTypes($user, $queue->getEvent());

            // email
            if (\in_array(UserNotificationSubscription::IS_EMAIL_ENABLED, $availableTypes, true)) {
                if (false === $queue->getIsEmailDone()) {
                    $queue->setIsEmailDone($this->notificationManager->doEmailNotification($queue, $user));
                }
            } else {
                $queue->setIsEmailDone(true);
            }

            // site
            if (\in_array(UserNotificationSubscription::IS_SITE_ENABLED, $availableTypes, true)) {
                if (false === $queue->getIsSiteDone()) {
                    $userNotification = $this->notificationManager->doSiteNotification($queue, $user);
                    $queue->setIsSiteDone($userNotification instanceof UserNotification);
                }
            } else {
                $queue->setIsSiteDone(true);
            }

            // sms
            if (\in_array(UserNotificationSubscription::IS_SMS_ENABLED, $availableTypes, true)) {
                if (false === $queue->getIsSmsDone()) {
                    $queue->setIsSmsDone($this->notificationManager->doSmsNotification($queue, $user));
                }
            } else {
                $queue->setIsSmsDone(true);
            }

            // push
            if (\in_array(UserNotificationSubscription::IS_PUSH_ENABLED, $availableTypes, true) && false === $user->getPushIds()->isEmpty()) {
                if (false === $queue->getIsPushDone()) {
                    $data = [];

                    if (isset($userNotification)) {
                        $data['notification_id'] = $userNotification->getId();
                        unset($userNotification);
                    }
                    $queue->setIsPushDone($this->notificationManager->doPushNotification($queue, $user, $data));
                }
            } else {
                $queue->setIsPushDone(true);
            }

            // проверяем все типы уведомлений, если все выполнены, то проставляем isDone
            if (
                $queue->getIsPushDone()
                && $queue->getIsEmailDone()
                && $queue->getIsSiteDone()
                && $queue->getIsSmsDone()
            ) {
                $queue->setIsDone(true);
            }

            $this->em->flush();
        }

        $output->writeln(sprintf('<info>Complete. Queues processed: %d.</info>', \count($queues)));
    }
}
