<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\ProfileBundle\Entity\Role;

class IrmagRoleDeleteCommand extends Command
{
    protected static $defaultName = 'irmag:role:delete';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Delete Role')
            ->addArgument('name', InputArgument::REQUIRED, 'Name')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $role = $this->em->getRepository(Role::class)->findOneBy(['name' => $name]);
        $io = new SymfonyStyle($input, $output);

        $io->section('Deleting a Role');

        if ($role) {
            $io->table(
                ['Id', 'Name'],
                [
                    [
                        $role->getId(),
                        $role->getName(),
                    ],
                ]
            );

            if ($io->confirm('Remove?', false)) {
                $this->em->remove($role);
                $this->em->flush();
                $io->success('Role was successfully deleted');
            }
        } else {
            $io->warning('Role not found');
        }
    }
}
