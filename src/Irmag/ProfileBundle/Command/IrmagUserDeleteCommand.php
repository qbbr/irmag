<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Config;

class IrmagUserDeleteCommand extends Command
{
    protected static $defaultName = 'irmag:user:delete';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Delete User')
            ->addArgument('username', InputArgument::REQUIRED, 'Username')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $user = $this->em->getRepository(User::class)->findOneBy(['username' => $username]);
        $io = new SymfonyStyle($input, $output);

        $io->section('Deleting a User');

        if ($user) {
            $io->table(
                ['Id', 'Username', 'Email', 'Full name', 'Roles', 'Created at', 'Last login'],
                [
                    [
                        $user->getId(),
                        $user->getUsername(),
                        $user->getEmail(),
                        $user->getFullname(),
                        implode(', ', $user->getRoles()),
                        $this->formatDateTime($user->getCreatedAt()),
                        $this->formatDateTime($user->getLastLoginAt()),
                    ],
                ]
            );

            if ($io->confirm('Remove?', false)) {
                $this->em->remove($user);
                $this->em->flush();
                $io->success('User was successfully deleted');
            }
        } else {
            $io->warning('User not found');
        }
    }

    /**
     * @param \DateTime|mixed $dateTime
     *
     * @return string
     */
    protected function formatDateTime($dateTime): string
    {
        return ($dateTime instanceof \DateTime) ? $dateTime->format(Config::DATETIME_FORMAT) : '';
    }
}
