<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Service\BonusManager\BonusManager;
use Irmag\SiteBundle\Controller\MyController;
use Irmag\CoreBundle\Exception\IrmagException;

class IrmagUserDonateBonusCommand extends Command
{
    /**
     * @const int
     */
    const OPERATIONS_BEFORE_FLUSH = 50;

    protected static $defaultName = 'irmag:user:donate_bonus';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BonusManager
     */
    private $bonusManager;

    /**
     * @var int
     */
    private $socialMoneyBoxUserId;

    /**
     * @param EntityManagerInterface $em
     * @param BonusManager           $bonusManager
     * @param int                    $socialMoneyBoxUserId
     */
    public function __construct(
        EntityManagerInterface $em,
        BonusManager $bonusManager,
        int $socialMoneyBoxUserId
    ) {
        $this->em = $em;
        $this->bonusManager = $bonusManager;
        $this->socialMoneyBoxUserId = $socialMoneyBoxUserId;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Donate user bonuses')
            ->addArgument('condition', InputArgument::REQUIRED, 'DQL WHERE condition')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln(sprintf('<comment>Social money box user id:</comment> %d', $this->socialMoneyBoxUserId));

        $condition = $input->getArgument('condition');

        $usersForDonate = $this->em->getRepository(User::class)->getUsersForDonate($this->socialMoneyBoxUserId, $condition);
        $totalFound = $usersForDonate->select('COUNT(u.id)')->getQuery()->getSingleScalarResult();

        $output->writeln(sprintf('<comment>Total found:</comment> %d', $totalFound));

        if ($totalFound < 1) {
            return;
        }

        $totalBonus = 0.0;
        /** @var User[] $users */
        $users = $usersForDonate->select('u')->getQuery()->getResult();

        foreach ($users as $user) {
            $totalBonus += (float) $user->getBonus();
        }

        $output->writeln(sprintf('<comment>SQL:</comment> %s', $usersForDonate->getQuery()->getDQL()));
        $output->writeln(sprintf('<comment>Total bonuses:</comment> %.2f', $totalBonus));

        $io = new SymfonyStyle($input, $output);

        if (!$io->confirm('Continue?', false)) {
            return;
        }

        if ($output->isVerbose()) {
            $progress = new ProgressBar($output, $totalFound);
            $progress->start();
        }

        $socialMoneyBoxUser = $this->getSocialMoneyBoxUser();
        $operations = 0;

        foreach ($users as $user) {
            ++$operations;

            $value = $user->getBonus();

            $this->em->getConnection()->beginTransaction();

            try {
                // списываем бонусы у пользователя
                $this->bonusManager
                    ->setUser($user)
                    ->update(-$value, MyController::SOCIAL_MONEY_DONATE_MESSAGE);
                // начисляем в социальную копилку
                $this->bonusManager
                    ->setUser($socialMoneyBoxUser)
                    ->update($value, sprintf(MyController::SOCIAL_MONEY_RECEIVE_DONATE_MESSAGE, $user->getId()));

                $this->em->getConnection()->commit();
            } catch (\Exception $e) {
                $this->em->getConnection()->rollBack();
                throw $e;
            }

            if ($output->isVerbose()) {
                $progress->advance();
            }

            if (0 === $operations % self::OPERATIONS_BEFORE_FLUSH) {
                $this->em->flush();
            }
        }

        if ($output->isVerbose()) {
            $progress->finish();
        }

        $this->em->flush();

        $output->writeln('<info>Done</info>');
    }

    /**
     * @throws IrmagException
     *
     * @return User
     */
    protected function getSocialMoneyBoxUser(): User
    {
        $socialMoneyBoxUser = $this->em->getRepository(User::class)
            ->findOneBy(['id' => $this->socialMoneyBoxUserId]);

        if (!$socialMoneyBoxUser) {
            throw new IrmagException(sprintf('User with id "%d" not found.', $this->socialMoneyBoxUserId));
        }

        return $socialMoneyBoxUser;
    }
}
