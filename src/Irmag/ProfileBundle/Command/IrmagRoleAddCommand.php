<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Irmag\ProfileBundle\Entity\Role;

class IrmagRoleAddCommand extends Command
{
    protected static $defaultName = 'irmag:role:add';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param EntityManagerInterface $em
     * @param ValidatorInterface     $validator
     */
    public function __construct(
        EntityManagerInterface $em,
        ValidatorInterface $validator
    ) {
        $this->em = $em;
        $this->validator = $validator;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Create new Role')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->section('Adding a Role');

        // name
        $name = $io->ask('Name');
        $role = new Role($name);
        $this->validatePropertyValue($role, 'name', $name);

        // description [optional]
        $description = $io->ask('Description (optional)', false);
        if (false !== $description) {
            $this->validatePropertyValue($role, 'description', $description);
            $role->setDescription($description);
        }

        $errors = $this->validator->validate($role);

        if (\count($errors) > 0) {
            $io->error((string) $errors);
        } else {
            $this->em->persist($role);
            $this->em->flush();

            $io->success('Role created!');
        }
    }

    /**
     * @param Role   $role
     * @param string $propertyName
     * @param        $propertyValue
     */
    protected function validatePropertyValue(Role $role, string $propertyName, $propertyValue)
    {
        $violations = $this->validator->validatePropertyValue($role, $propertyName, $propertyValue);

        if (\count($violations)) {
            throw new \RuntimeException((string) $violations);
        }
    }
}
