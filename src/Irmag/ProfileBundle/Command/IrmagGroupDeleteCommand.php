<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\ProfileBundle\Entity\Group;

class IrmagGroupDeleteCommand extends Command
{
    protected static $defaultName = 'irmag:group:delete';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Delete Group')
            ->addArgument('name', InputArgument::REQUIRED, 'Name')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $group = $this->em->getRepository(Group::class)->findOneBy(['name' => $name]);
        $io = new SymfonyStyle($input, $output);

        $io->section('Deleting a Group');

        if ($group) {
            $io->table(
                ['Id', 'Name', 'Count of Users', 'Roles'],
                [
                    [
                        $group->getId(),
                        $group->getName(),
                        $group->getUsers()->count(),
                        implode(', ', $group->getRoles()->toArray()),
                    ],
                ]
            );

            if ($io->confirm('Remove?', false)) {
                $this->em->remove($group);
                $this->em->flush();
                $io->success('Group was successfully deleted');
            }
        } else {
            $io->warning('Group not found');
        }
    }
}
