<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Config;

class IrmagUserListCommand extends Command
{
    protected static $defaultName = 'irmag:user:list';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('List of Users')
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'Limit', 50)
            ->addOption('offset', null, InputOption::VALUE_OPTIONAL, 'Offset', 0)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = $input->getOption('limit');
        $offset = $input->getOption('offset');
        $users = $this->em->getRepository(User::class)->findBy([], ['id' => 'DESC'], $limit, $offset);
        $io = new SymfonyStyle($input, $output);

        $io->section('List of Users');

        $usersAsPlainArrays = array_map(function (User $user) {
            return [
                $user->getId(),
                $user->getUsername(),
                $user->getEmail(),
                $user->getFullname(),
                implode(', ', $user->getRoles()),
                $user->getGroups()->count(),
                $this->formatDateTime($user->getCreatedAt()),
                $this->formatDateTime($user->getLastLoginAt()),
            ];
        }, $users);

        $io->table(
            ['Id', 'Username', 'Email', 'Full name', 'Roles', 'Count of Groups', 'Created at', 'Last login'],
            $usersAsPlainArrays
        );
    }

    /**
     * @param |DateTime|mixed $dateTime
     *
     * @return string
     */
    protected function formatDateTime($dateTime): string
    {
        return ($dateTime instanceof \DateTime) ? $dateTime->format(Config::DATETIME_FORMAT) : '';
    }
}
