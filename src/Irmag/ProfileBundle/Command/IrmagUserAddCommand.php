<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\Group;

class IrmagUserAddCommand extends Command
{
    protected static $defaultName = 'irmag:user:add';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * @param EntityManagerInterface       $em
     * @param ValidatorInterface           $validator
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        EntityManagerInterface $em,
        ValidatorInterface $validator,
        UserPasswordEncoderInterface $passwordEncoder
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->passwordEncoder = $passwordEncoder;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Create new User')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->section('Adding a User');

        $user = new User();

        // username
        $username = $io->ask('Username');
        $this->validatePropertyValue($user, 'username', $username);
        $user->setUsername($username);

        // email [optional]
        $email = $io->ask('Email (optional)', false);
        if (false !== $email) {
            $this->validatePropertyValue($user, 'email', $email);
            $user->setEmail($email);
        }

        // groups
        $groups = $this->em->createQueryBuilder()
            ->select('g.name')
            ->from(Group::class, 'g')
            ->getQuery()
            ->getResult();

        $groups = array_map('current', $groups);
        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Select groups (Example: 0,1,3)',
            $groups
        );
        $question->setMultiselect(true);
        $groups = $helper->ask($input, $output, $question);

        if (!empty($groups)) {
            foreach ($groups as $groupName) {
                if ($group = $this->em->getRepository(Group::class)->findOneBy(['name' => $groupName])) {
                    $user->addGroup($group);
                }
            }
        }

        // password
        $plainPassword = $io->askHidden('Password');
        $this->validatePropertyValue($user, 'password', $plainPassword);

        $password = $this->passwordEncoder->encodePassword($user, $plainPassword);
        $user->setPassword($password);

        $errors = $this->validator->validate($user);

        if (\count($errors) > 0) {
            $io->error((string) $errors);
        } else {
            $this->em->persist($user);
            $this->em->flush();

            $io->success('User created!');
        }
    }

    /**
     * @param User   $user
     * @param string $propertyName
     * @param        $propertyValue
     */
    protected function validatePropertyValue(User $user, string $propertyName, $propertyValue)
    {
        $violations = $this->validator->validatePropertyValue($user, $propertyName, $propertyValue);

        if (\count($violations)) {
            throw new \RuntimeException((string) $violations);
        }
    }
}
