<?php

namespace Irmag\ProfileBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\ProfileBundle\Entity\Role;

class IrmagRoleListCommand extends Command
{
    protected static $defaultName = 'irmag:role:list';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('List of Roles')
            ->addOption('limit', null, InputOption::VALUE_OPTIONAL, 'Limit', 50)
            ->addOption('offset', null, InputOption::VALUE_OPTIONAL, 'Offset', 0)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $limit = $input->getOption('limit');
        $offset = $input->getOption('offset');
        $roles = $this->em->getRepository(Role::class)->findBy([], ['id' => 'DESC'], $limit, $offset);
        $io = new SymfonyStyle($input, $output);

        $io->section('List of Roles');

        $rolesAsPlainArrays = array_map(function (Role $role) {
            return [
                $role->getId(),
                $role->getName(),
            ];
        }, $roles);

        $io->table(
            ['Id', 'Name'],
            $rolesAsPlainArrays
        );
    }
}
