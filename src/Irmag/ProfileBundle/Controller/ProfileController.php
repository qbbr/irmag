<?php

namespace Irmag\ProfileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormError;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\CoreBundle\Utils\TokenGenerator;
use Irmag\ProfileBundle\Form\Type\ProfileType;
use Irmag\ProfileBundle\Form\Type\ProfileRemoveType;
use Irmag\ProfileBundle\Form\Type\ProfileChangePasswordType;
use Irmag\ProfileBundle\Form\Type\ProfileChangeLoginType;
use Irmag\ProfileBundle\Form\Type\ProfileChangeEmailType;
use Irmag\ProfileBundle\Form\Type\ProfileChangeAvatarType;
use Irmag\ProfileBundle\Form\Type\ProfileOrderProfileType;
use Irmag\ProfileBundle\Entity\UserOrderProfile;
use Irmag\ProfileBundle\Entity\UserConfirmation;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Mailer\SecurityMailer;
use Irmag\ProfileBundle\Service\UserPushManager\UserPushManager;
use Irmag\SiteBundle\Entity\OrderDeliveryCity;

/**
 * @Route("/")
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 */
class ProfileController extends Controller
{
    /**
     * Показывает форму профиля пользователя.
     *
     * @Route("/", name="irmag_profile")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $form = $this->createForm(ProfileType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user = $form->getData();
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($user);
            $em->flush();

            $this->addFlash('success', 'Ваши изменения сохранены.');

            return $this->redirectToRoute('irmag_profile');
        }

        return $this->render('@IrmagProfile/Profile/index.html.twig', [
            'form' => $form->createView(),
            'push_ids' => $user->getPushIds(),
        ]);
    }

    /**
     * @Route("/push_ids_list/",
     *     name="irmag_profile_push_ids_list",
     *     options={"expose": true},
     *     methods={"POST"},
     *     condition="request.isXmlHttpRequest()"
     * )
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pushIdsListAction(Request $request)
    {
        return $this->render('@IrmagProfile/Profile/push-ids-list.html.twig', [
            'current_push_id' => $request->request->get('currentPushId'),
            'push_ids' => $this->getUser()->getPushIds(),
        ]);
    }

    /**
     * Удаляет профиль.
     *
     * @Route("/remove/", name="irmag_profile_remove")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function profileRemoveAction(Request $request)
    {
        $form = $this->createForm(ProfileRemoveType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // удаляем профиль
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($this->getUser());
            $em->flush();
            // очищаем аутентификационный токен и сессию
            $this->get('security.token_storage')->setToken(null);
            $request->getSession()->invalidate();

            return $this->redirectToRoute('irmag_homepage');
        }

        return $this->render('@IrmagProfile/Profile/remove.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Меняет пароль.
     *
     * @Route("/change-password/", name="irmag_profile_change_password")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changePasswordAction(Request $request)
    {
        $form = $this->createForm(ProfileChangePasswordType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            // назначаем новый пароль
            $user = $this->getUser();
            $pwd = $data->getNewPassword();
            $encoder = $this->container->get('security.password_encoder');
            $pwd = $encoder->encodePassword($user, $pwd);
            $user->setPassword($pwd);
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('irmag_profile_change_password_success');
        }

        return $this->render('@IrmagProfile/Profile/change-password.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Сообщает об успешном изменение пароля.
     *
     * @Route("/change-password/success/", name="irmag_profile_change_password_success")
     */
    public function changePasswordSuccessAction()
    {
        return $this->render('@IrmagProfile/Profile/change-password-success.html.twig');
    }

    /**
     * Меняет логин.
     *
     * @Route("/change-login/", name="irmag_profile_change_login")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeLoginAction(Request $request)
    {
        $form = $this->createForm(ProfileChangeLoginType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $newUsername = $data->getNewUsername();
            $em = $this->get('doctrine.orm.default_entity_manager');

            if ($em->getRepository(User::class)->exists($newUsername)) {
                $form->get('newUsername')->addError(new FormError('Пользователь с таким логином уже существует'));
            } else {
                $user = $this->getUser();
                $user->setUsername($newUsername);
                $em->persist($user);
                $em->flush();

                return $this->redirectToRoute('irmag_profile_change_login_success');
            }
        }

        return $this->render('@IrmagProfile/Profile/change-login.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Сообщает об успешном изменение логина.
     *
     * @Route("/change-login/success/", name="irmag_profile_change_login_success")
     */
    public function changeLoginSuccessAction()
    {
        return $this->render('@IrmagProfile/Profile/change-login-success.html.twig');
    }

    /**
     * Меняет эл.почту.
     *
     * @Route("/change-email/", name="irmag_profile_change_email")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeEmailAction(Request $request)
    {
        $form = $this->createForm(ProfileChangeEmailType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $newEmail = $data->getNewEmail();
            $em = $this->get('doctrine.orm.default_entity_manager');

            if ($em->getRepository(User::class)->exists($newEmail)) {
                $form->get('newEmail')->addError(new FormError('Пользователь с таким Email уже существует.'));
            } else {
                $user = $this->getUser();
                // создаём токен
                $userConfirmation = new UserConfirmation();
                $userConfirmation->setToken(TokenGenerator::generateToken());
                $userConfirmation->setUser($user);
                $userConfirmation->setEmail($newEmail);
                $em->persist($userConfirmation);
                $em->flush();

                // отсылаем письмо с подтверждением email
                $this->get(SecurityMailer::class)->sendConfirmationEmailMessage($newEmail, [
                    'userConfirmation' => $userConfirmation,
                    'user' => $user,
                ]);

                $request->getSession()->set('user_confirmation_user_id', $user->getId());

                return $this->redirectToRoute('security_confirmation_email_sended');
            }
        }

        return $this->render('@IrmagProfile/Profile/change-email.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Сообщает об успешном изменение эл.почты.
     *
     * @Route("/change-email/success/", name="irmag_profile_change_email_success")
     */
    public function changeEmailSuccessAction()
    {
        return $this->render('@IrmagProfile/Profile/change-email-success.html.twig');
    }

    /**
     * Меняет аватар.
     *
     * @Route("/change-avatar/", name="irmag_profile_change_avatar")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function changeAvatarAction(Request $request)
    {
        $user = $this->getUser();
        $form = $this->createForm(ProfileChangeAvatarType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('irmag_profile_change_avatar');
        }

        return $this->render('@IrmagProfile/Profile/change-avatar.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Удаляет аватар.
     *
     * @Route("/change-avatar/remove/", name="irmag_profile_remove_avatar")
     */
    public function removeAvatarAction()
    {
        $user = $this->getUser();
        $avatar = $user->getAvatar();

        if ($avatar && $avatar->getPath()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->remove($avatar);
            $em->flush();
        }

        return $this->redirectToRoute('irmag_profile_change_avatar');
    }

    /**
     * Выводит список профилей заказа.
     *
     * @Route("/order-profiles/", name="irmag_profile_order_profiles")
     */
    public function orderProfilesAction()
    {
        $orderProfiles = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(UserOrderProfile::class)
            ->findBy(['user' => $this->getUser()], ['id' => 'ASC']);

        return $this->render('@IrmagProfile/Profile/order-profiles.html.twig', [
            'order_profiles' => $orderProfiles,
        ]);
    }

    /**
     * Создаёт новый профиль заказа.
     *
     * @Route("/order-profiles/new/", name="irmag_profile_order_profiles_new")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function orderProfilesNewAction(Request $request)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        // default city
        $deliveryCity = $em->getRepository(OrderDeliveryCity::class)->findOneBy(['name' => 'Иркутск']);
        $userOrderProfile = new UserOrderProfile();
        $userOrderProfile->setDeliveryCity($deliveryCity);
        $form = $this->createForm(ProfileOrderProfileType::class, $userOrderProfile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userOrderProfile->setUser($this->getUser());
            $exists = $em->getRepository(UserOrderProfile::class)->findOneBy(['user' => $this->getUser()]);

            if (!$exists) {
                $userOrderProfile->setIsPrimary(true);
            }

            $em->persist($userOrderProfile);
            $em->flush();

            return $this->redirectToRoute('irmag_profile_order_profiles');
        }

        return $this->render('@IrmagProfile/Profile/order-profiles-form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Удаляет профиль заказа.
     * XXX: при удаление primary профиля нужно его переназначить, т.к в /order/ пропадает выбор профиля.
     *
     * @Route("/order-profiles/remove/{id}/", name="irmag_profile_order_profiles_remove", requirements={"id": "\d+"})
     *
     * @param $id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function orderProfilesRemoveAction(int $id)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $userOrderProfile = $this->getUserOrderProfileByIdOrThrowNotFoundException($id);
        $em->remove($userOrderProfile);
        $em->flush();

        $this->addFlash('success', 'Адрес успешно удалён');

        return $this->redirectToRoute('irmag_profile_order_profiles');
    }

    /**
     * Редактирует профиль заказа.
     *
     * @Route("/order-profiles/edit/{id}/", name="irmag_profile_order_profiles_edit", requirements={"id": "\d+"})
     *
     * @param Request $request
     * @param int     $id      UserOrderProfile id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function orderProfilesEditAction(Request $request, int $id)
    {
        $userOrderProfile = $this->getUserOrderProfileByIdOrThrowNotFoundException($id);
        $form = $this->createForm(ProfileOrderProfileType::class, $userOrderProfile);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.default_entity_manager');
            $em->persist($userOrderProfile);
            $em->flush();

            $this->addFlash('success', 'Профиль заказа успешно изменён!');

            return $this->redirectToRoute('irmag_profile_order_profiles');
        }

        return $this->render('@IrmagProfile/Profile/order-profiles-form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Назначает профиль заказа основным (по-умолчанию).
     *
     * @Route("/order-profiles/set-primary/{id}/", name="irmag_profile_order_profiles_set_primary")
     *
     * @param int $id UserOrderProfile id
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function orderProfilesSetPrimaryAction(int $id)
    {
        $user = $this->getUser();
        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->getRepository(UserOrderProfile::class)->unsetAllPrimary($user);
        $userOrderProfile = $this->getUserOrderProfileByIdOrThrowNotFoundException($id);
        $userOrderProfile->setIsPrimary(true);
        $em->persist($userOrderProfile);
        $em->flush();

        $this->addFlash('success', 'Основной профиль заказа успешно изменён!');

        return $this->redirectToRoute('irmag_profile_order_profiles');
    }

    /**
     * @param int $id UserOrderProfile id
     *
     * @return UserOrderProfile
     */
    private function getUserOrderProfileByIdOrThrowNotFoundException(int $id): UserOrderProfile
    {
        $userOrderProfile = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(UserOrderProfile::class)
            ->findOneBy(['user' => $this->getUser(), 'id' => $id]);

        if (!$userOrderProfile) {
            throw $this->createNotFoundException(sprintf('UserOrderProfile with id "%d" not found.', $id));
        }

        return $userOrderProfile;
    }

    /**
     * @Route("/push/register_unregister/",
     *     name="irmag_profile_push_register_unregister",
     *     options={"expose": true},
     *     methods={"POST"},
     *     condition="request.isXmlHttpRequest()"
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function pushRegisterUnregisterAction(Request $request): JsonResponse
    {
        $isSubscribed = $request->request->getBoolean('is_subscribed');
        $uuid = $request->request->get('uuid');
        $user = $this->getUser();

        if ($isSubscribed) {
            $ip = $request->request->get('ip');
            $device = $request->request->get('device');
            $ua = $request->request->get('ua');

            $this->get(UserPushManager::class)->register($user, $uuid, $device, $ip, $ua);
        } else {
            $this->get(UserPushManager::class)->unregister($user, $uuid);
        }

        return new JsonResponse(['success' => true]);
    }
}
