<?php

namespace Irmag\ProfileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\SiteBundle\Entity\Order;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;
use Irmag\ProfileBundle\Entity\UserNotification;
use Irmag\ProfileBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/~{username}/", name="irmag_profile_user")
     *
     * @param string $username
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(string $username)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $user = $this->getUserByUsernameOrThrowNotFoundException($username);

        return $this->render('@IrmagProfile/Default/index.html.twig', [
            'user' => $user,
            'orders_count' => $em->getRepository(Order::class)->getCount($user),
            'comments_count' => $em->getRepository(ThreadComment::class)->getCount($user),
        ]);
    }

    /**
     * @Route("/notification_clicked/", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function notificationClickedHookAction(Request $request)
    {
        $content = json_decode($request->getContent(), true);

        if (
            isset($content['event'])
            &&
            'notification.clicked' === $content['event']
            &&
            isset($content['data']['notification_id'])
        ) {
            $notificationId = (int) $content['data']['notification_id'];
            $em = $this->get('doctrine.orm.default_entity_manager');
            $notification = $em->getRepository(UserNotification::class)->find($notificationId);

            if ($notification instanceof UserNotification) {
                $notification->setIsAsRead(true);
                $em->persist($notification);
                $em->flush();
            }
        }

        return new JsonResponse([]);
    }

    /**
     * @param string $username
     *
     * @return User
     */
    private function getUserByUsernameOrThrowNotFoundException(string $username): User
    {
        $user = $this->get('doctrine.orm.default_entity_manager')->getRepository(User::class)->findOneBy(['username' => $username]);

        if (!$user) {
            throw $this->createNotFoundException(sprintf('User "%s" was not found', $username));
        }

        return $user;
    }
}
