<?php

namespace Irmag\AttachmentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\CoreBundle\Utils\Markdown;
use Irmag\AttachmentBundle\Service\TextTools;
use Irmag\AttachmentBundle\Exception\IrmagAttachmentException;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\BlogBundle\Entity\BlogPostAttachment;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ForumBundle\Entity\ForumPostAttachment;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;
use Irmag\ThreadCommentBundle\Entity\ThreadCommentAttachment;

class AttachmentController extends Controller
{
    const FILTER_THREAD_COMMENT = 'thread_comment_image';
    const FILTER_BLOG_POST = 'blog_post_image';
    const FILTER_FORUM_POST = 'forum_image';
    const FILTER_FORUM_MESSAGE = 'forum_image';

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/ajax/md2html/",
     *     name="irmag_markdown_to_html",
     *     options={"expose": true},
     *     condition="request.isXmlHttpRequest()",
     *     methods={"POST"}
     * )
     *
     * @param Request $request
     *
     * @throws IrmagAttachmentException
     *
     * @return Response
     */
    public function previewAction(Request $request): Response
    {
        $md = $request->request->get('md');
        $mode = $request->request->get('mode');
        $objectId = (null !== $request->get('object')) ? $request->get('object') : null;

        // оборачивать картинки в слайдер?
        $needWrapping = $request->request->getBoolean('need_wrapping');
        // чистить внешние ссылки?
        $needSanitizing = true;
        // заменять пустой текст ссылки на title страницы?
        $needReplacing = true;
        // проверять дополнительно на предоставленную роль?
        $needCheckingForAdvancedRole = false;
        // разрешить встраивать видео?
        $isVideoAllowed = false;

        switch ($mode) {
            case 'thread_comment':
                $hostClass = ThreadComment::class;
                $class = ThreadCommentAttachment::class;
                $needCheckingForAdvancedRole = true;
                $imageFilter = self::FILTER_THREAD_COMMENT;
                break;
            case 'forum_post':
                $hostClass = ForumPost::class;
                $class = ForumPostAttachment::class;
                $needReplacing = false;
                $needCheckingForAdvancedRole = true;
                $imageFilter = self::FILTER_FORUM_POST;
                break;
            case 'blog_post':
                $hostClass = BlogPost::class;
                $class = BlogPostAttachment::class;
                $isVideoAllowed = true;
                $imageFilter = self::FILTER_BLOG_POST;
                break;
            default:
                throw new IrmagAttachmentException('Cannot determine attachment type: wrong mode parameter.');
                break;
        }

        $em = $this->get('doctrine.orm.default_entity_manager');
        $user = $this->getUser();

        if (null !== $objectId && is_numeric($objectId)) {
            $object = $em->getRepository($hostClass)->find($objectId);
            if (!$object) {
                throw new IrmagAttachmentException(sprintf('Can not find %s object to attach to (id %s)!', $hostClass, $objectId));
            }
        } else {
            $object = null;
        }

        $allowedAttachments = $em->getRepository($class)->getAllowedAttachments($user, $object);
        $textToolsService = $this->get(TextTools::class);

        $md = $textToolsService->removeInlineLinksInMarkdown($md);
        $html = $this->get(Markdown::class)->toHtml($md);
        $html = $textToolsService->replaceMarksWithRealAttachments($html, $allowedAttachments, $imageFilter);

        if ($isVideoAllowed) {
            $html = html_entity_decode($textToolsService->injectVideo($html));
        }

        if ($needWrapping) {
            $html = html_entity_decode($textToolsService->wrapImagesToSliderContainer($html));
        }

        if ($needSanitizing) {
            if ($needCheckingForAdvancedRole) {
                if (!$this->get('security.authorization_checker')->isGranted('ROLE_ALLOW_ADVANCED_COMMENTS')) {
                    $html = html_entity_decode($textToolsService->sanitizeLinks($html, $needReplacing, false));
                } else {
                    $html = html_entity_decode($textToolsService->injectVideo($html));
                }
            } else {
                $html = html_entity_decode($textToolsService->sanitizeLinks($html, $needReplacing));
            }
        }

        return new Response($html);
    }
}
