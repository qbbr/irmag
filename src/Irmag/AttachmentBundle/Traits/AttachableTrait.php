<?php

namespace Irmag\AttachmentBundle\Traits;

use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Entity\File;

trait AttachableTrait
{
    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    /**
     * Ссылка на core File.
     *
     * @var File
     *
     * @ORM\OneToOne(targetEntity="\Irmag\SiteBundle\Entity\File", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $file;

    /**
     * Пользователь, загрузивший прикрепление.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $userAttachedBy;

    /**
     * @param bool $isActive
     *
     * @return self
     */
    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    /**
     * Set file.
     *
     * @param File $file
     *
     * @return self
     */
    public function setFile(?File $file): self
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file.
     *
     * @return File|null
     */
    public function getFile(): ?File
    {
        return $this->file;
    }

    /**
     * Set attachedBy.
     *
     * @param User|null $userAttachedBy
     *
     * @return self
     */
    public function setUserAttachedBy(?User $userAttachedBy): self
    {
        $this->userAttachedBy = $userAttachedBy;

        return $this;
    }

    /**
     * Get attachedBy.
     *
     * @return User|null
     */
    public function getUserAttachedBy(): ?User
    {
        return $this->userAttachedBy;
    }
}
