<?php

namespace Irmag\AttachmentBundle\Traits;

use Irmag\AttachmentBundle\Interfaces\IrmagAttachmentPoint;
use Irmag\ProfileBundle\Entity\User;

trait AttachmentsRepositoryTrait
{
    /**
     * Получить массив аттачей, которые разрешено прикрепить к данному объекту.
     * Разрешёнными считаются:
     *    - уже прикреплённые к данному объекту аттачи;
     *    - загруженные пользователем, но ещё не прикреплённые ни к одному объекту аттачи.
     *
     * @param User                      $user   Пользователь, загрузивший прикрепление
     * @param IrmagAttachmentPoint|null $object Объект для прикрепления
     *
     * @return array
     */
    public function getAllowedAttachments(User $user, ?IrmagAttachmentPoint $object): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.isActive = :isActive')
            ->andWhere('a.userAttachedBy = :user')
            ->andWhere('a.object = :object OR a.object IS NULL')
            ->setParameter('isActive', true)
            ->setParameter('user', $user)
            ->setParameter('object', $object)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить массив аттачей, которые были загружены пользователем, но не были прикреплены.
     *
     * @param User $user Пользователь, загрузивший прикрепление
     *
     * @return array
     */
    public function getFreeUploadedAttachments(User $user): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.isActive = :isActive')
            ->andWhere('a.userAttachedBy = :user')
            ->andWhere('a.object IS NULL')
            ->orderBy('a.createdAt', 'ASC')
            ->setParameter('isActive', true)
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
