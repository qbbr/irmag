<?php

namespace Irmag\AttachmentBundle\Service;

use Buzz\Browser;
use Doctrine\ORM\EntityManagerInterface;
use Twig_Environment as Twig;
use Symfony\Component\DomCrawler\Crawler;
use Irmag\CoreBundle\Utils\Markdown;

class TextTools
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var Markdown
     */
    private $md;

    /**
     * @var Browser
     */
    private $buzzBrowser;

    /**
     * @var string
     */
    private $innerDomain;

    /**
     * @param EntityManagerInterface $em
     * @param Twig                   $twig
     * @param Markdown               $md
     * @param string                 $innerDomain
     */
    public function __construct(
        EntityManagerInterface $em,
        Twig $twig,
        Markdown $md,
        string $innerDomain
    ) {
        $this->em = $em;
        $this->twig = $twig;
        $this->md = $md;
        $this->buzzBrowser = new Browser();
        $this->innerDomain = $innerDomain;
    }

    /**
     * Вставляет в теги <img> реальные пути до изображений вместо attachment_id.
     *
     * @param string $text        Текст (HTML или markdown)
     * @param array  $attachments Все аттачи
     * @param string $imageFilter Название фильтра Liip
     * @param bool   $isMarkdown  Текст в формате markdown
     *
     * @return string Форматированный HTML
     */
    public function replaceMarksWithRealAttachments($text, array $attachments, $imageFilter, $isMarkdown = false): string
    {
        $htmlText = (!$isMarkdown) ? $text : $this->md->toHtml($text);
        $doc = new \DOMDocument('1.0', 'utf-8');

        if (!empty($htmlText)) {
            $fragment = new \DOMDocument('1.0', 'utf-8');
            try {
                $fragment->loadHTML(
                    mb_convert_encoding(str_replace('&', '&amp;', $htmlText), 'html-entities', 'utf-8'),
                    LIBXML_HTML_NODEFDTD
                );
            } catch (\Exception $e) {
                $htmlText = $this->md->toHtml(strip_tags($text));
                $fragment->loadHTML(
                    mb_convert_encoding(str_replace('&', '&amp;', $htmlText), 'html-entities', 'utf-8'),
                    LIBXML_HTML_NODEFDTD
                );
            }
        } else {
            $fragment = '';
        }

        $root = $doc->createElement('div');
        $doc->appendChild($root);

        $crawler = new Crawler($fragment);
        foreach ($crawler->filterXPath('//body/child::node()') as $node) {
            $newNode = $doc->importNode($node, true);
            $root->appendChild($newNode);
        }

        $crawler = new Crawler($doc);
        $images = $crawler->filter('img');

        if (empty($attachments)) {
            return $doc->saveHTML();
        }

        $attaches = array_map(function ($val) {
            return $val->getId();
        }, $attachments);

        foreach ($images as $img) {
            $pic = (int) $img->getAttribute('src');
            // если картинки нет в списке аттачей, то мы не должны её показывать
            if (true === \in_array($pic, $attaches, true)) {
                $attach = $attachments[array_search($pic, $attaches, true)];
                $webpath = $attach->getFile()->getWebPath();
                $template = $this->twig->loadTemplate('@IrmagAttachment/Attachments/attached-image.html.twig');
                $imageCode = $template->renderBlock('code', [
                    'image_path' => $webpath,
                    'filter' => $imageFilter,
                    'alt' => $img->getAttribute('alt'),
                ]);

                $fragment = new \DOMDocument('1.0', 'utf-8');
                $fragment->loadHTML(mb_convert_encoding($imageCode, 'html-entities', 'utf-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
                $newImg = $fragment->getElementsByTagName('img')->item(0);
                $newNode = $doc->importNode($newImg, true);
                $img->parentNode->replaceChild($newNode, $img);
            } else {
                $img->parentNode->removeChild($img);
            }
        }

        return $doc->saveHTML();
    }

    /**
     * Форматирует ссылку таким образом, чтобы видео встроилось в превью
     * Формат: https://www.youtube.com/embed/{video_id}.
     *
     * @param string $link Строчка, содержащая URL видео
     *
     * @return string Форматированная строка
     */
    protected function getYoutubeLinkForEmbed($link): string
    {
        $id = '';
        $chunks = parse_url($link);

        if ('www.youtube.com' === $chunks['host'] || 'youtube.com' === $chunks['host']) {
            if ('/watch' === $chunks['path']) {
                $queryChunks = explode('=', $chunks['query']);
                $id = $queryChunks[1];
            } else {
                $pathChunks = explode('/', $chunks['path']);
                if (\in_array($pathChunks[1], ['v', 'embed'], true)) {
                    $id = $pathChunks[\count($pathChunks) - 1];
                }
            }
        } elseif ('youtu.be' === $chunks['host']) {
            $id = $chunks['path'];
        }

        $embedLink = ('' !== $id)
            ? sprintf('%s%s', 'https://www.youtube.com/embed/', $id)
            : '';

        return $embedLink;
    }

    /**
     * Получить список ID всех вставленных в текст картинок.
     *
     * @param string $text       Текст
     * @param bool   $isMarkdown Текст в разметке markdown?
     *
     * @return array
     */
    public function getAttachedToText($text, $isMarkdown = false): array
    {
        $attaches = [];
        $htmlText = (!$isMarkdown) ? $text : $this->md->toHtml($text);

        if ('' !== $htmlText) {
            $doc = new Crawler($htmlText);
            $images = $doc->filter('img');
            foreach ($images as $img) {
                if (is_numeric($img->getAttribute('src'))) {
                    $attaches[] = $img->getAttribute('src');
                }
            }
        }

        return $attaches;
    }

    /**
     * Оборачивает подряд идущие теги <img> в пределах одного параграфа (<p>) в тег <div>.
     * Используется для активации слайдера.
     *
     * @param string $text       Текст
     * @param bool   $isMarkdown Текст в разметке markdown?
     *
     * @return string HTML со вставленными div-обёртками
     */
    public function wrapImagesToSliderContainer($text, $isMarkdown = false): string
    {
        $htmlText = (!$isMarkdown) ? $text : $this->md->toHtml($text);
        $doc = new \DOMDocument('1.0', 'utf-8');
        $doc->loadHTML(mb_convert_encoding(str_replace('&', '&amp;', $htmlText), 'html-entities', 'utf-8'), LIBXML_HTML_NODEFDTD | LIBXML_HTML_NOIMPLIED);
        $crawler = new Crawler($doc);
        $paragraphs = $crawler->filter('p');

        foreach ($paragraphs as $par) {
            $subCrawler = new Crawler($par);
            $images = $subCrawler->filter('img');

            if ($images->count() > 1) {
                $div = $doc->createElement('div');
                $attr = $doc->createAttribute('class');
                $attr->value = 'slider-container';
                $div->appendChild($attr);

                foreach ($images as $img) {
                    $cloneImg = $img->cloneNode();
                    $div->appendChild($cloneImg);
                    $par->removeChild($img);
                }

                $par->parentNode->insertBefore($div, $par);

                if ($par->hasChildNodes()) {
                    $clonedPar = $par->cloneNode(true);
                    $div->parentNode->insertBefore($clonedPar, $div);
                }

                $par->parentNode->removeChild($par);
            }
        }

        return $doc->saveHTML();
    }

    /**
     * Обрезаем html для показа в превью (до 4 абзаца включительно).
     *
     * @param string $text       Полный текст
     * @param bool   $isMarkdown Текст в markdown?
     *
     * @return string Превью поста
     */
    public function cropTextToPreview($text, $isMarkdown = false): string
    {
        $html = (!$isMarkdown) ? $text : $this->md->toHtml($text);
        $doc = new \DOMDocument('1.0', 'utf-8');
        $doc->loadHTML(mb_convert_encoding(str_replace('&', '&amp;', $html), 'html-entities', 'utf-8'), LIBXML_HTML_NODEFDTD | LIBXML_HTML_NOIMPLIED);

        $fragment = new \DOMDocument('1.0', 'utf-8');
        $root = $fragment->createElement('div');
        $fragment->appendChild($root);

        $crawler = new Crawler($doc);
        $i = 0;

        $nodes = ($isMarkdown) ? $crawler : $crawler->filterXPath('//div/child::node()');
        foreach ($nodes as $node) {
            $name = $node->nodeName;
            if ('p' === $name) {
                ++$i;
            }

            if ($i <= 3) {
                $new = $fragment->importNode($node, true);
                $root->appendChild($new);
            } else {
                break;
            }
        }

        return $fragment->saveHTML();
    }

    /**
     * Заменяет ссылки вида [url](url) на ссылки вида [url](title).
     * Это нужно для того, чтобы парсер markdown-to-HTML не преобразовывал инлайновые ссылки, что приводит
     * к появлению невалидного HTML на выходе.
     *
     * @param string $md                       Текст в markdown
     * @param bool   $replaceUrlWithPageTitles Заменить текст ссылок на тайтлы страниц?
     *
     * @return string
     */
    public function removeInlineLinksInMarkdown($md, $replaceUrlWithPageTitles = false): string
    {
        // метаинформация, которую трогать не нужно
        $meta = ['img', 'YOUTUBE'];
        $output = $md;
        $data = [];
        preg_match_all('/\[((?:[^][]|(?R))*)\]/', $md, $data);
        $urlTexts = $data[1];
        foreach ($urlTexts as $value) {
            if (\in_array($value, $meta, true)) {
                continue;
            }

            $isUrl = preg_match('#((https?|ftp)://(\S*?\.\S*?))([\s)\[\]{},;"\':<]|\.\s|$)#i', $value);
            if ($isUrl) {
                $pageTitle = ($replaceUrlWithPageTitles) ? $this->getPageTitle($value) : 'ССЫЛКА';
            } else {
                $pageTitle = $value;
            }

            $title = ($pageTitle) ?: 'ССЫЛКА';

            $output = str_replace(sprintf('[%s]', $value), sprintf('[%s]', $title), $output);
        }

        return $output;
    }

    /**
     * Получить title страницы по URL.
     *
     * @param string $url URL страницы
     *
     * @return string
     */
    protected function getPageTitle($url): string
    {
        try {
            $page = new Crawler($this->buzzBrowser->get($url)->getBody());
        } catch (\Exception $e) {
            return '';
        }

        $titleNode = $page->filter('title');
        $title = ($titleNode->count() > 0) ? $titleNode->text() : false;

        return $title;
    }

    /**
     * Обработка ссылок. Внутренние (на ресурсы Ирмага) остаются как есть, внешние "портятся".
     *
     * @param string $text                  Текст
     * @param bool   $replaceInnerWithTitle Заменять текст внутренних на page title?
     * @param bool   $allowVideo            Разрешить встраивание видео?
     *
     * @return string
     */
    public function sanitizeLinks($text, $replaceInnerWithTitle = true, $allowVideo = true): string
    {
        if (!empty($text)) {
            $doc = new \DOMDocument('1.0', 'utf-8');
            $doc->loadHTML(mb_convert_encoding(str_replace('&', '&amp;', $text), 'html-entities', 'utf-8'), LIBXML_HTML_NODEFDTD | LIBXML_HTML_NOIMPLIED);

            $crawler = new Crawler($doc);
            // спецразметку для youtube-роликов не трогаем
            $links = ($allowVideo) ? $crawler->filterXPath('//a[text()!="YOUTUBE"]') : $crawler->filterXPath('//a');

            foreach ($links as &$link) {
                $urlText = $link->nodeValue;
                $urlHref = $link->getAttribute('href');

                $isProtocolPointed = (bool) preg_match('/^http/', $urlHref);
                $isInnerResource = (bool) preg_match(sprintf('/%s/', $this->innerDomain), $urlHref);
                $isTextUrl = (bool) preg_match('/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/', $urlText);

                if (false === $isProtocolPointed) {
                    $prefix = $isInnerResource ? 'https://' : 'http://';
                    $urlHref = $prefix.$urlHref;
                    $link->setAttribute('href', $urlHref);
                }

                // если ссылка на внешний ресурс
                if (!$isInnerResource) {
                    $text = $doc->createElement('span', $urlText);
                    $span = $doc->createElement('span', sprintf(' (%s) ', $urlHref));
                    $link->parentNode->insertBefore($text, $link);
                    $link->parentNode->insertBefore($span, $link);
                    $link->parentNode->removeChild($link);
                } else {
                    $sanitizedLinkText = ($isTextUrl && $replaceInnerWithTitle) ? $this->getPageTitle($urlText) : $urlText;
                    $link->nodeValue = htmlspecialchars($sanitizedLinkText);
                }
            }

            return $doc->saveHTML();
        }

        return '';
    }

    /**
     * Вставить блок с видео.
     *
     * @param string $text Текст
     *
     * @return string
     */
    public function injectVideo($text): string
    {
        if (!empty($text)) {
            $doc = new \DOMDocument('1.0', 'utf-8');
            $doc->loadHTML(mb_convert_encoding(str_replace('&', '&amp;', $text), 'html-entities', 'utf-8'), LIBXML_HTML_NODEFDTD | LIBXML_HTML_NOIMPLIED);

            $crawler = new Crawler($doc);
            $videos = $crawler->filterXPath('//a[text()="YOUTUBE"]');

            foreach ($videos as $video) {
                $videoLink = $this->getYoutubeLinkForEmbed($video->getAttribute('href'));
                if ('' === $videoLink) {
                    continue;
                }

                $template = $this->twig->loadTemplate('@IrmagAttachment/Attachments/attached-video.html.twig');
                $videoCode = $template->renderBlock('code', [
                    'youtube_link' => $videoLink,
                ]);
                $fragment = new \DOMDocument('1.0', 'utf-8');
                $fragment->loadHTML(mb_convert_encoding($videoCode, 'html-entities', 'utf-8'), LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

                $newNode = $fragment->getElementsByTagName('div')->item(0);
                $iframe = $doc->importNode($newNode, true);

                if ('p' === $video->parentNode->nodeName) {
                    $video->parentNode->parentNode->replaceChild($iframe, $video->parentNode);
                } else {
                    $video->parentNode->replaceChild($iframe, $video);
                }
            }

            return $doc->saveHTML();
        }

        return '';
    }

    /**
     * Заменить дефолтные alt-атрибуты на другую строку.
     *
     * @param string $md            Markdown text
     * @param string $replaceString
     *
     * @return string
     */
    public function changeImageAltInMarkdown($md, string $replaceString): string
    {
        $pattern = '/!\[(img|Введите описание изображения)\]\(([^)]+)\)/';
        $replaceRule = sprintf('![%s]($2)', $replaceString);
        $newText = preg_replace($pattern, $replaceRule, $md);

        return $newText;
    }
}
