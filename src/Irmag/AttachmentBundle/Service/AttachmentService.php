<?php

namespace Irmag\AttachmentBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\HttpFoundation\File\File;
use Twig_Environment as Twig;
use Irmag\SiteBundle\Entity\File as IrmagFile;
use Irmag\ProfileBundle\Entity\User;
use Irmag\AttachmentBundle\Exception\IrmagAttachmentException;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachable;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachmentPoint;

class AttachmentService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Twig
     */
    private $twig;

    /**
     * @var TextTools
     */
    private $textTools;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param EntityManagerInterface $em
     * @param Twig                   $twig
     * @param TextTools              $textTools
     * @param TokenStorageInterface  $tokenStorage
     */
    public function __construct(
        EntityManagerInterface $em,
        Twig $twig,
        TextTools $textTools,
        TokenStorageInterface $tokenStorage
    ) {
        $this->em = $em;
        $this->twig = $twig;
        $this->textTools = $textTools;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * Сохраняет загружаемый файл в БД.
     *
     * @param File            $file   Файл для загрузки
     * @param IrmagAttachable $attach Прикрепляемый объект
     *
     * @return array
     */
    public function upload(File $file, IrmagAttachable $attach): array
    {
        $mimeType = $file->getMimeType();

        if (!\in_array($mimeType, ['image/jpeg', 'image/png'], true)) {
            throw new UnsupportedMediaTypeHttpException('Incorrect mime-type provided');
        }

        $fileToUpload = new IrmagFile();
        $fileToUpload->setFile($file);
        $this->em->persist($fileToUpload);

        $attach->setFile($fileToUpload);
        $attach->setIsActive(true);
        $attach->setUserAttachedBy($this->getUserOrThrowException());
        $this->em->persist($attach);
        $this->em->flush();
        $imageCode = $this->twig->render('@IrmagAttachment/Thumbnails/attached-image-thumbnail.html.twig', [
            'image_path' => $fileToUpload->getWebPath(),
        ]);

        return [
            'irmag_file_id' => $fileToUpload->getId(),
            'attach_id' => $attach->getId(),
            'image_code' => $imageCode,
        ];
    }

    /**
     * Удаляет прикрепление и загруженный файл.
     *
     * @param IrmagAttachable $attach Прикреплённый объект
     */
    public function remove(IrmagAttachable $attach): void
    {
        $file = $attach->getFile();
        $this->em->remove($attach);
        $this->em->remove($file);
        $this->em->flush();
    }

    /**
     * Перепривязывает аттачи к объекту в соответствии с разметкой в тексте.
     *
     * @param IrmagAttachmentPoint $object              Объект, к которому прикрепляем
     * @param string               $attachmentClassName Класс аттача
     */
    public function recombineAttachments(IrmagAttachmentPoint $object, $attachmentClassName): void
    {
        $attachmentsInText = $this->textTools->getAttachedToText($object->getText(), true);
        $possibleAttachments = $this->em->getRepository($attachmentClassName)
            ->getAllowedAttachments($object->getUser(), $object);

        // рвём все предыдущие связи с объектами
        foreach ($possibleAttachments as $attach) {
            $attach->setObject(null);
            $this->em->persist($attach);
        }

        // устанавливаем связи только для тех прикреплений, которые явно упомянуты в тексте
        $attachments = $this->em->getRepository($attachmentClassName)->findBy(['id' => $attachmentsInText]);
        foreach ($attachments as $attach) {
            $attach->setObject($object);
            $this->em->persist($attach);
        }

        $this->em->flush();
    }

    /**
     * Проверяет авторизацию пользователя.
     *
     * @throws \Exception
     *
     * @return User $user
     */
    protected function getUserOrThrowException(): User
    {
        $token = $this->tokenStorage->getToken();

        if (null !== $token && $token->getUser() instanceof User) {
            return $token->getUser();
        }

        throw new IrmagAttachmentException('Authentication not found!');
    }
}
