<?php

namespace Irmag\AttachmentBundle\Interfaces;

use Doctrine\Common\Collections\Collection;
use Irmag\ProfileBundle\Entity\User;

interface IrmagAttachmentPoint
{
    /**
     * Get text with markup.
     *
     * @return string
     */
    public function getText(): ?string;

    /**
     * Set markup text.
     *
     * @param string $text
     *
     * @return self
     */
    public function setText(string $text);

    /**
     * Get text html.
     *
     * @return string
     */
    public function getTextHtml(): ?string;

    /**
     * Set text html.
     *
     * @param string|null $textHtml
     *
     * @return self
     */
    public function setTextHtml(?string $textHtml);

    /**
     * Add object attachment.
     *
     * @param IrmagAttachable $attachment
     *
     * @return self
     */
    public function addAttachment(IrmagAttachable $attachment);

    /**
     * Get object attachments.
     *
     * @return Collection
     */
    public function getAttachments(): Collection;

    /**
     * Remove object attachment.
     *
     * @param IrmagAttachable $attachment
     *
     * @return self
     */
    public function removeAttachment(IrmagAttachable $attachment);

    /**
     * Get attachment class name.
     *
     * @return string
     */
    public function getAttachmentClassName(): string;

    /**
     * Get object class name.
     * Use it to get class, because standard "get_class()" returns classname with proxy namespaces.
     *
     * @return string
     */
    public function getClassName(): string;

    /**
     * @return User|null
     */
    public function getUser(): ?User;
}
