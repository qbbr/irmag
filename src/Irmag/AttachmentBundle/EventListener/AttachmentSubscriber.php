<?php

namespace Irmag\AttachmentBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Irmag\AttachmentBundle\Controller\AttachmentController;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachmentPoint;
use Irmag\AttachmentBundle\Service\AttachmentService;
use Irmag\AttachmentBundle\Service\TextTools;
use Irmag\CoreBundle\Utils\Markdown;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;

class AttachmentSubscriber implements EventSubscriber
{
    /**
     * @var TextTools
     */
    private $textTools;

    /**
     * @var AttachmentService
     */
    private $attachmentService;

    /**
     * @var Markdown
     */
    private $markdown;

    /**
     * @var array
     */
    private $filtersMap = [];

    /**
     * @param TextTools         $textTools
     * @param AttachmentService $attachmentService
     * @param Markdown          $markdown
     */
    public function __construct(
        TextTools $textTools,
        AttachmentService $attachmentService,
        Markdown $markdown
    ) {
        $this->textTools = $textTools;
        $this->attachmentService = $attachmentService;
        $this->markdown = $markdown;

        $this->filtersMap = [
            BlogPost::class => AttachmentController::FILTER_BLOG_POST,
            ForumPost::class => AttachmentController::FILTER_FORUM_POST,
            ThreadComment::class => AttachmentController::FILTER_THREAD_COMMENT,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->update($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->update($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    private function update(LifecycleEventArgs $args): void
    {
        $this->updateHtmlCache($args);
        $this->recombineAttachments($args);
    }

    /**
     * Обновляет html-представление объекта.
     *
     * @param LifecycleEventArgs $args
     */
    private function updateHtmlCache(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof IrmagAttachmentPoint) {
            $em = $args->getObjectManager();
            $entityAttachmentClass = $entity->getAttachmentClassName();
            $entityClass = $entity->getClassName();
            $text = $this->textTools->removeInlineLinksInMarkdown($entity->getText());
            $html = $this->markdown->toHtml($text);
            $allowedAttachments = $em->getRepository($entityAttachmentClass)->getAllowedAttachments($entity->getUser(), $entity);

            $html = html_entity_decode(
                $this->textTools->replaceMarksWithRealAttachments(
                    $html,
                    $allowedAttachments,
                    $this->filtersMap[$entityClass]
                )
            );

            // кастомный код
            if ($entity instanceof BlogPost) {
                $htmlPreview = html_entity_decode($this->textTools->cropTextToPreview($html));
                $htmlPreview = html_entity_decode($this->textTools->injectVideo($htmlPreview));
                $htmlPreview = html_entity_decode($this->textTools->wrapImagesToSliderContainer($htmlPreview));
                $entity->setPreviewTextHtml($htmlPreview);

                $html = html_entity_decode($this->textTools->injectVideo($html));
                $html = $this->textTools->wrapImagesToSliderContainer($html);
                $html = html_entity_decode($this->textTools->sanitizeLinks($html));
            } elseif (\in_array('ROLE_ALLOW_ADVANCED_COMMENTS', $entity->getUser()->getRoles(), true)) {
                $html = html_entity_decode($this->textTools->injectVideo($html));
            } else {
                $html = html_entity_decode($this->textTools->sanitizeLinks($html, true, false));
            }

            $entity->setTextHtml(html_entity_decode($html));

            $em->persist($entity);
            $em->flush();
        }
    }

    /**
     * Перепривязывает аттачи к объекту в соответствии с разметкой в тексте.
     *
     * @param LifecycleEventArgs $args
     */
    private function recombineAttachments(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof IrmagAttachmentPoint) {
            $em = $args->getObjectManager();
            $entityAttachmentClass = $entity->getAttachmentClassName();

            $this->attachmentService->recombineAttachments($entity, $entityAttachmentClass);

            // кастомный код для блогпостов - установка основного аттача (показывается в превью)
            if ($entity instanceof BlogPost) {
                $attachedList = $this->textTools->getAttachedToText($entity->getText(), true);

                if (!empty($attachedList)) {
                    $attachments = $em->getRepository($entityAttachmentClass)->findBy(['id' => $attachedList]);

                    if (!empty($attachments)) {
                        foreach ($attachments as $attach) {
                            $attach->setIsPrimary(false);
                            $em->persist($attach);
                        }

                        foreach ($attachedList as $att) {
                            $firstAttachment = $em->getRepository($entityAttachmentClass)->find($att);
                            if (!$firstAttachment) {
                                continue;
                            }

                            $firstAttachment->setIsPrimary(true);
                            $em->persist($firstAttachment);

                            break;
                        }

                        $em->flush();
                    }
                }
            }
        }
    }
}
