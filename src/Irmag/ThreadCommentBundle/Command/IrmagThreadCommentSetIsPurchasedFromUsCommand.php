<?php

namespace Irmag\ThreadCommentBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class IrmagThreadCommentSetIsPurchasedFromUsCommand extends Command
{
    protected static $defaultName = 'irmag:thread_comment:set_is_purchased_from_us';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Set is purchased from us')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $sql = <<<SQL
            UPDATE threads_comments tc
            SET is_purchased_from_us = true
            FROM threads_comments tc2
            LEFT JOIN threads t on tc2.thread_id = t.id
            WHERE t.owner_id IN (
                SELECT oe.element_id
                FROM orders_elements oe
                LEFT JOIN orders o ON oe.order_id = o.id
                WHERE o.user_id = tc2.user_id
            )
            AND tc.id = tc2.id
SQL;

        $conn = $this->em->getConnection();
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $count = $stmt->rowCount();

        $output->writeln(sprintf('<info>Done. Count: %d.</info>', $count));
    }
}
