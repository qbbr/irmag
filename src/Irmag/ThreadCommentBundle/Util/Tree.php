<?php

namespace Irmag\ThreadCommentBundle\Util;

class Tree
{
    /**
     * Строит дерево комментариев.
     *
     * @param array $comments Комментарии
     * @param int   $parentId Ид родителя
     *
     * @return array Дерево комментариев
     */
    public static function build($comments, $parentId = null): array
    {
        $branch = [];

        foreach ($comments as $comment) {
            $commentParentId = $comment->getParent() ? $comment->getParent()->getId() : null;

            if ($commentParentId === $parentId) {
                $children = static::build($comments, $comment->getId());

                if ($children) {
                    $comment->children = $children;
                }

                $branch[] = $comment;
            }
        }

        return $branch;
    }
}
