<?php

namespace Irmag\ThreadCommentBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\AttachmentBundle\Service\TextTools;
use Irmag\ProfileBundle\Service\UserTrait;
use Irmag\ProfileBundle\Entity\Group;
use Irmag\ThreadCommentBundle\Entity\Thread;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;
use Irmag\ThreadCommentBundle\Entity\ThreadCommentVote;
use Irmag\ThreadCommentBundle\Entity\ThreadOwnerInterface;
use Irmag\ThreadCommentBundle\Mailer\ThreadCommentMailer;

class ThreadManager
{
    use UserTrait;

    const SET_THREAD_METHOD = 'setThread';
    const GET_THREAD_METHOD = 'getThread';

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var TextTools
     */
    protected $textTools;

    /**
     * @var ThreadCommentMailer
     */
    protected $threadCommentMailer;

    /**
     * @var Thread
     */
    protected $thread;

    /**
     * @var ThreadComment
     */
    protected $threadComment;

    /**
     * @var array
     */
    protected $config;

    /**
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     * @param TextTools              $textTools
     * @param ThreadCommentMailer    $threadCommentMailer
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        TextTools $textTools,
        ThreadCommentMailer $threadCommentMailer
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->textTools = $textTools;
        $this->threadCommentMailer = $threadCommentMailer;
    }

    /**
     * @param array $config ThreadComment config
     */
    public function setConfig(array $config): void
    {
        $this->config = $config;
    }

    /**
     * @param Thread $thread
     */
    protected function applyConfig(Thread $thread): void
    {
        $thread->setIsCommentable($this->config['is_commentable']);
        $thread->setIsScorable($this->config['is_scorable']);
        $thread->setIsRatable($this->config['is_ratable']);
    }

    /**
     * @param ThreadOwnerInterface $entity Relationship Entity
     *
     * @return Thread
     */
    public function getThreadOrCreateNew(ThreadOwnerInterface $entity): Thread
    {
        if (null === $this->thread) {
            $this->thread = $entity->{static::GET_THREAD_METHOD}();

            if (!$this->thread) {
                $this->thread = new Thread();
                $this->thread->setOwnerId($entity->getId());
                $entity->{static::SET_THREAD_METHOD}($this->thread);
            }

            $this->applyConfig($this->thread);
        }

        return $this->thread;
    }

    /**
     * Создать коммент.
     *
     * @param Thread $thread
     * @param array  $commentOptions
     *
     * @return ThreadComment
     */
    public function createThreadComment(Thread $thread, array $commentOptions): ThreadComment
    {
        $this->threadComment = new ThreadComment();
        $this->threadComment->setThread($thread);
        $this->threadComment->setUser($this->getUser());
        $this->threadComment->setText($commentOptions['text']);

        if (!empty($commentOptions['parent'])) {
            $parentComment = $this->em->getRepository(ThreadComment::class)->find($commentOptions['parent']);
            $this->threadComment->setParent($parentComment);
        }

        if (!empty($commentOptions['rating']) && $thread->getIsRatable()) {
            $this->threadComment->setRating($commentOptions['rating']);
        }

        $this->em->persist($this->threadComment);

        return $this->threadComment;
    }

    /**
     * @param Thread $thread
     *
     * @return ThreadManager
     */
    public function setThread(Thread $thread): self
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * @param ThreadComment $threadComment
     *
     * @return ThreadManager
     */
    public function setThreadComment(ThreadComment $threadComment): self
    {
        $this->threadComment = $threadComment;

        return $this;
    }

    /**
     * @return ThreadComment
     */
    public function getThreadComment(): ThreadComment
    {
        return $this->threadComment;
    }

    /**
     * Обновляет количество комментариев.
     *
     * @return ThreadManager
     */
    public function updateCommentsTotalCount(): self
    {
        $this->thread->setCommentsTotalCount($this->em->getRepository(ThreadComment::class)->getThreadCommentsTotalCountByThread($this->thread));

        return $this;
    }

    /**
     * Обновляет среднее значение рейтинга.
     *
     * @return ThreadManager
     */
    public function updateAvgRating(): self
    {
        $this->thread->setAvgRating($this->em->getRepository(ThreadComment::class)->getAvgRatingByThread($this->thread));

        return $this;
    }

    /**
     * @return ThreadComment[]|null
     */
    public function getThreadComments()
    {
        return $this->thread->getId()
            ? $this->em->getRepository(ThreadComment::class)->getThreadCommentsByThread($this->thread)
            : null;
    }

    /**
     * Возвращает массив с голосами пользователя.
     *
     * @return array
     */
    public function getUserVotes(): array
    {
        $votes = [];

        if ($this->getUser() && $this->thread->getId()) {
            $votes = $this->em->getRepository(ThreadCommentVote::class)->getVotesByThreadAndUser($this->thread, $this->getUser());
        }

        return $votes;
    }

    /**
     * Отправляет сообщение модераторам.
     */
    public function sendEmailToModerators(): void
    {
        $moderatorGroup = $this->em->getRepository(Group::class)->findOneBy(['shortname' => 'thread_comment_moderators']);

        if (!$moderatorGroup) {
            return;
        }

        /** @var \Irmag\ProfileBundle\Entity\User $user */
        foreach ($moderatorGroup->getUsers() as $user) {
            if ($user->getEmail()) {
                $emails[] = $user->getEmail();
            }
        }

        if (!empty($emails)) {
            $this->threadCommentMailer->sendNewCommentToModeratorsMessage($emails, ['comment' => $this->threadComment]);
        }
    }

    /**
     * Отправляет сообщение автору откомментированного объекта (кроме случаев, когда ему самому и ответили).
     */
    public function sendEmailToAuthor(): void
    {
        $commentedEntity = $this->em->getRepository($this->thread->getOwnerFQCN())->find($this->thread->getOwnerId());

        if (!$commentedEntity) {
            return;
        }

        $author = $commentedEntity->getUser();
        $parentCommentAuthor = $this->threadComment->getParent() ? $this->threadComment->getParent()->getUser() : null;

        if (
            !empty($author->getEmail())
            && $author !== $this->threadComment->getUser()
            && $author !== $parentCommentAuthor
        ) {
            $this->threadCommentMailer->sendNewCommentToAuthorMessage($author->getEmail(), [
                'comment' => $this->threadComment,
                'post' => $commentedEntity,
            ]);
        }
    }

    /**
     * Сохраняет комментарий.
     */
    public function save(): void
    {
        $this->em->persist($this->thread);
        $this->em->flush();
    }
}
