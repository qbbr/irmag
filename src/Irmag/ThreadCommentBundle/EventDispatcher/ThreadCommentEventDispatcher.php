<?php

namespace Irmag\ThreadCommentBundle\EventDispatcher;

use Irmag\ThreadCommentBundle\Entity\ThreadComment;
use Irmag\ThreadCommentBundle\Event\ThreadCommentEvent;
use Irmag\ThreadCommentBundle\IrmagThreadCommentEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class ThreadCommentEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param ThreadComment $comment
     */
    public function dispatchNewThreadCommentCreated(ThreadComment $comment)
    {
        $this->eventDispatcher->dispatch(IrmagThreadCommentEvents::THREAD_COMMENT_NEW_COMMENT_CREATED, $this->getThreadCommentEvent($comment));
    }

    /**
     * @param ThreadComment $comment
     *
     * @return ThreadCommentEvent
     */
    private function getThreadCommentEvent(ThreadComment $comment)
    {
        return new ThreadCommentEvent($comment);
    }
}
