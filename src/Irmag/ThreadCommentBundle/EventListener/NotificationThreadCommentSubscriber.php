<?php

namespace Irmag\ThreadCommentBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Irmag\ThreadCommentBundle\Event\ThreadCommentEvent;
use Irmag\ThreadCommentBundle\Manager\ThreadManager;
use Irmag\ThreadCommentBundle\IrmagThreadCommentEvents;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;
use Irmag\ProfileBundle\NotificationEvents;
use Irmag\ProfileBundle\Service\UserNotificationEventManager\UserNotificationEventManager;
use Irmag\ProfileBundle\Service\UserNotificationQueueManager\UserNotificationQueueManager;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\Element;
use Irmag\BlogBundle\Entity\BlogPost;

class NotificationThreadCommentSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ThreadManager
     */
    private $threadManager;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var UserNotificationEventManager
     */
    private $userNotificationEventManager;

    /**
     * @var UserNotificationQueueManager
     */
    private $userNotificationQueueManager;

    /**
     * @param EntityManagerInterface       $em
     * @param ThreadManager                $manager
     * @param UrlGeneratorInterface        $router
     * @param UserNotificationEventManager $userNotificationEventManager
     * @param UserNotificationQueueManager $userNotificationQueueManager
     */
    public function __construct(
        EntityManagerInterface $em,
        ThreadManager $manager,
        UrlGeneratorInterface $router,
        UserNotificationEventManager $userNotificationEventManager,
        UserNotificationQueueManager $userNotificationQueueManager
    ) {
        $this->em = $em;
        $this->threadManager = $manager;
        $this->router = $router;
        $this->userNotificationEventManager = $userNotificationEventManager;
        $this->userNotificationQueueManager = $userNotificationQueueManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagThreadCommentEvents::THREAD_COMMENT_NEW_COMMENT_CREATED => 'sendNewCommentEmails',
        ];
    }

    /**
     * @param ThreadCommentEvent $event
     */
    public function sendNewCommentEmails(ThreadCommentEvent $event): void
    {
        $comment = $event->getThreadComment();
        $thread = $comment->getThread();
        $parent = $comment->getParent();
        $this->threadManager->sendEmailToModerators();

        // replies
        if ($parent instanceof ThreadComment && $parent->getUser() !== $comment->getUser()) {
            if (\in_array($thread->getOwnerFQCN(), [Element::class, Action::class, Element::class], true)) { // site
                $notificationEvent = $this->userNotificationEventManager->getEventByName(NotificationEvents::SITE_COMMENT_REPLY);
            } elseif (BlogPost::class === $thread->getOwnerFQCN()) { // blog
                $notificationEvent = $this->userNotificationEventManager->getEventByName(NotificationEvents::BLOG_COMMENT_REPLY);
            }

            if (isset($notificationEvent)) {
                $parameters = $thread->getOwnerRoute()['parameters'];
                $parameters['_fragment'] = 'jmp-to-comment-'.$comment->getId();
                $url = $this->router->generate($thread->getOwnerRoute()['name'], $parameters, UrlGeneratorInterface::ABSOLUTE_URL);
                $this->userNotificationQueueManager->create($notificationEvent, $parent->getUser(), sprintf('от %s', $comment->getUser()->getName()), $url);
            }
        }

        // custom for BlogPost
        if (BlogPost::class === $thread->getOwnerFQCN()) {
            $this->threadManager->sendEmailToAuthor();
        }
    }
}
