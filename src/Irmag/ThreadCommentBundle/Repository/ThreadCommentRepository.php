<?php

namespace Irmag\ThreadCommentBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Irmag\CoreBundle\Repository\Traits\GetTotalTrait;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ThreadCommentBundle\Entity\Thread;

class ThreadCommentRepository extends \Doctrine\ORM\EntityRepository
{
    use GetTotalTrait;

    public function getCount(User $user): int
    {
        return $this->createQueryBuilder('tc')
            ->select('COUNT(tc)')
            ->where('tc.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Возвращает комментарии в ветке.
     *
     * @param Thread $thread
     *
     * @return \Irmag\ThreadCommentBundle\Entity\ThreadComment[]
     */
    public function getThreadCommentsByThread(Thread $thread)
    {
        return $this->getQbByThread($thread)
            ->addOrderBy('tc.id', 'ASC')
            ->addOrderBy('tc.parent', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * Возвращает кол-во комментариев в ветке.
     *
     * @param Thread $thread
     *
     * @return int
     */
    public function getThreadCommentsTotalCountByThread(Thread $thread): int
    {
        return $this->getQbByThread($thread)
            ->select('COUNT(tc.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Возвращает среднее значение оценок.
     *
     * @param Thread $thread
     *
     * @return float
     */
    public function getAvgRatingByThread(Thread $thread): float
    {
        return $this->getQbByThread($thread)
            ->select('COALESCE(AVG(tc.rating), 0)')
            ->andWhere('tc.rating IS NOT NULL')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param Thread $thread
     *
     * @return QueryBuilder
     */
    protected function getQbByThread(Thread $thread): QueryBuilder
    {
        return $this->createQueryBuilder('tc')
            ->where('tc.thread = :thread')
            ->setParameter('thread', $thread);
    }
}
