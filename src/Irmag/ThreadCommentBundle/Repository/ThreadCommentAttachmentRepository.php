<?php

namespace Irmag\ThreadCommentBundle\Repository;

use Symfony\Component\Security\Core\User\UserInterface;
use Irmag\AttachmentBundle\Traits\AttachmentsRepositoryTrait;

class ThreadCommentAttachmentRepository extends \Doctrine\ORM\EntityRepository
{
    use AttachmentsRepositoryTrait;

    /**
     * Возвращает картинки, которые были залиты пользователем для комментариев, но не были прикреплены.
     *
     * @param UserInterface $user
     *
     * @return array
     */
    public function getUnattachedPicsByUser(UserInterface $user)
    {
        return $this->createQueryBuilder('tca')
            ->select('tca', 'f')
            ->join('tca.file', 'f')
            ->where('tca.userAttachedBy = :user')
            ->andWhere('tca.object IS NULL')
            ->setParameter('user', $user)
            ->getQuery()
            ->getResult();
    }
}
