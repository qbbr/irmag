<?php

namespace Irmag\ThreadCommentBundle;

final class IrmagThreadCommentEvents
{
    /**
     * Это событие вызывается при создании нового комментария.
     */
    const THREAD_COMMENT_NEW_COMMENT_CREATED = 'irmag.thread.comment.created';
}
