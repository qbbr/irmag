<?php

namespace Irmag\ThreadCommentBundle\Entity;

interface ThreadOwnerInterface
{
    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int;

    /**
     * Set thread.
     *
     * @param Thread|null $thread
     *
     * @return self
     */
    public function setThread(?Thread $thread);

    /**
     * Get thread.
     *
     * @return Thread|null
     */
    public function getThread(): ?Thread;

    /**
     * Set ownerRoute.
     */
    public function setThreadOwnerRoute(): void;
}
