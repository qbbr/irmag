<?php

namespace Irmag\ThreadCommentBundle\Entity\Traits;

use Irmag\ThreadCommentBundle\Entity\Thread;

trait ThreadTrait
{
    /**
     * @var Thread
     *
     * @ORM\ManyToOne(targetEntity="Irmag\ThreadCommentBundle\Entity\Thread")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $thread;

    /**
     * Set thread.
     *
     * @param Thread|null $thread
     *
     * @return $this
     */
    public function setThread(?Thread $thread): self
    {
        $this->thread = $thread;
        $thread->setOwnerFQCN(self::class);
        $thread->setOwnerId($this->getId());
        $this->setThreadOwnerRoute();

        return $this;
    }

    /**
     * Get thread.
     *
     * @return Thread|null
     */
    public function getThread(): ?Thread
    {
        return $this->thread;
    }
}
