<?php

namespace Irmag\ThreadCommentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachable;
use Symfony\Component\Validator\Constraints as Assert;
use Irmag\ProfileBundle\Entity\User;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachmentPoint;

/**
 * @ORM\Table(name="threads_comments")
 * @ORM\Entity(repositoryClass="Irmag\ThreadCommentBundle\Repository\ThreadCommentRepository")
 */
class ThreadComment implements IrmagAttachmentPoint
{
    use TimestampableEntity;

    const ATTACHMENT_CLASS_NAME = ThreadCommentAttachment::class;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var ThreadComment
     *
     * @ORM\ManyToOne(targetEntity="ThreadComment")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $textHtml;

    /**
     * @var Thread
     *
     * @ORM\ManyToOne(targetEntity="Thread", inversedBy="comments")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $thread;

    /**
     * @var int
     *
     * Очки кармы (Like/Dislike)
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $score;

    /**
     * Рейтинг от 1 до 5.
     *
     * @var int
     *
     * @Assert\Range(min=0, max=5)
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $rating;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    /**
     * @ORM\OneToMany(targetEntity="ThreadCommentAttachment", mappedBy="object")
     */
    private $attachments;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isBonusPayed;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isPurchasedFromUs;

    public function __construct()
    {
        $this->isActive = true;
        $this->isBonusPayed = false;
        $this->isPurchasedFromUs = false;
        $this->attachments = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    /**
     * Get attachment class name.
     * XXX: custom.
     *
     * @return string
     */
    public function getAttachmentClassName(): string
    {
        return self::ATTACHMENT_CLASS_NAME;
    }

    /**
     * Get object class name.
     * Use it to get class, because standard "get_class()" returns classname with proxy namespaces.
     * XXX: custom.
     *
     * @return string
     */
    public function getClassName(): string
    {
        return __CLASS__;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getTextHtml(): ?string
    {
        return $this->textHtml;
    }

    public function setTextHtml(?string $textHtml): self
    {
        $this->textHtml = $textHtml;

        return $this;
    }

    public function getScore(): ?int
    {
        return $this->score;
    }

    public function setScore(?int $score): self
    {
        $this->score = $score;

        return $this;
    }

    public function getRating(): ?int
    {
        return $this->rating;
    }

    public function setRating(?int $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsBonusPayed(): ?bool
    {
        return $this->isBonusPayed;
    }

    public function setIsBonusPayed(bool $isBonusPayed): self
    {
        $this->isBonusPayed = $isBonusPayed;

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getThread(): ?Thread
    {
        return $this->thread;
    }

    public function setThread(?Thread $thread): self
    {
        $this->thread = $thread;

        return $this;
    }

    /**
     * @return Collection|ThreadCommentAttachment[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(IrmagAttachable $attachment): self
    {
        if (!$this->attachments->contains($attachment)) {
            $this->attachments[] = $attachment;
            $attachment->setObject($this);
        }

        return $this;
    }

    public function removeAttachment(IrmagAttachable $attachment): self
    {
        if ($this->attachments->contains($attachment)) {
            $this->attachments->removeElement($attachment);
            // set the owning side to null (unless already changed)
            if ($attachment->getObject() === $this) {
                $attachment->setObject(null);
            }
        }

        return $this;
    }

    public function getIsPurchasedFromUs(): ?bool
    {
        return $this->isPurchasedFromUs;
    }

    public function setIsPurchasedFromUs(bool $isPurchasedFromUs): self
    {
        $this->isPurchasedFromUs = $isPurchasedFromUs;

        return $this;
    }
}
