<?php

namespace Irmag\ThreadCommentBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

class IrmagThreadCommentException extends IrmagException
{
}
