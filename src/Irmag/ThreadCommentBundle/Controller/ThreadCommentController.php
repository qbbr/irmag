<?php

namespace Irmag\ThreadCommentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\AttachmentBundle\Service\AttachmentService;
use Irmag\ThreadCommentBundle\Form\Type\ThreadCommentType;
use Irmag\ThreadCommentBundle\Entity\Thread;
use Irmag\ThreadCommentBundle\Entity\ThreadComment;
use Irmag\ThreadCommentBundle\Entity\ThreadCommentVote;
use Irmag\ThreadCommentBundle\Entity\ThreadOwnerInterface;
use Irmag\ThreadCommentBundle\Entity\ThreadCommentAttachment as Attach;
use Irmag\ThreadCommentBundle\EventDispatcher\ThreadCommentEventDispatcher;
use Irmag\ThreadCommentBundle\Exception\IrmagThreadCommentException;
use Irmag\ThreadCommentBundle\Util\Tree;
use Irmag\ThreadCommentBundle\Manager\ThreadManager;

class ThreadCommentController extends Controller
{
    /**
     * @param int    $id     ThreadOwnerInterface Entity id
     * @param string $config ThreadComment config name
     *
     * @throws IrmagThreadCommentException When $entity is not commentable
     *
     * @return Response
     */
    public function indexAction(int $id, string $config)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $currentConfig = $this->getThreadCommentConfig($config);
        $createGranted = false === $this->isGranted($currentConfig['disallow_roles']['create']);

        // скрываем не нужные эл-ты
        if (false === $createGranted) {
            $currentConfig['is_commentable'] = false;
            $currentConfig['is_scorable'] = false;
        }

        $entity = $em->getRepository($currentConfig['fqcn'])->find($id);

        if (!$entity instanceof ThreadOwnerInterface) {
            throw new IrmagThreadCommentException('Entity must be implements ThreadOwnerInterface.');
        }

        $threadManager = $this->get(ThreadManager::class);
        $threadManager->setConfig($currentConfig);
        $thread = $threadManager->getThreadOrCreateNew($entity);
        $threadManager->save();

        if ($this->getUser()) {
            $unattachedPics = $this->get('doctrine.orm.default_entity_manager')->getRepository(Attach::class)->getUnattachedPicsByUser($this->getUser());
        }

        $commentForm = $this->createForm(ThreadCommentType::class);

        return $this->render($currentConfig['template'], [
            'thread' => $thread,
            'config_name' => $config,
            'unattached_pics' => $unattachedPics ?? [],
            'comment_tree' => $thread->getId() ? Tree::build($threadManager->getThreadComments()) : [],
            'comment_form' => $commentForm->createView(),
            'comment_votes' => $threadManager->getUserVotes(),
            'disallow_roles' => $currentConfig['disallow_roles'],
        ]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/ajax/thread_comment/vote/",
     *     name="irmag_thread_comment_vote",
     *     methods={"POST"},
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     *     condition="request.isXmlHttpRequest()",
     *     options={"expose": true}
     * )
     *
     * @param Request $request
     *
     * @throws IrmagThreadCommentException
     *
     * @return JsonResponse
     */
    public function voteAction(Request $request)
    {
        $score = $request->request->getInt('score');

        if (1 !== $score && -1 !== $score) {
            throw new IrmagThreadCommentException(sprintf('ThreadComment wrong vote (score: %d).', $score));
        }

        $em = $this->get('doctrine.orm.default_entity_manager');
        $commentId = $request->request->getInt('commentId');
        /** @var ThreadComment $comment */
        $comment = $em->getRepository(ThreadComment::class)->find($commentId);

        if (!$comment) {
            throw $this->createNotFoundException(sprintf('ThreadComment with id "%d" not found.', $commentId));
        }

        if (false === $comment->getThread()->getIsScorable()) {
            throw new IrmagThreadCommentException(sprintf('Thread with id "%d" does not support score.', $comment->getThread()->getId()));
        }

        $user = $this->getUser();

        if ($user === $comment->getUser()) {
            throw new IrmagThreadCommentException(sprintf('You can not vote themselves (ThreadComment id: "%d").', $commentId));
        }

        $commentVote = $em->getRepository(ThreadCommentVote::class)
            ->findOneBy(['user' => $user, 'threadComment' => $comment]);

        // нельзя голосовать 2й раз
        if ($commentVote) {
            $success = false;
        } else {
            $commentVote = new ThreadCommentVote();
            $commentVote->setUser($user);
            $commentVote->setThreadComment($comment);
            $commentVote->setScore($score);

            $comment->setScore($comment->getScore() + $score);

            $em->persist($commentVote);
            $em->persist($comment);
            $em->flush();

            $success = true;
        }

        $score = $comment->getScore();

        return new JsonResponse([
            'success' => $success,
            'score' => $score > 0 ? '+'.$score : $score,
        ]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/ajax/thread_comment/attachment/upload/",
     *     name="irmag_thread_comment_upload_pics",
     *     options={"expose": true},
     *     condition="request.isXmlHttpRequest()",
     *     methods={"POST"},
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"}
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function uploadPicAction(Request $request)
    {
        $files = $request->files->all();

        if (empty($files['irmag_thread_comment']['uploader']['uploader'][0])) {
            return new JsonResponse(['success' => false]);
        }

        $file = $files['irmag_thread_comment']['uploader']['uploader'][0];
        $attach = new Attach();
        $response = $this->get(AttachmentService::class)->upload($file, $attach);

        return new JsonResponse(['success' => true, 'response' => $response]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/ajax/thread_comment/attachment/remove/",
     *     name="irmag_thread_comment_remove_attachment",
     *     options={"expose": true},
     *     condition="request.isXmlHttpRequest()",
     *     methods={"POST"},
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removePicAction(Request $request)
    {
        $id = $request->request->getInt('id');
        $attachment = $this->get('doctrine.orm.default_entity_manager')->getRepository(Attach::class)->find($id);

        if (!$attachment) {
            throw $this->createNotFoundException(sprintf('Attachment with id "%d" not found.', $id));
        }

        $this->get(AttachmentService::class)->remove($attachment);

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     * @Route("/ajax/thread_comment/save/",
     *     name="irmag_thread_comment_save",
     *     options={"expose": true},
     *     condition="request.isXmlHttpRequest()",
     *     methods={"POST"},
     *     defaults={"_format": "json"},
     *     requirements={"_format": "json"},
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function saveCommentAction(Request $request)
    {
        $threadId = $request->request->getInt('threadId');
        $formData = [];
        parse_str($request->request->get('formData'), $formData);

        $thread = $this->get('doctrine.orm.default_entity_manager')->getRepository(Thread::class)->find($threadId);
        $threadManager = $this->get(ThreadManager::class);

        $comment = $threadManager->createThreadComment($thread, $formData['irmag_thread_comment']);
        $threadManager->setThread($thread);
        $threadManager->save();

        $this->get(ThreadCommentEventDispatcher::class)->dispatchNewThreadCommentCreated($comment);

        return new JsonResponse([
            'success' => true,
            'id' => $comment->getId(),
        ]);
    }

    /**
     * Загрузить конфиг для комментариев.
     *
     * @param string $configName
     *
     * @return mixed
     */
    private function getThreadCommentConfig(string $configName)
    {
        $configs = $this->getParameter('irmag_thread_comment.configs');

        if (empty($configs[$configName])) {
            throw new \InvalidArgumentException(
                sprintf('The parameter "irmag_thread_comment.configs.%s" must be defined.', $configName)
            );
        }

        return $configs[$configName];
    }
}
