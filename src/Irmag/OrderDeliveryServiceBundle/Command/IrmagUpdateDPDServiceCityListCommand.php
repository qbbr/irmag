<?php

namespace Irmag\OrderDeliveryServiceBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Irmag\OrderDeliveryServiceBundle\Service\DPDService\DPDDataUpdaterService;
use Irmag\OrderDeliveryServiceBundle\Service\DPDService\DPDService;
use Irmag\OrderDeliveryServiceBundle\Exception\OrderDeliveryServiceRequestFailedException;

class IrmagUpdateDPDServiceCityListCommand extends Command
{
    protected static $defaultName = 'irmag:dpd:update';

    /**
     * @var DPDService
     */
    private $dpd;

    /**
     * @var DPDDataUpdaterService
     */
    private $updater;

    /**
     * @param DPDService            $dpd
     * @param DPDDataUpdaterService $updater
     */
    public function __construct(
        DPDService $dpd,
        DPDDataUpdaterService $updater
    ) {
        $this->dpd = $dpd;
        $this->updater = $updater;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Updates city list for DPD delivery service.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $output->writeln('<info>Started</info>');

        if ($output->isVerbose()) {
            $output->writeln('');
            $output->writeln('Retrieving data from DPD API ...');
        }

        try {
            $citylist = $this->dpd->getCityList();
        } catch (OrderDeliveryServiceRequestFailedException $ex) {
            $output->writeln('');
            $output->writeln(sprintf('<error>%s</error>', $ex->getMessage()));

            return;
        }

        if ($output->isVerbose()) {
            $output->writeln(sprintf('<info>Total cities: </info>%d', \count($citylist)));
            $progress = new ProgressBar($output, \count($citylist));

            $output->writeln('');
            $output->writeln('Start data updating ...');

            $progress->start();
        }

        foreach ($citylist as $item) {
            $this->updater->updateCityItem($item);

            if (isset($progress)) {
                $progress->advance();
            }
        }

        if (isset($progress)) {
            $progress->finish();
        }

        $output->writeln('');
        $output->writeln('<info>OK.</info>');
    }
}
