<?php

namespace Irmag\OrderDeliveryServiceBundle\Form\Model;

class DeliveryCalculatorVariant
{
    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $shortname;

    /**
     * @var float
     */
    private $cost;

    /**
     * @var int
     */
    private $days;

    /**
     * @var bool
     */
    private $isCheapest;

    /**
     * @var bool
     */
    private $isSelected;

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return DeliveryCalculatorVariant
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return DeliveryCalculatorVariant
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getShortname(): string
    {
        return $this->shortname;
    }

    /**
     * @param string $shortname
     *
     * @return DeliveryCalculatorVariant
     */
    public function setShortname(string $shortname): self
    {
        $this->shortname = $shortname;

        return $this;
    }

    /**
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }

    /**
     * @param float $cost
     *
     * @return DeliveryCalculatorVariant
     */
    public function setCost(float $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * @return int
     */
    public function getDays(): int
    {
        return $this->days;
    }

    /**
     * @param int $days
     *
     * @return DeliveryCalculatorVariant
     */
    public function setDays(int $days): self
    {
        $this->days = $days;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsCheapest(): bool
    {
        return $this->isCheapest;
    }

    /**
     * @param bool $isCheapest
     *
     * @return DeliveryCalculatorVariant
     */
    public function setIsCheapest(bool $isCheapest): self
    {
        $this->isCheapest = $isCheapest;

        return $this;
    }

    /**
     * @return bool
     */
    public function getIsSelected(): bool
    {
        return $this->isSelected;
    }

    /**
     * @param bool $isSelected
     *
     * @return DeliveryCalculatorVariant
     */
    public function setIsSelected(bool $isSelected): self
    {
        $this->isSelected = $isSelected;

        return $this;
    }
}
