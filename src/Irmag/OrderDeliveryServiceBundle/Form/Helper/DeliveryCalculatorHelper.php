<?php

namespace Irmag\OrderDeliveryServiceBundle\Form\Helper;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryService;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCity;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceData;
use Irmag\OrderDeliveryServiceBundle\Form\Model\DeliveryCalculatorVariant;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceManager;

class DeliveryCalculatorHelper
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var OrderDeliveryServiceManager
     */
    private $deliveryManager;

    /**
     * @param EntityManagerInterface      $em
     * @param OrderDeliveryServiceManager $dm
     */
    public function __construct(
        EntityManagerInterface $em,
        OrderDeliveryServiceManager $dm
    ) {
        $this->em = $em;
        $this->deliveryManager = $dm;
    }

    /**
     * Получить все доступные службы доставки.
     *
     * @return array
     */
    public function getAvailableDeliveryServices(): array
    {
        return $this->em->getRepository(OrderDeliveryService::class)->findBy(['isActive' => true]) ?: [];
    }

    /**
     * Получить подсчитанные результаты по доставке.
     *
     * @return array
     */
    public function getResultsToDoor(): array
    {
        $choices = [];
        $data = $this->deliveryManager->getSelectedData();

        if (!empty($data)) {
            $results = $this->deliveryManager->getCalculatedResults();

            if (!empty($results[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_DOOR])) {
                foreach ($results[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_DOOR] as $key => $item) {
                    $choices[] = $this->getDeliveryVariant(OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_DOOR, $key, $item);
                }
            }
        }

        return $choices;
    }

    /**
     * Получить подсчитанные результаты по доставке.
     *
     * @return array
     */
    public function getResultsToTerminal(): array
    {
        $choices = [];
        $data = $this->deliveryManager->getSelectedData();

        if (!empty($data)) {
            $results = $this->deliveryManager->getCalculatedResults();

            if (!empty($results[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL])) {
                foreach ($results[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL] as $key => $item) {
                    $choices[] = $this->getDeliveryVariant(OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL, $key, $item);
                }
            }
        }

        return $choices;
    }

    /**
     * Получить выбранный город для расчета стоимости доставки.
     *
     * @return array
     */
    public function getSelectedCity(): array
    {
        $data = $this->deliveryManager->getSelectedData();

        if (empty($data['deliveryCity'])) {
            return [];
        }

        $selectedCity = $this->em->getRepository(OrderDeliveryServiceCity::class)->find($data['deliveryCity']);

        return [
            'cityId' => $selectedCity->getId(),
            'cityName' => $selectedCity->getFullName(),
        ];
    }

    /**
     * Формирует DeliveryCalculatorVariant из массива.
     *
     * @param string $type      Тип доставки
     * @param string $shortname Код варианта доставки
     * @param array  $data      Массив с данными
     *
     * @return DeliveryCalculatorVariant
     */
    private function getDeliveryVariant(string $type, string $shortname, array $data): DeliveryCalculatorVariant
    {
        $variant = new DeliveryCalculatorVariant();

        $variant
            ->setType($type)
            ->setName($data['serviceName'])
            ->setShortname($shortname)
            ->setCost($data['cost'])
            ->setDays($data['days'])
            ->setIsCheapest($data['isCheapest'])
            ->setIsSelected($data['isSelected']);

        return $variant;
    }
}
