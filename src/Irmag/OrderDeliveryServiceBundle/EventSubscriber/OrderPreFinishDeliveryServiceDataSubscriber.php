<?php

namespace Irmag\OrderDeliveryServiceBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\SiteBundle\Event\OrderEvent;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceManager;

class OrderPreFinishDeliveryServiceDataSubscriber implements EventSubscriberInterface
{
    /**
     * @var OrderDeliveryServiceManager
     */
    private $manager;

    /**
     * @param OrderDeliveryServiceManager $manager
     */
    public function __construct(OrderDeliveryServiceManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::ORDER_PRE_FINISH => 'onOrderPreFinish',
        ];
    }

    /**
     * @param OrderEvent $event
     */
    public function onOrderPreFinish(OrderEvent $event): void
    {
        $order = $event->getOrder();

        if ('transport_company' !== $order->getDeliveryMethod()->getShortname()) {
            return;
        }

        $this->manager->setOrderDeliveryServiceData($order);
    }
}
