<?php

namespace Irmag\OrderDeliveryServiceBundle\RequestContract;

use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCity;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCountry;

interface SelfDeliveryTerminalRequestParameterInterface
{
    /**
     * Get country code.
     *
     * @return OrderDeliveryServiceCountry
     */
    public function getCountry(): OrderDeliveryServiceCountry;

    /**
     * Set country code.
     *
     * @param OrderDeliveryServiceCountry $country
     *
     * @return SelfDeliveryTerminalRequestParameterInterface
     */
    public function setCountry(OrderDeliveryServiceCountry $country);

    /**
     * Get city.
     *
     * @return OrderDeliveryServiceCity
     */
    public function getCity(): OrderDeliveryServiceCity;

    /**
     * Set city.
     *
     * @param OrderDeliveryServiceCity $city
     *
     * @return SelfDeliveryTerminalRequestParameterInterface
     */
    public function setCity(OrderDeliveryServiceCity $city);
}
