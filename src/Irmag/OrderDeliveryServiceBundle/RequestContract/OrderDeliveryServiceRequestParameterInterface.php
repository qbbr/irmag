<?php

namespace Irmag\OrderDeliveryServiceBundle\RequestContract;

interface OrderDeliveryServiceRequestParameterInterface
{
    /**
     * Transform request object to array.
     *
     * @return array
     */
    public function toArray(): array;
}
