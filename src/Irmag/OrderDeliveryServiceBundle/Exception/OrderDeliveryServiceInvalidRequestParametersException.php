<?php

namespace Irmag\OrderDeliveryServiceBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

/**
 * {@inheritdoc}
 */
class OrderDeliveryServiceInvalidRequestParametersException extends IrmagException
{
}
