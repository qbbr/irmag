<?php

namespace Irmag\OrderDeliveryServiceBundle\Service\DPDService;

use Symfony\Component\Validator\Validator\ValidatorInterface;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Naming\IdenticalPropertyNamingStrategy;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\OrderDeliveryServiceBundle\Service\DeliveryServiceInterface;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceData;
use Irmag\OrderDeliveryServiceBundle\Exception\OrderDeliveryServiceNotFoundException;
use Irmag\OrderDeliveryServiceBundle\Exception\OrderDeliveryServiceUnavailableException;
use Irmag\OrderDeliveryServiceBundle\Exception\OrderDeliveryServiceRequestFailedException;
use Irmag\OrderDeliveryServiceBundle\Exception\OrderDeliveryServiceInvalidRequestParametersException;
use Irmag\OrderDeliveryServiceBundle\RequestContract\ServiceCostRequestParameterInterface;
use Irmag\OrderDeliveryServiceBundle\RequestContract\OrderDeliveryServiceRequestParameterInterface;
use Irmag\OrderDeliveryServiceBundle\RequestContract\SelfDeliveryTerminalRequestParameterInterface;
use Irmag\OrderDeliveryServiceBundle\Service\DPDService\RequestParameter\ServiceCostParameter;
use Irmag\OrderDeliveryServiceBundle\Service\DPDService\RequestParameter\ParcelShopsParameter;

/**
 * Класс для вызова API-методов транспортной компании DPD.
 */
class DPDService implements DeliveryServiceInterface
{
    /**
     * Название сервиса.
     */
    const SERVICE_NAME = 'DPD';

    /**
     * Ограничение на длину истории заказа (нас интересуют только последние события в истории).
     */
    const MAX_EVENTS_LIMIT = 100;

    /**
     * Коды состояний заказа, которые мы можем считать как доставленные.
     */
    const EVENT_FINISHED_CODES = [
        'OrderReady',
        'OrderWorkCompleted',
        'OrderEAWaybill',
    ];

    /**
     * Константы для обозначения типов терминалов (для вывода пользователям).
     */
    const DPD_TYPE_TERMINAL = 'ПВП';
    const DPD_TYPE_TERMINAL_DESCRIPTION = 'Пункт приёма/выдачи посылок';
    const DPD_TYPE_POSTAMAT = 'П';
    const DPD_TYPE_POSTAMAT_DESCRIPTION = 'Постамат';

    /**
     * Типы терминалов, принимающих оплаты.
     */
    const DPD_TERMINAL_PAYMENTABLE = [
        'Payment', 'PaymentByBankCard',
    ];

    /**
     * Поправочный коэффициент на стоимость доставки (для покрытия наших издержек).
     */
    const DPD_DELIVERY_COST_MULTIPLIER = 1.3;

    /**
     * Указывается конкретная реализация класса параметра для текущего сервиса (DPD)/.
     */
    const SERVICE_PARAMETERS_MAP = [
        ServiceCostRequestParameterInterface::class => ServiceCostParameter::class,
        SelfDeliveryTerminalRequestParameterInterface::class => ParcelShopsParameter::class,
    ];

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var string
     */
    private $apiHost;

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $key;

    /**
     * @var array
     */
    private $serviceMap = [
        'getCitiesCashPay' => 'geography2',
        'getTerminalsSelfDelivery2' => 'geography2',
        'getParcelShops' => 'geography2',
        'getStoragePeriod' => 'geography2',
        'getServiceCost2' => 'calculator2',
        'getServiceCostByParcels2' => 'calculator2',
        'createOrder' => 'order2',
        'getOrderStatus' => 'order2',
        'addParcels' => 'order2',
        'removeParcels' => 'order2',
        'cancelOrder' => 'order2',
        'getStatesByDPDOrder' => 'tracing',
        'getStatesByClientOrder' => 'tracing',
        'getStatesByClientParcel' => 'tracing',
        'getEvents' => 'event-tracking',
        'getNLAmount' => 'nl',
        'getNLInvoice' => 'nl',
        'createLabelFile' => 'label-print',
    ];

    /**
     * @var array
     */
    private $currentAuth;

    /**
     * Конструктор.
     *
     * @param ValidatorInterface $validator Валидатор
     * @param string             $host      Хост DPD API
     * @param string             $login     Логин от аккаунта
     * @param string             $key       Ключ от аккаунта
     */
    public function __construct(
        ValidatorInterface $validator,
        string $host,
        string $login,
        string $key
    ) {
        $this->validator = $validator;
        $this->apiHost = $host;
        $this->login = $login;
        $this->key = $key;

        $this->currentAuth = [
            'clientNumber' => $this->login,
            'clientKey' => $this->key,
        ];
    }

    /**
     * Создает подключение к API DPD, возвращает клиента.
     *
     * @param string $method Используемый метод
     *
     * @throws OrderDeliveryServiceNotFoundException|OrderDeliveryServiceUnavailableException
     *
     * @return \SoapClient
     */
    private function connect(string $method): \SoapClient
    {
        if (!isset($this->serviceMap[$method])) {
            throw new OrderDeliveryServiceNotFoundException(sprintf('Trying to call a non-existing service "%s"!', $method));
        }

        $soapClientOptions = [
            'stream_context' => stream_context_create([
                'http' => [
                    'user_agent' => 'IrmagPHPSoapClient',
                ],
            ]),
            'cache_wsdl' => WSDL_CACHE_NONE,
        ];

        try {
            $client = new \SoapClient(sprintf(
                '%s%s?wsdl',
                $this->apiHost,
                $this->serviceMap[$method]
            ), $soapClientOptions);
        } catch (\SoapFault $e) {
            throw new OrderDeliveryServiceUnavailableException(
                'Unable to initialize SoapClient! Message: '.$e->getMessage(),
                $e->getCode(),
                $e->getPrevious()
            );
        }

        return $client;
    }

    /**
     * Выполнить запрос к DPD API.
     *
     * @param string $method            Название метода
     * @param array  $params            Параметры запроса
     * @param bool   $needExtraRootNode
     *
     * @throws OrderDeliveryServiceRequestFailedException
     *
     * @return array
     */
    private function request(string $method, array $params = [], bool $needExtraRootNode = false): array
    {
        $client = $this->connect($method);

        if ($needExtraRootNode) {
            $requestParams['request'] = $params;
            $requestParams['request']['auth'] = $this->currentAuth;
        } else {
            $requestParams['auth'] = $this->currentAuth;
        }

        try {
            $result = $client->$method($requestParams);
            $result = SerializerBuilder::create()
                ->setPropertyNamingStrategy(new IdenticalPropertyNamingStrategy())
                ->build()
                ->toArray($result);
        } catch (\SoapFault $e) {
            $code = (!empty($e->detail->WSFault->code)) ? $e->detail->WSFault->code : $e->getCode();

            throw new OrderDeliveryServiceRequestFailedException(
                sprintf('[%s] Error while calling API method "%s". Message: [%s] %s!', self::SERVICE_NAME, $method, $code, $e->getMessage())
            );
        }

        return $result['return'];
    }

    /**
     * Валидирует параметры запроса и бросает исключение, если обнаружены ошибки.
     *
     * @param OrderDeliveryServiceRequestParameterInterface $params Параметры запроса
     *
     * @throws OrderDeliveryServiceInvalidRequestParametersException
     */
    private function throwExceptionIfParametersAreInvalid(OrderDeliveryServiceRequestParameterInterface $params): void
    {
        $errors = $this->validator->validate($params);

        if (\count($errors) > 0) {
            throw new OrderDeliveryServiceInvalidRequestParametersException(
                self::SERVICE_NAME,
                sprintf('Invalid request parameters! %s', (string) $errors)
            );
        }
    }

    /**
     * Получить общую стоимость доставки заказа.
     *
     * @param ServiceCostParameter $params Параметры для запроса
     *
     * @return array
     */
    private function getServiceCost(ServiceCostParameter $params): array
    {
        $this->throwExceptionIfParametersAreInvalid($params);
        $result = $this->request('getServiceCost2', $params->toArray(), true);

        return $this->getMultipliedCostResults($result);
    }

    /**
     * Вернуть название класса для параметров запроса по названию интерфейса.
     *
     * @param string $parameterInterface
     *
     * @throws IrmagException
     *
     * @return string
     */
    public function getServiceParameter(string $parameterInterface): string
    {
        $map = self::SERVICE_PARAMETERS_MAP;

        if (!isset($map[$parameterInterface])) {
            throw new IrmagException(
                sprintf(
                    'Could not find configured implementation of "%s" for service "%s".',
                    $parameterInterface,
                    self::class
                )
            );
        }

        return $map[$parameterInterface];
    }

    /**
     * Получить список населённых пунктов с возможностью доставки.
     *
     * @return array
     */
    public function getCityList(): array
    {
        return $this->request('getCitiesCashPay', [], true);
    }

    /**
     * Получить список подразделений DPD.
     *
     * @return array
     */
    public function getSelfDeliveryTerminalsList(): array
    {
        $result = $this->request('getTerminalsSelfDelivery2');

        return $result['terminal'];
    }

    /**
     * Получить список пунктов приема/выдачи посылок, имеющих ограничения по габаритам и весу, с указанием режима работы пункта
     * и доступностью выполнения самопривоза/самовывоза.
     *
     * @param SelfDeliveryTerminalRequestParameterInterface $params Параметры для запроса
     *
     * @return array
     */
    public function getSelfDeliveryTerminalsAtPoint(SelfDeliveryTerminalRequestParameterInterface $params): array
    {
        $this->throwExceptionIfParametersAreInvalid($params);
        $result = $this->request('getParcelShops', $params->toArray(), true);

        // отфильтровываем те пункты, где нет оплаты
        if (!empty($result['parcelShop'])) {
            $result = $this->filterTerminalsWithPaymentAbility($result['parcelShop']);
        }

        return $result ?? [];
    }

    /**
     * Рассчитать все возможные варианты доставки и выбрать самые экономичные.
     *
     * @param ServiceCostRequestParameterInterface $params                 Параметры для расчёта стоимости
     * @param bool                                 $needSelfDeliveryResult Считать ли вдобавок доставку до пунктов самовывоза
     *
     * @return array
     */
    public function getDeliveryVariants(ServiceCostRequestParameterInterface $params, bool $needSelfDeliveryResult = false): array
    {
        $result = $this->getServiceCost($params);
        $output[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_DOOR] = $this->addExtraFields($result);

        if ($needSelfDeliveryResult) {
            try {
                $additionalParams = $params;
                $additionalParams->setIsSelfDelivery(true);

                $selfDeliveryResult = $this->getServiceCost($additionalParams);
                $output[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL] = $this->addExtraFields($selfDeliveryResult);
            } catch (OrderDeliveryServiceRequestFailedException $ex) {
                $output[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL] = [];
            }
        }
        $output = $this->markTheCheapestVariant($output);
        $output = $this->sortVariantsByCost($output);

        return $output;
    }

    /**
     * Получить события по заказам.
     *
     * @return array
     */
    public function getEvents(): array
    {
        $eventsHistory = [];
        $resultComplete = false;

        while (!$resultComplete) {
            $result = $this->request('getEvents', ['maxRowCount' => self::MAX_EVENTS_LIMIT], true);

            if (empty($result['event'])) {
                $resultComplete = true;
            } else {
                $eventsHistory = array_merge($eventsHistory, $result['event']);
                $resultComplete = $result['resultComplete'];
            }
        }

        usort($eventsHistory, function (array $first, array $second) {
            return $first['eventDate'] <=> $second['eventDate'];
        });

        return $eventsHistory;
    }

    /**
     * Доставлен ли получателю.
     *
     * @param string $trackCode
     * @param array  $events
     *
     * @return bool
     */
    public function isParcelDelivered(string $trackCode, array $events = []): bool
    {
        if (empty($events)) {
            $events = $this->getEvents();
        }

        $isDelivered = false;

        foreach ($events as $event) {
            if ($event['dpdOrderNr'] !== $trackCode) {
                continue;
            }

            if (true === \in_array($event['eventCode'], self::EVENT_FINISHED_CODES, true)) {
                $isDelivered = true;
                break;
            }
        }

        return $isDelivered ?? false;
    }

    /**
     * Сортировать варианты доставки в порядке возрастания стоимости.
     *
     * @param array $variants
     *
     * @return array
     */
    private function sortVariantsByCost(array $variants): array
    {
        uasort($variants[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_DOOR], function (array $first, array $second) {
            return $first['cost'] <=> $second['cost'];
        });

        if (!empty($variants[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL])) {
            uasort($variants[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL], function (array $first, array $second) {
                return $first['cost'] <=> $second['cost'];
            });
        }

        return $variants;
    }

    /**
     * Выбрать самый дешёвый вариант из рассчитанных и сделать пометку.
     *
     * @param array $variants Рассчитанные варианты
     *
     * @return array
     */
    private function markTheCheapestVariant(array $variants): array
    {
        $toDoorVariants = $variants[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_DOOR];
        $toSelfDeliveryVariants = $variants[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL];
        $cheapest = [];

        foreach ($toDoorVariants as $key => $var) {
            if (empty($cheapest) || $var['cost'] < $cheapest['cost']) {
                $cheapest = $this->fillTheCheapest($var, $key, OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_DOOR);
            }
        }

        $variants[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_DOOR][$cheapest['serviceCode']]['isCheapest'] = true;
        $cheapest = [];

        if (!empty($toSelfDeliveryVariants)) {
            foreach ($toSelfDeliveryVariants as $key => $var) {
                if (empty($cheapest) || $var['cost'] < $cheapest['cost']) {
                    $cheapest = $this->fillTheCheapest($var, $key, OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL);
                }
            }

            $variants[OrderDeliveryServiceData::DELIVERY_SERVICE_TYPE_TO_TERMINAL][$cheapest['serviceCode']]['isCheapest'] = true;
        }

        return $variants;
    }

    /**
     * Заполнить данные о самом выгодном найденном варианте.
     *
     * @param array  $variant     Массив с данными о варианте доставки
     * @param string $serviceCode Код тарифа текущего варианта доставки
     * @param string $type        Тип доставки
     *
     * @return array
     */
    private function fillTheCheapest(array $variant, string $serviceCode, string $type): array
    {
        $cheapest = [
            'type' => $type,
            'cost' => $variant['cost'],
            'serviceCode' => $serviceCode,
        ];

        return $cheapest;
    }

    /**
     * Добавить дополнительные поля для показа.
     *
     * @param array $result
     *
     * @return array
     */
    private function addExtraFields(array $result): array
    {
        $output = [];

        foreach ($result as $res) {
            $key = $res['serviceCode'];

            $output[$key]['serviceName'] = $res['serviceName'];
            $output[$key]['cost'] = $res['cost'];
            $output[$key]['days'] = $res['days'];
            $output[$key]['isSelected'] = false;
            $output[$key]['isCheapest'] = false;
            $output[$key]['account'] = $this->login;
        }

        return $output;
    }

    /**
     * Отфильтровывает те пункты самовывоза, в которых невозможно рассчитаться за доставку.
     *
     * @param array $terminals
     *
     * @return array
     */
    private function filterTerminalsWithPaymentAbility(array $terminals): array
    {
        $filteredTerminals = [];

        foreach ($terminals as $termKey => $terminal) {
            $isTheOnlyTerminal = !is_numeric($termKey);

            $services = (true === $isTheOnlyTerminal) ? $terminals['schedule'] : $terminal['schedule'];
            $isPaymentAble = false;

            foreach ($services as $key => $service) {
                $operation = (!is_numeric($key)) ? $service : $service['operation'];

                if (true === \in_array($operation, self::DPD_TERMINAL_PAYMENTABLE, true)) {
                    $isPaymentAble = true;
                    break;
                }
            }

            if (true === $isPaymentAble) {
                $filteredTerminals[] = ($isTheOnlyTerminal) ? $terminals : $terminal;
            }

            if (true === $isTheOnlyTerminal) {
                break;
            }
        }

        return $filteredTerminals;
    }

    /**
     * Увеличить стоимость доставки в соответствии с поправочным коэффициентом.
     *
     * @param array $result
     *
     * @return array
     */
    private function getMultipliedCostResults(array $result): array
    {
        $multiplied = [];

        if (!empty($result['cost'])) {
            $multiplied = $result;
            $multiplied['cost'] = round($multiplied['cost'] * self::DPD_DELIVERY_COST_MULTIPLIER, 2);
        } else {
            foreach ($result as $key => $resultItem) {
                $multiplied[$key] = $resultItem;
                $multiplied[$key]['cost'] = round($multiplied[$key]['cost'] * self::DPD_DELIVERY_COST_MULTIPLIER, 2);
            }
        }

        return $multiplied;
    }
}
