<?php

namespace Irmag\OrderDeliveryServiceBundle\Service\DPDService;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryService;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCity;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceRegion;

/**
 * Класс для обновления справочных данных DPD.
 */
class DPDDataUpdaterService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * Обновляет данные о пункте доставки.
     *
     * @param array $item Массив с информацией о пункте доставки
     *
     * @throws IrmagException
     */
    public function updateCityItem(array $item): void
    {
        $dpdServiceEntity = $this->em->getRepository(OrderDeliveryService::class)->findOneBy(['name' => 'DPD']);

        if (!$dpdServiceEntity) {
            throw new IrmagException('Unable to get DPD delivery service: it does not exist.');
        }

        $region = $this->em->getRepository(OrderDeliveryServiceRegion::class)->find((int) $item['regionCode']);

        if (!$region) {
            throw new IrmagException(sprintf('Could not find region with id %d', $item['regionCode']));
        }

        $city = $this->em->getRepository(OrderDeliveryServiceCity::class)->findOneBy([
            'orderDeliveryService' => $dpdServiceEntity,
            'orderDeliveryServiceRegion' => $region,
            'name' => $item['cityName'],
        ]);

        if (!$city) {
            $city = new OrderDeliveryServiceCity();
        }

        $city->setOrderDeliveryService($dpdServiceEntity);
        $city->setName($item['cityName']);
        $city->setServiceCityCode($item['cityId']);
        $city->setOrderDeliveryServiceRegion($region);

        $this->em->persist($city);
        $this->em->flush();
        $this->em->clear();
    }
}
