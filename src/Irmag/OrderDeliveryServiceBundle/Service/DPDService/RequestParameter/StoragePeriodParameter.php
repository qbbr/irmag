<?php

namespace Irmag\OrderDeliveryServiceBundle\Service\DPDService\RequestParameter;

use Symfony\Component\Validator\Constraints as Assert;
use Irmag\OrderDeliveryServiceBundle\RequestContract\OrderDeliveryServiceRequestParameterInterface;

/**
 * Класс для параметров запроса к сервису DPD getStoragePeriod.
 */
class StoragePeriodParameter implements OrderDeliveryServiceRequestParameterInterface
{
    /**
     * @var string Код терминала доставки
     * @Assert\NotBlank
     */
    private $terminalCode;

    /**
     * @var string Код сервиса DPD
     */
    private $serviceCode;

    /**
     * Get terminal code.
     *
     * @return string
     */
    public function getTerminalCode(): string
    {
        return $this->terminalCode;
    }

    /**
     * Set terminal code.
     *
     * @param string $terminalCode Terminal code
     *
     * @return StoragePeriodParameter
     */
    public function setTerminalCode($terminalCode): self
    {
        $this->terminalCode = $terminalCode;

        return $this;
    }

    /**
     * Get service code.
     *
     * @return string
     */
    public function getServiceCode(): string
    {
        return $this->serviceCode;
    }

    /**
     * Get terminal code.
     *
     * @param string $serviceCode
     *
     * @return StoragePeriodParameter
     */
    public function setServiceCode($serviceCode): self
    {
        $this->serviceCode = $serviceCode;

        return $this;
    }

    /**
     * Get terminal code.
     *
     * @return array
     */
    public function toArray(): array
    {
        $array = [
            'terminalCode' => $this->getTerminalCode(),
        ];

        if (!empty($this->getServiceCode())) {
            $array['serviceCode'] = $this->getServiceCode();
        }

        return $array;
    }
}
