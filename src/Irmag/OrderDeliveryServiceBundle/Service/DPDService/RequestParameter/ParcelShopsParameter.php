<?php

namespace Irmag\OrderDeliveryServiceBundle\Service\DPDService\RequestParameter;

use Symfony\Component\Validator\Constraints as Assert;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCity;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCountry;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceRegion;
use Irmag\OrderDeliveryServiceBundle\RequestContract\OrderDeliveryServiceRequestParameterInterface;
use Irmag\OrderDeliveryServiceBundle\RequestContract\SelfDeliveryTerminalRequestParameterInterface;

/**
 * Класс для параметров запроса к сервису DPD getParcelShops.
 */
class ParcelShopsParameter implements OrderDeliveryServiceRequestParameterInterface, SelfDeliveryTerminalRequestParameterInterface
{
    /**
     * @var OrderDeliveryServiceCountry Страна
     * @Assert\NotBlank
     */
    private $country;

    /**
     * @var OrderDeliveryServiceRegion Регион
     */
    private $region;

    /**
     * @var OrderDeliveryServiceCity Населенный пункт
     * @Assert\NotBlank
     */
    private $city;

    /**
     * @var string Список кодов услуг DPD (через запятую)
     */
    private $serviceCode;

    /**
     * Get country code.
     *
     * @return OrderDeliveryServiceCountry
     */
    public function getCountry(): OrderDeliveryServiceCountry
    {
        return $this->country;
    }

    /**
     * Set country code.
     *
     * @param OrderDeliveryServiceCountry $country
     *
     * @return ParcelShopsParameter
     */
    public function setCountry(OrderDeliveryServiceCountry $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get region.
     *
     * @return OrderDeliveryServiceRegion
     */
    public function getRegion(): ?OrderDeliveryServiceRegion
    {
        return $this->region;
    }

    /**
     * Set country code.
     *
     * @param OrderDeliveryServiceRegion $region
     *
     * @return ParcelShopsParameter
     */
    public function setRegion($region): self
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get city.
     *
     * @return OrderDeliveryServiceCity
     */
    public function getCity(): OrderDeliveryServiceCity
    {
        return $this->city;
    }

    /**
     * Set city.
     *
     * @param OrderDeliveryServiceCity $city
     *
     * @return ParcelShopsParameter
     */
    public function setCity(OrderDeliveryServiceCity $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get service code.
     *
     * @return string
     */
    public function getServiceCode(): ?string
    {
        return $this->serviceCode;
    }

    /**
     * Set service code.
     *
     * @param string $serviceCode Service code
     *
     * @return ParcelShopsParameter
     */
    public function setServiceCode($serviceCode): self
    {
        $this->serviceCode = $serviceCode;

        return $this;
    }

    /**
     * Transform object to a request array.
     *
     * @return array
     */
    public function toArray(): array
    {
        $array = [];
        $array['countryCode'] = $this->getCountry()->getCountryCode();

        if (!empty($this->getRegion())) {
            $array['regionCode'] = $this->getRegion()->getId();
        }

        $array['cityCode'] = $this->getCity()->getServiceCityCode();
        $array['cityName'] = $this->getCity()->getName();

        if (!empty($this->getServiceCode())) {
            $array['serviceCode'] = $this->getServiceCode();
        }

        return $array;
    }
}
