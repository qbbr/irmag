<?php

namespace Irmag\OrderDeliveryServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\OrderDeliveryServiceBundle\Form\Type\DeliveryPointSelectorType;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCountry;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryService;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCity;
use Irmag\OrderDeliveryServiceBundle\Model\OrderDeliveryServiceAddress;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceMapper;
use Irmag\OrderDeliveryServiceBundle\RequestContract\SelfDeliveryTerminalRequestParameterInterface;
use Irmag\OrderDeliveryServiceBundle\RequestContract\ServiceCostRequestParameterInterface;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceManager;

/**
 * @Route("/delivery/ajax",
 *     options={"expose": true},
 *     condition="request.isXmlHttpRequest()",
 *     methods={"POST"},
 *     host="%irmag_site_domain%",
 * )
 */
class AjaxController extends Controller
{
    /**
     * Сколько городов показывать пользователю в подсказке (максимум).
     */
    const MAX_CITIES_TO_SHOW = 15;

    /**
     * Получить список пунктов доставки для выбранной службы доставки.
     *
     * @Route("/getcities/", name="irmag_get_delivery_city", methods={"GET"})
     *
     * @param Request $request
     *
     * @throws IrmagException
     *
     * @return JsonResponse
     */
    public function getDeliveryCityAction(Request $request): JsonResponse
    {
        $query = $request->get('term');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $selectedService = $em->getRepository(OrderDeliveryService::class)->find($request->get('service'));

        if (!$selectedService) {
            throw new IrmagException('Delivery service not found');
        }

        if (empty($query)) {
            throw new IrmagException('Query not found');
        }

        $results = $em
            ->getRepository(OrderDeliveryServiceCity::class)
            ->createQueryBuilder('c')
            ->select('c.id', "CONCAT(c.name, ' (', r.name, ')') AS name")
            ->join('c.orderDeliveryServiceRegion', 'r')
            ->where('c.orderDeliveryService = :service')
            ->andWhere('LOWER(c.name) LIKE :term')
            ->setParameter('service', $selectedService)
            ->setParameter('term', '%'.mb_strtolower($query).'%')
            ->setMaxResults(self::MAX_CITIES_TO_SHOW)
            ->getQuery()
            ->getResult();

        return new JsonResponse(['success' => true, 'results' => $results]);
    }

    /**
     * Запомнить выбранный город доставки в сессию.
     *
     * @Route("/savecity/", name="irmag_save_delivery_city", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function saveSelectedDeliveryCity(Request $request): JsonResponse
    {
        $this->get(OrderDeliveryServiceManager::class)
            ->saveDeliveryServiceData([
                'deliveryService' => $request->request->getInt('service'),
                'deliveryCity' => $request->request->getInt('cityId'),
            ]);

        return new JsonResponse(['success' => true]);
    }

    /**
     * Стереть выбранные данные по доставке.
     *
     * @Route("/cleardata/", name="irmag_clear_delivery_data", methods={"POST"})
     *
     * @return JsonResponse
     */
    public function clearDeliveryData(): JsonResponse
    {
        $this->get(OrderDeliveryServiceManager::class)->clearDeliveryServiceData();

        return new JsonResponse(['success' => true]);
    }

    /**
     * Выбрать вариант доставки и запомнить в сессию.
     *
     * @Route("/selectdelivery/", name="irmag_select_delivery_variant", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function selectDeliveryVariant(Request $request): JsonResponse
    {
        $dm = $this->get(OrderDeliveryServiceManager::class);
        $success = true;
        $results = $dm->getCalculatedResults();

        if (empty($results)) {
            $success = false;
        }

        $deliveryType = $request->request->get('type');
        $deliveryVariant = $request->request->get('variant');
        $dm->selectDeliveryVariant(['type' => $deliveryType, 'variant' => $deliveryVariant]);

        return new JsonResponse(['success' => $success]);
    }

    /**
     * Рассчитать стоимость доставки в указанный пункт.
     *
     * @Route("/getcost/", name="irmag_get_delivery_cost", methods={"POST"})
     *
     * @param Request $request
     *
     * @throws IrmagException
     *
     * @return JsonResponse
     */
    public function getDeliveryCostAction(Request $request): JsonResponse
    {
        if (empty($request->request->get('city'))) {
            return new JsonResponse(['success' => false]);
        }

        $em = $this->get('doctrine.orm.default_entity_manager');
        $dm = $this->get(OrderDeliveryServiceManager::class);
        $dm->saveBasketContent();

        /** @var OrderDeliveryService $deliveryService */
        $deliveryService = $em->getRepository(OrderDeliveryService::class)->find($request->request->getInt('service'));

        if (!$deliveryService) {
            throw new IrmagException(sprintf('Specified delivery service %d not found.', $request->request->getInt('service')));
        }

        $deliveryServiceShortname = $deliveryService->getShortname();
        /** @var OrderDeliveryServiceCity $deliveryCity */
        $deliveryCity = $em->getRepository(OrderDeliveryServiceCity::class)->find($request->request->getInt('city'));
        /** @var OrderDeliveryServiceCountry $country */
        $country = $em->getRepository(OrderDeliveryServiceCountry::class)->findOneBy(['countryCode' => 'RU']);

        if (!$deliveryCity) {
            throw new IrmagException(sprintf(
                'Specified delivery city %d for service %s not found.',
                $request->request->getInt('city'),
                $deliveryService->getName()
            ));
        }

        /** @var OrderDeliveryServiceCity $pickupCity */
        $pickupCity = $em->getRepository(OrderDeliveryServiceCity::class)->findOneBy([
            'name' => 'Иркутск',
            'orderDeliveryService' => $deliveryService,
        ]);

        /* Выбор сервиса и вызов его методов */
        $service = $this->get(OrderDeliveryServiceMapper::getService($deliveryServiceShortname));
        $serviceCostParameterClass = $service->getServiceParameter(ServiceCostRequestParameterInterface::class);
        $param = new $serviceCostParameterClass();

        // откуда везем
        $pickupAddress = new OrderDeliveryServiceAddress();
        $pickupAddress
            ->setCountry($country)
            ->setRegion($pickupCity->getOrderDeliveryServiceRegion())
            ->setCity($pickupCity);

        // куда везем
        $deliveryAddress = new OrderDeliveryServiceAddress();
        $deliveryAddress
            ->setCountry($country)
            ->setRegion($deliveryCity->getOrderDeliveryServiceRegion())
            ->setCity($deliveryCity);

        $param
            ->setPickupAddress($pickupAddress)
            ->setDeliveryAddress($deliveryAddress)
            ->setWeight((float) $request->request->get('weight'))
            ->setVolume((float) $request->request->get('volume'));

        $results = $service->getDeliveryVariants($param, true);
        $results['deliveryService'] = $deliveryService;
        $dm->setCalculatedResults($results);

        return new JsonResponse(['success' => true]);
    }

    /**
     * Рассчитать стоимость доставки в указанный пункт.
     *
     * @Route("/setterminal/", name="irmag_set_delivery_terminal", methods={"POST"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setDeliveryPointAction(Request $request): JsonResponse
    {
        $terminal = $request->request->get('terminal');
        $address = trim($request->request->get('address'));

        $this->get(OrderDeliveryServiceManager::class)->saveDeliveryServiceData([
            'deliveryPoint' => $terminal,
            'deliveryPointAddress' => $address,
        ]);

        return new JsonResponse(['success' => true]);
    }

    /**
     * Получить список пунктов самовывоза.
     *
     * @Route("/terminals", name="irmag_get_available_delivery_points", methods={"POST"})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response|JsonResponse
     */
    public function getDeliveryPointsFormAction(Request $request)
    {
        $manager = $this->get(OrderDeliveryServiceManager::class);
        $em = $this->get('doctrine.orm.default_entity_manager');
        $data = $manager->getSelectedData();
        $service = $this->get(OrderDeliveryServiceMapper::getService($data['deliveryServiceShortname']));

        if (empty($manager->getDeliveryTerminalsData())) {
            $paramObjectName = $service->getServiceParameter(SelfDeliveryTerminalRequestParameterInterface::class);
            $paramObject = new $paramObjectName();

            $country = $em->getRepository(OrderDeliveryServiceCountry::class)
                ->findOneBy(['countryCode' => 'RU']);

            $city = $em->getRepository(OrderDeliveryServiceCity::class)->find($data['deliveryCity']);
            $paramObject
                ->setCountry($country)
                ->setCity($city)
                ->setRegion($city->getOrderDeliveryServiceRegion());

            $points = $service->getSelfDeliveryTerminalsAtPoint($paramObject);
            $manager->setDeliveryTerminalsData($points);
        }

        $points = $manager->getDeliveryTerminalsData();
        $weight = $request->request->get('weight');
        $filteredPoints = (!empty($points['code'])) ? [$points] : $points;

        foreach ($filteredPoints as $key => $point) {
            if (!empty($point['limits']['maxWeight']) && (float) $weight > (float) $point['limits']['maxWeight']) {
                unset($filteredPoints[$key]);
            }
        }

        /* Если нет подходящих пунктов самовывоза, то выбора не предлагаем */
        if (empty($filteredPoints)) {
            return new JsonResponse(['empty' => true]);
        }

        $choices = [];

        foreach ($points as $key => $point) {
            if (\is_array($point) && !empty($point['code'])) {
                $choices[] = $point['code'];
            } elseif ('code' === $key) {
                $choices[] = $point;
                break;
            }
        }

        $form = $this->createForm(DeliveryPointSelectorType::class, null, [
            'choices' => $choices ?? [],
            'checked' => $data['deliveryPoint'] ?? null,
        ]);

        return $this->render('@IrmagOrderDeliveryService/delivery-points.html.twig', [
            'serviceName' => $data['deliveryServiceShortname'],
            'points' => $points,
            'currentWeight' => $weight,
            'form' => $form->createView(),
        ]);
    }
}
