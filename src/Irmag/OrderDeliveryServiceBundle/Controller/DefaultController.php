<?php

namespace Irmag\OrderDeliveryServiceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\OrderDeliveryServiceBundle\Form\Type\DeliveryCalculatorType;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceManager;

/**
 * @Route("/delivery")
 */
class DefaultController extends Controller
{
    /**
     * Во сколько раз увеличить вес товаров с учетом упаковки в посылку.
     */
    const PARCEL_WEIGHT_MULTIPLIER = 1.2;

    /**
     * @Route("/", name="irmag_get_delivery_service", host="%irmag_site_domain%")
     *
     * @param int $weight Общий вес товаров в корзине (в граммах)
     * @param int $volume Общий объём товаров в корзине
     *
     * @return Response
     */
    public function indexAction(int $weight, int $volume): Response
    {
        $dm = $this->get(OrderDeliveryServiceManager::class);
        $selectedData = $dm->getSelectedData();

        if (true === $dm->isBasketContentChanged()) {
            $dm->resetCalculatedResults();
            $dm->clearBasketContentSessionData();
        }

        return $this->render('@IrmagOrderDeliveryService/calculator.html.twig', [
            'data' => $selectedData,
            'weight' => $weight,
            'volume' => $this->getParcelVolume((float) $volume * 0.000001),
        ]);
    }

    /**
     * @Route("/calculator", name="irmag_get_delivery_service_form", host="%irmag_site_domain%", options={"expose": true})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function getFormAction(Request $request): Response
    {
        $weight = $request->request->get('weight');
        $volume = $request->request->get('volume');
        $deliveryForm = $this->createForm(DeliveryCalculatorType::class);

        return $this->render('@IrmagOrderDeliveryService/delivery-calculator.html.twig', [
            'form' => $deliveryForm->createView(),
            'weight' => (float) $weight * 0.001 * self::PARCEL_WEIGHT_MULTIPLIER,
            'volume' => $this->getParcelVolume((float) $volume * 0.000001),
        ]);
    }

    /**
     * @Route("/delivery_cost", name="irmag_get_delivery_service_cost", host="%irmag_site_domain%")
     *
     * @return Response
     */
    public function deliveryCostAction(): Response
    {
        $dm = $this->get(OrderDeliveryServiceManager::class);
        $selectedData = $dm->getSelectedData();

        $cost = (!empty($selectedData['cost'])) ? $selectedData['cost'] : null;

        return $this->render('@IrmagOrderDeliveryService/delivery-cost-line.html.twig', [
            'cost' => $cost,
        ]);
    }

    /**
     * Получить объём посылки, исходя из суммарного объёма товаров.
     *
     * @param float $basketVolume Объём товаров в корзине (кубометры)
     *
     * @return float
     */
    private function getParcelVolume(float $basketVolume): float
    {
        $parcelVolume = $basketVolume;

        switch ($basketVolume) {
            case $basketVolume < 0.001:
                $parcelVolume = 0.001;
                break;
            case $basketVolume > 0.001 && $basketVolume <= 0.002:
                $parcelVolume = 0.002;
                break;
            case $basketVolume > 0.002 && $basketVolume <= 0.005:
                $parcelVolume = 0.005;
                break;
            case $basketVolume > 0.005 && $basketVolume <= 0.01:
                $parcelVolume = 0.01;
                break;
            case $basketVolume > 0.01 && $basketVolume <= 0.026:
                $parcelVolume = 0.026;
                break;
            default:
                break;
        }

        return $parcelVolume;
    }
}
