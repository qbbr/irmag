<?php

namespace Irmag\OrderDeliveryServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @Serializer\ExclusionPolicy("all")
 * @ORM\Table(name="orders_delivery_services")
 * @ORM\Entity
 */
class OrderDeliveryService
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $id;

    /**
     * Название.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $name;

    /**
     * Короткое название.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=32, unique=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $shortname;

    /**
     * Описание.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $description;

    /**
     * Активность.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $isActive;

    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): self
    {
        $this->shortname = $shortname;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
