<?php

namespace Irmag\OrderDeliveryServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\Order;

/**
 * @ORM\Table(name="orders_delivery_services_data")
 * @ORM\Entity
 * @Serializer\ExclusionPolicy("all")
 */
class OrderDeliveryServiceData
{
    /**
     * Тип доставки "до двери".
     */
    const DELIVERY_SERVICE_TYPE_TO_DOOR = 'toDoor';

    /**
     * Тип доставки "до пункта самовывоза (терминала)".
     */
    const DELIVERY_SERVICE_TYPE_TO_TERMINAL = 'toSelfDelivery';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data", "api_order"})
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\OneToOne(targetEntity="\Irmag\SiteBundle\Entity\Order", inversedBy="deliveryServiceData")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $order;

    /**
     * @var OrderDeliveryService
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryService")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $orderDeliveryService;

    /**
     * @var OrderDeliveryServiceCity
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryServiceCity")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $orderDeliveryServiceCity;

    /**
     * @var string
     *
     * @Assert\Choice({"toDoor", "toSelfDelivery"})
     *
     * @ORM\Column(type="string")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $tariff;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     */
    private $cost;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $days;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $deliveryPoint;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $deliveryPointAddress;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $trackCode;

    /**
     * XXX: custom for SonataAdmin.
     */
    public function getTypes(): array
    {
        return [
            self::DELIVERY_SERVICE_TYPE_TO_DOOR => 'До двери',
            self::DELIVERY_SERVICE_TYPE_TO_TERMINAL => 'До пункта самовывоза',
        ];
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTariff(): ?string
    {
        return $this->tariff;
    }

    public function setTariff(string $tariff): self
    {
        $this->tariff = $tariff;

        return $this;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getDays(): ?int
    {
        return $this->days;
    }

    public function setDays(int $days): self
    {
        $this->days = $days;

        return $this;
    }

    public function getDeliveryPoint(): ?string
    {
        return $this->deliveryPoint;
    }

    public function setDeliveryPoint(?string $deliveryPoint): self
    {
        $this->deliveryPoint = $deliveryPoint;

        return $this;
    }

    public function getDeliveryPointAddress(): ?string
    {
        return $this->deliveryPointAddress;
    }

    public function setDeliveryPointAddress(?string $deliveryPointAddress): self
    {
        $this->deliveryPointAddress = $deliveryPointAddress;

        return $this;
    }

    public function getTrackCode(): ?string
    {
        return $this->trackCode;
    }

    public function setTrackCode(?string $trackCode): self
    {
        $this->trackCode = $trackCode;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getOrderDeliveryService(): ?OrderDeliveryService
    {
        return $this->orderDeliveryService;
    }

    public function setOrderDeliveryService(?OrderDeliveryService $orderDeliveryService): self
    {
        $this->orderDeliveryService = $orderDeliveryService;

        return $this;
    }

    public function getOrderDeliveryServiceCity(): ?OrderDeliveryServiceCity
    {
        return $this->orderDeliveryServiceCity;
    }

    public function setOrderDeliveryServiceCity(?OrderDeliveryServiceCity $orderDeliveryServiceCity): self
    {
        $this->orderDeliveryServiceCity = $orderDeliveryServiceCity;

        return $this;
    }
}
