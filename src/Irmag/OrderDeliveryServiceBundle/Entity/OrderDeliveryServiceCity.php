<?php

namespace Irmag\OrderDeliveryServiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="orders_delivery_services_cities")
 * @ORM\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderDeliveryServiceCity
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $id;

    /**
     * Название.
     *
     * @var string
     *
     * @ORM\Column(type="string")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $name;

    /**
     * Служба доставки для города.
     *
     * @var OrderDeliveryService
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryService")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $orderDeliveryService;

    /**
     * Регион города.
     *
     * @var OrderDeliveryServiceRegion
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryServiceRegion", inversedBy="cities")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_delivery_service_data"})
     */
    private $orderDeliveryServiceRegion;

    /**
     * Код города (внутренний для службы доставки).
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $serviceCityCode;

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    /**
     * XXX: custom.
     *
     * @return string
     */
    public function getFullName(): string
    {
        return sprintf('%s (%s)', $this->name, $this->getOrderDeliveryServiceRegion()->getName());
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getServiceCityCode(): ?string
    {
        return $this->serviceCityCode;
    }

    public function setServiceCityCode(?string $serviceCityCode): self
    {
        $this->serviceCityCode = $serviceCityCode;

        return $this;
    }

    public function getOrderDeliveryService(): ?OrderDeliveryService
    {
        return $this->orderDeliveryService;
    }

    public function setOrderDeliveryService(?OrderDeliveryService $orderDeliveryService): self
    {
        $this->orderDeliveryService = $orderDeliveryService;

        return $this;
    }

    public function getOrderDeliveryServiceRegion(): ?OrderDeliveryServiceRegion
    {
        return $this->orderDeliveryServiceRegion;
    }

    public function setOrderDeliveryServiceRegion(?OrderDeliveryServiceRegion $orderDeliveryServiceRegion): self
    {
        $this->orderDeliveryServiceRegion = $orderDeliveryServiceRegion;

        return $this;
    }
}
