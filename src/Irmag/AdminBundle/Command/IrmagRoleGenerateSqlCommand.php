<?php

namespace Irmag\AdminBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Irmag\AdminBundle\Util\RoleHierarchyGenerator;

class IrmagRoleGenerateSqlCommand extends Command
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('irmag:role:generate_sql')
            ->setDescription('Generate hierarchy roles for admin')
            ->addArgument('admin-service-name', InputArgument::REQUIRED, 'Sonata admin service name (Example: "irmag.admin.content.page")')
            ->addArgument('description', InputArgument::REQUIRED, 'Description for role (Example: "Контент - Страницы")')
            ->addOption('decorate-prefix', null, InputOption::VALUE_REQUIRED, 'Decorate prefix', '')
            ->addOption('decorate-suffix', null, InputOption::VALUE_REQUIRED, 'Decorate suffix', '')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $adminServiceName = $input->getArgument('admin-service-name');
        $description = $input->getArgument('description');

        $result = RoleHierarchyGenerator::generate($adminServiceName, $description, $input->getOption('decorate-prefix'), $input->getOption('decorate-suffix'));

        $output->writeln($result);
    }
}
