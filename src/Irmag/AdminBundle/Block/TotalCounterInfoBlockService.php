<?php

namespace Irmag\AdminBundle\Block;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Admin\Pool;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\CoreBundle\Repository\Traits\GetTotalTrait;

class TotalCounterInfoBlockService extends AbstractBlockService
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Pool
     */
    protected $adminPool;

    /**
     * @param string                 $name
     * @param EngineInterface        $templating
     * @param EntityManagerInterface $em
     * @param Pool                   $adminPool
     */
    public function __construct(
        $name,
        EngineInterface $templating,
        EntityManagerInterface $em,
        Pool $adminPool = null
    ) {
        $this->em = $em;
        $this->adminPool = $adminPool;

        parent::__construct($name, $templating);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();
        /** @var GetTotalTrait $repo */
        $repo = $this->em->getRepository($settings['entity']);

        $parameters = [
            'context' => $blockContext,
            'settings' => $settings,
            'block' => $blockContext->getBlock(),
            'admin_pool' => $this->adminPool,
            'total_per_all_time' => $repo->getTotalPerAllTime(),
            'total_per_months' => $repo->getTotalPerMonths(),
            'total_per_today' => $repo->getTotalPerToday(),
        ];

        return $this->renderPrivateResponse($blockContext->getTemplate(), $parameters, $response);
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'title' => '',
            'entity' => '',
            'info_box_bg' => '',
            'icon' => '',
            'list_route' => '',
            'template' => '@IrmagAdmin/Block/total-counter-info.html.twig',
        ]);
    }
}
