<?php

namespace Irmag\AdminBundle\Block;

use Sonata\AdminBundle\Admin\Pool;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AdministrationBlockService extends AbstractBlockService
{
    /**
     * @var string
     */
    protected $maintenanceFile;

    /**
     * @var Pool
     */
    protected $adminPool;

    /**
     * @param string          $name
     * @param EngineInterface $templating
     * @param Pool            $adminPool
     */
    public function __construct($name, EngineInterface $templating, Pool $adminPool = null)
    {
        $this->adminPool = $adminPool;

        parent::__construct($name, $templating);
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $maintenanceFile = $blockContext->getSetting('maintenance_file');

        $parameters = [
            'context' => $blockContext,
            'settings' => $blockContext->getSettings(),
            'block' => $blockContext->getBlock(),
            'admin_pool' => $this->adminPool,
            'maintenance_is_active' => file_exists($maintenanceFile),
        ];

        return $this->renderPrivateResponse($blockContext->getTemplate(), $parameters, $response);
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'maintenance_file' => 'maintenance.file',
            'title' => '',
            'template' => '@IrmagAdmin/Block/administration.html.twig',
        ]);
    }
}
