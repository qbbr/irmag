<?php

namespace Irmag\AdminBundle\Controller;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Irmag\Exchange1CBundle\Entity\Exchange1CQueue;
use Irmag\Exchange1CBundle\Service\Exchange1CQueueManager;

class Exchange1CQueueAdminController extends Controller
{
    public function postponeQueueProcessingAction()
    {
        /** @var Exchange1CQueue $object */
        if (!$object = $this->admin->getSubject()) {
            throw new NotFoundHttpException('Object not found');
        }

        $queueManager = $this->container->get(Exchange1CQueueManager::class);
        $queueManager->postpone($object, true);

        return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }

    public function upQueuePriorityAction()
    {
        /** @var Exchange1CQueue $object */
        if (!$object = $this->admin->getSubject()) {
            throw new NotFoundHttpException('Object not found');
        }

        $queueManager = $this->container->get(Exchange1CQueueManager::class);
        $queueManager->priorityUp($object);

        return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }

    public function downQueuePriorityAction()
    {
        /** @var Exchange1CQueue $object */
        if (!$object = $this->admin->getSubject()) {
            throw new NotFoundHttpException('Object not found');
        }

        $queueManager = $this->container->get(Exchange1CQueueManager::class);
        $queueManager->priorityDown($object);

        return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }

    public function downloadXmlAction()
    {
        /** @var Exchange1CQueue $object */
        if (!$object = $this->admin->getSubject()) {
            throw new NotFoundHttpException('Object not found');
        }

        $file = $this->getParameter('irmag_exchange_1c_xml_dir').'/'.$object->getFile();

        if (!is_file($file)) {
            throw $this->createNotFoundException('XML file not found!');
        }

        return $this->file($file);
    }
}
