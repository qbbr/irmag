<?php

namespace Irmag\AdminBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Irmag\AdminBundle\Controller\Traits\ActivateDeactivateTrait;

class ElementAdminController extends Controller
{
    use ActivateDeactivateTrait;
}
