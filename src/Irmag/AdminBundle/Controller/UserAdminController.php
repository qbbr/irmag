<?php

namespace Irmag\AdminBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Irmag\ProfileBundle\Service\UserBlockHistoryManager\UserBlockHistoryManager;

class UserAdminController extends Controller
{
    /**
     * Блокировка / разблокировка пользователя.
     *
     * @return RedirectResponse
     */
    public function changeUserActivityAction()
    {
        $this->checkPrivileges();
        $object = $this->getObjectOrThrowException();

        $this->get(UserBlockHistoryManager::class)->blockUser($object, true);

        return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }

    /**
     * Запрет на комментирование на форуме.
     *
     * @return RedirectResponse
     */
    public function blockOnForumAction()
    {
        $this->checkPrivileges();
        $object = $this->getObjectOrThrowException();
        $forumConfigDisallowRoles = $this->getParameter('irmag_forum.disallow_roles');

        if (!empty($forumConfigDisallowRoles)) {
            $roles = [
                $forumConfigDisallowRoles['forum_post']['create'],
                $forumConfigDisallowRoles['forum_topic']['create'],
            ];

            $this->get(UserBlockHistoryManager::class)->addDisallowRolesForUser($object, 'forum', $roles);
        }

        return new RedirectResponse($this->admin->generateUrl('list', ['filter' => $this->admin->getFilterParameters()]));
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function blockOnElementCommentsAction(Request $request)
    {
        $this->blockOnComments('element');

        return new RedirectResponse($request->headers->get('referer'));
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function blockOnNewsCommentsAction(Request $request)
    {
        $this->blockOnComments('news');

        return new RedirectResponse($request->headers->get('referer'));
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function blockOnActionCommentsAction(Request $request)
    {
        $this->blockOnComments('action');

        return new RedirectResponse($request->headers->get('referer'));
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function blockOnBlogCommentsAction(Request $request)
    {
        $this->blockOnComments('blog');

        return new RedirectResponse($request->headers->get('referer'));
    }

    /**
     * Запрет на комментирование.
     *
     * @param string $mode Режим комментариев
     *
     * @throws \Exception
     */
    private function blockOnComments(string $mode)
    {
        $this->checkPrivileges();
        $object = $this->getObjectOrThrowException();
        $commentsConfigs = $this->getParameter('irmag_thread_comment.configs');

        if (!empty($commentsConfigs)) {
            $this->get(UserBlockHistoryManager::class)->addDisallowRolesForUser($object, $mode, [$commentsConfigs[$mode]['disallow_roles']['create']]);
        }
    }

    /**
     * Возвращает объект или бросает исключение.
     *
     * @throws NotFoundHttpException
     *
     * @return mixed
     */
    private function getObjectOrThrowException()
    {
        $object = $this->admin->getSubject();

        if (!$object) {
            throw new NotFoundHttpException('Object not found');
        }

        return $object;
    }

    /**
     * Проверка на наличие роли.
     *
     * @throws AccessDeniedException
     */
    private function checkPrivileges()
    {
        if (!$this->isGranted('ROLE_IRMAG_ADMIN_ADMINISTRATION_USER_EDIT')) {
            throw new AccessDeniedException('You are not allowed to block users!');
        }
    }
}
