<?php

namespace Irmag\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends Controller
{
    /**
     * @Route("/maintenance_page", name="admin_irmag_maintenance_page_toggler")
     */
    public function indexAction()
    {
        $maintenanceFile = $this->getParameter('irmag_maintenance_file');

        if (file_exists($maintenanceFile)) {
            $this->addFlash('sonata_flash_success', 'Режим технической работы выключен.');
            unlink($maintenanceFile);
        } else {
            $this->addFlash('sonata_flash_error', 'Режим технической работы ВКЛЮЧЕН.');
            touch($maintenanceFile);
        }

        return $this->redirectToRoute('sonata_admin_dashboard');
    }
}
