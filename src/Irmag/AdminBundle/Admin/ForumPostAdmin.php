<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Irmag\SiteBundle\Config;

class ForumPostAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Пост')
                ->with('Контент')
                    ->add('id', null, ['disabled' => true, 'required' => false])
                    ->add('topic')
                    ->add('inReplyTo')
                    ->add('text', SimpleFormatterType::class, [
                        'format' => 'markdown',
                        'ckeditor_context' => 'default',
                        'attr' => ['rows' => 20],
                    ])
                    ->add('totalScore', null, ['disabled' => true])
                    ->add('isActive')
                ->end()
            ->end()
            ->tab('Автор')
                ->with('Автор')
                    ->add('user', ModelAutocompleteType::class, [
                        'property' => ['username', 'fullname', 'email'],
                        'to_string_callback' => function ($entity) {
                            return (string) $entity;
                        },
                    ])
                    ->add('userModifiedBy', ModelAutocompleteType::class, [
                        'required' => false,
                        'property' => ['username', 'fullname', 'email'],
                        'to_string_callback' => function ($entity) {
                            return (string) $entity;
                        },
                    ])
                    ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                ->end()
            ->end()
            ->tab('Изображения')
                ->with('Прикрепленные изображения')
                    ->add('attachments', CollectionType::class, [
                        'by_reference' => false,
                        'required' => false,
                        'label' => false,
                    ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                    ])
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('topic')
            ->add('isActive')
            ->add('user', ModelAutocompleteFilter::class, [], null, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('user')
            ->add('topic')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('updatedAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('isActive', null, ['editable' => true])
        ;
    }

    public function preUpdate($object)
    {
        $object->setPostAttachments($object->getAttachments());
        $object->setUserModifiedBy($this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser());
    }
}
