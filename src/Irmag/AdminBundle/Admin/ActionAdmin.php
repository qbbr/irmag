<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Irmag\SiteBundle\Config;
use Irmag\AdminBundle\Admin\Traits\ActivateDeactivateAdminTrait;
use Irmag\SiteBundle\Entity\Action;
//use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\Form\Type\DatePickerType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;

class ActionAdmin extends AbstractBaseAdmin
{
    use ActivateDeactivateAdminTrait;

    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Элемент')
                ->with('Основное')
                    ->add('id', null, ['required' => false])
                    ->add('slug', null, ['disabled' => true, 'required' => false])
                    ->add('title')
                    ->add('body', SimpleFormatterType::class, [
                        'format' => 'richhtml',
                        'ckeditor_context' => 'default',
                    ])
                    ->add('position')
                    ->add('isImportant')
                    ->add('isActive')
                ->end()
                ->with('Условия')
                    ->add('startDateTime', DatePickerType::class, ['format' => Config::DATETIME_FORMAT_RFC3339])
                    ->add('endDateTime', DatePickerType::class, ['format' => Config::DATETIME_FORMAT_RFC3339])
                    ->add('conditionMinSum')
                    ->add('conditionMinCount')
                    ->add('conditionPaymentMethod')
                    ->add('conditionIsForAllElements')
                    ->add('conditionMaxGifts')
                ->end()
            ->end()
            ->tab('Изображения')
                ->with('Фон баннера', ['class' => 'col-md-6'])
                    ->add('bannerBg', AdminType::class, [
                        'delete' => true,
                        'required' => false,
                        'label' => false,
                        'help' => 'Рекомендованный размер: 1920x550',
                    ])
                ->end()
                ->with('Баннер', ['class' => 'col-md-6'])
                    ->add('banner', AdminType::class, [
                        'delete' => true,
                        'required' => false,
                        'label' => false,
                        'help' => 'Рекомендованный размер: 1000x450',
                    ])
                ->end()
                ->with('Стикер')
                    ->add('sticker', AdminType::class, [
                        'delete' => true,
                        'required' => false,
                        'label' => false,
                        'help' => 'Рекомендованный размер: 60x60',
                    ])
                ->end()
            ->end()
//            ->tab('Товары')
//                ->with('Товары')
//                    ->add('elements', CollectionType::class, [
//                        'required' => false,
//                    ], [
//                        'edit' => 'inline',
//                        'inline' => 'table',
//                    ])
//                ->end()
//            ->end()
            ->tab('Другое')
                ->with('Даты')
                    ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                ->end()
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('slug')
            ->add('title')
            ->add('body')
            ->add('startDateTime')
            ->add('endDateTime')
            ->add('conditionMinSum')
            ->add('conditionMinCount')
            ->add('conditionPaymentMethod')
            ->add('conditionIsForAllElements')
            ->add('conditionMaxGifts')
            ->add('position')
            ->add('bannerBg')
            ->add('banner')
            ->add('sticker')
            ->add('isActive')
            ->add('isImportant')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('startDateTime')
            ->add('endDateTime')
            ->add('isImportant')
            ->add('isActive')
            ->add('conditionMinSum')
            ->add('conditionMinCount')
            ->add('conditionPaymentMethod')
            ->add('conditionIsForAllElements')
            ->add('conditionMaxGifts')
            ->add('createdAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->addIdentifier('sticker', null, [
                'template' => '@IrmagAdmin/list_preview_picture.html.twig',
                'width' => 60,
                'height' => 60,
            ])
            ->add('numberOfItems', 'string', ['template' => '@IrmagAdmin/Action/list_count_elements.html.twig'])
            ->add('startDateTime', null, ['format' => Config::DATETIME_FORMAT])
            ->add('endDateTime', null, ['format' => Config::DATETIME_FORMAT])
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('conditionMinSum')
            ->add('conditionMinCount')
            ->add('conditionPaymentMethod')
            ->add('conditionIsForAllElements')
            ->add('conditionMaxGifts')
            ->add('position', null, ['editable' => true])
            ->add('isImportant', null, ['editable' => true])
            ->add('isActive', null, ['editable' => true])
            ->add('', 'string', ['template' => '@IrmagAdmin/Action/list_link.html.twig'])
        ;
    }

    /**
     * @param Action $object
     */
    public function preUpdate($object)
    {
        $this->fixUploadFiles($object, ['banner', 'bannerBg', 'sticker']);
        $this->fixDeleteFiles($object, ['banner', 'bannerBg', 'sticker']);
    }

    public function configureBatchActions($actions)
    {
        return array_merge(parent::configureBatchActions($actions), $this->getActivateDeactivateBatchActions());
    }
}
