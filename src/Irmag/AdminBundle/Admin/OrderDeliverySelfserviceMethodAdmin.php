<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Irmag\CoreBundle\Utils\TokenGenerator;

class OrderDeliverySelfserviceMethodAdmin extends AbstractBaseAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    ];

    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $instance->setToken(mb_substr(TokenGenerator::generateToken(), 0, 32));

        return $instance;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true, 'required' => false])
            ->add('name')
            ->add('shortname')
            ->add('address')
            ->add('description')
            ->add('deliveryTime')
            ->add('token')
            ->add('isActive')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('shortname')
            ->add('address')
            ->add('isActive')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->addIdentifier('shortname')
            ->add('address')
            ->add('deliveryTime')
            ->add('isActive', null, ['editable' => true])
        ;
    }
}
