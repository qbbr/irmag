<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Irmag\SiteBundle\Config;

class OrderOnlinePaymentAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('create')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('Данные по платежу')
                ->add('id', null, ['disabled' => true, 'required' => false])
                ->add('provider')
                ->add('order', ModelAutocompleteType::class, [
                    'property' => ['id'],
                    'to_string_callback' => function ($entity) {
                        return (string) $entity;
                    },
                ])
                ->add('generatedId')
                ->add('acquiringId')
                ->add('status')
                ->add('declineReason', null, ['disabled' => true])
                ->add('needToSync')
            ->end()
            ->with('Суммы по платежу (в копейках)')
                ->add('sumHeld', null, ['disabled' => true])
                ->add('sumDeposited')
                ->add('sumRefund')
            ->end()
            ->with('Данные по плательщику')
                ->add('ipAddress', null, ['disabled' => true])
                ->add('cardHolderName', null, ['disabled' => true])
                ->add('cardPAN', null, ['disabled' => true])
                ->add('cardExpire', null, ['disabled' => true])
                ->add('cardBankName', null, ['disabled' => true])
                ->add('cardBankCountry', null, ['disabled' => true])
            ->end()
            ->with('Даты операций')
                ->add('registeredAt', null, ['widget' => 'single_text', 'disabled' => true, 'format' => Config::DATETIME_FORMAT_RFC3339])
                ->add('approvedAt', null, ['widget' => 'single_text', 'disabled' => true, 'format' => Config::DATETIME_FORMAT_RFC3339])
                ->add('revertedAt', null, ['widget' => 'single_text', 'disabled' => true, 'format' => Config::DATETIME_FORMAT_RFC3339])
                ->add('depositedAt', null, ['widget' => 'single_text', 'disabled' => true, 'format' => Config::DATETIME_FORMAT_RFC3339])
                ->add('refundedAt', null, ['widget' => 'single_text', 'disabled' => true, 'format' => Config::DATETIME_FORMAT_RFC3339])
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('status')
            ->add('provider')
            ->add('order', ModelAutocompleteFilter::class, [], null, [
                'property' => 'id',
            ])
            ->add('generatedId')
            ->add('acquiringId')
            ->add('createdAt')
            ->add('sumHeld')
            ->add('sumDeposited')
            ->add('sumRefund')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('provider', 'string', [
                'template' => '@IrmagAdmin/OrderOnlinePayment/list_provider.html.twig',
            ])
            ->add('order')
            ->add('acquiringId', 'string', [
                'template' => '@IrmagAdmin/OrderOnlinePayment/list_acquiring.html.twig',
            ])
            ->add('status', 'string', [
                'template' => '@IrmagAdmin/OrderOnlinePayment/list_status.html.twig',
            ])
            ->add('sumHeld')
            ->add('sumDeposited')
            ->add('sumRefund')
            ->add('cardHolderName')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }
}
