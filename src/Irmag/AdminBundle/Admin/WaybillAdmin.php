<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Irmag\SiteBundle\Config;

class WaybillAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Основное')
                ->with('Свойства', ['class' => 'col-md-6'])
                    ->add('id', null, ['disabled' => true, 'required' => false])
                    ->add('driver', ModelAutocompleteType::class, [
                        'property' => ['username', 'fullname', 'email'],
                        'to_string_callback' => function ($entity) {
                            return (string) $entity;
                        },
                    ])
                    ->add('date')
                    ->add('quantityOfExtraDelivery')
                    ->add('quantityOutOfCity')
                    ->add('shift')
                    ->add('mileage')
                    ->add('isCompleted')
                    ->add('comment')
                ->end()
                ->with('Суммы', ['class' => 'col-md-6'])
                    ->add('totalSum')
                    ->add('totalSum')
                    ->add('sumCash')
                    ->add('sumCard')
                    ->add('sumCertificate')
                    ->add('sumRs')
                    ->add('sumOnline')
                ->end()
            ->end()
            ->tab('Заказы')
                ->with('Заказы')
                    ->add('orders', CollectionType::class, [
                        'label' => false,
                        'btn_add' => false,
                        'type_options' => ['delete' => false],
                    ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                    ])
                ->end()
            ->end()
            ->tab('Другое')
                ->with('Даты')
                    ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                ->end()
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('shift')
            ->add('totalSum')
            ->add('sumCash')
            ->add('sumCard')
            ->add('sumCertificate')
            ->add('sumRs')
            ->add('sumOnline')
            ->add('driver')
            ->add('date', null, ['format' => Config::DATE_FORMAT])
            ->add('comment')
            ->add('quantityOfExtraDelivery')
            ->add('quantityOutOfCity')
            ->add('mileage')
            ->add('isCompleted')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('updatedAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('shift')
            ->add('totalSum')
            ->add('sumCash')
            ->add('sumCard')
            ->add('sumCertificate')
            ->add('sumRs')
            ->add('sumOnline')
            ->add('driver', ModelAutocompleteFilter::class, [], null, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('date')
            ->add('quantityOfExtraDelivery')
            ->add('quantityOutOfCity')
            ->add('mileage')
            ->add('isCompleted')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('shift')
            ->add('totalSum')
            ->add('sumCash')
            ->add('sumCard')
            ->add('sumCertificate')
            ->add('sumRs')
            ->add('sumOnline')
            ->add('driver')
            ->add('date', null, ['format' => Config::DATE_FORMAT])
            ->add('quantityOfExtraDelivery')
            ->add('quantityOutOfCity')
            ->add('mileage')
            ->add('isCompleted')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('updatedAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }
}
