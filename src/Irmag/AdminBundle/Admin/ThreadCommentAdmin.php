<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Irmag\SiteBundle\Config;
use Irmag\AdminBundle\Admin\Traits\ActivateDeactivateAdminTrait;
use Irmag\ThreadCommentBundle\Entity\Thread;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ThreadCommentAdmin extends AbstractBaseAdmin
{
    use ActivateDeactivateAdminTrait;

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('create')
            ->add('pay_bonus', $this->getRouterIdParameter().'/pay_bonus')
        ;
    }

    public function createQuery($context = 'list')
    {
        /** @var \Doctrine\ORM\QueryBuilder $query */
        $query = parent::createQuery($context);

        $alias = $query->getAllAliases()[0];
        $query
            ->addSelect('u, t')
            ->leftJoin($alias.'.user', 'u')
            ->leftJoin($alias.'.thread', 't')
        ;

        return $query;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true, 'required' => false])
            ->add('user', ModelAutocompleteType::class, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('text')
            ->add('textHtml', null, ['disabled' => true])
            ->add('score')
            ->add('rating')
            ->add('isBonusPayed')
            ->add('isPurchasedFromUs')
            ->add('isActive')
            ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
            ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('user', ModelAutocompleteFilter::class, [], null, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('thread.ownerFQCN', null, [], ChoiceType::class, [
                'choices' => Thread::getChoiceList(),
            ])
            ->add('isBonusPayed')
            ->add('isPurchasedFromUs')
            ->add('isActive')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('user')
            ->add('textHtml')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('isActive')
            ->add('isPurchasedFromUs')
            ->add('_action', null, [
                'actions' => [
                    'pay_bonus' => [
                        'template' => '@IrmagAdmin/ThreadComment/list__action_pay_bonus.html.twig',
                    ],
                ],
            ])
        ;
    }

    public function configureBatchActions($action)
    {
        return array_merge(parent::configureBatchActions($action), $this->getActivateDeactivateBatchActions());
    }
}
