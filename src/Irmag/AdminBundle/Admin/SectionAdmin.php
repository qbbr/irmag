<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Irmag\SiteBundle\Config;

class SectionAdmin extends AbstractBaseAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('create')
            ->add('tree')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Элемент')
                ->with('Основное')
                    ->add('id', null, ['disabled' => true, 'required' => false])
                    ->add('name')
                    ->add('parent')
                    ->add('seoKeywords')
                    ->add('seoDescription')
                    ->add('seoTitle')
                    ->add('seoH1')
                    ->add('elementsCount', null, ['disabled' => true])
                    ->add('isActive')
                ->end()
            ->end()
            ->tab('Другое')
                ->with('Даты')
                    ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('isActive')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->add('isActive', null, ['editable' => true])
            ->add('', 'string', ['template' => '@IrmagAdmin/Catalog/list_section_link.html.twig'])
        ;
    }
}
