<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Irmag\SiteBundle\Config;
use Irmag\BlogBundle\EventDispatcher\BlogPostEventDispatcher;
use Irmag\BlogBundle\Entity\BlogPost;

class BlogPostAdmin extends AbstractBaseAdmin
{
    public function getNewInstance()
    {
        $instance = parent::getNewInstance();
        $user = $this->getConfigurationPool()->getContainer()->get('security.token_storage')->getToken()->getUser();
        $instance->setUser($user);

        return $instance;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Пост')
                ->with('Автор')
                    ->add('user', ModelAutocompleteType::class, [
                        'property' => ['username', 'fullname', 'email'],
                        'to_string_callback' => function ($entity) {
                            return (string) $entity;
                        },
                    ])
                    ->add('userModifiedBy', ModelAutocompleteType::class, [
                        'property' => ['username', 'fullname', 'email'],
                        'required' => false,
                        'to_string_callback' => function ($entity) {
                            return (string) $entity;
                        },
                    ])
                ->end()
                ->with('Контент')
                    ->add('id', null, ['disabled' => true, 'required' => false])
                    ->add('title')
                    ->add('subtitle')
                    ->add('text', SimpleFormatterType::class, [
                        'format' => 'markdown',
                        'ckeditor_context' => 'default',
                        'attr' => ['rows' => 20],
                    ])
                    ->add('status', ChoiceType::class, [
                        'choices' => array_flip($this->getSubject()->statusNames),
                    ])
                    ->add('blogSection')
                ->end()
            ->end()
            ->tab('Товары')
                ->with('Прикреплённые товары')
                    ->add('postElements', CollectionType::class, [
                        'type_options' => [
                            'delete' => true,
                        ],
                    ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                    ])
                ->end()
            ->end()
            ->tab('Теги')
                ->with('Теги')
                    ->add('blogPostTags')
                ->end()
            ->end()
            ->tab('Изображения')
                ->with('Прикрепленные изображения')
                    ->add('attachments', CollectionType::class, [
                        'by_reference' => false,
                        'required' => false,
                        'label' => false,
                    ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                    ])
                ->end()
            ->end()
            ->tab('Модерация')
                ->with('Отклонение поста')
                    ->add('declineReason')
                ->end()
                ->with('Прочее')
                    ->add('isNotifyUser', CheckboxType::class, [
                        'mapped' => false,
                        'required' => false,
                        'data' => true,
                        'label' => 'Отправить автору письмо о статусе модерации',
                    ])
                    ->add('views', null, ['disabled' => true])
                    ->add('rating', null, ['label' => 'Рейтинг поста', 'disabled' => true])
                    ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('isActive')
                    ->add('isBonusPayed')
                ->end()
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('status')
            ->add('user', ModelAutocompleteFilter::class, [], null, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('title')
            ->add('subtitle')
            ->add('blogSection')
            ->add('createdAt')
            ->add('isActive')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->addIdentifier('subtitle')
            ->add('user')
            ->add('status', 'string', ['template' => '@IrmagAdmin/BlogPost/blog-post-status-in-list.html.twig'])
            ->add('blogSection')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('isActive', null, ['editable' => true])
        ;
    }

    /**
     * @param BlogPost $object
     */
    public function preUpdate($object)
    {
        $object->setPostElements($object->getPostElements());
        $object->setPostAttachments($object->getAttachments());
    }

    /**
     * @param BlogPost $object
     */
    public function postUpdate($object)
    {
        $needToNotify = $this->getForm()->get('isNotifyUser')->getData();

        if (true === $needToNotify) {
            $this->getConfigurationPool()->getContainer()->get(BlogPostEventDispatcher::class)->dispatchBlogPostStatusChanged($object);
        }
    }
}
