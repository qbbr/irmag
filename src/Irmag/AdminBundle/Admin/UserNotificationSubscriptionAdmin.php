<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;

class UserNotificationSubscriptionAdmin extends AbstractBaseAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true, 'required' => false])
            ->add('user', ModelAutocompleteType::class, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('event', ModelAutocompleteType::class, [
                'property' => ['name', 'description'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('isPushEnabled')
            ->add('isEmailEnabled')
            ->add('isSiteEnabled')
            ->add('isSmsEnabled')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('user')
            ->add('event')
            ->add('isPushEnabled')
            ->add('isEmailEnabled')
            ->add('isSiteEnabled')
            ->add('isSmsEnabled')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('user', ModelAutocompleteFilter::class, [], null, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('event', ModelAutocompleteFilter::class, [], null, [
                'property' => ['name', 'description'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('isPushEnabled')
            ->add('isEmailEnabled')
            ->add('isSiteEnabled')
            ->add('isSmsEnabled')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('user')
            ->add('event')
            ->add('isPushEnabled', null, ['editable' => true])
            ->add('isEmailEnabled', null, ['editable' => true])
            ->add('isSiteEnabled', null, ['editable' => true])
            ->add('isSmsEnabled', null, ['editable' => true])
        ;
    }
}
