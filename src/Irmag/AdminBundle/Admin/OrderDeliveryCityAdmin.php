<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\CollectionType;

class OrderDeliveryCityAdmin extends AbstractBaseAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true, 'required' => false])
            ->add('name')
            ->add('shortname')
            ->add('districts', CollectionType::class, [
                'by_reference' => false,
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('deliveryMethod', null, ['required' => true])
            ->add('selfserviceMethod', CollectionType::class, [
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('deliveryPrice')
            ->add('extraPriceForFastDeliveryTime')
            ->add('minPriceForFreeDelivery')
            ->add('paymentMethod', null, ['required' => true])
            ->add('banDates', CollectionType::class, [
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('deliveryCityDayOfWeek', CollectionType::class, [
            ], [
                'edit' => 'inline',
                'inline' => 'table',
            ])
            ->add('position')
            ->add('isActive')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('shortname')
            ->add('deliveryMethod')
            ->add('paymentMethod')
            ->add('isActive')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->addIdentifier('shortname')
            ->add('deliveryMethod')
            ->add('paymentMethod')
            ->add('position', null, ['editable' => true])
            ->add('isActive', null, ['editable' => true])
        ;
    }
}
