<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Route\RouteCollection;

class BlogPostElementAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('element', ModelAutocompleteType::class, [
                'property' => ['name'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('rate')
        ;
    }
}
