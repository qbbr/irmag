<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Form\Type\AdminType;

class ElementManufacturerAdmin extends AbstractBaseAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true, 'required' => false])
            ->add('name')
            ->add('picture', AdminType::class, [
                'delete' => true,
                'required' => false,
            ])
            ->add('seoKeywords')
            ->add('seoDescription')
            ->add('seoTitle')
            ->add('seoH1')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->addIdentifier('picture', null, [
                'template' => '@IrmagAdmin/list_preview_picture.html.twig',
                'width' => 172,
                'height' => 50,
            ])
        ;
    }

    /**
     * @param \Irmag\SiteBundle\Entity\ElementManufacturer $object
     */
    public function preUpdate($object)
    {
        $this->fixUploadFiles($object, ['picture']);
        $this->fixDeleteFiles($object, ['picture']);
    }
}
