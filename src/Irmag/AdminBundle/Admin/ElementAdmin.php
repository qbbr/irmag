<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Irmag\SiteBundle\Entity\Section;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\PromocodeElementsList;
use Irmag\SiteBundle\Config;
use Irmag\AdminBundle\Admin\Traits\ActivateDeactivateAdminTrait;

class ElementAdmin extends AbstractBaseAdmin
{
    use ActivateDeactivateAdminTrait;

    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'updatedAt',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
        $collection->remove('create');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        switch ($this->getRoot()->getClass()) {
            case PromocodeElementsList::class:
                $this->configureFormFieldsForPromocodeElementList($formMapper);
                break;
            default:
                $this->configureFormFieldsByDefault($formMapper);
                break;
        }
    }

    private function configureFormFieldsByDefault(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Элемент')
                ->with('Основное')
                    ->add('id', null, ['disabled' => true, 'required' => false])
                    ->add('name')
                    ->add('toneName')
                    ->add('price')
                    ->add('inStock')
                    ->add('inPackage')
                    ->add('isActive')
                    ->add('isPreOrderZone')
                    ->add('isOnlyForHome')
                ->end()
                ->with('Разделы / группировка')
                    ->add('sectionElements', CollectionType::class, [
                        'required' => false,
                        'label' => false,
                    ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                    ])
                    ->add('groupBy')
                    ->add('abcGroup')
                    ->add('series')
                ->end()
            ->end()
            ->tab('Тексты')
                ->with('Тексты')
                    ->add('description', SimpleFormatterType::class, [
                        'format' => 'richhtml',
                        'ckeditor_context' => 'default',
                    ])
                    ->add('composition', SimpleFormatterType::class, [
                        'format' => 'richhtml',
                        'ckeditor_context' => 'default',
                        'required' => false,
                    ])
                    ->add('howToUse', SimpleFormatterType::class, [
                        'format' => 'richhtml',
                        'ckeditor_context' => 'default',
                        'required' => false,
                    ])
                ->end()
            ->end()
            ->tab('Свойства')
                ->with('Свойства', ['class' => 'col-md-6'])
                    ->add('brand')
                    ->add('manufacturer')
                    ->add('country')
                    ->add('barcode')
                    ->add('weight')
                    ->add('volume')
                    ->add('tableSize')
                    ->add('videoUrl')
                ->end()
                ->with('Свойства - иконки', ['class' => 'col-md-6'])
                    ->add('propEco')
                    ->add('propSpecialPrice')
                    ->add('propSafeAnimal')
                    ->add('propTransfer')
                ->end()
            ->end()
            ->tab('Изображения')
                ->with('Основные', ['description' => 'Рекомендованный размер: 999x999 px.'])
                    ->add('pictures', CollectionType::class, [
                        'by_reference' => false,
                        'required' => false,
                        'label' => false,
                    ], [
                        'edit' => 'inline',
                        'inline' => 'table',
                    ])
                ->end()
                ->with('Тон')
                    ->add('tonePicture', AdminType::class, [
                        'delete' => true,
                        'required' => false,
                    ])
                ->end()
            ->end()
            ->tab('Другое')
                ->with('Даты')
                    ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                ->end()
            ->end()
        ;
    }

    private function configureFormFieldsForPromocodeElementList(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true])
            ->add('name', null, ['disabled' => true])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('name')
            ->add('toneName')
            ->add('price')
            ->add('sections', CallbackFilter::class,
                [
                    'callback' => function ($qb, $alias, $field, $value) {
                        if (!$value['value'] || !$value['value'] instanceof Section) {
                            return false;
                        }

                        /** @var Section $section */
                        $section = $value['value'];

                        $qb
                            ->leftJoin(sprintf('%s.sectionElements', $alias), 'se')
                            ->leftJoin('se.section', 's')
                            ->andWhere('s.lft >= :lft')
                            ->andWhere('s.rgt <= :rgt')
                            ->andWhere('s.root = :root')
                            ->setParameters([
                                'lft' => $section->getLft(),
                                'rgt' => $section->getRgt(),
                                'root' => $section->getRoot(),
                            ]);

                        return true;
                    },
                ],
                EntityType::class,
                [
                    'class' => Section::class,
                ]
            )
            ->add('brand', ModelAutocompleteFilter::class, [], null, [
                'property' => ['name'],
            ])
            ->add('manufacturer', ModelAutocompleteFilter::class, [], null, [
                'property' => ['name'],
            ])
            ->add('country', ModelAutocompleteFilter::class, [], null, [
                'property' => ['name'],
            ])
            ->add('barcode')
            ->add('weight')
            ->add('volume')
            ->add('tableSize')
            ->add('videoUrl')
            ->add('propEco')
            ->add('propSpecialPrice')
            ->add('propSafeAnimal')
            ->add('propTransfer')
            ->add('isPreOrderZone')
            ->add('isActive')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('picture', null, ['template' => '@IrmagAdmin/Catalog/list_element_preview_picture.html.twig'])
            ->addIdentifier('id')
            ->addIdentifier('name')
            ->addIdentifier('toneName')
            ->add('sections', null, ['template' => '@IrmagAdmin/Catalog/list_element_sections_names.html.twig'])
            ->add('price')
            ->add('inStock')
            ->add('isActive', null, ['editable' => true])
            ->add('', 'string', ['template' => '@IrmagAdmin/Catalog/list_element_link.html.twig']);
    }

    /**
     * @param Element $object
     */
    public function preUpdate($object)
    {
        $this->fixUploadFiles($object, ['tonePicture']);
        $this->fixDeleteFiles($object, ['tonePicture']);
    }

    public function configureBatchActions($actions)
    {
        return array_merge(parent::configureBatchActions($actions), $this->getActivateDeactivateBatchActions());
    }
}
