<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Irmag\SiteBundle\Config;
use Irmag\ProfileBundle\Form\Type\GenderType;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Service\UserBlockHistoryManager\UserBlockHistoryManager;

class UserAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->add('change_user_activity', $this->getRouterIdParameter().'/change_user_activity')
            ->add('block_on_forum', $this->getRouterIdParameter().'/block_on_forum')
            ->add('block_on_element_comments', $this->getRouterIdParameter().'/block_on_element_comments')
            ->add('block_on_news_comments', $this->getRouterIdParameter().'/block_on_news_comments')
            ->add('block_on_blog_comments', $this->getRouterIdParameter().'/block_on_blog_comments')
            ->add('block_on_action_comments', $this->getRouterIdParameter().'/block_on_action_comments')
        ;
    }

    public function createQuery($context = 'list')
    {
        /** @var \Doctrine\ORM\QueryBuilder $query */
        $query = parent::createQuery($context);

        $alias = $query->getAllAliases()[0];
        $query
            ->addSelect('pr', 'nr', 'g')
            ->leftJoin($alias.'.groups', 'g')
            ->leftJoin($alias.'.positiveRoles', 'pr')
            ->leftJoin($alias.'.negativeRoles', 'nr');

        return $query;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Элемент')
                ->with('Личные данные', ['class' => 'col-md-6'])
                    ->add('id', null, ['disabled' => true, 'required' => false])
                    ->add('username', null, ['disabled' => true, 'required' => false])
                    ->add('fullname')
                    ->add('isActive')
                    ->add('birthday', BirthdayType::class, ['required' => false])
                    ->add('profession')
                    ->add('about')
                    ->add('gender', GenderType::class, ['translation_domain' => 'irmag_profile', 'placeholder' => 'Выбрать'])
                    ->add('addressCity')
                ->end()
                ->with('Avatar', ['class' => 'col-md-6'])
                    ->add('avatar', AdminType::class, [
                        'delete' => true,
                        'required' => false,
                    ])
                    ->add('gravatar', null, ['disabled' => true])
                ->end()
                ->with('Контакты', ['class' => 'col-md-6'])
                    ->add('email')
                    ->add('isEmailInvalid', null, ['disabled' => true])
                    ->add('isEmailSubscribed')
                    ->add('telephone')
                    ->add('isSmsSubscribed')
                ->end()
                ->with('Свойства', ['class' => 'col-md-6'])
                    ->add('discount')
                    ->add('spendMoney')
                    ->add('bonus')
                    ->add('isPrePaymentOnly')
                ->end()
                ->with('Форум', ['class' => 'col-md-6'])
                    ->add('forumRating')
                ->end()
            ->end()
            ->tab('Группы и роли')
                ->with('Группы и роли')
                    ->add('groups')
                    ->add('positiveRoles')
                    ->add('negativeRoles')
                ->end()
            ->end()
            ->tab('Пароль')
                ->with('Изменить пароль')
                    ->add('plainPassword', TextType::class, ['required' => false])
                ->end()
            ->end()
            ->tab('Другое')
                ->with('Даты')
                    ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('lastLoginAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                ->end()
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('avatar', null, ['template' => '@IrmagAdmin/User/show_avatar_preview.html.twig'])
            ->add('username')
            ->add('fullname')
            ->add('email')
            ->add('isEmailSubscribed')
            ->add('telephone')
            ->add('isSmsSubscribed')
            ->add('birthday', null, ['format' => Config::DATE_FORMAT])
            ->add('about')
            ->add('gender')
            ->add('addressCity')
            ->add('discount')
            ->add('spendMoney')
            ->add('bonus')
            ->add('forumRating')
            ->add('isActive')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('updatedAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('lastLoginAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('username')
            ->add('email')
            ->add('fullname')
            ->add('telephone')
            ->add('isEmailSubscribed')
            ->add('isSmsSubscribed')
            ->add('birthday')
            ->add('gender')
            ->add('addressCity')
            ->add('discount')
            ->add('spendMoney')
            ->add('bonus')
            ->add('groups')
            ->add('forumRating')
            ->add('isEmailInvalid')
            ->add('isActive')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('lastLoginAt')
            ->add('isPrePaymentOnly')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('avatar', null, ['template' => '@IrmagAdmin/User/list_avatar_preview.html.twig'])
            ->addIdentifier('username')
            ->addIdentifier('email')
            ->addIdentifier('fullname')
            ->addIdentifier('telephone')
            ->addIdentifier('bonus')
            ->add('isActive', null, ['editable' => true])
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('lastLoginAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('', null, ['template' => '@IrmagAdmin/User/list_user_actions.html.twig'])
        ;
    }

    public function getExportFields()
    {
        $defaultExportFields = parent::getExportFields();

        unset($defaultExportFields[array_search('password', $defaultExportFields, true)]);

        return $defaultExportFields;
    }

    /**
     * @param User $object
     */
    public function prePersist($object)
    {
        $this->fixUploadFiles($object, ['avatar']);
    }

    /**
     * @param User $object
     */
    public function preUpdate($object)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $plainPassword = $object->getPlainPassword();

        if (!empty($plainPassword)) {
            // назначаем новый пароль
            $encoder = $container->get('security.password_encoder');
            $pwd = $encoder->encodePassword($object, $plainPassword);
            $object->setPassword($pwd);
        }

        $em = $container->get('doctrine.orm.default_entity_manager');
        $originalObjectData = $em->getUnitOfWork()->getOriginalEntityData($object);

        // если менялось isActive, то записать в журнал бана
        if ($originalObjectData['isActive'] !== $object->getIsActive()) {
            $userBlockHistoryManager = $container->get(UserBlockHistoryManager::class);
            $userBlockHistoryManager->blockUser($object, false, !$object->getIsActive());
        }

        $this->fixUploadFiles($object, ['avatar']);
        $this->fixDeleteFiles($object, ['avatar']);
    }
}
