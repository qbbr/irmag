<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Irmag\SiteBundle\Entity\PromocodeElementsList;
use Irmag\SiteBundle\Config;

class PromocodeAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        switch ($this->getRoot()->getClass()) {
            case PromocodeElementsList::class:
                $this->configureFormFieldsForPromocodeElementAdmin($formMapper);
                break;
            default:
                $this->configureFormFieldsByDefault($formMapper);
                break;
        }
    }

    private function configureFormFieldsByDefault(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Элемент')
                ->with('Основное')
                    ->add('id', null, ['disabled' => true, 'required' => false])
                    ->add('code')
                    ->add('description')
                    ->add('discount')
                    ->add('isActive')
                    ->add('isPersonal')
                    ->add('bonus')
                    ->addHelp('bonus', 'Если указаны фантики, то это БОНУС ДОНАТ.')
                ->end()
            ->end()
            ->tab('Другое')
                ->with('Даты')
                    ->add('activatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                ->end()
            ->end()
            ->tab('На товары')
                ->with('Товары по промокодам')
                    ->add('elementList', ModelAutocompleteType::class, [
                        'required' => false,
                        'property' => ['id', 'uuid1C'],
                        'label' => 'ID списка в 1С',
                    ])
                ->end()
            ->end()
        ;
    }

    private function configureFormFieldsForPromocodeElementAdmin(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true, 'required' => false])
            ->add('code')
            ->add('discount')
            ->add('isActive')
            ->add('isPersonal')
            ->add('bonus')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('code')
            ->add('discount')
            ->add('isActive')
            ->add('isPersonal')
            ->add('bonus')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('code')
            ->add('discount')
            ->add('description')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('activatedAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('isActive', null, ['editable' => true])
            ->add('isPersonal', null, ['editable' => true])
            ->add('bonus')
        ;
    }
}
