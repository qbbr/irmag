<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class OrderDeliveryCityOrderDeliverySelfserviceMethodAdmin extends AbstractBaseAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true, 'required' => false])
            ->add('deliveryCity')
            ->add('selfservice')
            ->add('paymentMethod')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('deliveryCity')
            ->add('selfservice')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('deliveryCity')
            ->add('selfservice')
        ;
    }
}
