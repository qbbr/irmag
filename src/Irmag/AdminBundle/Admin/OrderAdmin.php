<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\Form\Type\DatePickerType;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Irmag\SiteBundle\Config;
use Irmag\SiteBundle\Entity\Order;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

class OrderAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('create')
        ;
    }

    public function createQuery($context = 'list')
    {
        /** @var \Doctrine\ORM\QueryBuilder $query */
        $query = parent::createQuery($context);

        $alias = $query->getAllAliases()[0];
        $query
            ->addSelect('u', 'dsd', 'pdd')
            ->leftJoin($alias.'.user', 'u')
            ->leftJoin($alias.'.deliveryServiceData', 'dsd')
            ->leftJoin($alias.'.postDeliveryData', 'pdd')
        ;

        return $query;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        if ($this->getParentFieldDescription()) {
            $formMapper
                ->add('id', null, ['disabled' => true])
                ->add('payerUsername', null, ['disabled' => true])
            ;
        } else {
            /** @var Order $order */
            $order = $this->getSubject();
            $deliveryMethod = $order->getDeliveryMethod();
            $paymentMethod = $order->getPaymentMethod();
            $deliveryData = $order->getDeliveryServiceData();
            $deliveryServiceTypes = !empty($deliveryData) ? $deliveryData->getTypes() : [];

            $formMapper
                ->tab('Заказчик')
                    ->with('Покупатель')
                        ->add('id', null, ['disabled' => true, 'required' => false])
                        ->add('user', ModelAutocompleteType::class, [
                            'property' => ['username', 'fullname', 'email'],
                            'to_string_callback' => function ($entity) {
                                return (string) $entity;
                            },
                        ])
                        ->add('payerUsername')
                        ->add('payerTelephone')
                        ->add('payerEmail')
                        ->add('checkDeliveryMethod', ChoiceType::class, ['multiple' => false, 'choices' => [
                            'не выбрано' => null,
                            'email' => 'email',
                            'sms' => 'sms',
                        ]])
                        ->add('isAgreementAccepted', null, ['disabled' => true])
                    ->end()
                    ->with('Юридические данные')
                        ->add('isPayerLegal')
                        ->add('payerLegalAddress', null, ['required' => false])
                        ->add('payerLegalActualAddress', null, ['required' => false])
                        ->add('payerLegalConsigneeAddress', null, ['required' => false])
                        ->add('payerLegalName', null, ['required' => false])
                        ->add('payerLegalInn', null, ['required' => false])
                        ->add('payerLegalKpp', null, ['required' => false])
                        ->add('payerLegalOgrn', null, ['required' => false])
                        ->add('payerLegalRs', null, ['required' => false])
                        ->add('payerLegalKrs', null, ['required' => false])
                        ->add('payerLegalRb', null, ['required' => false])
                        ->add('payerLegalRbCity', null, ['required' => false])
                        ->add('payerLegalBik', null, ['required' => false])
                        ->add('payerLegalTelephone', null, ['required' => false])
                    ->end()
                ->end()
                ->tab('Доставка')
                    ->with('Служба доставки')
                        ->add('deliveryDate', DatePickerType::class)
                        ->add('deliveryMethod')
                        ->add('deliveryTime')
                        ->add('deliveryPrice')
                        ->ifTrue(null !== $order->getIsRemoteDeliveryCompleted())
                            ->add('isRemoteDeliveryCompleted')
                        ->ifEnd()
                    ->end()
                    ->with('Способ самовывоза')
                        ->ifTrue($deliveryMethod && 'selfservice' === $deliveryMethod->getShortname())
                            ->add('selfserviceMethod')
                        ->ifEnd()
                    ->end()
                    ->with('Доставка транспортной компанией')
                        ->ifTrue(!empty($deliveryData))
                            ->add('deliveryServiceData.orderDeliveryService', null, ['label' => 'Транспортная компания'])
                            ->add('deliveryServiceData.orderDeliveryServiceCity', null, ['label' => 'Населённый пункт'])
                            ->add('deliveryServiceData.orderDeliveryServiceCity.orderDeliveryServiceRegion', null, ['label' => 'Регион', 'disabled' => true])
                            ->add('deliveryServiceData.type', ChoiceType::class, [
                                'label' => 'Тип доставки',
                                'choices' => array_flip($deliveryServiceTypes),
                            ])
                            ->add('deliveryServiceData.deliveryPoint', null, ['label' => 'Код терминала'])
                            ->add('deliveryServiceData.deliveryPointAddress', null, ['label' => 'Адрес терминала'])
                            ->add('deliveryServiceData.tariff', null, ['label' => 'Тариф', 'disabled' => true])
                            ->add('deliveryServiceData.cost', null, ['label' => 'Стоимость доставки', 'disabled' => true])
                            ->add('deliveryServiceData.days', null, ['label' => 'Дней доставки', 'disabled' => true])
                            ->add('deliveryServiceData.trackCode', null, ['label' => 'Трек-код'])
                        ->ifEnd()
                    ->end()
                    ->with('Доставка Почтой России')
                        ->ifTrue(!empty($order->getPostDeliveryData()))
                            ->add('postDeliveryData.deliveryIndex', null, ['label' => 'Индекс доставки'])
                            ->add('postDeliveryData.deliveryCity', null, ['label' => 'Населённый пункт'])
                            ->add('postDeliveryData.cost', null, ['label' => 'Стоимость доставки', 'disabled' => true])
                            ->add('postDeliveryData.weight', null, ['label' => 'Вес отправления (гр.)', 'disabled' => true])
                            ->add('postDeliveryData.minDays', null, ['label' => 'Дней доставки (минимум)', 'disabled' => true])
                            ->add('postDeliveryData.maxDays', null, ['label' => 'Дней доставки (максимум)', 'disabled' => true])
                            ->add('postDeliveryData.trackCode', null, ['label' => 'Трек-код отправления'])
                        ->ifEnd()
                    ->end()
                    ->with('Адрес доставки')
                        ->add('deliveryCity')
                        ->add('deliveryDistrict', null, ['required' => false])
                        ->add('deliveryStreet', null, ['required' => false])
                        ->add('deliveryHouse', null, ['required' => false])
                        ->add('deliveryApartment', null, ['required' => false])
                        ->add('deliveryComment')
                    ->end()
                    ->with('Путёвка')
                        ->add('waybill', ModelAutocompleteType::class, [
                            'required' => false,
                            'minimum_input_length' => 1,
                            'property' => ['id'],
                            'to_string_callback' => function ($entity) {
                                return (string) $entity;
                            },
                        ])
                    ->end()
                ->end()
                ->tab('Оплата')
                    ->with('Оплата')
                        ->add('paymentMethod')
                    ->end()
                    ->ifTrue($paymentMethod && 'mixed' === $paymentMethod->getShortname() && $order->getMixedPayment())
                        ->with('Смешанный платёж')
                            ->add('mixedPayment.sumCash', null, ['label' => 'Наличными'])
                            ->add('mixedPayment.sumCard', null, ['label' => 'Картой'])
                            ->add('mixedPayment.sumCertificate', null, ['label' => 'Сертификатом'])
                        ->end()
                    ->ifEnd()
                ->end()
                ->tab('Статус')
                    ->with('Статус')
                        ->add('status')
                    ->end()
                ->end()
                ->tab('Товары')
                    ->with('Список товаров')
                        ->add('totalPrice', null, ['scale' => 2])
                        ->add('totalPriceWithoutDiscount', null, ['scale' => 2])
                        ->add('spendBonus')
                        ->add('promocodeDiscount')
                        ->add('promocode')
                        ->add('orderElement', CollectionType::class, [
                            'label' => false,
                            'btn_add' => false,
                            'type_options' => ['delete' => false],
                        ], [
                            'edit' => 'inline',
                            'inline' => 'table',
                        ])
                    ->end()
                ->end()
                ->tab('Другое')
                    ->with('Даты')
                        ->add('createdAt', null, [
                            'format' => Config::DATETIME_FORMAT_RFC3339,
                            'widget' => 'single_text',
                            'required' => false,
                            'disabled' => true,
                        ])
                        ->add('updatedAt', null, [
                            'format' => Config::DATETIME_FORMAT_RFC3339,
                            'widget' => 'single_text',
                            'required' => false,
                            'disabled' => true,
                        ])
                    ->end()
                    ->with('Родитель')
                        ->add('parentId', IntegerType::class, [
                            'disabled' => true,
                            'required' => false,
                            'label' => false,
                        ])
                        ->setHelps(['parentId' => 'для дозаказов'])
                    ->end()
                ->end()
            ;
        }
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('user', ModelAutocompleteFilter::class, [], null, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('totalPrice')
            ->add('payerUsername')
            ->add('payerTelephone')
            ->add('payerEmail')
            ->add('deliveryMethod')
            ->add('deliveryDate')
            ->add('deliveryTime')
            ->add('paymentMethod')
            ->add('selfserviceMethod')
            ->add('status')
            ->add('isPayerLegal')
            ->add('parent', ModelAutocompleteFilter::class, [], null, [
                'property' => ['id'],
            ])
            ->add('spendBonus')
            ->add('promocodeDiscount')
            ->add('promocode')
            ->add('device')
            ->add('isRemoteDeliveryCompleted')
            ->add('createdAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('user')
            ->add('totalPrice')
            ->add('deliveryMethod')
            ->add('deliveryDate', null, ['format' => Config::DATE_FORMAT])
            ->add('deliveryTime')
            ->add('paymentMethod')
            ->add('selfserviceMethod')
            ->add('status')
            ->add('isPayerLegal')
            ->add('parent')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }
}
