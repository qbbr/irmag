<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Irmag\SiteBundle\Config;

class UserLoginAttemptsAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
            ->remove('edit')
            ->remove('create')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('username')
            ->add('ip')
            ->add('createdAt')
            ->add('updatedAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('username')
            ->add('attempts')
            ->add('ip')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('updatedAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }
}
