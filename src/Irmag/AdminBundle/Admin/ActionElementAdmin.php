<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;

class ActionElementAdmin extends AbstractBaseAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('action')
            ->add('element', ModelAutocompleteType::class, [
                'property' => 'name',
            ])
            ->add('price')
        ;
    }
}
