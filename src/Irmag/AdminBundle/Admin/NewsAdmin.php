<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Irmag\SiteBundle\Config;
use Irmag\SiteBundle\Entity\News;
use Irmag\AdminBundle\Admin\Traits\ActivateDeactivateAdminTrait;

class NewsAdmin extends AbstractBaseAdmin
{
    use ActivateDeactivateAdminTrait;

    protected $datagridValues = [
        '_sort_order' => 'ASC',
        '_sort_by' => 'position',
    ];

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->tab('Элемент')
                ->with('Основное')
                    ->add('id', null, ['disabled' => true, 'required' => false])
                    ->add('slug', null, ['disabled' => true, 'required' => false])
                    ->add('title')
                    ->add('body', SimpleFormatterType::class, [
                        'format' => 'richhtml',
                        'ckeditor_context' => 'default',
                    ])
                    ->add('position')
                    ->add('isImportant')
                    ->add('isShowInIndexBanners')
                    ->add('isActive')
                ->end()
                ->with('Дополнительно')
                    ->add('extraContentOnBanner', SimpleFormatterType::class, [
                        'format' => 'richhtml',
                        'ckeditor_context' => 'default',
                        'required' => false,
                    ])
                ->end()
            ->end()
            ->tab('Изображения')
                ->with('Фон баннера', ['class' => 'col-md-6'])
                    ->add('bannerBg', AdminType::class, [
                        'delete' => true,
                        'required' => false,
                        'label' => false,
                        'help' => 'Рекомендованный размер: 1920x550',
                    ])
                ->end()
                ->with('Баннер', ['class' => 'col-md-6'])
                    ->add('banner', AdminType::class, [
                        'delete' => true,
                        'required' => false,
                        'label' => false,
                        'help' => 'Рекомендованный размер: 1000x450',
                    ])
                ->end()
            ->end()
            ->tab('Другое')
                ->with('Даты')
                    ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                    ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
                ->end()
            ->end()
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('slug')
            ->add('title')
            ->add('body')
            ->add('extraContentOnBanner')
            ->add('banner')
            ->add('bannerBg')
            ->add('position')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('isImportant')
            ->add('isShowInIndexBanners')
            ->add('isActive')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('isImportant')
            ->add('isShowInIndexBanners')
            ->add('isActive')
            ->add('createdAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('title')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('isImportant', null, ['editable' => true])
            ->add('isShowInIndexBanners', null, ['editable' => true])
            ->add('position', null, ['editable' => true])
            ->add('isActive', null, ['editable' => true])
            ->add('', 'string', ['template' => '@IrmagAdmin/News/list_link.html.twig'])
        ;
    }

    /**
     * @param News $object
     */
    public function prePersist($object)
    {
        $this->fixUploadFiles($object, ['banner', 'bannerBg']);
    }

    /**
     * @param News $object
     */
    public function preUpdate($object)
    {
        $this->fixUploadFiles($object, ['banner', 'bannerBg']);
        $this->fixDeleteFiles($object, ['banner', 'bannerBg']);
    }

    public function configureBatchActions($actions)
    {
        return array_merge(parent::configureBatchActions($actions), $this->getActivateDeactivateBatchActions());
    }
}
