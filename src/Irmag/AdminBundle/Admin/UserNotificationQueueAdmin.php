<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\Form\Type\DatePickerType;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Irmag\SiteBundle\Config;

class UserNotificationQueueAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('show')
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true, 'required' => false])
            ->add('user', ModelAutocompleteType::class, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('event')
            ->add('title')
            ->add('body')
            ->add('url')
            ->add('isPushDone')
            ->add('isEmailDone')
            ->add('isSiteDone')
            ->add('isSmsDone')
            ->add('isDone')
            ->add('expiredAt', DatePickerType::class, ['format' => Config::DATE_FORMAT_RFC3339])
            ->add('createdAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
            ->add('updatedAt', null, ['widget' => 'single_text', 'format' => Config::DATETIME_FORMAT_RFC3339, 'disabled' => true, 'required' => false])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('body')
            ->add('url')
            ->add('user', ModelAutocompleteFilter::class, [], null, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('event')
            ->add('isPushDone')
            ->add('isEmailDone')
            ->add('isSiteDone')
            ->add('isSmsDone')
            ->add('isDone')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('expiredAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('user')
            ->add('event')
            ->add('title')
            ->add('body')
            ->add('url')
            ->add('isPushDone')
            ->add('isEmailDone')
            ->add('isSiteDone')
            ->add('isSmsDone')
            ->add('isDone')
            ->add('expiredAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('updatedAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }
}
