<?php

namespace Irmag\AdminBundle\Admin;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\Form\Type\DateTimePickerType;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Irmag\SiteBundle\Config;
use Irmag\PollBundle\Entity\Poll;
use Irmag\PollBundle\Entity\PollOwnerInterface;
use Irmag\PollBundle\Service\PollService;

class PollAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $owners = $this->getConfigurationPool()->getContainer()->get(PollService::class)->getOwnersList();

        $formMapper
            ->tab('Опрос')
                ->with('Общая информация')
                    ->add('id', null, ['disabled' => true, 'required' => false])
                    ->add('title', null, ['label' => 'Название опроса'])
                    ->add('createdAt', null, [
                        'format' => Config::DATETIME_FORMAT_RFC3339,
                        'widget' => 'single_text',
                        'required' => false,
                        'disabled' => true,
                    ])
                    ->add('updatedAt', null, [
                        'format' => Config::DATETIME_FORMAT_RFC3339,
                        'widget' => 'single_text',
                        'required' => false,
                        'disabled' => true,
                    ])
                    ->add('totalVoters', null, [
                        'disabled' => true,
                        'required' => false,
                    ])
                    ->add('totalVotes', null, [
                        'disabled' => true,
                        'required' => false,
                    ])
                ->end()
                ->with('Привязка к объекту')
                    ->add('ownerFQCN', ChoiceType::class, [
                        'choices' => $owners,
                    ])
                    ->add('ownerId')
                ->end()
                ->with('Опции опроса')
                    ->add('isActive')
                    ->add('isMultiple')
                    ->add('isAnonymous')
                    ->add('estimatedAt', DateTimePickerType::class, [
                        'required' => false,
                        'format' => Config::DATETIME_FORMAT_RFC3339,
                    ])
                ->end()
            ->end()
            ->tab('Варианты ответов')
                ->add('variants', CollectionType::class, [], [
                    'edit' => 'inline',
                    'inline' => 'table',
                ])
            ->end()
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('createdAt')
            ->add('updatedAt')
            ->add('isActive')
            ->add('isMultiple')
            ->add('isAnonymous')
            ->add('estimatedAt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('ownerObject', 'string', [
                'template' => '@IrmagAdmin/Poll/list_object.html.twig',
            ])
            ->add('title', null, ['label' => 'Название'])
            ->add('isActive')
            ->add('isMultiple')
            ->add('isAnonymous')
            ->add('totalVotes')
            ->add('totalVoters')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('updatedAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('estimatedAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('', 'string', ['template' => '@IrmagAdmin/Poll/list_link.html.twig'])
        ;
    }

    /**
     * @param Poll $object
     */
    public function prePersist($object)
    {
        $container = $this->getConfigurationPool()->getContainer();
        $user = $container->get('security.token_storage')->getToken()->getUser();

        $object->setUser($user);

        $em = $container->get('doctrine.orm.default_entity_manager');
        $ownerRepo = $em->getRepository($object->getOwnerFQCN());

        /** @var PollOwnerInterface|null $previousOwner */
        $previousOwner = $ownerRepo->findOneBy(['poll' => $object]);

        if (null !== $previousOwner) {
            $previousOwner->setPoll(null);
            $em->persist($previousOwner);
        }

        /** @var PollOwnerInterface|null $ownerObject */
        $ownerObject = $ownerRepo->find($object->getOwnerId());

        if (null === $ownerObject) {
            throw new NotFoundHttpException(sprintf('Entity "%s" with id "%d" not found.', $object->getOwnerFQCN(), $object->getOwnerId()));
        }

        $ownerObject->setPoll($object);

        foreach ($object->getVariants() as $variant) {
            $variant->setPoll($object);
        }
    }

    /**
     * @param Poll $object
     */
    public function preUpdate($object)
    {
        $this->prePersist($object);
    }
}
