<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Irmag\SiteBundle\Config;
use Irmag\AdminBundle\Form\Type\IrmagFilePreviewType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class FileAdmin extends AbstractBaseAdmin
{
    protected $datagridValues = [
        '_sort_order' => 'DESC',
        '_sort_by' => 'updatedAt',
    ];

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('show');
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('mimeType')
            ->add('size')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('', 'string', ['template' => '@IrmagAdmin/File/list_preview.html.twig'])
            ->addIdentifier('mimeType')
            ->addIdentifier('size')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
            ->add('updatedAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('file', FileType::class, ['label' => false, 'required' => true])
            ->add('preview', IrmagFilePreviewType::class, ['label' => false])
        ;
    }
}
