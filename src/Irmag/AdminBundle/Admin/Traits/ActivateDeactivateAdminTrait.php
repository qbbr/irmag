<?php

namespace Irmag\AdminBundle\Admin\Traits;

trait ActivateDeactivateAdminTrait
{
    protected function getActivateDeactivateBatchActions()
    {
        return [
            'activate' => [
                'label' => 'batch.activate',
                'ask_confirmation' => false,
            ],
            'deactivate' => [
                'label' => 'batch.deactivate',
                'ask_confirmation' => false,
            ],
        ];
    }
}
