<?php

namespace Irmag\AdminBundle\Admin;

use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelAutocompleteType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Sonata\DoctrineORMAdminBundle\Filter\CallbackFilter;
use Irmag\SiteBundle\Config;

class UserBonusHistoryAdmin extends AbstractBaseAdmin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id', null, ['disabled' => true, 'required' => false])
            ->add('uuid1C', null, ['disabled' => true, 'required' => false])
            ->add('user', ModelAutocompleteType::class, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('value')
            ->add('message')
            ->add('createdAt', null, [
                'format' => Config::DATETIME_FORMAT_RFC3339,
                'widget' => 'single_text',
                'required' => false,
                'disabled' => true,
            ])
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('uuid1C')
            ->add('user')
            ->add('value')
            ->add('message')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('uuid1C', CallbackFilter::class, [
                'callback' => function ($qb, $alias, $field, $value) {
                    if (!$value['value']) {
                        return null;
                    }

                    $qb->where($alias.'.'.$field.' = :uuid')
                        ->setParameter('uuid', $value['value']);

                    return true;
                },
            ])
            ->add('user', ModelAutocompleteFilter::class, [], null, [
                'property' => ['username', 'fullname', 'email'],
                'to_string_callback' => function ($entity) {
                    return (string) $entity;
                },
            ])
            ->add('value')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->addIdentifier('uuid1C')
            ->add('user')
            ->add('value')
            ->add('createdAt', null, ['format' => Config::DATETIME_FORMAT])
        ;
    }
}
