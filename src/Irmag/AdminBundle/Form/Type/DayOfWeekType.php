<?php

namespace Irmag\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class DayOfWeekType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'choices' => [
                'Понедельник' => 1,
                'Вторник' => 2,
                'Среда' => 3,
                'Четверг' => 4,
                'Пятница' => 5,
                'Суббота' => 6,
                'Воскресенье' => 0,
            ],
            'required' => true,
            'placeholder' => 'Выбрать',
            'translation_domain' => false,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function getParent()
    {
        return ChoiceType::class;
    }
}
