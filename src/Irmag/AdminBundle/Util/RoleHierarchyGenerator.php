<?php

namespace Irmag\AdminBundle\Util;

class RoleHierarchyGenerator
{
    const DESCRIPTION_PREFIX = '[ADMIN]';
    const ROLE_ID_SEQUENCE = 'roles_id_seq';

    public static function generate(string $serviceName, string $description, string $decoratePrefix, string $decorateSuffix): string
    {
        $description = self::DESCRIPTION_PREFIX.' '.$description;

        // irmag.admin.content.page -> ROLE_IRMAG_ADMIN_CONTENT_PAGE_
        $prefixRoleName = self::getPrefixRoleName($serviceName);

        $output = '';

        // READER
        $output .= self::decorate(self::getCreateRoleSQL($prefixRoleName.'READER', $description.' - Читатель'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getCreateRoleSQL($prefixRoleName.'VIEW', $description.' - Просмотр'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getCreateRoleSQL($prefixRoleName.'LIST', $description.' - Список'), $decoratePrefix, $decorateSuffix);

        $output .= self::decorate(self::getInsertRoleChildrensSQL($prefixRoleName.'READER', $prefixRoleName.'VIEW'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getInsertRoleChildrensSQL($prefixRoleName.'READER', $prefixRoleName.'LIST'), $decoratePrefix, $decorateSuffix);

        // EDITOR
        $output .= self::decorate(self::getCreateRoleSQL($prefixRoleName.'EDITOR', $description.' - Редактор'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getCreateRoleSQL($prefixRoleName.'EDIT', $description.' - Редактировать'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getCreateRoleSQL($prefixRoleName.'CREATE', $description.' - Создавать'), $decoratePrefix, $decorateSuffix);

        $output .= self::decorate(self::getInsertRoleChildrensSQL($prefixRoleName.'EDITOR', $prefixRoleName.'EDIT'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getInsertRoleChildrensSQL($prefixRoleName.'EDITOR', $prefixRoleName.'CREATE'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getInsertRoleChildrensSQL($prefixRoleName.'EDITOR', $prefixRoleName.'READER'), $decoratePrefix, $decorateSuffix);

        // ADMIN
        $output .= self::decorate(self::getCreateRoleSQL($prefixRoleName.'ADMIN', $description.' - Администратор'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getCreateRoleSQL($prefixRoleName.'DELETE', $description.' - Удалять'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getCreateRoleSQL($prefixRoleName.'EXPORT', $description.' - Экспорт'), $decoratePrefix, $decorateSuffix);

        $output .= self::decorate(self::getInsertRoleChildrensSQL($prefixRoleName.'ADMIN', $prefixRoleName.'DELETE'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getInsertRoleChildrensSQL($prefixRoleName.'ADMIN', $prefixRoleName.'EXPORT'), $decoratePrefix, $decorateSuffix);
        $output .= self::decorate(self::getInsertRoleChildrensSQL($prefixRoleName.'ADMIN', $prefixRoleName.'EDITOR'), $decoratePrefix, $decorateSuffix);

        return $output;
    }

    private static function getPrefixRoleName(string $serviceName): string
    {
        return 'ROLE_'.str_replace('.', '_', mb_strtoupper($serviceName)).'_';
    }

    private static function getCreateRoleSQL(string $name, string $description = ''): string
    {
        return sprintf(<<<'SQL'
    INSERT INTO roles (id, name, description)
    VALUES (NEXTVAL('%s'), '%s', '%s');
SQL
, self::ROLE_ID_SEQUENCE, $name, $description);
    }

    private static function getInsertRoleChildrensSQL($roleName, $childrenName)
    {
        return PHP_EOL.sprintf(<<<'SQL'
    INSERT INTO roles_childrens (role_id, children_id)
    VALUES (
        (SELECT id FROM roles WHERE name = '%s'),
        (SELECT id FROM roles WHERE name = '%s')
    );
SQL
, $roleName, $childrenName);
    }

    private static function decorate(string $text, string $prefix, string $suffix): string
    {
        return PHP_EOL.str_replace('\n', "\n", $prefix).$text.str_replace('\n', "\n", $suffix).PHP_EOL;
    }
}
