<?php

namespace Irmag\SberbankAcquiringBundle\Service;

use Buzz\Browser;
use Buzz\Client\Curl;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment;
use Irmag\OnlinePaymentBundle\ProviderContract\PaymentProviderInterface;
use Irmag\OnlinePaymentBundle\ProviderContract\PaymentStateInterface;
use Irmag\OnlinePaymentBundle\Exception\IrmagOnlinePaymentException;
use Irmag\SberbankAcquiringBundle\Exception\SberbankAcquiringException;
use Irmag\SberbankAcquiringBundle\Model\SberbankPaymentState;

/**
 * Класс для вызова API-методов интернет-эквайринга Сбербанка.
 */
class SberbankService implements PaymentProviderInterface
{
    /**
     * Роут для Сбербанка в случае успешной оплаты.
     */
    const PAYMENT_SUCCEED_ROUTE = 'irmag_order_finish_online_payment';

    /**
     * Роут для Сбербанка в случае неудавшейся оплаты.
     */
    const PAYMENT_FAILED_ROUTE = 'irmag_order_cancel_online_payment';

    /**
     * @var Browser
     */
    private $buzzBrowser;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var string
     */
    private $applePayLogin;

    /**
     * @var string
     */
    private $apiHost;

    /**
     * @var string
     */
    private $applePayMerchant;

    /**
     * @var string
     */
    private $payloadBasic;

    /**
     * @param UrlGeneratorInterface $router
     * @param string                $apiHost
     * @param string                $login
     * @param string                $key
     * @param string                $applePayMerchant
     * @param string                $applePayLogin
     */
    public function __construct(
        UrlGeneratorInterface $router,
        $apiHost,
        $login,
        $key,
        $applePayMerchant,
        $applePayLogin
    ) {
        $this->router = $router;
        $this->apiHost = $apiHost;
        $this->applePayMerchant = $applePayMerchant;
        $this->applePayLogin = $applePayLogin;

        $client = new Curl([
            'curl' => [
                CURLOPT_TIMEOUT => 20, // Сбер не всегда успевает отвечать в 5 секунд, которые по умолчанию
            ],
        ]);
        $this->buzzBrowser = new Browser($client);

        $this->payloadBasic = sprintf(
            'userName=%s&password=%s',
            $login,
            $key
        );
    }

    /**
     * Выполняет запрос на регистрацию платежа в Сбербанке при двухстадийном платеже.
     *
     * @param array $options Массив данных для платежа
     *
     * @return array
     */
    public function register(array $options): array
    {
        $paymentSum = $options['sum'];
        $registerOrderID = (!empty($options['orderId'])) ? $options['orderId'] : uniqid(sprintf('%d-', $paymentSum));

        $response = $this->buzzBrowser->post(
            $this->apiHost.'rest/registerPreAuth.do',
            [],
            sprintf(
                '%s&orderNumber=%s&amount=%d&returnUrl=%s&failUrl=%s',
                $this->payloadBasic,
                $registerOrderID,
                $paymentSum,
                $this->router->generate(self::PAYMENT_SUCCEED_ROUTE, [], UrlGeneratorInterface::ABSOLUTE_URL),
                $this->router->generate(self::PAYMENT_FAILED_ROUTE, [], UrlGeneratorInterface::ABSOLUTE_URL)
            )
        );

        return $this->handleResponse($response);
    }

    /**
     * Зарегистрировать платеж через Apple Pay.
     *
     * @param array $options Массив данных для платежа
     *
     * @return array
     */
    public function registerApplePay(array $options): array
    {
        $registerOrderID = (!empty($options['orderId'])) ? $options['orderId'] : uniqid();
        $params = json_encode([
            'merchant' => $this->applePayLogin,
            'orderNumber' => $registerOrderID,
            'paymentToken' => base64_encode($options['paymentToken']),
            'preAuth' => true,
        ]);

        $response = $this->buzzBrowser->post(
            $this->apiHost.'applepay/payment.do',
            ['Content-Type' => 'application/json;charset=UTF-8'],
            $params
        );

        return $this->handleDeferredPaymentResponse($response);
    }

    /**
     * Выполняет запрос на получение информации по платежу.
     *
     * @param string $acquiringOrderID ID платежа в платежке Сбербанка
     *
     * @return array
     */
    public function getData($acquiringOrderID): array
    {
        $response = $this->buzzBrowser->post(
            $this->apiHost.'rest/getOrderStatus.do',
            [],
            sprintf('%s&orderId=%s', $this->payloadBasic, $acquiringOrderID)
        );

        return $this->handleResponse($response);
    }

    /**
     * Выполняет запрос на получение расширенной информации по платежу.
     *
     * @param OrderOnlinePayment $payment Платеж
     *
     * @throws IrmagOnlinePaymentException
     *
     * @return PaymentStateInterface
     */
    public function getState(OrderOnlinePayment $payment): PaymentStateInterface
    {
        $this->throwExceptionIfAcquiringIdIsEmpty($payment);

        $response = $this->buzzBrowser->post(
            $this->apiHost.'rest/getOrderStatusExtended.do',
            [],
            sprintf('%s&orderId=%s', $this->payloadBasic, $payment->getAcquiringId())
        );

        $responseContent = $this->handleResponse($response);

        if (empty($responseContent)) {
            throw new IrmagOnlinePaymentException(sprintf('Empty state returned for payment %d, order № %d', $payment->getId(), $payment->getOrder()->getId()));
        }

        $paymentState = new SberbankPaymentState($responseContent);

        return $paymentState;
    }

    /**
     * Выполняет запрос на подтверждение платежа и списание средств.
     *
     * @param OrderOnlinePayment $payment    Платеж
     * @param int                $depositSum Сумма списания (в копейках)
     *
     * @return array
     */
    public function deposit(OrderOnlinePayment $payment, int $depositSum): array
    {
        $this->throwExceptionIfAcquiringIdIsEmpty($payment);

        $response = $this->buzzBrowser->post(
            $this->apiHost.'rest/deposit.do',
            [],
            sprintf(
                '%s&orderId=%s&amount=%d',
                $this->payloadBasic,
                $payment->getAcquiringId(),
                $depositSum
            )
        );

        return $this->handleResponse($response);
    }

    /**
     * Выполняет запрос на возврат средств.
     *
     * @param OrderOnlinePayment $payment   Платеж
     * @param int                $refundSum Сумма возврата (в копейках)
     *
     * @return array
     */
    public function refund(OrderOnlinePayment $payment, int $refundSum): array
    {
        $this->throwExceptionIfAcquiringIdIsEmpty($payment);

        $response = $this->buzzBrowser->post(
            $this->apiHost.'rest/refund.do',
            [],
            sprintf(
                '%s&orderId=%s&amount=%d',
                $this->payloadBasic,
                $payment->getAcquiringId(),
                $refundSum
            )
        );

        return $this->handleResponse($response);
    }

    /**
     * Выполняет запрос на отмену платежа (транзакция отменяется, деньги расхолдируются автоматически).
     *
     * @param OrderOnlinePayment $payment Платеж
     * @param int                $sum     Сумма отмены (ИГНОРИРУЕТСЯ СБЕРБАНКОМ)
     *
     * @return array
     */
    public function revert(OrderOnlinePayment $payment, int $sum): array
    {
        $this->throwExceptionIfAcquiringIdIsEmpty($payment);

        $response = $this->buzzBrowser->post(
            $this->apiHost.'rest/reverse.do',
            [],
            sprintf(
                '%s&orderId=%s',
                $this->payloadBasic,
                $payment->getAcquiringId()
            )
        );

        return $this->handleResponse($response);
    }

    /**
     * Возвращает URL платежной формы для текущего платежа.
     *
     * @param OrderOnlinePayment $payment           Платеж
     * @param bool               $isDeferredPayment Является ли платеж отложенным?
     *
     * @return string|null
     */
    public function getPaymentFormUrl(OrderOnlinePayment $payment, bool $isDeferredPayment = false): ?string
    {
        if (false === $isDeferredPayment) {
            $this->throwExceptionIfAcquiringIdIsEmpty($payment);
        }

        return $payment->getPaymentUrl();
    }

    /**
     * Бросает исключение, если номер платежа в эквайринге не установлен (нужен для API запросов).
     *
     * @param OrderOnlinePayment $payment
     *
     * @throws SberbankAcquiringException
     */
    private function throwExceptionIfAcquiringIdIsEmpty(OrderOnlinePayment $payment): void
    {
        if (empty($payment->getAcquiringId())) {
            throw new SberbankAcquiringException(sprintf('Empty acquiring ID for payment %d', $payment->getId()));
        }
    }

    /**
     * Обработка ответа от банка, возвращает ассоциативный массив или кидает исключение.
     *
     * @param ResponseInterface $response Ответ от сервера Сбербанка
     *
     * @throws SberbankAcquiringException
     *
     * @return array
     */
    private function handleResponse(ResponseInterface $response): array
    {
        $data = json_decode($response->getBody(), true);

        if (isset($data['errorCode']) && isset($data['errorMessage']) && 0 !== (int) $data['errorCode']) {
            if (isset($data['orderStatus'])) {
                return $data;
            }

            throw new SberbankAcquiringException(sprintf('[SberbankError]: %s (CODE: %d)', $data['errorMessage'], $data['errorCode']), $data['errorCode']);
        }

        return $data ?? [];
    }

    /**
     * Обработка ответа от банка для отложенных платежей.
     *
     * @param ResponseInterface $response
     *
     * @throws SberbankAcquiringException
     *
     * @return array
     */
    private function handleDeferredPaymentResponse(ResponseInterface $response): array
    {
        $data = json_decode($response->getBody(), true);

        if (false === (bool) $data['success']) {
            $messageCode = '';
            $messageTextFriendly = '';
            $messageText = 'Неизвестная ошибка';

            if (isset($data['error'])) {
                $messageText = $data['error']['description'];
                $messageTextFriendly = $data['error']['message'];
                $messageCode = $data['error']['code'];
            }

            throw new SberbankAcquiringException(sprintf('[SberbankError]: %s [%s] (CODE: %d)', $messageText, $messageTextFriendly, $messageCode));
        }

        $extraData = $data['orderStatus'] ?? [];

        return array_merge($data['data'], $extraData);
    }
}
