<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParsePromocodeElementsListCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.promocode_elements_list';
    const XML_NAME = 'promocode_elements_list';
}
