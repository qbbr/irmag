<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParseManufacturersAndBrandsCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.manufacturer_and_brand';
    const XML_NAME = 'manufacturers_and_brands';
}
