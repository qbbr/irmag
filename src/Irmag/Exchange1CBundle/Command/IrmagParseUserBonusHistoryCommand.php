<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParseUserBonusHistoryCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.user_bonus_history';
    const XML_NAME = 'users_bonus_histories';
}
