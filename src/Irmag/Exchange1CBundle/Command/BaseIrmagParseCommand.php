<?php

namespace Irmag\Exchange1CBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\Exchange1CBundle\Parser\ParserInterface;
use Irmag\Exchange1CBundle\Entity\Exchange1CQueue;
use Irmag\Exchange1CBundle\EventDispatcher\Exchange1CQueueEventDispatcher;

abstract class BaseIrmagParseCommand extends Command
{
    /**
     * @var string
     */
    private $container;

    /**
     * @var Exchange1CQueue
     */
    private $task;

    /**
     * @var Exchange1CQueueEventDispatcher
     */
    private $queueEventDispatcher;

    /**
     * @param ContainerInterface             $container
     * @param Exchange1CQueueEventDispatcher $queueEventDispatcher
     */
    public function __construct(
        ContainerInterface $container,
        Exchange1CQueueEventDispatcher $queueEventDispatcher
    ) {
        $this->container = $container;
        $this->queueEventDispatcher = $queueEventDispatcher;

        $this->task = null;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('irmag:parse:'.static::XML_NAME)
            ->setDescription('Parse '.static::XML_NAME.'.xml')
            ->addOption('file', null, InputOption::VALUE_REQUIRED, 'Path to XML file', static::XML_NAME.'.xml')
            ->addOption('no-delete-file', null, InputOption::VALUE_NONE, 'Do not delete XML file after parsing')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'This will simulate the parsing and show you what would happen')
            ->addOption('queue-item', null, InputOption::VALUE_OPTIONAL, 'Saves exception message to a referencing queue element')
            ->addOption('skip-not-found', null, InputOption::VALUE_NONE, 'Skip not found entities instead of throwing exception')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('dry-run') && !empty($input->getOption('queue-item'))) {
            $task = $this->container
                ->get('doctrine.orm.default_entity_manager')
                ->getRepository(Exchange1CQueue::class)
                ->find($input->getOption('queue-item'));

            if ($task) {
                $this->task = $task;
                $this->queueEventDispatcher->dispatchQueueItemProcessingStarted($task, getmypid());
            }
        }

        $xmlFile = $this->container->getParameter('irmag_exchange_1c_xml_dir').'/'.$input->getOption('file');
        /* @var \Irmag\Exchange1CBundle\Parser\AbstractBaseParser $parser */
        $parser = $this->container->get(static::PARSER_SERVICE_NAME);

        if (!$parser instanceof ParserInterface) {
            throw new IrmagException(sprintf('Parser must be instance of "%s"', ParserInterface::class));
        }

        if ($input->getOption('dry-run')) {
            $parser->setIsDryRun(true);
        }

        if ($input->getOption('skip-not-found')) {
            $parser->setSkipNotFoundEntities(true);
        }

        try {
            $parser->setXmlFile($xmlFile);
            $parser->setOutput($output);

            if ($output->isVerbose()) {
                $progressBar = new ProgressBar($output, $parser->getUnits());
                $parser->setProgressBar($progressBar);
            }

            $parser->parse(static::PARSER_SERVICE_NAME);

            if (!$input->getOption('dry-run') && $this->task) {
                $this->queueEventDispatcher->dispatchQueueItemProcessed($this->task);
            }
        } catch (\Exception $ex) {
            if (!$input->getOption('dry-run') && $this->task) {
                $this->queueEventDispatcher->dispatchQueueItemFailed($this->task, $ex->getMessage());
            }

            throw $ex;
        }

        if (!$input->getOption('dry-run') and !$input->getOption('no-delete-file')) {
            if ($output->isVerbose()) {
                $output->writeln(sprintf('Remove xml file: <info>%s</info>', $xmlFile));
            }

            unlink($xmlFile);

            if ($output->isVerbose()) {
                $output->writeln('Remove xml file: <info>Complete</info>');
            }
        }
    }
}
