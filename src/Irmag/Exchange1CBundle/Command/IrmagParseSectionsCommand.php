<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParseSectionsCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.section';
    const XML_NAME = 'sections';
}
