<?php

namespace Irmag\Exchange1CBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Process;
use Doctrine\ORM\EntityManagerInterface;
use Irmag\Exchange1CBundle\Entity\Exchange1CQueue;
use Irmag\Exchange1CBundle\Service\Exchange1CQueueManager;
use Irmag\Exchange1CBundle\EventDispatcher\Exchange1CQueueEventDispatcher;

class IrmagProcessExchange1CQueueCommand extends Command
{
    /**
     * Название команды.
     */
    const COMMAND_NAME = 'irmag:queue:process';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Exchange1CQueueManager
     */
    private $queueManager;

    /**
     * @var Exchange1CQueueEventDispatcher
     */
    private $eventDispatcher;

    /**
     * @param EntityManagerInterface         $em
     * @param Exchange1CQueueManager         $manager
     * @param Exchange1CQueueEventDispatcher $eventDispatcher
     */
    public function __construct(
        EntityManagerInterface $em,
        Exchange1CQueueManager $manager,
        Exchange1CQueueEventDispatcher $eventDispatcher
    ) {
        $this->em = $em;
        $this->queueManager = $manager;
        $this->eventDispatcher = $eventDispatcher;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(self::COMMAND_NAME)
            ->setDescription('Processes items from exchange queue with 1C');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ignore_user_abort(true);
        set_time_limit(0);

        $running = $this->em
            ->getRepository(Exchange1CQueue::class)
            ->getRunningItems();

        $currentCommands = [];

        // если есть уже запущенные таски
        if ($running) {
            $output->writeln(sprintf('Currently running: <info>%d</info>', \count($running)));
            /** @var Exchange1CQueue $item */
            foreach ($running as &$item) {
                if (false === \in_array($item->getCommand(), $currentCommands, true)) {
                    $currentCommands[] = $item->getCommand();
                }

                // если такого процесса нет или он завис
                if (false === $this->queueManager->isItemReallyRunning($item) || $this->queueManager->isItemTimedOut($item)) {
                    // элемент на перезапуск
                    $this->queueManager->postpone($item);
                }
            }
        }

        $tasksToRun = $this->em
            ->getRepository(Exchange1CQueue::class)
            ->pickItemsToParallelProcess($currentCommands);

        // если есть, что запустить
        if ($tasksToRun) {
            foreach ($tasksToRun as $data) {
                $task = $this->em->getRepository(Exchange1CQueue::class)->find($data['id']);
                $command = sprintf(
                    '%s/../../../../bin/console %s --file %s --queue-item %d',
                    __DIR__,
                    $task->getCommand(),
                    $task->getFile(),
                    $task->getId()
                );

                try {
                    $process = new Process($command);
                    $process->start();
                } catch (\Exception $ex) {
                    $output->writeLn(sprintf('[ERROR]: %s', $ex->getMessage()));
                    continue;
                }
            }
        } else {
            $output->writeln('<info>No tasks to run.</info>');
        }
    }
}
