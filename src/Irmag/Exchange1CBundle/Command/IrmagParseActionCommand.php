<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParseActionCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.action';
    const XML_NAME = 'actions';
}
