<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParseElementsStockCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.element_stock';
    const XML_NAME = 'elements_stock';
}
