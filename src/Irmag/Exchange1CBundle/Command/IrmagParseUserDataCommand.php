<?php

namespace Irmag\Exchange1CBundle\Command;

class IrmagParseUserDataCommand extends BaseIrmagParseCommand
{
    const PARSER_SERVICE_NAME = 'irmag.parser.user_data';
    const XML_NAME = 'users_data';
}
