<?php

namespace Irmag\Exchange1CBundle;

final class IrmagExchange1CEvents
{
    /**
     * Это событие вызывается после окончания парсинга.
     *
     * @var string
     */
    const PARSE_COMPLETE = 'irmag_exchange1c.parse.complete';

    /**
     * Это событие вызывается, когда элемент очереди начинает обрабатываться.
     *
     * @var string
     */
    const QUEUE_ITEM_PROCESS_STARTED = 'irmag.exchange1c.queue.process.started';

    /**
     * Это событие вызывается, когда во время обработки элемента очереди происходит ошибка.
     *
     * @var string
     */
    const QUEUE_ITEM_PROCESS_FAILED = 'irmag.exchange1c.queue.process.failed';

    /**
     * Это событие вызывается, когда элемент очереди успешно обработан.
     *
     * @var string
     */
    const QUEUE_ITEM_PROCESS_COMPLETED = 'irmag_exchange1c.queue.process.complete';
}
