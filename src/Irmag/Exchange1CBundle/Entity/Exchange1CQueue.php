<?php

namespace Irmag\Exchange1CBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Table(name="exchange_1c_queue")
 * @ORM\Entity(repositoryClass="Irmag\Exchange1CBundle\Repository\Exchange1CQueueRepository")
 */
class Exchange1CQueue
{
    use TimestampableEntity;

    /**
     * Приоритеты заданий в очереди.
     */
    const PRIORITY_LOW = 1;
    const PRIORITY_NORMAL = 2;
    const PRIORITY_HIGH = 4;

    /**
     * Статусы заданий в очереди.
     */
    const STATUS_WAITING = 1;
    const STATUS_RUNNING = 2;
    const STATUS_FAILED = 4;

    public $statuses = [
        self::STATUS_WAITING => 'Ожидает',
        self::STATUS_RUNNING => 'Обрабатывается',
        self::STATUS_FAILED => 'Ошибка',
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $command;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $verbosity;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", options={"default": 1})
     * @Assert\Choice(choices={1, 2, 4})
     */
    private $status;

    /**
     * @var string
     *
     * @Assert\Ip
     *
     * @ORM\Column(type="string", length=50)
     */
    private $ip;

    /**
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $runAt;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $pid;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $attempts;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $lastOutputMessage;

    /**
     * @var int
     *
     * @Assert\Choice(choices={1, 2, 4})
     *
     * @ORM\Column(type="integer", options={"default": 2})
     */
    private $priority;

    public function __construct()
    {
        $this->attempts = 0;
        $this->status = self::STATUS_WAITING;
        $this->priority = self::PRIORITY_NORMAL;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return sprintf('%d (%s)', $this->id, $this->command);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCommand(): ?string
    {
        return $this->command;
    }

    public function setCommand(string $command): self
    {
        $this->command = $command;

        return $this;
    }

    public function getFile(): ?string
    {
        return $this->file;
    }

    public function setFile(string $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getVerbosity(): ?string
    {
        return $this->verbosity;
    }

    public function setVerbosity(?string $verbosity): self
    {
        $this->verbosity = $verbosity;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getIp(): ?string
    {
        return $this->ip;
    }

    public function setIp(string $ip): self
    {
        $this->ip = $ip;

        return $this;
    }

    public function getRunAt(): ?\DateTimeInterface
    {
        return $this->runAt;
    }

    public function setRunAt(?\DateTimeInterface $runAt): self
    {
        $this->runAt = $runAt;

        return $this;
    }

    public function getPid(): ?int
    {
        return $this->pid;
    }

    public function setPid(?int $pid): self
    {
        $this->pid = $pid;

        return $this;
    }

    public function getAttempts(): ?int
    {
        return $this->attempts;
    }

    public function setAttempts(int $attempts): self
    {
        $this->attempts = $attempts;

        return $this;
    }

    public function getLastOutputMessage(): ?string
    {
        return $this->lastOutputMessage;
    }

    public function setLastOutputMessage(?string $lastOutputMessage): self
    {
        $this->lastOutputMessage = $lastOutputMessage;

        return $this;
    }

    public function getPriority(): ?int
    {
        return $this->priority;
    }

    public function setPriority(int $priority): self
    {
        $this->priority = $priority;

        return $this;
    }
}
