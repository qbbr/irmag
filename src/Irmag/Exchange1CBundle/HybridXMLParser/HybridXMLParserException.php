<?php

namespace Irmag\Exchange1CBundle\HybridXMLParser;

use Irmag\CoreBundle\Exception\IrmagException;

class HybridXMLParserException extends IrmagException
{
}
