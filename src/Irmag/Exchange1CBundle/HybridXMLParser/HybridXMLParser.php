<?php

namespace Irmag\Exchange1CBundle\HybridXMLParser;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Hybrid XML parser.
 *
 * Class to parse huge XML files in a memory-efficient way.
 *
 * Based on: https://github.com/alexanderk23/hybrid-xml-parser
 */
class HybridXMLParser
{
    /**
     * @var \XMLReader
     */
    protected $xml;

    /**
     * @var array
     */
    protected $path;

    /**
     * @var bool
     */
    protected $stop;

    /**
     * @var string
     */
    protected $encoding;

    /**
     * @var array
     */
    protected $pathListeners = [];

    /**
     * @param string $encoding
     */
    public function __construct(
        $encoding = 'UTF-8'
    ) {
        $this->xml = new \XMLReader();
        $this->encoding = $encoding;
    }

    /**
     * @param string $path     XML path to watch for (slash-separated)
     * @param mixed  $listener Callable (lambda, inline function, function name as string, or array(class, method))
     *
     * @throws HybridXMLParserException
     *
     * @return HybridXMLParser
     */
    public function bind($path, $listener): self
    {
        if (!\is_callable($listener)) {
            throw new \InvalidArgumentException('Listener is not callable');
        }

        if (isset($this->pathListeners[$path])) {
            throw new HybridXMLParserException(sprintf('Another listener is already bound to path "%s"', $path));
        }

        $this->pathListeners[$path] = $listener;

        return $this;
    }

    /**
     * @param string $path XML path
     *
     * @return HybridXMLParser
     */
    public function unbind($path): self
    {
        if (isset($this->pathListeners[$path])) {
            unset($this->pathListeners[$path]);
        }

        return $this;
    }

    /**
     * @return HybridXMLParser
     */
    public function unbindAll(): self
    {
        $this->pathListeners = [];

        return $this;
    }

    /**
     * @return HybridXMLParser
     */
    public function stop(): self
    {
        $this->stop = true;

        return $this;
    }

    /**
     * @return string XML path
     */
    protected function getCurrentPath(): string
    {
        return '/'.implode('/', $this->path);
    }

    /**
     * @param string $path XML path
     *
     * @return HybridXMLParser
     */
    protected function notifyListener($path): self
    {
        if (isset($this->pathListeners[$path])) {
            $node = new Crawler();
            $node->addXmlContent($this->xml->readOuterXml(), $this->encoding);
            $this->pathListeners[$path]($node, $this);
        }

        return $this;
    }

    /**
     * @param string $uri     URI pointing to the document
     * @param int    $options A bitmask of the LIBXML_* constants
     *
     * @throws HybridXMLParserException
     *
     * @return HybridXMLParser
     */
    public function process($uri, $options = 0): self
    {
        $this->path = [];

        if (false === $this->xml->open($uri, $this->encoding, $options | LIBXML_NOERROR | LIBXML_NOWARNING | LIBXML_PARSEHUGE)) {
            throw new HybridXMLParserException(sprintf('Cannot open URI "%s"', $uri));
        }

        $this->stop = false;

        while (!$this->stop && $this->xml->read()) {
            switch ($this->xml->nodeType) {
                case \XMLReader::ELEMENT:
                    array_push($this->path, $this->xml->name);
                    $this->notifyListener($this->getCurrentPath());

                    if (!$this->xml->isEmptyElement) {
                        break;
                    }

                    // no break
                case \XMLReader::END_ELEMENT:
                    array_pop($this->path);
                    break;
            }
        }

        $this->xml->close();

        return $this;
    }
}
