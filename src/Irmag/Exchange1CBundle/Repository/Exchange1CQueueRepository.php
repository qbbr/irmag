<?php

namespace Irmag\Exchange1CBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Irmag\Exchange1CBundle\Entity\Exchange1CQueue;

class Exchange1CQueueRepository extends EntityRepository
{
    /**
     * Возвращает id самых старых элементов очереди, которые можно обрабатывать.
     *
     * @param array $alreadyRun Список команд, которые прямо сейчас отрабатываются и которых в выборку не нужно добавлять
     *
     * @return array
     */
    public function pickItemsToParallelProcess(array $alreadyRun = []): array
    {
        $qb = $this->createQueryBuilder('eq')
            ->select('MIN(eq.id) AS id, eq.command', 'eq.priority')
            ->where('eq.pid IS NULL')
            ->andWhere('eq.status = :status');

        if (!empty($alreadyRun)) {
            $qb->andWhere('eq.command != :alreadyRun')
               ->setParameter('alreadyRun', $alreadyRun);
        }

        $result = $qb->setParameter('status', Exchange1CQueue::STATUS_WAITING)
            ->groupBy('eq.command', 'eq.priority')
            ->orderBy('eq.priority', 'DESC')
            ->getQuery()
            ->getResult();

        return $result;
    }

    /**
     * @return array
     */
    public function getRunningItems(): array
    {
        return $this
            ->createQueryBuilder('eq')
            ->where('eq.pid IS NOT NULL')
            ->andWhere('eq.status = :status')
            ->setParameter('status', Exchange1CQueue::STATUS_RUNNING)
            ->getQuery()
            ->getResult();
    }
}
