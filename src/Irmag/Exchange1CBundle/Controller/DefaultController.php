<?php

namespace Irmag\Exchange1CBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\Exchange1CBundle\Entity\Exchange1CQueue;
use Irmag\Exchange1CBundle\Utils\ImageResizeUtil;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\ElementPicture;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     *
     * @param Request $request
     *
     * @throws IrmagException, \InvalidArgumentException
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $token = $request->query->get('token');
        $ip = $request->getClientIp();
        $this->throwExceptionIfExchangeTokenIsInvalid($token);

        $command = $request->query->get('command');
        $file = $request->query->get('file');
        $verbosity = $request->query->get('verbosity');

        if (empty($command) || empty($file)) {
            throw new \InvalidArgumentException('Arguments "command" and "file" is required!');
        }

        if ('irmag:parse' !== mb_substr($command, 0, 11)) {
            throw new IrmagException(sprintf('Command "%s" is not allowed', $command));
        }

        /* Проверяем есть ли такой таск в очереди */
        $em = $this->get('doctrine.orm.default_entity_manager');
        $task = $em->getRepository(Exchange1CQueue::class)->findOneBy([
            'file' => $file,
            'command' => $command,
        ]);

        if (empty($task)) {
            $queueItem = new Exchange1CQueue();
            $queueItem->setFile($file)
                ->setCommand($command)
                ->setVerbosity($verbosity)
                ->setIp($ip)
                ->setStatus(Exchange1CQueue::STATUS_WAITING);

            // выставляем пониженный приоритет для parse:elements, т.к. они разбираются в последнюю очередь
            if ('irmag:parse:elements' === $command) {
                $queueItem->setPriority(Exchange1CQueue::PRIORITY_LOW);
            }

            $em->persist($queueItem);
            $em->flush();
        } else {
            $message = 'This item has been already pushed to queue';
        }

        return new Response($message ?? 'Successfully pushed to exchange queue', 200);
    }

    /**
     * @Route("/resizeimage/")
     *
     * @param Request $request
     *
     * @throws IrmagException
     *
     * @return Response
     */
    public function resizeImageAction(Request $request): Response
    {
        $this->setConnectionOptions();

        $token = $request->get('token');
        $this->throwExceptionIfExchangeTokenIsInvalid($token);

        $picture = $request->get('picture');
        $isNewImageMode = !empty($picture);
        $maxWidth = (int) $request->get('width');
        $maxHeight = (int) $request->get('height');
        $id = (int) $request->get('id');
        $keepScale = $request->get('keepscale') ?? true;

        if (true === $isNewImageMode) {
            // картинки нет, т.к. товар ещё не выгружен
            $picture = base64_decode(rawurldecode($picture), true);
            $filename = sprintf('/tmp/%s', uniqid());
            file_put_contents($filename, $picture);
            $newFilename = $filename.$this->getFileExtension($filename);
            rename($filename, $newFilename);

            $response = new Response(
                $this->get(ImageResizeUtil::class)->resize($newFilename, mime_content_type($newFilename), $maxWidth, $maxHeight, $keepScale),
                Response::HTTP_OK,
                ['content-type' => mime_content_type($newFilename)]
            );

            unlink($newFilename);
        } else {
            // товар и картинка есть
            if (empty($id)) {
                throw new IrmagException('Element ID has not been provided!');
            }

            $em = $this->get('doctrine.orm.default_entity_manager');
            $element = $em->getRepository(Element::class)->find($id);

            if (empty($element)) {
                throw new IrmagException(sprintf('Could not find any Element with ID %d', $id));
            }

            $picture = $em->getRepository(ElementPicture::class)->findOneBy([
                'element' => $element,
                'position' => 1,
            ]);

            if (empty($picture)) {
                throw new IrmagException('No picture found!');
            }

            $file = $picture->getFile();

            $response = new Response(
                $this->get(ImageResizeUtil::class)->resize($file->getWebPath(), $file->getMimeType(), $maxWidth, $maxHeight, $keepScale),
                Response::HTTP_OK,
                ['content-type' => $file->getMimeType()]
            );
        }

        return $response;
    }

    /**
     * Выбрасывает исключение, если токен невалидный или отсутствует.
     *
     * @param string $token Токен обмена с 1С
     *
     * @throws IrmagException
     */
    private function throwExceptionIfExchangeTokenIsInvalid(string $token = null): void
    {
        if (empty($token) || $token !== $this->getParameter('irmag_exchange_1c_token')) {
            throw new IrmagException('Authentication token is invalid.');
        }
    }

    /**
     * Получить расширения файла.
     *
     * @param string $filepath Путь к файлу
     *
     * @throws IrmagException
     *
     * @return string
     */
    private function getFileExtension(string $filepath): string
    {
        $mimeType = mime_content_type($filepath);

        switch ($mimeType) {
            case 'image/jpeg':
                return '.jpg';
                break;
            case 'image/png':
                return '.png';
                break;
            case 'image/gif':
                return '.gif';
                break;
        }

        throw new IrmagException(sprintf('Provided mime-type %s is incorrect', $mimeType));
    }

    /**
     * Настройки соединения с 1С.
     */
    private function setConnectionOptions(): void
    {
        ini_set('memory_limit', -1);
        ini_set('max_execution_time', 0);
        ignore_user_abort(true);
        set_time_limit(0);
    }
}
