<?php

namespace Irmag\Exchange1CBundle\Parser;

use Irmag\ProfileBundle\Entity\UserBonusHistory;
use Irmag\Exchange1CBundle\HybridXMLParser\HybridXMLParser;
use Symfony\Component\DomCrawler\Crawler;
use Irmag\ProfileBundle\Entity\User;

class UserBonusHistoryParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    protected $howManyOperationsToFlush = 500;

    /**
     * {@inheritdoc}
     */
    protected function parseXml()
    {
        $all = 0;

        (new HybridXMLParser())
            ->bind('/root/bonuses/bonus', function (Crawler $node) use (&$all) {
                ++$all;

                if (0 === $all % 1000) {
                    $this->output->writeln($all);
                }

                $uuid1C = (int) $node->filter('uuid1C')->text();
                $userBonusHistory = $this->em->getRepository(UserBonusHistory::class)->findOneBy(['uuid1C' => $uuid1C]);

                if (!$userBonusHistory) {
                    $userBonusHistory = new UserBonusHistory();
                    $userBonusHistory->setUuid1C($uuid1C);
                }

                $userId = (int) $node->filter('user')->attr('id');
                $user = $this->em->getRepository(User::class)->findOneBy(['id' => $userId]);

                if (!$user) {
                    $this->output->writeln('');
                    $this->output->writeln(sprintf('<error>User with id "%d" not found, skipped (uuid1C: %d).</error>', $userId, $uuid1C));

                    return;
                }
                $userBonusHistory->setUser($user);
                $userBonusHistory->setValue($node->filter('value')->text());
                $userBonusHistory->setMessage($node->filter('message')->text() ?? '');

                if (0 !== $node->filter('createdAt')->count()) {
                    $userBonusHistory->setCreatedAt(new \DateTime($node->filter('createdAt')->text()));
                }

                $this->em->persist($userBonusHistory);

                ++$this->operations;
                $this->flushIfNeed(true);

                if (null !== $this->progressBar) {
                    $this->progressBar->advance();
                }
            })
            ->process($this->xmlFile);
    }
}
