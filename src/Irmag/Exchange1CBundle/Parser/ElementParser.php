<?php

namespace Irmag\Exchange1CBundle\Parser;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\DomCrawler\Crawler;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\Exchange1CBundle\HybridXMLParser\HybridXMLParser;
use Irmag\SiteBundle\Entity\SectionElement;
use Irmag\SiteBundle\Entity\File as IrmagFile;
use Irmag\SiteBundle\Entity\Section;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\ElementManufacturer;
use Irmag\SiteBundle\Entity\ElementBrand;
use Irmag\SiteBundle\Entity\ElementCountry;
use Irmag\SiteBundle\Entity\ElementPicture;

class ElementParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    protected $fieldsToFill = [
        'name', 'toneName', 'price',
        'description', 'composition', 'howToUse',
        'barcode', 'weight', 'volume', 'tableSize', 'videoUrl',
        'propEco', 'propSpecialPrice', 'propSafeAnimal', 'propTransfer',
        'groupBy', 'inStock', 'inPackage', 'abcGroup', 'series', 'isActive', 'isPreOrderZone', 'isOnlyForHome',
    ];

    /**
     * {@inheritdoc}
     */
    protected $fieldsRelationshipToFill = [
        'brand' => ElementBrand::class,
        'manufacturer' => ElementManufacturer::class,
        'country' => ElementCountry::class,
    ];

    /**
     * {@inheritdoc}
     */
    protected $howManyOperationsToFlush = 500;

    public function parseXml()
    {
        $elementRepo = $this->em->getRepository(Element::class);
        $all = 0;

        (new HybridXMLParser())
            ->bind('/root/elements/element', function (Crawler $node) use (&$elementRepo, &$all) {
                ++$all;

                if (0 === $node->filter('id')->count()) {
                    $this->output->writeln(sprintf('<error>Id is empty! counter: "%d"</error>', $all));

                    return;
                }

                if (0 === $all % 1000) {
                    $this->output->writeln('<info>'.$all.'</info>');
                }

                $id = (int) $node->filter('id')->text();
                $element = $elementRepo->find($id);

                if (!$element) {
                    if ($this->skipNotFoundEntities) {
                        $this->output->writeln(sprintf('<info>Element %d not found, skipping ...</info>', $id));

                        return;
                    }

                    $element = new Element();
                    $element->setId($id);
                }

                $this->fillFields($element, $node);
                $this->fillFieldsRelationship($element, $node);
                $this->parseSections($element, $node);
                $this->parsePictures($element, $node);
                $this->parseTonePicture($element, $node);
                $this->em->persist($element);
                ++$this->operations;
                $this->flushIfNeed(true);

                if (null !== $this->progressBar) {
                    $this->progressBar->advance();
                }
            })
            ->process($this->xmlFile);
    }

    /**
     * @param Element $element
     * @param Crawler $node
     *
     * @throws IrmagException
     */
    protected function parseSections(Element $element, Crawler $node)
    {
        if (0 === $node->filter('sections')->count()) {
            return;
        }

        if (0 === $node->filter('sections > id')->count()) {
            throw new IrmagException(sprintf('Sections in element with id "%d" is empty.', $element->getId()));
        }

        $currentSectionElements = $element->getSectionElements();
        $xmlSectionElements = new ArrayCollection();
        $checkForUniqXmlSectionIds = []; // 1c section id duplicate fix

        $node->filter('sections > id')->each(function (Crawler $node) use ($element, $xmlSectionElements, &$checkForUniqXmlSectionIds) {
            $sectionId = $node->text();

            if (empty($sectionId)) {
                throw new IrmagException(sprintf('Section id in element with id "%d" is empty.', $element->getId()));
            }

            $sectionId = (int) $sectionId;

            if (false === \in_array($sectionId, $checkForUniqXmlSectionIds, true)) {
                $section = $this->em->getRepository(Section::class)->find($sectionId);

                if ($section) {
                    $sectionElement = $this->em->getRepository(SectionElement::class)->findOneBy(['section' => $section, 'element' => $element]);

                    if (!$sectionElement) {
                        $sectionElement = new SectionElement();
                        $sectionElement->setElement($element);
                        $sectionElement->setSection($section);
                        $element->addSectionElement($sectionElement);

                        if ($this->output->isVerbose()) {
                            $this->output->writeln(sprintf('<info>Element (id: "%d") added to Section (id: "%d").</info>', $element->getId(), $section->getId()));
                        }
                    }

                    $xmlSectionElements->add($sectionElement);
                } else {
                    $this->output->writeln(sprintf('<error>Section with id "%d" not found!</error>', $sectionId));
                }

                $checkForUniqXmlSectionIds[] = $sectionId;
            }
        });

        /** @var SectionElement $currentSectionElement */
        foreach ($currentSectionElements as $currentSectionElement) {
            if (false === $xmlSectionElements->contains($currentSectionElement)) {
                if ($this->output->isVerbose()) {
                    $this->output->writeln(sprintf('<info>Element (id: "%d") deleted from Section (id: "%d").</info>', $element->getId(), $currentSectionElement->getSection()->getId()));
                }

                $element->removeSectionElement($currentSectionElement);
            }
        }
    }

    /**
     * @param Element $element
     * @param Crawler $node
     */
    protected function parsePictures(Element $element, Crawler $node)
    {
        if (0 === $node->filter('pictures > picture')->count()) {
            return;
        }

        $currentPictures = [];

        foreach ($element->getPictures() as $picture) {
            $currentPictures[$picture->getPosition()] = $picture;
        }

        $node->filter('pictures > picture')->each(function (Crawler $node) use ($element, $currentPictures) {
            $value = $node->text();
            $position = $node->attr('position');

            // удаляем старое изображение
            if (isset($currentPictures[$position])) {
                $element->removePicture($currentPictures[$position]);
            }

            // загружаем новое
            if (!empty($value)) {
                $uploadedFile = $this->getUploadedFileFromBase64($value);

                if (null !== $uploadedFile) {
                    $file = new IrmagFile();
                    $file->setFile($uploadedFile);

                    $elementPicture = new ElementPicture();
                    $elementPicture->setFile($file);
                    $elementPicture->setPosition($position);
                    $element->addPicture($elementPicture);
                }
            }
        });
    }

    /**
     * @param Element $element
     * @param Crawler $node
     */
    protected function parseTonePicture(Element $element, Crawler $node)
    {
        if (0 === $node->filter('tonePicture')->count()) {
            return;
        }

        $value = $node->filter('tonePicture')->text();

        // удаляем старую картинку
        if (empty($value)) {
            $element->setTonePicture(null);

            return;
        }

        $uploadedFile = $this->getUploadedFileFromBase64($value);

        if (null !== $uploadedFile) {
            if ($element->getTonePicture()) {
                $element->getTonePicture()->setFile($uploadedFile);
            } else {
                $picture = new IrmagFile();
                $picture->setFile($uploadedFile);
                $element->setTonePicture($picture);
            }
        }
    }
}
