<?php

namespace Irmag\Exchange1CBundle\Parser;

use Irmag\SiteBundle\Entity\Element;
use Irmag\Exchange1CBundle\HybridXMLParser\HybridXMLParser;
use Symfony\Component\DomCrawler\Crawler;

class ElementStockParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    protected $fieldsToFill = [
        'isActive',
        'inStock',
    ];

    /**
     * {@inheritdoc}
     */
    protected $howManyOperationsToFlush = 500;

    public function parseXml()
    {
        $elementRepo = $this->em->getRepository(Element::class);
        $all = 0;

        (new HybridXMLParser())
            ->bind('/root/elements/element', function (Crawler $node) use (&$elementRepo, &$all) {
                ++$all;

                if (0 === $node->filter('id')->count()) {
                    $this->output->writeln(sprintf('<error>Id is empty! counter: "%d"</error>', $all));

                    return;
                }

                if (0 === $all % 1000) {
                    $this->output->writeln($all);
                }

                $id = (int) $node->filter('id')->text();
                $element = $elementRepo->find($id);

                if (!$element) {
                    $this->output->writeln(sprintf('<error>Element with id "%d" not found, skipped.</error>', $id));

                    return;
                }

                $this->fillFields($element, $node);
                $this->em->persist($element);
                ++$this->operations;
                $this->flushIfNeed(true);

                if (null !== $this->progressBar) {
                    $this->progressBar->advance();
                }
            })
            ->process($this->xmlFile);
    }
}
