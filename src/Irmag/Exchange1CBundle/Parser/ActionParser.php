<?php

namespace Irmag\Exchange1CBundle\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\ActionElement;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\File as IrmagFile;
use Irmag\SiteBundle\Entity\OrderPaymentMethod;

class ActionParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    protected $fieldsToFill = [
        'title',
        'isActive',
        'conditionMinSum',
        'conditionMinCount',
        'conditionIsForAllElements',
        'conditionMaxGifts',
    ];

    /**
     * {@inheritdoc}
     */
    protected $fieldsRelationshipToFill = [
        'conditionPaymentMethod' => OrderPaymentMethod::class,
    ];

    /**
     * {@inheritdoc}
     */
    protected $howManyOperationsToFlush = 100;

    /**
     * {@inheritdoc}
     */
    public function getUnits()
    {
        return $this->getCrawler()->filter('root action, root element')->count();
    }

    /**
     * {@inheritdoc}
     */
    protected function parseXml()
    {
        $this->getCrawler()->filter('root action')->each(function (Crawler $node) {
            $id = (int) ($node->filter('id')->text());
            $action = $this->em->getRepository(Action::class)->find($id);

            // стикер
            if (0 !== $node->filter('sticker')->count()) {
                $base64Picture = $node->filter('sticker')->text();
            }

            if (!$action) {
                $action = new Action();
                $action->setId($id);
            }

            $this->fillFields($action, $node);
            $this->fillFieldsRelationship($action, $node);

            $action->setStartDateTime(new \DateTime($node->filter('startDateTime')->text()));
            $action->setEndDateTime(new \DateTime($node->filter('endDateTime')->text()));

            if (isset($base64Picture)) {
                // если в xml пустые данные, то удаляем картинку.
                if (empty($base64Picture)) {
                    $action->setSticker(null);
                } else {
                    // загружаем новую картинку
                    $uploadedFile = $this->getUploadedFileFromBase64($base64Picture);

                    if (null !== $uploadedFile) {
                        if ($action->getSticker()) {
                            $action->getSticker()->setFile($uploadedFile);
                        } else {
                            $picture = new IrmagFile();
                            $picture->setFile($uploadedFile);
                            $action->setSticker($picture);
                            $this->em->persist($picture);
                        }
                    }
                }
            }

            $this->em->persist($action);
            $this->purgeActionElements($action);

            $node->filter('elements element')->each(function (Crawler $elementNode) use ($action) {
                $elementId = (int) $elementNode->attr('id');
                $element = $this->em->getRepository(Element::class)->find($elementId);

                if (!$element) {
                    $this->output->writeln('');
                    $this->output->writeln(sprintf('<error>Element with id "%d" not found, skipped.</error>', $elementId));

                    return;
                }

                $actionElement = $this->em->getRepository(ActionElement::class)->findOneBy([
                    'element' => $element,
                    'action' => $action,
                ]);

                if (!$actionElement) {
                    $actionElement = new ActionElement();
                    $actionElement->setAction($action);
                    $actionElement->setElement($element);
                }

                if ($elementNode->attr('price')) {
                    $actionElement->setPrice($elementNode->attr('price'));
                } else {
                    $actionElement->setPrice(null);
                }

                $this->em->persist($actionElement);

                ++$this->operations;
                $this->flushIfNeed();

                if (null !== $this->progressBar) {
                    $this->progressBar->advance();
                }
            });

            ++$this->operations;
            $this->flushIfNeed();

            if (null !== $this->progressBar) {
                $this->progressBar->advance();
            }
        });
    }

    /**
     * Очищает товары у акции.
     *
     * @param Action $action
     */
    private function purgeActionElements(Action $action)
    {
        $this->em->createQueryBuilder()
            ->delete(ActionElement::class, 'ae')
            ->where('ae.action = :action')
            ->setParameter('action', $action)
            ->getQuery()
            ->execute();
    }
}
