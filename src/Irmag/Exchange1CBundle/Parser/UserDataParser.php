<?php

namespace Irmag\Exchange1CBundle\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Irmag\ProfileBundle\Entity\User;

class UserDataParser extends AbstractBaseParser
{
    /**
     * {@inheritdoc}
     */
    protected $fieldsToFill = [
        'discount',
        'spendMoney',
        'isPrePaymentOnly',
        // 'bonus', // XXX: not for prod, use API for update user bonus
    ];

    /**
     * {@inheritdoc}
     */
    public function getUnits()
    {
        return $this->getCrawler()->filter('root user')->count();
    }

    /**
     * {@inheritdoc}
     */
    protected $howManyOperationsToFlush = 200;

    /**
     * {@inheritdoc}
     */
    protected function parseXml()
    {
        $this->getCrawler()->filter('root user')->each(function (Crawler $node) {
            $id = (int) $node->filter('id')->text();
            $user = $this->em->getRepository(User::class)->find($id);

            if (!$user) {
                $this->output->writeln('');
                $this->output->writeln(sprintf('<error>User with id "%d" not found, skipped.</error>', $id));

                return;
            }
            $this->fillFields($user, $node);
            $this->em->persist($user);

            ++$this->operations;
            $this->flushIfNeed(true);

            if (null !== $this->progressBar) {
                $this->progressBar->advance();
            }
        });
    }
}
