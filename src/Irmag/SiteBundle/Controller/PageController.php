<?php

namespace Irmag\SiteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Irmag\SiteBundle\Entity\Page;

class PageController extends Controller
{
    public function renderAction(string $slug)
    {
        $page = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Page::class)
            ->findOneBy(['slug' => $slug, 'isActive' => true]);

        if (!$page) {
            throw $this->createNotFoundException(sprintf('Page with slug "%s" not found.', $slug));
        }

        return $this->render('@IrmagSite/Page/page.html.twig', [
            'page' => $page,
        ]);
    }
}
