<?php

namespace Irmag\SiteBundle\Controller;

use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\BasketBundle\Service\DataCollector;
use Irmag\OrderPostDeliveryBundle\Service\OrderPostDeliveryManager;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceManager;
use Irmag\SiteBundle\Form\Type\OrderType;
use Irmag\SiteBundle\Form\Type\UserOrderProfileType;
use Irmag\SiteBundle\Entity\OrderDeliveryCity;
use Irmag\SiteBundle\Entity\OrderDeliveryMethod;
use Irmag\SiteBundle\EventDispatcher\OrderEventDispatcher;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\SiteBundle\Service\OrderManager\OrderManager;

/**
 * @Route("/order")
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 */
class OrderController extends Controller
{
    const SESSION_SUCCESS_ORDER_ID = 'success_order_id';

    /**
     * @Route("/", name="irmag_order")
     * @Route("/ajax/", name="irmag_order_ajax_content", methods={"POST"}, options={"expose": true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $basketManager = $this->get(BasketManager::class);

        // если пустая корзина, то редирект в каталог
        if ($basketManager->isEmpty(true)) {
            return $this->redirectToRoute('irmag_catalog');
        }

        $orderManager = $this->get(OrderManager::class);
        $em = $this->get('doctrine.orm.default_entity_manager');
        $postDeliveryManager = $this->get(OrderPostDeliveryManager::class);
        $order = $orderManager->newOrder();

        // адрес доставки
        $userOrderProfilePrimary = $orderManager->getUserOrderProfilePrimary();
        $transportCompanyData = $this->get(OrderDeliveryServiceManager::class)->getSelectedData();
        $isPostDeliverySelected = $postDeliveryManager->getIsSelected();
        $postDeliveryData = $postDeliveryManager->getResult();

        // если считали в корзине транспортную доставку и выбрали, то подставляем данные доставки
        if (!$request->isXmlHttpRequest()) {
            $orderManager->setOrderDataFromSession();
        }

        if ($userOrderProfilePrimary) {
            // форма профиля заказа
            $userOrderProfileForm = $this->createForm(UserOrderProfileType::class, null, [
                'user' => $this->getUser(),
                'em' => $em,
            ]);

            $userOrderProfileForm->handleRequest($request);

            if ($userOrderProfileForm->isSubmitted() && $userOrderProfileForm->isValid()) {
                $data = $userOrderProfileForm->getData();
                $userOrderProfilePrimary = $data['order_profile'];
            }

            // заполняем поля заказа из профиля заказа пользователя (если не было расчёта в корзине)
            if (empty($transportCompanyData) && false === $isPostDeliverySelected) {
                $orderManager->fillOrderFieldsFromUserOrderProfile($userOrderProfilePrimary);
            }
        }

        $dataCollector = $this->get(DataCollector::class)->getData();
        $basketTotal = $dataCollector->getTotalPrice();

        /* Получаем данные о способе доставки заранее, т.к. нужны для инициализации формы смешанного платежа */
        $orderData = $request->request->get('order');
        $total = $basketTotal;

        if (!empty($orderData['deliveryMethod'])) {
            $deliveryMethod = $em->getRepository(OrderDeliveryMethod::class)->find($orderData['deliveryMethod']);
            $deliveryCity = $em->getRepository(OrderDeliveryCity::class)->find($orderData['deliveryAddress']['deliveryCity']);

            if (false === \in_array($deliveryMethod->getShortname(), ['selfservice', 'transport_company', 'russian_post'], true) && $basketTotal < $deliveryCity->getMinPriceForFreeDelivery()) {
                $total += $deliveryCity->getDeliveryPrice();
            }
        }

        $options['total'] = $total;

        // only for home
        if ($dataCollector->hasOnlyForHome()) {
            $options['has_only_for_home'] = true;
        }

        // transfer days
        $maxTransferValue = $basketManager->getMaxTransferValue();

        if ($maxTransferValue > 0) {
            $options['max_transfer_value'] = $maxTransferValue;
        }

        if ($request->isXmlHttpRequest()) {
            $options['validation_groups'] = false;
            $template = '@IrmagSite/Order/index-content.html.twig';
        } else {
            $template = '@IrmagSite/Order/index.html.twig';
        }

        // форма заказа
        $form = $this->createForm(OrderType::class, $order, $options ?? []);
        $form->handleRequest($request);

        if (!$request->isXmlHttpRequest() && $form->isSubmitted() && $form->isValid() && $orderManager->isRemoteDeliveryDataSelected()) {
            // проводим заказ
            $orderManager->checkout();
            $paymentMethodShortname = $orderManager->getOrder()->getPaymentMethod()->getShortname();

            if (true === \in_array($paymentMethodShortname, ['apple_pay', 'android_pay', 'online'], true)) {
                // статус "Ожидает онлайн оплату"
                $orderManager->setStatusByShortname('S');
                $orderManager->save();

                //IrmagSiteEvents::ORDER_SET_ONLINE_PAYMENT
                $this->get(OrderEventDispatcher::class)->dispatchSetOnlinePayment($order);

                return $this->redirectToRoute('irmag_order_payment');
            }
            // запоминаем orderId
            $request->getSession()->set(self::SESSION_SUCCESS_ORDER_ID, $order->getId());

            return $this->redirectToRoute('irmag_order_finish');
        }

        return $this->render($template, [
            'form' => $form->createView(),
            'delivery_data' => $transportCompanyData,
            'post_delivery_data' => $postDeliveryData,
            'user_order_profile_form' => isset($userOrderProfileForm) ? $userOrderProfileForm->createView() : null,
        ]);
    }

    /**
     * @Route("/cancel/{id}/", name="irmag_order_cancel")
     *
     * @param int $id Ид заказа
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cancelAction(int $id)
    {
        $orderManager = $this->get(OrderManager::class)->setOrderById($id);
        $order = $orderManager->getOrder();

        if ('C' !== $order->getStatus()->getShortname()) {
            // IrmagSiteEvents::ORDER_PRE_CANCEL
            $this->get(OrderEventDispatcher::class)->dispatchPreCancel($order);

            $orderManager->cancel();
            $orderManager->save();

            // IrmagSiteEvents::ORDER_POST_CANCEL
            $this->get(OrderEventDispatcher::class)->dispatchPostCancel($order);
        }

        return $this->render('@IrmagSite/Order/cancel.html.twig', [
            'id' => $id,
        ]);
    }

    /**
     * @Route("/finish/", name="irmag_order_finish", options={"expose": true})
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function finishAction(Request $request)
    {
        $session = $request->getSession();

        if (false === $session->has(self::SESSION_SUCCESS_ORDER_ID)) {
            return $this->redirectToRoute('irmag_catalog');
        }

        $orderManager = $this->get(OrderManager::class);
        $order = $orderManager->getOrder($session->get(self::SESSION_SUCCESS_ORDER_ID));

        // IrmagSiteEvents::ORDER_PRE_FINISH
        $this->get(OrderEventDispatcher::class)->dispatchPreFinish($order);

        $orderManager->setStatusByShortname('N');
        $orderManager->save();

        // удаляем из сессии orderId
        $session->remove(self::SESSION_SUCCESS_ORDER_ID);

        // IrmagSiteEvents::ORDER_POST_FINISH
        $this->get(OrderEventDispatcher::class)->dispatchPostFinish($order);

        return $this->render('@IrmagSite/Order/finish.html.twig', ['order' => $order]);
    }

    /**
     * @Route("/payment/", name="irmag_order_payment")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function paymentAction(Request $request)
    {
        $session = $request->getSession();
        $paymentManager = $this->get(OnlinePaymentManager::class);
        $orderManager = $this->get(OrderManager::class);

        // если заказ был оплачен, но не оформился по каким-то причинам
        $id = $paymentManager->getOrderPayedAndNotFinished();

        if (!empty($id)) {
            $session->set(self::SESSION_SUCCESS_ORDER_ID, $id);
            $paymentManager->clearPayedAndNotFinishedOrder();

            return $this->redirectToRoute('irmag_order_finish');
        }

        $id = $paymentManager->getUnpayedOrderId();

        if (empty($id)) {
            return $this->redirectToRoute('irmag_catalog');
        }

        return $this->render('@IrmagSite/Order/payment.html.twig', ['order' => $orderManager->getOrder($id)]);
    }
}
