<?php

namespace Irmag\SiteBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use JMS\Serializer\SerializationContext;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Irmag\SiteBundle\Service\SearchQueryNormalizer;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\ElementBrand;
use Irmag\SiteBundle\Entity\ElementCountry;
use Irmag\SiteBundle\Entity\Section;

/**
 * @Route("/cat/search",
 *     defaults={"_format": "json"},
 *     requirements={"searchQuery": ".+", "_format": "json"},
 *     methods={"GET"},
 *     options={"expose": true}
 * )
 */
class CatalogSearchController extends Controller
{
    const LIMIT_ELEMENTS = 6;
    const LIMIT_SECTIONS = 2;
    const LIMIT_BRANDS = 1;
    const LIMIT_COUNTRIES = 1;
    const LIMIT_ACTIONS = 1;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param EntityManagerInterface $em
     * @param SerializerInterface    $serializer
     */
    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer
    ) {
        $this->em = $em;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/opensearch/{searchQuery}.json")
     *
     * @param string $searchQuery Поисковый запрос
     *
     * @return JsonResponse
     */
    public function openSearchAction(string $searchQuery): JsonResponse
    {
        $searchQuery = SearchQueryNormalizer::normilize($searchQuery);

        $result = $this->em
            ->getRepository(Element::class)
            ->search($searchQuery, self::LIMIT_ELEMENTS);

        $completions = [];

        /** @var Element $element */
        foreach ($result as $element) {
            $completions[] = $element->getFullName();
        }

        return new JsonResponse([$searchQuery, $completions]);
    }

    /**
     * @Route("/element/{searchQuery}.json",
     *     name="irmag_catalog_search_element"
     * )
     *
     * @param string $searchQuery Поисковый запрос
     *
     * @return Response
     */
    public function searchElementAction(string $searchQuery): Response
    {
        $searchQuery = SearchQueryNormalizer::normilize($searchQuery);

        $result = $this->em
            ->getRepository(Element::class)
            ->search($searchQuery, self::LIMIT_ELEMENTS);

        return new Response($this->serializeSearchResult($result));
    }

    /**
     * @Route("/section/{searchQuery}.json",
     *     name="irmag_catalog_search_section"
     * )
     *
     * @param string $searchQuery Поисковый запрос
     *
     * @return Response
     */
    public function searchSectionAction(string $searchQuery): Response
    {
        $searchQuery = SearchQueryNormalizer::normilize($searchQuery);

        $result = $this->em
            ->getRepository(Section::class)
            ->search($searchQuery, self::LIMIT_SECTIONS);

        $extraCount = self::LIMIT_SECTIONS - \count($result);

        if (0 !== $extraCount) {
            $elements = $this->em
                ->getRepository(Element::class)
                ->search($searchQuery, $extraCount);

            /** @var Element $element */
            foreach ($elements as $element) {
                if ($element->getSectionElements()->isEmpty()) {
                    continue;
                }

                $section = $element->getSectionElements()->first()->getSection();

                if ($section && !\in_array($section, $result, true)) {
                    array_push($result, $section);
                }
            }
        }

        return new Response($this->serializeSearchResult($result));
    }

    /**
     * @Route("/brand/{searchQuery}.json",
     *     name="irmag_catalog_search_brand"
     * )
     *
     * @param string $searchQuery Поисковый запрос
     *
     * @return Response
     */
    public function searchBrandAction(string $searchQuery): Response
    {
        $searchQuery = SearchQueryNormalizer::normilize($searchQuery);

        $result = $this->em
            ->getRepository(ElementBrand::class)
            ->search($searchQuery, self::LIMIT_BRANDS);

        $extraCount = self::LIMIT_BRANDS - \count($result);

        if (0 !== $extraCount) {
            $elements = $this->em
                ->getRepository(Element::class)
                ->search($searchQuery, $extraCount);

            /** @var Element $element */
            foreach ($elements as $element) {
                $brand = $element->getBrand();

                if ($brand && !\in_array($brand, $result, true)) {
                    array_push($result, $brand);
                }
            }
        }

        return new Response($this->serializeSearchResult($result));
    }

    /**
     * @Route("/country/{searchQuery}.json",
     *     name="irmag_catalog_search_country"
     * )
     *
     * @param string $searchQuery Поисковый запрос
     *
     * @return Response
     */
    public function searchCountryAction(string $searchQuery): Response
    {
        $searchQuery = SearchQueryNormalizer::normilize($searchQuery);

        $result = $this->em
            ->getRepository(ElementCountry::class)
            ->search($searchQuery, self::LIMIT_COUNTRIES);

        return new Response($this->serializeSearchResult($result));
    }

    /**
     * @Route("/action/{searchQuery}.json",
     *     name="irmag_catalog_search_action"
     * )
     *
     * @param string $searchQuery
     *
     * @return Response
     */
    public function searchActionAction(string $searchQuery): Response
    {
        $searchQuery = SearchQueryNormalizer::normilize($searchQuery);

        $result = $this->em
            ->getRepository(Action::class)
            ->search($searchQuery, self::LIMIT_ACTIONS);

        return new Response($this->serializeSearchResult($result));
    }

    /**
     * @param \StdClass[] $result
     * @param string      $group
     *
     * @return mixed|string
     */
    private function serializeSearchResult($result, string $group = 'search')
    {
        return $this->serializer->serialize($result, 'json', SerializationContext::create()->setGroups([$group]));
    }
}
