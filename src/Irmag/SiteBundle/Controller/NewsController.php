<?php

namespace Irmag\SiteBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Irmag\CoreBundle\Model\Feed;
use Irmag\CoreBundle\Model\FeedEntry;
use Irmag\CoreBundle\Service\FeedManager\FeedManager;
use Irmag\SiteBundle\Entity\News;

/**
 * @Route("/news")
 */
class NewsController extends Controller
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * @Route("/", name="irmag_news", options={"sitemap": true})
     *
     * @param Request $request
     *
     * @return Response
     */
    public function indexAction(Request $request): Response
    {
        $pagination = $this->get('knp_paginator')->paginate(
            $this->em->getRepository(News::class)->getNewsQb(),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('@IrmagSite/News/index.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/atom/", name="irmag_news_atom")
     * @Cache(expires="10 minutes", public=true)
     */
    public function atomAction(): Response
    {
        $newsRepo = $this->em->getRepository(News::class);
        $newsElements = $newsRepo->findBy(['isActive' => true], ['id' => 'DESC'], 10);
        $latestNews = $newsRepo->getLatest();
        $lastUpdate = $latestNews->getUpdatedAt();

        $feed = new Feed();
        $feed->setId($this->generateUrl('irmag_news', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setTitle('Интернет-магазин IRMAG.RU — Новости');
        $feed->setLinkSelf($this->generateUrl('irmag_news_atom', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setLink($this->generateUrl('irmag_news', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setUpdated($lastUpdate);

        foreach ($newsElements as $news) {
            $feedEntry = new FeedEntry();
            $feedEntry->setId($this->generateUrl('irmag_news_detail', ['slug' => $news->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL));
            $feedEntry->setTitle($news->getTitle());
            $feedEntry->setLink($this->generateUrl('irmag_news_detail', ['slug' => $news->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL));
            $feedEntry->setUpdated($news->getUpdatedAt());
            $feedEntry->setPublished($news->getCreatedAt());
            $feedEntry->setAuthor('IRMAG');
            $feedEntry->setContent($news->getBody());
            $feed->addEntry($feedEntry);
        }

        $feedManager = $this->get(FeedManager::class);
        $feedManager->setFeed($feed);

        return $feedManager->getResponse();
    }

    /**
     * @Route("/{slug}/", name="irmag_news_detail")
     *
     * @param string $slug
     *
     * @return Response
     */
    public function detailAction(string $slug): Response
    {
        $news = $this->em
            ->getRepository(News::class)
            ->findOneBy(['slug' => $slug, 'isActive' => true]);

        if (!$news) {
            throw $this->createNotFoundException(sprintf('News with slug "%s" not found.', $slug));
        }

        return $this->render('@IrmagSite/News/detail.html.twig', [
            'news' => $news,
        ]);
    }

    /**
     * Важная новость (полоска в шапке).
     *
     * @param bool $isAbsolute
     *
     * @return Response
     */
    public function headerWarningLineAction(bool $isAbsolute = false): Response
    {
        $news = $this->em->getRepository(News::class)->getWarningNews();

        if (!$news) {
            return new Response();
        }

        $masterRequest = $this->get('request_stack')->getMasterRequest();

        if ($masterRequest->query->has('filter') && !empty($masterRequest->query->get('filter')['brand'])) {
            $isAbsolute = true;
        }

        return $this->render('@IrmagSite/News/header-warning-line.html.twig', [
            'news' => $news,
            'is_absolute' => $isAbsolute,
        ]);
    }
}
