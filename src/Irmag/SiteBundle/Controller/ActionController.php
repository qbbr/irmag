<?php

namespace Irmag\SiteBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Irmag\CoreBundle\Model\Feed;
use Irmag\CoreBundle\Model\FeedEntry;
use Irmag\CoreBundle\Service\FeedManager\FeedManager;
use Irmag\SiteBundle\Service\CatalogElementViewSwitcher;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\ActionElement;
use Irmag\SiteBundle\Entity\PromocodeElementsList;
use Irmag\SiteBundle\Repository\ActionRepository;

/**
 * @Route("/action")
 */
class ActionController extends Controller
{
    /**
     * Кол-во товаров в списке к промокоду на странице.
     */
    const PROMOCODE_ELEMENTS_LIST_ITEMS_PER_PAGE = 12;

    /**
     * @var array
     */
    const AVAILABLE_SORT_COLS = ['createdAt'];

    /**
     * @Route("/", name="irmag_action", options={"sitemap": true})
     */
    public function indexAction()
    {
        /** @var ActionRepository $repo */
        $repo = $this->get('doctrine.orm.default_entity_manager')->getRepository(Action::class);

        return $this->render('@IrmagSite/Action/index.html.twig', [
            'actions' => $repo->getActionsQb()->getQuery()->getResult(),
        ]);
    }

    /**
     * @Route("/archive/", name="irmag_action_archive")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function archiveAction(Request $request)
    {
        /** @var ActionRepository $repo */
        $repo = $this->get('doctrine.orm.default_entity_manager')->getRepository(Action::class);

        $actions = $this->get('knp_paginator')->paginate(
            $repo->getActionsQb(true)->getQuery()->getResult(),
            $request->query->getInt('page', 1),
            5
        );

        return $this->render('@IrmagSite/Action/archive.html.twig', [
            'actions' => $actions,
        ]);
    }

    /**
     * @Route("/atom/", name="irmag_action_atom")
     * @Cache(expires="10 minutes", public=true)
     */
    public function atomAction()
    {
        /** @var ActionRepository $repo */
        $repo = $this->get('doctrine.orm.default_entity_manager')->getRepository(Action::class);
        /** @var Action[] $actions */
        $actions = $repo->findBy(['isActive' => true], ['id' => 'DESC'], 10);
        $latestAction = $repo->getLatest();
        $lastUpdate = $latestAction->getUpdatedAt();

        $feed = new Feed();
        $feed->setId($this->generateUrl('irmag_action', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setTitle('Интернет-магазин IRMAG.RU — Акции');
        $feed->setLinkSelf($this->generateUrl('irmag_action_atom', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setLink($this->generateUrl('irmag_action', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setUpdated($lastUpdate);

        foreach ($actions as $action) {
            $feedEntry = new FeedEntry();
            $feedEntry->setId($this->generateUrl('irmag_action_detail', ['slug' => $action->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL));
            $feedEntry->setTitle($action->getTitle());
            $feedEntry->setLink($this->generateUrl('irmag_action_detail', ['slug' => $action->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL));
            $feedEntry->setUpdated($action->getUpdatedAt());
            $feedEntry->setPublished($action->getCreatedAt());
            $feedEntry->setAuthor('IRMAG');
            $feedEntry->setContent($action->getBody());
            $feed->addEntry($feedEntry);
        }

        $feedManager = $this->get(FeedManager::class);
        $feedManager->setFeed($feed);

        return $feedManager->getResponse();
    }

    /**
     * @Route("/{slug}/", name="irmag_action_detail")
     *
     * @param Request $request
     * @param string  $slug    Id or slug
     *
     * @return Response
     */
    public function detailAction(Request $request, string $slug)
    {
        if (
            $request->query->has('sort')
            &&
            false === \in_array($request->query->get('sort'), self::AVAILABLE_SORT_COLS, true)
        ) {
            throw $this->createNotFoundException();
        }

        /** @var ActionRepository $repo */
        $repo = $this->get('doctrine.orm.default_entity_manager')->getRepository(Action::class);

        if (is_numeric($slug)) {
            if ($action = $repo->find($slug)) {
                return $this->redirectToRoute('irmag_action_detail', ['slug' => $action->getSlug()]);
            }
        } else {
            $action = $repo->getOneBySlug($slug);
        }

        if (!$action) {
            throw $this->createNotFoundException(sprintf('Action with slug "%s" not found.', $slug));
        }

        if ($request->isXmlHttpRequest()) {
            return $this->contentAction($action);
        }

        return $this->render('@IrmagSite/Action/detail.html.twig', [
            'action' => $action,
            'is_archive' => (false === $action->getIsActive() || $action->getEndDateTime() < new \DateTime()),
        ]);
    }

    /**
     * Товары в акции.
     *
     * @param Action $action Акция
     *
     * @return Response
     */
    public function contentAction(Action $action)
    {
        $masterRequest = $this->get('request_stack')->getMasterRequest();
        $this->get(CatalogElementViewSwitcher::class)->setGrouped(CatalogElementViewSwitcher::GROUPED_VIEW_NAME);

        $showAll = $this->isGranted('ROLE_SONATA_ADMIN');
        $actionElements = $this->get('doctrine.orm.default_entity_manager')->getRepository(ActionElement::class)->getElementsByAction($action, $showAll);

        $pagination = $this->get('knp_paginator')->paginate(
            $actionElements,
            $masterRequest->query->getInt('page', 1),
            $masterRequest->query->getInt('perpage', 108),
            [
                'sortFieldWhitelist' => self::AVAILABLE_SORT_COLS,
                'defaultSortFieldName' => self::AVAILABLE_SORT_COLS[0],
                'defaultSortDirection' => 'desc',
            ]
        );

        return $this->render('@IrmagSite/Action/content.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @param Action $action
     *
     * @return Response
     */
    public function contentPromocodeElementsAction(Action $action)
    {
        $masterRequest = $this->get('request_stack')->getMasterRequest();
        $elements = $this
            ->get('doctrine.orm.default_entity_manager')
            ->getRepository(PromocodeElementsList::class)
            ->getElementsOfList($action->getPromocodeElementsList());

        $promocodeElementsPagination = $this->get('knp_paginator')->paginate(
            $elements,
            $masterRequest->query->getInt('promocodeListPage', 1),
            self::PROMOCODE_ELEMENTS_LIST_ITEMS_PER_PAGE,
            [
                'pageParameterName' => 'promocodeListPage',
            ]
        );

        return $this->render('@IrmagSite/Action/promocode-elements-content.html.twig', [
            'promocodeElementsListPagination' => $promocodeElementsPagination,
        ]);
    }

    public function oneRandomOfferAction()
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $action = $em->getRepository(Action::class)->getOneRandom();

        if (!$action) {
            return new Response();
        }

        $em->refresh($action);

        return $this->render('@IrmagSite/Action/one-random-offer.html.twig', [
            'action' => $action,
        ]);
    }
}
