<?php

namespace Irmag\SiteBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Irmag\SiteBundle\Utils\PromocodeGenerator;
use Irmag\SiteBundle\Entity\Promocode;
use Irmag\SiteBundle\Entity\SecretPromocodePage;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class SecretPromocodePageController extends Controller
{
    const SESSION_SECRET_PROMOCODE_ID = 'secret_promocode_id';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param EntityManagerInterface $em
     * @param SessionInterface       $session
     */
    public function __construct(
        EntityManagerInterface $em,
        SessionInterface $session
    ) {
        $this->em = $em;
        $this->session = $session;
    }

    /**
     * @Route("/secret/{slug}.json",
     *     name="irmag_secret_page_get_promocode",
     *     defaults={"_format": "json"},
     *     requirements={"query": ".+", "_format": "json"},
     *     methods={"GET"},
     *     options={"expose": true}
     * )
     *
     * @param string $slug
     *
     * @return JsonResponse
     */
    public function getPromocodeAction(string $slug)
    {
        return new JsonResponse([
            'code' => $this->getPromocode($slug)->getCode(),
        ]);
    }

    /**
     * @Route("/secret/{slug}/", name="irmag_secret_page")
     *
     * @param string $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(string $slug)
    {
        return $this->render('@IrmagSite/SecretPromocodePage/index.html.twig', [
            'secret_promocode_page' => $this->getSecretPromocodePage($slug),
        ]);
    }

    /**
     * @param string $slug
     *
     * @return Promocode
     */
    private function getPromocode(string $slug): Promocode
    {
        $secretPromocodePage = $this->getSecretPromocodePage($slug);

        if ($this->session->has(self::SESSION_SECRET_PROMOCODE_ID)) {
            $promocode = $this->em->getRepository(Promocode::class)->find($this->session->get(self::SESSION_SECRET_PROMOCODE_ID));
        }

        // (re)generate
        if (!isset($promocode) || false === $promocode->getIsActive() || $secretPromocodePage->getDiscount() !== $promocode->getDiscount()) {
            $code = 'secret-'.PromocodeGenerator::generate(6);

            $promocode = new Promocode();
            $promocode->setCode($code);
            $promocode->setDiscount($secretPromocodePage->getDiscount());
            $promocode->setDescription(sprintf('Секретная страница (id: %d) "%s"', $secretPromocodePage->getId(), $secretPromocodePage->getName()));
            $promocode->setIsPersonal(true);

            $this->em->persist($promocode);
            $this->em->flush();

            $this->session->set(self::SESSION_SECRET_PROMOCODE_ID, $promocode->getId());
        }

        return $promocode;
    }

    /**
     * @param string $slug
     *
     * @return SecretPromocodePage
     */
    private function getSecretPromocodePage(string $slug): SecretPromocodePage
    {
        /** @var SecretPromocodePage $secretPromocodePage */
        $secretPromocodePage = $this->em
            ->getRepository(SecretPromocodePage::class)
            ->getOneBySlug($slug);

        if (!$secretPromocodePage) {
            throw $this->createNotFoundException(sprintf('SecretPromocodePage with slug "%s" not found.', $slug));
        }

        return $secretPromocodePage;
    }
}
