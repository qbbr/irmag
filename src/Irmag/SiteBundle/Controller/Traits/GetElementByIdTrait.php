<?php

namespace Irmag\SiteBundle\Controller\Traits;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Irmag\SiteBundle\Entity\Element;

trait GetElementByIdTrait
{
    /**
     * @param int $id
     *
     * @throws NotFoundHttpException When element not found
     *
     * @return Element
     */
    protected function getElementById(int $id): Element
    {
        if (!$element = $this->get('doctrine.orm.default_entity_manager')->getRepository(Element::class)->find($id)) {
            throw new NotFoundHttpException(sprintf('Element with id "%d" not found.', $id));
        }

        return $element;
    }
}
