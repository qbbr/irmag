<?php

namespace Irmag\SiteBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Asset\Packages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\ProfileBundle\Entity\UserNotification;
use Irmag\BlogBundle\Entity\BlogPost;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\News;
use Irmag\SiteBundle\Entity\IndexElementsCarousel;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="irmag_homepage", options={"expose": true, "sitemap": true})
     */
    public function indexAction(): Response
    {
        return $this->render('@IrmagSite/Default/index.html.twig');
    }

    /**
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function navbarUserMenuAction(
        EntityManagerInterface $em
    ): Response {
        $notificationCount = $em->createQueryBuilder()
            ->select('count(un.id)')
            ->from(UserNotification::class, 'un')
            ->where('un.isAsRead = false')
            ->andWhere('un.user = :user')
            ->setParameter('user', $this->getUser())
            ->getQuery()
            ->getSingleScalarResult();

        return $this->render('@IrmagSite/header-navbar-user-menu.html.twig', [
            'notification_count' => $notificationCount,
        ]);
    }

    /**
     * Баннеры на главной.
     *
     * @param EntityManagerInterface $em
     * @param Packages               $assetPackages
     *
     * @return Response
     */
    public function indexBannersAction(
        EntityManagerInterface $em,
        Packages $assetPackages
    ): Response {
        $news = $em->getRepository(News::class)->getIndexBanners();
        $actions = $actionsImportant = [];

        /** @var Action $action */
        foreach ($em->getRepository(Action::class)->getIndexBanners() as $action) {
            if ($action->getIsImportant()) {
                $actionsImportant[] = $action;
            } else {
                $actions[] = $action;
            }
        }

        shuffle($actions);

        /* @var Action|News $object */
        foreach (array_merge($news, $actionsImportant, $actions) as $object) {
            $route = $object instanceof News
                ? 'irmag_news_detail'
                : 'irmag_action_detail';

            $extraContent = $object instanceof News
                ? html_entity_decode($object->getExtraContentOnBanner())
                : null;

            $banners[] = [
                'extra_content' => $extraContent,
                'banner_bg' => $assetPackages->getUrl($object->getBannerBg()->getWebPath()),
                'banner' => $assetPackages->getUrl($object->getBanner()->getWebPath()),
                'link' => $this->generateUrl($route, ['slug' => $object->getSlug()]),
                'title' => $object->getTitle(),
            ];
        }

        return $this->render('@IrmagSite/Default/index-banners.html.twig', [
            'banners' => $banners ?? [],
        ]);
    }

    /**
     * Карусель товаров на главной.
     *
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function indexElementsCarouselAction(
        EntityManagerInterface $em
    ): Response {
        return $this->render('@IrmagSite/Default/index-elements-carousel.html.twig', [
            'carousels' => $em->getRepository(IndexElementsCarousel::class)->getCarousels(),
        ]);
    }

    /**
     * Новые обзоры в блоге.
     *
     * @param EntityManagerInterface $em
     *
     * @return Response
     */
    public function indexLastBlogPostsAction(
        EntityManagerInterface $em
    ): Response {
        return $this->render('@IrmagSite/Default/index-last-blog-posts.html.twig', [
            'blog_posts' => $em->getRepository(BlogPost::class)->getLastPosts(),
        ]);
    }
}
