<?php

namespace Irmag\SiteBundle\Mailer;

use Irmag\CoreBundle\Mailer\Mailer;

class SiteMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(
        Mailer $mailer
    ) {
        $this->mailer = $mailer;
    }

    /**
     * @param string|array $to
     * @param array        $context
     *
     * @return int
     */
    public function sendOrderFinishMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagSite/Emails/order-finish.html.twig', $context);
    }

    /**
     * @param string|array $to
     * @param array        $context
     *
     * @return int
     */
    public function sendOrderCancelMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagSite/Emails/order-cancel.html.twig', $context);
    }

    /**
     * @param string|array $to
     * @param array        $context
     *
     * @return int
     */
    public function sendOrderOnlinePaymentFinishedMessage($to, $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagSite/Emails/online-payment-finished.html.twig', $context);
    }
}
