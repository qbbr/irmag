<?php

namespace Irmag\SiteBundle\EventDispatcher;

use Irmag\SiteBundle\IrmagSiteEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\Event;

class DeliveryEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function dispatchSelectTransportCompany(): void
    {
        $this->eventDispatcher->dispatch(IrmagSiteEvents::DELIVERY_SELECT_TRANSPORT_COMPANY, new Event());
    }

    public function dispatchSelectPost(): void
    {
        $this->eventDispatcher->dispatch(IrmagSiteEvents::DELIVERY_SELECT_POST, new Event());
    }
}
