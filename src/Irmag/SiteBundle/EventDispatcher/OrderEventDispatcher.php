<?php

namespace Irmag\SiteBundle\EventDispatcher;

use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Event\OrderEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class OrderEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Order $order
     */
    public function dispatchPreCancel(Order $order): void
    {
        $this->eventDispatcher->dispatch(IrmagSiteEvents::ORDER_PRE_CANCEL, $this->getOrderEvent($order));
    }

    /**
     * @param Order $order
     */
    public function dispatchPostCancel(Order $order): void
    {
        $this->eventDispatcher->dispatch(IrmagSiteEvents::ORDER_POST_CANCEL, $this->getOrderEvent($order));
    }

    /**
     * @param Order $order
     */
    public function dispatchPreFinish(Order $order): void
    {
        $this->eventDispatcher->dispatch(IrmagSiteEvents::ORDER_PRE_FINISH, $this->getOrderEvent($order));
    }

    /**
     * @param Order $order
     */
    public function dispatchPostFinish(Order $order): void
    {
        $this->eventDispatcher->dispatch(IrmagSiteEvents::ORDER_POST_FINISH, $this->getOrderEvent($order));
    }

    /**
     * @param Order $order
     */
    public function dispatchSetOnlinePayment(Order $order): void
    {
        $this->eventDispatcher->dispatch(IrmagSiteEvents::ORDER_SET_ONLINE_PAYMENT, $this->getOrderEvent($order));
    }

    /**
     * @param Order $order
     *
     * @return OrderEvent
     */
    private function getOrderEvent(Order $order): OrderEvent
    {
        return new OrderEvent($order);
    }
}
