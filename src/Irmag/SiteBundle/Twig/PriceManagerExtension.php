<?php

namespace Irmag\SiteBundle\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Irmag\SiteBundle\Service\PriceManager\PriceManager;
use Irmag\SiteBundle\Entity\Element;

class PriceManagerExtension extends AbstractExtension
{
    /**
     * @var PriceManager
     */
    protected $priceManager;

    /**
     * @param PriceManager $priceManager
     */
    public function __construct(PriceManager $priceManager)
    {
        $this->priceManager = $priceManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            new TwigFunction('get_price_with_discount', [$this, 'getPriceWithDiscount']),
        ];
    }

    /**
     * @param Element $element
     *
     * @return float
     */
    public function getPriceWithDiscount(Element $element)
    {
        return $this->priceManager->getPrice($element);
    }
}
