<?php

namespace Irmag\SiteBundle\Features\Context;

use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;

class FeatureContext extends MinkContext implements KernelAwareContext
{
    private $kernel;
    private $siteParameterName;

    public function __construct($siteParameterName)
    {
        $this->siteParameterName = $siteParameterName;
    }

    /**
     * @BeforeScenario
     */
    public function before()
    {
        $container = $this->getContainer();
        $siteDomain = $container->getParameter($this->siteParameterName);
        $this->setMinkParameter('base_url', sprintf('https://%s/', $siteDomain));
    }

    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }

    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * Click on the element with the provided CSS Selector.
     *
     * @When /^я кликаю по элементу "([^"]*)"$/
     *
     * @param string $cssSelector
     */
    public function iClickOnTheElementWithCSSSelector($cssSelector)
    {
        $session = $this->getSession();
        $element = $session->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath('css', $cssSelector)
        );

        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Could not evaluate CSS Selector: "%s"', $cssSelector));
        }

        $element->click();
    }
}
