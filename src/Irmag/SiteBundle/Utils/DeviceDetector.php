<?php

namespace Irmag\SiteBundle\Utils;

class DeviceDetector
{
    const MOBILE_TYPE = 'mobile';
    const DESKTOP_TYPE = 'desktop';

    /**
     * @return string
     */
    public static function getDevice(): string
    {
        $detect = new \Mobile_Detect();

        if ($detect->isMobile()) {
            return self::MOBILE_TYPE;
        }

        return self::DESKTOP_TYPE;
    }
}
