<?php

namespace Irmag\SiteBundle\Form\Helper;

use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CatalogFilterPropIsNotAllowedException extends NotFoundHttpException
{
    /**
     * @param string|null     $propName
     * @param \Exception|null $previous
     * @param int             $code
     */
    public function __construct(string $propName = null, \Exception $previous = null, $code = 0)
    {
        parent::__construct(sprintf('Prop "%s" not allowed.', $propName), $previous, $code);
    }
}
