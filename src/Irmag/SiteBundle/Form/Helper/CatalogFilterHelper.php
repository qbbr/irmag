<?php

namespace Irmag\SiteBundle\Form\Helper;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Irmag\SiteBundle\Service\PriceManager\Adapter\ActionAdapter;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\Section;
use Irmag\SiteBundle\Entity\ElementManufacturer;
use Irmag\SiteBundle\Entity\ElementBrand;
use Irmag\SiteBundle\Entity\ElementCountry;

class CatalogFilterHelper
{
    /**
     * @var Section
     */
    private $section;

    /**
     * @var ElementCountry
     */
    private $country;

    /**
     * @var ElementManufacturer
     */
    private $manufacturer;

    /**
     * @var ElementBrand
     */
    private $brand;

    /**
     * @var int|float
     */
    private $minPrice;

    /**
     * @var int|float
     */
    private $maxPrice;

    /**
     * @var bool
     */
    private $isFilterUsed = false;

    /**
     * @var array
     */
    private $filterData = [];

    /**
     * @var bool
     */
    private $isSearchUsed = false;

    /**
     * @var string
     */
    private $searchQuery = '';

    /**
     * @var QueryBuilder
     */
    private $qb;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ActionAdapter
     */
    private $actionAdapter;

    /**
     * @param EntityManagerInterface $em
     * @param ActionAdapter          $actionAdapter
     */
    public function __construct(
        EntityManagerInterface $em,
        ActionAdapter $actionAdapter
    ) {
        $this->em = $em;
        $this->actionAdapter = $actionAdapter;
        $this->qb = $em->createQueryBuilder()
            ->from(Element::class, 'e')
            ->andWhere('e.isActive = true');
    }

    /**
     * @param Section|null $section
     *
     * @return CatalogFilterHelper
     */
    public function setSection(?Section $section): self
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @param ElementManufacturer|null $manufacturer
     *
     * @return CatalogFilterHelper
     */
    public function setManufacturer(?ElementManufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    /**
     * @param ElementBrand|null $brand
     *
     * @return CatalogFilterHelper
     */
    public function setBrand(?ElementBrand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * @param ElementCountry|null $country
     *
     * @return CatalogFilterHelper
     */
    public function setCountry(?ElementCountry $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @param int|float $minPrice
     *
     * @return CatalogFilterHelper
     */
    public function setMinPrice($minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    /**
     * @param int|float $maxPrice
     *
     * @return CatalogFilterHelper
     */
    public function setMaxPrice($maxPrice): self
    {
        $this->maxPrice = $maxPrice;

        return $this;
    }

    /**
     * @param array $filterData
     *
     * @return CatalogFilterHelper
     */
    public function setFilterData(array $filterData): self
    {
        $this->filterData = $filterData;
        $this->isFilterUsed = true;

        return $this;
    }

    /**
     * @param string $searchQuery
     *
     * @return CatalogFilterHelper
     */
    public function setSearchQuery(string $searchQuery): self
    {
        $this->searchQuery = $searchQuery;
        $this->isSearchUsed = true;

        return $this;
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return QueryBuilder
     */
    private function addSectionQbIfNeed(QueryBuilder $qb): QueryBuilder
    {
        if (null === $this->section) {
            return $qb;
        }

        return $qb
            ->leftJoin('e.sectionElements', 'se')
            ->leftJoin('se.section', 's')
            ->andWhere('s.isActive = true')
            ->andWhere('s.lft >= :lft')
            ->andWhere('s.rgt <= :rgt')
            ->andWhere('s.root = :root')
            ->setParameters([
                'lft' => $this->section->getLft(),
                'rgt' => $this->section->getRgt(),
                'root' => $this->section->getRoot(),
            ]);
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return QueryBuilder
     */
    private function addManufacturerAndBrandsQbIfNeed(QueryBuilder $qb): QueryBuilder
    {
        if (null !== $this->manufacturer) {
            $qb
                ->leftJoin('e.manufacturer', 'm_')
                ->andWhere('m_.id = :manufacturerId')
                ->setParameter('manufacturerId', $this->manufacturer->getId());
        }

        if (null !== $this->brand) {
            $qb
                ->leftJoin('e.brand', 'b_')
                ->andWhere('b_.id = :brandId')
                ->setParameter('brandId', $this->brand->getId());
        }

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return QueryBuilder
     */
    private function addCountryQbIfNeed(QueryBuilder $qb): QueryBuilder
    {
        if (null === $this->country) {
            return $qb;
        }

        return $qb
            ->leftJoin('e.country', 'c_')
            ->andWhere('c_.id = :countryId')
            ->setParameter('countryId', $this->country->getId());
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return QueryBuilder
     */
    private function addPriceQbIfNeed(QueryBuilder $qb): QueryBuilder
    {
        if ($this->minPrice) {
            $qb = $qb
                ->andWhere('e.price >= :minPrice')
                ->setParameter('minPrice', $this->minPrice);
        }

        if ($this->maxPrice) {
            $qb = $qb
                ->andWhere('e.price <= :maxPrice')
                ->setParameter('maxPrice', $this->maxPrice);
        }

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return QueryBuilder
     */
    private function addPropsQbIfNeed(QueryBuilder $qb): QueryBuilder
    {
        if (false === $this->isFilterUsed) {
            return $qb;
        }

        foreach ($this->filterData['prop'] ?? [] as $prop) {
            $dqlPropName = 'prop'.$prop;

            switch ($prop) {
                case 'Action': // pseudo-prop need sub request to actions
                    break;

                case 'Transfer': // int
                    $qb->andWhere(sprintf('e.%s >= 0', $dqlPropName));
                    break;

                default: // bool
                    $qb->andWhere(sprintf('e.%s = true', $dqlPropName));
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     *
     * @return QueryBuilder
     */
    private function addSearchQbIfNeed(QueryBuilder $qb): QueryBuilder
    {
        if (false === $this->isSearchUsed) {
            return $qb;
        }

        return $qb
            ->andWhere('PLAINTSQUERY(e.tsv, :searchQuery, \'irmag\') = true')
            ->setParameter('searchQuery', $this->searchQuery);
    }

    /**
     * @return array
     */
    public function getManufacturerAndBrandsList()
    {
        $qb = clone $this->qb;
        $qb
            ->select('b.id, b.name, m.id as manufacturer_id, m.name as manufacturer_name')
            ->innerJoin('e.brand', 'b')
            ->leftJoin('b.manufacturer', 'm')
            ->addGroupBy('b.id, m.id')
            ->orderBy('b.name', 'ASC');

        $qb = $this->addSectionQbIfNeed($qb);
        $qb = $this->addCountryQbIfNeed($qb);
        $qb = $this->addPriceQbIfNeed($qb);
        $qb = $this->addPropsQbIfNeed($qb);
        $qb = $this->addSearchQbIfNeed($qb);

        $brands = $qb
            ->getQuery()
            ->useResultCache(true)
            ->getResult();

        $manufacturersAndBrands = [];

        foreach ($brands as $brand) {
            if (!isset($manufacturersAndBrands[$brand['manufacturer_id']])) {
                $manufacturersAndBrands[$brand['manufacturer_id']] = [
                    'id' => $brand['manufacturer_id'],
                    'name' => $brand['manufacturer_name'],
                    '_childrens' => [],
                ];
            }

            $manufacturersAndBrands[$brand['manufacturer_id']]['_childrens'][] = [
                'id' => $brand['id'],
                'name' => $brand['name'],
            ];
        }

        return $manufacturersAndBrands;
    }

    /**
     * @return array
     */
    public function getCountryList()
    {
        $qb = clone $this->qb;
        $qb
            ->select('c.id, c.name')
            ->innerJoin('e.country', 'c')
            ->addGroupBy('c.id')
            ->orderBy('c.name', 'ASC');

        $qb = $this->addSectionQbIfNeed($qb);
        $qb = $this->addManufacturerAndBrandsQbIfNeed($qb);
        $qb = $this->addPriceQbIfNeed($qb);
        $qb = $this->addPropsQbIfNeed($qb);
        $qb = $this->addSearchQbIfNeed($qb);

        return $qb
            ->getQuery()
            ->useResultCache(true)
            ->getResult();
    }

    /**
     * @return int
     */
    public function getMinPrice(): int
    {
        $qb = clone $this->qb;
        $qb->select('MIN(e.price)');
        $qb = $this->addSectionQbIfNeed($qb);
        $qb = $this->addPropsQbIfNeed($qb);
        $qb = $this->addSearchQbIfNeed($qb);

        return (int) round($qb->getQuery()->useResultCache(true)->getSingleScalarResult(), 0, \PHP_ROUND_HALF_DOWN);
    }

    /**
     * @return int
     */
    public function getMaxPrice(): int
    {
        $qb = clone $this->qb;
        $qb->select('MAX(e.price)');
        $qb = $this->addSectionQbIfNeed($qb);
        $qb = $this->addPropsQbIfNeed($qb);
        $qb = $this->addSearchQbIfNeed($qb);

        return (int) round($qb->getQuery()->useResultCache(true)->getSingleScalarResult()); // , 0, \PHP_ROUND_HALF_UP
    }

    /**
     * @param string $propName Название свойства
     *
     * @throws CatalogFilterPropIsNotAllowedException
     *
     * @return bool
     */
    public function getPropIsDisabled(string $propName): bool
    {
        if (false === \in_array($propName, Element::getAvailablePropNames(), true)) {
            throw new CatalogFilterPropIsNotAllowedException($propName);
        }

        $qb = clone $this->qb;
        $qb->select('COUNT(DISTINCT e.id)');

        $qb = $this->addSectionQbIfNeed($qb);
        $qb = $this->addManufacturerAndBrandsQbIfNeed($qb);
        $qb = $this->addCountryQbIfNeed($qb);
        $qb = $this->addPriceQbIfNeed($qb);
        $qb = $this->addSearchQbIfNeed($qb);

        switch ($propName) {
            case 'Action':
                $qb->andWhere('e.id IN (:elementIds)')->setParameter('elementIds', $this->actionAdapter->getElementIds());
                break;

            case 'Transfer':
                $qb->andWhere('e.prop'.$propName.' IS NOT NULL');
                break;

            default:
                $qb->andWhere('e.prop'.$propName.' = true');
                break;
        }

        $result = $qb
            ->getQuery()
            ->useResultCache(true)
            ->getSingleScalarResult();

        return 0 === $result;
    }
}
