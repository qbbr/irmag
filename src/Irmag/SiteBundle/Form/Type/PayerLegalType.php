<?php

namespace Irmag\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PayerLegalType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('payerLegalAddress', null, ['label' => 'form.label.legal_address'])
            ->add('payerLegalActualAddress', null, ['label' => 'form.label.legal_actual_address'])
            ->add('payerLegalConsigneeAddress', null, ['label' => 'form.label.legal_consignee_address'])
            ->add('payerLegalName', null, ['label' => 'form.label.legal_name'])
            ->add('payerLegalInn', null, ['label' => 'form.label.legal_inn'])
            ->add('payerLegalKpp', null, ['label' => 'form.label.legal_kpp'])
            ->add('payerLegalOgrn', null, ['label' => 'form.label.legal_ogrn'])
            ->add('payerLegalRs', null, ['label' => 'form.label.legal_rs'])
            ->add('payerLegalKrs', null, ['label' => 'form.label.legal_krs'])
            ->add('payerLegalRb', null, ['label' => 'form.label.legal_rb'])
            ->add('payerLegalRbCity', null, ['label' => 'form.label.legal_rb_city'])
            ->add('payerLegalBik', null, ['label' => 'form.label.legal_bik'])
            ->add('payerLegalTelephone', null, ['label' => 'form.label.legal_telephone'])
            ->add('payerLegalEmail', null, ['label' => 'form.label.legal_email'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'irmag_site',
            'inherit_data' => true,
            'required' => true,
            'label' => false,
        ]);
    }
}
