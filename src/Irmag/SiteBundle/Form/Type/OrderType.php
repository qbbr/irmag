<?php

namespace Irmag\SiteBundle\Form\Type;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Irmag\SiteBundle\Form\Helper\OrderHelper;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Entity\OrderDeliveryMethod;
use Irmag\SiteBundle\Entity\OrderDeliveryTime;
use Irmag\SiteBundle\Entity\OrderDeliverySelfserviceMethod;
use Irmag\SiteBundle\Entity\OrderPaymentMethod;
use Irmag\SiteBundle\Config;
use Symfony\Component\Validator\Constraints\NotBlank;

class OrderType extends AbstractType
{
    /**
     * @var OrderHelper
     */
    private $orderHelper;

    /**
     * @param OrderHelper $orderHelper
     */
    public function __construct(
        OrderHelper $orderHelper
    ) {
        $this->orderHelper = $orderHelper;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var Order $order */
        $order = $builder->getData();

        if ($options['max_transfer_value'] > 0) {
            $this->orderHelper->setGlobalMinDaysBeforeDelivery($options['max_transfer_value']);
        }

        $this->orderHelper->process($order);

        $availableDeliveryMethods = $this->orderHelper->getAvailableDeliveryMethods();
        $dayOfWeekEnabled = $this->orderHelper->getAvailableDaysOfWeek();
        $dayOfWeekDisabled = array_diff([1, 2, 3, 4, 5, 6, 0], $dayOfWeekEnabled);
        $availableTimeIntervals = $this->orderHelper->getAvailableTimeIntervals();
        $deliveryCity = $order->getDeliveryCity();

        $builder
            // Тип плательщика
            ->add('isPayerLegal', HubChoiceType::class, [
                'label' => false,
                'expanded' => true,
                'required' => false,
                'placeholder' => false, // fix hub placeholder data bug if required=false
                'choices' => [
                    'form.label.payer_type_individual' => false,
                    'form.label.payer_type_legal' => true,
                ],
                'data' => $order->getIsPayerLegal(),
            ])
            // Физическое лицо
            ->add('payerIndividual', PayerIndividualType::class)
            // Способ доставки
            ->add('deliveryMethod', HubEntityType::class, [
                'class' => OrderDeliveryMethod::class,
                'choices' => $availableDeliveryMethods,
                'choice_label' => 'name',
                'label' => false,
                'expanded' => true,
            ])
            // Адрес доставки
            ->add('deliveryAddress', DeliveryAddressType::class, [
                'has_only_for_home' => $options['has_only_for_home'],
                'delivery_city' => $deliveryCity,
            ])
            // Дата доставки
            ->add('deliveryDate', DateType::class, [
                'widget' => 'single_text',
                'format' => Config::DATE_FORMAT_RFC3339,
                'label' => 'form.label.delivery_date',
                'required' => true,
                'attr' => [
                    'class' => 'datepicker',
                    'readonly' => true,
                    'placeholder' => 'Выбрать',
                    'data-date-days-of-week-highlighted' => json_encode(array_values($dayOfWeekEnabled)),
                    'data-date-days-of-week-disabled' => json_encode(array_values($dayOfWeekDisabled)),
                    'data-date-dates-disabled' => json_encode(array_values($this->orderHelper->getDisabledDates())),
                ],
            ])
            // Время доставки
            ->add('deliveryTime', EntityType::class, [
                'class' => OrderDeliveryTime::class,
                'choices' => $availableTimeIntervals,
                'label' => 'form.label.delivery_time',
                'choice_label' => 'name',
                'choice_attr' => function ($choiceValue, $key, $value) {
                    /* @var OrderDeliveryTime $choiceValue */
                    return empty($choiceValue->getShortname())
                        ? []
                        : ['data-shortname' => $choiceValue->getShortname()];
                },
                'preferred_choices' => function ($value, $key) {
                    /* @var OrderDeliveryTime $value */
                    return 'fast' !== $value->getShortname();
                },
                'placeholder' => empty($availableTimeIntervals) ? 'Выберите дату' : false,
                'disabled' => empty($availableTimeIntervals),
                'required' => true,
            ])
            // Способ оплаты
            ->add('paymentMethod', HubEntityType::class, [
                'class' => OrderPaymentMethod::class,
                'choices' => $this->orderHelper->getAvailablePaymentMethods(),
                'choice_label' => 'name',
                'label' => false,
                'expanded' => true,
            ])
            ->add('checkDeliveryMethod', CheckDeliveryMethodType::class, [
                'label' => false,
                'required' => false,
                'multiple' => false,
                'expanded' => true,
                'placeholder' => false,
                'choices' => [
                    'form.label.check_delivery.email' => 'email',
                    'form.label.check_delivery.sms' => 'sms',
                ],
                'has_email' => $this->orderHelper->hasEmail(),
                'has_telephone' => $this->orderHelper->hasTelephone(),
            ])
            ->add('isAgreementAccepted', AgreementType::class, [
                'data' => true,
                'label' => 'Я даю своё согласие на обработку ',
            ])
            ->addEventListener(FormEvents::POST_SET_DATA, function (FormEvent $event) use ($order, $options) {
                /** @var Order $data */
                $data = $event->getData();
                $form = $event->getForm();
                $paymentMethod = $data->getPaymentMethod();
                $deliveryMethod = $data->getDeliveryMethod();

                // Юридическое лицо
                if ($data->getIsPayerLegal()) {
                    $form->add('payerLegal', PayerLegalType::class);
                }

                // Спешанный платёж
                if ($paymentMethod && 'mixed' === $paymentMethod->getShortname()) {
                    $form
                        ->add('mixedPayment', MixedPaymentType::class, [
                            'label' => false,
                            'required' => false,
                            'total' => (float) $options['total'],
                        ])
                    ;
                }

                // Способ самовывоза
                if ($deliveryMethod && 'selfservice' === $deliveryMethod->getShortname()) {
                    $availableSelfserviceMethods = $this->orderHelper->getAvailableSelfserviceMethods();

                    $form
                        ->add('selfserviceMethod', ChoiceType::class, [
                            'required' => true,
                            'placeholder' => 'Выберите адрес',
                            'label' => 'form.label.selfservice',
                            'choices' => $availableSelfserviceMethods,
//                            'data' => $availableSelfserviceMethods->last(),
                            'choice_label' => function ($choiceValue) {
                                /* @var OrderDeliverySelfserviceMethod $choiceValue */
                                return (string) $choiceValue;
                            },
                            'choice_value' => function (?OrderDeliverySelfserviceMethod $entity) {
                                return $entity ? $entity->getId() : '';
                            },
                            'choice_attr' => function ($choiceValue, $key, $value) {
                                /* @var OrderDeliverySelfserviceMethod $choiceValue */
                                return ['data-address' => $choiceValue->getAddress()];
                            },
                            'constraints' => [
                                new NotBlank(),
                            ],
                        ])
                    ;

//                    if (!$order->getSelfserviceMethod() && false === $availableSelfserviceMethods->isEmpty()) {
//                        $order->setSelfserviceMethod($availableSelfserviceMethods->first());
//                    }
                }

                // default selects
                if (null !== $deliveryMethod = $form->get('deliveryMethod')->getData()) {
                    $order->setDeliveryMethod($deliveryMethod);
                }
            })
            ->addEventListener(FormEvents::POST_SUBMIT, function (FormEvent $event) {
                /** @var Order $data */
                $data = $event->getData();

                // XXX: bugfix deliveryTime. Wat? again?
                if (null === $data->getDeliveryTime()) {
                    $event->getForm()->addError(new \Symfony\Component\Form\FormError(''));
                }
            })
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars = array_merge($view->vars, [
            'max_transfer_value' => $options['max_transfer_value'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'irmag_site',
            'max_transfer_value' => null,
            'data_class' => Order::class,
            'attr' => [
                'autocomplete' => 'off', // disable autocomplete for all fields
            ],
            'validation_groups' => function (FormInterface $form) {
                /** @var Order $data */
                $data = $form->getData();
                $validators = ['Default'];

                // не самовывоз
                if (null !== $data->getDeliveryMethod() && 'selfservice' !== $data->getDeliveryMethod()->getShortname()) {
                    $validators[] = 'delivery_address';
                    $deliveryCity = $data->getDeliveryCity();

                    if ($deliveryCity && !$deliveryCity->getDistricts()->isEmpty()) {
                        $validators[] = 'delivery_address_district';
                    }
                }

                // юр.лицо
                if ($data->getIsPayerLegal()) {
                    $validators[] = 'payer_legal';
                }

                return $validators;
            },
            'has_only_for_home' => false,
        ]);

        $resolver->setRequired('total');
        $resolver->setAllowedTypes('has_only_for_home', 'bool');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'order';
    }
}
