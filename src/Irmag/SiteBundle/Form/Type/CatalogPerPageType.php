<?php

namespace Irmag\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CatalogPerPageType extends AbstractType
{
    const PER_PAGE_ALLOWED_LIST = [36, 72, 108, 180, 288, 360];
    const PER_PAGE_DEFAULT = 36;

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('perpage', ChoiceType::class, [
                'label' => 'По:',
                'choices' => $this->getChoices(),
            ])
            ->add('submit', SubmitType::class, ['label' => 'OK']);
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'required' => false,
            'csrf_protection' => false,
            'translation_domain' => false,
            'choice_translation_domain' => false,
        ]);
    }

    /**
     * @return array
     */
    private function getChoices(): array
    {
        $choices = [];

        foreach (self::PER_PAGE_ALLOWED_LIST as $value) {
            $choices[$value] = $value;
        }

        return $choices;
    }
}
