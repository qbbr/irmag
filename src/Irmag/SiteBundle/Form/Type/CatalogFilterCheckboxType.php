<?php

namespace Irmag\SiteBundle\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class CatalogFilterCheckboxType extends ChoiceType
{
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'filter_checkbox';
    }
}
