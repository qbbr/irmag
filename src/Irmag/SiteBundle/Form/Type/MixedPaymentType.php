<?php

namespace Irmag\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\SiteBundle\Entity\OrderMixedPayment;

class MixedPaymentType extends AbstractType
{
    const MIN_CERTIFICATE_SUM = 500;
    const MULTIPLE_OF_CERTIFICATE_SUM = 500; // XXX: unused, inject it to js logic

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('total', HiddenType::class, [
                'mapped' => false,
                'empty_data' => $options['total'],
                'label' => false,
            ])
            ->add('sumCash', MoneyType::class, [
                'currency' => 'RUB',
                'scale' => 2,
                'label' => 'Наличными',
                'empty_data' => $options['total'],
                'attr' => [
                    'min' => 0,
                    'data-tag' => 'cash',
                ],
            ])
            ->add('sumCard', MoneyType::class, [
                'currency' => 'RUB',
                'scale' => 2,
                'label' => 'Картой',
                'empty_data' => '0.00',
                'attr' => [
                    'min' => 0,
                    'data-tag' => 'card',
                ],
            ])
            ->add('sumCertificate', MoneyType::class, [
                'currency' => 'RUB',
                'scale' => 0,
                'label' => 'Сертификатом',
                'empty_data' => '0.00',
                'attr' => [
                    'data-tag' => 'cert',
                    'min' => 0,
                    'step' => self::MULTIPLE_OF_CERTIFICATE_SUM,
                    'max' => 50000,
                    'disabled' => $options['total'] < self::MIN_CERTIFICATE_SUM,
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderMixedPayment::class,
            'translation_domain' => false,
            // XXX: mb validation_groups nn? by default 'Default', check it and dd.
            'validation_groups' => function () {
                $validators = ['Default'];

                return $validators;
            },
        ]);

        $resolver->setRequired('total');
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'order_mixed_payment';
    }
}
