<?php

namespace Irmag\SiteBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CatalogFilterSelectType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildView(FormView $view, FormInterface $form, array $options)
    {
        $view->vars['mapped_to'] = $options['mapped_to'];
        $view->vars['mapped_to_lvl_2'] = $options['mapped_to_lvl_2'];
        $view->vars['selected'] = $options['selected'];
        $view->vars['selected_lvl_2'] = $options['selected_lvl_2'];
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'filter_select';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'mapped_to' => '',
            'mapped_to_lvl_2' => '',
            'selected' => '',
            'selected_lvl_2' => '',
        ]);
    }
}
