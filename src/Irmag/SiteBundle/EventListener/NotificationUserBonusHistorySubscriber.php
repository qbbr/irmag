<?php

namespace Irmag\SiteBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Irmag\ProfileBundle\Service\UserNotificationEventManager\UserNotificationEventManager;
use Irmag\ProfileBundle\Service\UserNotificationQueueManager\UserNotificationQueueManager;
use Irmag\ProfileBundle\NotificationEvents;
use Irmag\SiteBundle\Event\UserBonusHistoryEvent;
use Irmag\SiteBundle\IrmagSiteEvents;

class NotificationUserBonusHistorySubscriber implements EventSubscriberInterface
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var UserNotificationEventManager
     */
    private $userNotificationEventManager;

    /**
     * @var UserNotificationQueueManager
     */
    private $userNotificationQueueManager;

    /**
     * @param UrlGeneratorInterface        $router
     * @param UserNotificationEventManager $userNotificationEventManager
     * @param UserNotificationQueueManager $userNotificationQueueManager
     */
    public function __construct(
        UrlGeneratorInterface $router,
        UserNotificationEventManager $userNotificationEventManager,
        UserNotificationQueueManager $userNotificationQueueManager
    ) {
        $this->router = $router;
        $this->userNotificationEventManager = $userNotificationEventManager;
        $this->userNotificationQueueManager = $userNotificationQueueManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::BONUS_SEND => 'onBonusSend',
        ];
    }

    /**
     * @param UserBonusHistoryEvent $event
     */
    public function onBonusSend(UserBonusHistoryEvent $event): void
    {
        $userBonusHistory = $event->getUserBonusHistory();

        // only positive
        if ($userBonusHistory->getValue() > 0) {
            $event = $this->userNotificationEventManager->getEventByName(NotificationEvents::BONUS_SEND);
            $url = $this->router->generate('irmag_profile_bonus', ['_fragment' => 'history'], UrlGeneratorInterface::ABSOLUTE_URL);
            $this->userNotificationQueueManager->create($event, $userBonusHistory->getUser(), sprintf('(%d ф.) %s', $userBonusHistory->getValue(), $userBonusHistory->getMessage()), $url);
        }
    }
}
