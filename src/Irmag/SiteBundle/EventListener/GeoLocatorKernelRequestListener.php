<?php

namespace Irmag\SiteBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Irmag\SiteBundle\Service\GeoLocator\GeoLocator;

class GeoLocatorKernelRequestListener
{
    /**
     * @var GeoLocator
     */
    private $geoLocator;

    /**
     * @var string
     */
    private $siteDomain;

    /**
     * @param GeoLocator $geoLocator
     * @param string     $siteDomain
     */
    public function __construct(
        GeoLocator $geoLocator,
        string $siteDomain
    ) {
        $this->geoLocator = $geoLocator;
        $this->siteDomain = $siteDomain;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();

        // запоминаем геопозицию и отфильтровываем ботов
        if ($request->getHttpHost() === $this->siteDomain
            && !$request->getSession()->has(GeoLocator::SESSION_CURRENT_POSITION_LITERAL)
            && 0 === preg_match('/bot|spider/i', $request->headers->get('User-Agent'))
        ) {
            $this->geoLocator->getData();
        }
    }
}
