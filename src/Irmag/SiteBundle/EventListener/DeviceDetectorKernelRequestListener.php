<?php

namespace Irmag\SiteBundle\EventListener;

use Irmag\SiteBundle\Utils\DeviceDetector;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class DeviceDetectorKernelRequestListener
{
    const SESSION_DEVICE_NAME = 'device';

    /**
     * @var array
     */
    private $excludeDomains;

    /**
     * @param array $excludeDomains
     */
    public function __construct(
        array $excludeDomains
    ) {
        $this->excludeDomains = $excludeDomains;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();

        if (\in_array($request->getHttpHost(), $this->excludeDomains, true)) {
            return;
        }

        $session = $request->getSession();

        if ($session->has(self::SESSION_DEVICE_NAME)) {
            return;
        }

        $session->set(self::SESSION_DEVICE_NAME, DeviceDetector::getDevice());
    }
}
