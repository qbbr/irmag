<?php

namespace Irmag\SiteBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class KernelListener
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var AuthorizationCheckerInterface
     */
    private $authorizationChecker;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var string
     */
    private $siteDomain;

    /**
     * @param TokenStorageInterface         $tokenStorage
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param UrlGeneratorInterface         $router
     * @param string                        $siteDomain
     */
    public function __construct(
        TokenStorageInterface $tokenStorage,
        AuthorizationCheckerInterface $authorizationChecker,
        UrlGeneratorInterface $router,
        string $siteDomain
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->authorizationChecker = $authorizationChecker;
        $this->router = $router;
        $this->siteDomain = $siteDomain;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();

        // нужно для кнопки "Вернуться на сайт"
        // при переходе между доменами
        if ($request->query->has('return_url')) {
            $returnUrl = $request->query->get('return_url');
        } elseif ($request->headers->has('referer')) {
            $returnUrl = $request->headers->get('referer');
        }

        if (isset($returnUrl)) {
            $session = $request->getSession();

            $lastHost = $session->get('last_host');
            $currentHost = $request->getHost();

            // нужно сохранить URI для возврата
            if ($lastHost !== $currentHost && null !== $lastHost) {
                // проверяем, наш ли это $returnUrl
                // sub.domain.ru
                $host = parse_url($returnUrl, PHP_URL_HOST);
                $hostNames = explode('.', $host);
                // domain.ru
                $bottomHostName = $hostNames[\count($hostNames) - 2].'.'.$hostNames[\count($hostNames) - 1];

                if ($bottomHostName === $this->siteDomain) {
                    $session->set('return_url', $returnUrl);
                }
            }

            $session->set('last_host', $request->getHost());
        }
    }
}
