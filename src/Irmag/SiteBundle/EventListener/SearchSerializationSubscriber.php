<?php

namespace Irmag\SiteBundle\EventListener;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Asset\Packages;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\ElementBrand;
use Irmag\SiteBundle\Entity\ElementCountry;
use Irmag\SiteBundle\Entity\ElementPicture;
use Irmag\SiteBundle\Entity\Section;
use Irmag\SiteBundle\Service\PriceManager\PriceManager;
use Irmag\SiteBundle\Service\PriceManager\Adapter\ActionAdapter;

class SearchSerializationSubscriber implements EventSubscriberInterface
{
    const IMAGINE_ELEMENT_FILTER_NAME = 'catalog_element_thumb';
    const IMAGINE_BRAND_FILTER_NAME = 'brand_and_manufacturer';

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var PriceManager
     */
    private $priceManager;

    /**
     * @var ActionAdapter
     */
    private $actionAdapter;

    /**
     * @var CacheManager
     */
    private $imagineCacheManager;

    /**
     * @var Packages
     */
    private $assetPackages;

    /**
     * @param UrlGeneratorInterface $router
     * @param PriceManager          $priceManager
     * @param ActionAdapter         $actionAdapter
     * @param CacheManager          $imagineCacheManager
     * @param Packages              $assetPackages
     */
    public function __construct(
        UrlGeneratorInterface $router,
        PriceManager $priceManager,
        ActionAdapter $actionAdapter,
        CacheManager $imagineCacheManager,
        Packages $assetPackages
    ) {
        $this->router = $router;
        $this->priceManager = $priceManager;
        $this->actionAdapter = $actionAdapter;
        $this->imagineCacheManager = $imagineCacheManager;
        $this->assetPackages = $assetPackages;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            ['event' => 'serializer.post_serialize', 'class' => Element::class, 'method' => 'onPostSerialize'],
            ['event' => 'serializer.post_serialize', 'class' => Section::class, 'method' => 'onPostSerialize'],
            ['event' => 'serializer.post_serialize', 'class' => ElementBrand::class, 'method' => 'onPostSerialize'],
            ['event' => 'serializer.post_serialize', 'class' => ElementCountry::class, 'method' => 'onPostSerialize'],
            ['event' => 'serializer.post_serialize', 'class' => Action::class, 'method' => 'onPostSerialize'],
        ];
    }

    /**
     * @param ObjectEvent $event
     *
     * @throws IrmagException When instanceof is unknown
     */
    public function onPostSerialize(ObjectEvent $event): void
    {
        $object = $event->getObject();

        // only for group "search"
        if (false === \in_array('search', $event->getContext()->attributes->get('groups')->get(), true)) {
            return;
        }

        /** @var JsonSerializationVisitor $visitor */
        switch ($visitor = $event->getVisitor()) {
            case $object instanceof Element:
                /* @var Element $object */
                $visitor->setData('url', $this->router->generate('irmag_catalog_element', ['id' => $object->getId()], UrlGeneratorInterface::ABSOLUTE_URL));
                $visitor->setData('image', $this->getElementPictureWebPath($object));
                $visitor->setData('toneImage', $this->getElementTonePictureWebPath($object));
                $visitor->setData('price', number_format($this->priceManager->getPrice($object), 2, '.', ''));
                $visitor->setData('is_action_price', $this->actionAdapter->hasPrice($object));
                break;

            case $object instanceof Section:
                /* @var Section $object */
                $visitor->setData('url', $this->router->generate('irmag_catalog_section', ['sectionId' => $object->getId()], UrlGeneratorInterface::ABSOLUTE_URL));

                if ($parent = $object->getParent()) {
                    $visitor->setData('parent', ['id' => $parent->getId(), 'name' => $parent->getName()]);
                }
                break;

            case $object instanceof ElementBrand:
                /* @var ElementBrand $object */
                $visitor->setData('url', $this->router->generate('irmag_catalog', ['filter' => ['brand' => [$object->getId()]]], UrlGeneratorInterface::ABSOLUTE_URL));
                $visitor->setData('image', $this->getBrandPictureWebPath($object));
                break;

            case $object instanceof ElementCountry:
                /* @var ElementCountry $object */
                $visitor->setData('url', $this->router->generate('irmag_catalog', ['filter' => ['country' => [$object->getId()]]], UrlGeneratorInterface::ABSOLUTE_URL));
                break;

            case $object instanceof Action:
                /* @var Action $object */
                $visitor->setData('url', $this->router->generate('irmag_action_detail', ['slug' => $object->getSlug()], UrlGeneratorInterface::ABSOLUTE_URL));
                break;

            default:
                throw new IrmagException(sprintf('Unknown instance of class "%s".', \get_class($object)));
                break;
        }
    }

    /**
     * @param Element $element
     *
     * @return string
     */
    private function getElementPictureWebPath(Element $element): string
    {
        /** @var ElementPicture $elementPicture */
        if ($elementPicture = $element->getPictures()->first()) {
            if ($picture = $elementPicture->getFile()) {
                $path = $picture->getWebPath();
            }
        }

        if (!isset($path)) {
            $path = $this->assetPackages->getUrl(Element::NOT_FOUND_IMAGE);
        }

        return $this->imagineCacheManager->getBrowserPath($path, self::IMAGINE_ELEMENT_FILTER_NAME);
    }

    /**
     * @param Element $element
     *
     * @return string|null
     */
    private function getElementTonePictureWebPath(Element $element): ?string
    {
        if ($tonePicture = $element->getTonePicture()) {
            return $this->imagineCacheManager->getBrowserPath($tonePicture->getWebPath(), self::IMAGINE_ELEMENT_FILTER_NAME);
        }

        return null;
    }

    /**
     * @param ElementBrand $elementBrand
     *
     * @return string|null
     */
    private function getBrandPictureWebPath(ElementBrand $elementBrand): ?string
    {
        if ($picture = $elementBrand->getPicture()) {
            return $this->imagineCacheManager->getBrowserPath($picture->getWebPath(), self::IMAGINE_BRAND_FILTER_NAME);
        }

        return null;
    }
}
