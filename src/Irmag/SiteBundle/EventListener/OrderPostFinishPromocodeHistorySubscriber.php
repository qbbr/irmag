<?php

namespace Irmag\SiteBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\SiteBundle\Event\OrderEvent;
use Irmag\SiteBundle\Service\PriceManager\Adapter\PromocodeAdapter;
use Irmag\SiteBundle\Entity\PromocodeHistory;

class OrderPostFinishPromocodeHistorySubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var PromocodeAdapter
     */
    private $promocodeAdapter;

    /**
     * @param EntityManagerInterface $em
     * @param PromocodeAdapter       $promocodeAdapter
     */
    public function __construct(
        EntityManagerInterface $em,
        PromocodeAdapter $promocodeAdapter
    ) {
        $this->em = $em;
        $this->promocodeAdapter = $promocodeAdapter;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagSiteEvents::ORDER_POST_FINISH => 'onOrderPostFinish',
        ];
    }

    /**
     * @param OrderEvent $event
     */
    public function onOrderPostFinish(OrderEvent $event): void
    {
        $order = $event->getOrder();

        // промокод
        if (!$promocode = $this->promocodeAdapter->getPromocode()) {
            return;
        }

        // записываем в историю использования промокода
        $promocodeHistory = new PromocodeHistory();
        $promocodeHistory->setCode($promocode->getCode());
        $promocodeHistory->setUser($order->getUser());
        $promocodeHistory->setDiscount($promocode->getDiscount());

        // если промокод был персональный, то деактивируем его
        if (true === $promocode->getIsPersonal()) {
            $promocode->setIsActive(false);
        }

        $this->em->persist($promocode);
        $this->em->persist($promocodeHistory);
        $this->em->flush();

        // удаляем сессию промокода
        $this->promocodeAdapter->removeSession();
    }
}
