<?php

namespace Irmag\SiteBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Irmag\SiteBundle\Config;

class ReferalLinkKernelRequestListener
{
    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();

        // реферальная ссылка
        if ($request->query->has(Config::REFERRAL_LINK_GET_PARAMETER_NAME)) {
            $userOwnerId = $request->query->getInt(Config::REFERRAL_LINK_GET_PARAMETER_NAME);

            if ($userOwnerId > 0) {
                $request->getSession()->set(Config::REFERRAL_LINK_SESSION_NAME, $userOwnerId);
            }
        }
    }
}
