<?php

namespace Irmag\SiteBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Entity\UserReferralOrderPayoutQueue;
use Irmag\SiteBundle\Service\ReferralManager\ReferralManager;

class OrderSubscriber implements EventSubscriber
{
    /**
     * @var ReferralManager
     */
    private $referralManager;

    /**
     * @param ReferralManager $referralManager
     */
    public function __construct(
        ReferralManager $referralManager
    ) {
        $this->referralManager = $referralManager;
    }

    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            'onFlush',
        ];
    }

    /**
     * @param OnFlushEventArgs $args
     */
    public function onFlush(OnFlushEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();

        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if (!$entity instanceof Order) {
                continue;
            }

            $changes = $uow->getEntityChangeSet($entity);

            if (isset($changes['status'])) {
                // $oldStatus = $changes['status'][0];
                /** @var \Irmag\SiteBundle\Entity\OrderStatus $newStatus */
                $newStatus = $changes['status'][1];

                // если статус заказа изменился на выполнен
                if ('F' === $newStatus->getShortname()) {
                    // если существует реферальный пользователь, то записываем в очередь для выплаты
                    if ($this->referralManager->isSupportedForPutToQueue($entity)) {
                        $userReferralOrderPayoutQueue = $em->getRepository(UserReferralOrderPayoutQueue::class)->findOneBy(['order' => $entity]);

                        // exists check
                        if (null === $userReferralOrderPayoutQueue) {
                            $userReferralOrderPayoutQueue = new UserReferralOrderPayoutQueue();
                            $userReferralOrderPayoutQueue->setOrder($entity);
                            $em->persist($userReferralOrderPayoutQueue);

                            $classMetadata = $em->getClassMetadata(UserReferralOrderPayoutQueue::class);
                            $uow->computeChangeSet($classMetadata, $userReferralOrderPayoutQueue);
                        }
                    }
                }
            }
        }
    }
}
