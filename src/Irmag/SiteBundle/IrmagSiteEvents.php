<?php

namespace Irmag\SiteBundle;

final class IrmagSiteEvents
{
    /**
     * Это событие вызывается после создания объекта Order.
     *
     * @var string
     */
    public const ORDER_POST_CREATED = 'irmag.order.post_created';

    /**
     * Это событие вызывается перед финишем (finishAction).
     *
     * @var string
     */
    public const ORDER_PRE_FINISH = 'irmag.order.pre_finish';

    /**
     * Это событие вызывается после финиша (finishAction).
     *
     * @var string
     */
    public const ORDER_POST_FINISH = 'irmag.order.post_finish';

    /**
     * Это событие вызывается перед отменой заказа (cancelAction).
     *
     * @var string
     */
    public const ORDER_PRE_CANCEL = 'irmag.order.pre_cancel';

    /**
     * Это событие вызывается после отмены заказа (cancelAction).
     *
     * @var string
     */
    public const ORDER_POST_CANCEL = 'irmag.order.post_cancel';

    /**
     * Это событие вызывается, когда выбрана форма оплаты "Онлайн".
     *
     * @var string
     */
    public const ORDER_SET_ONLINE_PAYMENT = 'irmag.order.set_online_payment';

    /**
     * Это событие вызыается, когда выбран вариант доставки транспортной компанией.
     *
     * @var string
     */
    public const DELIVERY_SELECT_TRANSPORT_COMPANY = 'irmag.delivery.set_transport_company';

    /**
     * Это событие вызыается, когда выбран вариант доставки Почтой России.
     *
     * @var string
     */
    public const DELIVERY_SELECT_POST = 'irmag.delivery.set_post';

    /**
     * Это событие вызывается, когда начислен бонус.
     *
     * @var string
     */
    public const BONUS_SEND = 'irmag.bonus.payed';
}
