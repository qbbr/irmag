<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\Traits\OrderMethodTrait;

/**
 * @ORM\Table(name="orders_payment_methods")
 * @ORM\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderPaymentMethod
{
    use OrderMethodTrait;

    /**
     * Методы с предоплатой.
     */
    public const PRE_PAYMENT_METHODS = [
        'online',
        'apple_pay',
        'android_pay',
    ];

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }
}
