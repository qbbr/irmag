<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Table(name="users_referral_links")
 * @ORM\Entity
 */
class UserReferralLink
{
    /**
     * @var User
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $userReferral;

    /**
     * Пользователь, которому нужно начислить бонус после выполнения заказа от пользователя $user.
     *
     * @var User
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $userOwner;

    public function getUserReferral(): ?User
    {
        return $this->userReferral;
    }

    public function setUserReferral(?User $userReferral): self
    {
        $this->userReferral = $userReferral;

        return $this;
    }

    public function getUserOwner(): ?User
    {
        return $this->userOwner;
    }

    public function setUserOwner(?User $userOwner): self
    {
        $this->userOwner = $userOwner;

        return $this;
    }
}
