<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserBonusHistory;

/**
 * @ORM\Table(name="users_referral_order_payouts")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\UserReferralOrderPayoutRepository")
 */
class UserReferralOrderPayout
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $userReferral;

    /**
     * Пользователь, которому нужно начислить бонус после выполнения заказа от пользователя $user.
     *
     * @ORM\ManyToOne(targetEntity="Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $userOwner;

    /**
     * @var Order
     *
     * @ORM\ManyToOne(targetEntity="Order")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $order;

    /**
     * @var UserBonusHistory
     *
     * @ORM\ManyToOne(targetEntity="Irmag\ProfileBundle\Entity\UserBonusHistory")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $userBonusHistory;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set referral user.
     *
     * @param User $userReferral
     *
     * @return UserReferralOrderPayout
     */
    public function setUserReferral(User $userReferral): self
    {
        $this->userReferral = $userReferral;

        return $this;
    }

    /**
     * Get referral user.
     *
     * @return User
     */
    public function getUserReferral(): User
    {
        return $this->userReferral;
    }

    /**
     * Set owner user.
     *
     * @param User $userOwner
     *
     * @return UserReferralOrderPayout
     */
    public function setUserOwner(User $userOwner): self
    {
        $this->userOwner = $userOwner;

        return $this;
    }

    /**
     * Get owner user.
     *
     * @return User
     */
    public function getUserOwner(): User
    {
        return $this->userOwner;
    }

    /**
     * Set order.
     *
     * @param Order $order
     *
     * @return UserReferralOrderPayout
     */
    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order.
     *
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * Set userBonusHistory.
     *
     * @param UserBonusHistory $userBonusHistory
     *
     * @return UserReferralOrderPayout
     */
    public function setUserBonusHistory(UserBonusHistory $userBonusHistory): self
    {
        $this->userBonusHistory = $userBonusHistory;

        return $this;
    }

    /**
     * Get userBonusHistory.
     *
     * @return UserBonusHistory
     */
    public function getUserBonusHistory(): UserBonusHistory
    {
        return $this->userBonusHistory;
    }
}
