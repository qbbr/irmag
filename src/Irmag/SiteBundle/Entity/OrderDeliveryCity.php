<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\Traits\OrderMethodTrait;

/**
 * @ORM\Table(name="orders_delivery_cities")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\OrderDeliveryCityRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderDeliveryCity
{
    use OrderMethodTrait;

    /**
     * @var Collection|OrderDeliveryMethod[]
     *
     * @ORM\ManyToMany(targetEntity="OrderDeliveryMethod")
     * @ORM\JoinTable(name="orders_delivery_cities_orders_delivery_methods",
     *     joinColumns={@ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="id")}
     * )
     */
    private $deliveryMethod;

    /**
     * @var Collection|OrderDeliveryCitySelfserviceMethod[]
     *
     * @ORM\OneToMany(targetEntity="OrderDeliveryCitySelfserviceMethod", mappedBy="deliveryCity", cascade={"persist"}, orphanRemoval=true)
     */
    private $selfserviceMethod;

    /**
     * @var Collection|OrderDeliveryCityDistrict[]
     *
     * @ORM\OneToMany(targetEntity="OrderDeliveryCityDistrict", mappedBy="deliveryCity", cascade={"persist"}, orphanRemoval=true)
     */
    private $districts;

    /**
     * Стоимость доставки.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $deliveryPrice;

    /**
     * Доп.цена для срочного интервала доставки.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $extraPriceForFastDeliveryTime;

    /**
     * Минимальная стоимость для бесплатной доставки.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $minPriceForFreeDelivery;

    /**
     * @var Collection|OrderPaymentMethod[]
     *
     * @ORM\ManyToMany(targetEntity="OrderPaymentMethod")
     * @ORM\JoinTable(name="orders_delivery_cities_orders_payment_methods",
     *     joinColumns={@ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="id")}
     * )
     */
    private $paymentMethod;

    /**
     * @var Collection|OrderDeliveryCityBanDay[]
     *
     * @ORM\OneToMany(targetEntity="OrderDeliveryCityBanDay", mappedBy="deliveryCity", cascade={"persist"}, orphanRemoval=true)
     */
    private $banDates;

    /**
     * @var Collection|OrderDeliveryCityDayOfWeek[]
     *
     * @ORM\OneToMany(targetEntity="OrderDeliveryCityDayOfWeek", mappedBy="deliveryCity", cascade={"persist"}, orphanRemoval=true)
     * @ORM\OrderBy({"dayOfWeek": "ASC"})
     */
    private $deliveryCityDayOfWeek;

    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     *
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $position;

    public function __construct()
    {
        $this->deliveryMethod = new ArrayCollection();
        $this->selfserviceMethod = new ArrayCollection();
        $this->paymentMethod = new ArrayCollection();
        $this->banDates = new ArrayCollection();
        $this->deliveryCityDayOfWeek = new ArrayCollection();
        $this->position = 0;
        $this->districts = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return sprintf('%s (%s)', $this->name, $this->shortname);
    }

    public function getDeliveryPrice(): ?int
    {
        return $this->deliveryPrice;
    }

    public function setDeliveryPrice(?int $deliveryPrice): self
    {
        $this->deliveryPrice = $deliveryPrice;

        return $this;
    }

    public function getExtraPriceForFastDeliveryTime(): ?int
    {
        return $this->extraPriceForFastDeliveryTime;
    }

    public function setExtraPriceForFastDeliveryTime(?int $extraPriceForFastDeliveryTime): self
    {
        $this->extraPriceForFastDeliveryTime = $extraPriceForFastDeliveryTime;

        return $this;
    }

    public function getMinPriceForFreeDelivery(): ?int
    {
        return $this->minPriceForFreeDelivery;
    }

    public function setMinPriceForFreeDelivery(?int $minPriceForFreeDelivery): self
    {
        $this->minPriceForFreeDelivery = $minPriceForFreeDelivery;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return Collection|OrderDeliveryMethod[]
     */
    public function getDeliveryMethod(): Collection
    {
        return $this->deliveryMethod;
    }

    public function addDeliveryMethod(OrderDeliveryMethod $deliveryMethod): self
    {
        if (!$this->deliveryMethod->contains($deliveryMethod)) {
            $this->deliveryMethod[] = $deliveryMethod;
        }

        return $this;
    }

    public function removeDeliveryMethod(OrderDeliveryMethod $deliveryMethod): self
    {
        if ($this->deliveryMethod->contains($deliveryMethod)) {
            $this->deliveryMethod->removeElement($deliveryMethod);
        }

        return $this;
    }

    /**
     * @return Collection|OrderDeliveryCitySelfserviceMethod[]
     */
    public function getSelfserviceMethod(): Collection
    {
        return $this->selfserviceMethod;
    }

    public function addSelfserviceMethod(OrderDeliveryCitySelfserviceMethod $selfserviceMethod): self
    {
        if (!$this->selfserviceMethod->contains($selfserviceMethod)) {
            $this->selfserviceMethod[] = $selfserviceMethod;
            $selfserviceMethod->setDeliveryCity($this);
        }

        return $this;
    }

    public function removeSelfserviceMethod(OrderDeliveryCitySelfserviceMethod $selfserviceMethod): self
    {
        if ($this->selfserviceMethod->contains($selfserviceMethod)) {
            $this->selfserviceMethod->removeElement($selfserviceMethod);
            // set the owning side to null (unless already changed)
            if ($selfserviceMethod->getDeliveryCity() === $this) {
                $selfserviceMethod->setDeliveryCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderPaymentMethod[]
     */
    public function getPaymentMethod(): Collection
    {
        return $this->paymentMethod;
    }

    public function addPaymentMethod(OrderPaymentMethod $paymentMethod): self
    {
        if (!$this->paymentMethod->contains($paymentMethod)) {
            $this->paymentMethod[] = $paymentMethod;
        }

        return $this;
    }

    public function removePaymentMethod(OrderPaymentMethod $paymentMethod): self
    {
        if ($this->paymentMethod->contains($paymentMethod)) {
            $this->paymentMethod->removeElement($paymentMethod);
        }

        return $this;
    }

    /**
     * @return Collection|OrderDeliveryCityBanDay[]
     */
    public function getBanDates(): Collection
    {
        return $this->banDates;
    }

    public function addBanDate(OrderDeliveryCityBanDay $banDate): self
    {
        if (!$this->banDates->contains($banDate)) {
            $this->banDates[] = $banDate;
            $banDate->setDeliveryCity($this);
        }

        return $this;
    }

    public function removeBanDate(OrderDeliveryCityBanDay $banDate): self
    {
        if ($this->banDates->contains($banDate)) {
            $this->banDates->removeElement($banDate);
            // set the owning side to null (unless already changed)
            if ($banDate->getDeliveryCity() === $this) {
                $banDate->setDeliveryCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderDeliveryCityDayOfWeek[]
     */
    public function getDeliveryCityDayOfWeek(): Collection
    {
        return $this->deliveryCityDayOfWeek;
    }

    public function addDeliveryCityDayOfWeek(OrderDeliveryCityDayOfWeek $deliveryCityDayOfWeek): self
    {
        if (!$this->deliveryCityDayOfWeek->contains($deliveryCityDayOfWeek)) {
            $this->deliveryCityDayOfWeek[] = $deliveryCityDayOfWeek;
            $deliveryCityDayOfWeek->setDeliveryCity($this);
        }

        return $this;
    }

    public function removeDeliveryCityDayOfWeek(OrderDeliveryCityDayOfWeek $deliveryCityDayOfWeek): self
    {
        if ($this->deliveryCityDayOfWeek->contains($deliveryCityDayOfWeek)) {
            $this->deliveryCityDayOfWeek->removeElement($deliveryCityDayOfWeek);
            // set the owning side to null (unless already changed)
            if ($deliveryCityDayOfWeek->getDeliveryCity() === $this) {
                $deliveryCityDayOfWeek->setDeliveryCity(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|OrderDeliveryCityDistrict[]
     */
    public function getDistricts(): Collection
    {
        return $this->districts;
    }

    public function addDistrict(OrderDeliveryCityDistrict $district): self
    {
        if (!$this->districts->contains($district)) {
            $this->districts[] = $district;
            $district->setDeliveryCity($this);
        }

        return $this;
    }

    public function removeDistrict(OrderDeliveryCityDistrict $district): self
    {
        if ($this->districts->contains($district)) {
            $this->districts->removeElement($district);
            // set the owning side to null (unless already changed)
            if ($district->getDeliveryCity() === $this) {
                $district->setDeliveryCity(null);
            }
        }

        return $this;
    }
}
