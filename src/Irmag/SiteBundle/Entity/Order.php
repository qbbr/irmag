<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceData;
use Irmag\OrderPostDeliveryBundle\Entity\OrderPostDeliveryData;
use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Entity\Traits\PayerLegalTrait;
use Irmag\SiteBundle\Entity\Traits\DeliveryAddressTrait;
use Irmag\DriveBundle\Entity\Waybill;

/**
 * @ORM\Table(name="orders")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\OrderRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Order
{
    use PayerLegalTrait;
    use DeliveryAddressTrait;

    const MIN_PRICE_FOR_PAYER_LEGAL = 3000;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="SEQUENCE")
     * @ORM\SequenceGenerator(sequenceName="orders_id_seq", initialValue=300000)
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_id", "api_order_selfservices"})
     */
    private $id;

    /**
     * Пользователь.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_selfservices"})
     */
    private $user;

    /**
     * Список товара.
     *
     * @var Collection|OrderElement[]
     *
     * @ORM\OneToMany(targetEntity="OrderElement", mappedBy="order", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"name": "ASC"})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $orderElement;

    /**
     * Это юридическое лицо.
     * (false - individual,true - legal).
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $isPayerLegal;

    /**
     * ФИО плательщика.
     *
     * @var string
     *
     * @Assert\Length(max=255)
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_selfservices"})
     */
    private $payerUsername;

    /**
     * Телефон плательщика.
     *
     * @var string
     *
     * @Assert\Length(max=32)
     * @Assert\Regex(
     *     pattern="/^[0-9\+\-\(\)\s]+$/",
     *     htmlPattern="^[0-9\+\-\(\)\s]+$",
     *     message="Допустимые символы: 'числа', 'пробелы', '-', '+', '(', ')'. Дополнительные телефоны можете указать в комментарии."
     * )
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=32)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_selfservices"})
     */
    private $payerTelephone;

    /**
     * Email плательщика.
     *
     * @var string
     *
     * @Assert\Length(max=60)
     * @Assert\Email
     *
     * @ORM\Column(type="string", length=60, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerEmail;

    /**
     * Способ доставки.
     *
     * @var OrderDeliveryMethod
     *
     * @Assert\NotBlank(message="Не выбран способ доставки.")
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryMethod")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryMethod;

    /**
     * Способ самовывоза.
     *
     * @var OrderDeliverySelfserviceMethod
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliverySelfserviceMethod")
     * @ORM\JoinColumn(onDelete="SET NULL")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $selfserviceMethod;

    /**
     * Дата доставки.
     *
     * @var \DateTimeInterface
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="date", nullable=false)
     *
     * @Serializer\Type("DateTime<'d.m.Y'>")
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_selfservices"})
     */
    private $deliveryDate;

    /**
     * Время доставки.
     *
     * @var OrderDeliveryTime
     *
     * @Assert\NotBlank
     * @Assert\NotNull
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryTime")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryTime;

    /**
     * Стоимость доставки.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryPrice;

    /**
     * Способ оплаты.
     *
     * @var OrderPaymentMethod
     *
     * @Assert\NotBlank(message="Не выбран способ оплаты.")
     *
     * @ORM\ManyToOne(targetEntity="OrderPaymentMethod")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $paymentMethod;

    /**
     * Сумма заказа.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $totalPrice;

    /**
     * Сумма заказа без скидки.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $totalPriceWithoutDiscount;

    /**
     * Потрачено фантиков.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $spendBonus;

    /**
     * Скидка по промокоду.
     *
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $promocodeDiscount;

    /**
     * Промокод.
     *
     * @var string
     *
     * @Assert\Length(max=32)
     * @ORM\Column(type="string", length=32, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $promocode;

    /**
     * Статус заказа.
     *
     * @var OrderStatus
     *
     * @Assert\NotBlank
     *
     * @ORM\ManyToOne(targetEntity="OrderStatus")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_selfservices"})
     */
    private $status;

    /**
     * @var OrderDeliveryServiceData
     *
     * @ORM\OneToOne(targetEntity="\Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceData", mappedBy="order", cascade={"persist", "remove"})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryServiceData;

    /**
     * @var OrderPostDeliveryData
     *
     * @ORM\OneToOne(targetEntity="\Irmag\OrderPostDeliveryBundle\Entity\OrderPostDeliveryData", mappedBy="order", cascade={"persist", "remove"})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $postDeliveryData;

    /**
     * @var Collection|OrderOnlinePayment[]
     *
     * @ORM\OneToMany(targetEntity="\Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment", mappedBy="order")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $onlinePayments;

    /**
     * Смешанный платёж.
     *
     * @var OrderMixedPayment
     *
     * @ORM\OneToOne(targetEntity="OrderMixedPayment", mappedBy="order", cascade={"persist", "remove"})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $mixedPayment;

    /**
     * Устройство, с которого сделали заказ (mobile/tablet/desktop).
     *
     * @var string
     *
     * @ORM\Column(type="string", length=10, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $device;

    /**
     * Принял ли пользователь соглашение об обработке персональный данных (всегда должно быть true).
     *
     * @var bool
     *
     * @Assert\IsTrue(message="Вы должны принять соглашение об обработке персональных данных")
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isAgreementAccepted;

    /**
     * Способ доставки чека о заказе пользователю.
     *
     * @var string
     *
     * @Assert\Choice({"email", "sms"})
     *
     * @ORM\Column(type="string", length=5, nullable=true)
     *
     * @Serializer\SerializedName("checkDelivery")
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $checkDeliveryMethod;

    /**
     * @var Waybill|null
     *
     * @ORM\ManyToOne(targetEntity="Irmag\DriveBundle\Entity\Waybill", inversedBy="orders")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL", nullable=true)
     */
    private $waybill;

    /**
     * @var \DateTimeInterface
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_selfservices"})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order", "api_order_selfservices"})
     */
    private $updatedAt;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isRemoteDeliveryCompleted;

    /**
     * Родитель (для дозаказов).
     *
     * @var Order|null
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $parent;

    /**
     * @var Collection|Order[]
     *
     * @ORM\OneToMany(targetEntity="Order", mappedBy="parent")
     */
    private $children;

    public function __construct()
    {
        $this->isPayerLegal = false;
        $this->isAgreementAccepted = true;
        $this->orderElement = new ArrayCollection();
        $this->onlinePayments = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    /**
     * У физ.лица обнуляет юр.данные.
     * XXX: Custom.
     *
     * @ORM\PostPersist
     */
    public function nullableLegalDataIfPayerIsNotLegal(): void
    {
        if (!$this->getIsPayerLegal()) {
            $this->nullableLegalData();
        }
    }

    /**
     * Заполняет цены нулями.
     * XXX: Custom для API.
     *
     * @ORM\PreFlush
     */
    public function preUpdate(): void
    {
        if (null === $this->totalPrice) {
            $this->totalPrice = 0.00;
        }

        if (null === $this->totalPriceWithoutDiscount) {
            $this->totalPriceWithoutDiscount = 0.00;
        }
    }

    /**
     * XXX: Custom для API.
     *
     * @Serializer\MaxDepth(1)
     * @Serializer\VirtualProperty
     * @Serializer\SerializedName("parent")
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    public function getParentId(): ?int
    {
        return $this->parent ? $this->parent->getId() : null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getIsPayerLegal(): ?bool
    {
        return $this->isPayerLegal;
    }

    public function setIsPayerLegal(bool $isPayerLegal): self
    {
        $this->isPayerLegal = $isPayerLegal;

        return $this;
    }

    public function getPayerUsername(): ?string
    {
        return $this->payerUsername;
    }

    public function setPayerUsername(?string $payerUsername): self
    {
        $this->payerUsername = $payerUsername;

        return $this;
    }

    public function getPayerTelephone(): ?string
    {
        return $this->payerTelephone;
    }

    public function setPayerTelephone(?string $payerTelephone): self
    {
        $this->payerTelephone = $payerTelephone;

        return $this;
    }

    public function getPayerEmail(): ?string
    {
        return $this->payerEmail;
    }

    public function setPayerEmail(?string $payerEmail): self
    {
        $this->payerEmail = $payerEmail;

        return $this;
    }

    public function getDeliveryDate(): ?\DateTimeInterface
    {
        return $this->deliveryDate;
    }

    public function setDeliveryDate(?\DateTimeInterface $deliveryDate): self
    {
        $this->deliveryDate = $deliveryDate;

        return $this;
    }

    public function getDeliveryPrice(): ?int
    {
        return $this->deliveryPrice;
    }

    public function setDeliveryPrice(?int $deliveryPrice): self
    {
        $this->deliveryPrice = $deliveryPrice;

        return $this;
    }

    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    public function setTotalPrice($totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }

    public function getTotalPriceWithoutDiscount()
    {
        return $this->totalPriceWithoutDiscount;
    }

    public function setTotalPriceWithoutDiscount($totalPriceWithoutDiscount): self
    {
        $this->totalPriceWithoutDiscount = $totalPriceWithoutDiscount;

        return $this;
    }

    public function getSpendBonus(): ?int
    {
        return $this->spendBonus;
    }

    public function setSpendBonus(?int $spendBonus): self
    {
        $this->spendBonus = $spendBonus;

        return $this;
    }

    public function getPromocodeDiscount(): ?int
    {
        return $this->promocodeDiscount;
    }

    public function setPromocodeDiscount(?int $promocodeDiscount): self
    {
        $this->promocodeDiscount = $promocodeDiscount;

        return $this;
    }

    public function getPromocode(): ?string
    {
        return $this->promocode;
    }

    public function setPromocode(?string $promocode): self
    {
        $this->promocode = $promocode;

        return $this;
    }

    public function getDevice(): ?string
    {
        return $this->device;
    }

    public function setDevice(?string $device): self
    {
        $this->device = $device;

        return $this;
    }

    public function getIsAgreementAccepted(): ?bool
    {
        return $this->isAgreementAccepted;
    }

    public function setIsAgreementAccepted(bool $isAgreementAccepted): self
    {
        $this->isAgreementAccepted = $isAgreementAccepted;

        return $this;
    }

    public function getCheckDeliveryMethod(): ?string
    {
        return $this->checkDeliveryMethod;
    }

    public function setCheckDeliveryMethod(?string $checkDeliveryMethod): self
    {
        $this->checkDeliveryMethod = $checkDeliveryMethod;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getIsRemoteDeliveryCompleted(): ?bool
    {
        return $this->isRemoteDeliveryCompleted;
    }

    public function setIsRemoteDeliveryCompleted(?bool $isRemoteDeliveryCompleted): self
    {
        $this->isRemoteDeliveryCompleted = $isRemoteDeliveryCompleted;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|OrderElement[]
     */
    public function getOrderElement(): Collection
    {
        return $this->orderElement;
    }

    public function addOrderElement(OrderElement $orderElement): self
    {
        if (!$this->orderElement->contains($orderElement)) {
            $this->orderElement[] = $orderElement;
            $orderElement->setOrder($this);
        }

        return $this;
    }

    public function removeOrderElement(OrderElement $orderElement): self
    {
        if ($this->orderElement->contains($orderElement)) {
            $this->orderElement->removeElement($orderElement);
            // set the owning side to null (unless already changed)
            if ($orderElement->getOrder() === $this) {
                $orderElement->setOrder(null);
            }
        }

        return $this;
    }

    public function getDeliveryMethod(): ?OrderDeliveryMethod
    {
        return $this->deliveryMethod;
    }

    public function setDeliveryMethod(?OrderDeliveryMethod $deliveryMethod): self
    {
        $this->deliveryMethod = $deliveryMethod;

        return $this;
    }

    public function getSelfserviceMethod(): ?OrderDeliverySelfserviceMethod
    {
        return $this->selfserviceMethod;
    }

    public function setSelfserviceMethod(?OrderDeliverySelfserviceMethod $selfserviceMethod): self
    {
        $this->selfserviceMethod = $selfserviceMethod;

        return $this;
    }

    public function getDeliveryTime(): ?OrderDeliveryTime
    {
        return $this->deliveryTime;
    }

    public function setDeliveryTime(?OrderDeliveryTime $deliveryTime): self
    {
        $this->deliveryTime = $deliveryTime;

        return $this;
    }

    public function getPaymentMethod(): ?OrderPaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?OrderPaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getStatus(): ?OrderStatus
    {
        return $this->status;
    }

    public function setStatus(?OrderStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDeliveryServiceData(): ?OrderDeliveryServiceData
    {
        return $this->deliveryServiceData;
    }

    public function setDeliveryServiceData(?OrderDeliveryServiceData $deliveryServiceData): self
    {
        $this->deliveryServiceData = $deliveryServiceData;

        // set (or unset) the owning side of the relation if necessary
        $newOrder = null === $deliveryServiceData ? null : $this;
        if ($newOrder !== $deliveryServiceData->getOrder()) {
            $deliveryServiceData->setOrder($newOrder);
        }

        return $this;
    }

    public function getPostDeliveryData(): ?OrderPostDeliveryData
    {
        return $this->postDeliveryData;
    }

    public function setPostDeliveryData(?OrderPostDeliveryData $postDeliveryData): self
    {
        $this->postDeliveryData = $postDeliveryData;

        // set (or unset) the owning side of the relation if necessary
        $newOrder = null === $postDeliveryData ? null : $this;
        if ($newOrder !== $postDeliveryData->getOrder()) {
            $postDeliveryData->setOrder($newOrder);
        }

        return $this;
    }

    /**
     * @return Collection|OrderOnlinePayment[]
     */
    public function getOnlinePayments(): Collection
    {
        return $this->onlinePayments;
    }

    public function addOnlinePayment(OrderOnlinePayment $onlinePayment): self
    {
        if (!$this->onlinePayments->contains($onlinePayment)) {
            $this->onlinePayments[] = $onlinePayment;
            $onlinePayment->setOrder($this);
        }

        return $this;
    }

    public function removeOnlinePayment(OrderOnlinePayment $onlinePayment): self
    {
        if ($this->onlinePayments->contains($onlinePayment)) {
            $this->onlinePayments->removeElement($onlinePayment);
            // set the owning side to null (unless already changed)
            if ($onlinePayment->getOrder() === $this) {
                $onlinePayment->setOrder(null);
            }
        }

        return $this;
    }

    public function getMixedPayment(): ?OrderMixedPayment
    {
        return $this->mixedPayment;
    }

    public function setMixedPayment(?OrderMixedPayment $mixedPayment): self
    {
        $this->mixedPayment = $mixedPayment;

        // set (or unset) the owning side of the relation if necessary
        $newOrder = null === $mixedPayment ? null : $this;
        if ($newOrder !== $mixedPayment->getOrder()) {
            $mixedPayment->setOrder($newOrder);
        }

        return $this;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?self $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    public function addChild(self $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children[] = $child;
            $child->setParent($this);
        }

        return $this;
    }

    public function removeChild(self $child): self
    {
        if ($this->children->contains($child)) {
            $this->children->removeElement($child);
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $child->setParent(null);
            }
        }

        return $this;
    }

    public function getWaybill(): ?Waybill
    {
        return $this->waybill;
    }

    public function setWaybill(?Waybill $waybill): self
    {
        $this->waybill = $waybill;

        return $this;
    }
}
