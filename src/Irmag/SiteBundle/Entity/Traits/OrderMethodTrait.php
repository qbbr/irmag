<?php

namespace Irmag\SiteBundle\Entity\Traits;

use Symfony\Component\Validator\Constraints as Assert;

trait OrderMethodTrait
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $id;

    /**
     * Название.
     *
     * @var string
     *
     * @Assert\Length(max=255)
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $name;

    /**
     * Короткое название.
     *
     * @var string
     *
     * @Assert\Length(max=255)
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=32, unique=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $shortname;

    /**
     * Описание.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * Активность.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive = true;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): self
    {
        $this->shortname = $shortname;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
