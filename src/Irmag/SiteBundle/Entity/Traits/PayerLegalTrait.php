<?php

namespace Irmag\SiteBundle\Entity\Traits;

trait PayerLegalTrait
{
    /**
     * Юридический адрес.
     *
     * @var string
     *
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalAddress;

    /**
     * Фактический адрес.
     *
     * @var string
     *
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalActualAddress;

    /**
     * Грузополучатель.
     *
     * @var string
     *
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalConsigneeAddress;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalName;

    /**
     * @var string
     *
     * @Assert\Type(type="digit", message="type.numeric")
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=20)
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalInn;

    /**
     * @var string
     *
     * @Assert\Type(type="digit", message="type.numeric")
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=20)
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalKpp;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=30)
     *
     * @ORM\Column(type="string", length=30, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalOgrn;

    /**
     * @var string
     *
     * @Assert\Type(type="digit", message="type.numeric")
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=20)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalRs;

    /**
     * @var string
     *
     * @Assert\Type(type="digit", message="type.numeric")
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=20)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalKrs;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalRb;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalRbCity;

    /**
     * @var string
     *
     * @Assert\Type(type="digit", message="type.numeric")
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=20)
     *
     * @ORM\Column(type="string", length=20, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalBik;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=255)
     * @Assert\Regex(
     *     pattern="/^[0-9\+\-\(\)\s]+$/",
     *     htmlPattern="^[0-9\+\-\(\)\s]+$",
     *     message="Допустимые символы: 'числа', 'пробелы', '-', '+', '(', ')'."
     * )
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalTelephone;

    /**
     * @var string
     *
     * @Assert\Email(checkHost=true)
     * @Assert\NotBlank(groups={"payer_legal"})
     * @Assert\Length(max=255)
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $payerLegalEmail;

    /**
     * XXX: Custom.
     */
    public function nullableLegalData(): void
    {
        $this->payerLegalAddress = null;
        $this->payerLegalActualAddress = null;
        $this->payerLegalConsigneeAddress = null;
        $this->payerLegalName = null;
        $this->payerLegalInn = null;
        $this->payerLegalKpp = null;
        $this->payerLegalOgrn = null;
        $this->payerLegalRs = null;
        $this->payerLegalKrs = null;
        $this->payerLegalRb = null;
        $this->payerLegalRbCity = null;
        $this->payerLegalBik = null;
        $this->payerLegalTelephone = null;
        $this->payerLegalEmail = null;
    }

    /**
     * Set payerLegalAddress.
     *
     * @param string $payerLegalAddress
     *
     * @return $this
     */
    public function setPayerLegalAddress(?string $payerLegalAddress): self
    {
        $this->payerLegalAddress = $payerLegalAddress;

        return $this;
    }

    /**
     * Get payerLegalAddress.
     *
     * @return string
     */
    public function getPayerLegalAddress(): ?string
    {
        return $this->payerLegalAddress;
    }

    /**
     * Set payerLegalActualAddress.
     *
     * @param string $payerLegalActualAddress
     *
     * @return $this
     */
    public function setPayerLegalActualAddress(?string $payerLegalActualAddress): self
    {
        $this->payerLegalActualAddress = $payerLegalActualAddress;

        return $this;
    }

    /**
     * Get payerLegalActualAddress.
     *
     * @return string
     */
    public function getPayerLegalActualAddress(): ?string
    {
        return $this->payerLegalActualAddress;
    }

    /**
     * Set payerLegalConsigneeAddress.
     *
     * @param string $payerLegalConsigneeAddress
     *
     * @return $this
     */
    public function setPayerLegalConsigneeAddress(?string $payerLegalConsigneeAddress): self
    {
        $this->payerLegalConsigneeAddress = $payerLegalConsigneeAddress;

        return $this;
    }

    /**
     * Get payerLegalConsigneeAddress.
     *
     * @return string
     */
    public function getPayerLegalConsigneeAddress(): ?string
    {
        return $this->payerLegalConsigneeAddress;
    }

    /**
     * Set payerLegalName.
     *
     * @param string $payerLegalName
     *
     * @return $this
     */
    public function setPayerLegalName(?string $payerLegalName): self
    {
        $this->payerLegalName = $payerLegalName;

        return $this;
    }

    /**
     * Get payerLegalName.
     *
     * @return string
     */
    public function getPayerLegalName(): ?string
    {
        return $this->payerLegalName;
    }

    /**
     * Set payerLegalInn.
     *
     * @param string $payerLegalInn
     *
     * @return $this
     */
    public function setPayerLegalInn(?string $payerLegalInn): self
    {
        $this->payerLegalInn = $payerLegalInn;

        return $this;
    }

    /**
     * Get payerLegalInn.
     *
     * @return string
     */
    public function getPayerLegalInn(): ?string
    {
        return $this->payerLegalInn;
    }

    /**
     * Set payerLegalKpp.
     *
     * @param string $payerLegalKpp
     *
     * @return $this
     */
    public function setPayerLegalKpp(?string $payerLegalKpp): self
    {
        $this->payerLegalKpp = $payerLegalKpp;

        return $this;
    }

    /**
     * Get payerLegalKpp.
     *
     * @return string
     */
    public function getPayerLegalKpp(): ?string
    {
        return $this->payerLegalKpp;
    }

    /**
     * Set payerLegalOgrn.
     *
     * @param string $payerLegalOgrn
     *
     * @return $this
     */
    public function setPayerLegalOgrn(?string $payerLegalOgrn): self
    {
        $this->payerLegalOgrn = $payerLegalOgrn;

        return $this;
    }

    /**
     * Get payerLegalOgrn.
     *
     * @return string
     */
    public function getPayerLegalOgrn(): ?string
    {
        return $this->payerLegalOgrn;
    }

    /**
     * Set payerLegalRs.
     *
     * @param string $payerLegalRs
     *
     * @return $this
     */
    public function setPayerLegalRs(?string $payerLegalRs): self
    {
        $this->payerLegalRs = $payerLegalRs;

        return $this;
    }

    /**
     * Get payerLegalRs.
     *
     * @return string
     */
    public function getPayerLegalRs(): ?string
    {
        return $this->payerLegalRs;
    }

    /**
     * Set payerLegalKrs.
     *
     * @param string $payerLegalKrs
     *
     * @return $this
     */
    public function setPayerLegalKrs(?string $payerLegalKrs): self
    {
        $this->payerLegalKrs = $payerLegalKrs;

        return $this;
    }

    /**
     * Get payerLegalKrs.
     *
     * @return string
     */
    public function getPayerLegalKrs(): ?string
    {
        return $this->payerLegalKrs;
    }

    /**
     * Set payerLegalRb.
     *
     * @param string $payerLegalRb
     *
     * @return $this
     */
    public function setPayerLegalRb(?string $payerLegalRb): self
    {
        $this->payerLegalRb = $payerLegalRb;

        return $this;
    }

    /**
     * Get payerLegalRb.
     *
     * @return string
     */
    public function getPayerLegalRb(): ?string
    {
        return $this->payerLegalRb;
    }

    /**
     * Set payerLegalRbCity.
     *
     * @param string $payerLegalRbCity
     *
     * @return $this
     */
    public function setPayerLegalRbCity(?string $payerLegalRbCity): self
    {
        $this->payerLegalRbCity = $payerLegalRbCity;

        return $this;
    }

    /**
     * Get payerLegalRbCity.
     *
     * @return string
     */
    public function getPayerLegalRbCity(): ?string
    {
        return $this->payerLegalRbCity;
    }

    /**
     * Set payerLegalBik.
     *
     * @param string $payerLegalBik
     *
     * @return $this
     */
    public function setPayerLegalBik(?string $payerLegalBik): self
    {
        $this->payerLegalBik = $payerLegalBik;

        return $this;
    }

    /**
     * Get payerLegalBik.
     *
     * @return string
     */
    public function getPayerLegalBik(): ?string
    {
        return $this->payerLegalBik;
    }

    /**
     * Set payerLegalTelephone.
     *
     * @param string $payerLegalTelephone
     *
     * @return $this
     */
    public function setPayerLegalTelephone(?string $payerLegalTelephone): self
    {
        $this->payerLegalTelephone = $payerLegalTelephone;

        return $this;
    }

    /**
     * Get payerLegalTelephone.
     *
     * @return string
     */
    public function getPayerLegalTelephone(): ?string
    {
        return $this->payerLegalTelephone;
    }

    /**
     * Set payerLegalEmail.
     *
     * @param string $payerLegalEmail
     *
     * @return $this
     */
    public function setPayerLegalEmail(?string $payerLegalEmail): self
    {
        $this->payerLegalEmail = $payerLegalEmail;

        return $this;
    }

    /**
     * Get payerLegalEmail.
     *
     * @return string
     */
    public function getPayerLegalEmail(): ?string
    {
        return $this->payerLegalEmail;
    }
}
