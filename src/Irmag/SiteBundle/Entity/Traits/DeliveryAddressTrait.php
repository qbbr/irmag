<?php

namespace Irmag\SiteBundle\Entity\Traits;

use Irmag\SiteBundle\Entity\OrderDeliveryCity;
use Irmag\SiteBundle\Entity\OrderDeliveryCityDistrict;

trait DeliveryAddressTrait
{
    /**
     * @var OrderDeliveryCity|null
     *
     * @Assert\NotBlank(groups={"delivery_address"})
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\SiteBundle\Entity\OrderDeliveryCity")
     * @ORM\JoinColumn(nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryCity;

    /**
     * @var OrderDeliveryCityDistrict|null
     *
     * @Assert\NotBlank(groups={"delivery_address_district"})
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\SiteBundle\Entity\OrderDeliveryCityDistrict")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryDistrict;

    /**
     * @var string|null
     *
     * @Assert\Length(max=255)
     * @Assert\NotBlank(groups={"delivery_address"})
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryStreet;

    /**
     * @var string|null
     *
     * @Assert\Length(max=50)
     * @Assert\NotBlank(groups={"delivery_address"})
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryHouse;

    /**
     * @var string|null
     *
     * @Assert\Length(max=50)
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryApartment;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $deliveryComment;

    /**
     * XXX: Custom.
     *
     * @return string
     */
    public function getDeliveryFullAddress(): string
    {
        $address = [];

        if ($this->deliveryCity) {
            $address[] = $this->deliveryCity->getName();
        }

        if (!empty($this->deliveryStreet)) {
            if ($this->deliveryDistrict) {
                $address[] = '('.$this->deliveryDistrict->getName().') '.$this->deliveryStreet;
            } else {
                $address[] = $this->deliveryStreet;
            }
        }

        if (!empty($this->deliveryHouse)) {
            $address[] = $this->deliveryHouse;
        }

        if (!empty($this->deliveryApartment)) {
            $address[] = $this->deliveryApartment;
        }

        return implode(', ', $address);
    }

    /**
     * Set deliveryCity.
     *
     * @param OrderDeliveryCity|null $deliveryCity
     *
     * @return $this
     */
    public function setDeliveryCity(OrderDeliveryCity $deliveryCity = null): self
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    /**
     * Get deliveryCity.
     *
     * @return OrderDeliveryCity
     */
    public function getDeliveryCity(): ?OrderDeliveryCity
    {
        return $this->deliveryCity;
    }

    /**
     * Set deliveryDistrict.
     *
     * @param OrderDeliveryCityDistrict|null $deliveryDistrict
     *
     * @return $this
     */
    public function setDeliveryDistrict(OrderDeliveryCityDistrict $deliveryDistrict = null): self
    {
        $this->deliveryDistrict = $deliveryDistrict;

        return $this;
    }

    /**
     * Get deliveryDistrict.
     *
     * @return OrderDeliveryCityDistrict|null
     */
    public function getDeliveryDistrict(): ?OrderDeliveryCityDistrict
    {
        return $this->deliveryDistrict;
    }

    /**
     * Set deliveryStreet.
     *
     * @param string|null $deliveryStreet
     *
     * @return $this
     */
    public function setDeliveryStreet(string $deliveryStreet = null): self
    {
        $this->deliveryStreet = $deliveryStreet;

        return $this;
    }

    /**
     * Get deliveryStreet.
     *
     * @return string|null
     */
    public function getDeliveryStreet(): ?string
    {
        return $this->deliveryStreet;
    }

    /**
     * Set deliveryHouse.
     *
     * @param string|null $deliveryHouse
     *
     * @return $this
     */
    public function setDeliveryHouse(string $deliveryHouse = null): self
    {
        $this->deliveryHouse = $deliveryHouse;

        return $this;
    }

    /**
     * Get deliveryHouse.
     *
     * @return string|null
     */
    public function getDeliveryHouse(): ?string
    {
        return $this->deliveryHouse;
    }

    /**
     * Set deliveryApartment.
     *
     * @param string|null $deliveryApartment
     *
     * @return $this
     */
    public function setDeliveryApartment(string $deliveryApartment = null): self
    {
        $this->deliveryApartment = $deliveryApartment;

        return $this;
    }

    /**
     * Get deliveryApartment.
     *
     * @return string|null
     */
    public function getDeliveryApartment(): ?string
    {
        return $this->deliveryApartment;
    }

    /**
     * Set deliveryComment.
     *
     * @param string|null $deliveryComment
     *
     * @return $this
     */
    public function setDeliveryComment(string $deliveryComment = null): self
    {
        $this->deliveryComment = $deliveryComment;

        return $this;
    }

    /**
     * Get deliveryComment.
     *
     * @return string|null
     */
    public function getDeliveryComment(): ?string
    {
        return $this->deliveryComment;
    }
}
