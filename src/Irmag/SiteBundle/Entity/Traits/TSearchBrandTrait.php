<?php

namespace Irmag\SiteBundle\Entity\Traits;

trait TSearchBrandTrait
{
    /**
     * @param string $searchQuery
     * @param int    $limit
     *
     * @return array
     */
    public function search(string $searchQuery, int $limit): array
    {
        return $this->createQueryBuilder('b')
            ->addSelect('bp')
            ->addSelect('PLAINTSRANK(b.tsv, :searchQuery, \'irmag\') as HIDDEN rank')
            ->leftJoin('b.picture', 'bp')
            ->andWhere('PLAINTSQUERY(b.tsv, :searchQuery, \'irmag\') = true')
            ->setParameter('searchQuery', $searchQuery)
            ->orderBy('rank', 'desc')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
