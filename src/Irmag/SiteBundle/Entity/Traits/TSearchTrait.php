<?php

namespace Irmag\SiteBundle\Entity\Traits;

trait TSearchTrait
{
    /**
     * @param string $searchQuery
     * @param int    $limit
     *
     * @return array
     */
    public function search(string $searchQuery, int $limit): array
    {
        return $this->createQueryBuilder('e')
            ->addSelect('PLAINTSRANK(e.tsv, :searchQuery, \'irmag\') as HIDDEN rank')
            ->andWhere('PLAINTSQUERY(e.tsv, :searchQuery, \'irmag\') = true')
            ->setParameter('searchQuery', $searchQuery)
            ->orderBy('rank', 'desc')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
