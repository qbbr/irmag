<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Table(name="orders_delivery_cities_days_of_week")
 * @ORM\Entity
 */
class OrderDeliveryCityDayOfWeek
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * День недели.
     *
     * @var int
     *
     * @ORM\Column(type="smallint")
     */
    private $dayOfWeek;

    /**
     * Минимальное кол-во дней до доставки.
     *
     * @var int
     *
     * @ORM\Column(type="smallint", options={"default": 0})
     */
    private $minDaysBeforeDelivery;

    /**
     * Город доставки.
     *
     * @var OrderDeliveryCity
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryCity", inversedBy="deliveryCityDayOfWeek")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $deliveryCity;

    /**
     * Время доставки.
     *
     * @var Collection|OrderDeliveryTime[]
     *
     * @ORM\ManyToMany(targetEntity="OrderDeliveryTime")
     * @ORM\JoinTable(name="orders_delivery_cities_days_of_week_orders_delivery_times",
     *     joinColumns={@ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="id")}
     * )
     */
    private $deliveryTime;

    public function __construct()
    {
        $this->minDaysBeforeDelivery = 0;
        $this->deliveryTime = new ArrayCollection();
    }

    /**
     * XXX: Custom for deep self cloning.
     */
    public function __clone()
    {
        $this->deliveryTime = clone $this->deliveryTime;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setDayOfWeek(int $dayOfWeek): self
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    public function getDayOfWeek(): ?int
    {
        return $this->dayOfWeek;
    }

    public function setMinDaysBeforeDelivery(int $minDaysBeforeDelivery): self
    {
        $this->minDaysBeforeDelivery = $minDaysBeforeDelivery;

        return $this;
    }

    public function getMinDaysBeforeDelivery(): ?int
    {
        return $this->minDaysBeforeDelivery;
    }

    public function setDeliveryCity(?OrderDeliveryCity $deliveryCity): self
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    public function getDeliveryCity(): ?OrderDeliveryCity
    {
        return $this->deliveryCity;
    }

    public function addDeliveryTime(OrderDeliveryTime $deliveryTime): self
    {
        if (!$this->deliveryTime->contains($deliveryTime)) {
            $this->deliveryTime[] = $deliveryTime;
        }

        return $this;
    }

    public function removeDeliveryTime(OrderDeliveryTime $deliveryTime): self
    {
        if ($this->deliveryTime->contains($deliveryTime)) {
            $this->deliveryTime->removeElement($deliveryTime);
        }

        return $this;
    }

    /**
     * @return Collection|OrderDeliveryTime[]
     */
    public function getDeliveryTime(): Collection
    {
        return $this->deliveryTime;
    }
}
