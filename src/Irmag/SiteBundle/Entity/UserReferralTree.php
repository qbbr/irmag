<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Gedmo\Mapping\Annotation as Gedmo;
use Irmag\ProfileBundle\Entity\User;

/**
 * @Gedmo\Tree(type="nested")
 *
 * @ORM\Table(
 *     name="users_referral_trees",
 *     indexes={
 *         @ORM\Index(columns={"lft", "lvl", "rgt"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\UserReferralTreeRepository")
 */
class UserReferralTree
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @Gedmo\TreeLeft
     *
     * @ORM\Column(type="integer")
     */
    private $lft;

    /**
     * @var int
     *
     * @Gedmo\TreeLevel
     *
     * @ORM\Column(type="integer")
     */
    private $lvl;

    /**
     * @var int
     *
     * @Gedmo\TreeRight
     *
     * @ORM\Column(type="integer")
     */
    private $rgt;

    /**
     * @var UserReferralTree
     *
     * @Gedmo\TreeRoot
     *
     * @ORM\ManyToOne(targetEntity="UserReferralTree")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $root;

    /**
     * @var UserReferralTree
     *
     * @Gedmo\TreeParent
     *
     * @ORM\ManyToOne(targetEntity="UserReferralTree", inversedBy="children")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @var int
     *
     * @ORM\OneToMany(targetEntity="UserReferralTree", mappedBy="parent")
     * @ORM\OrderBy({"lft": "ASC"})
     */
    private $children;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false, unique=true)
     */
    private $user;

    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set lft.
     *
     * @param int $lft
     *
     * @return UserReferralTree
     */
    public function setLft($lft): self
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lft.
     *
     * @return int
     */
    public function getLft(): int
    {
        return $this->lft;
    }

    /**
     * Set lvl.
     *
     * @param int $lvl
     *
     * @return UserReferralTree
     */
    public function setLvl($lvl): self
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get lvl.
     *
     * @return int
     */
    public function getLvl(): int
    {
        return $this->lvl;
    }

    /**
     * Set rgt.
     *
     * @param int $rgt
     *
     * @return UserReferralTree
     */
    public function setRgt($rgt): self
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get rgt.
     *
     * @return int
     */
    public function getRgt(): int
    {
        return $this->rgt;
    }

    /**
     * Set root.
     *
     * @param UserReferralTree|null $root
     *
     * @return UserReferralTree
     */
    public function setRoot(self $root = null): self
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root.
     *
     * @return UserReferralTree|null
     */
    public function getRoot(): ?self
    {
        return $this->root;
    }

    /**
     * Set parent.
     *
     * @param self $parent
     *
     * @return UserReferralTree
     */
    public function setParent(self $parent = null): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return UserReferralTree|null
     */
    public function getParent(): ?self
    {
        return $this->parent;
    }

    /**
     * Add child.
     *
     * @param self $child
     *
     * @return UserReferralTree
     */
    public function addChild(self $child): self
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child.
     *
     * @param self $child
     *
     * @return bool TRUE if this collection contained the specified element, FALSE otherwise
     */
    public function removeChild(self $child): bool
    {
        return $this->children->removeElement($child);
    }

    /**
     * Get children.
     *
     * @return Collection
     */
    public function getChildren(): Collection
    {
        return $this->children;
    }

    /**
     * Set user.
     *
     * @param User $user
     *
     * @return UserReferralTree
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}
