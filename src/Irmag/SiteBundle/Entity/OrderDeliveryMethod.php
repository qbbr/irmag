<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\Traits\OrderMethodTrait;

/**
 * @ORM\Table(name="orders_delivery_methods")
 * @ORM\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderDeliveryMethod
{
    use OrderMethodTrait;

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }
}
