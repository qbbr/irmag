<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="promocode_elements_lists")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\PromocodeElementsListRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class PromocodeElementsList
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode_elements_list"})
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Uuid
     *
     * @ORM\Column(type="guid", unique=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode_elements_list"})
     */
    private $uuid1C;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=256, nullable=true)
     */
    private $name;

    /**
     * @var Collection|Element[]
     *
     * @Assert\NotBlank
     *
     * @ORM\ManyToMany(targetEntity="Element")
     * @ORM\JoinTable(name="promocode_elements_lists_elements")
     */
    private $elements;

    /**
     * @var Collection|Promocode[]
     *
     * @ORM\OneToMany(targetEntity="Promocode", mappedBy="elementList")
     */
    private $promocodes;

    /**
     * @var Action
     *
     * @Assert\NotBlank
     *
     * @ORM\OneToOne(targetEntity="Action", inversedBy="promocodeElementsList")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_promocode_elements_list"})
     */
    private $action;

    public function __construct()
    {
        $this->elements = new ArrayCollection();
        $this->promocodes = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name ?? $this->uuid1C;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setUuid1C(string $uuid1C): self
    {
        $this->uuid1C = $uuid1C;

        return $this;
    }

    public function getUuid1C(): ?string
    {
        return $this->uuid1C;
    }

    public function addElement(Element $element): self
    {
        if (!$this->elements->contains($element)) {
            $this->elements[] = $element;
        }

        return $this;
    }

    public function removeElement(Element $element): self
    {
        if ($this->elements->contains($element)) {
            $this->elements->removeElement($element);
        }

        return $this;
    }

    /**
     * @return Collection|Element[]
     */
    public function getElements(): Collection
    {
        return $this->elements;
    }

    public function addPromocode(Promocode $promocode): self
    {
        if (!$this->promocodes->contains($promocode)) {
            $this->promocodes[] = $promocode;
            $promocode->setElementList($this);
        }

        return $this;
    }

    public function removePromocode(Promocode $promocode): self
    {
        if ($this->promocodes->contains($promocode)) {
            $this->promocodes->removeElement($promocode);
            // set the owning side to null (unless already changed)
            if ($promocode->getElementList() === $this) {
                $promocode->setElementList(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Promocode[]
     */
    public function getPromocodes(): Collection
    {
        return $this->promocodes;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setAction(?Action $action): self
    {
        $this->action = $action;

        return $this;
    }

    public function getAction(): ?Action
    {
        return $this->action;
    }
}
