<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Table(name="files")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class File
{
    use TimestampableEntity;

    const UPLOAD_DIR = 'uploads/files';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="bigint")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $mimeType;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $size;

    /**
     * @Assert\File(maxSize="10m")
     */
    private $file;

    /**
     * @var string
     */
    private $dirStructure;

    /**
     * @var string
     */
    private $temp;

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    /**
     * @return string|null
     */
    public function getAbsolutePath(): ?string
    {
        return null === $this->path
            ? null
            : $this->getUploadRootDir().'/'.$this->path;
    }

    /**
     * @return string|null
     */
    public function getWebPath(): ?string
    {
        return null === $this->path
            ? null
            : $this->getUploadDir().'/'.$this->path;
    }

    /**
     * @return string
     */
    private function getUploadRootDir(): string
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../../web/'.$this->getUploadDir();
    }

    /**
     * @return string
     */
    private function getUploadDir(): string
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return self::UPLOAD_DIR;
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function preUpload(): void
    {
        if (null === $this->getFile()) {
            return;
        }

        $this->dirStructure = (new \DateTime())->format('Y/m/d');
        $filename = sha1(uniqid(mt_rand(), true));
        $this->path = $this->dirStructure.'/'.$filename.'.'.$this->getFile()->guessExtension();
        $this->mimeType = $this->getFile()->getMimeType();
        $this->size = (string) $this->getFile()->getClientSize();
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function upload(): void
    {
        if (null === $this->getFile()) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir().'/'.$this->dirStructure, $this->path);

        // check if we have an old image
        if (isset($this->temp)) {
            // delete the old image
            @unlink($this->getUploadRootDir().'/'.$this->temp);
            // clear the temp image path
            $this->temp = null;
        }

        $this->file = null;
    }

    /**
     * XXX: Doctrine internal bug? Need to be for correct postRemove callback executing.
     *
     * @ORM\PreRemove
     */
    public function emptyPreRemoveProcedure(): void
    {
    }

    /**
     * @ORM\PostRemove
     */
    public function removeUpload(): void
    {
        $file = $this->getAbsolutePath();

        if (file_exists($file)) {
            @unlink($file);
        }

        $this->file = null;
    }

    /**
     * @param UploadedFile|null $file
     *
     * @return File
     */
    public function setFile(?UploadedFile $file): self
    {
        $this->file = $file;

        // check if we have an old image path
        if (isset($this->path)) {
            // store the old name to delete after the update
            $this->temp = $this->path;
            $this->path = null;
        }

        return $this;
    }

    /**
     * @return UploadedFile|null
     */
    public function getFile(): ?UploadedFile
    {
        return $this->file;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $path
     *
     * @return File
     */
    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $mimeType
     *
     * @return File
     */
    public function setMimeType(?string $mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param string $size
     *
     * @return File
     */
    public function setSize(string $size): self
    {
        $this->size = $size;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSize(): ?string
    {
        return $this->size;
    }
}
