<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Table(name="orders_delivery_cities_selfservice_methods",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(columns={"delivery_city_id", "selfservice_id"})
 *     }
 * )
 * @ORM\Entity
 *
 * @UniqueEntity(fields={"deliveryCity", "selfservice"})
 */
class OrderDeliveryCitySelfserviceMethod
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var OrderDeliveryCity
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryCity", inversedBy="selfserviceMethod")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $deliveryCity;

    /**
     * @var OrderDeliverySelfserviceMethod
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliverySelfserviceMethod")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $selfservice;

    /**
     * @var Collection|OrderPaymentMethod[]
     *
     * @ORM\ManyToMany(targetEntity="OrderPaymentMethod")
     * @ORM\JoinTable(
     *     joinColumns={@ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="id")}
     * )
     */
    private $paymentMethod;

    public function __construct()
    {
        $this->paymentMethod = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDeliveryCity(): ?OrderDeliveryCity
    {
        return $this->deliveryCity;
    }

    public function setDeliveryCity(?OrderDeliveryCity $deliveryCity): self
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }

    public function getSelfservice(): ?OrderDeliverySelfserviceMethod
    {
        return $this->selfservice;
    }

    public function setSelfservice(?OrderDeliverySelfserviceMethod $selfservice): self
    {
        $this->selfservice = $selfservice;

        return $this;
    }

    /**
     * @return Collection|OrderPaymentMethod[]
     */
    public function getPaymentMethod(): Collection
    {
        return $this->paymentMethod;
    }

    public function addPaymentMethod(OrderPaymentMethod $paymentMethod): self
    {
        if (!$this->paymentMethod->contains($paymentMethod)) {
            $this->paymentMethod[] = $paymentMethod;
        }

        return $this;
    }

    public function removePaymentMethod(OrderPaymentMethod $paymentMethod): self
    {
        if ($this->paymentMethod->contains($paymentMethod)) {
            $this->paymentMethod->removeElement($paymentMethod);
        }

        return $this;
    }
}
