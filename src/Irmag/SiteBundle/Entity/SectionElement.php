<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="sections_elements")
 * @ORM\Entity
 */
class SectionElement
{
    /**
     * @var Element
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Element", inversedBy="sectionElements", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $element;

    /**
     * @var Section
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Section", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $section;

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setSection(?Section $section): self
    {
        $this->section = $section;

        return $this;
    }

    public function getSection(): ?Section
    {
        return $this->section;
    }
}
