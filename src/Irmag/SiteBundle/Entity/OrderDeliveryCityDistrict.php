<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="orders_delivery_cities_districts")
 * @ORM\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderDeliveryCityDistrict
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $id;

    /**
     * Название.
     *
     * @var string
     *
     * @Assert\Length(max=255)
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $name;

    /**
     * @var OrderDeliveryCity
     *
     * @ORM\ManyToOne(targetEntity="OrderDeliveryCity", inversedBy="districts")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $deliveryCity;

    /**
     * Активность.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive = true;

    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getDeliveryCity(): ?OrderDeliveryCity
    {
        return $this->deliveryCity;
    }

    public function setDeliveryCity(?OrderDeliveryCity $deliveryCity): self
    {
        $this->deliveryCity = $deliveryCity;

        return $this;
    }
}
