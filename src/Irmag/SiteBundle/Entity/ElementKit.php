<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="element_kit")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\ElementKitRepository")
 */
class ElementKit
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Element
     *
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $element1;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $element1ForAllGroupBy;

    /**
     * @var Element
     *
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $element2;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $element2ForAllGroupBy;

    /**
     * @var Element
     *
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $element3;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $element3ForAllGroupBy;

    /**
     * @var Element
     *
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $element4;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $element4ForAllGroupBy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setElement1ForAllGroupBy(?int $element1ForAllGroupBy): self
    {
        $this->element1ForAllGroupBy = $element1ForAllGroupBy;

        return $this;
    }

    public function getElement1ForAllGroupBy(): ?int
    {
        return $this->element1ForAllGroupBy;
    }

    public function setElement2ForAllGroupBy(?int $element2ForAllGroupBy): self
    {
        $this->element2ForAllGroupBy = $element2ForAllGroupBy;

        return $this;
    }

    public function getElement2ForAllGroupBy(): ?int
    {
        return $this->element2ForAllGroupBy;
    }

    public function setElement3ForAllGroupBy(?int $element3ForAllGroupBy): self
    {
        $this->element3ForAllGroupBy = $element3ForAllGroupBy;

        return $this;
    }

    public function getElement3ForAllGroupBy(): ?int
    {
        return $this->element3ForAllGroupBy;
    }

    public function setElement4ForAllGroupBy(?int $element4ForAllGroupBy): self
    {
        $this->element4ForAllGroupBy = $element4ForAllGroupBy;

        return $this;
    }

    public function getElement4ForAllGroupBy(): ?int
    {
        return $this->element4ForAllGroupBy;
    }

    public function setElement1(?Element $element1): self
    {
        $this->element1 = $element1;

        return $this;
    }

    public function getElement1(): ?Element
    {
        return $this->element1;
    }

    public function setElement2(?Element $element2): self
    {
        $this->element2 = $element2;

        return $this;
    }

    public function getElement2(): ?Element
    {
        return $this->element2;
    }

    public function setElement3(?Element $element3): self
    {
        $this->element3 = $element3;

        return $this;
    }

    public function getElement3(): ?Element
    {
        return $this->element3;
    }

    public function setElement4(?Element $element4): self
    {
        $this->element4 = $element4;

        return $this;
    }

    public function getElement4(): ?Element
    {
        return $this->element4;
    }
}
