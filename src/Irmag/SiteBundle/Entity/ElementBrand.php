<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\Traits\SeoTrait;

/**
 * @ORM\Table(name="elements_brands")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\ElementBrandRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class ElementBrand
{
    use SeoTrait;

    const SHORT_NAME = 'brand';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"search"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"search"})
     */
    private $name;

    /**
     * @var File
     *
     * @ORM\OneToOne(targetEntity="File", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $picture;

    /**
     * @var ElementManufacturer
     *
     * @ORM\ManyToOne(targetEntity="ElementManufacturer", inversedBy="brands")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     */
    private $manufacturer;

    /**
     * @ORM\Column(name="tsv", type="tsvector", nullable=true)
     */
    private $tsv;

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setPicture(?File $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getPicture(): ?File
    {
        return $this->picture;
    }

    public function setManufacturer(?ElementManufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getManufacturer(): ?ElementManufacturer
    {
        return $this->manufacturer;
    }
}
