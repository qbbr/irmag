<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Irmag\SiteBundle\Config;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="orders_delivery_times")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\OrderDeliveryTimeRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderDeliveryTime
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $id;

    /**
     * Название.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $name;

    /**
     * Короткое название.
     *
     * @var string|null
     *
     * @ORM\Column(type="string", length=32, unique=true, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $shortname;

    /**
     * Ограничение временем.
     *
     * @var \DateTimeInterface|null
     *
     * @ORM\Column(type="time", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Type("DateTime<'H:i:s'>")
     * @Serializer\Groups({"api_order"})
     */
    private $timeLimit;

    /**
     * Активность.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return $this->timeLimit
            ? sprintf('%s (%s)', $this->name, $this->getTimeLimit()->format(Config::TIME_WITHOUT_SECONDS_FORMAT))
            : (string) $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(?string $shortname): self
    {
        $this->shortname = $shortname;

        return $this;
    }

    public function getTimeLimit(): ?\DateTimeInterface
    {
        return $this->timeLimit;
    }

    public function setTimeLimit(?\DateTimeInterface $timeLimit): self
    {
        $this->timeLimit = $timeLimit;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }
}
