<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Irmag\SiteBundle\Entity\Traits\SeoTrait;

/**
 * @ORM\Table(name="elements_manufacturers")
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\ElementManufacturerRepository")
 */
class ElementManufacturer
{
    use SeoTrait;

    const SHORT_NAME = 'manufacturer';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var File
     *
     * @ORM\OneToOne(targetEntity="File", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $picture;

    /**
     * @var Collection|ElementBrand[]
     *
     * @ORM\OneToMany(targetEntity="ElementBrand", mappedBy="manufacturer")
     */
    private $brands;

    /**
     * @ORM\Column(name="tsv", type="tsvector", nullable=true)
     */
    private $tsv;

    public function __construct()
    {
        $this->brands = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setPicture(?File $picture): self
    {
        $this->picture = $picture;

        return $this;
    }

    public function getPicture(): ?File
    {
        return $this->picture;
    }

    public function addBrand(ElementBrand $brand): self
    {
        if (!$this->brands->contains($brand)) {
            $this->brands[] = $brand;
            $brand->setManufacturer($this);
        }

        return $this;
    }

    public function removeBrand(ElementBrand $brand): self
    {
        if ($this->brands->contains($brand)) {
            $this->brands->removeElement($brand);
            // set the owning side to null (unless already changed)
            if ($brand->getManufacturer() === $this) {
                $brand->setManufacturer(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ElementBrand[]
     */
    public function getBrands(): Collection
    {
        return $this->brands;
    }
}
