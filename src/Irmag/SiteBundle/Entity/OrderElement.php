<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(
 *     name="orders_elements",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="order_element_unique_indx", columns={"order_id", "element_id"})
 *     }
 * )
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderElement
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @serializer\expose
     * @serializer\groups({"api_order_element", "api_order"})
     */
    private $id;

    /**
     * @var Order
     *
     * @Assert\NotBlank
     *
     * @ORM\ManyToOne(targetEntity="Order", inversedBy="orderElement", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     *
     * @serializer\expose
     * @serializer\groups({"api_order_element"})
     */
    private $order;

    /**
     * @var Element
     *
     * @ORM\ManyToOne(targetEntity="Element")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element", "api_order"})
     */
    private $element;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element", "api_order"})
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element", "api_order"})
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element", "api_order"})
     */
    private $priceWithoutDiscount;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", options={"default": 1})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element", "api_order"})
     */
    private $amount;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", options={"default": 0})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element", "api_order"})
     */
    private $amountDiff;

    public function __construct()
    {
        $this->amount = 1;
        $this->amountDiff = 0;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->name;
    }

    /**
     * Записываем name из element.
     * XXX: Custom.
     *
     * @ORM\PreFlush
     */
    public function fillFieldsFromElementIfNeed(): void
    {
        if (null === $this->getElement()) {
            return;
        }

        if (empty($this->getName())) {
            $this->setName($this->getElement()->getFullName());
        }

        if (empty($this->getPrice())) {
            $this->setPrice($this->getElement()->getPrice());
        }
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPriceWithoutDiscount(?float $priceWithoutDiscount): self
    {
        $this->priceWithoutDiscount = $priceWithoutDiscount;

        return $this;
    }

    public function getPriceWithoutDiscount(): ?float
    {
        return $this->priceWithoutDiscount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmountDiff(int $amountDiff): self
    {
        $this->amountDiff = $amountDiff;

        return $this;
    }

    public function getAmountDiff(): ?int
    {
        return $this->amountDiff;
    }

    public function setOrder(?Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getOrder(): ?Order
    {
        return $this->order;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }
}
