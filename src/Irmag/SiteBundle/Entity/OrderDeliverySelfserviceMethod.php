<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\Traits\OrderMethodTrait;

/**
 * @ORM\Table(name="orders_delivery_selfservice_methods")
 * @ORM\Entity
 *
 * @UniqueEntity(fields="name")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderDeliverySelfserviceMethod
{
    use OrderMethodTrait;

    /**
     * @var string
     *
     * @Assert\Length(max=255)
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $address;

    /**
     * Время доставки.
     *
     * @var Collection|OrderDeliveryTime[]
     *
     * @ORM\ManyToMany(targetEntity="OrderDeliveryTime")
     * @ORM\JoinTable(name="orders_delivery_selfservice_methods_orders_delivery_times",
     *     joinColumns={@ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(referencedColumnName="id")}
     * )
     */
    private $deliveryTime;

    /**
     * @var string
     *
     * @Assert\Length(max=32)
     *
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $token;

    public function __construct()
    {
        $this->deliveryTime = new ArrayCollection();
    }

    public function __toString()
    {
        return sprintf('%s (%s)', $this->name, $this->address);
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return Collection|OrderDeliveryTime[]
     */
    public function getDeliveryTime(): Collection
    {
        return $this->deliveryTime;
    }

    public function addDeliveryTime(OrderDeliveryTime $deliveryTime): self
    {
        if (!$this->deliveryTime->contains($deliveryTime)) {
            $this->deliveryTime[] = $deliveryTime;
        }

        return $this;
    }

    public function removeDeliveryTime(OrderDeliveryTime $deliveryTime): self
    {
        if ($this->deliveryTime->contains($deliveryTime)) {
            $this->deliveryTime->removeElement($deliveryTime);
        }

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }
}
