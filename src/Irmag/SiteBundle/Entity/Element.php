<?php

namespace Irmag\SiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\ThreadCommentBundle\Entity\Traits\ThreadTrait;
use Irmag\ThreadCommentBundle\Entity\ThreadOwnerInterface;
use JMS\Serializer\Annotation as Serializer;

/**
 * При добавление/удаление/изменение полей, нужно пересоздать view "elements_grouped"
 * (src/Irmag/SiteBundle/Entity/SQL/elements_grouped.sql).
 *
 * @ORM\Table(
 *     name="elements",
 *     indexes={
 *         @ORM\Index(columns={"price"}),
 *         @ORM\Index(columns={"name"}),
 *         @ORM\Index(columns={"prop_eco"}),
 *         @ORM\Index(columns={"prop_special_price"}),
 *         @ORM\Index(columns={"prop_safe_animal"}),
 *         @ORM\Index(columns={"prop_transfer"}),
 *         @ORM\Index(columns={"weight"}),
 *         @ORM\Index(columns={"volume"}),
 *         @ORM\Index(columns={"group_by"}),
 *         @ORM\Index(columns={"series"}),
 *         @ORM\Index(columns={"created_at"}),
 *         @ORM\Index(columns={"is_active"}),
 *     }
 * )
 * @ORM\Entity(repositoryClass="Irmag\SiteBundle\Repository\ElementRepository")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class Element implements ThreadOwnerInterface
{
    use TimestampableEntity;
    use ThreadTrait;

    const NOT_FOUND_IMAGE = 'build/images/not-found-image.jpg';

    /**
     * Сколько товаров показывать в предкассовой зоне.
     */
    const PRE_ORDER_ZONE_COLS_AMOUNT = 4;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element", "api_order", "search"})
     */
    private $id;

    /**
     * Наименование.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"search"})
     */
    private $name;

    /**
     * Наименование тона.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"search"})
     */
    private $toneName;

    /**
     * Цена.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"search"})
     */
    private $price;

    /**
     * Описание.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * Состав.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $composition;

    /**
     * Способ применения.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $howToUse;

    /**
     * Разделы с товарами.
     *
     * @var Collection|SectionElement[]
     *
     * @ORM\OneToMany(targetEntity="SectionElement", mappedBy="element", cascade={"persist"}, orphanRemoval=true)
     */
    private $sectionElements;

    /**
     * Бренд.
     *
     * @var ElementBrand
     *
     * @ORM\ManyToOne(targetEntity="ElementBrand")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $brand;

    /**
     * Производитель.
     *
     * @var ElementManufacturer
     *
     * @ORM\ManyToOne(targetEntity="ElementManufacturer")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $manufacturer;

    /**
     * Страна.
     *
     * @var ElementCountry
     *
     * @ORM\ManyToOne(targetEntity="ElementCountry")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $country;

    /**
     * Штрихкод.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $barcode;

    /**
     * Вес.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $weight;

    /**
     * Объём.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $volume;

    /**
     * Таблица размеров.
     *
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $tableSize;

    /**
     * Ссылка на видео.
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $videoUrl;

    /**
     * Экологически чистый товар.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $propEco;

    /**
     * Спец. цена.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $propSpecialPrice;

    /**
     * Этичный товар.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $propSafeAnimal;

    /**
     * Трансфер.
     * (null - не трансфер, int  - трансфер).
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $propTransfer;

    /**
     * Фотографии товара.
     *
     * @var Collection|ElementPicture[]
     *
     * @ORM\OneToMany(targetEntity="ElementPicture", mappedBy="element", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\OrderBy({"position": "ASC"})
     */
    private $pictures;

    /**
     * Картинка тона.
     *
     * @var File
     *
     * @ORM\OneToOne(targetEntity="File", cascade={"persist", "remove"}, orphanRemoval=true)
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $tonePicture;

    /**
     * Группировка товара.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $groupBy;

    /**
     * Остаток товара.
     *
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $inStock;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", options={"default": 1})
     */
    private $inPackage;

    /**
     * Группа ABC анализа.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=3, nullable=true)
     */
    private $abcGroup;

    /**
     * Серия товара.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $series;

    /**
     * @ORM\Column(name="tsv", type="tsvector", nullable=true)
     */
    private $tsv;

    /**
     * Товар находится в предкассовой зоне.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isPreOrderZone;

    /**
     * Товар только для дома, недоступен к доставке в регионы.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isOnlyForHome;

    /**
     * Активность.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    public function __construct()
    {
        $this->sectionElements = new ArrayCollection();
        $this->pictures = new ArrayCollection();
        $this->propEco = false;
        $this->propSpecialPrice = false;
        $this->propSafeAnimal = false;
        $this->inStock = 0;
        $this->inPackage = 1;
        $this->isPreOrderZone = false;
        $this->isOnlyForHome = false;
        $this->isActive = true;
    }

    /**
     * Return $name + $toneName (if available).
     *
     * @return string
     */
    public function getFullName(): string
    {
        $fullName = $this->name;

        if (!empty($this->toneName)) {
            $fullName .= ' - '.$this->toneName;
        }

        return $fullName;
    }

    /**
     * Список доступных свойств для валидации без приставки `prop`.
     *
     * @return array
     */
    public static function getAvailablePropNames(): array
    {
        return ['Action', 'Eco', 'SpecialPrice', 'SafeAnimal', 'Transfer'];
    }

    /**
     * XXX: Custom for ThreadCommentBundle.
     */
    public function setThreadOwnerRoute(): void
    {
        $this->getThread()->setOwnerRoute([
            'name' => 'irmag_catalog_element',
            'parameters' => [
                'id' => $this->getId(),
            ],
        ]);
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return $this->getFullName();
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setToneName(?string $toneName): self
    {
        $this->toneName = $toneName;

        return $this;
    }

    public function getToneName(): ?string
    {
        return $this->toneName;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setComposition(?string $composition): self
    {
        $this->composition = $composition;

        return $this;
    }

    public function getComposition(): ?string
    {
        return $this->composition;
    }

    public function setHowToUse(?string $howToUse): self
    {
        $this->howToUse = $howToUse;

        return $this;
    }

    public function getHowToUse(): ?string
    {
        return $this->howToUse;
    }

    public function setBarcode(?string $barcode): self
    {
        $this->barcode = $barcode;

        return $this;
    }

    public function getBarcode(): ?string
    {
        return $this->barcode;
    }

    public function setWeight(?int $weight): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getWeight(): ?int
    {
        return $this->weight;
    }

    public function setVolume(?int $volume): self
    {
        $this->volume = $volume;

        return $this;
    }

    public function getVolume(): ?int
    {
        return $this->volume;
    }

    public function setTableSize(?int $tableSize): self
    {
        $this->tableSize = $tableSize;

        return $this;
    }

    public function getTableSize(): ?int
    {
        return $this->tableSize;
    }

    public function setVideoUrl(?string $videoUrl): self
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    public function getVideoUrl(): ?string
    {
        return $this->videoUrl;
    }

    public function setPropEco(bool $propEco): self
    {
        $this->propEco = $propEco;

        return $this;
    }

    public function getPropEco(): ?bool
    {
        return $this->propEco;
    }

    public function setPropSpecialPrice(bool $propSpecialPrice): self
    {
        $this->propSpecialPrice = $propSpecialPrice;

        return $this;
    }

    public function getPropSpecialPrice(): ?bool
    {
        return $this->propSpecialPrice;
    }

    public function setPropSafeAnimal(bool $propSafeAnimal): self
    {
        $this->propSafeAnimal = $propSafeAnimal;

        return $this;
    }

    public function getPropSafeAnimal(): ?bool
    {
        return $this->propSafeAnimal;
    }

    public function setPropTransfer(?int $propTransfer): self
    {
        $this->propTransfer = $propTransfer;

        return $this;
    }

    public function getPropTransfer(): ?int
    {
        return $this->propTransfer;
    }

    public function setGroupBy(?int $groupBy): self
    {
        $this->groupBy = $groupBy;

        return $this;
    }

    public function getGroupBy(): ?int
    {
        return $this->groupBy;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function addSectionElement(SectionElement $sectionElement): self
    {
        if (!$this->sectionElements->contains($sectionElement)) {
            $this->sectionElements[] = $sectionElement;
            $sectionElement->setElement($this);
        }

        return $this;
    }

    public function removeSectionElement(SectionElement $sectionElement): self
    {
        if ($this->sectionElements->contains($sectionElement)) {
            $this->sectionElements->removeElement($sectionElement);
            // set the owning side to null (unless already changed)
            if ($sectionElement->getElement() === $this) {
                $sectionElement->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SectionElement[]
     */
    public function getSectionElements(): Collection
    {
        return $this->sectionElements;
    }

    public function setBrand(?ElementBrand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getBrand(): ?ElementBrand
    {
        return $this->brand;
    }

    public function setManufacturer(?ElementManufacturer $manufacturer): self
    {
        $this->manufacturer = $manufacturer;

        return $this;
    }

    public function getManufacturer(): ?ElementManufacturer
    {
        return $this->manufacturer;
    }

    public function setCountry(?ElementCountry $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCountry(): ?ElementCountry
    {
        return $this->country;
    }

    public function addPicture(ElementPicture $picture): self
    {
        if (!$this->pictures->contains($picture)) {
            $this->pictures[] = $picture;
            $picture->setElement($this);
        }

        return $this;
    }

    public function removePicture(ElementPicture $picture): self
    {
        if ($this->pictures->contains($picture)) {
            $this->pictures->removeElement($picture);
            // set the owning side to null (unless already changed)
            if ($picture->getElement() === $this) {
                $picture->setElement(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ElementPicture[]
     */
    public function getPictures(): Collection
    {
        return $this->pictures;
    }

    public function setTonePicture(?File $tonePicture): self
    {
        $this->tonePicture = $tonePicture;

        return $this;
    }

    public function getTonePicture(): ?File
    {
        return $this->tonePicture;
    }

    public function setInStock(int $inStock): self
    {
        $this->inStock = $inStock;

        return $this;
    }

    public function getInStock(): ?int
    {
        return $this->inStock;
    }

    public function setInPackage(int $inPackage): self
    {
        $this->inPackage = $inPackage;

        return $this;
    }

    public function getInPackage(): ?int
    {
        return $this->inPackage;
    }

    public function setAbcGroup(?string $abcGroup): self
    {
        $this->abcGroup = $abcGroup;

        return $this;
    }

    public function getAbcGroup(): ?string
    {
        return $this->abcGroup;
    }

    public function setIsPreOrderZone(bool $isPreOrderZone): self
    {
        $this->isPreOrderZone = $isPreOrderZone;

        return $this;
    }

    public function getIsPreOrderZone(): ?bool
    {
        return $this->isPreOrderZone;
    }

    public function setIsOnlyForHome(bool $isOnlyForHome): self
    {
        $this->isOnlyForHome = $isOnlyForHome;

        return $this;
    }

    public function getIsOnlyForHome(): ?bool
    {
        return $this->isOnlyForHome;
    }

    public function getSeries(): ?int
    {
        return $this->series;
    }

    public function setSeries(?int $series): self
    {
        $this->series = $series;

        return $this;
    }
}
