<?php

namespace Irmag\SiteBundle\Variable;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Irmag\SiteBundle\EventListener\DeviceDetectorKernelRequestListener;
use Irmag\SiteBundle\Utils\DeviceDetector;
use Irmag\CoreBundle\Utils\Formatter;

/**
 * Wrapper for "irmag" global variable.
 */
class IrmagVariable
{
    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var ActionVariable
     */
    private $actionVariable;

    /**
     * @var BasketVariable
     */
    private $basketVariable;

    /**
     * @var FavoriteVariable
     */
    private $favoriteVariable;

    /**
     * @var int
     */
    private $elementsQuestionsTopicId;

    /**
     * @param SessionInterface $session
     * @param ActionVariable   $actionVariable
     * @param FavoriteVariable $favoriteVariable
     * @param BasketVariable   $basketVariable
     * @param int              $elementsQuestionsTopicId
     */
    public function __construct(
        SessionInterface $session,
        ActionVariable $actionVariable,
        FavoriteVariable $favoriteVariable,
        BasketVariable $basketVariable,
        int $elementsQuestionsTopicId
    ) {
        $this->session = $session;
        $this->actionVariable = $actionVariable;
        $this->favoriteVariable = $favoriteVariable;
        $this->basketVariable = $basketVariable;
        $this->elementsQuestionsTopicId = $elementsQuestionsTopicId;
    }

    /**
     * "irmag.favorite" wrapper.
     *
     * @return FavoriteVariable
     */
    public function getFavorite(): FavoriteVariable
    {
        return $this->favoriteVariable;
    }

    /**
     * "irmag.basket" wrapper.
     *
     * @return BasketVariable
     */
    public function getBasket(): BasketVariable
    {
        return $this->basketVariable;
    }

    /**
     * "irmag.action" wrapper.
     *
     * @return ActionVariable
     */
    public function getAction(): ActionVariable
    {
        return $this->actionVariable;
    }

    /**
     * @return int
     */
    public function getElementsQuestionsTopicId(): int
    {
        return $this->elementsQuestionsTopicId;
    }

    /**
     * @param int $grams
     *
     * @return string
     */
    public function formatGrams(int $grams): string
    {
        return Formatter::formatGrams($grams);
    }

    /**
     * @return bool
     */
    public function isMobile(): bool
    {
        return DeviceDetector::MOBILE_TYPE === $this->session->get(DeviceDetectorKernelRequestListener::SESSION_DEVICE_NAME);
    }

    /**
     * @return bool
     */
    public function isDesktop(): bool
    {
        return DeviceDetector::DESKTOP_TYPE === $this->session->get(DeviceDetectorKernelRequestListener::SESSION_DEVICE_NAME);
    }
}
