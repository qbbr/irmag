<?php

namespace Irmag\SiteBundle\Variable;

use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Service\ActionConditionsManager\ActionConditionsManager;
use Irmag\SiteBundle\Service\PriceManager\Adapter\ActionAdapter;

class ActionVariable
{
    /**
     * @var ActionAdapter
     */
    private $actionAdapter;

    /**
     * @var ActionConditionsManager
     */
    private $actionConditionsManager;

    /**
     * @param ActionAdapter           $actionAdapter
     * @param ActionConditionsManager $actionConditionsManager
     */
    public function __construct(
        ActionAdapter $actionAdapter,
        ActionConditionsManager $actionConditionsManager
    ) {
        $this->actionAdapter = $actionAdapter;
        $this->actionConditionsManager = $actionConditionsManager;
    }

    /**
     * @return array
     */
    public function getElements(): array
    {
        return $this->actionAdapter->getElements();
    }

    /**
     * @param Action|null $action
     *
     * @return array
     */
    public function getConditions(?Action $action = null): array
    {
        if (null === $action) {
            return $this->actionConditionsManager->getConditions();
        }

        return $this->actionConditionsManager->getConditionsByAction($action);
    }
}
