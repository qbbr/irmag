<?php

namespace Irmag\SiteBundle;

class Config
{
    const DATE_FORMAT = 'd.m.Y';
    const TIME_WITHOUT_SECONDS_FORMAT = 'H:i';
    const DATETIME_FORMAT = 'd.m.Y H:i:s';
    const DATE_FORMAT_RFC3339 = 'dd.MM.yyyy';
    const DATETIME_FORMAT_RFC3339 = 'dd.MM.yyyy HH:mm:ss';

    /** $_GET параметр для реферальной ссылки. */
    const REFERRAL_LINK_GET_PARAMETER_NAME = '_ref';
    /** Сессия для реферальной ссылки. */
    const REFERRAL_LINK_SESSION_NAME = 'referrer_link_user_id';

    /** Кол-во начисляемых бонусов за регистрацию. */
    const BONUS_FOR_REGISTRATION = 30;
    /** Кол-во начисляемых бонусов пользователю по реферальной ссылке. */
    const BONUS_FOR_REFERRAL = 100;

    /** Для behat тестов. */
    const TESTER_USERNAME = '__irmag_tester';

    /** Для генерации QR кодов. */
    const QR_CODE_BASE_URL = 'https://chart.googleapis.com/chart?chs=300x300&cht=qr&chl=';
}
