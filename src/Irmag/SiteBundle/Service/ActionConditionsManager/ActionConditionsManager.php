<?php

namespace Irmag\SiteBundle\Service\ActionConditionsManager;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\BasketBundle\Entity\BasketElement;
use Irmag\BasketBundle\Service\DataCollector;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\SiteBundle\Service\PriceManager\Adapter\ActionAdapter;
use Irmag\SiteBundle\Service\PriceManager\PriceManager;
use Irmag\SiteBundle\Entity\Action;

class ActionConditionsManager
{
    const CONDITION_CURRENT = 'current';
    const CONDITION_NEED = 'need';
    const CONDITION_IS_COMPLETED = 'isCompleted';

    const CONDITION_MIN_SUM = 'conditionMinSum';
    const CONDITION_MIN_COUNT = 'conditionMinCount';
    const CONDITION_PAYMENT_METHOD = 'conditionPaymentMethod';
    const CONDITION_IS_FOR_ALL_ELEMENTS = 'conditionIsForAllElements';
    const CONDITION_MAX_GIFTS = 'conditionMaxGifts';
    const CONDITION_ACTION_ELEMENT_IN_BASKET = 'conditionActionElementInBasket';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var BasketManager
     */
    private $basketManager;

    /**
     * @var DataCollector
     */
    private $dataCollector;

    /**
     * @var ActionAdapter
     */
    private $actionAdapter;

    /**
     * @var PriceManager
     */
    private $priceManager;

    /**
     * @var array
     *
     * @example
     * [
     *     [
     *         'action' => Action,
     *         'conditionMinSum' => [],
     *         'conditionMinCount' => [],
     *         'conditionPaymentMethod' => [],
     *         ...
     *     ],
     *     ...
     * ]
     */
    private $conditions = [];

    /**
     * @param EntityManagerInterface $em
     * @param BasketManager          $basketManager
     * @param DataCollector          $dataCollector
     * @param ActionAdapter          $actionAdapter
     * @param PriceManager           $priceManager
     */
    public function __construct(
        EntityManagerInterface $em,
        BasketManager $basketManager,
        DataCollector $dataCollector,
        ActionAdapter $actionAdapter,
        PriceManager $priceManager
    ) {
        $this->em = $em;
        $this->basketManager = $basketManager;
        $this->dataCollector = $dataCollector;
        $this->actionAdapter = $actionAdapter;
        $this->priceManager = $priceManager;
    }

    /**
     * @return array
     */
    public function getConditions(): array
    {
        $basketElements = $this->basketManager->getBasketElements(true);

        if (null === $basketElements) {
            return [];
        }

        foreach ($basketElements as $basketElement) {
            $element = $basketElement->getElement();

            if (null === $element) {
                continue;
            }

            $elementActions = $this->actionAdapter->getActions($element);

            if (null !== $elementActions) {
                foreach ($elementActions as $elementAction) {
                    if (false === $this->isActionWithConditions($elementAction)) {
                        continue;
                    }

                    if (!isset($this->conditions[$elementAction->getId()])) {
                        $this->conditions[$elementAction->getId()] = ['action' => $elementAction];
                    }

                    $this->calculate($elementAction, $basketElement);
                }
            }
        }

        foreach ($this->getActionsWithIsForAllElements() as $action) {
            $this->conditions[$action->getId()] = ['action' => $action];
            $this->calculate($action);
        }

        return $this->conditions;
    }

    /**
     * @param Action $action
     *
     * @return bool
     */
    public function isActionWithConditions(Action $action): bool
    {
        if (null !== $action->getConditionMinSum()
            || null !== $action->getConditionMinCount()
            || null !== $action->getConditionPaymentMethod()
            || true === $action->getConditionIsForAllElements()
            || null !== $action->getConditionMaxGifts()
        ) {
            return true;
        }

        return false;
    }

    /**
     * @param Action $action
     *
     * @return array
     */
    public function getConditionsByAction(Action $action): array
    {
        $this->conditions[$action->getId()] = ['action' => $action];
        $this->calculate($action);

        return $this->conditions[$action->getId()];
    }

    /**
     * @param Action             $action
     * @param BasketElement|null $basketElement
     */
    private function calculate(Action $action, ?BasketElement $basketElement = null): void
    {
        $actionConditions = &$this->conditions[$action->getId()];

        if ($minSum = $action->getConditionMinSum()) {
            if (null === $basketElement) {
                $actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_CURRENT] = $this->dataCollector->getData()->getTotalPrice();
                $actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_NEED] = $minSum;
                $actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_IS_COMPLETED] = false;
            } else {
                $sum = $this->priceManager->getPrice($basketElement->getElement()) * $basketElement->getAmount();

                if (isset($actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_CURRENT])) {
                    $actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_CURRENT] += $sum;
                } else {
                    $actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_CURRENT] = $sum;
                    $actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_NEED] = $minSum;
                    $actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_IS_COMPLETED] = false;
                }
            }

            if ($actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_CURRENT] >= $minSum) {
                $actionConditions[self::CONDITION_MIN_SUM][self::CONDITION_IS_COMPLETED] = true;
            }
        }

        if ($minCount = $action->getConditionMinCount()) {
            if (null === $basketElement) {
                $actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_CURRENT] = $this->dataCollector->getData()->getElementsCount();
                $actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_NEED] = $minCount;
                $actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_IS_COMPLETED] = false;
            } else {
                $count = $basketElement->getAmount();

                if (isset($actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_CURRENT])) {
                    $actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_CURRENT] += $count;
                } else {
                    $actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_CURRENT] = $count;
                    $actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_NEED] = $minCount;
                    $actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_IS_COMPLETED] = false;
                }
            }

            if ($actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_CURRENT] >= $minCount) {
                $actionConditions[self::CONDITION_MIN_COUNT][self::CONDITION_IS_COMPLETED] = true;
            }
        }

        if ($paymentMethod = $action->getConditionPaymentMethod()) {
            $actionConditions[self::CONDITION_PAYMENT_METHOD] = $paymentMethod;
        }

        $actionConditions[self::CONDITION_IS_FOR_ALL_ELEMENTS] = true === $action->getConditionIsForAllElements();

        if ($maxGifts = $action->getConditionMaxGifts()) {
            $actionConditions[self::CONDITION_MAX_GIFTS] = $maxGifts;
        }

        if (true === $action->getConditionIsForAllElements() && 1 === $action->getElements()->count()) {
            $element = $action->getElements()->first()->getElement();
            $this->conditions[$action->getId()][self::CONDITION_ACTION_ELEMENT_IN_BASKET][self::CONDITION_NEED] = $element;
            $this->conditions[$action->getId()][self::CONDITION_ACTION_ELEMENT_IN_BASKET][self::CONDITION_IS_COMPLETED] = $this->basketManager->exists($element);
        }
    }

    /**
     * @return Action[]
     */
    private function getActionsWithIsForAllElements(): array
    {
        return $this->em->getRepository(Action::class)->getActionsIsForAllElements();
    }
}
