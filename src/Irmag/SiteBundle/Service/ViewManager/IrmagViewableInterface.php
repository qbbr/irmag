<?php

namespace Irmag\SiteBundle\Service\ViewManager;

interface IrmagViewableInterface
{
    /**
     * Get views.
     *
     * @return int
     */
    public function getViews(): int;

    /**
     * Set views.
     *
     * @param int $views
     */
    public function setViews(int $views);

    /**
     * Get view object class.
     *
     * @return string
     */
    public function getViewClass(): string;
}
