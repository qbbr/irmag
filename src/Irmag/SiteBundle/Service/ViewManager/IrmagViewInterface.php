<?php

namespace Irmag\SiteBundle\Service\ViewManager;

interface IrmagViewInterface
{
    /**
     * Get view token.
     *
     * @return string
     */
    public function getToken(): ?string;

    /**
     * Set view token.
     *
     * @param string $token
     */
    public function setToken(string $token);

    /**
     * Get viewable object.
     */
    public function getObject(): ?IrmagViewableInterface;

    /**
     * Set viewable object.
     *
     * @param IrmagViewableInterface $object
     */
    public function setObject(?IrmagViewableInterface $object);

    /**
     * Get object view lifetime.
     *
     * @return int
     */
    public function getViewLifetime(): int;
}
