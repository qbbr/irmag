<?php

namespace Irmag\SiteBundle\Service\ViewManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Irmag\CoreBundle\Exception\IrmagException;

class ViewManager
{
    /**
     * @var string
     */
    private $token;

    /**
     * @var IrmagViewableInterface
     */
    private $object;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var \Symfony\Component\HttpFoundation\Request
     */
    private $request;

    /**
     * Конструктор.
     *
     * @param EntityManagerInterface $em
     * @param RequestStack           $requestStack
     */
    public function __construct(
        EntityManagerInterface $em,
        RequestStack $requestStack
    ) {
        $this->em = $em;
        $this->request = $requestStack->getMasterRequest();
    }

    /**
     * Установить объект просмотра.
     *
     * @param IrmagViewableInterface $object
     *
     * @return $this
     */
    public function setObject(IrmagViewableInterface $object): self
    {
        $this->object = $object;

        return $this;
    }

    /**
     * Зарегистрировать просмотр.
     *
     * @param string $token Токен просмотра
     */
    public function logVisitor(string $token = ''): void
    {
        $this->throwExceptionIfObjectIsNotSet();

        if (empty($token)) {
            $token = $this->getToken();
        }

        /** @var IrmagViewInterface|null $view */
        $view = $this->em->getRepository($this->object->getViewClass())->findOneBy([
            'object' => $this->object,
            'token' => $token,
        ]);

        if (null === $view) {
            $classString = $this->object->getViewClass();
            $view = new $classString();
            $view->setObject($this->object);
            $view->setToken($token);

            $this->incrementCounter();
        } else {
            $now = new \DateTime();
            $timeDiff = $view->getUpdatedAt()->diff($now, true);

            if ($timeDiff->days > 0 || 0 === $timeDiff->days && $timeDiff->h > $view->getViewLifetime()) {
                $view->setUpdatedAt(new \DateTime());
                $this->incrementCounter();
            }
        }

        $this->em->persist($view);
        $this->em->flush();
    }

    /**
     * Увеличить счётчик просмотров.
     */
    private function incrementCounter(): void
    {
        $this->throwExceptionIfObjectIsNotSet();

        $currentViews = $this->object->getViews();
        $this->object->setViews(++$currentViews);

        $this->em->persist($this->object);
        $this->em->flush();
    }

    /**
     * Выбросить исключение, если не установлен объект просмотра.
     *
     * @throws IrmagException
     */
    private function throwExceptionIfObjectIsNotSet(): void
    {
        if (empty($this->object)) {
            throw new IrmagException(sprintf('Object is not set for service "%s".', __CLASS__));
        }
    }

    /**
     * Получить токен.
     *
     * @return string
     */
    private function getToken(): string
    {
        if (empty($this->token)) {
            $this->token = $this->generateToken();
        }

        return $this->token;
    }

    /**
     * Сгенерировать токен.
     *
     * @return string
     */
    private function generateToken(): string
    {
        return md5($this->request->getClientIp().$this->request->headers->get('User-Agent'));
    }
}
