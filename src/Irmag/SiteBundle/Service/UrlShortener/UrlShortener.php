<?php

namespace Irmag\SiteBundle\Service\UrlShortener;

/**
 * Url Shortener for https://irm.ag/.
 *
 * @see https://docs.polrproject.org/en/latest/developer-guide/api/
 */
class UrlShortener
{
    private const API_KEY = '8abe4a3b6583b863f64f5c6478b821';
    private const API_URL = 'https://irm.ag/api/v2';
    private const API_ACTION_SHORTEN_PATH = '/action/shorten';

    /**
     * @param string      $url          The URL to shorten (e.g https://google.com) (e.g https://google.com)
     * @param bool        $isSecret     Whether the URL should be a secret URL or not. Defaults to false
     * @param string|null $customEnding [optional] A custom ending for the short URL. If left empty, no custom ending will be assigned.
     *
     * @return string|null
     */
    public function shorten(string $url, bool $isSecret = false, string $customEnding = null): ?string
    {
        $params = [
            'key' => self::API_KEY,
            'url' => $url,
            'is_secret' => $isSecret ? 'true' : 'false',
        ];

        if (null !== $customEnding) {
            $params['custom_ending'] = $customEnding;
        }

        $url = self::API_URL.self::API_ACTION_SHORTEN_PATH.'?'.http_build_query($params);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if (200 === $info['http_code']) {
            return $output;
        }

        return null;
    }
}
