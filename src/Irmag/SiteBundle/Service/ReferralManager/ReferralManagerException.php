<?php

namespace Irmag\SiteBundle\Service\ReferralManager;

use Irmag\CoreBundle\Exception\IrmagException;

class ReferralManagerException extends IrmagException
{
}
