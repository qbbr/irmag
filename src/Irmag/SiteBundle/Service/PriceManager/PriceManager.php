<?php

/**
 * Расчёт цены товара с учётом скидок и спец. цены.
 *
 * Скидки:
 *  - накопительная
 *  - по акции
 *  - по промокоду
 *
 * Акция:
 *  - со скидочными ценами у товара
 *  - без скидочных цен
 *
 * Логика:
 *  - Если товар в акции и у него есть акционная цена, то действует она.
 *  - Если товар в акции без скидочных цен, то идём дальше.
 *  - Если у товара есть свойство "Спец. цена", то скидки не действуют.
 *  - Если пользователь активировал промокод и скидка по нему > накопительной,
 *    то считаем скидку по промокоду.
 *  - Если у пользователя есть накопительная скидка, то действейт она.
 *  - Цена товара
 */

namespace Irmag\SiteBundle\Service\PriceManager;

use Irmag\SiteBundle\Service\PriceManager\Adapter\ActionAdapter;
use Irmag\SiteBundle\Service\PriceManager\Adapter\UserAdapter;
use Irmag\SiteBundle\Service\PriceManager\Adapter\PromocodeAdapter;
use Irmag\SiteBundle\Entity\Element;

class PriceManager
{
    /**
     * @var ActionAdapter
     */
    protected $actionAdapter;

    /**
     * @var UserAdapter
     */
    protected $userAdapter;

    /**
     * @var PromocodeAdapter
     */
    protected $promocodeAdapter;

    /**
     * @param ActionAdapter    $actionAdapter
     * @param UserAdapter      $userAdapter
     * @param PromocodeAdapter $promocodeAdapter
     */
    public function __construct(
        ActionAdapter $actionAdapter,
        UserAdapter $userAdapter,
        PromocodeAdapter $promocodeAdapter
    ) {
        $this->actionAdapter = $actionAdapter;
        $this->userAdapter = $userAdapter;
        $this->promocodeAdapter = $promocodeAdapter;
    }

    /**
     * Возвращает цену со всеми скидками с учётом цены по акции.
     *
     * @param Element $element Товар
     *
     * @return float
     */
    public function getPrice(Element $element): float
    {
        // акции
        if ($this->actionAdapter->exists($element)) {
            // цена по акции
            $price = $this->actionAdapter->getPrice($element);

            // акции бывают без цен
            if (null !== $price) {
                return $price;
            }
        }

        // специальная цена, скидки не действуют
        if (true === $element->getPropSpecialPrice()) {
            return $element->getPrice();
        }

        $promocode = $this->promocodeAdapter->getPromocode();
        $discount = false;

        // если есть промокод
        if (!empty($promocode)) {
            // если промокод на ограниченный список товаров, то возвращаем на него скидку
            if ($this->promocodeAdapter->isPromocodeForElementsList($promocode)) {
                $discount = $this->promocodeAdapter->getElementPromocodeDiscount($element);
            } else {
                // иначе, смотрим общую скидку по промокоду
                // если не null|false, значит скидка существует и она больше пользовательской
                $discount = $this->promocodeAdapter->getDiscount();
            }
        }

        if (!$discount) {
            // если по промокодам ничего не нашли, то на очереди - пользовательская скидка
            $discount = $this->userAdapter->getDiscount();
        }

        if (0 !== $discount) {
            return round($element->getPrice() - ($element->getPrice() / 100 * $discount), 2);
        }

        // обычная цена товара
        return $element->getPrice();
    }
}
