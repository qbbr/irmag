<?php

namespace Irmag\SiteBundle\Service\PriceManager\Adapter;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\SiteBundle\Entity\Promocode;
use Irmag\SiteBundle\Entity\PromocodeHistory;
use Irmag\SiteBundle\Entity\Element;
use Irmag\ProfileBundle\Service\UserTrait;

class PromocodeAdapter
{
    use UserTrait;

    const SESSION_PROMOCODE_ID = 'promocode_id';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var UserAdapter
     */
    private $userAdapter;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var int|false
     */
    private $discount;

    /**
     * @var Promocode
     */
    private $promocode;

    /**
     * @param EntityManagerInterface $em
     * @param SessionInterface       $session
     * @param TokenStorageInterface  $tokenStorage
     * @param UserAdapter            $userAdapter
     * @param UrlGeneratorInterface  $router
     */
    public function __construct(
        EntityManagerInterface $em,
        SessionInterface $session,
        TokenStorageInterface $tokenStorage,
        UserAdapter $userAdapter,
        UrlGeneratorInterface $router
    ) {
        $this->em = $em;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
        $this->userAdapter = $userAdapter;
        $this->router = $router;
    }

    /**
     * Возвращает активный промокод.
     *
     * @return Promocode|null
     */
    public function getPromocode(): ?Promocode
    {
        if ($this->promocode instanceof Promocode) {
            return $this->promocode;
        }

        if ($this->session->has(self::SESSION_PROMOCODE_ID)) {
            $this->promocode = $this->em->getRepository(Promocode::class)
                ->findOneBy(['id' => $this->session->get(self::SESSION_PROMOCODE_ID), 'isActive' => true]);

            // скидка промокода должна превышать скидку пользователя
            if ($this->promocode && $this->promocode->getDiscount() > $this->userAdapter->getDiscount()) {
                return $this->promocode;
            }

            // очищаем сессию, т.к такого промокода уже нет
            $this->removeSession();
        }

        return null;
    }

    /**
     * Возвращает скидку по промокоду.
     *
     * @return int|false|null
     */
    public function getDiscount()
    {
        if (null === $this->discount) {
            $promocode = $this->getPromocode();

            if (null !== $promocode) {
                $this->discount = $promocode->getDiscount();
            }

            // если discount по-прежнему null, то больше не ищем промокод
            if (null === $this->discount) {
                $this->discount = false;
            }
        }

        return $this->discount;
    }

    /**
     * Возвращает скидку на товар по промокоду для ограниченного списка товаров, если на указанный товар она распространяется.
     *
     * @param Element $element
     *
     * @return int|bool
     */
    public function getElementPromocodeDiscount(Element $element)
    {
        $promocode = $this->getPromocode();
        $discount = false;

        if (null !== $this->getPromocode()) {
            $elementList = $promocode->getElementList();

            // если к промокоду привязан список товаров, то ищем, входит ли указанный товар в этот список
            if (null !== $elementList) {
                $elements = $elementList->getElements();

                if (true === $elements->contains($element)) {
                    $discount = $promocode->getDiscount();
                }
            }
        }

        return $discount;
    }

    /**
     * Этот промокод действует только на ограниченный список товаров.
     *
     * @param Promocode $promocode
     *
     * @return bool
     */
    public function isPromocodeForElementsList(Promocode $promocode): bool
    {
        return !empty($promocode->getElementList());
    }

    /**
     * Удаляет сессию промокода.
     */
    public function removeSession(): void
    {
        $this->session->remove(self::SESSION_PROMOCODE_ID);
    }

    /**
     * Активирует промокод.
     *
     * @param string $code          Код
     * @param bool   $isBonusDonate Если это БОНУС ДОНАТ промокод с фантиками
     *
     * @throws PromocodeException
     *
     * @return Promocode
     */
    public function activate(string $code, bool $isBonusDonate = false): Promocode
    {
        $code = htmlentities(trim($code));

        if (empty($code)) {
            throw new PromocodeException('Промокод не указан.');
        }

        // не даём активировать более одного промокода
        if (false === $isBonusDonate && $promocodeActiveExists = $this->getPromocode()) {
            throw new PromocodeException(
                sprintf(
                    'У Вас уже активирован промокод "%s" со скидкой %d%%.',
                    $promocodeActiveExists->getCode(),
                    $promocodeActiveExists->getDiscount()
                )
            );
        }

        $user = $this->getUser();
        // не даём повторно активировать промокод
        $promocodeHistoryExists = $this->em->getRepository(PromocodeHistory::class)->findOneBy(['user' => $user, 'code' => $code]);

        if ($promocodeHistoryExists) {
            if (null !== $promocodeHistoryExists->getBonus()) {
                throw new PromocodeException(
                    sprintf(
                        'Вы уже пользовались промокодом "%s" с бонусами %d ф. Промокод одноразовый!',
                        $promocodeHistoryExists->getCode(),
                        $promocodeHistoryExists->getBonus()
                    )
                );
            }
            throw new PromocodeException(
                    sprintf(
                        'Вы уже пользовались промокодом "%s" со скидкой %d%%. Промокод одноразовый!',
                        $promocodeHistoryExists->getCode(),
                        $promocodeHistoryExists->getDiscount()
                    )
                );
        }

        $promocode = $this->em->getRepository(Promocode::class)->getByCode($code);

        if (!$promocode) {
            throw new PromocodeException(
                sprintf(
                    'Промокод "%s" не найден или уже/ещё не действителен &#9785;.',
                    $code
                )
            );
        }

        // БОНУС ДОНАТ.
        if (false === $isBonusDonate && null !== $promocode->getBonus()) {
            throw new PromocodeException(
                sprintf(
                    'Введите этот промокод в разделе <a href="%s">Мои фантики</a>.',
                    $this->router->generate('irmag_profile_bonus')
                )
            );
        }

        if (false === $isBonusDonate) {
            $userDiscount = $user->getDiscount();
            $promocodeDiscount = $promocode->getDiscount();

            if ($userDiscount >= $promocodeDiscount) {
                throw new PromocodeException(
                    sprintf(
                        'Промокод не активирован. Т.к, Ваша накопительная скидка "%d%%" больше или равна скидке по промокоду "%d%%".',
                        $userDiscount,
                        $promocodeDiscount
                    )
                );
            }
        }

        $promocode->setActivatedAt(new \DateTime());
        $this->em->persist($promocode);
        $this->em->flush();

        if (false === $isBonusDonate) {
            // запоминаем id активированного промокода в сессии.
            $this->session->set(self::SESSION_PROMOCODE_ID, $promocode->getId());
        }

        return $promocode;
    }
}
