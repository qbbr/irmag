<?php

namespace Irmag\SiteBundle\Service\FileManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Irmag\SiteBundle\Entity\File;

class FileManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $projectDir;

    /**
     * @var Finder
     */
    private $finder;

    /**
     * @var Filesystem
     */
    private $fs;

    /**
     * @param EntityManagerInterface $em
     * @param string                 $projectDir
     */
    public function __construct(
        EntityManagerInterface $em,
        string $projectDir
    ) {
        $this->em = $em;
        $this->projectDir = sprintf('%s/web/uploads/files', $projectDir);
        $this->finder = new Finder();
        $this->fs = new Filesystem();
    }

    /**
     * Возвращает массив путей до файлов.
     *
     * @param Finder $files Контейнер с файлами
     *
     * @return array
     */
    public function getFilePaths(Finder $files): array
    {
        $paths = [];

        foreach ($files as $file) {
            $paths[] = $file->getRelativePathname();
        }

        return $paths;
    }

    /**
     * Получить список файлов в директории.
     *
     * @return Finder
     */
    public function getFiles(): Finder
    {
        return $this->finder
            ->in($this->projectDir)
            ->ignoreDotFiles(true)
            ->files();
    }

    /**
     * Фильтрует файлы по путям.
     *
     * @param Finder $files      Файлы для фильтрации
     * @param array  $pathFilter Разрешенные пути
     *
     * @return Finder
     */
    public function filterFilesByPaths(Finder $files, array $pathFilter): Finder
    {
        return $files->filter(function (SplFileInfo $file) use ($pathFilter) {
            if (!\in_array($file->getRelativePathname(), $pathFilter, true)) {
                return false;
            }
        });
    }

    /**
     * Возвращает суммарный объём файлов.
     *
     * @param Finder $files Контейнер с файлами
     *
     * @return int
     */
    public function getTotalFileSize(Finder $files): int
    {
        $size = 0;

        foreach ($files as $file) {
            if (file_exists($file)) {
                $size += $file->getSize();
            }
        }

        return $size;
    }

    /**
     * Получить массив файлов, которые присутствуют в виде сущности Irmag\SiteBundle\Entity\File.
     *
     * @return array
     */
    public function getRegisteredFiles(): array
    {
        return $this->em->getRepository(File::class)->findAll();
    }

    /**
     * Удаляет файл/файлы.
     *
     * @param string|iterable $files
     */
    public function remove($files): void
    {
        $this->fs->remove($files);
    }

    /**
     * Удаляет сущности File из БД. Поиск по массиву путей.
     *
     * @param array $paths
     */
    public function clearEntitiesByPaths(array $paths): void
    {
        foreach ($paths as $path) {
            $file = $this->em->getRepository(File::class)->findOneBy(['path' => $path]);

            if ($file) {
                $this->em->remove($file);
            }
        }

        $this->em->flush();
    }
}
