<?php

namespace Irmag\SiteBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Irmag\SiteBundle\Entity\Element;

class CatalogElementViewSwitcher
{
    const NORMAL_TABLE_NAME = 'elements';
    const GROUPED_VIEW_NAME = 'elements_grouped';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;
    }

    /**
     * @param string $name
     */
    public function setGrouped(string $name): void
    {
        $this->switch($name);
    }

    /**
     * @param string $name
     */
    private function switch(string $name): void
    {
        $this->em
            ->getClassMetadata(Element::class)
            ->setPrimaryTable(['name' => $name]);
    }
}
