<?php

namespace Irmag\SiteBundle\Service\SmsSender;

use Symfony\Component\DomCrawler\Crawler;

class SmsSender
{
    private const MAX_LENGHT = 140;
    private const API_URL = 'https://api.mcommunicator.ru/m2m/m2m_api.asmx/SendMessage';

    /**
     * @var string
     */
    private $login;

    /**
     * @var string
     */
    private $password;

    /**
     * @param string $login
     * @param string $password
     */
    public function __construct(
        string $login,
        string $password
    ) {
        $this->login = $login;
        $this->password = $password;
    }

    public function send(string $telephone, string $message): ?string
    {
        $data = http_build_query([
            'msid' => $telephone,
            'message' => mb_substr($message, 0, self::MAX_LENGHT),
            'naming' => '',
            'login' => $this->login,
            'password' => $this->password,
        ]);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $output = curl_exec($ch);
        curl_close($ch);

        $crawler = new Crawler($output);
        $crawler = $crawler->filterXPath('long');

        if (0 === $crawler->count()) {
            return $output;
        }

        return $crawler->text();
    }
}
