<?php

namespace Irmag\SiteBundle\Service;

use Irmag\CoreBundle\Exception\IrmagException;

class SearchQueryNormalizerException extends IrmagException
{
}
