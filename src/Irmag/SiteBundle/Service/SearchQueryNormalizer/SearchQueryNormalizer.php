<?php

namespace Irmag\SiteBundle\Service;

class SearchQueryNormalizer
{
    /**
     * @param string $searchQuery
     *
     * @throws SearchQueryNormalizerException when $searchQuery is wrong
     *
     * @return string
     */
    public static function normilize(string $searchQuery): string
    {
        $searchQuery = trim($searchQuery);

        if (empty($searchQuery)) {
            throw new SearchQueryNormalizerException('Query not found.');
        }

        if (mb_strlen($searchQuery) < 2) {
            throw new SearchQueryNormalizerException('Query is too small.');
        }

        if (mb_strlen($searchQuery) > 255) {
            throw new SearchQueryNormalizerException('Query is too long');
        }

        return $searchQuery;
    }
}
