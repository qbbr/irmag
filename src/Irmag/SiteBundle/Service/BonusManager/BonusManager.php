<?php

namespace Irmag\SiteBundle\Service\BonusManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\SiteBundle\Event\UserBonusHistoryEvent;
use Irmag\SiteBundle\IrmagSiteEvents;
use Irmag\ProfileBundle\Service\UserTrait;
use Irmag\ProfileBundle\Entity\UserBonusHistory;

class BonusManager
{
    use UserTrait;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EntityManagerInterface   $em
     * @param TokenStorageInterface    $tokenStorage
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * Обновить кол-во бонусов у клиента.
     * Если число $value положительное,
     * то происходит начисление бонусов,
     * если отрицательное, то списание.
     *
     * @param int            $value     Кол-во фантиков
     * @param string         $message   Сообщение
     * @param \DateTime|null $createdAt Дата создания
     * @param string         $uuid1C    UUID 1C
     *
     * @throws \Exception
     *
     * @return UserBonusHistory When all right
     */
    public function update(int $value, string $message, \DateTime $createdAt = null, string $uuid1C = null): UserBonusHistory
    {
        $user = $this->getUser(true);
        $this->em->beginTransaction();

        try {
            $bonusHistory = new UserBonusHistory();
            $bonusHistory->setUser($user);
            $bonusHistory->setMessage($message);
            $bonusHistory->setValue($value);

            $currentUserBonus = $user->getBonus();

            if ($currentUserBonus + $value < 0) {
                throw new BonusManagerInvalidValueException(
                    sprintf(
                        'The final user bonus value is invalid (current: "%d", value: "%d").',
                        $currentUserBonus,
                        $value
                    )
                );
            }

            if (null !== $uuid1C) {
                $bonusHistory->setUuid1C($uuid1C);
            }

            if (null !== $createdAt) {
                $bonusHistory->setCreatedAt($createdAt);
            }

            $user->setBonus($user->getBonus() + $value);
            $this->em->persist($user);
            $this->em->persist($bonusHistory);
            $this->em->flush();
            $this->em->commit();
        } catch (\Exception $e) {
            $this->em->rollBack();
            throw $e;
        }

        $this->eventDispatcher->dispatch(IrmagSiteEvents::BONUS_SEND, new UserBonusHistoryEvent($bonusHistory));

        return $bonusHistory;
    }
}
