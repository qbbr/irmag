<?php

namespace Irmag\SiteBundle\Service\OrderManager;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Irmag\CoreBundle\Utils\PropertyFiller;
use Irmag\ProfileBundle\Entity\UserOrderProfile;
use Irmag\ProfileBundle\Service\UserTrait;
use Irmag\ProfileBundle\Service\UserStateManager\UserStateManager;
use Irmag\BasketBundle\Service\DataCollector;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Entity\OrderElement;
use Irmag\SiteBundle\Entity\OrderDeliveryCity;
use Irmag\SiteBundle\Entity\OrderDeliveryMethod;
use Irmag\SiteBundle\Entity\OrderDeliveryTime;
use Irmag\SiteBundle\Entity\OrderStatus;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\SiteBundle\Service\PriceManager\PriceManager;
use Irmag\SiteBundle\Service\BonusManager\BonusManager;
use Irmag\SiteBundle\Utils\DeviceDetector;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;
use Irmag\OrderDeliveryServiceBundle\Service\OrderDeliveryServiceManager;
use Irmag\OrderPostDeliveryBundle\Service\OrderPostDeliveryManager;

class OrderManager
{
    use UserTrait;

    const ORDER_FLASH_NOT_SELECTED_REMOTE_DELIVERY_DATA_LITERAL = 'order_remote_delivery_error_flash';
    const ORDER_FLASH_NOT_SELECTED_REMOTE_DELIVERY_DATA_MESSAGE = 'Вы не выполнили расчёт удалённой доставки!';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var BasketManager
     */
    private $basketManager;

    /**
     * @var DataCollector
     */
    private $dataCollector;

    /**
     * @var PriceManager
     */
    private $priceManager;

    /**
     * @var BonusManager
     */
    private $bonusManager;

    /**
     * @var UserStateManager
     */
    private $userStateManager;

    /**
     * @var PropertyFiller
     */
    private $propertyFiller;

    /**
     * @var OrderDeliveryServiceManager
     */
    private $deliveryServiceManager;

    /**
     * @var OrderPostDeliveryManager
     */
    private $postDeliveryManager;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var BasketToOrderDataTransformer
     */
    private $basketToOrderDataTransformer;

    /**
     * @var Order
     */
    private $order;

    /**
     * @var array Поля для заполнения
     */
    private $fieldsToFill = [
        'payerUsername', 'payerTelephone',
        // Адрес доставки
        'deliveryCity', 'deliveryDistrict', 'deliveryStreet', 'deliveryHouse', 'deliveryApartment', 'deliveryComment',
        // Реквизиты компании
        'payerLegalAddress', 'payerLegalActualAddress', 'payerLegalConsigneeAddress', 'payerLegalName',
        'payerLegalInn', 'payerLegalKpp', 'payerLegalOgrn', 'payerLegalBik', 'payerLegalRs',
        'payerLegalKrs', 'payerLegalRb', 'payerLegalRbCity', 'payerLegalTelephone', 'payerLegalEmail',
    ];

    /**
     * @param EntityManagerInterface       $em
     * @param TokenStorageInterface        $tokenStorage
     * @param BasketManager                $basketManager
     * @param DataCollector                $dataCollector
     * @param PriceManager                 $priceManager
     * @param BonusManager                 $bonusManager
     * @param UserStateManager             $userStateManager
     * @param PropertyFiller               $propertyFiller
     * @param OrderDeliveryServiceManager  $deliveryServiceManager
     * @param OrderPostDeliveryManager     $postDeliveryManager
     * @param SessionInterface             $session
     * @param BasketToOrderDataTransformer $basketToOrderDataTransformer
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        BasketManager $basketManager,
        DataCollector $dataCollector,
        PriceManager $priceManager,
        BonusManager $bonusManager,
        UserStateManager $userStateManager,
        PropertyFiller $propertyFiller,
        OrderDeliveryServiceManager $deliveryServiceManager,
        OrderPostDeliveryManager $postDeliveryManager,
        SessionInterface $session,
        BasketToOrderDataTransformer $basketToOrderDataTransformer
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->basketManager = $basketManager;
        $this->dataCollector = $dataCollector;
        $this->priceManager = $priceManager;
        $this->bonusManager = $bonusManager;
        $this->userStateManager = $userStateManager;
        $this->propertyFiller = $propertyFiller;
        $this->deliveryServiceManager = $deliveryServiceManager;
        $this->postDeliveryManager = $postDeliveryManager;
        $this->session = $session;
        $this->basketToOrderDataTransformer = $basketToOrderDataTransformer;
    }

    /**
     * Создаёт новый заказ.
     *
     * @return Order
     */
    public function newOrder(): Order
    {
        $this->order = new Order();
        // Статус "Принят" по-умолчанию
        $this->setStatusByShortname('N');
        // Город доставки по-умолчанию
        $this->setDeliveryCityByShortname('irkutsk');
        // Плательщик
        $user = $this->getUser();

        $this->order->setUser($user);
        $this->order->setPayerUsername($user->getFullname());
        $this->order->setPayerTelephone($user->getTelephone());
        $this->order->setPayerEmail($user->getEmail());

        return $this->order;
    }

    /**
     * Назначает заказ.
     *
     * @param int  $id
     * @param bool $checkUser
     *
     * @return OrderManager
     */
    public function setOrderById(int $id, bool $checkUser = true): self
    {
        $criteria = ['id' => $id];

        if (true === $checkUser) {
            $criteria['user'] = $this->getUser();
        }

        $order = $this->em->getRepository(Order::class)->findOneBy($criteria);

        if (!$order) {
            throw new NotFoundHttpException(sprintf('Order with id "%d" not found.', $id));
        }

        $this->order = $order;

        return $this;
    }

    /**
     * Возвращает заказ по id или текущий.
     *
     * @param int $id Ид заказа
     *
     * @return Order
     */
    public function getOrder(int $id = null): Order
    {
        if (null !== $id) {
            $user = $this->getUser();
            $this->order = $this->em->getRepository(Order::class)->findOneBy(['id' => $id, 'user' => $user]);

            if (!$this->order) {
                throw new NotFoundHttpException(sprintf('Order with id "%d" for user "%d" not found.', $id, $user->getId()));
            }
        }

        return $this->order;
    }

    /**
     * Сохраняет заказ.
     */
    public function save(): void
    {
        $this->em->persist($this->order);
        $this->em->flush();
    }

    /**
     * Устанавливает статус заказа по короткому имени.
     *
     * @param string $shortname Короткое имя
     */
    public function setStatusByShortname(string $shortname): void
    {
        $status = $this->em->getRepository(OrderStatus::class)->findOneBy(['shortname' => $shortname]);

        if (!$status) {
            throw new NotFoundHttpException(sprintf('OrderStatus with shortname "%s" not found.', $shortname));
        }

        $this->order->setStatus($status);
    }

    /**
     * Устанавливает город доставки по короткому имени.
     *
     * @param string $shortname Короткое имя
     */
    public function setDeliveryCityByShortname(string $shortname): void
    {
        $deliveryCity = $this->em->getRepository(OrderDeliveryCity::class)->findOneBy(['shortname' => $shortname, 'isActive' => true]);

        if (!$deliveryCity) {
            throw new NotFoundHttpException(sprintf('OrderDeliveryCity with shortname "%s" not found.', $shortname));
        }

        $this->order->setDeliveryCity($deliveryCity);
    }

    /**
     * Возвращает основной профиль заказа пользователя.
     *
     * @return UserOrderProfile|null
     */
    public function getUserOrderProfilePrimary(): ?UserOrderProfile
    {
        return $this->em->getRepository(UserOrderProfile::class)->getPrimary($this->getUser());
    }

    /**
     * Заполняет поля заказа из полей профиля заказа пользователя.
     *
     * @param UserOrderProfile $userOrderProfile
     */
    public function fillOrderFieldsFromUserOrderProfile(UserOrderProfile $userOrderProfile): void
    {
        $this->propertyFiller->fill($userOrderProfile, $this->order, $this->fieldsToFill);

        if ($userOrderProfile->getPayerLegalAddress()) {
            $this->order->setIsPayerLegal(true);
        }
    }

    /**
     * Проводит заказ.
     */
    public function checkout(): void
    {
        $this->em->getConnection()->beginTransaction();

        try {
            // У физ.лица обнуляем юр.данные
            if (!$this->order->getIsPayerLegal()) {
                $this->order->nullableLegalData();
            }

            // Заполняем поля профиля, если пустые
            $this->fillUserFieldsFromOrderIfEmpty();
            // Заполняем данные заказа из корзины
            $this->basketToOrderDataTransformer->transform($this->order);
            // Заполняем данные о доставке
            $this->fillDeliveryFields();
            // Очищаем корзину
            $this->basketManager->clear(true);
            // Если нет UserOrderProfile, то создаём и заполняем поля из заказа
            $this->createUserOrderProfileFromOrderIfNotExists();
            // Определяем устройство
            $this->order->setDevice(DeviceDetector::getDevice());

            // Сохраняем данные по смешанным оплатам если есть
            if (!empty($this->order->getMixedPayment())) {
                $mixedPayment = $this->order->getMixedPayment();
                $mixedPayment->setOrder($this->order);
                $this->em->persist($mixedPayment);
            }

            // Сохраняем заказ
            $this->save();
            // Если пользователь оплатил фантиками, то списываем
            if ($this->order->getSpendBonus()) {
                $this->bonusManager->update(-$this->order->getSpendBonus(), sprintf('Оплата заказа "%d"', $this->order->getId()));
            }

            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            throw $e;
        }
    }

    /**
     * Отменяет заказа.
     */
    public function cancel(): void
    {
        $this->em->getConnection()->beginTransaction();

        try {
            $this->setStatusByShortname('C');
            $orderElements = $this->order->getOrderElement();
            $user = $this->order->getUser();

            // возврат товаров в корзину
            /** @var OrderElement $orderElement */
            foreach ($orderElements as $orderElement) {
                if ($orderElement->getElement()->getIsActive()) {
                    $this->basketManager->addElementToUsersBasket($user, $orderElement->getElement(), $orderElement->getAmount());
                }
            }

            // возврат фантиков
            if (!empty($this->order->getSpendBonus())) {
                $this
                    ->bonusManager
                    ->setUser($user)
                    ->update($this->order->getSpendBonus(), sprintf('Возврат фантиков за отмену заказа №%d', $this->order->getId()));
            }

            $this->userStateManager->setUser($user);
            $this->userStateManager->remove(OnlinePaymentManager::USER_STATE_UNPAYED_ORDER_KEY);
            $this->userStateManager->remove(OnlinePaymentManager::USER_STATE_BANK_RESPONSE_KEY);
            $this->userStateManager->remove(OnlinePaymentManager::USER_STATE_ORDER_TO_FINISH);

            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            throw $e;
        }
    }

    /**
     * Заполняет адрес доставки из сессии.
     */
    public function fillOrderAddressFromSession(): void
    {
        $selectedDeliveryServiceData = $this->deliveryServiceManager->getSelectedData();
        $isSelectedPostDelivery = $this->postDeliveryManager->getIsSelected();

        if (!empty($selectedDeliveryServiceData) || true === $isSelectedPostDelivery) {
            $this->setDeliveryCityByShortname('other');
            $shortname = !empty($selectedDeliveryServiceData) ? 'transport_company' : 'russian_post';

            $deliveryMethod = $this->em->getRepository(OrderDeliveryMethod::class)->findOneBy(['shortname' => $shortname, 'isActive' => true]);
            $deliveryCity = $this->order->getDeliveryCity();

            if (!$deliveryMethod) {
                throw new NotFoundHttpException(sprintf('OrderDeliveryMethod with shortname "%s" not found.', $shortname));
            }

            if (true === $deliveryCity->getDeliveryMethod()->contains($deliveryMethod)) {
                $this->order->setDeliveryMethod($deliveryMethod);
            }

            if (!empty($selectedDeliveryServiceData['deliveryPointAddressFormatted'])) {
                $deliveryPointAddressChunks = explode(', ', trim($selectedDeliveryServiceData['deliveryPointAddressFormatted']));
                $this->order->setDeliveryStreet($deliveryPointAddressChunks[2]);
                $this->order->setDeliveryHouse($deliveryPointAddressChunks[3]);
            }
        }
    }

    /**
     * Заполнить дату доставки с учётом закрытых дней, если выбрана транспортная компания или Почта России.
     */
    public function fillOrderDeliveryDateForFarDelivery(): void
    {
        $minDaysForRemoteDelivery = $this->getMinDaysForSelectedRemoteDelivery();

        if (!empty($minDaysForRemoteDelivery)) {
            $deliveryCity = $this->order->getDeliveryCity();
            $deliveryMethod = $this->order->getDeliveryMethod();

            if (!empty($deliveryCity)
                && 'other' === $deliveryCity->getShortname()
                && !empty($deliveryMethod)
                && \in_array($deliveryMethod->getShortname(), ['transport_company', 'russian_post'], true)
            ) {
                $deliveryDate = new \DateTime();

                // считаем предварительную дату доставки, исключая выходные
                for ($i = 1; $i <= $minDaysForRemoteDelivery; ++$i) {
                    $deliveryDate->modify('+1 day');

                    // 6 - суббота
                    if ($deliveryDate->format('w') >= 6) {
                        $deliveryDate->modify('+'.(8 - (int) $deliveryDate->format('w')).' days');
                    }
                }

                /* Доставка в ближайший разрешённый день недели, но не ранее чем (текущая дата + срок доставки) */
                $allowedDaysOfWeek = [];
                $dayEntities = [];

                foreach ($deliveryCity->getDeliveryCityDayOfWeek() as $day) {
                    $allowedDaysOfWeek[] = $day->getDayOfWeek();
                    $dayEntities[$day->getDayOfWeek()] = $day;
                }

                while (!\in_array((int) $deliveryDate->format('w'), $allowedDaysOfWeek, true)) {
                    $deliveryDate->modify('+1 day');
                }

                $this->order->setDeliveryDate($deliveryDate);

                if (!empty($dayEntities[$deliveryDate->format('w')])) {
                    $selectedDayOfWeek = $dayEntities[$deliveryDate->format('w')];

                    $anyTime = $this->em->getRepository(OrderDeliveryTime::class)->findOneBy(['name' => 'В любое время']);

                    if ($anyTime && $selectedDayOfWeek->getDeliveryTime()->contains($anyTime)) {
                        $this->order->setDeliveryTime($anyTime);
                    }
                }
            }
        }
    }

    /**
     * Заполнить данными удалённой доставки.
     */
    public function setOrderDataFromSession(): void
    {
        $this->fillOrderAddressFromSession();
        $this->fillOrderDeliveryDateForFarDelivery();
    }

    /**
     * Заполнить данные по смешанному платежу.
     *
     * @param Order $order Заказ
     */
    public function setOrderMixedPayment(Order $order): void
    {
        if ('mixed' === $order->getPaymentMethod()->getShortname() && !empty($order->getMixedPayment())) {
            $mixedPayment = $order->getMixedPayment();
            $mixedPayment->setOrder($order);

            $this->em->persist($mixedPayment);
            $this->em->flush();
        }
    }

    /**
     * Рассчитана ли стоимость доставки, если выбрана удалённая доставка?
     *
     * @return bool
     */
    public function isRemoteDeliveryDataSelected(): bool
    {
        $selectedDeliveryMethod = $this->order->getDeliveryMethod();
        $isDataSelected = true;

        if (!empty($selectedDeliveryMethod)) {
            $shortname = $selectedDeliveryMethod->getShortname();

            if ('transport_company' === $shortname) {
                $data = $this->deliveryServiceManager->getSelectedData();
                $isDataSelected = (!empty($data['serviceName'])) ? true : false;
            } elseif ('russian_post' === $shortname) {
                $isDataSelected = $this->postDeliveryManager->getIsSelected();
            }
        }

        if (false === $isDataSelected) {
            $this->session->getFlashBag()->add(self::ORDER_FLASH_NOT_SELECTED_REMOTE_DELIVERY_DATA_LITERAL, self::ORDER_FLASH_NOT_SELECTED_REMOTE_DELIVERY_DATA_MESSAGE);
        }

        return $isDataSelected;
    }

    /**
     * Заполняет поля пользователя, если их нет, из полей заказа.
     */
    private function fillUserFieldsFromOrderIfEmpty(): void
    {
        $user = $this->getUser();

        if (empty($user->getFullname())) {
            $user->setFullname($this->order->getPayerUsername());
        }

        if (empty($user->getTelephone())) {
            $user->setTelephone($this->order->getPayerTelephone());
        }
    }

    /**
     * Заполняет поля доставки у заказа.
     */
    private function fillDeliveryFields(): void
    {
        $deliveryMethodShortname = $this->order->getDeliveryMethod()->getShortname();
        $deliveryTime = $this->order->getDeliveryTime();

        // самовывозы только из Иркутска
        if ('selfservice' === $deliveryMethodShortname) {
            // $this->setDeliveryCityByShortname('irkutsk');

            // nullable
            if (null !== $this->order->getSelfserviceMethod()) {
                $this->order->setDeliveryStreet();
                $this->order->setDeliveryHouse();
                $this->order->setDeliveryApartment();
            }
        }

        // стоимость доставки, для всего кроме самовывоза, транспортной компанией и почтой россии
        if (false === \in_array($deliveryMethodShortname, ['selfservice', 'transport_company', 'russian_post'], true)) {
            $deliveryCity = $this->getOrder()->getDeliveryCity();
            $basketData = $this->dataCollector->getData();

            if ($deliveryCity->getMinPriceForFreeDelivery() && $deliveryCity->getMinPriceForFreeDelivery() > $basketData->getTotalPrice()) {
                $this->order->setDeliveryPrice($deliveryCity->getDeliveryPrice());
            }
        }

        // стоимость доставки для срочной доставки (VIP курьер)
        if (null !== $deliveryTime && 'fast' === $deliveryTime->getShortname()) {
            $deliveryCity = $this->getOrder()->getDeliveryCity();

            if ($this->dataCollector->getData()->getTotalPrice() >= $deliveryCity->getMinPriceForFreeDelivery()) {
                $deliveryPrice = $deliveryCity->getExtraPriceForFastDeliveryTime();
            } else {
                $deliveryPrice = $deliveryCity->getDeliveryPrice() + $deliveryCity->getExtraPriceForFastDeliveryTime();
            }

            $this->order->setDeliveryPrice($deliveryPrice);
        }

        if ('russian_post' === $deliveryMethodShortname) {
            $postDeliveryResult = $this->postDeliveryManager->getResult();

            if (!empty($postDeliveryResult['price'])) {
                $this->order->setDeliveryPrice($postDeliveryResult['price']);
                $this->order->setIsRemoteDeliveryCompleted(false);
            }
        }

        if ('transport_company' === $deliveryMethodShortname) {
            $remoteDeliveryData = $this->deliveryServiceManager->getSelectedData();

            if (!empty($remoteDeliveryData['cost'])) {
                $this->order->setDeliveryPrice(round($remoteDeliveryData['cost']));
                $this->order->setIsRemoteDeliveryCompleted(false);
            }
        }
    }

    /**
     * Создаёт основной профиль заказа у пользователя, если его нет,
     * и заполняет его поля из полей заказа.
     *
     * @return bool
     */
    private function createUserOrderProfileFromOrderIfNotExists(): bool
    {
        if ($this->getUserOrderProfilePrimary()) {
            return false;
        }

        // Создаём профиль заказа
        $userOrderProfile = new UserOrderProfile();
        $userOrderProfile->setName(sprintf('Создан автоматически (от %s)', (new \DateTime())->format('H:i:s d.m.Y')));
        $userOrderProfile->setUser($this->getUser());
        $userOrderProfile->setIsPrimary(true);
        // Заполняем поля
        $this->propertyFiller->fill($this->order, $userOrderProfile, $this->fieldsToFill);
        $this->em->persist($userOrderProfile);

        return true;
    }

    /**
     * Возвращает минимальное кол-во дней для удаленной доставки транспортной компанией или почтой.
     *
     * @return int|null
     */
    private function getMinDaysForSelectedRemoteDelivery(): ?int
    {
        $days = null;

        $transportCompanyData = $this->deliveryServiceManager->getSelectedData();
        $postDeliveryData = (true === $this->postDeliveryManager->getIsSelected()) ? $this->postDeliveryManager->getResult() : null;

        if (!empty($transportCompanyData) && !empty($transportCompanyData['days'])) {
            $days = (int) $transportCompanyData['days'];
        } elseif (!empty($postDeliveryData)) {
            $days = (!empty($postDeliveryData['minDays'])) ? $postDeliveryData['minDays'] :
                ((!empty($postDeliveryData['maxDays'])) ? $postDeliveryData['maxDays'] : 0);
        }

        return $days;
    }
}
