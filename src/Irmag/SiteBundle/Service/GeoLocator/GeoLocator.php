<?php

namespace Irmag\SiteBundle\Service\GeoLocator;

use Buzz\Browser;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryService;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceCity;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryServiceRegion;
use Irmag\OrderPostDeliveryBundle\Service\OrderPostDeliveryKladrService;

class GeoLocator
{
    /**
     * Адрес API для определения геолокации по IP.
     */
    const GEOLOCATOR_API_HOST = 'https://api.sypexgeo.net/';

    const DEFAULT_COUNTRY = 'RU';
    const DEFAULT_CITY = 'Иркутск';

    /**
     * Города, которые мы считаем "местными".
     */
    const NEAR_CITIES = [
        'Иркутск',
        'Ангарск',
        'Шелехов',
    ];

    const SESSION_CURRENT_POSITION_LITERAL = 'irmag.site.current_geo';
    const SESSION_CITY_PREDICTION_CONFIRMED_LITERAL = 'irmag.site.city_prediction.confirmed';
    const SESSION_PREDICTION_POST_INDEX = 'irmag.site.prediction_post_index';

    /**
     * @var Browser
     */
    private $buzzBrowser;

    /**
     * @var OrderPostDeliveryKladrService
     */
    private $kladr;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @param OrderPostDeliveryKladrService $kladr
     * @param EntityManagerInterface        $em
     * @param SessionInterface              $session
     */
    public function __construct(
        OrderPostDeliveryKladrService $kladr,
        EntityManagerInterface $em,
        SessionInterface $session
    ) {
        $this->buzzBrowser = new Browser();
        $this->kladr = $kladr;
        $this->em = $em;
        $this->session = $session;
    }

    /**
     * Определяет, находится ли клиент в другом городе РФ.
     *
     * @return bool
     */
    public function isAnotherCity(): bool
    {
        return !\in_array($this->getCurrentCity(), self::NEAR_CITIES, true);
    }

    /**
     * Получить код текущего государства.
     *
     * @return string
     */
    public function getCurrentCountry(): string
    {
        $data = $this->getData();

        return $data['country']['iso'] ?? self::DEFAULT_COUNTRY;
    }

    /**
     * Получить город для транспортной компании.
     *
     * @param OrderDeliveryService $service
     *
     * @return OrderDeliveryServiceCity|null
     */
    public function getCurrentOrderDeliveryServiceCity(OrderDeliveryService $service): ?OrderDeliveryServiceCity
    {
        $data = $this->getData();

        if (!empty($data)) {
            $ids = explode(',', $data['region']['auto']);
            $regionId = (int) $ids[0];
            $region = $this->em->getRepository(OrderDeliveryServiceRegion::class)->find($regionId);

            if ($region) {
                $cityName = $data['city']['name_ru'];
                /** @var OrderDeliveryServiceCity|null $city */
                $city = $this->em->getRepository(OrderDeliveryServiceCity::class)->findOneBy([
                    'orderDeliveryServiceRegion' => $region,
                    'orderDeliveryService' => $service,
                    'name' => $cityName,
                ]);
            }
        }

        return $city ?? null;
    }

    /**
     * Получить текущий город (по умолчанию - Иркутск).
     *
     * @return string
     */
    public function getCurrentCity(): string
    {
        $data = $this->getData();

        return $data['city']['name_ru'] ?? self::DEFAULT_CITY;
    }

    /**
     * Получить индекс предполагаемого города доставки.
     *
     * @return int|null
     */
    public function getCurrentMainIndex(): ?int
    {
        $zipCode = $this->session->get(self::SESSION_PREDICTION_POST_INDEX, null);

        if (empty($zipCode)) {
            $data = $this->getData();

            if (!empty($data['city']['name_ru']) && !empty($data['city']['okato'])) {
                $zipCode = $this->kladr->getMainIndexByCityNameAndOkato($data['city']['name_ru'], $data['city']['okato']);
            }
        }

        return $zipCode;
    }

    /**
     * Получить данные геопозиции из сессии или запросить у сервиса.
     *
     * @return array
     */
    public function getData(): array
    {
        if (!$this->session->has(self::SESSION_CURRENT_POSITION_LITERAL)) {
            $this->session->set(self::SESSION_CURRENT_POSITION_LITERAL, $this->prepareData($this->getGeoLocatorData()));
        }

        return $this->session->get(self::SESSION_CURRENT_POSITION_LITERAL);
    }

    /**
     * Вернуть гео данные для текущего IP.
     *
     * @return array
     */
    private function getGeoLocatorData(): array
    {
        try {
            $response = $this->buzzBrowser->get(self::GEOLOCATOR_API_HOST)->getBody();
            $jsonResponse = json_decode((string) $response, \JSON_OBJECT_AS_ARRAY);
            $data = !empty($jsonResponse['error']) ? [] : $jsonResponse;
        } catch (\Exception $ex) {
            $data = [];
        }

        return $data;
    }

    /**
     * Оставить только нужные поля.
     *
     * @param array $data
     *
     * @return array
     */
    private function prepareData(array $data): array
    {
        $filteredData = [];

        if (!empty($data['city'])) {
            $filteredData['city']['okato'] = $data['city']['okato'];
            $filteredData['city']['name_ru'] = $data['city']['name_ru'];
        }

        if (!empty($data['country'])) {
            $filteredData['country']['iso'] = $data['country']['iso'];
        }

        if (!empty($data['region'])) {
            $filteredData['region']['auto'] = $data['region']['auto'];
        }

        return $filteredData;
    }
}
