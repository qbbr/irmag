<?php

namespace Irmag\SiteBundle\Service\BasketFavoriteManager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Irmag\CoreBundle\Utils\TokenGenerator;
use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\PromocodeElementsList;
use Irmag\SiteBundle\Service\CatalogElementViewSwitcher;
use Irmag\FavoriteBundle\Entity\FavoriteElement;
use Irmag\FavoriteBundle\Entity\FavoriteShare;
use Irmag\FavoriteBundle\Entity\FavoriteShareElement;
use Irmag\FavoriteBundle\Entity\Favorite;

/**
 * Манипуляции с избранным.
 * Избранное с название доступно только аутентифицированным пользователям,
 * по-умолчанию название избранного self::DEFAULT_FAVORITE_NAME.
 */
class FavoriteManager
{
    use BasketFavoriteTrait;

    const DEFAULT_FAVORITE_NAME = 'default';

    /**
     * @var array
     */
    private $cache = [];

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     * @param RequestStack           $requestStack
     * @param RouterInterface        $router
     * @param ContainerInterface     $container
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        RequestStack $requestStack,
        RouterInterface $router,
        ContainerInterface $container
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->requestStack = $requestStack;
        $this->router = $router;
        $this->container = $container;
    }

    /**
     * Создаёт новое избранное.
     *
     * @param string $favoriteName Название избранного [optional]
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function create(string $favoriteName = null): bool
    {
        try {
            $this->getFavorite($favoriteName, true);
        } catch (\Exception $e) {
            throw $e;
        }

        return true;
    }

    /**
     * Слияние временного избранного (default) с аутентифицированным (default).
     * После слияния, временное избранное удаляется со всеми его товарами.
     *
     * @return bool
     */
    public function merge(): bool
    {
        if (false === $this->isAuthenticated()) {
            return false;
        }

        $favoriteTmpSession = $this->getTmpFavorite();

        if (!$favoriteTmpSession) {
            return false;
        }

        /** @var FavoriteElement $favoriteElement */
        foreach ($favoriteTmpSession->getFavoriteElements() as $favoriteElement) {
            $this->addElement($favoriteElement->getElement());
        }

        $this->em->remove($favoriteTmpSession);
        $this->em->flush();

        return true;
    }

    /**
     * Очищает избранное (удаляет все товары).
     *
     * @param Favorite $favorite Избранное
     *
     * @return bool
     */
    public function clear(Favorite $favorite): bool
    {
        $this->denyAccessUnlessOwner($favorite);

        $favorite->getFavoriteElements()->clear();
        $favorite->setUpdatedAtNow();
        $this->em->persist($favorite);
        $this->em->flush();

        return true;
    }

    /**
     * Переименовывает избранное.
     *
     * @param Favorite $favorite     Избранное
     * @param string   $favoriteName Название избранного
     *
     * @return bool
     */
    public function rename(Favorite $favorite, $favoriteName): bool
    {
        if (empty($favoriteName)
            || self::DEFAULT_FAVORITE_NAME === $favoriteName
            || self::DEFAULT_FAVORITE_NAME === $favorite->getName()
            || false === $this->isAuthenticated()
        ) {
            return false;
        }

        $this->denyAccessUnlessOwner($favorite);

        $favorite->setName($favoriteName);
        $this->em->persist($favorite);
        $this->em->flush();

        return true;
    }

    /**
     * Удаляет избранное со всеми его товарами.
     *
     * @param Favorite $favorite Избранное
     *
     * @return bool
     */
    public function remove(Favorite $favorite): bool
    {
        $this->denyAccessUnlessOwner($favorite);

        $this->em->remove($favorite);
        $this->em->flush();

        return true;
    }

    /**
     * Добавляет товар в избранное.
     *
     * @param Element $element      Товар
     * @param string  $favoriteName Название избранного [optional]
     *
     * @return bool
     */
    public function addElement(Element $element, $favoriteName = null): bool
    {
        $favorite = $this->getFavorite($favoriteName, true);
        $favoriteElement = $this->em->getRepository(FavoriteElement::class)
            ->findOneBy(['favorite' => $favorite, 'element' => $element]);

        // exists
        if ($favoriteElement) {
            return false;
        }

        $favoriteElement = new FavoriteElement();
        $favoriteElement->setFavorite($favorite);
        $favoriteElement->setElement($element);

        $favorite->setUpdatedAtNow();
        $this->em->persist($favorite);
        $this->em->persist($favoriteElement);
        $this->em->flush();

        return true;
    }

    /**
     * Удаляет товар из избранного.
     *
     * @param Element $element      Товар
     * @param string  $favoriteName Название избранного [optional]
     *
     * @return bool
     */
    public function removeElement(Element $element, $favoriteName = null): bool
    {
        $favorite = $this->getFavorite($favoriteName, true);
        $favoriteElement = $this->em->getRepository(FavoriteElement::class)
            ->findOneBy(['favorite' => $favorite, 'element' => $element]);

        if ($favoriteElement) {
            $favorite->setUpdatedAtNow();
            $this->em->remove($favoriteElement);
            $this->em->persist($favorite);
            $this->em->flush();

            return true;
        }

        return false;
    }

    /**
     * Перемещает товар из одного избранного в другое.
     *
     * @param Favorite $favoriteFrom Старое избранное
     * @param Favorite $favoriteTo   Новое избранное
     * @param Element  $element      Товар
     *
     * @return bool
     */
    public function moveElementToFavoriteFromFavorite(Favorite $favoriteFrom, Favorite $favoriteTo, Element $element): bool
    {
        if (false === $this->isAuthenticated()) {
            return false;
        }

        $this->denyAccessUnlessOwner($favoriteFrom);
        $this->denyAccessUnlessOwner($favoriteTo);

        $favoriteElement = $this->em->getRepository(FavoriteElement::class)
            ->findOneBy(['favorite' => $favoriteFrom, 'element' => $element]);
        $favoriteElement->setFavorite($favoriteTo);
        $favoriteElement->setUpdatedAt(new \DateTime());

        $favoriteTo->setUpdatedAtNow();
        $favoriteFrom->setUpdatedAtNow();
        $this->em->persist($favoriteTo);
        $this->em->persist($favoriteFrom);
        $this->em->flush();

        return true;
    }

    /**
     * Копирует все товары в корзину.
     *
     * @param Favorite $favorite Избранное
     *
     * @return bool
     */
    public function copyAllElementsToBasket(Favorite $favorite): bool
    {
        $this->denyAccessUnlessOwner($favorite);

        /** @var FavoriteElement $favoriteElement */
        foreach ($favorite->getFavoriteElements() as $favoriteElement) {
            $element = $favoriteElement->getElement();

            if ($element->getIsActive()) {
                $this->container->get(BasketManager::class)->addElement($element);
            }
        }

        return true;
    }

    /**
     * Возвращает QueryBuilder всех избранных.
     *
     * @return QueryBuilder
     */
    public function getFavoritesQb(): QueryBuilder
    {
        if ($this->isAuthenticated()) {
            return $this->em->getRepository(Favorite::class)->getAllByUserQb($this->getUser());
        }

        return $this->em->getRepository(Favorite::class)->getAllByTmpSessionQb($this->getTmpSession());
    }

    /**
     * Возвращает все избранные, если есть.
     *
     * @return Favorite[]
     */
    public function getFavorites(): array
    {
        if (isset($this->cache['favorites'])) {
            return $this->cache['favorites'];
        }

        $this->cache['favorites'] = $this->getFavoritesQb()->getQuery()->getResult() ?? [];

        /** @var Favorite $favorite */
        foreach ($this->cache['favorites'] as $key => $favorite) {
            if (Favorite::WATCHING_FAVORITE_NAME === $favorite->getName()) {
                unset($this->cache['favorites'][$key]);
            }
        }

        return $this->cache['favorites'];
    }

    /**
     * Возвращает избранное.
     *
     * @param string $favoriteName     Название избранного [optional]
     * @param bool   $createIfNotExist Создать избранное, если нет [optional]
     *
     * @return Favorite
     */
    public function getFavorite(string $favoriteName = null, bool $createIfNotExist = false)
    {
        // force not grouped
        $this->container->get(CatalogElementViewSwitcher::class)->setGrouped(CatalogElementViewSwitcher::NORMAL_TABLE_NAME);

        if ($this->isAuthenticated()) {
            return $this->getAuthenticatedFavorite($favoriteName, $createIfNotExist);
        }

        return $this->getTmpFavorite($createIfNotExist);
    }

    /**
     * Возвращает кол-во товара в избранных.
     *
     * @return int
     */
    public function getTotalCount(): int
    {
        if (isset($this->cache['total_count'])) {
            return $this->cache['total_count'];
        }

        // force not grouped
        $this->container->get(CatalogElementViewSwitcher::class)->setGrouped(CatalogElementViewSwitcher::NORMAL_TABLE_NAME);

        $qb = $this->em->createQueryBuilder()
            ->select('COUNT(e.id)')
            ->from(Favorite::class, 'f')
            ->leftJoin('f.favoriteElements', 'fe')
            ->leftJoin('fe.element', 'e');

        if ($this->isAuthenticated()) {
            $qb->where('f.user = :user')
                ->setParameter('user', $this->getUser());
        } else {
            $qb->where('f.tmpSession = :tmpSession')
                ->setParameter('tmpSession', $this->getTmpSession());
        }

        $this->cache['total_count'] = (int) $qb->getQuery()->getSingleScalarResult() ?? 0;

        return $this->cache['total_count'];
    }

    /**
     * @example
     * [
     *     elementId => [
     *         'favorites' => Favorite[]
     *         'element' => Element
     *     ]
     * ]
     *
     * @return Element[]
     */
    public function getElements(): array
    {
        if (isset($this->cache['elements'])) {
            return $this->cache['elements'];
        }

        $elements = [];

        foreach ($this->getFavorites() as $favorite) {
            $favoriteElements = $favorite->getFavoriteElements();

            /** @var FavoriteElement $favoriteElement */
            foreach ($favoriteElements as $favoriteElement) {
                if (!$element = $favoriteElement->getElement()) {
                    continue;
                }

                if (!isset($elements[$element->getId()]['element'])) {
                    $elements[$element->getId()] = [
                        'element' => $element,
                        'favorites' => [],
                    ];
                }

                if (
                    isset($elements[$element->getId()]['favorites'])
                    &&
                    !\in_array($favorite, $elements[$element->getId()]['favorites'], true)
                ) {
                    $elements[$element->getId()]['favorites'][] = $favorite;
                }
            }
        }

        $this->cache['elements'] = $elements ?? [];

        return $this->cache['elements'];
    }

    /**
     * Запоминает свёрнуто ли избранное.
     *
     * @param Favorite $favorite  Избранное
     * @param bool     $collapsed Свёрнуто (JSON boolean in string)
     *
     * @return bool
     */
    public function setCollapsed(Favorite $favorite, bool $collapsed): bool
    {
        $this->denyAccessUnlessOwner($favorite);
        $favorite->setIsCollapsed($collapsed);

        $this->em->persist($favorite);
        $this->em->flush();

        return true;
    }

    /**
     * Создаёт FavoriteShare и возвращает ссылку на избранное.
     *
     * @param Favorite $favorite
     *
     * @throws BasketFavoriteManagerException When favorite is empty
     *
     * @return string
     */
    public function getShareLink(Favorite $favorite): string
    {
        $this->denyAccessUnlessOwner($favorite);

        if ($favorite->getFavoriteElements()->isEmpty()) {
            throw new BasketFavoriteManagerException(sprintf('Favorite with id "%d" is empty. Nothing to share.', $favorite->getId()));
        }

        $user = $this->getUser();

        $token = mb_substr(str_replace('-', '', TokenGenerator::generateToken()), 0, 8);
        $token = $user->getUsername().'-'.$token;

        $favoriteShare = new FavoriteShare();
        $favoriteShare->setFavorite($favorite);
        $favoriteShare->setUser($user);
        $favoriteShare->setName($favorite->getName());
        $favoriteShare->setToken($token);

        /** @var FavoriteElement $favoriteElement */
        foreach ($favorite->getFavoriteElements() as $favoriteElement) {
            $favoriteShareElement = new FavoriteShareElement();
            $favoriteShareElement->setFavoriteShare($favoriteShare);
            $favoriteShareElement->setElement($favoriteElement->getElement());
            $favoriteShare->addFavoriteShareElement($favoriteShareElement);
            $this->em->persist($favoriteShareElement);
        }

        $this->em->persist($favoriteShare);
        $this->em->flush();

        return $this->router->generate('irmag_favorite_shared', ['token' => $token], UrlGeneratorInterface::ABSOLUTE_URL);
    }

    /**
     * Копирует все товары из share избранного в своё.
     *
     * @param FavoriteShare $favoriteShare Share избранное
     * @param string        $favoriteName  Название избранного [optional]
     *
     * @return bool
     */
    public function copyAllFromFavoriteShareToFavorite(FavoriteShare $favoriteShare, string $favoriteName = null): bool
    {
        /** @var FavoriteShareElement $favoriteShareElement */
        foreach ($favoriteShare->getFavoriteShareElements() as $favoriteShareElement) {
            $this->addElement($favoriteShareElement->getElement(), $favoriteName);
        }

        return true;
    }

    /**
     * Копирует все товары из share избранного в корзину.
     *
     * @param FavoriteShare $favoriteShare Share избранное
     *
     * @return bool
     */
    public function copyAllFromFavoriteShareToBasket(FavoriteShare $favoriteShare): bool
    {
        /** @var FavoriteShareElement $favoriteShareElement */
        foreach ($favoriteShare->getFavoriteShareElements() as $favoriteShareElement) {
            $element = $favoriteShareElement->getElement();

            if ($element->getIsActive()) {
                $this->container->get(BasketManager::class)->addElement($element);
            }
        }

        return true;
    }

    /**
     * Копирует все товары из списка товаров для промокодов по акции в избранное.
     *
     * @param string $favoriteName
     * @param Action $action
     *
     * @return bool
     */
    public function copyAllPromocodeElementsToFavoriteByAction(string $favoriteName, Action $action): bool
    {
        $elements = $this->em->getRepository(PromocodeElementsList::class)->getElementsByAction($action);
        $favorite = $this->getFavorite($favoriteName, true);

        foreach ($elements as $element) {
            $favoriteElement = new FavoriteElement();
            $favoriteElement
                ->setElement($element)
                ->setFavorite($favorite);

            $favorite->addFavoriteElement($favoriteElement);
            $this->em->persist($favoriteElement);
        }

        $this->em->flush();

        return true;
    }

    /**
     * Проверяет, находится ли товар в отслеживаемом избранном.
     *
     * @param Element $element
     *
     * @return bool
     */
    public function isWatching(Element $element): bool
    {
        $watchingFavorite = $this->em->getRepository(Favorite::class)->findOneBy(['user' => $this->getUser(), 'name' => Favorite::WATCHING_FAVORITE_NAME]);

        if ($watchingFavorite instanceof Favorite) {
            /** @var FavoriteElement $favoriteElement */
            foreach ($watchingFavorite->getFavoriteElements() as $favoriteElement) {
                if ($favoriteElement->getElement() === $element) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Проверяет принадлежность избранного.
     *
     * @param Favorite $favorite Избранное
     *
     * @throws AccessDeniedException Если избранное не принадлежит пользователю
     *
     * @return bool
     */
    private function denyAccessUnlessOwner(Favorite $favorite): bool
    {
        if ($this->isAuthenticated() && $this->getUser() === $favorite->getUser()) {
            return true;
        } elseif ($this->getTmpSession() === $favorite->getTmpSession()) {
            return true;
        }

        throw new AccessDeniedException();
    }

    /**
     * Возвращает аутентифицированное избранное.
     *
     * @param string $favoriteName     Название избранного [optional]
     * @param bool   $createIfNotExist Создать избранное, если нет [optional]
     *
     * @return Favorite|array
     */
    private function getAuthenticatedFavorite(string $favoriteName = null, bool $createIfNotExist = false)
    {
        if (empty($favoriteName)) {
            $favoriteName = self::DEFAULT_FAVORITE_NAME;
        }

        $cacheKey = 'favorite_auth_'.$favoriteName;

        if (isset($this->cache[$cacheKey])) {
            return $this->cache[$cacheKey];
        }

        $user = $this->getUser();
        $favorite = $this->em->getRepository(Favorite::class)
            ->findOneBy(['user' => $user, 'name' => $favoriteName]);

        if (!$favorite && true === $createIfNotExist) {
            $favorite = new Favorite();
            $favorite->setUser($user);
            $favorite->setName($favoriteName);

            $this->em->persist($favorite);
            $this->em->flush();
        }

        $this->cache[$cacheKey] = $favorite ?? [];

        return $this->cache[$cacheKey];
    }

    /**
     * Возвращает временное избранное.
     *
     * @param bool $createIfNotExist Создать избранное, если нет [optional]
     *
     * @return Favorite|array|null
     */
    private function getTmpFavorite(bool $createIfNotExist = false)
    {
        if (empty($tmpSession = $this->getTmpSession())) {
            return null;
        }

        if (isset($this->cache['favorite_tmp'])) {
            return $this->cache['favorite_tmp'];
        }

        $favorite = $this->em->getRepository(Favorite::class)
            ->findOneBy(['tmpSession' => $tmpSession, 'name' => self::DEFAULT_FAVORITE_NAME]);

        if (!$favorite && true === $createIfNotExist) {
            $favorite = new Favorite();
            $favorite->setTmpSession($tmpSession);
            $favorite->setName(self::DEFAULT_FAVORITE_NAME);

            $this->em->persist($favorite);
            $this->em->flush();
        }

        $this->cache['favorite_tmp'] = $favorite ?? [];

        return $this->cache['favorite_tmp'];
    }
}
