<?php

namespace Irmag\SiteBundle\Service\BasketFavoriteManager;

use Irmag\CoreBundle\Utils\TokenGenerator;
use Irmag\ProfileBundle\Entity\User;

trait BasketFavoriteTrait
{
    private static $sessionId = 'basket_favorite_token';

    /**
     * Проверяет, аутентифицирован ли пользователь.
     *
     * @return bool
     */
    private function isAuthenticated(): bool
    {
        $token = $this->tokenStorage->getToken();

        return null !== $token && $token->getUser() instanceof User;
    }

    /**
     * Возвращает текущего пользователя.
     *
     * @return User|null
     */
    private function getUser(): ?User
    {
        $token = $this->tokenStorage->getToken();

        if (null !== $token && $token->getUser() instanceof User) {
            return $token->getUser();
        }

        return null;
    }

    /**
     * Возвращает токен временной сессии.
     *
     * @throws BasketFavoriteManagerException
     *
     * @return string
     */
    private function getTmpSession(): string
    {
        $masterRequest = $this->requestStack->getMasterRequest();

        if (null === $masterRequest) {
            throw new BasketFavoriteManagerException('Master request is null, something wrong...');
        }

        $session = $masterRequest->getSession();

        if ($session->has(self::$sessionId)) {
            $tmpSession = $session->get(self::$sessionId);
        }

        if (empty($tmpSession)) {
            $tmpSession = TokenGenerator::generateToken();
            $session->set(self::$sessionId, $tmpSession);
        }

        return $tmpSession;
    }
}
