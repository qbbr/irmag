<?php

namespace Irmag\SiteBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\SiteBundle\Entity\Section;

class IrmagSeoSectionsCommand extends Command
{
    protected static $defaultName = 'irmag:seo_parser:sections';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Section SEO CSV parser')
            ->addArgument('file', InputArgument::REQUIRED, 'Path to CSV file')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $filePath = $input->getArgument('file');

        if (!is_file($filePath)) {
            $this->io->error(sprintf('File "%s" not found!', $filePath));

            return null;
        }

        $file = fopen($filePath, 'r');
        $count = 1;

        while (false !== ($line = fgetcsv($file, 0, ';'))) {
            $title = $this->decode($line[0]);
            $h1 = $this->decode($line[1]);
            $description = $this->decode($line[2]);
            $url = $this->decode($line[3]);

            if (empty($url)) {
                $this->io->warning(sprintf('Url is empty. Line: %d.', $count));
                continue;
            }

            $section = $this->getSectionByUrl($url);

            if (null === $section) {
                continue;
            }

            if (!empty($title)) {
                $section->setSeoTitle($title);
            }

            if (!empty($h1)) {
                $section->setSeoH1($h1);
            }

            if (!empty($description)) {
                $section->setSeoDescription($description);
            }

            $this->em->persist($section);
            ++$count;
        }

        fclose($file);
        $this->em->flush();
        $this->io->success(sprintf('Done. Count: %d.', $count));
    }

    private function decode(string $string): string
    {
        return mb_convert_encoding($string, 'utf-8', 'cp1251');
    }

    private function getSectionByUrl(string $url): ?Section
    {
        if (false === preg_match('/\/cat\/(\d+)\//', $url, $matches)) {
            throw new IrmagException('Cant get section id from url.');
        }

        if (empty($matches[1]) || !is_numeric($matches[1])) {
            throw new IrmagException('Section id is broken.');
        }

        $id = (int) $matches[1];

        $section = $this->em->getRepository(Section::class)->find($id);

        if (!$section) {
            $this->io->warning(sprintf('Section with id "%d" not found!', $id));
        }

        return $section;
    }
}
