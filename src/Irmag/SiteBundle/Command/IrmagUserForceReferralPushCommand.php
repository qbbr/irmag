<?php

namespace Irmag\SiteBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Entity\UserReferralLink;
use Irmag\SiteBundle\Service\ReferralManager\ReferralManager;

class IrmagUserForceReferralPushCommand extends Command
{
    protected static $defaultName = 'irmag:user:force_referral';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ReferralManager
     */
    private $referralManager;

    /**
     * @param EntityManagerInterface $em
     * @param ReferralManager        $referralManager
     */
    public function __construct(
        EntityManagerInterface $em,
        ReferralManager $referralManager
    ) {
        $this->em = $em;
        $this->referralManager = $referralManager;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Force push user to referral tree')
            ->addArgument('owner', InputArgument::REQUIRED, 'Owner user id')
            ->addArgument('referral', InputArgument::REQUIRED, 'Referral user id')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ownerId = $input->getArgument('owner');
        $referralId = $input->getArgument('referral');

        $userRepo = $this->em->getRepository(User::class);
        $owner = $userRepo->find($ownerId);

        if (!$owner instanceof User) {
            throw new IrmagException(sprintf('Owner user with id "%d" not found!', $ownerId));
        }

        $referral = $userRepo->find($referralId);

        if (!$referral instanceof User) {
            throw new IrmagException(sprintf('Referral user with id "%d" not found!', $referralId));
        }

        $io = new SymfonyStyle($input, $output);

        if ($io->confirm('Push (Be Careful plz)?', false)) {
            $referralLink = new UserReferralLink();
            $referralLink->setUserOwner($owner);
            $referralLink->setUserReferral($referral);
            $this->em->persist($referralLink);
            $this->em->flush();

            $this->referralManager->registerToTreeIfNeed($referral);

            $io->success('Done');
        }
    }
}
