<?php

namespace Irmag\SiteBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Irmag\SiteBundle\Entity\Action;

class IrmagCleanerActionCommand extends Command
{
    protected static $defaultName = 'irmag:cleaner:action';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Garbage cleaner for Action')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Execute the command as a dry run')
            ->addOption('days-of-life-after-end', null, InputOption::VALUE_REQUIRED, 'Days of life after action is end', 90)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $daysOfLife = (int) $input->getOption('days-of-life-after-end');
        /** @var \Irmag\FavoriteBundle\Repository\FavoriteShareRepository $favoriteShareRepo */
        $repository = $this->em->getRepository(Action::class);

        $output->writeln('<info>Cleaning Action ...</info>');
        $output->writeln(sprintf('<comment>Days of life after end: %d</comment>', $daysOfLife));

        $garbageCount = $repository->getGarbageCount($daysOfLife);
        $output->writeln(sprintf('<comment>Garbage count: %d</comment>', $garbageCount));

        if (0 === $garbageCount) {
            $output->writeln(sprintf('<info>Nothing to do</info>'));

            return;
        }

        if ($input->isInteractive()) {
            /** @var \Symfony\Component\Console\Helper\SymfonyQuestionHelper $helper */
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('<question>Continue?</question>', false);

            if (!$helper->ask($input, $output, $question)) {
                return;
            }
        }

        if (false === (bool) $input->getOption('dry-run')) {
            $repository->garbage($daysOfLife);
        }

        $output->writeln('<info>Complete</info>');
    }
}
