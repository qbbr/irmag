<?php

namespace Irmag\SiteBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Irmag\ProfileBundle\Entity\UserBonusHistory;

class UserBonusHistoryEvent extends Event
{
    /**
     * @var UserBonusHistory
     */
    private $userBonusHistory;

    /**
     * @param UserBonusHistory $userBonusHistory
     */
    public function __construct(UserBonusHistory $userBonusHistory)
    {
        $this->userBonusHistory = $userBonusHistory;
    }

    /**
     * @return UserBonusHistory
     */
    public function getUserBonusHistory(): UserBonusHistory
    {
        return $this->userBonusHistory;
    }
}
