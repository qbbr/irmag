<?php

namespace Irmag\SiteBundle\Repository;

use Irmag\ProfileBundle\Entity\User;

class UserReferralOrderPayoutRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param User $userReferral
     * @param User $userOwner
     *
     * @return int
     */
    public function getOrderCount(User $userReferral, User $userOwner): int
    {
        return $this->createQueryBuilder('urop')
            ->select('COUNT(urop.id)')
            ->where('urop.userReferral = :userReferral')
            ->andWhere('urop.userOwner = :userOwner')
            ->setParameters([
                'userReferral' => $userReferral,
                'userOwner' => $userOwner,
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param User $userReferral
     * @param User $userOwner
     *
     * @return int
     */
    public function getAllBonusProfits(User $userReferral, User $userOwner): int
    {
        return $this->createQueryBuilder('urop')
            ->select('COALESCE(SUM(ubh.value), 0)')
            ->leftJoin('urop.userBonusHistory', 'ubh')
            ->where('urop.userReferral = :userReferral')
            ->andWhere('urop.userOwner = :userOwner')
            ->setParameters([
                'userReferral' => $userReferral,
                'userOwner' => $userOwner,
            ])
            ->getQuery()
            ->getSingleScalarResult();
    }
}
