<?php

namespace Irmag\SiteBundle\Repository;

use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\PromocodeElementsList;

class PromocodeElementsListRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param PromocodeElementsList $list
     *
     * @return mixed
     */
    public function getElementsOfList(PromocodeElementsList $list)
    {
        return $this->createQueryBuilder('cl')
            ->select('cl, e, se, s, p, f, tp, t')
            ->join('cl.elements', 'e')
            ->leftJoin('e.sectionElements', 'se')
            ->leftJoin('se.section', 's')
            ->leftJoin('e.pictures', 'p')
            ->leftJoin('p.file', 'f')
            ->leftJoin('e.tonePicture', 'tp')
            ->leftJoin('e.thread', 't')
            ->where('cl.id = :id')
            ->setParameter('id', $list->getId())
            ->getQuery()
            ->getSingleResult()
            ->getElements();
    }

    /**
     * Получить товары из списка по акции.
     *
     * @param Action $action
     *
     * @return mixed
     */
    public function getElementsByAction(Action $action)
    {
        return $this->createQueryBuilder('cl')
            ->select('cl, e')
            ->join('cl.elements', 'e')
            ->where('cl.action = :action')
            ->setParameter('action', $action)
            ->getQuery()
            ->getSingleResult()
            ->getElements();
    }
}
