<?php

namespace Irmag\SiteBundle\Repository;

class ElementCountryRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param string $searchQuery
     * @param int    $limit
     *
     * @return array
     */
    public function search(string $searchQuery, int $limit): array
    {
        return $this->createQueryBuilder('c')
            ->andWhere('lower(c.name) = lower(:searchQuery)')
            ->setParameter('searchQuery', $searchQuery)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
