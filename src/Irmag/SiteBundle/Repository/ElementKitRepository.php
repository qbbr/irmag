<?php

namespace Irmag\SiteBundle\Repository;

use Irmag\SiteBundle\Entity\Element;
use Irmag\SiteBundle\Entity\ElementKit;

class ElementKitRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param Element $element
     *
     * @return array
     */
    public function getByElement(Element $element)
    {
        /* @var ElementKit $elementKit */
        $elementKit = $this->createQueryBuilder('k')
            ->select('partial k.{id, element1, element2, element3, element4}')
            ->where('k.element1 = :elementId')
            ->orWhere('k.element2 = :elementId')
            ->orWhere('k.element3 = :elementId')
            ->orWhere('k.element4 = :elementId')
            ->setParameter('elementId', $element->getId())
            ->setMaxResults(1)
            ->orderBy('RANDOM()')
            ->getQuery()
            ->getOneOrNullResult();

        if (!$elementKit) {
            return [];
        }

        return $this->buildElements($elementKit, $element);
    }

    /**
     * @param Element $element
     *
     * @return array
     */
    public function getByGroupBy(Element $element)
    {
        if (empty($element->getGroupBy())) {
            return [];
        }

        $elementKit = $this->createQueryBuilder('k')
            ->select('partial k.{id, element1, element2, element3, element4}')
            ->where('k.element1ForAllGroupBy = :groupBy')
            ->orWhere('k.element2ForAllGroupBy = :groupBy')
            ->orWhere('k.element3ForAllGroupBy = :groupBy')
            ->orWhere('k.element4ForAllGroupBy = :groupBy')
            ->setParameter('groupBy', $element->getGroupBy())
            ->getQuery()
            ->useResultCache(true)
            ->getOneOrNullResult();

        if (!$elementKit) {
            return [];
        }

        return $this->buildElements($elementKit, $element);
    }

    /**
     * @param ElementKit $elementKit
     * @param Element    $element
     *
     * @return array
     */
    private function buildElements(ElementKit $elementKit, Element $element)
    {
        $elements = [];

        if ($elementKit->getElement1() && $elementKit->getElement1() !== $element) {
            $elements[] = $elementKit->getElement1();
        }

        if ($elementKit->getElement2() && $elementKit->getElement2() !== $element) {
            $elements[] = $elementKit->getElement2();
        }

        if ($elementKit->getElement3() && $elementKit->getElement3() !== $element) {
            $elements[] = $elementKit->getElement3();
        }

        if ($elementKit->getElement4() && $elementKit->getElement4() !== $element) {
            $elements[] = $elementKit->getElement4();
        }

        if (!empty($elements)) {
            shuffle($elements);
            array_unshift($elements, $element);
        }

        return $elements;
    }
}
