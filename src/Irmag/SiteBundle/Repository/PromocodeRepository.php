<?php

namespace Irmag\SiteBundle\Repository;

class PromocodeRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param string $code Код промокода
     *
     * @return \Irmag\SiteBundle\Entity\Promocode|null
     */
    public function getByCode(string $code)
    {
        return $this->createQueryBuilder('c')
            ->where('c.code = :code')
            ->andWhere('c.isActive = true')
            ->setParameter('code', trim($code))
            ->getQuery()
            ->getOneOrNullResult();
    }
}
