<?php

namespace Irmag\SiteBundle\Repository;

use Irmag\SiteBundle\Entity\UserReferralTree;

class UserReferralTreeRepository extends \Gedmo\Tree\Entity\Repository\NestedTreeRepository
{
    /**
     * @param UserReferralTree $referralNode
     *
     * @return UserReferralTree[]
     */
    public function getParentNodes(UserReferralTree $referralNode)
    {
        return $this->createQueryBuilder('rt')
            ->addSelect('u')
            ->leftJoin('rt.user', 'u')
            ->where('rt.lft < :lft')
            ->andWhere('rt.rgt > :rgt')
            ->andWhere('rt.root = :root')
            ->orderBy('rt.lvl', 'DESC')
            ->setParameters([
                'lft' => $referralNode->getLft(),
                'rgt' => $referralNode->getRgt(),
                'root' => $referralNode->getRoot(),
            ])
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param UserReferralTree|null $rootNode
     *
     * @return array UserReferralTree[]
     */
    public function getTree(UserReferralTree $rootNode = null): array
    {
        $qb = $this->createQueryBuilder('node')
            ->addSelect('u, ava')
            ->leftJoin('node.user', 'u')
            ->leftJoin('u.avatar', 'ava')
            ->orderBy('node.root, node.lft', 'ASC')
        ;

        if (null !== $rootNode) {
            $qb
                ->where('node.lft >= :lft')
                ->andWhere('node.rgt <= :rgt')
                ->andWhere('node.root = :root')
                ->setParameters([
                    'lft' => $rootNode->getLft(),
                    'rgt' => $rootNode->getRgt(),
                    'root' => $rootNode->getRoot(),
                ])
            ;
        }

        return $qb
            ->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_INCLUDE_META_COLUMNS, true)
            ->getArrayResult();
    }
}
