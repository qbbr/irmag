<?php

namespace Irmag\SiteBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Entity\ActionElement;

class ActionRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @return Action[]
     */
    public function getActionsIsForAllElements(): array
    {
        return $this->createQueryBuilder('a')
            ->where('a.conditionIsForAllElements = true')
            ->andWhere('a.isActive = true')
            ->andWhere('a.endDateTime > :now')
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $searchQuery
     * @param int    $limit
     *
     * @return Action[]
     */
    public function search(string $searchQuery, int $limit): array
    {
        return $this->createQueryBuilder('a')
            ->addSelect('PLAINTSRANK(a.tsv, :searchQuery, \'irmag\') as HIDDEN rank')
            ->andWhere('PLAINTSQUERY(a.tsv, :searchQuery, \'irmag\') = true')
            ->andWhere('a.endDateTime > :now')
            ->andWhere('a.startDateTime < :now')
            ->andWhere('a.isActive = true')
            ->setParameters([
                'searchQuery' => $searchQuery,
                'now' => new \DateTime(),
            ])
            ->orderBy('rank', 'desc')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $slug
     *
     * @return mixed
     */
    public function getOneBySlug(string $slug)
    {
        return $this->createQueryBuilder('a')
            ->addSelect('b, bg, s, t, cl, cle')
            ->leftJoin('a.banner', 'b')
            ->leftJoin('a.bannerBg', 'bg')
            ->leftJoin('a.sticker', 's')
            ->leftJoin('a.thread', 't')
            ->leftJoin('a.promocodeElementsList', 'cl')
            ->leftJoin('cl.elements', 'cle')
            ->where('a.slug = :slug')
            ->setParameter('slug', $slug)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Возвращает QueryBuilder акций.
     *
     * @param bool $isArchive Если true, то архив, иначе активные акции
     *
     * @return QueryBuilder
     */
    public function getActionsQb(bool $isArchive = false): QueryBuilder
    {
        $qb = $this->createQueryBuilder('a')
            ->addSelect('b, bg, t')
            ->leftJoin('a.banner', 'b')
            ->leftJoin('a.bannerBg', 'bg')
            ->leftJoin('a.thread', 't')
            ->where('a.banner IS NOT NULL')
            ->andWhere('a.bannerBg IS NOT NULL')
            ->orderBy('a.position', 'ASC');

        if (false === $isArchive) {
            // active
            $qb
                ->andWhere('a.isActive = true')
                ->andWhere('a.endDateTime > :now');
        } else {
            // archive
            $qb->andWhere('a.endDateTime < :now');
        }

        return $qb->setParameter('now', new \DateTime());
    }

    /**
     * Возвращает последнюю созданную акцию.
     *
     * @return Action
     */
    public function getLatest(): Action
    {
        return $this->createQueryBuilder('t')
            ->where('t.isActive = true')
            ->orderBy('t.updatedAt', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * @return Action|null
     */
    public function getOneRandom(): ?Action
    {
        return $this->getActionsQb()
            ->orderBy('RANDOM()')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Возвращает баннеры для главной страницы.
     *
     * @param int $limit
     *
     * @return array
     */
    public function getIndexBanners($limit = 10)
    {
        return $this->createQueryBuilder('a')
            ->select('partial a.{id, slug, title, isImportant}, partial b.{id, path}, partial bg.{id, path}')
            ->leftJoin('a.banner', 'b')
            ->leftJoin('a.bannerBg', 'bg')
            ->where('a.isActive = true')
            ->andWhere('b.path IS NOT NULL')
            ->andWhere('bg.path IS NOT NULL')
            ->andWhere('a.endDateTime > :now')
            ->andWhere('a.startDateTime < :now')
            ->orderBy('a.isImportant', 'DESC')
            ->setParameter('now', new \DateTime())
            ->setMaxResults($limit)
            ->getQuery()
            ->setHint(\Doctrine\ORM\Query::HINT_REFRESH, true)
            ->getResult();
    }

    /**
     * Чистит сущность от мусора.
     * Для консольной команды сборки мусора.
     *
     * @param int $daysOfLife
     */
    public function garbage(int $daysOfLife): void
    {
        $actions = $this->getGarbageQb($daysOfLife)
            ->select('a.id')
            ->getQuery()
            ->getResult();

        if ($actions) {
            $this->getEntityManager()->createQueryBuilder()
                ->delete()
                ->from(ActionElement::class, 'ae')
                ->where('ae.action IN (:actions)')
                ->setParameter('actions', $actions)
                ->getQuery()
                ->execute();
        }
    }

    /**
     * Возвращает кол-во мусора.
     * Для консольной команды сборки мусора.
     *
     * @param int $daysOfLife
     *
     * @return int
     */
    public function getGarbageCount(int $daysOfLife): int
    {
        return $this->getGarbageQb($daysOfLife)
            ->select('COUNT(a)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param int $daysOfLife
     *
     * @return QueryBuilder
     */
    private function getGarbageQb(int $daysOfLife): QueryBuilder
    {
        return $this->createQueryBuilder('a')
            ->where('a.endDateTime < :now')
            ->setParameter('now', $this->getModifiedDate($daysOfLife));
    }

    /**
     * @param int $daysOfLife
     *
     * @return \DateTime
     */
    private function getModifiedDate(int $daysOfLife): \DateTime
    {
        return (new \DateTime())->modify(sprintf('-%d days', $daysOfLife));
    }
}
