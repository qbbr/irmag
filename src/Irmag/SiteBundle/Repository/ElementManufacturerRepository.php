<?php

namespace Irmag\SiteBundle\Repository;

use Irmag\SiteBundle\Entity\ElementBrand;
use Irmag\SiteBundle\Entity\ElementManufacturer;
use Irmag\SiteBundle\Entity\Traits\TSearchBrandTrait;

class ElementManufacturerRepository extends \Doctrine\ORM\EntityRepository
{
    use TSearchBrandTrait;

    /**
     * @param int|null $limit
     *
     * @return array
     */
    public function getRandomManufacturerAndBrandsWithPicture(int $limit = null): array
    {
        $manufacturers = $this->getEntityManager()->createQueryBuilder()
            ->select('m, p')
            ->from(ElementManufacturer::class, 'm')
            ->leftJoin('m.picture', 'p')
            ->where('m.picture IS NOT NULL')
            ->orderBy('RANDOM()')
            ->getQuery()
            ->getResult();

        $brands = $this->getEntityManager()->createQueryBuilder()
            ->select('b, p')
            ->from(ElementBrand::class, 'b')
            ->leftJoin('b.picture', 'p')
            ->where('b.picture IS NOT NULL')
            ->orderBy('RANDOM()')
            ->getQuery()
            ->getResult();

        $data = array_merge($manufacturers, $brands);
        shuffle($data);

        if (null !== $limit && \count($data) > $limit) {
            $data = \array_slice($data, 0, $limit);
        }

        return $data;
    }
}
