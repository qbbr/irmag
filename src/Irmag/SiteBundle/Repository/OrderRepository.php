<?php

namespace Irmag\SiteBundle\Repository;

use Irmag\CoreBundle\Repository\Traits\GetTotalTrait;
use Irmag\ProfileBundle\Entity\User;

class OrderRepository extends \Doctrine\ORM\EntityRepository
{
    use GetTotalTrait;

    public function getCount(User $user): int
    {
        return $this->createQueryBuilder('o')
            ->select('COUNT(o)')
            ->where('o.user = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
