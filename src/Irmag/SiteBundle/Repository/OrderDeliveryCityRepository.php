<?php

namespace Irmag\SiteBundle\Repository;

class OrderDeliveryCityRepository extends \Doctrine\ORM\EntityRepository
{
    public function getLastDeliveryCityPosition()
    {
        return $this->createQueryBuilder('dc')
            ->select('MAX(dc.position) AS lastPosition')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
