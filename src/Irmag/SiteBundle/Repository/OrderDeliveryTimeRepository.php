<?php

namespace Irmag\SiteBundle\Repository;

class OrderDeliveryTimeRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * Ид временного интервала "В любое время".
     */
    const ANY_TIME_INTERVAL_ID = 1;

    /**
     * Ид субботнего интервала "9:00 - 16:30".
     */
    const SELF_SERVICE_TIME_INTERVAL_ID = 9;

    /**
     * @return \Irmag\SiteBundle\Entity\OrderDeliveryTime|null
     */
    public function getAnyTime()
    {
        return $this->createQueryBuilder('dt')
            ->where('dt.id = :id')
            ->setParameter('id', self::ANY_TIME_INTERVAL_ID)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
