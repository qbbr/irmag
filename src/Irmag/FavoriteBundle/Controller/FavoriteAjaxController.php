<?php

namespace Irmag\FavoriteBundle\Controller;

use Irmag\SiteBundle\Service\UrlShortener\UrlShortener;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\SiteBundle\Entity\Action;
use Irmag\SiteBundle\Controller\Traits\GetElementByIdTrait;
use Irmag\SiteBundle\Service\BasketFavoriteManager\FavoriteManager;
use Irmag\FavoriteBundle\Entity\Favorite;
use Irmag\FavoriteBundle\Entity\FavoriteShare;

/**
 * @Route("/ajax/favorite",
 *     methods={"POST"},
 *     defaults={"_format": "json"},
 *     requirements={"_format": "json"},
 *     condition="request.isXmlHttpRequest()",
 *     options={"expose": true}
 * )
 */
class FavoriteAjaxController extends Controller
{
    use GetElementByIdTrait;

    /**
     * @var FavoriteManager
     */
    private $favoriteManager;

    /**
     * @param FavoriteManager $favoriteManager
     */
    public function __construct(
        FavoriteManager $favoriteManager
    ) {
        $this->favoriteManager = $favoriteManager;
    }

    /**
     * @Route("/create/", name="irmag_favorite_ajax_create")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function createAction(Request $request)
    {
        $favoriteName = $request->request->get('favoriteName');

        return new JsonResponse([
            'success' => $this->favoriteManager->create($favoriteName),
            'favoriteName' => $favoriteName,
        ]);
    }

    /**
     * @Route("/clear/", name="irmag_favorite_ajax_clear")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function clearAction(Request $request)
    {
        $favoriteId = $request->request->getInt('favoriteId');
        $favorite = $this->getFavoriteByIdOrThrowNotFoundException($favoriteId);

        return new JsonResponse([
            'success' => $this->favoriteManager->clear($favorite),
        ]);
    }

    /**
     * @Route("/rename/", name="irmag_favorite_ajax_rename")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function renameAction(Request $request)
    {
        $favoriteId = $request->request->getInt('favoriteId');
        $favoriteName = $request->request->get('favoriteName');
        $favorite = $this->getFavoriteByIdOrThrowNotFoundException($favoriteId);

        return new JsonResponse([
            'success' => $this->favoriteManager->rename($favorite, $favoriteName),
        ]);
    }

    /**
     * @Route("/remove/", name="irmag_favorite_ajax_remove")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeAction(Request $request)
    {
        $favoriteId = $request->request->getInt('favoriteId');
        $favorite = $this->getFavoriteByIdOrThrowNotFoundException($favoriteId);

        return new JsonResponse([
            'success' => $this->favoriteManager->remove($favorite),
        ]);
    }

    /**
     * @Route("/add_element/", name="irmag_favorite_ajax_add_element")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addElementAction(Request $request)
    {
        $elementId = $request->request->getInt('elementId');
        $favoriteName = $request->request->get('favoriteName');
        $element = $this->getElementById($elementId);

        return new JsonResponse([
            'success' => $this->favoriteManager->addElement($element, $favoriteName),
        ]);
    }

    /**
     * @Route("/remove_element/", name="irmag_favorite_ajax_remove_element")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeElementAction(Request $request)
    {
        $elementId = $request->request->getInt('elementId');
        $favoriteName = $request->request->get('favoriteName');
        $element = $this->getElementById($elementId);

        return new JsonResponse([
            'success' => $this->favoriteManager->removeElement($element, $favoriteName),
        ]);
    }

    /**
     * @Route("/move_element_to_favorite_from_favorite/", name="irmag_favorite_ajax_move_element_to_favorite_from_favorite")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function moveElementToFavoriteFromFavoriteAction(Request $request)
    {
        $elementId = $request->request->getInt('elementId');
        $favoriteFromId = $request->request->getInt('favoriteFromId');
        $favoriteToId = $request->request->getInt('favoriteToId');

        $element = $this->getElementById($elementId);
        $favoriteFrom = $this->getFavoriteByIdOrThrowNotFoundException($favoriteFromId);
        $favoriteTo = $this->getFavoriteByIdOrThrowNotFoundException($favoriteToId);

        return new JsonResponse([
            'success' => $this->favoriteManager->moveElementToFavoriteFromFavorite($favoriteFrom, $favoriteTo, $element),
        ]);
    }

    /**
     * @Route("/copy_all_elements_to_basket/", name="irmag_favorite_ajax_copy_all_elements_to_basket")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function copyAllElementsToBasketAction(Request $request)
    {
        $favoriteId = $request->request->getInt('favoriteId');
        $favorite = $this->getFavoriteByIdOrThrowNotFoundException($favoriteId);

        return new JsonResponse([
            'success' => $this->favoriteManager->copyAllElementsToBasket($favorite),
        ]);
    }

    /**
     * @Route("/copy_all_promocode_elements_to_favorite/", name="irmag_favorite_ajax_copy_all_promocode_elements_to_favorite")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function copyAllPromocodeElementsToFavoriteAction(Request $request)
    {
        $favoriteName = $request->request->get('favoriteName');
        $actionId = $request->request->getInt('actionId');
        $action = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Action::class)
            ->find($actionId);

        if (!$action) {
            throw new NotFoundHttpException(sprintf('Action with id "%d" not found.', $actionId));
        }

        return new JsonResponse([
            'success' => $this->favoriteManager->copyAllPromocodeElementsToFavoriteByAction($favoriteName, $action),
        ]);
    }

    /**
     * @Route("/set_collapsed/", name="irmag_favorite_ajax_set_collapsed")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function setCollapsedAction(Request $request)
    {
        $favoriteId = $request->request->getInt('favoriteId');
        $collapsed = $request->request->getBoolean('collapsed');
        $favorite = $this->getFavoriteByIdOrThrowNotFoundException($favoriteId);

        return new JsonResponse([
            'success' => $this->favoriteManager->setCollapsed($favorite, $collapsed),
        ]);
    }

    /**
     * @Route("/get_share_link/", name="irmag_favorite_ajax_get_share_link")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getShareLinkAction(Request $request)
    {
        $favoriteId = $request->request->getInt('favoriteId');
        $favorite = $this->getFavoriteByIdOrThrowNotFoundException($favoriteId);
        $url = $this->favoriteManager->getShareLink($favorite);

        try {
            $urlShort = $this->get(UrlShortener::class)->shorten($url, true);
        } catch (\Exception $e) {
        }

        return new JsonResponse([
            'success' => true,
            'url' => $urlShort ?? $url,
        ]);
    }

    /**
     * @Route("/copy_all_from_favorite_share_to_favorite/", name="irmag_favorite_ajax_copy_all_from_favorite_share_to_favorite")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function copyAllFromFavoriteShareToFavoriteAction(Request $request)
    {
        $token = $request->request->get('favoriteShareToken');
        $favoriteName = $request->request->get('favoriteName');
        $favoriteShare = $this->getFavoriteShareByTokenOrThrowNotFoundException($token);

        return new JsonResponse([
            'success' => $this->favoriteManager->copyAllFromFavoriteShareToFavorite($favoriteShare, $favoriteName),
        ]);
    }

    /**
     * @Route("/copy_all_from_favorite_share_to_basket/", name="irmag_favorite_ajax_copy_all_from_favorite_share_to_basket")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function copyAllFromFavoriteShareToBasketAction(Request $request)
    {
        $token = $request->request->get('favoriteShareToken');
        $favoriteShare = $this->getFavoriteShareByTokenOrThrowNotFoundException($token);

        return new JsonResponse([
            'success' => $this->favoriteManager->copyAllFromFavoriteShareToBasket($favoriteShare),
        ]);
    }

    /**
     * @param int $id Ид избранного
     *
     * @throws NotFoundHttpException When favorite not found
     *
     * @return Favorite
     */
    protected function getFavoriteByIdOrThrowNotFoundException(int $id): Favorite
    {
        $favorite = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(Favorite::class)
            ->find($id);

        if (!$favorite) {
            throw new NotFoundHttpException(sprintf('Favorite with id "%d" not found.', $id));
        }

        return $favorite;
    }

    /**
     * @param string $token
     *
     * @throws NotFoundHttpException     When FavoriteShare not found
     * @throws \InvalidArgumentException When token is empty
     *
     * @return FavoriteShare
     */
    protected function getFavoriteShareByTokenOrThrowNotFoundException(string $token): FavoriteShare
    {
        if (empty($token)) {
            throw new \InvalidArgumentException('FavoriteShare token is empty.');
        }

        $favoriteShare = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(FavoriteShare::class)
            ->findOneBy(['token' => $token]);

        if (!$favoriteShare) {
            throw new NotFoundHttpException(sprintf('FavoriteShare with token "%d" not found.', $token));
        }

        return $favoriteShare;
    }

    /**
     * @Route("/data/", name="irmag_favorite_ajax_data")
     */
    public function getDataAction()
    {
        return new JsonResponse([
            'total-count' => $this->forward('Irmag\FavoriteBundle\Controller\FavoriteController::totalCountAction')->getContent(),
            'mini-favorite' => $this->forward('Irmag\FavoriteBundle\Controller\FavoriteController::miniFavoriteAction')->getContent(),
        ]);
    }
}
