<?php

namespace Irmag\FavoriteBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\FavoriteBundle\Entity\FavoriteShare;
use Irmag\SiteBundle\Service\BasketFavoriteManager\FavoriteManager;

/**
 * @Route("/favorite")
 */
class FavoriteController extends Controller
{
    private $sortFieldWhitelist = ['name', 'updatedAt'];
    private $sortDirectionWhitelist = ['asc', 'desc'];
    private $defaultSortFieldName = 'updatedAt';
    private $defaultSortDirection = 'desc';

    /**
     * @Route("/", name="irmag_favorite")
     */
    public function indexAction()
    {
        return $this->render('@IrmagFavorite/index.html.twig');
    }

    /**
     * @Route("/content/", name="irmag_favorite_ajax_content", options={"expose": true})
     */
    public function indexContentAction()
    {
        $masterRequest = $this->get('request_stack')->getMasterRequest();

        $sort = $masterRequest->query->getAlpha('sort', $this->defaultSortFieldName);

        if (!\in_array($sort, $this->sortFieldWhitelist, true)) {
            $sort = $this->defaultSortFieldName;
        }

        $direction = $masterRequest->query->getAlpha('direction', $this->defaultSortDirection);

        if (!\in_array($direction, $this->sortDirectionWhitelist, true)) {
            $direction = $this->defaultSortDirection;
        }

        $this->get('doctrine.orm.default_entity_manager')->clear();

        $favorites = $this->get(FavoriteManager::class)->getFavoritesQb();

        if ('name' === $sort) {
            $favorites
                ->addOrderBy('e.name', $direction)
                ->addOrderBy('e.toneName', $direction);
        } elseif ('updatedAt' === $sort) {
            $favorites
                ->addOrderBy('fe.updatedAt', $direction);
        }

        return $this->render('@IrmagFavorite/index-content.html.twig', [
            'favorites' => $favorites->getQuery()->getResult(),
            'sortable' => [
                'sort' => $sort,
                'direction' => $direction,
                'negative_direction' => 'asc' === $direction ? 'desc' : 'asc',
            ],
        ]);
    }

    public function totalCountAction()
    {
        return $this->render('@IrmagFavorite/total-count.html.twig');
    }

    public function miniFavoriteAction()
    {
        return $this->render('@IrmagFavorite/mini-favorite.html.twig');
    }

    /**
     * @Route("/{token}/", name="irmag_favorite_shared")
     *
     * @param string $token
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function sharedAction(string $token)
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $favoriteShare = $em->getRepository(FavoriteShare::class)->getByToken($token);

        if (!$favoriteShare) {
            throw $this->createNotFoundException(sprintf('FavoriteShare with token "%s" not found.', $token));
        }

        $favoriteShare->setLastVisitAt(new \DateTime());
        $em->persist($favoriteShare);
        $em->flush($favoriteShare);

        return $this->render('@IrmagFavorite/shared.html.twig', [
            'favorite' => $favoriteShare,
        ]);
    }
}
