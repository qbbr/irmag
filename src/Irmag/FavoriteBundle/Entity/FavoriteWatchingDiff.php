<?php

namespace Irmag\FavoriteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\SiteBundle\Entity\Element;

/**
 * @ORM\Table(name="favorites_watching_diff")
 * @ORM\Entity
 */
class FavoriteWatchingDiff
{
    use TimestampableEntity;

    /**
     * @var Favorite
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Favorite")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $favorite;

    /**
     * @var Element
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="\Irmag\SiteBundle\Entity\Element", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $element;

    /**
     * Активность.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isActive;

    public function __construct()
    {
        $this->isActive = false;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getFavorite(): ?Favorite
    {
        return $this->favorite;
    }

    public function setFavorite(?Favorite $favorite): self
    {
        $this->favorite = $favorite;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }
}
