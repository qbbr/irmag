<?php

namespace Irmag\FavoriteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\SiteBundle\Entity\Element;

/**
 * @ORM\Table(name="favorites_shares_elements")
 * @ORM\Entity
 */
class FavoriteShareElement
{
    use TimestampableEntity;

    /**
     * @var FavoriteShare
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="FavoriteShare", inversedBy="favoriteShareElements")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $favoriteShare;

    /**
     * @var Element
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="\Irmag\SiteBundle\Entity\Element", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $element;

    public function getFavoriteShare(): ?FavoriteShare
    {
        return $this->favoriteShare;
    }

    public function setFavoriteShare(?FavoriteShare $favoriteShare): self
    {
        $this->favoriteShare = $favoriteShare;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }
}
