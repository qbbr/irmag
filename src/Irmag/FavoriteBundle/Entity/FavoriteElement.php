<?php

namespace Irmag\FavoriteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\SiteBundle\Entity\Element;

/**
 * @ORM\Table(name="favorites_elements")
 * @ORM\Entity
 */
class FavoriteElement
{
    use TimestampableEntity;

    /**
     * @var Favorite
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Favorite", inversedBy="favoriteElements")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $favorite;

    /**
     * @var Element
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="\Irmag\SiteBundle\Entity\Element", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $element;

    public function setFavorite(?Favorite $favorite): self
    {
        $this->favorite = $favorite;

        return $this;
    }

    public function getFavorite(): ?Favorite
    {
        return $this->favorite;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }
}
