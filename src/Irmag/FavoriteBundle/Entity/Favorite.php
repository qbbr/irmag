<?php

namespace Irmag\FavoriteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Irmag\ProfileBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Table(
 *     name="favorites",
 *     indexes={
 *         @ORM\Index(columns={"tmp_session"})
 *     },
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(columns={"user_id", "name"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="Irmag\FavoriteBundle\Repository\FavoriteRepository")
 *
 * @UniqueEntity(fields={"user", "name"})
 */
class Favorite
{
    use TimestampableEntity;

    /**
     * Название избранного по-умолчанию.
     */
    const DEFAULT_FAVORITE_NAME = 'default';

    /**
     * Название отслеживаемого избранного.
     */
    const WATCHING_FAVORITE_NAME = 'watching';

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tmpSession;

    /**
     * @var string
     *
     * @Assert\NotBlank
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @var Collection|FavoriteElement[]
     *
     * @ORM\OneToMany(targetEntity="FavoriteElement", mappedBy="favorite", cascade={"persist"}, orphanRemoval=true)
     */
    private $favoriteElements;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $isCollapsed;

    public function __construct()
    {
        $this->favoriteElements = new ArrayCollection();
        $this->isCollapsed = false;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    /**
     * XXX: custom.
     *
     * @deprecated
     */
    public function hasElement($element): bool
    {
        return false;
    }

    /**
     * XXX: Custom.
     *
     * @return Favorite
     */
    public function setUpdatedAtNow()
    {
        $this->setUpdatedAt(new \DateTime());

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTmpSession(?string $tmpSession): self
    {
        $this->tmpSession = $tmpSession;

        return $this;
    }

    public function getTmpSession(): ?string
    {
        return $this->tmpSession;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setIsCollapsed(bool $isCollapsed): self
    {
        $this->isCollapsed = $isCollapsed;

        return $this;
    }

    public function getIsCollapsed(): ?bool
    {
        return $this->isCollapsed;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function addFavoriteElement(FavoriteElement $favoriteElement): self
    {
        if (!$this->favoriteElements->contains($favoriteElement)) {
            $this->favoriteElements[] = $favoriteElement;
            $favoriteElement->setFavorite($this);
        }

        return $this;
    }

    public function removeFavoriteElement(FavoriteElement $favoriteElement): self
    {
        if ($this->favoriteElements->contains($favoriteElement)) {
            $this->favoriteElements->removeElement($favoriteElement);
            // set the owning side to null (unless already changed)
            if ($favoriteElement->getFavorite() === $this) {
                $favoriteElement->setFavorite(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|FavoriteElement[]
     */
    public function getFavoriteElements(): Collection
    {
        return $this->favoriteElements;
    }
}
