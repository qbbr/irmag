<?php

namespace Irmag\FavoriteBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Security\Core\User\UserInterface;
use Irmag\FavoriteBundle\Entity\Favorite;
use Irmag\BasketBundle\Repository\Traits\GarbageTrait;

class FavoriteRepository extends \Doctrine\ORM\EntityRepository
{
    use GarbageTrait;

    /**
     * @return Favorite[]
     */
    public function getWatching(): array
    {
        return $this->createQueryBuilder('f')
            ->leftJoin('f.favoriteElements', 'fe')
            ->leftJoin('fe.element', 'e')
            ->where('f.name = :name')
            ->setParameter('name', Favorite::WATCHING_FAVORITE_NAME)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param UserInterface $user
     *
     * @return QueryBuilder
     */
    public function getAllByUserQb(UserInterface $user): QueryBuilder
    {
        return $this->getAllQb()
            ->where('f.user = :user')
            ->setParameter('user', $user);
    }

    /**
     * @param string $tmpSession
     *
     * @return QueryBuilder
     */
    public function getAllByTmpSessionQb(string $tmpSession): QueryBuilder
    {
        return $this->getAllQb()
            ->where('f.tmpSession = :tmpSession')
            ->setParameter('tmpSession', $tmpSession);
    }

    /**
     * @return QueryBuilder
     */
    private function getAllQb(): QueryBuilder
    {
        return $this->createQueryBuilder('f')
            ->addSelect('fe, e, p, file, tp')
            ->leftJoin('f.favoriteElements', 'fe')
            ->leftJoin('fe.element', 'e')
            ->leftJoin('e.pictures', 'p')
            ->leftJoin('p.file', 'file')
            ->leftJoin('e.tonePicture', 'tp')
            ->orderBy('f.id', 'asc');
    }
}
