<?php

namespace Irmag\FavoriteBundle\Repository;

use Doctrine\ORM\QueryBuilder;
use Irmag\FavoriteBundle\Entity\FavoriteShare;

class FavoriteShareRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param string $token
     *
     * @return FavoriteShare|null
     */
    public function getByToken(string $token)
    {
        return $this->createQueryBuilder('f')
            ->addSelect('fe, e, p, file, tp')
            ->leftJoin('f.favoriteShareElements', 'fe')
            ->leftJoin('fe.element', 'e')
            ->leftJoin('e.pictures', 'p')
            ->leftJoin('p.file', 'file')
            ->leftJoin('e.tonePicture', 'tp')
            ->where('f.token = :token')
            ->setParameter('token', $token)
            ->orderBy('f.id', 'asc')
            ->addOrderBy('e.name', 'asc')
            ->addOrderBy('e.toneName', 'asc')
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Чистит сущьность от мусора.
     * Для консольной команды сборки мусора.
     *
     * @param int $daysOfLife
     */
    public function garbage(int $daysOfLife): void
    {
        $this->getGarbageQb($daysOfLife)
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * Возвращает кол-во мусора.
     * Для консольной команды сборки мусора.
     *
     * @param int $daysOfLife
     *
     * @return int
     */
    public function getGarbageCount(int $daysOfLife): int
    {
        return $this->getGarbageQb($daysOfLife)
            ->select('COUNT(f)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param int $daysOfLife
     *
     * @return QueryBuilder
     */
    private function getGarbageQb(int $daysOfLife): QueryBuilder
    {
        return $this->createQueryBuilder('f')
            ->where('f.lastVisitAt IS NOT NULL AND f.lastVisitAt < :date')
            ->orWhere('f.lastVisitAt IS NULL AND f.createdAt < :date')
            ->setParameter('date', $this->getModifiedDate($daysOfLife));
    }

    /**
     * @param int $daysOfLife
     *
     * @return \DateTime
     */
    private function getModifiedDate(int $daysOfLife): \DateTime
    {
        return (new \DateTime())->modify(sprintf('-%d days', $daysOfLife));
    }
}
