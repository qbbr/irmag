<?php

namespace Irmag\FavoriteBundle\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Irmag\FavoriteBundle\Entity\Favorite;

class FavoriteFactory
{
    /**
     * Ключ для хранения токена анонимного избранного.
     */
    const SESSION_TMP_ID = 'irmag_favorite_tmp_id';

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @param EntityManagerInterface  $em
     * @param SessionInterface        $session
     * @param TokenStorageInterface   $tokenStorage
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(
        EntityManagerInterface $em,
        SessionInterface $session,
        TokenStorageInterface $tokenStorage,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $this->em = $em;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * Возвращает избранное, если его нет, то создаёт.
     * Если пользователь не аутентифицирован, тогда создаётся временное избранное.
     *
     * @param string $name Название избранного
     *
     * @return Favorite
     */
    public function get(string $name = null): Favorite
    {
        if ($this->isAuthenticated()) {
            return $this->getAuthenticated($name);
        }

        return $this->getAnonymous();
    }

    /**
     * Возвращает все избранные со всеми товарами.
     *
     * @return Favorite[]
     */
    public function getAll(): array
    {
        if ($this->isAuthenticated()) {
            return $this->em->getRepository(Favorite::class)->getAllByUser($this->getUser());
        }

        return $this->em->getRepository(Favorite::class)->getAllByTmpSession($this->getTmpSession());
    }

    /**
     * Возвращает аутентифицированное избранное.
     *
     * @param string $name Название избранного
     *
     * @throws FavoriteFactoryException
     *
     * @return Favorite
     */
    public function getAuthenticated(string $name = null): Favorite
    {
        $user = $this->getUser();

        if (null === $user) {
            throw new FavoriteFactoryException('User is not authenticated.');
        }

        if (empty(trim($name))) {
            $name = Favorite::DEFAULT_FAVORITE_NAME;
        }

        $favorite = $this->em->getRepository(Favorite::class)->findOneBy(['user' => $user, 'name' => $name]);

        if (!$favorite) {
            $favorite = new Favorite();
            $favorite->setUser($user);
            $favorite->setName($name);
        }

        return $favorite;
    }

    /**
     * @return Favorite
     */
    public function getAnonymous(): Favorite
    {
        $tmpSession = $this->getTmpSession();
        $name = Favorite::DEFAULT_FAVORITE_NAME;
        $favorite = $this->em->getRepository(Favorite::class)->findOneBy(['tmpSession' => $tmpSession, 'name' => $name]);

        if (!$favorite) {
            $favorite = new Favorite();
            $favorite->setTmpSession($tmpSession);
            $favorite->setName($name);
        }

        return $favorite;
    }

    /**
     * @return bool
     */
    public function isAuthenticated(): bool
    {
        return null !== $this->getUser();
    }

    /**
     * Назначает пользователя.
     *
     * @param UserInterface $user
     */
    public function setUser(UserInterface $user = null)
    {
        $this->user = $user;
    }

    /**
     * Возвращает текущего пользователя.
     * За исключением случая, когда использовался метод setUser.
     *
     * @return UserInterface|null
     */
    private function getUser()
    {
        if (null !== $this->user) {
            return $this->user;
        }

        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!\is_object($this->user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $this->user;
    }

    /**
     * Возвращает токен для анонимного избранного.
     *
     * @return string
     */
    private function getTmpSession()
    {
        if (!$this->session->has(self::SESSION_TMP_ID)) {
            $this->session->set(self::SESSION_TMP_ID, $this->tokenGenerator->generateToken());
        }

        return $this->session->get(self::SESSION_TMP_ID);
    }
}
