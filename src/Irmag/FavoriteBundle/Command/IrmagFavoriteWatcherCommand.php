<?php

namespace Irmag\FavoriteBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Irmag\ProfileBundle\Service\UserNotificationEventManager\UserNotificationEventManager;
use Irmag\ProfileBundle\Service\UserNotificationQueueManager\UserNotificationQueueManager;
use Irmag\ProfileBundle\NotificationEvents;
use Irmag\FavoriteBundle\Entity\Favorite;
use Irmag\FavoriteBundle\Entity\FavoriteWatchingDiff;

class IrmagFavoriteWatcherCommand extends Command
{
    protected static $defaultName = 'irmag:favorite:watch';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var UserNotificationEventManager
     */
    private $userNotificationEventManager;

    /**
     * @var UserNotificationQueueManager
     */
    private $userNotificationQueueManager;

    /**
     * @param EntityManagerInterface       $em
     * @param UrlGeneratorInterface        $router
     * @param UserNotificationEventManager $userNotificationEventManager
     * @param UserNotificationQueueManager $userNotificationQueueManager
     */
    public function __construct(
        EntityManagerInterface $em,
        UrlGeneratorInterface $router,
        UserNotificationEventManager $userNotificationEventManager,
        UserNotificationQueueManager $userNotificationQueueManager
    ) {
        $this->em = $em;
        $this->router = $router;
        $this->userNotificationEventManager = $userNotificationEventManager;
        $this->userNotificationQueueManager = $userNotificationQueueManager;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('For watching favorite')
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $favoriteCount = 0;
        $elementsCount = 0;
        $activatedCount = 0;

        $event = $this->userNotificationEventManager->getEventByName(NotificationEvents::FAVORITE_ELEMENT_IS_ACTIVE);

        foreach ($this->em->getRepository(Favorite::class)->getWatching() as $favorite) {
            foreach ($favorite->getFavoriteElements() as $favoriteElement) {
                $element = $favoriteElement->getElement();

                $diff = $this->em->getRepository(FavoriteWatchingDiff::class)->findOneBy(['favorite' => $favorite, 'element' => $element]);

                if ($diff instanceof FavoriteWatchingDiff) {
                    if (false === $diff->getIsActive()
                        &&
                        true === $element->getIsActive()
                    ) {
                        $url = $this->router->generate('irmag_catalog_element', ['id' => $element->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
                        $this->userNotificationQueueManager->create($event, $favorite->getUser(), $element->getName(), $url);

                        $diff->setIsActive(true);
                        $this->em->persist($diff);

                        ++$activatedCount;
                    }
                } elseif (false === $element->getIsActive()) {
                    $diff = new FavoriteWatchingDiff();
                    $diff->setFavorite($favorite);
                    $diff->setElement($element);
                    $diff->setIsActive(false);
                    $this->em->persist($diff);
                }

                ++$elementsCount;
            }

            $this->em->flush();
            ++$favoriteCount;
        }

        $output->writeln(sprintf('Favorites: <info>%d</info>', $favoriteCount));
        $output->writeln(sprintf('Elements: <info>%d</info>', $elementsCount));
        $output->writeln(sprintf('Activated: <info>%d</info>', $activatedCount));
        $output->writeln('');
        $output->writeln('<info>Complete.</info>');
    }
}
