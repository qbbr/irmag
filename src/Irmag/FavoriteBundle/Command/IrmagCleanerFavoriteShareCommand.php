<?php

namespace Irmag\FavoriteBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Irmag\FavoriteBundle\Entity\FavoriteShare;

class IrmagCleanerFavoriteShareCommand extends Command
{
    protected static $defaultName = 'irmag:cleaner:favorite_share';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDescription('Garbage cleaner for FavoriteShare')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Execute the command as a dry run')
            ->addOption('days-of-life', null, InputOption::VALUE_REQUIRED, 'Days of life', 7)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $daysOfLife = (int) $input->getOption('days-of-life');
        /** @var \Irmag\FavoriteBundle\Repository\FavoriteShareRepository $repository */
        $repository = $this->em->getRepository(FavoriteShare::class);

        $output->writeln('<info>Cleaning FavoriteShare ...</info>');
        $output->writeln(sprintf('<comment>Days of life: %d</comment>', $daysOfLife));

        $garbageCount = $repository->getGarbageCount($daysOfLife);
        $output->writeln(sprintf('<comment>Garbage count: %d</comment>', $garbageCount));

        if (0 === $garbageCount) {
            $output->writeln(sprintf('<info>Nothing to do</info>'));

            return;
        }

        if ($input->isInteractive()) {
            /** @var \Symfony\Component\Console\Helper\SymfonyQuestionHelper $helper */
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('<question>Continue?</question>', false);

            if (!$helper->ask($input, $output, $question)) {
                return;
            }
        }

        if (false === (bool) $input->getOption('dry-run')) {
            $repository->garbage($daysOfLife);
        }

        $output->writeln('<info>Complete</info>');
    }
}
