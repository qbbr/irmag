<?php

namespace Irmag\FavoriteBundle\Command;

use Irmag\BasketBundle\Command\IrmagCleanerBasketCommand;

class IrmagCleanerFavoriteCommand extends IrmagCleanerBasketCommand
{
    protected $entityFQCN = \Irmag\FavoriteBundle\Entity\Favorite::class;
    protected $commandSuffixName = 'favorite';
}
