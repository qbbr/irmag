<?php

namespace Irmag\FavoriteBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\Collection;
use Irmag\BasketBundle\Entity\BasketElement;
use Irmag\BasketBundle\Factory\BasketFactory;
use Irmag\SiteBundle\Entity\Element;
use Irmag\FavoriteBundle\Entity\Favorite;
use Irmag\FavoriteBundle\Factory\FavoriteFactory;

class FavoriteManager
{
    /**
     * @var Favorite
     */
    private $favorite;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var FavoriteFactory
     */
    private $favoriteFactory;

    /**
     * @var BasketFactory
     */
    private $basketFactory;

    /**
     * @param EntityManager   $em
     * @param FavoriteFactory $favoriteFactory
     *                                         //param BasketFactory   $basketFactory
     */
    public function __construct(
        EntityManager $em,
        FavoriteFactory $favoriteFactory
        //BasketFactory $basketFactory
    ) {
        $this->favoriteFactory = $favoriteFactory;
        //$this->basketFactory = $basketFactory;
        $this->em = $em;
    }

    /**
     * @param string $name Название избранного
     *
     * @return Favorite
     */
    public function getFavorite(string $name = null): Favorite
    {
        if (null === $this->favorite) {
            $this->favorite = $this->favoriteFactory->get($name);
        }

        return $this->favorite;
    }

    /**
     * @param Favorite $favorite
     *
     * @return FavoriteManager
     */
    public function setFavorite(Favorite $favorite): self
    {
        $this->favorite = $favorite;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getElements(): Collection
    {
        return $this->getFavorite()->getElements();
    }

    /**
     * Создаёт новое избранное.
     * Только для аутентифицированных пользователей.
     *
     * @param string $name Название избранного
     *
     * @return FavoriteManager
     */
    public function create(string $name = null)
    {
        $favorite = $this->favoriteFactory->getAuthenticated($name);

        $this->em->persist($favorite);
        $this->em->flush();

        return $this;
    }

    /**
     * Очищает избранное.
     * Удаляет все товары, но сама сущьность остаётся.
     *
     * @param string $name Название избранного
     *
     * @return FavoriteManager
     */
    public function clear(string $name = null): self
    {
        $favorite = $this->getFavorite($name);
        $favorite->getElements()->clear();

        return $this;
    }

    /**
     * Удаляет избранное со всеми его товарами.
     *
     * @param string $name Название избранного
     *
     * @throws FavoriteManagerException
     *
     * @return FavoriteManager
     */
    public function remove(string $name = null): self
    {
        if (Favorite::DEFAULT_FAVORITE_NAME === $name) {
            throw new FavoriteManagerException('Could not remove default favorite.');
        }

        $favorite = $this->getFavorite($name);
        $this->em->remove($favorite);

        return $this;
    }

    /**
     * Переименовывает избранное.
     *
     * @param string $name    Название избранного
     * @param string $newName Новое название избранного
     *
     * @return FavoriteManager
     */
    public function rename(string $name, string $newName): self
    {
        $favorite = $this->getFavorite($name);
        $favorite->setName($newName);
        $this->em->persist($favorite);
        $this->em->flush();

        return $this;
    }

    /**
     * Добавляет товар в избранное.
     *
     * @param Element $element Товар
     * @param string  $name    Название избранного
     *
     * @throws FavoriteManagerException When element already exists
     *
     * @return FavoriteManager When element already exists
     */
    public function addElement(Element $element, string $name = null): self
    {
        $favorite = $this->getFavorite($name);

        if ($favorite->hasElement($element)) {
            throw new FavoriteManagerException(sprintf('Element with id "%d" already exists.', $element->getId()));
        }

        $favorite->addElement($element);

        return $this;
    }

    /**
     * Удаляет товар из избранного.
     *
     * @param Element $element Товар
     * @param string  $name    Название избранного
     *
     * @throws FavoriteManagerException When element is not contains in favorite
     *
     * @return FavoriteManager When element is not contains in favorite
     */
    public function removeElement(Element $element, string $name = null): self
    {
        $favorite = $this->getFavorite($name);

        if (!$favorite->hasElement($element)) {
            throw new FavoriteManagerException(
                sprintf('Could not remove element with id "%d", because favorite not contains it.', $element->getId())
            );
        }

        $favorite->removeElement($element);

        return $this;
    }

    /**
     * @param string  $favoriteNameFrom Название избранного
     * @param string  $favoriteNameTo   Название избранного
     * @param Element $element          Товар
     *
     * @return FavoriteManager
     */
    public function moveElementToFavoriteFromFavorite(string $favoriteNameFrom, string $favoriteNameTo, Element $element): self
    {
        $favoriteFrom = $this->getFavorite($favoriteNameFrom);
        $favoriteTo = $this->getFavorite($favoriteNameTo);

        $this
            ->setFavorite($favoriteFrom)->removeElement($element)
            ->setFavorite($favoriteTo)->addElement($element)
            ->setFavorite(null);

        return $this;
    }

    public function copyAllElementsToBasket(Favorite $favorite)
    {
        $basket = $this->basketFactory->get();
        $favorite = $this->favoriteFactory->get();

        /** @var BasketElement $basketElement */
        foreach ($basket->getBasketElements() as $basketElement) {
            $element = $basketElement->getElement();
        }
    }
}
