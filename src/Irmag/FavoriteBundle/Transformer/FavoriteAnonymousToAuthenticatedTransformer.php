<?php

namespace Irmag\FavoriteBundle\Transformer;

use Doctrine\ORM\EntityManager;
use Irmag\FavoriteBundle\Factory\FavoriteFactory;

class FavoriteAnonymousToAuthenticatedTransformer
{
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var FavoriteFactory
     */
    private $favoriteFactory;

    /**
     * @param EntityManager   $em
     * @param FavoriteFactory $favoriteFactory
     */
    public function __construct(
        EntityManager $em,
        FavoriteFactory $favoriteFactory
    ) {
        $this->em = $em;
        $this->favoriteFactory = $favoriteFactory;
    }

    /**
     * Перемещает товары из анонимного избранного в аутентифицированное.
     */
    public function transform()
    {
        $anonymousFavorite = $this->favoriteFactory->getAnonymous();
        $authenticatedFavorite = $this->favoriteFactory->getAuthenticated();

        foreach ($anonymousFavorite->getElements() as $element) {
            if (!$authenticatedFavorite->hasElement($element)) {
                $authenticatedFavorite->addElement($element);
            }
        }

        $this->em->persist($authenticatedFavorite);
        $this->em->remove($anonymousFavorite);
        $this->em->flush();
    }
}
