<?php

namespace Irmag\FavoriteBundle\EventListener;

use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Core\User\UserInterface;
use Irmag\FavoriteBundle\Transformer\FavoriteAnonymousToAuthenticatedTransformer;

class SecurityListener
{
    /**
     * @var FavoriteAnonymousToAuthenticatedTransformer
     */
    private $transformer;

    /**
     * @param FavoriteAnonymousToAuthenticatedTransformer $transformer
     */
    public function __construct(FavoriteAnonymousToAuthenticatedTransformer $transformer)
    {
        $this->transformer = $transformer;
    }

    /**
     * @param InteractiveLoginEvent $event
     */
    public function onInteractiveLogin(InteractiveLoginEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();

        if (!$user instanceof UserInterface) {
            return;
        }

        $this->transformer->transform();
    }
}
