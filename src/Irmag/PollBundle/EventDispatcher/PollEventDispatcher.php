<?php

namespace Irmag\PollBundle\EventDispatcher;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Irmag\PollBundle\Entity\Poll;
use Irmag\PollBundle\Event\PollEvent;
use Irmag\PollBundle\IrmagPollEvents;

class PollEventDispatcher
{
    /**
     * @var EventDispatcherInterface
     */
    private $eventDispatcher;

    /**
     * @param EventDispatcherInterface $eventDispatcher
     */
    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @param Poll $poll
     */
    public function dispatchPollVoted(Poll $poll)
    {
        $this->eventDispatcher->dispatch(IrmagPollEvents::POLL_USER_VOTED, $this->getPollEvent($poll));
    }

    /**
     * @param Poll $poll
     *
     * @return PollEvent
     */
    private function getPollEvent(Poll $poll)
    {
        return new PollEvent($poll);
    }
}
