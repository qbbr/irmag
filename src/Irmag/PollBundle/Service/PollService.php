<?php

namespace Irmag\PollBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\ProfileBundle\Service\UserTrait;
use Irmag\PollBundle\Exception\IrmagPollException;
use Irmag\PollBundle\Entity\PollOpinion;
use Irmag\PollBundle\Entity\Poll;

class PollService
{
    use UserTrait;

    /**
     * Ключ для хранения проголосовавшего в сессии.
     */
    const POLL_VOTER_KEY = 'irmag_poll_voter_key';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var array
     */
    private $owners;

    /**
     * @var Poll
     */
    private $poll;

    /**
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     * @param SessionInterface       $session
     * @param array                  $owners
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        SessionInterface $session,
        array $owners
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->session = $session;
        $this->owners = $owners;
    }

    /**
     * Получить список объектов для привязки опросов.
     */
    public function getOwnersList(): array
    {
        $list = [];

        foreach ($this->owners as $owner) {
            $list[$owner['name']] = stripslashes($owner['fqcn']);
        }

        return $list;
    }

    /**
     * @param Poll $poll
     */
    public function setPoll(Poll $poll): void
    {
        $this->poll = $poll;
    }

    /**
     * Сохранить выбор.
     *
     * @param array $variants Мнения
     */
    public function saveOpinions(array $variants): void
    {
        if (!empty($variants) && !$this->isAlreadyVote() && !$this->isEstimated() && $this->poll->getIsActive()) {
            foreach ($variants as $var) {
                $opinion = new PollOpinion();
                $opinion->setPollVariant($var);
                $opinion->setPoll($this->poll);

                if (!$this->poll->getIsAnonymous()) {
                    $opinion->setUser($this->getUser(true));
                } else {
                    // если опрос анонимный, но пользователь залогинен - надо этим воспользоваться
                    if ($this->getUser()) {
                        $opinion->setUser($this->getUser());
                    } else {
                        $this->session->set(self::POLL_VOTER_KEY, true);
                    }
                }

                $this->em->persist($opinion);
                $this->em->flush();
            }

            $totalVoters = $this->poll->getTotalVoters();
            $this->poll->setTotalVoters(++$totalVoters);

            $this->em->persist($this->poll);
            $this->em->flush();
        }
    }

    /**
     * Показывать форму опроса?
     *
     * @throws \Exception
     *
     * @return bool
     */
    public function isShowForm(): bool
    {
        $poll = $this->getCurrentPollOrThrowException();

        return $poll->getIsActive() && !$this->isEstimated() && $this->isAvailableForUser() && !$this->isAlreadyVote();
    }

    /**
     * Вернуть лидирующий результат голосования.
     * Если никто не голосовал, либо есть 2 и более результата с максимумом, то возвращает 0 (нет лидера).
     *
     * @return int
     */
    public function getLeadResult(): int
    {
        $poll = $this->getCurrentPollOrThrowException();
        $countStats = array_count_values($poll->getStats());
        $max = 0;
        $maxIndex = 0;

        foreach ($poll->getStats() as $key => $statItem) {
            if ($statItem > $max) {
                $max = $statItem;
                $maxIndex = $key;
            }
        }

        return ($countStats[$max] > 1 || 1 === \count($countStats)) ? 0 : $maxIndex;
    }

    /**
     * Участвовал ли пользователь в этом опросе?
     *
     * @return bool
     */
    private function isAlreadyVote(): bool
    {
        $user = $this->getUser();

        if (!empty($user)) {
            $votes = $this->em->getRepository(PollOpinion::class)->findBy([
                'user' => $user,
                'poll' => $this->poll,
            ]);

            return !empty($votes);
        }

        return $this->session->has(self::POLL_VOTER_KEY);
    }

    /**
     * Завершен ли опрос по времени?
     *
     * @return bool
     */
    private function isEstimated(): bool
    {
        $estimatedAt = $this->poll->getEstimatedAt();

        if (!empty($estimatedAt)) {
            $now = new \DateTime();

            return $now > $estimatedAt;
        }

        return false;
    }

    /**
     * Может ли текущий пользователь голосовать?
     *
     * @return bool
     */
    private function isAvailableForUser(): bool
    {
        return $this->poll->getIsAnonymous() || !$this->poll->getIsAnonymous() && $this->getUser();
    }

    /**
     * Возвращает текущий опрос или выбрасывает исключение.
     *
     * @throws IrmagPollException
     *
     * @return Poll
     */
    private function getCurrentPollOrThrowException(): Poll
    {
        if (empty($this->poll)) {
            throw new IrmagPollException('Poll did not set for PollService.');
        }

        return $this->poll;
    }
}
