<?php

namespace Irmag\PollBundle\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\SiteBundle\Service\BonusManager\BonusManager;
use Irmag\PollBundle\Entity\PollOpinion;
use Irmag\PollBundle\Event\PollEvent;
use Irmag\PollBundle\IrmagPollEvents;

class PollEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var BonusManager
     */
    private $bonusManager;

    /**
     * @param BonusManager $bonusManager
     */
    public function __construct(BonusManager $bonusManager)
    {
        $this->bonusManager = $bonusManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagPollEvents::POLL_USER_VOTED => 'payBonusForPollVoting',
        ];
    }

    /**
     * @param PollEvent $event
     */
    public function payBonusForPollVoting(PollEvent $event)
    {
        $this->bonusManager->update(
            PollOpinion::USER_BONUS_FOR_VOTING,
            sprintf('Бонус за участие в опросе "%s"', $event->getPoll()->getTitle())
        );
    }
}
