<?php

namespace Irmag\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Event\LifecycleEventArgs;

/**
 * @ORM\Table(name="polls_variants")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PollVariant
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Poll
     *
     * @ORM\ManyToOne(targetEntity="Poll", inversedBy="variants", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $poll;

    /**
     * Текст (HTML) варианта голосования.
     *
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $textHtml;

    /**
     * @var Collection|PollOpinion[]
     *
     * @ORM\OneToMany(targetEntity="PollOpinion", mappedBy="pollVariant")
     */
    private $opinions;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", options={"default": 0})
     */
    private $totalVotes;

    public function __construct()
    {
        $this->totalVotes = 0;
        $this->opinions = new ArrayCollection();
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @ORM\PostPersist
     */
    public function updatePollStatsStructure(LifecycleEventArgs $args)
    {
        /** @var PollVariant $object */
        $object = $args->getObject();
        $poll = $object->getPoll();

        /* Инициализация массива статистики, если его нет, иначе добавление в него нового варианта ответа */
        $stats = (empty($poll->getStats())) ? [] : $poll->getStats();
        if (empty($stats)) {
            foreach ($poll->getVariants() as $variant) {
                $stats[$variant->getId()] = 0;
            }
        } elseif (empty($stats[$object->getId()])) {
            $stats[$object->getId()] = 0;
        }

        $poll->setStats($stats);
        $args->getEntityManager()->persist($poll);
        $args->getEntityManager()->flush();
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @ORM\PreRemove
     */
    public function removeFromStats(LifecycleEventArgs $args)
    {
        /** @var PollVariant $object */
        $object = $args->getObject();
        $poll = $object->getPoll();
        $stats = $poll->getStats();

        unset($stats[$object->getId()]);
        $poll->setStats($stats);
        $args->getEntityManager()->persist($poll);
        $args->getEntityManager()->flush();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTextHtml(): ?string
    {
        return $this->textHtml;
    }

    public function setTextHtml(string $textHtml): self
    {
        $this->textHtml = $textHtml;

        return $this;
    }

    public function getTotalVotes(): ?int
    {
        return $this->totalVotes;
    }

    public function setTotalVotes(int $totalVotes): self
    {
        $this->totalVotes = $totalVotes;

        return $this;
    }

    public function getPoll(): ?Poll
    {
        return $this->poll;
    }

    public function setPoll(?Poll $poll): self
    {
        $this->poll = $poll;

        return $this;
    }

    /**
     * @return Collection|PollOpinion[]
     */
    public function getOpinions(): Collection
    {
        return $this->opinions;
    }

    public function addOpinion(PollOpinion $opinion): self
    {
        if (!$this->opinions->contains($opinion)) {
            $this->opinions[] = $opinion;
            $opinion->setPollVariant($this);
        }

        return $this;
    }

    public function removeOpinion(PollOpinion $opinion): self
    {
        if ($this->opinions->contains($opinion)) {
            $this->opinions->removeElement($opinion);
            // set the owning side to null (unless already changed)
            if ($opinion->getPollVariant() === $this) {
                $opinion->setPollVariant(null);
            }
        }

        return $this;
    }
}
