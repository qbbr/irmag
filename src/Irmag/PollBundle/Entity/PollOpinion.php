<?php

namespace Irmag\PollBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Table(name="polls_opinions")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PollOpinion
{
    use TimestampableEntity;

    /**
     * Бонус за участие в опросе.
     */
    const USER_BONUS_FOR_VOTING = 10;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var PollVariant
     *
     * @ORM\ManyToOne(targetEntity="PollVariant", inversedBy="opinions")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $pollVariant;

    /**
     * @var Poll
     *
     * @ORM\ManyToOne(targetEntity="Poll")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $poll;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=true)
     */
    private $user;

    /**
     * @param LifecycleEventArgs $args
     *
     * @ORM\PostPersist
     */
    public function addPollVoter(LifecycleEventArgs $args)
    {
        /** @var PollVariant $variant */
        $variant = $args->getObject()->getPollVariant();
        $poll = $variant->getPoll();

        /* Суммируем общее кол-во голосов за вариант */
        $totalVotes = $variant->getTotalVotes();
        $variant->setTotalVotes(++$totalVotes);

        /* Суммируем общее кол-во голосов в опросе */
        $totalPollVotes = $poll->getTotalVotes();
        $poll->setTotalVotes(++$totalPollVotes);

        /* Пишем в статистику */
        $stats = $poll->getStats();
        $stats[$variant->getId()] = ++$stats[$variant->getId()];
        $poll->setStats($stats);

        $em = $args->getEntityManager();

        $em->persist($poll);
        $em->persist($variant);
        $em->flush();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPollVariant(): ?PollVariant
    {
        return $this->pollVariant;
    }

    public function setPollVariant(?PollVariant $pollVariant): self
    {
        $this->pollVariant = $pollVariant;

        return $this;
    }

    public function getPoll(): ?Poll
    {
        return $this->poll;
    }

    public function setPoll(?Poll $poll): self
    {
        $this->poll = $poll;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
