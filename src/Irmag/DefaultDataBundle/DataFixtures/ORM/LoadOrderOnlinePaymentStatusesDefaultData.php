<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePaymentStatus;

class LoadOrderOnlinePaymentStatusesDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $paymentStatuses = [
            [0, OrderOnlinePaymentStatus::PAYMENT_STATUS_REGISTERED_SHORTNAME, 'Зарегистрирован'],
            [1, OrderOnlinePaymentStatus::PAYMENT_STATUS_APPROVED_SHORTNAME, 'Подтверждён'],
            [2, OrderOnlinePaymentStatus::PAYMENT_STATUS_DEPOSITED_SHORTNAME, 'Завершён'],
            [3, OrderOnlinePaymentStatus::PAYMENT_STATUS_REVERTED_SHORTNAME, 'Отменён'],
            [4, OrderOnlinePaymentStatus::PAYMENT_STATUS_REFUND_SHORTNAME, 'Возврат'],
            [5, OrderOnlinePaymentStatus::PAYMENT_STATUS_ACS_AUTHORIZED_SHORTNAME, 'Ожидает подтверждения'],
            [6, OrderOnlinePaymentStatus::PAYMENT_STATUS_DECLINED_SHORTNAME, 'Отклонён'],
        ];

        foreach ($paymentStatuses as $statuses) {
            $existingStatus = $manager->getRepository(OrderOnlinePaymentStatus::class)->findOneBy(['name' => $statuses[1]]);

            if (!$existingStatus) {
                $paymentStatus = new OrderOnlinePaymentStatus();
                $paymentStatus->setId($statuses[0]);
                $paymentStatus->setName($statuses[1]);
                $paymentStatus->setNameTranslated($statuses[2]);

                $manager->persist($paymentStatus);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 10;
    }
}
