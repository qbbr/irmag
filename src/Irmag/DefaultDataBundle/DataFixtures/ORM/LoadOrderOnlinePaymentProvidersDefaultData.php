<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePaymentProvider;

class LoadOrderOnlinePaymentProvidersDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $paymentProviders = [
            ['Сбербанк', 'sberbank'],
            ['Банк Открытие', 'openbank'],
        ];

        foreach ($paymentProviders as $provider) {
            $existingProvider = $manager->getRepository(OrderOnlinePaymentProvider::class)->findOneBy(['shortname' => $provider[1]]);

            if (!$existingProvider) {
                $paymentProvider = new OrderOnlinePaymentProvider();
                $paymentProvider->setName($provider[0]);
                $paymentProvider->setShortname($provider[1]);

                $manager->persist($paymentProvider);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 9;
    }
}
