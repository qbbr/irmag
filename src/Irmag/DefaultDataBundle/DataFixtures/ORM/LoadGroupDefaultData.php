<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\ProfileBundle\Entity\Group;

class LoadGroupDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $this->addGroup('Суперадминистраторы', 'super_administrators', 'Владеет всеми ролями.');
        $this->addGroup('Администраторы', 'administrators', 'Владеет всеми ролями, но не может редактировать "Суперадминистраторов".');
        $this->addGroup('Модераторы', 'moderators', "Не может войти под пользователем.\nНе может удалять роли, группы и пользователей.\n Не может редактировать роли и группы.");
        $this->addGroup('Пользователи', 'users', 'Обычный зарегистрированный пользователь.');
        $this->addGroup('Модераторы блога', 'blog_moderators', 'Модераторы постов в блоге.');
        $this->addGroup('Модераторы форума', 'forum_moderators', 'Модераторы постов и тем на форуме.');
        $this->addGroup('Модераторы комментариев', 'thread_comment_moderators', 'Модераторы комментариев на сайте и в блоге.');

        $this->manager->flush();
    }

    /**
     * Добавляет группу.
     *
     * @param string      $name
     * @param string      $shortname
     * @param string|null $description
     */
    private function addGroup(string $name, string $shortname, string $description = null): void
    {
        $existingGroup = $this->manager->getRepository(Group::class)->findOneBy(['shortname' => $shortname]);

        if (!$existingGroup) {
            $group = new Group();
            $group->setName($name);
            $group->setShortName($shortname);

            if (null !== $description) {
                $group->setDescription($description);
            }

            $this->manager->persist($group);
            $this->addReference('group_'.$shortname, $group);
        }
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 1;
    }
}
