<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\SiteBundle\Entity\OrderDeliveryTime;

class LoadOrderDeliveryTimesDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $deliveryTimes = [
            ['В любое время', '15:00:00'],
            ['11:00 - 14:00', '08:00:00'],
            ['14:00 - 16:00', '08:00:00'],
            ['17:00 - 19:00', '15:00:00'],
            ['19:00 - 21:00', '15:00:00'],
            ['12:00 - 16:00', '10:00:00'],
            ['11:00 - 16:00', '08:00:00'],
            ['17:00 - 21:00', '15:00:00'],
            ['9:00 - 16:30', '15:00:00'],
        ];

        foreach ($deliveryTimes as $data) {
            $existingDeliveryTime = $manager->getRepository(OrderDeliveryTime::class)->findOneBy(['name' => $data[0]]);

            if (!$existingDeliveryTime) {
                $deliveryTime = new OrderDeliveryTime();
                $deliveryTime->setName($data[0]);
                $deliveryTime->setTimeLimit(new \DateTime($data[1]));

                $manager->persist($deliveryTime);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 7;
    }
}
