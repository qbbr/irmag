<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\SiteBundle\Entity\Page;

class LoadPagesDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        // О нас
        $page = new Page();
        $page->setName('О нас');
        $page->setSlug('about-the-company');
        $page->setBody(file_get_contents(__DIR__.'/../../Resources/data/page/about-the-company.html'));

        $manager->persist($page);

        // Помощь
        $page = new Page();
        $page->setName('Помощь');
        $page->setSlug('help');
        $page->setBody(file_get_contents(__DIR__.'/../../Resources/data/page/help.html'));

        $manager->persist($page);

        // Правила публикаций в блоге
        $page = new Page();
        $page->setName('Правила публикаций');
        $page->setSlug('rules');
        $page->setBody(file_get_contents(__DIR__.'/../../Resources/data/page/rules.html'));

        $manager->persist($page);

        // Правила форума
        $page = new Page();
        $page->setName('Правила форума');
        $page->setSlug('forum-rules');
        $page->setBody(file_get_contents(__DIR__.'/../../Resources/data/page/forum-rules.html'));

        $manager->persist($page);
        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 15;
    }
}
