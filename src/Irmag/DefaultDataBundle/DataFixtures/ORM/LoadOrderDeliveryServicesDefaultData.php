<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\OrderDeliveryServiceBundle\Entity\OrderDeliveryService;

class LoadOrderDeliveryServicesDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $deliveryServices = [
            ['DPD', 'dpd'],
        ];

        foreach ($deliveryServices as $services) {
            $existingDeliveryService = $manager->getRepository(OrderDeliveryService::class)->findOneBy(['shortname' => $services[1]]);

            if (!$existingDeliveryService) {
                $deliveryService = new OrderDeliveryService();
                $deliveryService->setName($services[0]);
                $deliveryService->setShortname($services[1]);

                if (isset($services[2])) {
                    $deliveryService->setDescription($services[2]);
                }

                $manager->persist($deliveryService);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 11;
    }
}
