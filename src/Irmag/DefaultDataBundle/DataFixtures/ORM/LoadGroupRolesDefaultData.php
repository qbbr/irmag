<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\ProfileBundle\Entity\Role;
use Irmag\ProfileBundle\Entity\Group;

class LoadGroupRolesDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $adminGroup = $manager->getRepository(Group::class)->findOneBy(['shortname' => 'administrators']);
        $rootGroup = $manager->getRepository(Group::class)->findOneBy(['shortname' => 'super_administrators']);

        $adminLoginRole = $manager->getRepository(Role::class)->findOneBy(['name' => 'ROLE_SONATA_ADMIN']);
        $allowedToSwitchRole = $manager->getRepository(Role::class)->findOneBy(['name' => 'ROLE_ALLOWED_TO_SWITCH']);

        $rootGroup->addRole($adminLoginRole);
        $adminGroup->addRole($adminLoginRole);
        $rootGroup->addRole($allowedToSwitchRole);
        $adminGroup->addRole($allowedToSwitchRole);

        $manager->persist($rootGroup);
        $manager->persist($adminGroup);

        $sonataAdminRoles = $manager->getRepository(Role::class)->getSonataAdminRoles();

        foreach ($sonataAdminRoles as $role) {
            $adminGroup->addRole($role);
            $manager->persist($adminGroup);
        }

        $blogModeratorGroup = $manager->getRepository(Group::class)->findOneBy(['shortname' => 'blog_moderators']);
        $blogAdminRoles = [
            'ROLE_IRMAG_ADMIN_BLOG_POST_SECTION_ADMIN',
            'ROLE_IRMAG_ADMIN_BLOG_POST_ADMIN',
            'ROLE_IRMAG_ADMIN_BLOG_POST_ELEMENT_ADMIN',
            'ROLE_IRMAG_ADMIN_BLOG_POST_ATTACHMENT_ADMIN',
            'ROLE_IRMAG_ADMIN_BLOG_POST_TAG_ADMIN',
        ];

        $roles = $manager->getRepository(Role::class)->findBy(['name' => $blogAdminRoles]);

        foreach ($roles as $role) {
            $blogModeratorGroup->addRole($role);
            $manager->persist($blogModeratorGroup);
        }

        $forumModeratorGroup = $manager->getRepository(Group::class)->findOneBy(['shortname' => 'forum_moderators']);
        $forumAdminRoles = [
            'ROLE_IRMAG_ADMIN_FORUM_ADMIN',
            'ROLE_IRMAG_ADMIN_FORUM_TOPIC_ADMIN',
            'ROLE_IRMAG_ADMIN_FORUM_POST_ADMIN',
            'ROLE_IRMAG_ADMIN_FORUM_POST_ATTACHMENT_ADMIN',
        ];

        $roles = $manager->getRepository(Role::class)->findBy(['name' => $forumAdminRoles]);

        foreach ($roles as $role) {
            $forumModeratorGroup->addRole($role);
            $manager->persist($forumModeratorGroup);
        }

        $commentModeratorGroup = $manager->getRepository(Group::class)->findOneBy(['shortname' => 'thread_comment_moderators']);
        $role = $manager->getRepository(Role::class)->findOneBy(['name' => 'ROLE_IRMAG_ADMIN_CONTENT_THREAD_COMMENT_ADMIN']);
        $commentModeratorGroup->addRole($role);
        $manager->persist($commentModeratorGroup);

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 3;
    }
}
