<?php

namespace Irmag\DefaultDataBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Irmag\SiteBundle\Entity\OrderDeliveryMethod;

class LoadOrderDeliveryMethodsDefaultData extends AbstractFixture implements OrderedFixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        $deliveryMethods = [
            ['Курьером', 'courier', 'С понедельника по субботу.'],
            ['Самовывоз', 'selfservice', 'В будни по адресу Иркутск, пер. Восточный, 8/2. до 16:30.'],
            ['Курьером в удаленные города области', 'courier_other_cities', 'Доставка в выбранный вами город осуществляется нашим транспортом бесплатно при заказе от 3 000 рублей со 100% предоплатой. Время доставки вам сообщит оператор.'],
            ['Транспортной компанией', 'transport_company', 'Доставка осуществляется транспортной компанией «Энергия» при условии суммы заказа от 3 000 рублей и 100% предоплаты по Иркутской области и другим городам РФ. Стоимость доставки рассчитывается оператором индивидуально.'],
            ['Почтой России', 'russian_post', 'Доставка Почтой России по всему миру'],
        ];

        foreach ($deliveryMethods as $method) {
            $existingMethod = $manager->getRepository(OrderDeliveryMethod::class)->findOneBy(['shortname' => $method[1]]);

            if (!$existingMethod) {
                $deliveryMethod = new OrderDeliveryMethod();
                $deliveryMethod->setName($method[0]);
                $deliveryMethod->setShortname($method[1]);
                $deliveryMethod->setDescription($method[2]);

                $manager->persist($deliveryMethod);
            }
        }

        $manager->flush();
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return 6;
    }
}
