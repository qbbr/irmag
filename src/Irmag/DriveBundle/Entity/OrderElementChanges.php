<?php

namespace Irmag\DriveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serializer;
use Gedmo\Mapping\Annotation as Gedmo;
use Irmag\SiteBundle\Entity\Element;

/**
 * @ORM\Table(
 *     name="orders_elements_changes",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="order_element_changes_unique_indx", columns={"order_id", "element_id"})
 *     }
 * )
 * @ORM\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderElementChanges
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @serializer\expose
     * @serializer\groups({"api_order_element_changes"})
     */
    private $id;

    /**
     * @var OrderChanges
     *
     * @Assert\NotBlank
     *
     * @ORM\ManyToOne(targetEntity="Irmag\DriveBundle\Entity\OrderChanges", inversedBy="orderElement", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     *
     * @serializer\expose
     * @serializer\groups({"api_order_element_changes"})
     */
    private $order;

    /**
     * @var Element
     *
     * @ORM\ManyToOne(targetEntity="Irmag\SiteBundle\Entity\Element")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element_changes"})
     */
    private $element;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element_changes"})
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element_changes"})
     */
    private $amount;

    /**
     * @var \DateTimeInterface
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element_changes"})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_element_changes"})
     */
    private $updatedAt;

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->element ? $this->element->getName() : $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price = null): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount = null): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getOrder(): ?OrderChanges
    {
        return $this->order;
    }

    public function setOrder(?OrderChanges $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }
}
