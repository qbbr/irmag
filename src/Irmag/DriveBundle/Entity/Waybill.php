<?php

namespace Irmag\DriveBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Irmag\SiteBundle\Entity\Order;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Entity
 */
class Waybill
{
    use TimestampableEntity;

    /**
     * Ид.
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $id;

    /**
     * Заказы.
     *
     * @var Collection|Order[]
     *
     * @ORM\OneToMany(targetEntity="Irmag\SiteBundle\Entity\Order", mappedBy="waybill", cascade={"persist"})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $orders;

    /**
     * Смена.
     *
     * @var string
     *
     * @Assert\Length(max=32)
     *
     * @ORM\Column(type="string", length=32)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $shift;

    /**
     * Общая сумма.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $totalSum;

    /**
     * Сумма наличными.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, options={"default": 0}, nullable=true)
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $sumCash;

    /**
     * Сумма безнал.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, options={"default": 0}, nullable=true)
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $sumCard;

    /**
     * Сумма сертификат.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, options={"default": 0}, nullable=true)
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $sumCertificate;

    /**
     * Сумма расчётный счёт.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, options={"default": 0}, nullable=true)
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $sumRs;

    /**
     * Сумма онлайн.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, options={"default": 0}, nullable=true)
     *
     * @Assert\GreaterThanOrEqual(0)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order"})
     */
    private $sumOnline;

    /**
     * Водитель.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL", nullable=false)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $driver;

    /**
     * Дата выезда.
     *
     * @var \DateTimeInterface
     *
     * @ORM\Column(type="date")
     *
     * @Serializer\Type("DateTime<'d.m.Y'>")
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $date;

    /**
     * Комментарий.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $comment;

    /**
     * Кол-во довозов.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $quantityOfExtraDelivery;

    /**
     * Кол-во межгорода.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $quantityOutOfCity;

    /**
     * Пробег.
     *
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $mileage;

    /**
     * Выполнено.
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": false})
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_waybill"})
     */
    private $isCompleted;

    public function __construct()
    {
        $this->sumCash = 0.00;
        $this->sumCard = 0.00;
        $this->sumCertificate = 0.00;
        $this->sumRs = 0.00;
        $this->sumOnline = 0.00;
        $this->quantityOfExtraDelivery = 0;
        $this->quantityOutOfCity = 0;
        $this->mileage = 0;
        $this->isCompleted = false;
        $this->orders = new ArrayCollection();
    }

    public function __toString()
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getShift(): ?string
    {
        return $this->shift;
    }

    public function setShift(string $shift): self
    {
        $this->shift = $shift;

        return $this;
    }

    public function getTotalSum()
    {
        return $this->totalSum;
    }

    public function setTotalSum($totalSum): self
    {
        $this->totalSum = $totalSum;

        return $this;
    }

    public function getSumCash()
    {
        return $this->sumCash;
    }

    public function setSumCash($sumCash): self
    {
        $this->sumCash = $sumCash;

        return $this;
    }

    public function getSumCard()
    {
        return $this->sumCard;
    }

    public function setSumCard($sumCard): self
    {
        $this->sumCard = $sumCard;

        return $this;
    }

    public function getSumCertificate()
    {
        return $this->sumCertificate;
    }

    public function setSumCertificate($sumCertificate): self
    {
        $this->sumCertificate = $sumCertificate;

        return $this;
    }

    public function getSumRs()
    {
        return $this->sumRs;
    }

    public function setSumRs($sumRs): self
    {
        $this->sumRs = $sumRs;

        return $this;
    }

    public function getSumOnline()
    {
        return $this->sumOnline;
    }

    public function setSumOnline($sumOnline): self
    {
        $this->sumOnline = $sumOnline;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getQuantityOfExtraDelivery(): ?int
    {
        return $this->quantityOfExtraDelivery;
    }

    public function setQuantityOfExtraDelivery(?int $quantityOfExtraDelivery): self
    {
        $this->quantityOfExtraDelivery = $quantityOfExtraDelivery;

        return $this;
    }

    public function getQuantityOutOfCity(): ?int
    {
        return $this->quantityOutOfCity;
    }

    public function setQuantityOutOfCity(?int $quantityOutOfCity): self
    {
        $this->quantityOutOfCity = $quantityOutOfCity;

        return $this;
    }

    public function getMileage(): ?int
    {
        return $this->mileage;
    }

    public function setMileage(?int $mileage): self
    {
        $this->mileage = $mileage;

        return $this;
    }

    public function getIsCompleted(): ?bool
    {
        return $this->isCompleted;
    }

    public function setIsCompleted(bool $isCompleted): self
    {
        $this->isCompleted = $isCompleted;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setWaybill($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->contains($order)) {
            $this->orders->removeElement($order);
            // set the owning side to null (unless already changed)
            if ($order->getWaybill() === $this) {
                $order->setWaybill(null);
            }
        }

        return $this;
    }

    public function getDriver(): ?User
    {
        return $this->driver;
    }

    public function setDriver(?User $driver): self
    {
        $this->driver = $driver;

        return $this;
    }
}
