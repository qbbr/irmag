<?php

namespace Irmag\DriveBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as Serializer;
use Irmag\SiteBundle\Entity\OrderPaymentMethod;

/**
 * @ORM\Table(name="orders_changes")
 * @ORM\Entity
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderChanges
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes", "api_order_changes_id"})
     */
    private $id;

    /**
     * Список товара.
     *
     * @var Collection|OrderElementChanges[]
     *
     * @ORM\OneToMany(targetEntity="OrderElementChanges", mappedBy="order", cascade={"persist", "remove"}, orphanRemoval=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes"})
     */
    private $orderElement;

    /**
     * Способ оплаты.
     *
     * @var OrderPaymentMethod
     *
     * @ORM\ManyToOne(targetEntity="Irmag\SiteBundle\Entity\OrderPaymentMethod")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes"})
     */
    private $paymentMethod;

    /**
     * Статус заказа.
     *
     * @var OrderChangesStatus
     *
     * @ORM\ManyToOne(targetEntity="OrderChangesStatus")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes"})
     */
    private $status;

    /**
     * Смешанный платёж.
     *
     * @var OrderChangesMixedPayment
     *
     * @ORM\OneToOne(targetEntity="OrderChangesMixedPayment", mappedBy="order")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes"})
     */
    private $mixedPayment;

    /**
     * Комментарий.
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes"})
     */
    private $comment;

    /**
     * Сумма заказа.
     *
     * @var float
     *
     * @ORM\Column(type="decimal", scale=2, nullable=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes"})
     */
    private $totalPrice;

    /**
     * @var \DateTimeInterface
     *
     * @Gedmo\Timestampable(on="create")
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes"})
     */
    private $createdAt;

    /**
     * @var \DateTimeInterface
     *
     * @Gedmo\Timestampable(on="update")
     *
     * @ORM\Column(type="datetime")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes"})
     */
    private $updatedAt;

    public function __construct()
    {
        $this->orderElement = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return Collection|OrderElementChanges[]
     */
    public function getOrderElement(): Collection
    {
        return $this->orderElement;
    }

    public function addOrderElement(OrderElementChanges $orderElement): self
    {
        if (!$this->orderElement->contains($orderElement)) {
            $this->orderElement[] = $orderElement;
            $orderElement->setOrder($this);
        }

        return $this;
    }

    public function removeOrderElement(OrderElementChanges $orderElement): self
    {
        if ($this->orderElement->contains($orderElement)) {
            $this->orderElement->removeElement($orderElement);
            // set the owning side to null (unless already changed)
            if ($orderElement->getOrder() === $this) {
                $orderElement->setOrder(null);
            }
        }

        return $this;
    }

    public function getPaymentMethod(): ?OrderPaymentMethod
    {
        return $this->paymentMethod;
    }

    public function setPaymentMethod(?OrderPaymentMethod $paymentMethod): self
    {
        $this->paymentMethod = $paymentMethod;

        return $this;
    }

    public function getStatus(): ?OrderChangesStatus
    {
        return $this->status;
    }

    public function setStatus(?OrderChangesStatus $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getMixedPayment(): ?OrderChangesMixedPayment
    {
        return $this->mixedPayment;
    }

    public function setMixedPayment(?OrderChangesMixedPayment $mixedPayment): self
    {
        $this->mixedPayment = $mixedPayment;

        return $this;
    }

    public function getTotalPrice()
    {
        return $this->totalPrice;
    }

    public function setTotalPrice($totalPrice): self
    {
        $this->totalPrice = $totalPrice;

        return $this;
    }
}
