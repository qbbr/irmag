<?php

namespace Irmag\DriveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use JMS\Serializer\Annotation as Serializer;

/**
 * @ORM\Table(name="orders_changes_statuses")
 * @ORM\Entity
 *
 * @UniqueEntity(fields="shortname")
 *
 * @Serializer\ExclusionPolicy("all")
 */
class OrderChangesStatus
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes", "api_order_changes_status"})
     */
    private $id;

    /**
     * @var string
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=255)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes", "api_order_changes_status"})
     */
    private $name;

    /**
     * @var string
     *
     * @Assert\NotBlank
     *
     * @ORM\Column(type="string", length=32, unique=true)
     *
     * @Serializer\Expose
     * @Serializer\Groups({"api_order_changes", "api_order_changes_status"})
     */
    private $shortname;

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return sprintf('%s - %s', $this->shortname, $this->name);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): self
    {
        $this->shortname = $shortname;

        return $this;
    }
}
