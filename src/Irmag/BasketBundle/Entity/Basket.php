<?php

namespace Irmag\BasketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Table(
 *     name="baskets",
 *     indexes={
 *         @ORM\Index(columns={"tmp_session"})
 *     }
 * )
 * @ORM\Entity(repositoryClass="Irmag\BasketBundle\Repository\BasketRepository")
 */
class Basket
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Для аутентифицированной корзины.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE", unique=true)
     */
    private $user;

    /**
     * Для анонимной корзины.
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $tmpSession;

    /**
     * @var Collection|BasketElement[]
     *
     * @ORM\OneToMany(targetEntity="BasketElement", mappedBy="basket", cascade={"persist"}, orphanRemoval=true)
     */
    private $basketElements;

    /**
     * Потрачено фантиков.
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $spendBonus;

    public function __construct()
    {
        $this->basketElements = new ArrayCollection();
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    /**
     * XXX: Custom.
     *
     * @return Basket
     */
    public function setUpdatedAtNow(): self
    {
        $this->setUpdatedAt(new \DateTime());

        return $this;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setTmpSession(?string $tmpSession): self
    {
        $this->tmpSession = $tmpSession;

        return $this;
    }

    public function getTmpSession(): ?string
    {
        return $this->tmpSession;
    }

    public function setSpendBonus(?int $spendBonus): self
    {
        $this->spendBonus = $spendBonus;

        return $this;
    }

    public function getSpendBonus(): ?int
    {
        return $this->spendBonus;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function addBasketElement(BasketElement $basketElement): self
    {
        if (!$this->basketElements->contains($basketElement)) {
            $this->basketElements[] = $basketElement;
            $basketElement->setBasket($this);
        }

        return $this;
    }

    public function removeBasketElement(BasketElement $basketElement): self
    {
        if ($this->basketElements->contains($basketElement)) {
            $this->basketElements->removeElement($basketElement);
            // set the owning side to null (unless already changed)
            if ($basketElement->getBasket() === $this) {
                $basketElement->setBasket(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|BasketElement[]
     */
    public function getBasketElements(): Collection
    {
        return $this->basketElements;
    }
}
