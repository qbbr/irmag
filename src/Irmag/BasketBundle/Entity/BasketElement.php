<?php

namespace Irmag\BasketBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\SiteBundle\Entity\Element;

/**
 * @ORM\Table(name="baskets_elements")
 * @ORM\Entity(repositoryClass="Irmag\BasketBundle\Repository\BasketElementRepository")
 */
class BasketElement
{
    use TimestampableEntity;

    /**
     * @var Basket
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Basket", inversedBy="basketElements")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $basket;

    /**
     * @var Element
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="\Irmag\SiteBundle\Entity\Element", cascade={"persist"})
     * @ORM\JoinColumn(referencedColumnName="id")
     */
    private $element;

    /**
     * @var int
     *
     * @ORM\Column(type="smallint", options={"default": 1})
     */
    private $amount;

    public function __construct()
    {
        $this->amount = 1;
    }

    public function getAmount(): ?int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getBasket(): ?Basket
    {
        return $this->basket;
    }

    public function setBasket(?Basket $basket): self
    {
        $this->basket = $basket;

        return $this;
    }

    public function getElement(): ?Element
    {
        return $this->element;
    }

    public function setElement(?Element $element): self
    {
        $this->element = $element;

        return $this;
    }
}
