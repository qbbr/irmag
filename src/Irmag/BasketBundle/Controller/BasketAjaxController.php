<?php

namespace Irmag\BasketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\SiteBundle\Controller\Traits\GetElementByIdTrait;
use Irmag\SiteBundle\Service\PriceManager\Adapter\PromocodeException;
use Irmag\SiteBundle\Service\PriceManager\Adapter\PromocodeAdapter;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;

/**
 * @Route("/ajax/basket",
 *     methods={"POST"},
 *     defaults={"_format": "json"},
 *     requirements={"_format": "json"},
 *     condition="request.isXmlHttpRequest()",
 *     options={"expose": true}
 * )
 */
class BasketAjaxController extends Controller
{
    use GetElementByIdTrait;

    /**
     * @var BasketManager
     */
    private $basketManager;

    /**
     * @param BasketManager $basketManager
     */
    public function __construct(
        BasketManager $basketManager
    ) {
        $this->basketManager = $basketManager;
    }

    /**
     * @Route("/clear/", name="irmag_basket_ajax_clear")
     */
    public function clearAction(): JsonResponse
    {
        return new JsonResponse([
            'success' => $this->basketManager->clear(),
        ]);
    }

    /**
     * @Route("/add_element/", name="irmag_basket_ajax_add_element")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function addElementAction(Request $request): JsonResponse
    {
        $elementId = $request->request->get('elementId');
        $amount = $request->request->getInt('amount');

        if (!\is_array($elementId)) {
            $elementId = [$elementId];
        }

        foreach ($elementId as $id) {
            $element = $this->getElementById((int) $id);

            if (false === $this->basketManager->addElement($element, $amount)) {
                $success = false;
            }
        }

        return new JsonResponse([
            'success' => $success ?? true,
        ]);
    }

    /**
     * @Route("/remove_element/", name="irmag_basket_ajax_remove_element")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function removeAction(Request $request): JsonResponse
    {
        $elementId = $request->request->getInt('elementId');
        $element = $this->getElementById($elementId);

        return new JsonResponse([
            'success' => $this->basketManager->removeElement($element),
        ]);
    }

    /**
     * @Route("/move_element_to_favorite/", name="irmag_basket_ajax_move_element_to_favorite")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function moveElementToFavoriteAction(Request $request): JsonResponse
    {
        $elementId = $request->request->getInt('elementId');
        $favoriteName = $request->request->get('favoriteName');
        $element = $this->getElementById($elementId);

        return new JsonResponse([
            'success' => $this->basketManager->moveElementToFavorite($element, $favoriteName),
        ]);
    }

    /**
     * @Route("/move_all_elements_to_favorite/", name="irmag_basket_ajax_move_all_elements_to_favorite")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function moveAllElementsToFavoriteAction(Request $request): JsonResponse
    {
        $favoriteName = $request->request->get('favoriteName');

        return new JsonResponse([
            'success' => $this->basketManager->moveAllElementsToFavorite($favoriteName),
        ]);
    }

    /**
     * @Route("/change_element_amount/", name="irmag_basket_ajax_change_element_amount")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function changeElementAmountAction(Request $request): JsonResponse
    {
        $elementId = $request->request->getInt('elementId');
        $amount = $request->request->getInt('amount');
        $element = $this->getElementById($elementId);

        return new JsonResponse([
            'success' => $this->basketManager->changeElementAmount($element, $amount),
        ]);
    }

    /**
     * @Route("/activate_promocode/", name="irmag_basket_ajax_activate_promocode")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function activatePromocodeAction(Request $request): JsonResponse
    {
        $success = false;
        $promocodeAdapter = $this->get(PromocodeAdapter::class);

        try {
            $promocode = $promocodeAdapter->activate($request->request->get('code'));
            $success = true;
            $msg = sprintf(
                'Промокод "%s" со скидкой "%d%%" успешно активирован &#9786;.',
                $promocode->getCode(),
                $promocode->getDiscount()
            );
        } catch (PromocodeException $e) {
            $msg = $e->getMessage();
        }

        return new JsonResponse(['success' => $success, 'msg' => $msg]);
    }

    /**
     * @Route("/deactivate_promocode/", name="irmag_basket_ajax_deactivate_promocode")
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @return JsonResponse
     */
    public function deactivatePromocodeAction(): JsonResponse
    {
        $this->get(PromocodeAdapter::class)->removeSession();

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/data/", name="irmag_basket_ajax_data")
     *
     * @return JsonResponse
     */
    public function getDataAction(): JsonResponse
    {
        return new JsonResponse([
            'total-count' => $this->forward('Irmag\BasketBundle\Controller\BasketController::totalCountAction')->getContent(),
            'mini-basket' => $this->forward('Irmag\BasketBundle\Controller\BasketController::miniBasketAction')->getContent(),
        ]);
    }
}
