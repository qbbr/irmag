<?php

namespace Irmag\BasketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Irmag\CoreBundle\Exception\IrmagException;
use Irmag\SiteBundle\Entity\OrderDeliveryCity;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\BasketBundle\Service\DataCollector;

/**
 * @Route("/basket")
 */
class BasketController extends Controller
{
    const DEFAULT_CITY_SHORTNAME = 'irkutsk';

    /**
     * @Route("/", name="irmag_basket")
     */
    public function indexAction()
    {
        return $this->render('@IrmagBasket/index.html.twig');
    }

    /**
     * @Route("/content/", name="irmag_basket_ajax_content", options={"expose": true})
     *
     * @throws IrmagException When default city not found
     */
    public function indexContentAction()
    {
        $defaultCity = $this->get('doctrine.orm.default_entity_manager')
            ->getRepository(OrderDeliveryCity::class)
            ->findOneBy(['shortname' => self::DEFAULT_CITY_SHORTNAME]);

        if (!$defaultCity) {
            throw new IrmagException(sprintf('OrderDeliveryCity with shortname "%s" not found!', self::DEFAULT_CITY_SHORTNAME));
        }

        return $this->render('@IrmagBasket/index-content.html.twig', [
            'default_city' => $defaultCity,
        ]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @Route("/bonus_pay/",
     *     name="irmag_basket_bonus_pay",
     *     condition="request.isXmlHttpRequest()",
     *     options={"expose": true}
     * )
     */
    public function bonusPayAction()
    {
        $masterRequest = $this->get('request_stack')->getMasterRequest();
        $bonusMaxToPay = $this->get(DataCollector::class)->getData()->getBonusMaxToPay();
        $bonusValue = $masterRequest->request->getInt('value');

        if ($bonusValue < 1) {
            return new JsonResponse([
                'error' => 'Введённая Вами сумма фантиков не допустима.',
            ]);
        }

        if ($bonusValue > $bonusMaxToPay) {
            return new JsonResponse([
                'error' => sprintf('Введённая Вами сумма фантиков (%d ф.) превышает предельно допустимую в %d ф.', $bonusValue, $bonusMaxToPay),
            ]);
        }

        $this->get(BasketManager::class)->setSpendBonus($bonusValue);

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @Route("/cancel_bonus_pay/",
     *     name="irmag_basket_cancel_bonus_pay",
     *     condition="request.isXmlHttpRequest()",
     *     options={"expose": true}
     * )
     */
    public function cancelBonusPayAction()
    {
        $basket = $this->get(BasketManager::class)->getBasket();
        $basket->setSpendBonus(null);
        $em = $this->get('doctrine.orm.default_entity_manager');
        $em->persist($basket);
        $em->flush();

        return new JsonResponse(['success' => true]);
    }

    public function totalCountAction()
    {
        return $this->render('@IrmagBasket/total-count.html.twig');
    }

    public function miniBasketAction()
    {
        return $this->render('@IrmagBasket/mini-basket.html.twig');
    }
}
