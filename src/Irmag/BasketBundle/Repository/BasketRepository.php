<?php

namespace Irmag\BasketBundle\Repository;

use Irmag\BasketBundle\Repository\Traits\GarbageTrait;

class BasketRepository extends \Doctrine\ORM\EntityRepository
{
    use GarbageTrait;
}
