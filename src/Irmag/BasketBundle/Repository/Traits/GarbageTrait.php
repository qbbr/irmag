<?php

namespace Irmag\BasketBundle\Repository\Traits;

use Doctrine\ORM\QueryBuilder;

trait GarbageTrait
{
    /**
     * Чистит аутентифицированные корзины от мусора.
     * Для консольной команды сборки мусора.
     *
     * @param int $daysOfLife
     */
    public function garbageAuthenticated(int $daysOfLife): void
    {
        $this->getGarbageAuthenticatedQb($daysOfLife)
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * Возвращает кол-во мусора для аутентифицированных корзин.
     * Для консольной команды сборки мусора.
     *
     * @param int $daysOfLife
     *
     * @return int
     */
    public function getGarbageAuthenticatedCount(int $daysOfLife): int
    {
        return $this->getGarbageAuthenticatedQb($daysOfLife)
            ->select('COUNT(e)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Чистит анонимные корзины от мусора.
     * Для консольной команды сборки мусора.
     *
     * @param int $daysOfLife
     */
    public function garbageAnonymous(int $daysOfLife): void
    {
        $this->getGarbageAnonymousQb($daysOfLife)
            ->delete()
            ->getQuery()
            ->execute();
    }

    /**
     * Возвращает кол-во мусора для анонимных корзин.
     * Для консольной команды сборки мусора.
     *
     * @param int $daysOfLife
     *
     * @return int
     */
    public function getGarbageAnonymousCount(int $daysOfLife): int
    {
        return $this->getGarbageAnonymousQb($daysOfLife)
            ->select('COUNT(e)')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param int $daysOfLife
     *
     * @return QueryBuilder
     */
    private function getGarbageAuthenticatedQb(int $daysOfLife): QueryBuilder
    {
        return $this->getGarbageQb($daysOfLife)
            ->andWhere('e.user IS NOT NULL');
    }

    /**
     * @param int $daysOfLife
     *
     * @return QueryBuilder
     */
    private function getGarbageAnonymousQb(int $daysOfLife): QueryBuilder
    {
        return $this->getGarbageQb($daysOfLife)
            ->andWhere('e.user IS NULL AND e.tmpSession IS NOT NULL');
    }

    /**
     * @param int $daysOfLife
     *
     * @return QueryBuilder
     */
    private function getGarbageQb(int $daysOfLife): QueryBuilder
    {
        return $this->createQueryBuilder('e')
            ->where('e.updatedAt IS NOT NULL AND e.updatedAt < :date')
            ->setParameter('date', $this->getModifiedDate($daysOfLife));
    }

    /**
     * @param int $daysOfLife
     *
     * @return \DateTime
     */
    private function getModifiedDate(int $daysOfLife): \DateTime
    {
        return (new \DateTime())->modify(sprintf('-%d days', $daysOfLife));
    }
}
