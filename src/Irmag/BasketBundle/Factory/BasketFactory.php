<?php

namespace Irmag\BasketBundle\Factory;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Irmag\BasketBundle\Entity\Basket;

class BasketFactory
{
    /**
     * Ключ для хранения токена анонимной корзины.
     */
    const SESSION_TMP_ID = 'irmag_basket_tmp_id';

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SessionInterface
     */
    private $session;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    /**
     * @param EntityManagerInterface  $em
     * @param SessionInterface        $session
     * @param TokenStorageInterface   $tokenStorage
     * @param TokenGeneratorInterface $tokenGenerator
     */
    public function __construct(
        EntityManagerInterface $em,
        SessionInterface $session,
        TokenStorageInterface $tokenStorage,
        TokenGeneratorInterface $tokenGenerator
    ) {
        $this->em = $em;
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
        $this->tokenGenerator = $tokenGenerator;
    }

    /**
     * Возвращает корзину, если её нет, то создаёт.
     * Если пользователь не аутентифицирован, тогда создаётся временная корзина.
     *
     * @return Basket
     */
    public function get(): Basket
    {
        if ($this->isAuthenticated()) {
            return $this->getAuthenticated();
        }

        return $this->getAnonymous();
    }

    /**
     * @throws BasketFactoryException When user is not authenticated
     *
     * @return Basket
     */
    public function getAuthenticated(): Basket
    {
        $user = $this->getUser();

        if (null === $user) {
            throw new BasketFactoryException('User is not authenticated.');
        }

        $basket = $this->em->getRepository(Basket::class)->findOneBy(['user' => $user]);

        if (!$basket) {
            $basket = new Basket();
            $basket->setUser($user);
        }

        return $basket;
    }

    /**
     * @return Basket
     */
    public function getAnonymous(): Basket
    {
        $tmpSession = $this->getTmpSession();
        $basket = $this->em->getRepository(Basket::class)->findOneBy(['tmpSession' => $tmpSession]);

        if (!$basket) {
            $basket = new Basket();
            $basket->setTmpSession($tmpSession);
        }

        return $basket;
    }

    /**
     * @return bool
     */
    public function isAuthenticated(): bool
    {
        return null !== $this->getUser();
    }

    /**
     * Назначает пользователя.
     *
     * @param UserInterface $user
     */
    public function setUser(UserInterface $user = null)
    {
        $this->user = $user;
    }

    /**
     * Возвращает текущего пользователя.
     * За исключением случая, когда использовался метод setUser.
     *
     * @return UserInterface|null
     */
    private function getUser()
    {
        if (null !== $this->user) {
            return $this->user;
        }

        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!\is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }

    /**
     * Возвращает токен для анонимной корзины.
     *
     * @return string
     */
    private function getTmpSession(): string
    {
        if (!$this->session->has(self::SESSION_TMP_ID)) {
            $this->session->set(self::SESSION_TMP_ID, $this->tokenGenerator->generateToken());
        }

        return $this->session->get(self::SESSION_TMP_ID);
    }
}
