<?php

namespace Irmag\BasketBundle\Traits;

trait BasketContentTrait
{
    /**
     * @var string
     */
    private $sessionLiteral = 'basket_content';

    /**
     * Получить текущий состав корзины.
     */
    public function getBasketContentTree()
    {
        $basketElements = $this->basketManager->getBasketElements(true);
        $basketContent = [];

        foreach ($basketElements as $basketElement) {
            $basketContent[$basketElement->getElement()->getId()] = $basketElement->getAmount();
        }

        return $basketContent;
    }

    /**
     * Сохранить состояние товаров в корзине.
     */
    public function saveBasketContent()
    {
        $this->session->set($this->sessionLiteral, $this->getBasketContentTree());
    }

    /**
     * Получить состояние корзины на текущий момент.
     *
     * @return array
     */
    public function getBasketContent()
    {
        return $this->session->get($this->sessionLiteral, []);
    }

    /**
     * Изменилось ли состояние корзины с момента последнего расчёта?
     *
     * @return bool
     */
    public function isBasketContentChanged(): bool
    {
        $diff = [];
        $savedContent = $this->getBasketContent();

        if (!empty($savedContent)) {
            $currentContent = $this->getBasketContentTree();
            $diff = array_diff_assoc($currentContent, $savedContent);
        }

        return !empty($diff);
    }

    /**
     * Очищает сессию от данных.
     */
    public function clearBasketContentSessionData()
    {
        if ($this->session->has($this->sessionLiteral)) {
            $this->session->remove($this->sessionLiteral);
        }
    }

    /**
     * @param string $sessionKey
     * @param string $msg
     */
    public function addFlashMessageAboutBasketContentChanging(string $sessionKey, string $msg): void
    {
        $this
            ->session
            ->getFlashBag()
            ->add($sessionKey, $msg);
    }
}
