<?php

namespace Irmag\BasketBundle\Service;

use Doctrine\ORM\EntityManager;
use Doctrine\Common\Collections\Collection;
use Irmag\SiteBundle\Entity\Element;
use Irmag\BasketBundle\Entity\Basket;
use Irmag\BasketBundle\Entity\BasketElement;
use Irmag\BasketBundle\Factory\BasketFactory;

class BasketManager
{
    /**
     * @var Basket
     */
    private $basket;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var BasketFactory
     */
    private $basketFactory;

    /**
     * @param EntityManager $em
     * @param BasketFactory $basketFactory
     */
    public function __construct(
        EntityManager $em,
        BasketFactory $basketFactory
    ) {
        $this->em = $em;
        $this->basketFactory = $basketFactory;
    }

    /**
     * @return Basket
     */
    public function getBasket(): Basket
    {
        if (null === $this->basket) {
            $this->basket = $this->basketFactory->get();
        }

        return $this->basket;
    }

    /**
     * @param Basket $basket
     *
     * @return BasketManager
     */
    public function setBasket(Basket $basket): self
    {
        $this->basket = $basket;

        return $this;
    }

    /**
     * @return Collection[]
     */
    public function getElements(): array
    {
        $elements = [];
        $basket = $this->getBasket();

        /** @var BasketElement $basketElement */
        foreach ($basket->getBasketElements() as $basketElement) {
            $elements[] = $basketElement->getElement();
        }

        return $elements;
    }

    /**
     * Очищает корзину.
     * Удаляет все товары, но сама сущьность остаётся.
     *
     * @return BasketManager
     */
    public function clear(): self
    {
        $basket = $this->getBasket();
        $basket
            ->setUpdatedAtNow()
            ->getBasketElements()->clear();

        $this->em->persist($basket);

        return $this;
    }

    /**
     * Удаляет корзину со всеми его товарами.
     *
     * @return BasketManager
     */
    public function remove(): self
    {
        $basket = $this->getBasket();
        $this->em->remove($basket);

        return $this;
    }

    /**
     * Добавляет товар в корзину.
     *
     * @param Element $element  Товар
     * @param int     $quantity Количество
     *
     * @throws BasketManagerException When element is not active
     *
     * @return BasketManager
     */
    public function addElement(Element $element, int $quantity = 1): self
    {
        if (false === $element->getIsActive()) {
            throw new BasketManagerException(sprintf('Element with id "%d" is not active.', $element->getId()));
        }

        $basket = $this->getBasket();
        $basketElement = $this->getBasketElement($basket, $element);

        if ($basketElement) {
            $basketElement->setQuantity($basketElement->getQuantity() + $quantity);
        } else {
            $basketElement = new BasketElement();
            $basketElement->setBasket($basket);
            $basketElement->setElement($element);
            $basketElement->setQuantity($quantity);
        }

        $basket->addBasketElement($basketElement);

        $this->em->persist($basket);
        $this->em->persist($basketElement);
        $this->em->flush();

        return $this;
    }

    /**
     * Удаляет товар из корзины.
     *
     * @param Element $element Товар
     *
     * @return BasketManager
     */
    public function removeElement(Element $element): self
    {
        $basket = $this->getBasket();
        $basketElement = $this->getBasketElement($basket, $element);

        if ($basketElement) {
            $basket->setUpdatedAtNow();
            $this->em->persist($basket);
            $this->em->remove($basketElement);
            $this->em->flush();
        }

        return $this;
    }

    /**
     * @param Element $element  Товар
     * @param int     $quantity Количество
     *
     * @throws BasketManagerException When element is not found in basket
     *
     * @return BasketManager
     */
    public function setQuantity(Element $element, int $quantity): self
    {
        $basket = $this->getBasket();
        $basketElement = $this->getBasketElement($basket, $element);

        if (!$basketElement) {
            throw new BasketManagerException(sprintf('Element with id "%d" is not found in basket.'));
        }

        $basketElement->setQuantity($quantity);
        $basket->setUpdatedAtNow();

        $this->em->persist($basket);
        $this->em->persist($basketElement);
        $this->em->flush();

        return $this;
    }

    /**
     * @param Basket  $basket
     * @param Element $element
     *
     * @return BasketElement|null
     */
    private function getBasketElement(Basket $basket, Element $element)
    {
        return $this->em->getRepository(BasketElement::class)->findOneBy(['basket' => $basket, 'element' => $element]);
    }
}
