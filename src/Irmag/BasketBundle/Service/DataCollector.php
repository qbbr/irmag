<?php

namespace Irmag\BasketBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Irmag\ProfileBundle\Entity\User;
use Irmag\SiteBundle\Service\BasketFavoriteManager\BasketManager;
use Irmag\SiteBundle\Service\PriceManager\Adapter\ActionAdapter;
use Irmag\SiteBundle\Service\PriceManager\Adapter\PromocodeAdapter;
use Irmag\SiteBundle\Service\PriceManager\Adapter\UserAdapter;
use Irmag\SiteBundle\Service\PriceManager\PriceManager;
use Irmag\SiteBundle\Utils\BonusCalculator;
use Irmag\SiteBundle\Entity\Promocode;

class DataCollector implements DataCollectorGetInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var BasketManager
     */
    private $basketManager;

    /**
     * @var PriceManager
     */
    private $priceManager;

    /**
     * @var ActionAdapter
     */
    private $actionAdapter;

    /**
     * @var PromocodeAdapter
     */
    private $promocodeAdapter;

    /**
     * @var UserAdapter
     */
    private $userAdapter;

    /**
     * @var bool
     */
    private $collected = false;

    /** @var array */
    private $basketElements = [];
    /** @var int */
    private $basketElementsCount = 0;
    /** @var int */
    private $elementsCount = 0;
    /** @var float */
    private $totalPrice = 0.00;
    /** @var float */
    private $totalPriceWithoutDiscount = 0.00;
    /** @var float */
    private $totalDiscount = 0.0;
    /** @var float */
    private $totalDiscountByUser = 0.0;
    /** @var float */
    private $totalDiscountByAction = 0.0;
    /** @var float */
    private $totalDiscountByPromocode = 0.0;
    /** @var int */
    private $totalWeight = 0;
    /** @var int */
    private $totalVolume = 0;
    /** @var int */
    private $bonusSpend = 0;
    /** @var int */
    private $bonusMaxToPay = 0;
    /** @var int */
    private $bonusReceive = 0;
    /** @var int */
    private $bonusReceiveInPercent = 0;
    /** @var bool */
    private $hasOnlyForHome = false;
    /** @var Promocode|null */
    private $promocode;

    /**
     * @param EntityManagerInterface $em
     * @param TokenStorageInterface  $tokenStorage
     * @param BasketManager          $basketManager
     * @param PriceManager           $priceManager
     * @param ActionAdapter          $actionAdapter
     * @param PromocodeAdapter       $promocodeAdapter
     * @param UserAdapter            $userAdapter
     */
    public function __construct(
        EntityManagerInterface $em,
        TokenStorageInterface $tokenStorage,
        BasketManager $basketManager,
        PriceManager $priceManager,
        ActionAdapter $actionAdapter,
        PromocodeAdapter $promocodeAdapter,
        UserAdapter $userAdapter
    ) {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
        $this->basketManager = $basketManager;
        $this->priceManager = $priceManager;
        $this->actionAdapter = $actionAdapter;
        $this->promocodeAdapter = $promocodeAdapter;
        $this->userAdapter = $userAdapter;
    }

    /**
     * @return \Irmag\BasketBundle\Entity\BasketElement[]
     */
    public function getBasketElements(): array
    {
        return $this->basketElements;
    }

    public function getBasketElementsCount(): int
    {
        return $this->basketElementsCount;
    }

    public function getElementsCount(): int
    {
        return $this->elementsCount;
    }

    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    public function getTotalPriceWithoutDiscount(): float
    {
        return $this->totalPriceWithoutDiscount;
    }

    public function getTotalDiscount(): float
    {
        return $this->totalDiscount;
    }

    public function getTotalDiscountByUser(): float
    {
        return $this->totalDiscountByUser;
    }

    public function getTotalDiscountByAction(): float
    {
        return $this->totalDiscountByAction;
    }

    public function getTotalDiscountByPromocode(): float
    {
        return $this->totalDiscountByPromocode;
    }

    public function getTotalWeight(): int
    {
        return $this->totalWeight;
    }

    public function getTotalVolume(): int
    {
        return $this->totalVolume;
    }

    public function getBonusSpend(): int
    {
        return $this->bonusSpend;
    }

    public function getBonusMaxToPay(): int
    {
        return $this->bonusMaxToPay;
    }

    public function getBonusReceive(): int
    {
        return $this->bonusReceive;
    }

    public function getBonusReceiveInPercent(): int
    {
        return $this->bonusReceiveInPercent;
    }

    public function hasOnlyForHome(): bool
    {
        return $this->hasOnlyForHome;
    }

    /**
     * @return Promocode|null
     */
    public function getPromocode(): ?Promocode
    {
        return $this->promocode;
    }

    /**
     * @return DataCollector
     */
    public function getData(): self
    {
        $this->collect();

        return $this;
    }

    /**
     * Collect basket data.
     */
    private function collect(): void
    {
        if (true === $this->collected) {
            return;
        }

        $basket = $this->basketManager->getBasket();
        $basketElements = $this->basketManager->getBasketElements(true);

        if ($basketElements) {
            $this->basketElements = $basketElements;
            $promocodeDiscount = $this->promocodeAdapter->getDiscount();
            $this->promocode = $this->promocodeAdapter->getPromocode();
            $userDiscount = $this->userAdapter->getDiscount();

            $isPromocodeForElementList = !empty($this->promocode) && !empty($this->promocode->getElementList());
            $elementsByPromocode = $isPromocodeForElementList ? $this->promocode->getElementList()->getElements() : [];

            foreach ($basketElements as $basketElement) {
                $skipCalculateUserDiscount = false;
                $element = $basketElement->getElement();
                $amount = $basketElement->getAmount();
                $price = $element->getPrice();

                $this->totalPriceWithoutDiscount += $price * $amount;
                $this->totalPrice += $this->priceManager->getPrice($element) * $amount;

                $this->totalWeight += $element->getWeight() * $amount;
                $this->totalVolume += $element->getVolume() * $amount;

                if (false === $this->hasOnlyForHome && true === $element->getIsOnlyForHome()) {
                    $this->hasOnlyForHome = true;
                }

                if ($this->actionAdapter->exists($element) && $elementActionPrice = $this->actionAdapter->getPrice($element)) {
                    // скидка по акции
                    $this->totalDiscountByAction += ($price - $elementActionPrice) * $amount;
                    $skipCalculateUserDiscount = true;
                } elseif ($element->getPropSpecialPrice()) {
                    // спец.цена
                    $skipCalculateUserDiscount = true;
                } elseif ($promocodeDiscount && $promocodeDiscount > $userDiscount) {
                    // скидка по промокоду
                    if (
                        (true === $isPromocodeForElementList && $elementsByPromocode->contains($basketElement->getElement()))
                        ||
                        (false === $isPromocodeForElementList && $this->promocode)
                    ) {
                        $this->totalDiscountByPromocode += $this->getDiscountByPriceAndDiscountPercent($price * $amount, $promocodeDiscount);
                        $skipCalculateUserDiscount = true;
                    }
                }

                if (false === $skipCalculateUserDiscount && $userDiscount > 0) {
                    // скидка пользователя
                    $this->totalDiscountByUser += $this->getDiscountByPriceAndDiscountPercent($price * $amount, $userDiscount);
                }

                ++$this->basketElementsCount;
                $this->elementsCount += $basketElement->getAmount();
            }

            $this->totalDiscount = $this->totalPriceWithoutDiscount - $this->totalPrice;
        }

        if ($this->isAuthenticated() && $basket) {
            $user = $this->getUser();
            $this->bonusSpend = (int) $basket->getSpendBonus();
            $this->bonusMaxToPay = $this->getSpendBonusMaxToPay($this->totalPrice, $user->getBonus());

            // если есть оплата фантиками, то
            if ($this->bonusSpend) {
                // проверяем оплату фантиками на допустимое значение (50%)
                if ($this->bonusSpend > $this->bonusMaxToPay) {
                    $basket->setSpendBonus(null);
                    $this->em->persist($basket);
                    $this->em->flush();
                }

                // вычитаем из суммарной цены
                $this->totalPrice -= $this->bonusSpend;
            }
        }

        $this->bonusReceive = BonusCalculator::getBonusByPrice($this->totalPrice);
        $this->bonusReceiveInPercent = BonusCalculator::getBonusPercentByPrice($this->totalPrice);

        $this->collected = true;
    }

    /**
     * Возвращает максимальное кол-во фантиков допустимое для оплаты.
     *
     * @param float $priceTotalWithDiscount
     * @param int   $userBonus
     *
     * @return int
     */
    private function getSpendBonusMaxToPay(float $priceTotalWithDiscount, int $userBonus): int
    {
        $spendBonusMaxToPay = $priceTotalWithDiscount / 2;

        return (int) floor($userBonus < $spendBonusMaxToPay ? $userBonus : $spendBonusMaxToPay);
    }

    /**
     * Возвращает скидку.
     *
     * @param float $price           Цена
     * @param int   $discountPercent Скидка в процентах
     *
     * @return float
     */
    private function getDiscountByPriceAndDiscountPercent(float $price, int $discountPercent): float
    {
        if (0 === $discountPercent) {
            return $price;
        }

        return $price / 100 * $discountPercent;
    }

    /**
     * Аутентифицирован.
     *
     * @return bool
     */
    private function isAuthenticated(): bool
    {
        $token = $this->tokenStorage->getToken();

        return null !== $token && $token->getUser() instanceof User;
    }

    /**
     * Возвращает текущего пользователя.
     *
     * @return User|null
     */
    private function getUser(): ?User
    {
        $token = $this->tokenStorage->getToken();

        if (null !== $token && $token->getUser() instanceof User) {
            return $token->getUser();
        }

        return null;
    }
}
