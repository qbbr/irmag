<?php

namespace Irmag\BasketBundle\Service;

use Irmag\SiteBundle\Entity\Promocode;

/**
 * Все данные храняться в рамках одного цыкла покупки.
 */
interface DataCollectorGetInterface
{
    /** Кол-во наименований (позиций) */
    public function getBasketElementsCount(): int;

    /** Кол-во товаров (учитывая amount) */
    public function getElementsCount(): int;

    /** Суммарная цена (со скидками) */
    public function getTotalPrice(): float;

    /** Суммарная цена без скидок */
    public function getTotalPriceWithoutDiscount(): float;

    /** Суммарная скидка */
    public function getTotalDiscount(): float;

    /** Суммарная скидка пользователя */
    public function getTotalDiscountByUser(): float;

    /** Суммарная скидка по акции */
    public function getTotalDiscountByAction(): float;

    /** Суммарная скидка по промокодам */
    public function getTotalDiscountByPromocode(): float;

    /** Общий вес (г.) */
    public function getTotalWeight(): int;

    /** Общий объём (см3.) */
    public function getTotalVolume(): int;

    /** Оплачено фантиками */
    public function getBonusSpend(): int;

    /** Максимальное кол-во фантиков допустимое для оплаты */
    public function getBonusMaxToPay(): int;

    /** Фантики за заказ */
    public function getBonusReceive(): int;

    /** Фантики за заказ в процентах */
    public function getBonusReceiveInPercent(): int;

    /** В корзине есть товар только для дома, з.н заказ недоступен к доставке в регионы */
    public function hasOnlyForHome(): bool;

    /** Промокод */
    public function getPromocode(): ?Promocode;
}
