<?php

namespace Irmag\BasketBundle\Command;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

class IrmagCleanerBasketCommand extends Command
{
    protected $entityFQCN = \Irmag\BasketBundle\Entity\Basket::class;
    protected $commandSuffixName = 'basket';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(
        EntityManagerInterface $em
    ) {
        $this->em = $em;

        // you *must* call the parent constructor
        parent::__construct();
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName(sprintf('irmag:cleaner:%s', $this->commandSuffixName))
            ->setDescription(sprintf('Garbage cleaner for %s', ucfirst($this->commandSuffixName)))
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Execute the command as a dry run')
            ->addOption('authenticated-days-of-life', null, InputOption::VALUE_REQUIRED, sprintf('Days of life for authenticated %s', $this->commandSuffixName), 180)
            ->addOption('anonymous-days-of-life', null, InputOption::VALUE_REQUIRED, sprintf('Days of life for anonymous %s', $this->commandSuffixName), 7)
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $authenticatedDaysOfLife = (int) $input->getOption('authenticated-days-of-life');
        $anonymousDaysOfLife = (int) $input->getOption('anonymous-days-of-life');
        $repository = $this->em->getRepository($this->entityFQCN);

        $output->writeln(sprintf('<info>Cleaning entity "%s" ...</info>', $this->entityFQCN));
        $output->writeln(sprintf('<comment>Authenticated days of life: %d</comment>', $authenticatedDaysOfLife));
        $output->writeln(sprintf('<comment>Anonymous days of life: %d</comment>', $anonymousDaysOfLife));

        $garbageAuthenticatedCount = $repository->getGarbageAuthenticatedCount($authenticatedDaysOfLife);
        $garbageAnonymousCount = $repository->getGarbageAnonymousCount($anonymousDaysOfLife);

        $output->writeln(sprintf('<comment>Garbage authenticated count: %d</comment>', $garbageAuthenticatedCount));
        $output->writeln(sprintf('<comment>Garbage anonymous count: %d</comment>', $garbageAnonymousCount));

        if (0 === $garbageAuthenticatedCount && 0 === $garbageAnonymousCount) {
            $output->writeln(sprintf('<info>Nothing to do</info>'));

            return null;
        }

        if ($input->isInteractive()) {
            /** @var \Symfony\Component\Console\Helper\SymfonyQuestionHelper $helper */
            $helper = $this->getHelper('question');
            $question = new ConfirmationQuestion('<question>Continue?</question>', false);

            if (!$helper->ask($input, $output, $question)) {
                return null;
            }
        }

        if (false === (bool) $input->getOption('dry-run')) {
            $repository->garbageAuthenticated($authenticatedDaysOfLife);
            $repository->garbageAnonymous($anonymousDaysOfLife);
        }

        $output->writeln('<info>Complete</info>');
    }
}
