<?php

namespace Irmag\RestApiBundle\Controller;

use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class AbstractRestController extends FOSRestController
{
    /**
     * @var string
     */
    protected $entityClassName;

    /**
     * @return EntityManager
     */
    public function getEntityManager(): EntityManager
    {
        return $this->get('doctrine.orm.default_entity_manager');
    }

    /**
     * Обработка формы.
     *
     * @param Request $request  Request class
     * @param object  $entity   ORM сущность
     * @param string  $formType The fully qualified class name of the form type
     *
     * @return View
     */
    protected function processForm(Request $request, $entity, string $formType): View
    {
        $statusCode = Response::HTTP_BAD_REQUEST;

        $form = $this->createForm($formType, $entity, ['csrf_protection' => false, 'method' => $request->getMethod()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getEntityManager();
            $em->persist($entity);
            $em->flush();

            if (Request::METHOD_POST === $request->getMethod()) {
                // если создали новую сущность, то возвращаем id
                return new View(['id' => $entity->getId()], Response::HTTP_CREATED);
            } elseif (Request::METHOD_PATCH === $request->getMethod()) {
                $statusCode = Response::HTTP_NO_CONTENT;
            }
        }

        return new View($form, $statusCode);
    }

    /**
     * Получить сущность по id.
     *
     * @param int         $id              Entity Id
     * @param string|null $entityClassName
     *
     * @return mixed
     */
    protected function getEntityByIdOrThrowNotFoundHttpException(int $id, string $entityClassName = null)
    {
        $entityClassName = $entityClassName ?? $this->entityClassName;
        $entity = $this->getEntityManager()->getRepository($entityClassName)->find($id);

        if (!$entity) {
            throw new NotFoundHttpException(
                sprintf('Entity "%s" with id "%d" not found.', $entityClassName, $id)
            );
        }

        return $entity;
    }

    /**
     * Удаляет сущность.
     *
     * @param object $entity
     */
    protected function deleteEntity($entity): void
    {
        $em = $this->getEntityManager();
        $em->remove($entity);
        $em->flush();
    }

    /**
     * @param ParamFetcher $paramFetcher
     *
     * @return array
     */
    protected function getQueryParams(ParamFetcher $paramFetcher): array
    {
        $queryParameters = [];

        foreach ($paramFetcher->all() as $key => $value) {
            if (!empty($value) && !\in_array($key, ['offset', 'limit'], true)) {
                $queryParameters[$key] = $value;
            }
        }

        return $queryParameters;
    }
}
