<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\RestApiBundle\Form\Api\Type\OrderOnlinePaymentType;
use Irmag\OnlinePaymentBundle\Entity\OrderOnlinePayment;
use Irmag\OnlinePaymentBundle\Service\OnlinePaymentManager;

/**
 * @Rest\RouteResource("OrderOnlinePayment")
 */
class OrderOnlinePaymentController extends AbstractRestController
{
    protected $entityClassName = OrderOnlinePayment::class;

    /**
     * Список всех онлайн оплат.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Онлайн оплаты")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="order",
     *     in="query",
     *     description="Order id",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="status",
     *     in="query",
     *     description="Online payment status.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderOnlinePayment::class, groups={"api_online_payment", "api_order_id"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     * @Rest\QueryParam(name="order", requirements="\d+", nullable=false, default=null, description="Order id")
     * @Rest\QueryParam(name="status", requirements="\d+", nullable=true, default=null, description="Online payment status.")
     *
     * @Rest\View(serializerGroups={"api_online_payment", "api_order_id"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return OrderOnlinePayment[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($this->getQueryParams($paramFetcher), ['createdAt' => 'DESC'], $limit, $offset);
    }

    /**
     * Получить информацию об онлайн-платеже по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Онлайн оплаты")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderOnlinePayment::class, groups={"api_online_payment", "api_order_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_online_payment", "api_order_id"})
     *
     * @param OrderOnlinePayment $orderOnlinePayment
     *
     * @return OrderOnlinePayment
     */
    public function getAction(OrderOnlinePayment $orderOnlinePayment)
    {
        return $orderOnlinePayment;
    }

    /**
     * Обновить существующую оплату по id.
     *
     * Действия:
     *  * Refund - возврат средств,
     *  * Deposit - завершение оплаты (списание средств),
     *  * Revert - отмена оплаты.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Онлайн оплаты")
     *
     * @SWG\Parameter(
     *     name="order_online_payment",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\OrderOnlinePaymentType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when parameters are wrong"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request            $request
     * @param OrderOnlinePayment $orderOnlinePayment
     *
     * @return View
     */
    public function patchAction(Request $request, OrderOnlinePayment $orderOnlinePayment)
    {
        $statusCode = Response::HTTP_BAD_REQUEST;
        $form = $this->createForm(OrderOnlinePaymentType::class, null, [
            'csrf_protection' => false,
            'method' => $request->getMethod(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $pm = $this->get(OnlinePaymentManager::class);
            $pm->setPaymentProviderService($this->getPaymentProviderService($orderOnlinePayment));
            $sum = (int) $form->getData()->getOperationSum();

            switch ($form->getData()->getAction()) {
                case 'Deposit':
                    $pm->depositOnlinePayment($orderOnlinePayment, $sum, true);
                    break;

                case 'Refund':
                    $pm->refundOnlinePayment($orderOnlinePayment, $sum, true);
                    break;

                case 'Revert':
                    $pm->cancelOnlinePayment($orderOnlinePayment, $sum, true);
                    break;
            }

            $statusCode = Response::HTTP_NO_CONTENT;
        }

        return new View($form, $statusCode);
    }

    /**
     * Получить сервис платежного шлюза.
     *
     * @param OrderOnlinePayment $payment
     *
     * @return \object
     */
    private function getPaymentProviderService(OrderOnlinePayment $payment)
    {
        $order = $payment->getOrder();
        $paymentMethodShortname = $order->getPaymentMethod()->getShortname();

        $providerConfigs = $this->getParameter('irmag_online_payment.configs');

        return $this->get($providerConfigs[$paymentMethodShortname]['service']);
    }
}
