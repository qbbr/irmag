<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\RestApiBundle\Form\Api\Type\OrderMixedPaymentType;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Entity\OrderMixedPayment;

/**
 * @Rest\RouteResource("OrderMixedPayment")
 */
class OrderMixedPaymentController extends AbstractRestController
{
    protected $entityClassName = OrderMixedPayment::class;

    /**
     * Получить смешанный платёж по id заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Смешанные платежи")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderMixedPayment::class, groups={"api_order_mixed_payment", "api_order_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_order_mixed_payment", "api_order_id"})
     *
     * @param Order $order
     *
     * @return OrderMixedPayment
     */
    public function getAction(Order $order)
    {
        $this->checkOrderForMixedPayment($order);

        return $order->getMixedPayment();
    }

    /**
     * Создать смешанный платёж.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Смешанные платежи")
     *
     * @SWG\Parameter(
     *     name="order_mixed_payment",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\OrderMixedPaymentType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request)
    {
        return $this->processForm($request, new OrderMixedPayment(), OrderMixedPaymentType::class);
    }

    /**
     * Обновить смешанный платёж по id заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Смешанные платежи")
     *
     * @SWG\Parameter(
     *     name="order_mixed_payment",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\OrderMixedPaymentType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request $request
     * @param Order   $order
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, Order $order)
    {
        $this->checkOrderForMixedPayment($order);

        return $this->processForm($request, $order->getMixedPayment(), OrderMixedPaymentType::class);
    }

    /**
     * Удалить смешанный платёж по id заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Смешанные платежи")
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Order $order
     *
     * @Rest\View(statusCode=204)
     */
    public function deleteAction(Order $order)
    {
        $this->checkOrderForMixedPayment($order);
        $this->deleteEntity($order->getMixedPayment());
    }

    private function checkOrderForMixedPayment(Order $order): void
    {
        if (!$order->getMixedPayment() instanceof OrderMixedPayment) {
            throw $this->createNotFoundException('MixedPayment not found for this order');
        }
    }
}
