<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\SiteBundle\Utils\PromocodeGenerator;
use Irmag\SiteBundle\Entity\Promocode;
use Irmag\RestApiBundle\Form\Api\Type\PromocodeType;

/**
 * @Rest\RouteResource("Promocode")
 */
class PromocodeController extends AbstractRestController
{
    protected $entityClassName = Promocode::class;

    /**
     * Список всех промокодов.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Промокоды")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=Promocode::class, groups={"api_promocode"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     *
     * @Rest\View(serializerGroups={"api_promocode"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Promocode[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy([], null, $limit, $offset);
    }

    /**
     * Сгенерировать промокод (без записи в БД).
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Промокоды")
     *
     * @SWG\Parameter(
     *     name="length",
     *     in="query",
     *     description="Promocode code length.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="quantity",
     *     in="query",
     *     description="Quantity of promocodes.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=Promocode::class, groups={"api_promocode"})
     * )
     *
     * @Rest\QueryParam(name="length", requirements="\d+", default="5", nullable=true, description="Promocode code length.")
     * @Rest\QueryParam(name="quantity", requirements="\d+", default="10", description="Quantity of promocodes.")
     *
     * @Rest\View(serializerGroups={"api_promocode"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return View
     */
    public function cgetGenerateAction(ParamFetcher $paramFetcher)
    {
        $length = $paramFetcher->get('length');
        $quantity = $paramFetcher->get('quantity');

        $promocodes = [];

        for ($i = 0; $i < $quantity; ++$i) {
            $promocodes[] = ['code' => PromocodeGenerator::generate($length)];
        }

        return new View(['promocodes' => $promocodes]);
    }

    /**
     * Получить промокод по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Промокоды")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found",
     *     @Model(type=Promocode::class, groups={"api_promocode"})
     * )
     *
     * @Rest\View(serializerGroups={"api_promocode"})
     *
     * @param Promocode $promocode
     *
     * @return Promocode
     */
    public function getAction(Promocode $promocode)
    {
        return $promocode;
    }

    /**
     * Создать промокод.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Промокоды")
     *
     * @SWG\Parameter(
     *     name="promocode",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\PromocodeType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        return $this->processForm($request, new Promocode(), PromocodeType::class);
    }

    /**
     * Удалить промокод по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Промокоды")
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Promocode $promocode
     *
     * @Rest\View(statusCode=204)
     */
    public function deleteAction(Promocode $promocode)
    {
        $this->deleteEntity($promocode);
    }
}
