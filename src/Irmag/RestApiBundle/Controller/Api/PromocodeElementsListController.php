<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\SiteBundle\Entity\PromocodeElementsList;

/**
 * @Rest\RouteResource("PromocodeElementsList")
 */
class PromocodeElementsListController extends AbstractRestController
{
    protected $entityClassName = PromocodeElementsList::class;

    /**
     * Список списков товаров.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Промокоды: Списки товаров")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="uuid1C",
     *     in="query",
     *     description="UUID в 1C",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=PromocodeElementsList::class, groups={"api_promocode_elements_list"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     * @Rest\QueryParam(name="uuid1C", nullable=true, default=null, description="UUID в 1C")
     *
     * @Rest\View(serializerGroups={"api_promocode_elements_list"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return PromocodeElementsList[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($this->getQueryParams($paramFetcher), ['id' => 'ASC'], $limit, $offset);
    }

    /**
     * Получить список товаров по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Промокоды: Списки товаров")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=PromocodeElementsList::class, groups={"api_promocode_elements_list"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param PromocodeElementsList $promocodeElementsList
     *
     * @Rest\View(serializerGroups={"api_promocode_elements_list"})
     *
     * @return PromocodeElementsList
     */
    public function getAction(PromocodeElementsList $promocodeElementsList)
    {
        return $promocodeElementsList;
    }
}
