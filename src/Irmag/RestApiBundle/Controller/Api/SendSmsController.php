<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Irmag\SiteBundle\Service\SmsSender\SmsSender;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Swagger\Annotations as SWG;
use Irmag\RestApiBundle\Controller\AbstractRestController;

/**
 * @Rest\RouteResource("Send_Sms", pluralize=false)
 */
class SendSmsController extends AbstractRestController
{
    /**
     * Отправить Email.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Sender")
     *
     * @SWG\Parameter(
     *     name="sms",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\SendSmsType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request)
    {
        $content = json_decode($request->getContent(), true);
        $result = $this->get(SmsSender::class)->send($content['to'], $content['body']);

        return new View(['result' => $result], Response::HTTP_CREATED);
    }
}
