<?php

namespace Irmag\RestApiBundle\Controller\Api;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Swagger\Annotations as SWG;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\SiteBundle\Service\BonusManager\BonusManager;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\UserBonusHistory;
use Irmag\RestApiBundle\Form\Api\Type\UserBonusHistoryType;

/**
 * @Rest\RouteResource("UserBonusHistory")
 */
class UserBonusHistoryController extends AbstractRestController
{
    protected $entityClassName = UserBonusHistory::class;

    /**
     * Список истории операций бонусов пользователей.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Пользователи: История операций по бонусному счёту")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="filter",
     *     in="query",
     *     description="Если параметр не указан (null), то выбираются все операции, если 1 - только операции с сайта (uuid1C = null)",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=UserBonusHistory::class, groups={"api_user_bonus_history", "api_user_id"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     * @Rest\QueryParam(name="filter", requirements="\d+", nullable=true, default=null, description="Если параметр не указан (null), то выбираются все операции, если 1 - только операции с сайта (uuid1C = null)")
     *
     * @Rest\View(serializerGroups={"api_user_bonus_history", "api_user_id"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return UserBonusHistory[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        if ('1' === $paramFetcher->get('filter')) {
            $criteria = ['uuid1C' => null];
        }

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($criteria ?? [], null, $limit, $offset);
    }

    /**
     * Получить операцию по бонусному счёту пользователя по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Пользователи: История операций по бонусному счёту")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=UserBonusHistory::class, groups={"api_user_bonus_history", "api_user_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param UserBonusHistory $userBonusHistory Id
     *
     * @Rest\View(serializerGroups={"api_user_bonus_history", "api_user_id"})
     *
     * @return UserBonusHistory
     */
    public function getAction(UserBonusHistory $userBonusHistory)
    {
        return $userBonusHistory;
    }

    /**
     * Создать операцию по бонусному счёту пользователя.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Пользователи: История операций по бонусному счёту")
     *
     * @SWG\Parameter(
     *     name="user_bonus_history",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\UserBonusHistoryType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="500",
     *     description="Returned when has errors"
     * )
     *
     * @param Request $request
     *
     * @return View
     */
    public function postAction(Request $request)
    {
        $data = $request->request->all();
        $user = $this->getEntityByIdOrThrowNotFoundHttpException((int) $data['user'], User::class);

        $bonusManager = $this->get(BonusManager::class);
        $bonusManager->setUser($user);
        $userBonusHistory = $bonusManager->update(
            $data['value'],
            $data['message'],
            !empty($data['createdAt']) ? new \DateTime($data['createdAt']) : null,
            $data['uuid1C'] ?? null
        );

        if ($userBonusHistory instanceof UserBonusHistory) {
            return new View(['id' => $userBonusHistory->getId()], Response::HTTP_CREATED);
        }

        return new View(null, Response::HTTP_BAD_REQUEST);
    }

    /**
     * Обновить существующую операцию по бонусному счёту пользователя по id.
     * Внимание! Изменения не обновляют бонусы у пользователя, а только меняют историю операции.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Пользователи: История операций по бонусному счёту")
     *
     * @SWG\Parameter(
     *     name="user_bonus_history",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\UserBonusHistoryType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request          $request
     * @param UserBonusHistory $userBonusHistory
     *
     * @return View
     */
    public function patchAction(Request $request, UserBonusHistory $userBonusHistory)
    {
        return $this->processForm($request, $userBonusHistory, UserBonusHistoryType::class);
    }

    /**
     * Удалить историю операции по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Пользователи: История операций по бонусному счёту")
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param UserBonusHistory $userBonusHistory
     *
     * @Rest\View(statusCode=204)
     */
    public function deleteAction(UserBonusHistory $userBonusHistory)
    {
        $this->deleteEntity($userBonusHistory);
    }
}
