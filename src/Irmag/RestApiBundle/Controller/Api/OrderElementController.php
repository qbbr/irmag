<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\SiteBundle\Entity\OrderElement;
use Irmag\RestApiBundle\Form\Api\Type\OrderElementType;

/**
 * @Rest\RouteResource("OrderElement")
 */
class OrderElementController extends AbstractRestController
{
    protected $entityClassName = OrderElement::class;

    /**
     * Список всех товаров заказа по id заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Товары")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderElement::class, groups={"api_order_element", "api_order_id"})
     * )
     *
     * @Rest\QueryParam(name="order", requirements="\d+", nullable=false, default=null, description="Order id", strict=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     *
     * @Rest\View(serializerGroups={"api_order_element", "api_order_id"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return OrderElement[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($this->getQueryParams($paramFetcher), null, $limit, $offset);
    }

    /**
     * Получить товар заказа у заказа по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Товары")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderElement::class, groups={"api_order_element", "api_order_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_order_element", "api_order_id"})
     *
     * @param OrderElement $orderElement
     *
     * @return OrderElement
     */
    public function getAction(OrderElement $orderElement)
    {
        return $orderElement;
    }

    /**
     * Создать товар заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Товары")
     *
     * @SWG\Parameter(
     *     name="order_element",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\OrderElementType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request)
    {
        return $this->processForm($request, new OrderElement(), OrderElementType::class);
    }

    /**
     * Обновить существующий товар у заказа по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Товары")
     *
     * @SWG\Parameter(
     *     name="order_element",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\OrderElementType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request      $request
     * @param OrderElement $orderElement
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, OrderElement $orderElement)
    {
        return $this->processForm($request, $orderElement, OrderElementType::class);
    }
}
