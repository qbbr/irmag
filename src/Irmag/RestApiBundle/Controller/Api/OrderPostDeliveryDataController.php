<?php

namespace Irmag\RestApiBundle\Controller\Api;

use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Swagger\Annotations as SWG;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\RestApiBundle\Form\Api\Type\OrderPostDeliveryDataType;
use Irmag\OrderPostDeliveryBundle\Entity\OrderPostDeliveryData;

/**
 * @Rest\RouteResource("OrderPostDeliveryData")
 */
class OrderPostDeliveryDataController extends AbstractRestController
{
    protected $entityClassName = OrderPostDeliveryData::class;

    /**
     * Список всех данных по доставке Почтой России.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Данные по доставке Почтой России")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderPostDeliveryData::class, groups={"api_post_delivery_data", "api_order_id"})
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     *
     * @Rest\View(serializerGroups={"api_post_delivery_data", "api_order_id"})
     *
     * @return OrderPostDeliveryData[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy([], null, $limit, $offset);
    }

    /**
     * Получить данные по доставке по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Данные по доставке Почтой России")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderPostDeliveryData::class, groups={"api_post_delivery_data", "api_order_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param OrderPostDeliveryData $orderPostDeliveryData
     *
     * @Rest\View(serializerGroups={"api_post_delivery_data", "api_order_id"})
     *
     * @return OrderPostDeliveryData
     */
    public function getAction(OrderPostDeliveryData $orderPostDeliveryData)
    {
        return $orderPostDeliveryData;
    }

    /**
     * Обновить данные по доставке Почтой России по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы: Данные по доставке Почтой России")
     *
     * @SWG\Parameter(
     *     name="order_post_delivery_data",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\Api\Type\OrderPostDeliveryDataType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request               $request
     * @param OrderPostDeliveryData $orderPostDeliveryData
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, OrderPostDeliveryData $orderPostDeliveryData)
    {
        return $this->processForm($request, $orderPostDeliveryData, OrderPostDeliveryDataType::class);
    }
}
