<?php

namespace Irmag\RestApiBundle\Controller\SelfserviceApi;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\SiteBundle\Entity\Order;
use Irmag\SiteBundle\Entity\OrderDeliverySelfserviceMethod;
use Irmag\SiteBundle\Entity\OrderStatus;
use Irmag\RestApiBundle\Form\SelfserviceApi\Type\OrderSelfserviceType;
use Irmag\RestApiBundle\EventListener\SelfserviceApiRequestListener;

/**
 * @Rest\RouteResource("OrderSelfservice")
 */
class OrderSelfserviceController extends AbstractRestController
{
    protected $entityClassName = Order::class;

    private const AVAILABLE_ORDER_STATUSES = ['L', 'D', 'E'];

    /**
     * Список всех заказов.
     * По-умолчанию, если не используется фильтрация по статусу,
     * то возвращаются заказы со статусом:
     * * "3 - (D) В доставке",
     * * "5 - (L) В доставке с изменениями",
     * * "13 - (E) Доставлен в пункт самовывоза";.
     *
     * Для PATCH:
     * * "14 - (V) Выдан из пункта самовывоза";
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=Order::class, groups={"api_order_selfservices", "api_user_id"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     * @Rest\QueryParam(name="status", requirements="\d+", nullable=true, default=null, description="Order status.")
     *
     * @Rest\View(serializerGroups={"api_order_selfservices", "api_user_id"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Order[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        $queryParam = $this->getQueryParams($paramFetcher);
        $queryParam['selfserviceMethod'] = $this->getCurrentSelfserviceMethod();

        if (empty($queryParam['status'])) {
            foreach (self::AVAILABLE_ORDER_STATUSES as $statusShortname) {
                $queryParam['status'][] = $this->getEntityManager()->getRepository(OrderStatus::class)->findOneBy(['shortname' => $statusShortname]);
            }
        }

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($queryParam, null, $limit, $offset);
    }

    /**
     * Получить заказ по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=Order::class, groups={"api_order_selfservices", "api_user_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_order_selfservices", "api_user_id"})
     *
     * @param Order $order
     *
     * @return \object
     */
    public function getAction(Order $order)
    {
        $this->throwAcessDeniedHttpExceptionIfOrderIsNotMine($order);

        return $order;
    }

    /**
     * Обновить существующий заказ по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Заказы")
     *
     * @SWG\Parameter(
     *     name="order",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\SelfserviceApi\Type\OrderSelfserviceType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request $request
     * @param Order   $order
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, Order $order)
    {
        $this->throwAcessDeniedHttpExceptionIfOrderIsNotMine($order);

        return $this->processForm($request, $order, OrderSelfserviceType::class);
    }

    /**
     * @param Order $order
     */
    private function throwAcessDeniedHttpExceptionIfOrderIsNotMine(Order $order): void
    {
        $selfserviceMethod = $order->getSelfserviceMethod();
        $currentSelfserviceMethod = $this->getCurrentSelfserviceMethod();

        if (!$selfserviceMethod || $selfserviceMethod !== $currentSelfserviceMethod) {
            throw new AccessDeniedHttpException('Access denied for this order. Maybe it not your? Leave me alone.');
        }

        if (!\in_array($order->getStatus()->getShortname(), self::AVAILABLE_ORDER_STATUSES, true)) {
            throw new AccessDeniedHttpException(sprintf('Access denied for this order. Available statuses: %s.', implode(', ', self::AVAILABLE_ORDER_STATUSES)));
        }
    }

    private function getCurrentSelfserviceMethod(): OrderDeliverySelfserviceMethod
    {
        return $this->get(SelfserviceApiRequestListener::class)->getSelfserviceMethod();
    }
}
