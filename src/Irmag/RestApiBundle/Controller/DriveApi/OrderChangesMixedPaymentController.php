<?php

namespace Irmag\RestApiBundle\Controller\DriveApi;

use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\DriveBundle\Entity\OrderChanges;
use Irmag\DriveBundle\Entity\OrderChangesMixedPayment;
use Irmag\RestApiBundle\Form\DriveApi\Type\OrderChangesMixedPaymentType;
use Irmag\RestApiBundle\Controller\AbstractRestController;

/**
 * @Rest\RouteResource("OrderChangesMixedPayments")
 */
class OrderChangesMixedPaymentController extends AbstractRestController
{
    protected $entityClassName = OrderChangesMixedPayment::class;

    /**
     * Получить смешанный платёж по id заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказа: Смешанные платежи")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderChangesMixedPayment::class, groups={"api_order_mixed_payment", "api_order_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_order_mixed_payment", "api_order_id"})
     *
     * @param OrderChanges $order
     *
     * @return OrderChangesMixedPayment
     */
    public function getAction(OrderChanges $order)
    {
        $this->checkOrderForMixedPayment($order);

        return $order->getMixedPayment();
    }

    /**
     * Создать смешанный платёж.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказа: Смешанные платежи")
     *
     * @SWG\Parameter(
     *     name="order_changes_mixed_payment",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\DriveApi\Type\OrderChangesMixedPaymentType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request)
    {
        return $this->processForm($request, new OrderChangesMixedPayment(), OrderChangesMixedPaymentType::class);
    }

    /**
     * Обновить смешанный платёж по id заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказа: Смешанные платежи")
     *
     * @SWG\Parameter(
     *     name="order_changes_mixed_payment",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\DriveApi\Type\OrderChangesMixedPaymentType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request      $request
     * @param OrderChanges $order
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, OrderChanges $order)
    {
        $this->checkOrderForMixedPayment($order);

        return $this->processForm($request, $order->getMixedPayment(), OrderChangesMixedPaymentType::class);
    }

    /**
     * Удалить смешанный платёж по id заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказа: Смешанные платежи")
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param OrderChanges $order
     *
     * @Rest\View(statusCode=204)
     */
    public function deleteAction(OrderChanges $order)
    {
        $this->checkOrderForMixedPayment($order);
        $order->setMixedPayment(null);
        $this->getEntityManager()->flush();
    }

    private function checkOrderForMixedPayment(OrderChanges $order): void
    {
        if (!$order->getMixedPayment() instanceof OrderChangesMixedPayment) {
            throw $this->createNotFoundException('MixedPayment not found for this order');
        }
    }
}
