<?php

namespace Irmag\RestApiBundle\Controller\DriveApi;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\ProfileBundle\Entity\User;
use Irmag\DriveBundle\Entity\Waybill;
use Irmag\RestApiBundle\Form\DriveApi\Type\WaybillType;
use Irmag\RestApiBundle\EventListener\DriveApiRequestListener;

/**
 * @Rest\RouteResource("Waybill")
 */
class WaybillController extends AbstractRestController
{
    protected $entityClassName = Waybill::class;

    /**
     * Список всех путёвок.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Путёвки")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="driver",
     *     in="query",
     *     description="Driver id.",
     *     required=false,
     *     type="integer"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=Waybill::class, groups={"api_waybill", "api_order", "api_user_id"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     * @Rest\QueryParam(name="driver", requirements="\d+", nullable=true, default=null, description="Driver id.")
     *
     * @Rest\View(serializerGroups={"api_waybill", "api_order", "api_user_id"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Waybill[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        $criteria = $this->getQueryParams($paramFetcher);

        $driver = $this->get(DriveApiRequestListener::class)->getDriver();

        if ($driver instanceof User) {
            $criteria['driver'] = $driver;
        }

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($criteria, ['isCompleted' => 'ASC'], $limit, $offset);
    }

    /**
     * Получить путёвку по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Путёвки")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=Waybill::class, groups={"api_waybill", "api_order", "api_user_id"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_waybill", "api_order", "api_user_id"})
     *
     * @param Waybill $waybill
     *
     * @return \object
     */
    public function getAction(Waybill $waybill)
    {
        return $waybill;
    }

    /**
     * Создать путёвку.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Путёвки")
     *
     * @SWG\Parameter(
     *     name="waybill",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\DriveApi\Type\WaybillType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request)
    {
        $waybill = new Waybill();

        return $this->processForm($request, $waybill, WaybillType::class);
    }

    /**
     * Обновить существующую путёвку по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Путёвки")
     *
     * @SWG\Parameter(
     *     name="waybill",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\DriveApi\Type\WaybillType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request $request
     * @param Waybill $waybill
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, Waybill $waybill)
    {
        return $this->processForm($request, $waybill, WaybillType::class);
    }
}
