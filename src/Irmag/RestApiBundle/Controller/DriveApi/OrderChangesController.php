<?php

namespace Irmag\RestApiBundle\Controller\SelfserviceApi;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\DriveBundle\Entity\OrderChanges;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\RestApiBundle\Form\DriveApi\Type\OrderChangesType;

/**
 * @Rest\RouteResource("OrderChanges")
 */
class OrderChangesController extends AbstractRestController
{
    protected $entityClassName = OrderChanges::class;

    /**
     * Список всех изменений по заказам.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderChanges::class, groups={"api_order", "api_order_changes", "api_order_element_changes", "api_order_element"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     *
     * @Rest\View(serializerGroups={"api_order", "api_order_changes", "api_order_element_changes", "api_order_element"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return OrderChanges[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($this->getQueryParams($paramFetcher), null, $limit, $offset);
    }

    /**
     * Получить изменения заказа по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderChanges::class, groups={"api_order", "api_order_changes", "api_order_element_changes", "api_order_element"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_order", "api_order_changes", "api_order_element_changes", "api_order_element"})
     *
     * @param OrderChanges $orderChanges
     *
     * @return \object
     */
    public function getAction(OrderChanges $orderChanges)
    {
        return $orderChanges;
    }

    /**
     * Создать изменения заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам")
     *
     * @SWG\Parameter(
     *     name="waybill",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\DriveApi\Type\OrderChangesType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request)
    {
        $orderChanges = new OrderChanges();

        return $this->processForm($request, $orderChanges, OrderChangesType::class);
    }

    /**
     * Обновить существующее изменения заказа по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам")
     *
     * @SWG\Parameter(
     *     name="order",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\DriveApi\Type\OrderChangesType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request      $request
     * @param OrderChanges $orderChanges
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, OrderChanges $orderChanges)
    {
        return $this->processForm($request, $orderChanges, OrderChangesType::class);
    }

    /**
     * Удалить изменения по id заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам")
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param OrderChanges $order
     *
     * @Rest\View(statusCode=204)
     */
    public function deleteAction(OrderChanges $order)
    {
        $this->deleteEntity($order);
    }
}
