<?php

namespace Irmag\RestApiBundle\Controller\Api;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Irmag\DriveBundle\Entity\OrderElementChanges;
use Irmag\DriveBundle\Entity\OrderChanges;
use Irmag\RestApiBundle\Controller\AbstractRestController;
use Irmag\RestApiBundle\Form\DriveApi\Type\OrderElementChangesType;

/**
 * @Rest\RouteResource("OrderElementChanges")
 */
class OrderElementChangesController extends AbstractRestController
{
    protected $entityClassName = OrderElementChanges::class;

    /**
     * Список всех изменений товаров у заказа по id заказа.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам: Товары")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderElementChanges::class, groups={"api_order_element_changes", "api_order_changes_id", "api_order_element"})
     * )
     *
     * @Rest\QueryParam(name="order", requirements="\d+", nullable=false, default=null, description="Order id", strict=true)
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     *
     * @Rest\View(serializerGroups={"api_order_element_changes", "api_order_changes_id", "api_order_element"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return OrderElementChanges[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($this->getQueryParams($paramFetcher), null, $limit, $offset);
    }

    /**
     * Получить изменения товара по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам: Товары")
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderElementChanges::class, groups={"api_order_element_changes", "api_order_changes_id", "api_order_element"})
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @Rest\View(serializerGroups={"api_order_element_changes", "api_order_changes_id", "api_order_element"})
     *
     * @param OrderElementChanges $orderElementChanges
     *
     * @return OrderElementChanges
     */
    public function getAction(OrderElementChanges $orderElementChanges)
    {
        return $orderElementChanges;
    }

    /**
     * Создать изменения товара.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам: Товары")
     *
     * @SWG\Parameter(
     *     name="order_element",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\DriveApi\Type\OrderElementChangesType")
     * )
     *
     * @SWG\Response(
     *     response="201",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @param Request $request
     *
     * @return \FOS\RestBundle\View\View
     */
    public function postAction(Request $request)
    {
        if ($request->request->has('order')) {
            $orderChangesId = $request->request->getInt('order');

            $em = $this->getEntityManager();
            $orderChanges = $em->getRepository(OrderChanges::class)->find($orderChangesId);

            if (!$orderChanges instanceof OrderChanges) {
                $orderChanges = new OrderChanges();
                $orderChanges->setId($orderChangesId);
                $em->persist($orderChanges);
                $em->flush();
            }
        }

        return $this->processForm($request, new OrderElementChanges(), OrderElementChangesType::class);
    }

    /**
     * Обновить существующее изменения товара по id.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам: Товары")
     *
     * @SWG\Parameter(
     *     name="order_element",
     *     in="body",
     *     required=false,
     *     type="json",
     *     @Model(type="\Irmag\RestApiBundle\Form\DriveApi\Type\OrderElementChangesType")
     * )
     *
     * @SWG\Response(
     *     response="204",
     *     description="Returned when successful"
     * )
     *
     * @SWG\Response(
     *     response="400",
     *     description="Returned when the form has errors"
     * )
     *
     * @SWG\Response(
     *     response="404",
     *     description="Returned when not found"
     * )
     *
     * @param Request             $request
     * @param OrderElementChanges $orderElementChanges
     *
     * @return \FOS\RestBundle\View\View
     */
    public function patchAction(Request $request, OrderElementChanges $orderElementChanges)
    {
        return $this->processForm($request, $orderElementChanges, OrderElementChangesType::class);
    }
}
