<?php

namespace Irmag\RestApiBundle\Controller\DriveApi;

use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security;
use Swagger\Annotations as SWG;
use Irmag\DriveBundle\Entity\OrderChanges;
use Irmag\DriveBundle\Entity\OrderChangesStatus;
use Irmag\RestApiBundle\Controller\AbstractRestController;

/**
 * @Rest\RouteResource("OrderChangesStatus")
 */
class OrderChangesStatusController extends AbstractRestController
{
    protected $entityClassName = OrderChangesStatus::class;

    /**
     * Список всех статусов для изменений по заказам.
     *
     * @Security(name="x_api_token")
     *
     * @SWG\Tag(name="Изменения по заказам: Статусы")
     *
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     description="Offset of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     description="Limit of results.",
     *     required=false,
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="Returned when successful",
     *     @Model(type=OrderChanges::class, groups={"api_order_changes_status"})
     * )
     *
     * @Rest\QueryParam(name="offset", requirements="\d+", nullable=true, description="Offset of results.")
     * @Rest\QueryParam(name="limit", requirements="\d+", default="10", description="Limit of results.")
     *
     * @Rest\View(serializerGroups={"api_order_changes_status"})
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return OrderChanges[]
     */
    public function cgetAction(ParamFetcher $paramFetcher)
    {
        $offset = $paramFetcher->get('offset');
        $limit = $paramFetcher->get('limit');

        return $this->getEntityManager()->getRepository($this->entityClassName)->findBy($this->getQueryParams($paramFetcher), null, $limit, $offset);
    }
}
