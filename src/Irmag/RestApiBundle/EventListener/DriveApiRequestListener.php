<?php

namespace Irmag\RestApiBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Irmag\ProfileBundle\Entity\UserDriveToken;
use Irmag\ProfileBundle\Entity\User;

/**
 * Аутентификация по @see `ApiRequestListener::TOKEN_PARAM`,
 * которвый сравнивается с @see `UserDriveToken::$token`.
 */
class DriveApiRequestListener
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var string
     */
    private $driveApiDomain;

    /**
     * @var string
     */
    private $token;

    /**
     * @var User
     */
    private $driver;

    /**
     * @param EntityManagerInterface $em
     * @param string                 $driveApiDomain
     * @param string                 $token
     */
    public function __construct(
        EntityManagerInterface $em,
        string $driveApiDomain,
        string $token
    ) {
        $this->em = $em;
        $this->driveApiDomain = $driveApiDomain;
        $this->token = $token;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $host = $request->getHost();

        // only for Drive API domain
        if ($host !== $this->driveApiDomain) {
            return;
        }

        // skip for wdt
        if (ApiRequestListener::isDebugPath($request->getPathInfo())) {
            return;
        }

        // skip for api doc
        if (ApiRequestListener::isDocPath($request->getPathInfo())) {
            return;
        }

        // nn auth for login
        if ('/login' === $request->getPathInfo()) {
            return;
        }

        // auth
        $token = $request->headers->get(ApiRequestListener::TOKEN_PARAM);

        if (empty($token)) {
            $event->setResponse(ApiRequestListener::getAccessDeniedResponse());
        }

        // allow 1c access by token
        // will be returns all waybills
        if ($token === $this->token) {
            return;
        }

        $driveToken = $this->em->getRepository(UserDriveToken::class)->findOneBy(['token' => $token]);

        if ($driveToken && $user = $driveToken->getUser()) {
            /** @var User $user */
            $isDriver = self::isDriverUser($user);

            if (true === $isDriver) {
                $this->driver = $user;
                $driveToken->setUpdatedAt(new \DateTime());
                $this->em->persist($driveToken);
                $this->em->flush();
            }
        }

        if (!isset($isDriver) || false === $isDriver) {
            $event->setResponse(ApiRequestListener::getAccessDeniedResponse());
        }
    }

    public function getDriver(): ?User
    {
        return $this->driver;
    }

    public static function isDriverUser(User $user): bool
    {
        return \in_array(User::ROLE_IRMAG_DRIVER, $user->getRoles(), true);
    }
}
