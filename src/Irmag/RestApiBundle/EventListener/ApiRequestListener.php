<?php

namespace Irmag\RestApiBundle\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

/**
 * Аутентификация по @see `ApiRequestListener::TOKEN_PARAM`.
 *
 * @example curl -H "x-api-token: %IRMAG_API_TOKEN%" https://api.irmag.local/users.json
 */
class ApiRequestListener
{
    public const TOKEN_PARAM = 'x-api-token';
    private const DEBUG_PATHS = ['#^/_wdt/#', '#^/_profiler/#'];

    /**
     * @var string
     */
    private $apiDomain;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $environment;

    /**
     * @param string $apiDomain
     * @param string $token
     * @param string $environment
     */
    public function __construct(
        string $apiDomain,
        string $token,
        string $environment
    ) {
        $this->apiDomain = $apiDomain;
        $this->token = $token;
        $this->environment = $environment;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $host = $request->getHost();

        // only for API domain
        if ($host !== $this->apiDomain) {
            return;
        }

        // skip for non prod
        if ('prod' !== $this->environment) {
            return;
        }

        // skip for api doc
        if (self::isDocPath($request->getPathInfo())) {
            return;
        }

        // auth
        $token = $request->headers->get(self::TOKEN_PARAM);

        if (
            empty($token)
            ||
            $token !== $this->token
        ) {
            $event->setResponse(self::getAccessDeniedResponse());
        }
    }

    public static function getAccessDeniedResponse(): JsonResponse
    {
        return new JsonResponse(['code' => 403, 'message' => 'Access denied. Invalid token.'], 403);
    }

    public static function isDocPath(string $path): bool
    {
        if ('/doc' === $path) {
            return true;
        }

        return false;
    }

    public static function isDebugPath(string $path): bool
    {
        foreach (self::DEBUG_PATHS as $debugPath) {
            if (1 === preg_match($debugPath, $path)) {
                return true;
            }
        }

        return false;
    }
}
