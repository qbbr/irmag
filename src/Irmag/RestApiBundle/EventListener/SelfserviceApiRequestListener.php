<?php

namespace Irmag\RestApiBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Irmag\SiteBundle\Entity\OrderDeliverySelfserviceMethod;

/**
 * Аутентификация по @see `ApiRequestListener::TOKEN_PARAM`,
 * которвый сравнивается с @see `OrderDeliverySelfserviceMethod::$token`.
 *
 * @example curl -H "x-api-token: %SELFSERVICE_TOKEN_FROM_BD%" https://public-api.irmag.local/orderselfservices.json
 */
class SelfserviceApiRequestListener
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var string
     */
    private $selfserviceApiDomain;

    /**
     * @var OrderDeliverySelfserviceMethod
     */
    private $selfserviceMethod;

    /**
     * @param EntityManagerInterface $em
     * @param string                 $selfserviceApiDomain
     */
    public function __construct(
        EntityManagerInterface $em,
        string $selfserviceApiDomain
    ) {
        $this->em = $em;
        $this->selfserviceApiDomain = $selfserviceApiDomain;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        if (!$event->isMasterRequest()) {
            return;
        }

        $request = $event->getRequest();
        $host = $request->getHost();

        // only for Selfservice API domain
        if ($host !== $this->selfserviceApiDomain) {
            return;
        }

        // skip for wdt
        if (ApiRequestListener::isDebugPath($request->getPathInfo())) {
            return;
        }

        // skip for api doc
        if (ApiRequestListener::isDocPath($request->getPathInfo())) {
            return;
        }

        // auth
        $token = $request->headers->get(ApiRequestListener::TOKEN_PARAM);

        if (empty($token)) {
            $event->setResponse(ApiRequestListener::getAccessDeniedResponse());
        }

        // check token
        $this->selfserviceMethod = $this->em->getRepository(OrderDeliverySelfserviceMethod::class)->findOneBy(['token' => $token]);

        if (!$this->selfserviceMethod instanceof OrderDeliverySelfserviceMethod) {
            $event->setResponse(ApiRequestListener::getAccessDeniedResponse());
        }
    }

    public function getSelfserviceMethod(): ?OrderDeliverySelfserviceMethod
    {
        return $this->selfserviceMethod;
    }
}
