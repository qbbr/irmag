<?php

namespace Irmag\RestApiBundle\Form\Api\Model;

use Symfony\Component\Validator\Constraints as Assert;

class OrderOnlinePaymentModel
{
    /**
     * @var string
     *
     * @Assert\NotBlank
     * @Assert\Choice({"Deposit", "Refund", "Revert"})
     */
    private $action;

    /**
     * @var int
     *
     * @Assert\NotBlank
     * @Assert\GreaterThan(0)
     */
    private $operationSum;

    /**
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->action = $action;
    }

    /**
     * @return int
     */
    public function getOperationSum()
    {
        return $this->operationSum;
    }

    /**
     * @param int $operationSum
     */
    public function setOperationSum($operationSum)
    {
        $this->operationSum = $operationSum;
    }
}
