<?php

namespace Irmag\RestApiBundle\Form\Api\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\RestApiBundle\Form\Api\Model\OrderOnlinePaymentModel;

class OrderOnlinePaymentType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('action', ChoiceType::class, [
                'label' => 'Операция',
                'choices' => [
                    'Deposit' => 'Deposit',
                    'Revert' => 'Revert',
                    'Refund' => 'Refund',
                ],
            ])
            ->add('operationSum', IntegerType::class, ['label' => 'Сумма операции'])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderOnlinePaymentModel::class,
            'required' => false,
        ]);
    }
}
