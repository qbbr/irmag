<?php

namespace Irmag\RestApiBundle\Form\Api\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\SiteBundle\Entity\OrderElement;

class OrderElementType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (Request::METHOD_POST === $builder->getMethod()) {
            $builder->add('order');
        }

        $builder
            ->add('element')
            ->add('name')
            ->add('price')
            ->add('priceWithoutDiscount')
            ->add('amount')
            ->add('amountDiff')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => OrderElement::class,
        ]);
    }
}
