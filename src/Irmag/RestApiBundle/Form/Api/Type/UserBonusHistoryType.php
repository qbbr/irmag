<?php

namespace Irmag\RestApiBundle\Form\Api\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\SiteBundle\Config;
use Irmag\ProfileBundle\Entity\UserBonusHistory;

class UserBonusHistoryType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('uuid1C')
            ->add('user')
            ->add('value')
            ->add('message')
            ->add('createdAt', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => Config::DATETIME_FORMAT_RFC3339,
                'label' => 'form.label.created_at',
                'required' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'irmag_site',
            'data_class' => UserBonusHistory::class,
        ]);
    }
}
