<?php

namespace Irmag\RestApiBundle\Form\Api\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\SiteBundle\Config;
use Irmag\SiteBundle\Entity\Order;

class OrderType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('user')
            ->add('orderElement')

            ->add('payerUsername', null, ['label' => 'form.label.username'])
            ->add('payerTelephone', null, ['label' => 'form.label.telephone'])
            ->add('payerEmail', null, ['label' => 'form.label.email'])

            // доставка
            ->add('deliveryMethod', null, ['label' => 'form.label.delivery_method'])
            ->add('deliveryCity', null, ['label' => 'form.label.delivery_city'])
            ->add('deliveryDistrict', null, ['label' => 'form.label.delivery_district'])
            ->add('deliveryStreet', null, ['label' => 'form.label.delivery_street'])
            ->add('deliveryHouse', null, ['label' => 'form.label.delivery_house'])
            ->add('deliveryApartment', null, ['label' => 'form.label.delivery_apartment'])
            ->add('deliveryComment', null, ['label' => 'form.label.delivery_comment'])
            ->add('deliveryDate', DateType::class, [
                'widget' => 'single_text',
                'format' => Config::DATE_FORMAT_RFC3339,
                'label' => 'form.label.delivery_date',
            ])
            ->add('deliveryTime', null, ['label' => 'form.label.delivery_time'])
            ->add('deliveryPrice', null, ['label' => 'form.label.delivery_price'])

            ->add('paymentMethod', null, ['label' => 'form.label.payment_method'])
            ->add('selfserviceMethod', null, ['label' => 'form.label.selfservice_method'])

            ->add('totalPrice', null, ['label' => 'form.label.total_price'])
            ->add('totalPriceWithoutDiscount', null, ['label' => 'form.label.total_price_without_discount'])
            ->add('spendBonus', null, ['label' => 'form.label.bonus'])
            ->add('promocodeDiscount')
            ->add('promocode')
            ->add('status', null, ['label' => 'form.label.status'])

            // юр. данные
            ->add('isPayerLegal', null, ['label' => 'form.label.is_payer_legal'])
            ->add('payerLegalAddress', null, ['label' => 'form.label.legal_address', 'required' => false])
            ->add('payerLegalActualAddress', null, ['label' => 'form.label.legal_actual_address', 'required' => false])
            ->add('payerLegalConsigneeAddress', null, ['label' => 'form.label.legal_consignee_address'])
            ->add('payerLegalName', null, ['label' => 'form.label.legal_name', 'required' => false])
            ->add('payerLegalInn', null, ['label' => 'form.label.legal_inn', 'required' => false])
            ->add('payerLegalKpp', null, ['label' => 'form.label.legal_kpp', 'required' => false])
            ->add('payerLegalOgrn', null, ['label' => 'form.label.legal_ogrn', 'required' => false])
            ->add('payerLegalRs', null, ['label' => 'form.label.legal_rs', 'required' => false])
            ->add('payerLegalKrs', null, ['label' => 'form.label.legal_krs', 'required' => false])
            ->add('payerLegalRb', null, ['label' => 'form.label.legal_rb', 'required' => false])
            ->add('payerLegalRbCity', null, ['label' => 'form.label.legal_rb_city', 'required' => false])
            ->add('payerLegalBik', null, ['label' => 'form.label.legal_bik', 'required' => false])
            ->add('payerLegalTelephone', null, ['label' => 'form.label.legal_telephone', 'required' => false])
            ->add('checkDeliveryMethod', null, ['label' => 'form.label.check_delivery_method', 'required' => false])

            ->add('createdAt', DateTimeType::class, [
                'widget' => 'single_text',
                'format' => Config::DATETIME_FORMAT_RFC3339,
                'label' => 'form.label.created_at',
                'required' => false,
            ])

            ->add('parent')
            ->add('waybill', null, ['required' => false])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'irmag_site',
            'data_class' => Order::class,
        ]);
    }
}
