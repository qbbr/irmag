<?php

namespace Irmag\RestApiBundle\Form\DriveApi\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Irmag\DriveBundle\Entity\OrderChanges;

class OrderChangesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if (Request::METHOD_POST === $builder->getMethod()) {
            $builder->add('id');
        }

        $builder
            ->add('paymentMethod', null, ['label' => 'form.label.payment_method'])
            ->add('status', null, ['label' => 'form.label.status'])
            ->add('comment')
            ->add('totalPrice')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'irmag_site',
            'data_class' => OrderChanges::class,
        ]);
    }
}
