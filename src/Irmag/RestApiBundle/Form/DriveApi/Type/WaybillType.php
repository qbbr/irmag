<?php

namespace Irmag\RestApiBundle\Form\DriveApi\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Irmag\DriveBundle\Entity\Waybill;
use Irmag\SiteBundle\Config;

class WaybillType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('shift')
            ->add('totalSum')
            ->add('sumCash')
            ->add('sumCard')
            ->add('sumCertificate')
            ->add('sumRs')
            ->add('sumOnline')
            ->add('driver')
            ->add('date', DateType::class, [
                'widget' => 'single_text',
                'format' => Config::DATE_FORMAT_RFC3339,
                'label' => 'form.label.delivery_date',
            ])
            ->add('comment')
            ->add('quantityOfExtraDelivery')
            ->add('quantityOutOfCity')
            ->add('mileage')
            ->add('isCompleted')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => 'irmag_site',
            'data_class' => Waybill::class,
        ]);
    }
}
