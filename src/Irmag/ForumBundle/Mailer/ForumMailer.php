<?php

namespace Irmag\ForumBundle\Mailer;

use Irmag\CoreBundle\Mailer\Mailer;

class ForumMailer
{
    /**
     * @var Mailer
     */
    private $mailer;

    /**
     * @param Mailer $mailer
     */
    public function __construct(
        Mailer $mailer
    ) {
        $this->mailer = $mailer;
    }

    /**
     * @param string|array $to
     * @param array        $context Массив с переменными контекста
     *
     * @return int
     */
    public function sendNewTopicMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagForum/Emails/new-topic.html.twig', $context);
    }

    /**
     * @param string|array $to
     * @param array        $context Массив с переменными контекста
     *
     * @return int
     */
    public function sendNewPostMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagForum/Emails/new-post.html.twig', $context);
    }

    /**
     * @param string|array $to
     * @param array        $context Массив с переменными контекста
     *
     * @return int
     */
    public function sendNewSubscriptionPostMessage($to, array $context = []): int
    {
        return $this->mailer->sendMessage($to, '@IrmagForum/Emails/new-subscription-post.html.twig', $context);
    }
}
