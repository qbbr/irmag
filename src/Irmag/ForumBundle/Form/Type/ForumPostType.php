<?php

namespace Irmag\ForumBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Qbbr\EntityHiddenTypeBundle\Form\Type\EntityHiddenType;
use Irmag\AttachmentBundle\Form\Type\AttachmentUploaderType;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ForumBundle\Entity\ForumPostAttachment;
use Irmag\ForumBundle\Entity\ForumTopic;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Service\UserTrait;

class ForumPostType extends AbstractType
{
    use UserTrait;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('text', TextareaType::class, [
                'required' => true,
                'attr' => [
                    'rows' => 20,
                ],
            ])
            ->add('button', SubmitType::class, [
                'label' => 'Отправить',
            ])
            ->add('uploader', AttachmentUploaderType::class)
            ->add('attachments', CollectionType::class, [
                'entry_type' => EntityHiddenType::class,
                'entry_options' => [
                    'label' => false,
                    'required' => false,
                    'class' => ForumPostAttachment::class,
                ],
                'label' => false,
                'prototype' => true,
                'allow_add' => true,
                'allow_delete' => true,
                'delete_empty' => true,
                'attr' => [
                    'class' => 'attachment-items',
                ],
            ])
            ->add('topic', EntityHiddenType::class, [
                'class' => ForumTopic::class,
                'data' => $options['topic'] ?? null,
                'data_class' => null,
            ])
            ->add('user', EntityHiddenType::class, [
                'class' => User::class,
                'data' => $this->getUser(),
                'data_class' => null,
            ])
            ->add('user_modified_by', EntityHiddenType::class, [
                'class' => User::class,
                'required' => false,
                'data' => $this->getUser(),
                'data_class' => null,
            ])
            ->add('id', HiddenType::class)
            ->add('in_reply_to', EntityHiddenType::class, [
                'class' => ForumPost::class,
                'required' => false,
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ForumPost::class,
            'translation_domain' => false,
        ]);

        $resolver->setDefined('topic');
        $resolver->setAllowedTypes('topic', ForumTopic::class);
    }
}
