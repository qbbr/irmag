<?php

namespace Irmag\ForumBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Qbbr\EntityHiddenTypeBundle\Form\Type\EntityHiddenType;
use Irmag\ForumBundle\Entity\ForumTopic;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Service\UserTrait;

class ForumTopicType extends AbstractType
{
    use UserTrait;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param TokenStorageInterface $tokenStorage
     */
    public function __construct(
        TokenStorageInterface $tokenStorage
    ) {
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class)
            ->add('topic_starter', HiddenType::class)
            ->add('forum', HiddenType::class, [
                'data' => $options['forum'],
            ])
            ->add('title', TextType::class, [
                'label' => 'Название темы',
                'required' => true,
            ])
            ->add('user', EntityHiddenType::class, [
                'class' => User::class,
                'data' => $this->getUser(),
                'data_class' => null,
            ])
            ->add('description', TextType::class, [
                'label' => 'Описание темы',
                'required' => false,
            ])
            ->add('tags', TextType::class, [
                'label' => 'Теги: ',
                'required' => false,
                'attr' => [
                    'data-available-tags' => json_encode(array_values($options['tags'])),
                ],
            ])
            ->add('post', ForumPostType::class, [
                'mapped' => false,
                'label' => false,
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Создать тему',
                'attr' => [
                    'class' => 'btn btn-success',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ForumTopic::class,
            'translation_domain' => false,
        ]);

        $resolver->setRequired('tags');
        $resolver->setAllowedTypes('tags', 'array');

        $resolver->setRequired('forum');
        $resolver->setAllowedTypes('forum', 'int');
    }
}
