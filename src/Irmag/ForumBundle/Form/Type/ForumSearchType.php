<?php

namespace Irmag\ForumBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ForumSearchType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mode', ChoiceType::class, [
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'label' => 'Искать по: ',
                'choices' => [
                    'названию тем' => 'name',
                    'содержанию поста' => 'content',
                    'тегам темы' => 'tags',
                ],
            ])
            ->add('query', TextType::class, [
                'required' => true,
                'label' => 'Что искать',
            ])
            ->add('submit', SubmitType::class, [
                'label' => 'Найти',
                'attr' => [
                    'class' => 'btn btn-success',
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'translation_domain' => false,
        ]);
    }
}
