<?php

namespace Irmag\ForumBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Irmag\ForumBundle\Entity\ForumPost;

class ForumPostEvent extends Event
{
    /**
     * @var ForumPostEvent
     */
    private $post;

    /**
     * @param ForumPost $post
     */
    public function __construct(ForumPost $post)
    {
        $this->post = $post;
    }

    /**
     * @return ForumPost
     */
    public function getForumPost(): ForumPost
    {
        return $this->post;
    }
}
