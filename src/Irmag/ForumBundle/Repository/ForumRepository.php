<?php

namespace Irmag\ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Irmag\ForumBundle\Entity\Forum;
use Irmag\ForumBundle\Entity\ForumPost;

class ForumRepository extends EntityRepository
{
    /**
     * Получить форум (если он единственный, как в нашем случае).
     *
     * @return Forum|null
     */
    public function getForum(): ?Forum
    {
        return $this->createQueryBuilder('f')
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Получить статистику по количеству тем и сообщений в данном форуме.
     *
     * @param Forum $forum Форум
     *
     * @return array
     */
    public function getForumStatistics(Forum $forum): array
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('COUNT(DISTINCT ft.id) AS topics_cnt')
            ->addSelect('COUNT(fp.id) AS posts_cnt')
            ->from(Forum::class, 'f')
            ->leftJoin('f.forumTopics', 'ft')
            ->leftJoin('ft.topicPosts', 'fp')
            ->where('ft.forum = :forum')
            ->andWhere('ft.isHidden = false')
            ->setParameter('forum', $forum)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Получить последний опубликованный пост форума.
     *
     * @param Forum $forum Форум
     *
     * @return ForumPost|null
     */
    public function getLatestPostInForum(Forum $forum): ?ForumPost
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('fp', 'fu', 'ft')
            ->from(ForumPost::class, 'fp')
            ->join('fp.topic', 'ft')
            ->join('fp.user', 'fu')
            ->where('ft.forum = :forum')
            ->orderBy('fp.updatedAt', 'desc')
            ->setParameter('forum', $forum)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
