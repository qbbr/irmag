<?php

namespace Irmag\ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Irmag\ForumBundle\Entity\Forum;
use Irmag\ForumBundle\Entity\ForumTopic;
use Irmag\ProfileBundle\Entity\User;

class ForumTopicRepository extends EntityRepository
{
    /**
     * Получить все активные темы форума.
     *
     * @param Forum $forum Форум
     *
     * @return QueryBuilder
     */
    public function getAllTopicsByForum(Forum $forum): QueryBuilder
    {
        return $this->createQueryBuilder('ft')
            ->addSelect('fu', 'pl', 'p', 'pu')
            ->addSelect('p.createdAt AS HIDDEN lastPostCreatedAt')
            ->join('ft.user', 'fu')
            ->join('ft.lastPost', 'p')
            ->join('p.user', 'pu')
            ->leftJoin('ft.visits', 'v')
            ->leftJoin('ft.poll', 'pl')
            ->where('ft.forum = :forum')
            ->orderBy('lastPostCreatedAt', 'desc')
            ->addOrderBy('ft.isImportant', 'desc')
            ->groupBy('ft.id', 'fu.id', 'pl.id', 'p.id', 'pu.id')
            ->setParameter('forum', $forum);
    }

    /**
     * Получить последнюю измененную тему форума.
     *
     * @param Forum $forum Форум
     *
     * @return ForumTopic|null
     */
    public function getLatest(Forum $forum): ?ForumTopic
    {
        return $this->createQueryBuilder('ft')
            ->where('ft.forum = :forum')
            ->orderBy('ft.updatedAt', 'desc')
            ->setParameter('forum', $forum)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Получить id всех просмотренных пользователем топиков форума.
     *
     * @param User  $user  Пользователь
     * @param Forum $forum Форум
     *
     * @return array
     */
    public function getAllSeenTopics(User $user, Forum $forum): array
    {
        $output = [];
        $result = $this->createQueryBuilder('ft')
            ->select('ft.id', 'fv.visitTime')
            ->join('ft.visits', 'fv')
            ->where('ft.forum = :forum')
            ->andWhere('fv.user = :user')
            ->setParameter('user', $user)
            ->setParameter('forum', $forum)
            ->getQuery()
            ->getResult();

        foreach ($result as $value) {
            $output[$value['id']] = $value['visitTime'];
        }

        return $output;
    }
}
