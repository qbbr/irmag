<?php

namespace Irmag\ForumBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Irmag\ForumBundle\Entity\ForumTopic;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ProfileBundle\Entity\User;

class ForumPostRepository extends EntityRepository
{
    /**
     * Сколько последних постов попадают в Atom feed.
     */
    const ATOM_FEED_POSTS_AMOUNT = 10;

    /**
     * Получить все активные посты темы.
     *
     * @param ForumTopic $topic
     *
     * @return QueryBuilder
     */
    public function getAllPostsByTopic(ForumTopic $topic): QueryBuilder
    {
        return $this->createQueryBuilder('fp')
            ->select('fp as post, fu, pa, v, fv, fuv')
            ->addSelect('fp.createdAt AS HIDDEN createdAt')
            ->leftJoin('fp.attachments', 'pa')
            ->leftJoin('fp.postVotes', 'v')
            ->leftJoin('v.user', 'fv')
            ->join('fp.user', 'fu')
            ->leftJoin('fu.forumVisits', 'fuv')
            ->where('fp.topic = :topic')
            ->orderBy('fp.createdAt', 'asc')
            ->setParameter('topic', $topic);
    }

    /**
     * Возвращает последний опубликованный в теме пост.
     *
     * @param ForumTopic $topic
     *
     * @return ForumPost|null
     */
    public function getLatestCreatedPost(ForumTopic $topic): ?ForumPost
    {
        return $this->createQueryBuilder('fp')
            ->where('fp.topic = :topic')
            ->orderBy('fp.createdAt', 'desc')
            ->setParameter('topic', $topic)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Получить последний пост, который был создан / изменён.
     *
     * @param ForumTopic $topic
     *
     * @return ForumPost|null
     */
    public function getLatestModifiedPost(ForumTopic $topic): ?ForumPost
    {
        return $this->createQueryBuilder('fp')
            ->where('fp.topic = :topic')
            ->orderBy('fp.updatedAt', 'desc')
            ->setParameter('topic', $topic)
            ->setMaxResults(1)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Вернуть предпоследний пост.
     *
     * @param ForumTopic $topic
     *
     * @return ForumPost|null
     */
    public function getPreLatestPost(ForumTopic $topic): ?ForumPost
    {
        $latestPosts = $this->createQueryBuilder('fp')
            ->where('fp.topic = :topic')
            ->orderBy('fp.createdAt', 'desc')
            ->setParameter('topic', $topic)
            ->setMaxResults(2)
            ->getQuery()
            ->getResult();

        return (\count($latestPosts) > 1) ? $latestPosts[1] : null;
    }

    /**
     * Получить все посты пользователя.
     *
     * @param User $user Пользователь
     *
     * @return QueryBuilder
     */
    public function getAllForumPostsByUser(User $user): QueryBuilder
    {
        return $this->createQueryBuilder('fp')
            ->addSelect('fp.createdAt AS HIDDEN createdAt')
            ->join('fp.topic', 'ft')
            ->where('fp.user = :user')
            ->orderBy('fp.createdAt')
            ->addOrderBy('fp.topic')
            ->setParameter('user', $user);
    }

    /**
     * Получить последние N постов из темы для Atom Feed.
     *
     * @param ForumTopic $topic Тема форума
     *
     * @return array
     */
    public function getLatestTopicPosts(ForumTopic $topic): array
    {
        return $this->createQueryBuilder('fp')
            ->where('fp.topic = :topic')
            ->orderBy('fp.createdAt', 'DESC')
            ->setParameter('topic', $topic)
            ->setMaxResults(self::ATOM_FEED_POSTS_AMOUNT)
            ->getQuery()
            ->getResult();
    }

    /**
     * Получить информацию по профилю на форуме.
     *
     * @param User $user Пользователь
     *
     * @return array
     */
    public function getForumUserInfo(User $user): array
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('fu', 'tsb', 'vs')
            ->addSelect('
                (SELECT COUNT(fp.id)
                 FROM IrmagForumBundle:ForumPost fp
                 WHERE fp.user = :user
                 ) AS posts_cnt')
            ->addSelect('
                (SELECT COUNT(ft.id)
                 FROM IrmagForumBundle:ForumTopic ft
                 WHERE ft.user = :user
                ) AS topics_cnt')
            ->addSelect('
                (SELECT COUNT(fpv.id)
                 FROM IrmagForumBundle:ForumPostVote fpv
                 WHERE fpv.user = :user
                ) AS votes_cnt')
            ->addSelect('
                (SELECT COALESCE(SUM(pv1.score), 0)
                 FROM IrmagForumBundle:ForumPostVote pv1
                 JOIN pv1.post p
                 JOIN p.user a
                 WHERE a = :user
                ) AS voted_cnt')
            ->from(User::class, 'fu')
            ->leftJoin('fu.topicSubscriptions', 'tsb')
            ->leftJoin('fu.forumVisits', 'vs')
            ->where('fu = :user')
            ->setParameter('user', $user)
            ->getQuery()
            ->getSingleResult();
    }

    /**
     * Получить список форумных пользователей со статистикой.
     *
     * @return QueryBuilder
     */
    public function getAllForumUsers(): QueryBuilder
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('fu AS forumuser')
            ->addSelect('
                (SELECT COUNT(fp)
                 FROM IrmagForumBundle:ForumPost fp
                 JOIN fp.user fus
                 WHERE fus = fu
                ) AS posts_cnt')
            ->addSelect('
                (SELECT COALESCE(SUM(pv1.score), 0)
                 FROM IrmagForumBundle:ForumPostVote pv1
                 JOIN pv1.post p
                 JOIN p.user a
                 WHERE a = fu
                ) AS voted_cnt')
            ->from(User::class, 'fu')
            ->leftJoin('fu.forumVisits', 'fv')
            ->where('(SELECT COUNT(fp2)
                 FROM IrmagForumBundle:ForumPost fp2
                 JOIN fp2.user fus2
                 WHERE fus2 = fu
                ) > 0');
    }
}
