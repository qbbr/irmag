<?php

namespace Irmag\ForumBundle\Repository;

use Irmag\AttachmentBundle\Traits\AttachmentsRepositoryTrait;

class ForumPostAttachmentRepository extends \Doctrine\ORM\EntityRepository
{
    use AttachmentsRepositoryTrait;
}
