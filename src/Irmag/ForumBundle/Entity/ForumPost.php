<?php

namespace Irmag\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachable;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachmentPoint;
use Irmag\ProfileBundle\Entity\User;

/**
 * @ORM\Table(name="forum_posts")
 * @ORM\Entity(repositoryClass="\Irmag\ForumBundle\Repository\ForumPostRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ForumPost implements IrmagAttachmentPoint
{
    use TimestampableEntity;

    const ATTACHMENT_CLASS_NAME = ForumPostAttachment::class;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Автор поста.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * Кем отредактирован.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="Irmag\ProfileBundle\Entity\User")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $userModifiedBy;

    /**
     * Ссылка на сообщение, на которое ответили.
     *
     * @var ForumPost
     *
     * @ORM\ManyToOne(targetEntity="ForumPost")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="SET NULL")
     */
    private $inReplyTo;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     */
    private $text;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $textHtml;

    /**
     * Тема, в которой находится пост.
     *
     * @var ForumTopic
     *
     * @ORM\ManyToOne(targetEntity="ForumTopic", inversedBy="topicPosts")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $topic;

    /**
     * @var Collection|ForumPostAttachment[]
     *
     * @ORM\OneToMany(targetEntity="ForumPostAttachment", mappedBy="object", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $attachments;

    /**
     * @var Collection|ForumPostVote[]
     *
     * @ORM\OneToMany(targetEntity="ForumPostVote", mappedBy="post")
     */
    private $postVotes;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    private $totalScore;

    /**
     * Признак активности (true - активен и виден, false - скрыт).
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    public function __construct()
    {
        $this->totalScore = 0;
        $this->isActive = true;
        $this->attachments = new ArrayCollection();
        $this->postVotes = new ArrayCollection();
    }

    /**
     * Get attachment class name.
     * XXX: custom.
     *
     * @return string
     */
    public function getAttachmentClassName(): string
    {
        return self::ATTACHMENT_CLASS_NAME;
    }

    /**
     * Get object class name.
     * Use it to get class, because standard "get_class()" returns classname with proxy namespaces.
     * XXX: custom.
     *
     * @return string
     */
    public function getClassName(): string
    {
        return __CLASS__;
    }

    /**
     * XXX: Custom.
     *
     * @ORM\PostPersist
     */
    public function addToParentTopic(): void
    {
        $totalPostsInTopic = $this->topic->getTotalPosts();
        $this->topic->setTotalPosts(++$totalPostsInTopic);
        $this->topic->setLastPost($this);
    }

    /**
     * XXX: Custom.
     *
     * @ORM\PreRemove
     *
     * @param LifecycleEventArgs $args
     */
    public function deleteFromParentTopic(LifecycleEventArgs $args): void
    {
        $em = $args->getEntityManager();
        $totalPostsInTopic = $this->topic->getTotalPosts();
        $this->topic->setTotalPosts(--$totalPostsInTopic);
        $this->topic->setLastPost(
            $em->getRepository(self::class)->getPreLatestPost($this->topic)
        );
    }

    /**
     * XXX: Custom for Sonata Admin.
     *
     * @param Collection $postAttachments
     *
     * @return ForumPost
     */
    public function setPostAttachments(Collection $postAttachments): self
    {
        if (\count($postAttachments) > 0) {
            foreach ($postAttachments as $postAttachment) {
                $postAttachment->setFile($postAttachment->getFile());
                $postAttachment->setObject($this);
                $this->addAttachment($postAttachment);
            }
        }

        return $this;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getTextHtml(): ?string
    {
        return $this->textHtml;
    }

    public function setTextHtml(?string $textHtml): self
    {
        $this->textHtml = $textHtml;

        return $this;
    }

    public function getTotalScore(): ?int
    {
        return $this->totalScore;
    }

    public function setTotalScore(?int $totalScore): self
    {
        $this->totalScore = $totalScore;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUserModifiedBy(): ?User
    {
        return $this->userModifiedBy;
    }

    public function setUserModifiedBy(?User $userModifiedBy): self
    {
        $this->userModifiedBy = $userModifiedBy;

        return $this;
    }

    public function getInReplyTo(): ?self
    {
        return $this->inReplyTo;
    }

    public function setInReplyTo(?self $inReplyTo): self
    {
        $this->inReplyTo = $inReplyTo;

        return $this;
    }

    public function getTopic(): ?ForumTopic
    {
        return $this->topic;
    }

    public function setTopic(?ForumTopic $topic): self
    {
        $this->topic = $topic;

        return $this;
    }

    /**
     * @return Collection|ForumPostAttachment[]
     */
    public function getAttachments(): Collection
    {
        return $this->attachments;
    }

    public function addAttachment(IrmagAttachable $attachment): self
    {
        if (!$this->attachments->contains($attachment)) {
            $this->attachments[] = $attachment;
            $attachment->setObject($this);
        }

        return $this;
    }

    public function removeAttachment(IrmagAttachable $attachment): self
    {
        if ($this->attachments->contains($attachment)) {
            $this->attachments->removeElement($attachment);
            // set the owning side to null (unless already changed)
            if ($attachment->getObject() === $this) {
                $attachment->setObject(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ForumPostVote[]
     */
    public function getPostVotes(): Collection
    {
        return $this->postVotes;
    }

    public function addPostVote(ForumPostVote $postVote): self
    {
        if (!$this->postVotes->contains($postVote)) {
            $this->postVotes[] = $postVote;
            $postVote->setPost($this);
        }

        return $this;
    }

    public function removePostVote(ForumPostVote $postVote): self
    {
        if ($this->postVotes->contains($postVote)) {
            $this->postVotes->removeElement($postVote);
            // set the owning side to null (unless already changed)
            if ($postVote->getPost() === $this) {
                $postVote->setPost(null);
            }
        }

        return $this;
    }
}
