<?php

namespace Irmag\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\ProfileBundle\Entity\User;

/**
 * Подписки на обновления тем.
 *
 * @ORM\Table(name="forum_topic_subscriptions")
 * @ORM\Entity
 */
class ForumTopicSubscription
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Форумная тема для подписки.
     *
     * @var ForumTopic
     *
     * @ORM\ManyToOne(targetEntity="ForumTopic", inversedBy="subscriptions")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $topic;

    /**
     * Подписчик.
     *
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="\Irmag\ProfileBundle\Entity\User", inversedBy="topicSubscriptions")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @ORM\Column(type="boolean", options={"default": true})
     */
    private $isActive;

    public function __construct()
    {
        $this->isActive = true;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setTopic(?ForumTopic $topic): self
    {
        $this->topic = $topic;

        return $this;
    }

    public function getTopic(): ?ForumTopic
    {
        return $this->topic;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }
}
