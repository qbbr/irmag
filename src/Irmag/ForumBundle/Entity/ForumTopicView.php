<?php

namespace Irmag\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\SiteBundle\Service\ViewManager\IrmagViewableInterface;
use Irmag\SiteBundle\Service\ViewManager\IrmagViewInterface;

/**
 * @ORM\Table(name="forum_topics_views", indexes={
 *     @ORM\Index(name="forum_topics_search_idx", columns={"topic_id", "token"})
 * })
 * @ORM\Entity
 */
class ForumTopicView implements IrmagViewInterface
{
    use TimestampableEntity;

    /**
     * Время жизни просмотра, в часах.
     */
    const FORUM_TOPIC_VIEW_LIFETIME = 1;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $token;

    /**
     * @var ForumTopic
     *
     * @ORM\ManyToOne(targetEntity="ForumTopic")
     * @ORM\JoinColumn(name="topic_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $object;

    /**
     * XXX: Custom.
     *
     * @return int
     */
    public function getViewLifetime(): int
    {
        return self::FORUM_TOPIC_VIEW_LIFETIME;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setObject(?IrmagViewableInterface $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getObject(): ?IrmagViewableInterface
    {
        return $this->object;
    }
}
