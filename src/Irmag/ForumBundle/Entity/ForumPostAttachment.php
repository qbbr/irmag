<?php

namespace Irmag\ForumBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Irmag\AttachmentBundle\Traits\AttachableTrait;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachable;
use Irmag\AttachmentBundle\Interfaces\IrmagAttachmentPoint;

/**
 * @ORM\Table(name="forum_post_attachments")
 * @ORM\Entity(repositoryClass="\Irmag\ForumBundle\Repository\ForumPostAttachmentRepository")
 */
class ForumPostAttachment implements IrmagAttachable
{
    use TimestampableEntity;
    use AttachableTrait;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Пост, к которому прикрепляется файл.
     *
     * @var ForumPost
     *
     * @ORM\ManyToOne(targetEntity="ForumPost", inversedBy="attachments")
     * @ORM\JoinColumn(referencedColumnName="id", onDelete="CASCADE")
     */
    private $object;

    public function __construct()
    {
        $this->isActive = true;
    }

    /**
     * XXX: Custom for SonataAdmin.
     */
    public function __toString(): string
    {
        return (string) $this->id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getObject(): ?IrmagAttachmentPoint
    {
        return $this->object;
    }

    public function setObject(?IrmagAttachmentPoint $object): self
    {
        $this->object = $object;

        return $this;
    }
}
