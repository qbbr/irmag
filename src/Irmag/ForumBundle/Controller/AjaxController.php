<?php

namespace Irmag\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ForumBundle\Entity\Forum;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ForumBundle\Entity\ForumTopic;
use Irmag\ForumBundle\Entity\ForumPostVote;
use Irmag\ForumBundle\Entity\ForumVisit;

/**
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 * @Route("/ajax",
 *     options={"expose": true},
 *     condition="request.isXmlHttpRequest()",
 *     methods={"POST"},
 *     host="%irmag_forum_domain%",
 * )
 */
class AjaxController extends Controller
{
    /**
     * @Route("/quote/", name="irmag_forum_get_quote")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function getPostQuoteAction(Request $request): JsonResponse
    {
        $id = $request->request->getInt('id');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $post = $em->getRepository(ForumPost::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException('Post did not found!');
        }

        $name = $post->getUser()->getName();
        $md = $post->getText();
        $md = sprintf(
            "_Цитата_ ([#%d](%s) _от_ _%s_):\n %s \n\n",
            $id,
            $this->generateUrl('irmag_forum_goto_topic', [
                'id' => $post->getTopic()->getId(),
                'post' => $id,
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
            ),
            $name,
            preg_replace('/^(?m)([\w\W])?+/m', '> ${0}', $md)
        );

        return new JsonResponse([
            'success' => true,
            'md' => $md,
        ]);
    }

    /**
     * @Route("/vote/", name="irmag_forum_vote")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function voteAction(Request $request): JsonResponse
    {
        $id = $request->request->getInt('id');
        $score = $request->request->getInt('score');
        $em = $this->get('doctrine.orm.default_entity_manager');

        $forumUser = $this->getUser();
        $post = $em->getRepository(ForumPost::class)->find($id);

        if (!$post) {
            throw $this->createNotFoundException('Post did not found!');
        }

        $vote = new ForumPostVote();
        $vote->setUser($forumUser);
        $vote->setScore($score);
        $vote->setPost($post);
        $em->persist($vote);
        $em->flush();

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/visit/", name="irmag_forum_log_visitor")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function logVisit(Request $request): JsonResponse
    {
        $topicId = $request->request->getInt('topic');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $topic = $em->getRepository(ForumTopic::class)->find($topicId);

        if (!$topic) {
            throw $this->createNotFoundException('Topic did not found!');
        }

        $user = $this->getUser();
        $existingRecords = $user->getForumVisits();

        /** @var ForumVisit $record */
        foreach ($existingRecords as $record) {
            if ($record->getTopic() === $topic) {
                $visit = $record;
                break;
            }
        }

        if (!isset($visit)) {
            $visit = new ForumVisit();
            $visit->setUser($user);
            $visit->setTopic($topic);
        }

        $visit->setVisitTime(new \DateTime('now'));
        $em->persist($visit);
        $em->flush();

        return new JsonResponse(['success' => true]);
    }

    /**
     * @Route("/get_users/", name="irmag_forum_get_users")
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function getUsersToWrite(Request $request): JsonResponse
    {
        $query = $request->request->get('term');
        $user = $this->getUser();
        $found = [];

        if (!empty($query) && mb_strlen($query) > 1) {
            // XXX: LIKE query n to refactor, its a crap!
            $result = $this->get('doctrine.orm.default_entity_manager')
                ->getRepository(User::class)
                ->createQueryBuilder('u')
                ->select("COALESCE(CONCAT(u.username, ' (', u.fullname, ')'), u.username) AS name", 'u.id')
                ->where('u.isActive = true')
                ->andWhere('u.id != :currentUserId')
                ->andWhere('LOWER(u.username) LIKE :term OR LOWER(u.fullname) LIKE :term')
                ->setParameter('currentUserId', $user->getId())
                ->setParameter('term', '%'.mb_strtolower($query).'%')
                ->setMaxResults(10)
                ->getQuery()
                ->getResult();

            foreach ($result as $item) {
                $found['results'][] = [
                    'id' => $item['id'],
                    'name' => $item['name'],
                ];
            }
        }

        return new JsonResponse($found);
    }

    /**
     * @Route("/readalltopics/", name="irmag_forum_read_topics")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function markAllTopicsAsRead(Request $request): JsonResponse
    {
        $forumId = $request->request->getInt('forum');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $user = $this->getUser();
        $forum = $em->getRepository(Forum::class)->find($forumId);

        if (!$forum) {
            throw $this->createNotFoundException('Forum did not found!');
        }

        $topics = $em->getRepository(ForumTopic::class)->findBy([
            'forum' => $forum,
        ]);

        foreach ($topics as $topic) {
            $existsFlag = false;

            /** @var ForumVisit $alreadyVisited */
            foreach ($user->getForumVisits() as $alreadyVisited) {
                if ($alreadyVisited->getTopic() === $topic) {
                    $existsFlag = true;
                    $alreadyVisited->setVisitTime(new \DateTime('now'));
                    $em->persist($alreadyVisited);
                    break;
                }
            }

            if (false === $existsFlag) {
                $visit = new ForumVisit();
                $visit->setUser($user);
                $visit->setTopic($topic);
                $visit->setVisitTime(new \DateTime('now'));
                $em->persist($visit);
            }
        }

        $em->flush();

        return new JsonResponse(['success' => true]);
    }
}
