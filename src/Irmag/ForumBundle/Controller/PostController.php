<?php

namespace Irmag\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Irmag\SiteBundle\Config;
use Irmag\SiteBundle\Entity\Element;
use Irmag\CoreBundle\Model\Feed;
use Irmag\CoreBundle\Model\FeedEntry;
use Irmag\CoreBundle\Service\FeedManager\FeedManager;
use Irmag\ProfileBundle\Entity\Group;
use Irmag\ForumBundle\ForumConfig;
use Irmag\ForumBundle\Exception\IrmagForumException;
use Irmag\ForumBundle\Form\Type\ForumPostType;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ForumBundle\Entity\ForumTopic;
use Irmag\ForumBundle\Entity\ForumPostAttachment;
use Irmag\ForumBundle\Service\ForumService;
use Irmag\ForumBundle\EventDispatcher\ForumPostEventDispatcher;

class PostController extends Controller
{
    /**
     * @Route("/topic{id}/", name="irmag_forum_goto_topic", requirements={"id": "\d+"})
     *
     * @param Request $request
     * @param int     $id      ID темы форума
     *
     * @throws AccessDeniedHttpException
     *
     * @return Response
     */
    public function viewTopicAction(Request $request, int $id): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $topic = $this->getEntityByIdOrThrowException(ForumTopic::class, $id);
        $forumService = $this->get(ForumService::class);
        $posts = $em->getRepository(ForumPost::class)->getAllPostsByTopic($topic);

        if ($request->query->get('element')) {
            $refererElement = $em->getRepository(Element::class)->find($request->query->getInt('element'));

            if ($refererElement) {
                $initialText = sprintf(
                    '**Вопрос по товару: [%s](%s)**  ',
                    $refererElement->getFullName(),
                    $this->get('router')->generate(
                        'irmag_catalog_element',
                        ['id' => $refererElement->getId()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    )
                );

                $page = ceil(\count($posts->getQuery()->getResult()) / ForumConfig::POSTS_PER_PAGE);
            }
        }

        $pagination = $this->get('knp_paginator')->paginate(
            $posts,
            $request->query->getInt('page', $page ?? 1),
            ForumConfig::POSTS_PER_PAGE,
            [
                'sortFieldWhitelist' => ['createdAt'],
                'defaultSortFieldName' => 'createdAt',
                'defaultSortDirection' => 'asc',
            ]
        );

        $queryParams = $pagination->getQuery();
        $paginationData = $pagination->getPaginationData();
        $queryParams['totalCount'] = $paginationData['totalCount'];
        $queryParams['numItemsPerPage'] = $paginationData['numItemsPerPage'];
        $queryParams['route'] = $request->get('_route');

        // если ссылка на пост - определяем на какой он странице и переходим
        if ($request->query->get('post')) {
            return $this->goToPost($request->query->get('post'), $posts, $queryParams);
        }

        $moderatorGroup = $em->getRepository(Group::class)->findOneBy(['shortname' => 'forum_moderators']);
        $moderators = ($moderatorGroup) ? $moderatorGroup->getUsers() : [];

        return $this->render('@IrmagForum/Topic/topic.html.twig', [
            'topic' => $topic,
            'pagination' => $pagination,
            'moderators' => $moderators,
            'is_subscribed' => $forumService->isUserSubscribedToTopic($topic),
            'initial_text' => $initialText ?? '',
        ]);
    }

    /**
     * @Route("/topic{id}/new", name="irmag_forum_new_post", requirements={"id": "\d+"})
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     * @param int     $id
     *
     * @return RedirectResponse|Response
     */
    public function createOrUpdatePostAction(Request $request, int $id)
    {
        $user = $this->getUser();
        $topic = $this->getEntityByIdOrThrowException(ForumTopic::class, $id);
        $initialText = $request->get('initialText') ?? '';

        /* Если редактирование */
        $postId = $request->get('postId');

        if (!empty($postId)) {
            $post = $this->getEntityByIdOrThrowException(ForumPost::class, $postId);

            if ($user !== $post->getUser()) {
                throw new AccessDeniedHttpException(sprintf('User "%s" has no rights to update post "%d"', (string) $user, $post->getId()));
            }
        }

        if ($request->isMethod(Request::METHOD_POST)) {
            $forumService = $this->get(ForumService::class);

            if ($forumService->isUserBannedToWritePost()) {
                throw new AccessDeniedHttpException(sprintf('User "%s" has no rights to comment on forum', (string) $user));
            }

            $newPost = $forumService->createOrUpdatePost($request->request->get('forum_post'));
            $forumPostEventDispatcher = $this->get(ForumPostEventDispatcher::class);

            if (true === isset($post)) {
                $forumPostEventDispatcher->dispatchForumPostEdited($post);
            } else {
                $forumPostEventDispatcher->dispatchNewForumPostCreated($newPost);
            }

            return $this->redirectToRoute('irmag_forum_goto_topic', [
                'id' => $topic->getId(),
                'post' => $newPost->getId(),
            ]);
        }

        $em = $this->get('doctrine.orm.default_entity_manager');
        $attachments = (true === isset($post))
            ? $em->getRepository(ForumPostAttachment::class)->getAllowedAttachments($post->getUser(), $post)
            : $em->getRepository(ForumPostAttachment::class)->getFreeUploadedAttachments($user);

        $form = $this->createForm(ForumPostType::class, $post ?? null, [
            'action' => $this->generateUrl('irmag_forum_new_post', ['id' => $id]),
            'topic' => $topic,
        ]);

        return $this->render('@IrmagForum/Post/edit_post.html.twig', [
            'form' => $form->createView(),
            'attaches' => $attachments,
            'initialText' => $initialText,
        ]);
    }

    /**
     * @Route("/topic{id}/edit/{postId}", name="irmag_forum_edit_post", requirements={"id": "\d+"})
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param int $postId ID поста форума
     *
     * @return Response
     */
    public function updatePostAction(int $postId): Response
    {
        $post = $this->getEntityByIdOrThrowException(ForumPost::class, $postId);

        return $this->render('@IrmagForum/Post/edit.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/topic{id}/atom/", name="irmag_forum_topic_atom", requirements={"id": "\d+"})
     * @Cache(expires="10 minutes", public=true)
     *
     * @param int $id
     *
     * @return Response
     */
    public function topicAtomAction(int $id): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $topic = $em->getRepository(ForumTopic::class)->find($id);

        if (!$topic) {
            throw $this->createNotFoundException(sprintf('Topic with id %d does not exists.', $id));
        }

        $latestPosts = $em->getRepository(ForumPost::class)->getLatestTopicPosts($topic);

        $feed = new Feed();
        $feed->setId($this->generateUrl(
            'irmag_forum_goto_topic',
            ['id' => $topic->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL)
        );
        $feed->setTitle('Интернет-магазин IRMAG.RU — Форум — '.$topic->getTitle());
        $feed->setLinkSelf($this->generateUrl(
            'irmag_forum_topic_atom',
            ['id' => $topic->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL)
        );
        $feed->setLink($this->generateUrl(
            'irmag_forum_goto_topic',
            ['id' => $topic->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL)
        );
        $feed->setUpdated($latestPosts[0]->getUpdatedAt());

        foreach ($latestPosts as $post) {
            $feedEntry = new FeedEntry();
            $feedEntry->setId($this->generateUrl(
                'irmag_forum_goto_topic',
                ['id' => $topic->getId(), 'post' => $post->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL)
            );
            $feedEntry->setTitle(sprintf(
                '%s от %s',
                $post->getCreatedAt()->format(Config::DATETIME_FORMAT),
                $post->getUser()->getName()
            ));
            $feedEntry->setLink($this->generateUrl(
                'irmag_forum_goto_topic',
                ['id' => $topic->getId(), 'post' => $post->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL)
            );
            $feedEntry->setUpdated($post->getUpdatedAt());
            $feedEntry->setPublished($post->getCreatedAt());
            $feedEntry->setAuthor($post->getUser());
            $feedEntry->setContent(strip_tags($post->getTextHtml()));
            $feed->addEntry($feedEntry);
        }

        $feedManager = $this->get(FeedManager::class);
        $feedManager->setFeed($feed);

        return $feedManager->getResponse();
    }

    /**
     * Генерирует прямую ссылку на пост, учитывая настройки пагинатора и текущее положение.
     *
     * @param int                        $postId      ID поста
     * @param \Doctrine\ORM\QueryBuilder $posts       Список всех постов
     * @param array                      $queryParams Массив get-параметров пагинации
     * @param bool                       $isNew       Новый ли пост
     *
     * @return Response
     */
    private function goToPost(int $postId, $posts, array $queryParams, bool $isNew = false): Response
    {
        if (false === $isNew) {
            // определяем позицию указанного поста в списке постов
            $position = 0;

            foreach ($posts->getQuery()->getResult() as $key => $item) {
                if ($item['post']->getId() === $postId) {
                    $position = $key + 1;
                    break;
                }
            }
            $pageNumber = (int) (ceil($position / (int) ForumConfig::POSTS_PER_PAGE));
        } else {
            // считаем, сколько будет в пагинаторе страниц вместе с нашим текущим постом, чтобы перемотать сразу на него
            $pageNumber = (int) (ceil(((int) ($queryParams['totalCount']) + 1) / $queryParams['numItemsPerPage']));
        }

        return $this->redirect($this->generateUrl(
            $queryParams['route'], [
                'page' => $pageNumber,
                'id' => $queryParams['id'],
            ]).'#'.$postId
        );
    }

    /**
     * @param string $class
     * @param int    $id
     *
     * @throws IrmagForumException
     *
     * @return \object|null
     */
    private function getEntityByIdOrThrowException(string $class, int $id): ?object
    {
        $entity = $this->get('doctrine.orm.default_entity_manager')->getRepository($class)->find($id);

        if (!$entity) {
            throw new IrmagForumException(sprintf('Entity "%s" (id %d) did not found', $class, $id));
        }

        return $entity;
    }
}
