<?php

namespace Irmag\ForumBundle\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Irmag\ForumBundle\Entity\ForumTopic;
use Irmag\SiteBundle\Service\ViewManager\ViewManager;

/**
 * @Route("/log", options={"expose": true},
 *     condition="request.isXmlHttpRequest()",
 *     methods={"POST"},
 *     host="%irmag_forum_domain%"
 * )
 */
class ViewController extends Controller
{
    /**
     * @Route("/view/", name="irmag_forum_log_topic_view")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function logViewAction(Request $request): JsonResponse
    {
        $topicId = $request->request->getInt('objectId');
        $token = $request->request->get('fingerprint');
        /** @var ForumTopic $topic */
        $topic = $this->get('doctrine.orm.default_entity_manager')->getRepository(ForumTopic::class)->find($topicId);

        if (!$topic) {
            throw new NotFoundHttpException(sprintf('ForumTopic with ID "%d" was not found', $topicId));
        }

        $this->get(ViewManager::class)
            ->setObject($topic)
            ->logVisitor($token);

        return new JsonResponse(['success' => true]);
    }
}
