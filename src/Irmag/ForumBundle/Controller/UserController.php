<?php

namespace Irmag\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Irmag\ProfileBundle\Entity\User;
use Irmag\ProfileBundle\Entity\Group;
use Irmag\ForumBundle\Entity\ForumPost;

class UserController extends Controller
{
    /**
     * @Route("/users/", name="irmag_forum_users")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function usersAction(Request $request): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $users = $em->getRepository(ForumPost::class)->getAllForumUsers();
        $pagination = $this->get('knp_paginator')->paginate(
            $users,
            $request->query->getInt('page', 1),
            20,
            [
                'defaultSortFieldName' => 'posts_cnt',
                'defaultSortDirection' => 'desc',
            ]
        );

        return $this->render('@IrmagForum/User/userslist.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/profile/{id}/", name="irmag_forum_profile", requirements={"id": "\d+"})
     *
     * @param int $id ID пользователя
     *
     * @return Response
     */
    public function profileAction(int $id): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $user = $em->getRepository(User::class)->find($id);

        if (!$user) {
            throw $this->createNotFoundException('User did not found!');
        }

        $userInfo = $em->getRepository(ForumPost::class)->getForumUserInfo($user);
        $visitEntities = $user->getForumVisits()->toArray();
        $visits = [];

        foreach ($visitEntities as $entity) {
            $visits[] = $entity->getVisitTime();
        }

        sort($visits);

        return $this->render('@IrmagForum/User/profile.html.twig', [
            'userinfo' => $userInfo,
            'user' => $user,
            'topic_subscriptions' => $user->getTopicSubscriptions(),
            'visits' => $visits,
        ]);
    }

    /**
     * @Route("/user/{userId}/posts", name="irmag_forum_user_posts", requirements={"id": "\d+"})
     *
     * @param Request $request
     * @param int     $userId
     *
     * @return Response
     */
    public function userPostsAction(Request $request, int $userId): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $user = $em->getRepository(User::class)->find($userId);

        if (!$user) {
            throw $this->createNotFoundException('User did not found!');
        }

        $posts = $em->getRepository(ForumPost::class)->getAllForumPostsByUser($user);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $posts,
            $request->query->getInt('page', 1),
            20,
            [
                'sortFieldWhitelist' => ['createdAt'],
                'defaultSortFieldName' => 'createdAt',
                'defaultSortDirection' => 'desc',
            ]
        );

        $moderatorGroup = $em->getRepository(Group::class)->findOneBy(['shortname' => 'forum_moderators']);
        $moderators = ($moderatorGroup) ? $moderatorGroup->getUsers() : [];

        return $this->render('@IrmagForum/User/user-post-list.html.twig', [
            'pagination' => $pagination,
            'username' => $user->getName(),
            'moderators' => $moderators,
        ]);
    }
}
