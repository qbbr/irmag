<?php

namespace Irmag\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Irmag\ForumBundle\Entity\ForumTopicSubscription;
use Irmag\ForumBundle\Entity\ForumTopic;

/**
 * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
 * @Route("/ajax/subscribe/",
 *     options={"expose": true},
 *     condition="request.isXmlHttpRequest()",
 *     methods={"POST"},
 *     host="%irmag_forum_domain%",
 * )
 */
class SubscriptionController extends Controller
{
    /**
     * @Route("/", name="irmag_forum_subscribe_to_topic")
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function subscribeToTopicAction(Request $request): JsonResponse
    {
        $topicId = $request->request->getInt('id');
        $status = $request->request->get('status');
        $em = $this->get('doctrine.orm.default_entity_manager');
        $user = $this->getUser();

        $topic = $em->getRepository(ForumTopic::class)->find($topicId);

        if (!$topic) {
            throw $this->createNotFoundException('Topic did not found!');
        }

        if ('false' === $status) {
            $subscription = new ForumTopicSubscription();
            $subscription->setTopic($topic);
            $subscription->setUser($user);
            $em->persist($subscription);
        } else {
            $subscription = $em
                ->getRepository(ForumTopicSubscription::class)
                ->findOneBy(['topic' => $topic, 'user' => $user]);

            if (!$subscription) {
                throw $this->createNotFoundException('Subscription did not found!');
            }

            $em->remove($subscription);
        }

        $em->flush();

        return new JsonResponse([]);
    }

    /**
     * @Route("/unsubscribe/", name="irmag_forum_unsubscribe")
     *
     * @param Request $request
     *
     * @throws \Exception
     *
     * @return JsonResponse
     */
    public function unsubscribeAction(Request $request): JsonResponse
    {
        $id = $request->request->getInt('id');
        $em = $this->get('doctrine.orm.default_entity_manager');

        $subscription = $em->getRepository(ForumTopicSubscription::class)->find($id);

        if (!$subscription) {
            throw $this->createNotFoundException('Subscription did not found!');
        }

        $em->remove($subscription);
        $em->flush();

        return new JsonResponse([]);
    }
}
