<?php

namespace Irmag\ForumBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Irmag\CoreBundle\Model\Feed;
use Irmag\CoreBundle\Model\FeedEntry;
use Irmag\CoreBundle\Service\FeedManager\FeedManager;
use Irmag\ForumBundle\ForumConfig;
use Irmag\ForumBundle\Form\Type\ForumTopicType;
use Irmag\ForumBundle\Entity\Forum;
use Irmag\ForumBundle\Entity\ForumTopic;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ForumBundle\Entity\ForumPostAttachment;
use Irmag\ForumBundle\Service\ForumService;
use Irmag\ForumBundle\EventDispatcher\ForumTopicEventDispatcher;

class TopicController extends Controller
{
    /**
     * Все топики форума.
     *
     * @Route("/", name="irmag_forum_homepage")
     * @Route("/forum/", name="irmag_forum_goto_forum")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function viewForumAction(Request $request): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $forum = $em->getRepository(Forum::class)->getForum();

        if (!$forum) {
            throw $this->createNotFoundException('No forums was found.');
        }

        if ($this->getUser()) {
            $seen = $em->getRepository(ForumTopic::class)->getAllSeenTopics($this->getUser(), $forum);
        }

        $topics = $em->getRepository(ForumTopic::class)->getAllTopicsByForum($forum);
        $pagination = $this->get('knp_paginator')->paginate(
            $topics,
            $request->query->getInt('page', 1),
            ForumConfig::TOPICS_PER_PAGE,
            [
                'sortFieldWhitelist' => ['lastPostCreatedAt'],
                'defaultSortFieldName' => 'lastPostCreatedAt',
                'defaultSortDirection' => 'desc',
            ]
        );

        return $this->render('@IrmagForum/Forum/forum.html.twig', [
            'seen' => $seen ?? [],
            'forum' => $forum,
            'posts_per_page' => ForumConfig::POSTS_PER_PAGE,
            'pagination' => $pagination,
        ]);
    }

    /**
     * @Route("/forum/atom/", name="irmag_forum_atom")
     * @Route("/forum{id}/atom/", name="irmag_forum_atom_duplicate", requirements={"id": "\d+"})
     * @Cache(expires="10 minutes", public=true)
     *
     * @return Response
     */
    public function forumAtomAction(): Response
    {
        $em = $this->get('doctrine.orm.default_entity_manager');
        $forum = $em->getRepository(Forum::class)->getForum();

        if (!$forum) {
            throw $this->createNotFoundException('No forum found.');
        }

        $topics = $em->getRepository(ForumTopic::class)->findBy(['forum' => $forum]);
        $latestTopic = $em->getRepository(ForumTopic::class)->getLatest($forum);

        $feed = new Feed();
        $feed->setId($this->generateUrl('irmag_forum_homepage', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setTitle('Интернет-магазин IRMAG.RU — Форум — '.$forum->getName());
        $feed->setLinkSelf($this->generateUrl('irmag_forum_atom', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setLink($this->generateUrl('irmag_forum_homepage', [], UrlGeneratorInterface::ABSOLUTE_URL));
        $feed->setUpdated($latestTopic->getUpdatedAt());

        foreach ($topics as $topic) {
            $post = $em->getRepository(ForumPost::class)->getLatestModifiedPost($topic);
            $feedEntry = new FeedEntry();
            $feedEntry->setId($this->generateUrl(
                'irmag_forum_goto_topic',
                ['id' => $topic->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL)
            );
            $feedEntry->setTitle($topic->getTitle());
            $feedEntry->setLink($this->generateUrl(
                'irmag_forum_goto_topic',
                ['id' => $topic->getId()],
                UrlGeneratorInterface::ABSOLUTE_URL)
            );
            $feedEntry->setUpdated($topic->getUpdatedAt());
            $feedEntry->setPublished($topic->getCreatedAt());
            $feedEntry->setAuthor($topic->getUser());
            $feedEntry->setContent(strip_tags($post->getTextHtml()));
            $feed->addEntry($feedEntry);
        }

        $feedManager = $this->get(FeedManager::class);
        $feedManager->setFeed($feed);

        return $feedManager->getResponse();
    }

    /**
     * @Route("/forum{id}/new/",
     *     name="irmag_forum_new_topic",
     *     requirements={"id": "\d+"},
     *     options={"expose": true}
     * )
     * @Security("is_granted('IS_AUTHENTICATED_REMEMBERED')")
     *
     * @param Request $request
     * @param int     $id      ID форума
     *
     * @return Response
     */
    public function newTopicAction(Request $request, int $id): Response
    {
        $forumService = $this->get(ForumService::class);
        $em = $this->get('doctrine.orm.default_entity_manager');

        if ($forumService->isUserBannedToCreateTopic()) {
            throw new AccessDeniedHttpException(sprintf('User "%s" has no rights to create forum topics', (string) $this->getUser()));
        }

        $forum = $em->getRepository(Forum::class)->find($id);

        if (!$forum) {
            throw $this->createNotFoundException('Forum does not exists');
        }

        $tags = $forumService->getAllUserTopicsTags();
        $form = $this->createForm(ForumTopicType::class, null, ['tags' => $tags, 'forum' => $id]);

        if ($request->isMethod(Request::METHOD_POST)) {
            $topic = $forumService->newTopic($request->request->get('forum_topic'));
            $this->get(ForumTopicEventDispatcher::class)->dispatchNewForumTopicCreated($topic);

            return $this->render('@IrmagForum/Topic/topic-created.html.twig', [
                'topic' => $topic,
            ]);
        }

        return $this->render('@IrmagForum/Topic/new-topic.html.twig', [
            'forum' => $forum,
            'form' => $form->createView(),
            'attaches' => $em
                ->getRepository(ForumPostAttachment::class)->getAllowedAttachments($this->getUser(), null),
        ]);
    }
}
