<?php

namespace Irmag\ForumBundle;

final class IrmagForumEvents
{
    /**
     * Это событие вызывается, когда написан новый пост на форуме.
     */
    const FORUM_NEW_POST_CREATED = 'irmag.forum.post.created';

    /**
     * Это событие вызывается, когда пост отредактирован.
     */
    const FORUM_POST_EDITED = 'irmag.forum.post.edited';

    /**
     * Это событие вызывается, когда создана новая тема.
     */
    const FORUM_TOPIC_CREATED = 'irmag.forum.topic.created';
}
