<?php

namespace Irmag\ForumBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Irmag\ForumBundle\ForumConfig;
use Irmag\ForumBundle\Entity\ForumPost;

class ForumPostSubscriber implements EventSubscriber
{
    /**
     * {@inheritdoc}
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'preRemove',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->updateUserForumRating($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args): void
    {
        $this->updateUserForumRating($args, true);
    }

    /**
     * Обновить рейтинг пользователя на форуме.
     *
     * @param LifecycleEventArgs $args      Аргументы
     * @param bool               $isExclude Исключить из текущего рейтинга?
     */
    private function updateUserForumRating(LifecycleEventArgs $args, bool $isExclude = false): void
    {
        $entity = $args->getObject();

        if ($entity instanceof ForumPost) {
            $user = $entity->getUser();
            $currentRating = $user->getForumRating();
            $em = $args->getObjectManager();

            if ($isExclude) {
                $currentRating -= (ForumConfig::FORUM_RATING_POSTS_AMOUNT_MULTIPLIER * 1);
            } else {
                $currentRating += (ForumConfig::FORUM_RATING_POSTS_AMOUNT_MULTIPLIER * 1);
            }

            $user->setForumRating($currentRating);

            $em->persist($user);
            $em->flush();
        }
    }
}
