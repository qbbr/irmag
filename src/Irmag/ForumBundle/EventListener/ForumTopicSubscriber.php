<?php

namespace Irmag\ForumBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Irmag\ProfileBundle\Entity\Group;
use Irmag\ForumBundle\Event\ForumTopicEvent;
use Irmag\ForumBundle\Mailer\ForumMailer;
use Irmag\ForumBundle\IrmagForumEvents;

class ForumTopicSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ForumMailer
     */
    private $mailer;

    /**
     * @param EntityManagerInterface $em
     * @param ForumMailer            $mailer
     */
    public function __construct(
        EntityManagerInterface $em,
        ForumMailer $mailer
    ) {
        $this->em = $em;
        $this->mailer = $mailer;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagForumEvents::FORUM_TOPIC_CREATED => 'sendEmailsOnTopicCreate',
        ];
    }

    /**
     * @param ForumTopicEvent $event
     */
    public function sendEmailsOnTopicCreate(ForumTopicEvent $event): void
    {
        $topic = $event->getForumTopic();

        // уведомление модераторам
        $moderatorGroup = $this->em->getRepository(Group::class)->findOneBy(['shortname' => 'forum_moderators']);

        if ($moderatorGroup) {
            $moderators = $moderatorGroup->getUsers();

            foreach ($moderators as $moderator) {
                if (!empty($moderator->getEmail()) && $topic->getUser() !== $moderator) {
                    $this->mailer->sendNewTopicMessage($moderator->getEmail(), ['topic' => $topic]);
                }
            }
        }
    }
}
