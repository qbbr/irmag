<?php

namespace Irmag\ForumBundle\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Irmag\ForumBundle\Entity\ForumPost;
use Irmag\ForumBundle\Event\ForumPostEvent;
use Irmag\ForumBundle\Mailer\ForumMailer;
use Irmag\ForumBundle\IrmagForumEvents;
use Irmag\ProfileBundle\Service\UserNotificationEventManager\UserNotificationEventManager;
use Irmag\ProfileBundle\Service\UserNotificationQueueManager\UserNotificationQueueManager;
use Irmag\ProfileBundle\Entity\Group;
use Irmag\ProfileBundle\NotificationEvents;

class NotificationForumPostSubscriber implements EventSubscriberInterface
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var ForumMailer
     */
    private $mailer;

    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * @var UserNotificationEventManager
     */
    private $userNotificationEventManager;

    /**
     * @var UserNotificationQueueManager
     */
    private $userNotificationQueueManager;

    /**
     * @param EntityManagerInterface       $em
     * @param ForumMailer                  $mailer
     * @param UrlGeneratorInterface        $router
     * @param UserNotificationEventManager $userNotificationEventManager
     * @param UserNotificationQueueManager $userNotificationQueueManager
     */
    public function __construct(
        EntityManagerInterface $em,
        ForumMailer $mailer,
        UrlGeneratorInterface $router,
        UserNotificationEventManager $userNotificationEventManager,
        UserNotificationQueueManager $userNotificationQueueManager
    ) {
        $this->em = $em;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->userNotificationEventManager = $userNotificationEventManager;
        $this->userNotificationQueueManager = $userNotificationQueueManager;
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            IrmagForumEvents::FORUM_NEW_POST_CREATED => 'sendEmailsOnPostCreate',
        ];
    }

    /**
     * @param ForumPostEvent $event
     */
    public function sendEmailsOnPostCreate(ForumPostEvent $event): void
    {
        $post = $event->getForumPost();

        // уведомление модераторам
        $moderatorGroup = $this->em->getRepository(Group::class)->findOneBy(['shortname' => 'forum_moderators']);

        if ($moderatorGroup) {
            $moderators = $moderatorGroup->getUsers();

            foreach ($moderators as $moderator) {
                if (!empty($moderator->getEmail()) && $post->getUser() !== $moderator) {
                    $this->mailer->sendNewPostMessage($moderator->getEmail(), ['post' => $post]);
                }
            }
        }

        // уведомление подписчикам
        $subscriptions = $post->getTopic()->getSubscriptions();

        foreach ($subscriptions as $sub) {
            $subscriber = $sub->getUser();

            if ($subscriber !== $post->getUser() && !empty($subscriber->getEmail())) {
                $this->mailer->sendNewSubscriptionPostMessage($subscriber->getEmail(), ['post' => $post]);
            }
        }

        // reply
        if ($post->getInReplyTo() instanceof ForumPost) {
            $event = $this->userNotificationEventManager->getEventByName(NotificationEvents::FORUM_POST_REPLY);
            $url = $this->router->generate('irmag_forum_goto_topic', ['id' => $post->getTopic()->getId(), 'post' => $post->getId()], UrlGeneratorInterface::ABSOLUTE_URL);
            $this->userNotificationQueueManager->create($event, $post->getInReplyTo()->getUser(), sprintf('от %s', $post->getUser()->getName()), $url);
        }
    }
}
