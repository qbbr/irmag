<?php

namespace Irmag\ForumBundle\Exception;

use Irmag\CoreBundle\Exception\IrmagException;

class IrmagForumException extends IrmagException
{
}
