# CONFIGURATION

## Domains

 * irmag.ru
 * profile.irmag.ru
 * blog.irmag.ru
 * forum.irmag.ru
 * admin.irmag.ru
 * exchange-1c.irmag.ru (for local only)
 * api.irmag.ru (for local only)

## Depends

```
apt install apt-transport-https lsb-release ca-certificates
```

## Nginx

[nginx.org repo](http://nginx.org/ru/linux_packages.html)

```bash
# add repo
echo "deb http://nginx.org/packages/debian/ $(lsb_release -sc) nginx" | tee /etc/apt/sources.list.d/nginx.list
echo "#deb-src http://nginx.org/packages/debian/ $(lsb_release -sc) nginx" | tee -a /etc/apt/sources.list.d/nginx.list
# add GnuPG key
wget -O - http://nginx.org/keys/nginx_signing.key | apt-key add -
# install nginx
apt update
apt install nginx
```

## PHP

```bash
# add repo
echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | tee /etc/apt/sources.list.d/php.list
# add GnuPG key
wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
# install php (see Makefile)
make dev-install-depends-apt
# configure
make dev-configure-php-env
```

## PostgreSQL

See: [PostgreSQL: Linux downloads (Debian)](https://www.postgresql.org/download/linux/debian/)

```bash
su - postgres -c psql
```

```sql
CREATE USER symfony WITH PASSWORD 'symfony123';
CREATE DATABASE symfony_db OWNER symfony;
\q
```

### Dump and Restore

Database:

```bash
# dump
pg_dump symfony_db | gzip > symfony_db.gz
# hard drop/create if n
DROP DATABASE symfony_db;
CREATE DATABASE symfony_db OWNER symfony;
# restore
cat symfony_db.gz | gunzip | psql symfony_db
```

Cluster:

```bash
# dump
pg_dumpall > dump_all.sql
# restore
psql -f dump_all.sql postgres
```

### Tuning

[PgTune](https://pgtune.leopard.in.ua/)

### TS Vector Search Synonyms

Synonym and thesaurus files must be exists.

```bash
touch /usr/share/postgresql/9.6/tsearch_data/{irmag_synonym.syn,irmag_thesaurus.ths}
make pgsql-recreate-ts-search
```

## Redis

```bash
apt install redis-server/stretch-backports
```

`/etc/redis/redis.conf`:

```conf
bind 127.0.0.1
appendonly no
maxmemory <bytes> 
maxmemory-policy allkeys-lru
```

## ACL

Package [acl](https://packages.debian.org/jessie/acl) must be installed.

```bash
HTTPDUSER=`ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`
setfacl -R -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var web/media web/uploads config/parameters.yml
setfacl -dR -m u:"$HTTPDUSER":rwX -m u:`whoami`:rwX var web/media web/uploads
```

## FTP Server

```bash
apt install vsftpd
modprobe nf_conntrack_ftp
echo 'nf_conntrack_ftp' >> /etc/modules
```

File `/etc/vsftpd.conf`:

```ini
listen=YES
listen_ipv6=NO
anonymous_enable=NO
local_enable=YES
write_enable=YES
local_umask=022
use_localtime=YES
xferlog_enable=YES
connect_from_port_20=YES
chroot_local_user=YES
chroot_list_enable=YES
chroot_list_file=/etc/vsftpd.chroot_list
secure_chroot_dir=/var/run/vsftpd/empty

# passive mode
listen_port=21
pasv_enable=YES
pasv_min_port=10090
pasv_max_port=10100
port_enable=YES
pasv_addr_resolve=NO
pasv_address=
```

File `/etc/vsftpd.chroot_list`:

```
pusher
```

Create `pusher` user (for 1C exchange):

```bash
useradd --home=/var/www/irmag/shared/var/exchange_1c/ --shell=/bin/sh --comment="1C XML Pusher" pusher
```

Set permissions:

```bash
setfacl -R -m u:pusher:rwX var/exchange_1c
setfacl -dR -m u:pusher:rwX var/exchange_1c
```

## SFTP Server

Create `web` user (for tema.irk):

```bash
useradd --home=/var/www/irmag/shared/web/uploads/custom --shell=/bin/sh --comment="sftp user (tema.irk)" web
```

Set permissions:

```bash
setfacl -R -m u:web:rwX -m u:`whoami`:rwX web/uploads/custom
```

## Yarn

See [installation](https://yarnpkg.com/en/docs/install).

```bash
# add repo
echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
# add GnuPG key
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
# install
apt update
apt install yarn
```
